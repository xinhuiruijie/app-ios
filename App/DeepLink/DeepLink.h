//
//  DeepLink.h
//  TestDeepLinkKit
//
//  Created by Chenyun on 16/3/9.
//  Copyright © 2016年 geek-zoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DeepLinkKit.h"

@interface DeepLink : NSObject

@singleton( DeepLink )

@property (nonatomic, strong) DPLDeepLinkRouter * router;

/**
 *  每次跳转之前设置为YES  跳转完毕后设置为NO，用来判断登录后，是否要执行跳转
 */
@property (nonatomic, assign) BOOL isJump;

/**
 *  注册deeplink
 */
+ (void)powerOn;

+ (BOOL)isOpenWithURLString:(NSString *)urlString;
+ (BOOL)isOpenWithURL:(NSURL *)url;

+ (BOOL)handleURLString:(NSString *)urlString withCompletion:(DPLRouteCompletionBlock)completionHandler;
+ (BOOL)handleURL:(NSURL *)url withCompletion:(DPLRouteCompletionBlock)completionHandler;

- (UIViewController *)returnViewcontrollerWithUrl:(NSString *)url;

@end
