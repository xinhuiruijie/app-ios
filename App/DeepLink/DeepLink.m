//
//  DeepLink.m
//  TestDeepLinkKit
//
//  Created by Chenyun on 16/3/9.
//  Copyright © 2016年 geek-zoo. All rights reserved.
//

#import "DeepLink.h"
#import "DPLDeepLink_Private.h"
#import "RouterParser.h"
#import "AppService.h"

#import "MPBaseWebViewController.h"
#import "AuthorInfoController.h"
#import "ArticleListController.h"
#import "CategoryVideoListController.h"
#import "CategoryListController.h"
#import "VideoInfoController.h"
#import "ArticleInfoController.h"
#import "MessageRemindController.h"
#import "MyFollowListController.h"
#import "MyCollectionController.h"
#import "MyWatchListController.h"
#import "CouponListViewController.h"
#import "SearchViewController.h"
#import "TopicListController.h"
#import "TopicVideoListController.h"
#import "RecentAuthorListController.h"
#import "AudioViewController.h"

@interface DeepLink ()

@property (nonatomic, strong) NSURL * currentUrl;

@end

@implementation DeepLink

@def_singleton( DeepLink )

#pragma mark -

+ (void)load {
	[AppService addService:[self sharedInstance]];
}

+ (void)powerOn {
    [[self sharedInstance] powerOn];
}

- (void)powerOn {
	[self setupRouter];

	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(signin) name:AuthorizationWasSucceedNotification object:nil];
	[[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(cancelSignin) name:AuthorizationWasCancelledNotification object:nil];
}

- (void)signin {
	// 根据当前url进行跳转
	if (self.isJump) {
		[self.router handleURL:self.currentUrl	withCompletion:nil];
		self.isJump = NO;
	}
}

- (void)cancelSignin {
	// 取消登录
	self.isJump = NO;
}

+ (BOOL)handleURLString:(NSString *)urlString withCompletion:(DPLRouteCompletionBlock)completionHandler {
    if (!urlString) {
        return NO;
    }
    
    NSString *encodedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (CFStringRef)urlString, (CFStringRef)@"!$&'()*+,-./:;=?@_~%#[]", NULL, kCFStringEncodingUTF8));
    return [self handleURL:[NSURL URLWithString:encodedString] withCompletion:completionHandler];
}

+ (BOOL)handleURL:(NSURL *)url withCompletion:(DPLRouteCompletionBlock)completionHandler {
    return [[self sharedInstance].router handleURL:url withCompletion:completionHandler];
}

+ (BOOL)isOpenWithURLString:(NSString *)urlString {
    return [[self sharedInstance] isOpenWithUrl:urlString];
}

+ (BOOL)isOpenWithURL:(NSURL *)url {
    return [[self sharedInstance] isOpenWithUrl:url.absoluteString];
}

#pragma mark -

- (BOOL)isOpenWithUrl:(NSString *)url {
    if ([url hasPrefix:[AppConfig sharedInstance].scheme]) {
        return YES;
    } else if ([url hasPrefix:@"http://"]) {
		return YES;
	} else if ([url hasPrefix:@"https://"]) {
		return YES;
	}

	return NO;
}

#pragma mark -

- (void)setupRouter {
	self.router = [[DPLDeepLinkRouter alloc] init];

	@weakify(self);
	self.router[@"http://"] = ^( DPLDeepLink * link ) {
		@strongify(self);
		self.currentUrl = link.URL;
		[self pushWebViewWithUrl:link.URL];
	};

	self.router[@"https://"] = ^( DPLDeepLink * link ) {
		@strongify(self);
		self.currentUrl = link.URL;
		[self pushWebViewWithUrl:link.URL];
	};
    
    self.router[@"mplanet://star"] = ^( DPLDeepLink * link ) {
        @strongify(self);
        [self pushHome];
    };
    self.router[@"mplanet://planet"] = ^( DPLDeepLink * link ) {
        @strongify(self);
        [self pushPlanet];
    };
    self.router[@"mplanet://task"] = ^( DPLDeepLink * link ) {
        @strongify(self);
        [self pushTask];
    };
    self.router[@"mplanet://profile"] = ^( DPLDeepLink * link ) {
        @strongify(self);
        [self pushProfile];
    };
    self.router[@"mplanet://authorInfo/:id"] = ^( DPLDeepLink * link ) {
        @strongify(self);
        NSString *target_id = link.routeParameters[@"id"];
        [self pushAuthorInfo:target_id];
    };
    self.router[@"mplanet://articleList"] = ^( DPLDeepLink * link ) {
        @strongify(self);
        [self pushArticleListController];
    };
    self.router[@"mplanet://categoryVideoList/:id"] = ^( DPLDeepLink * link ) {
        @strongify(self);
        NSString *target_id = link.routeParameters[@"id"];
        [self pushCategoryVideoListController:target_id];
    };
    self.router[@"mplanet://categoryList"] = ^( DPLDeepLink * link ) {
        @strongify(self);
        [self pushCategoryListController];
    };
    self.router[@"mplanet://videoInfo/:id"] = ^( DPLDeepLink * link ) {
        @strongify(self);
        NSString *target_id = link.routeParameters[@"id"];
        [self pushVideoInfo:target_id];
    };
    self.router[@"mplanet://articleInfo/:id"] = ^( DPLDeepLink * link ) {
        @strongify(self);
        NSString *target_id = link.routeParameters[@"id"];
        [self pushArticleInfo:target_id];
    };
    self.router[@"mplanet://messageList"] = ^( DPLDeepLink * link ) {
        @strongify(self);
        [self pushMessageList];
    };
    self.router[@"mplanet://myFollowAuthorList"] = ^( DPLDeepLink * link ) {
        @strongify(self);
        [self pushMyFollowAuthorList];
    };
    self.router[@"mplanet://myVideoCollectList"] = ^( DPLDeepLink * link ) {
        @strongify(self);
        [self pushMyCollectVideo];
    };
    self.router[@"mplanet://myTopicCollectList"] = ^( DPLDeepLink * link ) {
        @strongify(self);
        [self pushMyCollectTopic];
    };
    self.router[@"mplanet://myWatchList"] = ^( DPLDeepLink * link ) {
        @strongify(self);
        [self pushMyWatchList];
    };
    self.router[@"mplanet://myCouponList"] = ^( DPLDeepLink * link ) {
        @strongify(self);
        [self pushMyCouponList];
    };
    self.router[@"mplanet://search"] = ^( DPLDeepLink * link ) {
        @strongify(self);
        NSString *keyword = link.queryParameters[@"k"];
        [self pushSearch:keyword];
    };
    self.router[@"mplanet://topicList"] = ^( DPLDeepLink * link ) {
        @strongify(self);
        [self pushTopicList];
    };
    self.router[@"mplanet://topicVideoList/:id"] = ^( DPLDeepLink * link ) {
        @strongify(self);
        NSString *target_id = link.routeParameters[@"id"];
        [self pushTopicVideoList:target_id];
    };
    self.router[@"mplanet://recentJoinAuthor"] = ^( DPLDeepLink * link) {
        @strongify(self)
        [self pushRecentJoinAuthor];
    };
    self.router[@"mplanet://audioInfo/:id"] = ^( DPLDeepLink * link ) {
        @strongify(self);
        NSString *target_id = link.routeParameters[@"id"];
        [self pushAudioList:target_id];
    };
}

#pragma mark -

/**
 * 跳转到作者详情
 */
- (void)pushAuthorInfo:(NSString *)author_id {
    USER *author = [[USER alloc] init];
    author.id = author_id;
    author.is_author = YES;
    AuthorInfoController *authorInfo = [AuthorInfoController spawn];
    authorInfo.author = author;
    authorInfo.isFromMessage = YES;
    authorInfo.hidesBottomBarWhenPushed = YES;
    [[self topViewController] presentNavigationController:authorInfo];
}

/**
 * 跳转到日报精选列表
 */
- (void)pushArticleListController {
    ArticleListController *articleList = [ArticleListController spawn];
    articleList.hidesBottomBarWhenPushed = YES;
    [[self topViewController].navigationController pushViewController:articleList animated:YES];
}

/**
 * 跳转到分类视频列表
 */
- (void)pushCategoryVideoListController:(NSString *)category_id {
    CATEGORY *category = [[CATEGORY alloc] init];
    category.id = category_id;
    CategoryVideoListController *categoryVideoList = [CategoryVideoListController spawn];
    categoryVideoList.category = category;
    categoryVideoList.hidesBottomBarWhenPushed = YES;
    [[self topViewController].navigationController pushViewController:categoryVideoList animated:YES];
}

/**
 * 跳转到全部分类列表
 */
- (void)pushCategoryListController {
    CategoryListController *categoryList = [CategoryListController spawn];
    categoryList.hidesBottomBarWhenPushed = YES;
    [[self topViewController].navigationController pushViewController:categoryList animated:YES];
}

- (UIViewController *)returnViewcontrollerWithUrl:(NSString *)url {
    UIViewController *board = [[UIViewController alloc] init];
    board.hidesBottomBarWhenPushed = NO;
    board.view.backgroundColor = [AppTheme backgroundColor];
    return board;
}

#pragma mark -

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
	if ( [self isOpenWithUrl:url.absoluteString] )
	{
		return [self.router handleURL:url withCompletion:nil];
	}

	return YES;
}

#pragma mark - Push Controller

/**
 *  跳转到web页面
 */
- (void)pushWebViewWithUrl:(NSURL *)url {
    NSString *urlStr = [NSString stringWithFormat:@"%@", url.absoluteString];
    MPBaseWebViewController *webViewController = [[MPBaseWebViewController alloc] init];
    webViewController.url = urlStr;
    webViewController.hidesBottomBarWhenPushed = YES;
    [[self topViewController].navigationController pushViewController:webViewController animated:YES];
}

/**
 * 跳转到首页
 */
- (void)pushHome {
    [[MPRootViewController sharedInstance].currentViewController.navigationController popViewControllerAnimated:NO];
    if ([MPRootViewController sharedInstance].selectedIndex != 0) {
        [MPRootViewController sharedInstance].selectedIndex = 0;
    }
}

/**
 * 跳转到星球页
 */
- (void)pushPlanet {
    [[MPRootViewController sharedInstance].currentViewController.navigationController popViewControllerAnimated:NO];
    if ([MPRootViewController sharedInstance].selectedIndex != 1) {
        [MPRootViewController sharedInstance].selectedIndex = 1;
    }
}

/**
 * 跳转到实验室
 */
- (void)pushTask {
    [[MPRootViewController sharedInstance].currentViewController.navigationController popViewControllerAnimated:NO];
    if ([MPRootViewController sharedInstance].selectedIndex != 2) {
        [MPRootViewController sharedInstance].selectedIndex = 2;
    }
}

/**
 * 跳转到个人中心
 */
- (void)pushProfile {
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }
    [[MPRootViewController sharedInstance].currentViewController.navigationController popViewControllerAnimated:NO];
    if ([MPRootViewController sharedInstance].selectedIndex != 4) {
        [MPRootViewController sharedInstance].selectedIndex = 4;
    }
}

/**
 * 跳转到视频详情
 */
- (void)pushVideoInfo:(NSString *)video_id {
    VIDEO *video = [[VIDEO alloc] init];
    video.id = video_id;
    
    VideoInfoController *videoInfo = [VideoInfoController spawn];
    videoInfo.video = video;
    videoInfo.hidesBottomBarWhenPushed = YES;
//    [[self topViewController] presentNavigationController:videoInfo];
    [[self topViewController].navigationController pushViewController:videoInfo animated:YES];
}

/**
 * 跳转到文章详情
 */
- (void)pushArticleInfo:(NSString *)article_id {
    ARTICLE *article = [[ARTICLE alloc] init];
    article.id = article_id;
    ArticleInfoController *articleInfo = [ArticleInfoController spawn];
    articleInfo.article = article;
    articleInfo.hidesBottomBarWhenPushed = YES;
    [[self topViewController] presentNavigationController:articleInfo];
}

/**
 * 跳转到消息提醒页
 */
- (void)pushMessageList {
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }
    MessageRemindController *messageList = [MessageRemindController spawn];
    messageList.hidesBottomBarWhenPushed = YES;
    messageList.topController = [self topViewController];
    messageList.selectIndex = 1;
    [[self topViewController].navigationController pushViewController:messageList animated:YES];
}

/**
 * 跳转到我的关注
 */
- (void)pushMyFollowAuthorList {
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }
    MyFollowListController *myFollowList = [MyFollowListController spawn];
    myFollowList.hidesBottomBarWhenPushed = YES;
    [[self topViewController].navigationController pushViewController:myFollowList animated:YES];
}

/**
 * 跳转到我的收藏视频
 */
- (void)pushMyCollectVideo {
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }
    MyCollectionController *collectVideo = [MyCollectionController spawn];
    collectVideo.hidesBottomBarWhenPushed = YES;
    [[self topViewController].navigationController pushViewController:collectVideo animated:YES];
}


/**
 * 跳转到我的收藏话题
 */
- (void)pushMyCollectTopic {
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }
    MyCollectionController *collectTopic = [MyCollectionController spawn];
    collectTopic.selectIndex = 1;
    collectTopic.hidesBottomBarWhenPushed = YES;
    [[self topViewController].navigationController pushViewController:collectTopic animated:YES];
}

/**
 * 跳转到我的足迹
 */
- (void)pushMyWatchList {
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }
    MyWatchListController *myWatchList = [MyWatchListController spawn];
    myWatchList.hidesBottomBarWhenPushed = YES;
    [[self topViewController].navigationController pushViewController:myWatchList animated:YES];
}

/**
 * 跳转到我的卡包
 */
- (void)pushMyCouponList {
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }
    CouponListViewController *couponList = [CouponListViewController spawn];
    couponList.hidesBottomBarWhenPushed = YES;
    [[self topViewController].navigationController pushViewController:couponList animated:YES];
}

/**
 * 跳转到搜索
 */
- (void)pushSearch:(NSString *)keyword {
    SearchViewController *search = [SearchViewController spawn];
    search.searchType = SEARCH_TYPE_ALL;
    if (keyword && keyword.length) {
        search.actionState = SearchActionStateDone;
        search.keyword = keyword;
    } else {
        search.actionState = SearchActionStatePending;
    }
    search.hidesBottomBarWhenPushed = YES;
    [[self topViewController].navigationController pushViewController:search animated:NO];
}

/**
 * 跳转全部话题列表
 */
- (void)pushTopicList {
    TopicListController *topicList = [TopicListController spawn];
    topicList.hidesBottomBarWhenPushed = YES;
    [[self topViewController].navigationController pushViewController:topicList animated:YES];
}

/**
 * 跳转话题详情列表
 */
- (void)pushTopicVideoList:(NSString *)topic_id {
    TopicVideoListController *topicVideoList = [TopicVideoListController spawn];
    TOPIC *topic = [[TOPIC alloc] init];
    topic.id = topic_id;
    topicVideoList.topic = topic;
    topicVideoList.hidesBottomBarWhenPushed = YES;
    [[self topViewController].navigationController pushViewController:topicVideoList animated:YES];
}

/**
 * 跳转最新加入星球
 */
- (void)pushRecentJoinAuthor {
    RecentAuthorListController *recentJoinAuthor = [RecentAuthorListController spawn];
    recentJoinAuthor.hidesBottomBarWhenPushed = YES;
    [[self topViewController].navigationController pushViewController:recentJoinAuthor animated:YES];
}

/**
 * 跳转FM爱音斯坦列表。id传当前正在播放的id
 */
- (void)pushAudioList:(NSString *)audioId {
    AUDIO *audio = [[AUDIO alloc] init];
    audio.id = audioId;
    AudioViewController *audioController = [AudioViewController spawn];
    audioController.currentAudio = audio;
    [[self topViewController] presentViewController:audioController animated:YES completion:nil];
}

@end
