//
//  NSObject+APIExtension.h
//  MotherPlanet
//
//  Created by liuyadi on 16/9/19.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (APIExtension) <AutoModelCoding>

@end
