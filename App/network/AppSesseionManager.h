//
//  AppSesseionManager.h
//  fomo
//
//  Created by QFish on 3/20/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import "STIHTTPSessionManager.h"

@interface AppSesseionManager : STIHTTPSessionManager

+ (instancetype)sharedManager;
- (void)encryRequestHeader:(id)parameters;
- (NSMutableDictionary *)getEncryPostbodyParams:(id)parameters;
- (NSDictionary *)convertDictionary:(NSDictionary *)parameters;

@end
