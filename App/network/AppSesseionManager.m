//
//  AppSesseionManager.m
//  fomo
//
//  Created by QFish on 3/20/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import "AppSesseionManager.h"
#import "UserModel.h"
#import "AFURLRequestSerialization.h"
#import "NSString+APP_ENCRYPTION.h"
#import "TraceEncryption.h"

typedef NS_ENUM(NSUInteger, CLIENT_ERROR_CODE) {
    CLIENT_ERROR_CODE_OK             = 0,     // 正常
    CLIENT_ERROR_CODE_UNKNOWN_ERROR  = 500,   // 内部错误
    CLIENT_ERROR_CODE_BAD_REQUEST    = 101,   // 错误的请求
//    CLIENT_ERROR_CODE_AUTH_FAIL      = 401, // APPKEY与APPSEC认证失败
    CLIENT_ERROR_CODE_AUTH_INVALID   = 10001, // token无效
    CLIENT_ERROR_CODE_AUTH_EXPIRED   = 10002, // 授权过期
    CLIENT_ERROR_CODE_NOT_FOUND      = 404,   // 页面不存在
    CLIENT_ERROR_CODE_ACCOUNT_LOCKED = 600,   // 账号被锁定
    CLIENT_ERROR_CODE_EMPTY_DATA     = 201,   // 操作成功，空数据
};

@implementation AppSesseionManager

+ (void)load {
    [STIHTTPApi setGlobalHTTPSessionManager:[self sharedManager]];
}

- (id)processedDataWithResponseObject:(id)responseObject task:(NSURLSessionDataTask *)task {
	if ([responseObject isKindOfClass:NSDictionary.class]) {
		NSNumber *succeed = [responseObject valueForKey:@"succeed"];
		if (!succeed.boolValue) {
			NSNumber *error_code = [responseObject valueForKey:@"error_code"];

            if (error_code.intValue == CLIENT_ERROR_CODE_AUTH_EXPIRED ||
                error_code.intValue == CLIENT_ERROR_CODE_AUTH_INVALID) {
                [[UserModel sharedInstance] kickout:nil];
            }
		}
	}

    return responseObject;
}

- (id)processedDataWithResponseObject:(id)responseObject task:(NSURLSessionDataTask *)task allHeaders:(NSDictionary *)allHeaders {
    if (allHeaders) {
        if (X_MPlanet_ErrorCode(allHeaders) ==  CLIENT_ERROR_CODE_AUTH_EXPIRED ||
            X_MPlanet_ErrorCode(allHeaders) == CLIENT_ERROR_CODE_AUTH_INVALID) {
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                NSString *errorDesc = X_MPlanet_ErrorDesc(allHeaders);
                [[UserModel sharedInstance] kickout:errorDesc];
            });
        }
    }
    
    return responseObject;
}

- (void)handleError:(NSError *)error responseObject:(id)responseObject task:(NSURLSessionDataTask *)task failureBlock:(void (^)(id data, NSDictionary * allHeaders, id error))failureBlock {
    STIHTTPResponseError *e = [STIHTTPResponseError new];
    e.code = error.code;
    e.message = responseObject[@"message"];
    
    if (failureBlock) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        NSDictionary *allHeaders = response.allHeaderFields;
        [self processedDataWithResponseObject:responseObject task:task allHeaders:allHeaders];
        e.code = X_MPlanet_ErrorCode(allHeaders);
        e.message = X_MPlanet_ErrorDesc(allHeaders);
        
        if (!e.message.length) {
            // 检测当前网络状态
//            if ([ConfigModel currentNetworkStatus] == NotReachable) {
//                e.message = LocalizedStringWithKeyFromTable(@"network_not_reachable", nil);
//                e.code = NSURLErrorNotConnectedToInternet;
//            }
        }
        
        failureBlock(nil,allHeaders, e);
    }
}

// 加密header
- (void)encryRequestHeader:(id)parameters {
    // 添加head参数
    if ([UserModel sharedInstance].token && [UserModel sharedInstance].token.length) {
        [[AppSesseionManager sharedManager].requestSerializer setValue:[UserModel sharedInstance].token forHTTPHeaderField:@"Authorization"];
    } else {
        [[AppSesseionManager sharedManager].requestSerializer setValue:nil forHTTPHeaderField:@"Authorization"];
    }
    
    // 添加head参数:传入X-Sign: HMAC-SHA256(TIMESTAMP + BODY, KEY), 时间戳
    NSString *timeStampString = [NSString stringWithFormat:@"%ld", (long)[[NSDate date] timeIntervalSince1970]];
    
    NSString *post_body = [self getEncryPostbodyValue:parameters];
    post_body = [post_body URLEncoding];
    NSString *sign = [[timeStampString stringByAppendingString:[NSString stringWithFormat:@"x=%@",post_body]] appEncryptionWithKey:APP_ENCRYPTION_KEY];
    
    NSString *xSign = [sign stringByAppendingString:[NSString stringWithFormat:@",%@", timeStampString]];
    
    // 添加head参数:X-Sign
    [[AppSesseionManager sharedManager].requestSerializer setValue:xSign forHTTPHeaderField:@"X-MPlanet-Sign"];
    // 添加head参数:X-UDID
    [[AppSesseionManager sharedManager].requestSerializer setValue:[STISystemInfo deviceUUID] forHTTPHeaderField:@"X-MPlanet-UDID"];
    
    // 添加head参数:X-Ver
    [[AppSesseionManager sharedManager].requestSerializer setValue:[STISystemInfo appVersion] forHTTPHeaderField:@"X-MPlanet-Ver"];
    
    // 添加head参数:X-UserAgent
    NSString * xUserAgent = [NSString stringWithFormat:@"Platform/iOS, Device/%@,  ScreenWidth/%lf, ScreenHeight/%lf", [UIDevice currentDevice].model, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height];
    [[AppSesseionManager sharedManager].requestSerializer setValue:xUserAgent forHTTPHeaderField:@"X-MPlanet-UserAgent"];
}

// 获取加密的postbody
- (NSString *)getEncryPostbodyValue:(id)parameters {
    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:parameters];
    NSString *body = AFQueryStringFromParameters(params);
    /*
     post body 加密
     
     postbody = password=123456&username=buyer
     key = getprogname()
     x = base64(XXTEA(postbody, key))
     */
    
    NSString *post_body = @"";
    if (body && body.length) {
        post_body = [TraceEncryption encryptStringToBase64String:body stringKey:TRACE_ENCRYPTION_KEY sign:YES];
    }
    
    return post_body;
}

// 获取加密的postbody
- (NSMutableDictionary *)getEncryPostbodyParams:(id)parameters {
    // 设置params
    NSMutableDictionary *paramsEncryptionDic = [NSMutableDictionary dictionary];
    NSString *post_body = [self getEncryPostbodyValue:parameters];
    
    if (post_body) {
        [paramsEncryptionDic setValue:post_body forKey:@"x"];
    }
    
    return paramsEncryptionDic;
}

- (NSURLSessionDataTask *)method:(STIHTTPRequestMethod)method endpoint:(NSString *)endpoint parameters:(id)parameters completion:(void (^)(NSURLSessionDataTask *, id, NSError *))completion {
    // 设置header
    [self encryRequestHeader:parameters];
    
    // 设置params
    NSMutableDictionary *paramsEncryptionDic = [self getEncryPostbodyParams:parameters];
    NSURLSessionDataTask *task = [super method:method endpoint:endpoint parameters:paramsEncryptionDic completion:completion];
    return task;
}

+ (instancetype)sharedManager {
    static AppSesseionManager *sharedManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSString *baseURL = kAppServerAPIURL;

        sharedManager = [[AppSesseionManager alloc] initWithBaseURL:[NSURL URLWithString:baseURL]];
        sharedManager.requestSerializer = [AFHTTPRequestSerializer serializer];
		[sharedManager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
        // 设置超时时间
        sharedManager.requestSerializer.timeoutInterval = 60;
        sharedManager.setup = ^(id vars,...) {
            [sharedManager setupParams];
        };
    });
    return sharedManager;
}

- (void)setupParams{
}

#pragma mark -


- (NSDictionary *)convertDictionary:(NSDictionary *)parameters {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    for (__unsafe_unretained NSString *key in parameters) {
        id value = [parameters valueForKey:key];
        if ([value isKindOfClass:[NSDictionary class]]) {
            value = [value JSONStringRepresentation];
        } else if ([value isKindOfClass:[NSArray class]]) {
            value = [self jsonArrayString:value];
        }
        
        [dict setObject:value forKey:key];
    }
    return dict.count ? dict : nil;
}

- (NSString *)jsonArrayString:(NSObject *)value {
    __block NSMutableString *jsonString = [NSMutableString string];
    
    if ([value isKindOfClass:[NSArray class]]) {
        [jsonString appendString:@"["];
        [(NSArray *)value enumerateObjectsUsingBlock:^(NSObject * obj, NSUInteger idx, BOOL *stop) {
            NSString *itemString;
            if ([obj.class isSubclassOfClass:NSValue.class]
                || [obj.class isSubclassOfClass:NSString.class]
                || [obj.class isSubclassOfClass:NSDictionary.class]
                ) {
                itemString = [obj JSONStringRepresentation];
            } else if ([obj.class isSubclassOfClass:NSArray.class]) {
                itemString = [self jsonArrayString:obj];
            }
            
            [jsonString appendString:[NSString stringWithFormat:@"%@,",itemString]];
        }];
        
        NSUInteger location = [jsonString length]-1;
        NSRange range = NSMakeRange(location, 1);
        [jsonString replaceCharactersInRange:range withString:@"]"];
    } else {
        jsonString = [NSMutableString stringWithString:[value JSONStringRepresentation]];
    }
    
    return jsonString;
}

@end
