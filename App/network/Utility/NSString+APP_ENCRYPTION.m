//
//  NSString+SHA.m
//  ECMall
//
//  Created by Chenyun on 16/4/20.
//  Copyright © 2016年 geek-zoo. All rights reserved.
//

#import "NSString+APP_ENCRYPTION.h"
#import <CommonCrypto/CommonDigest.h>
#import <CommonCrypto/CommonHMAC.h>

@implementation NSString (APP_ENCRYPTION)

- (NSString *)appEncryptionWithKey:(NSString *)key
{
	const char *cKey = [key cStringUsingEncoding:NSASCIIStringEncoding];
	const char *cData = [self cStringUsingEncoding:NSASCIIStringEncoding];

	unsigned char cHMAC[CC_SHA256_DIGEST_LENGTH];

	CCHmac(kCCHmacAlgSHA256, cKey, strlen(cKey), cData, strlen(cData), cHMAC);
	NSData *hash = [[NSData alloc] initWithBytes:cHMAC length:sizeof(cHMAC)];

	NSString *string = [hash description];
	string = [string stringByReplacingOccurrencesOfString:@" " withString:@""];
	string = [string stringByReplacingOccurrencesOfString:@"<" withString:@""];
	string = [string stringByReplacingOccurrencesOfString:@">" withString:@""];

	if ( string && [string isKindOfClass:[NSString class]] && string.length )
	{
		string = [string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
	}
	else
	{
		string = nil;
	}

	string = [string stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];

	return string;
}

@end
