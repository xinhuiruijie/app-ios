//
//  NSString+SHA.h
//  ECMall
//
//  Created by Chenyun on 16/4/20.
//  Copyright © 2016年 geek-zoo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (APP_ENCRYPTION)

- (NSString *)appEncryptionWithKey:(NSString *)key;

@end
