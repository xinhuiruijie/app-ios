//
//  NSObject+APIExtension.m
//  MotherPlanet
//
//  Created by liuyadi on 16/9/19.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import "NSObject+APIExtension.h"

@implementation NSObject (APIExtension)

+ (id)processedValueForKey:(NSString *)key
               originValue:(id)originValue
            convertedValue:(id)convertedValue
                     class:(__unsafe_unretained Class)clazz
                  subClass:(__unsafe_unretained Class)subClazz
{
    if ( [clazz isEqual:NSNumber.class] )
    {
        if (  [originValue isKindOfClass:NSString.class] )
        {
            return [NSNumber numberWithDouble:[originValue doubleValue]];
        }
    }
    
    if ( [clazz isEqual:NSString.class] )
    {
        if (  [originValue isKindOfClass:NSNumber.class] )
        {
            return [originValue description];
        }
    }
    
    return convertedValue;
}

@end
