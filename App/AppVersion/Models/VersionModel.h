//
//  VersionModel.h
//  MotherPlanet
//
//  Created by 陈熙 on 2018/1/3.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "STIOnceModel.h"

@interface VersionModel : STIOnceModel

- (void)checkVersionCompletion:(void (^)(STIHTTPResponseError *e, VERSION *version))completion;

@end

