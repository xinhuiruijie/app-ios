//
//  VersionModel.m
//  MotherPlanet
//
//  Created by 陈熙 on 2018/1/3.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "VersionModel.h"

@implementation VersionModel

- (void)checkVersionCompletion:(void (^)(STIHTTPResponseError *, VERSION *))completion {
    V1_API_VERSION_CHECK_API *api = [[V1_API_VERSION_CHECK_API alloc] init];
    api.whenUpdated = ^(V1_API_VERSION_CHECK_RESPONSE *resp, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if (error == nil) {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                self.item = resp.version;
                [self saveCache];
                PERFORM_BLOCK_SAFELY(completion, nil, self.item);
            } else {
                STIHTTPResponseError *errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(completion, errors, nil);
            }
        } else {
            PERFORM_BLOCK_SAFELY(completion, error, nil);
        }
    };
    [api send];
}

@end

