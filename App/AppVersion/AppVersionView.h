//
//  AppVersionView.h
//  ParkviewGreen
//
//  Created by liuyadi on 2017/3/23.
//  Copyright © 2017年 GeekZooStudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppVersionView : UIView

- (instancetype)initWithTitle:(NSString *)title
                       message:(NSString *)message
                   updateType:(UPDATE_TYPE)updateType;

@property (nonatomic, copy) void(^cancelAction)(void);
@property (nonatomic, copy) void(^confirmAction)(void);

- (void)show;
- (void)hide;

@end
