//
//  AppVersionView.m
//  ParkviewGreen
//
//  Created by liuyadi on 2017/3/23.
//  Copyright © 2017年 GeekZooStudio. All rights reserved.
//

#import "AppVersionView.h"

@interface AppVersionView ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIView *buttonView;

@end

@implementation AppVersionView

#pragma mark -

- (instancetype)initWithTitle:(NSString *)title message:(NSString *)message updateType:(UPDATE_TYPE)updateType {
    AppVersionView *view = [AppVersionView loadFromNib];
    view.titleLabel.text = title;
    view.messageLabel.text = message;
    
    CGFloat buttonWidth = view.buttonView.width;
    CGFloat orgingX = 0;
    if (updateType == UPDATE_TYPE_OPTIONAL) {
        // 可选升级
        buttonWidth = (view.buttonView.width - 10) / 2;
        UIButton *cancelButton = [[UIButton alloc] initWithFrame:CGRectMake(orgingX, 0, buttonWidth, view.buttonView.height)];
        [cancelButton setTitle:@"稍后再说" forState:UIControlStateNormal];
        [cancelButton setTitleColor:[UIColor colorWithRGBValue:0xA9AEB5] forState:UIControlStateNormal];
        [cancelButton addTarget:self action:@selector(cancelButtonAction) forControlEvents:UIControlEventTouchUpInside];
        [cancelButton setBackgroundColor:[UIColor whiteColor]];
        [cancelButton.titleLabel setFont:[UIFont systemFontOfSize:15]];
        cancelButton.titleLabel.adjustsFontSizeToFitWidth = YES;
        cancelButton.titleLabel.minimumScaleFactor = 0.5;
        cancelButton.layer.borderWidth = [AppTheme onePixel];
        [view.buttonView addSubview:cancelButton];
        orgingX = buttonWidth + 10;
    }
    
    UIButton *confirmButton = [[UIButton alloc] initWithFrame:CGRectMake(orgingX, 0, buttonWidth, view.buttonView.height)];
    [confirmButton setTitle:@"立即更新" forState:UIControlStateNormal];
    [confirmButton addTarget:self action:@selector(confirmButtonAction) forControlEvents:UIControlEventTouchUpInside];
    [confirmButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [confirmButton setBackgroundColor:[UIColor colorWithRGBValue:0x343338]];
    [confirmButton.titleLabel setFont:[UIFont systemFontOfSize:15]];
    confirmButton.titleLabel.adjustsFontSizeToFitWidth = YES;
    confirmButton.titleLabel.minimumScaleFactor = 0.5;
    [view.buttonView addSubview:confirmButton];
    
    return view;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.alpha = 0.0;
}

- (void)setFrame:(CGRect)frame {
    [super setFrame:[UIScreen mainScreen].bounds];
}

- (void)cancelButtonAction {
    if (self.cancelAction) {
        [self hide];
        self.cancelAction();
    }
}

- (void)confirmButtonAction {
    if (self.confirmAction) {
        [self hide];
        self.confirmAction();
    }
}

- (void)show {
    [[MPRootViewController sharedInstance].view addSubview:self];
    [UIView animateWithDuration:0.25 animations:^{
        self.alpha = 1.0;
    }];
}

- (void)hide {
    [UIView animateWithDuration:0.25 animations:^{
        self.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

@end
