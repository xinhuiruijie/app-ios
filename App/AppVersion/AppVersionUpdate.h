//
//  AppVersionUpdate.h
//  ECMall
//
//  Created by Chenyun on 16/3/29.
//  Copyright © 2016年 geek-zoo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppService.h"

@interface AppVersionUpdate : NSObject

@singleton( AppVersionUpdate )

@property (nonatomic, assign) BOOL isTipHidden;

+ (void)versionAppUpdate;

@end
