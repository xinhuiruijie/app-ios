//
//  AppVersionUpdate.m
//  ECMall
//
//  Created by Chenyun on 16/3/29.
//  Copyright © 2016年 geek-zoo. All rights reserved.
//

#import "AppVersionUpdate.h"
#import "VersionModel.h"
#import "AppVersionView.h"

@interface AppVersionUpdate ()

@end

@implementation AppVersionUpdate

@def_singleton( AppVersionUpdate )

#pragma mark -

+ (void)load
{
    [AppService addService:[self sharedInstance]];
}

+ (void)versionAppUpdate
{
    [AppVersionUpdate sharedInstance].isTipHidden = YES;
    [[self new] versionAppUpdate];
}

- (void)versionAppUpdate
{
    [[VersionModel alloc] checkVersionCompletion:^(STIHTTPResponseError *e, VERSION *version) {
        if ( e == nil )
        {
            if ( version && [version isKindOfClass:[VERSION class]] )
            {
                if ([AppVersionUpdate sharedInstance].isTipHidden == NO) {
                    return;
                }
                
                if (version.type == UPDATE_TYPE_NONE) {
                    // 不需要升级
                    return;
                }
                
                MPAlertView *alertView;
                if (version.type == UPDATE_TYPE_FORCE) {
                    alertView = [MPAlertView alertWithTitle:[NSString stringWithFormat:@"发现新版本 v%@", version.version] message:version.content cancelButton:nil confirmButton:@"立即更新"];
                } else {
                    alertView = [MPAlertView alertWithTitle:[NSString stringWithFormat:@"发现新版本 v%@", version.version] message:version.content cancelButton:@"稍后再说" confirmButton:@"立即更新"];
                }
                alertView.cancelAction = ^{
                    [AppVersionUpdate sharedInstance].isTipHidden = YES;
                };
                alertView.confirmAction = ^{
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:version.url]];
                    if (version.type == UPDATE_TYPE_FORCE) {
                        exit(0);
                    }
                };
                
                if (version.type == UPDATE_TYPE_OPTIONAL) {
                    NSUserDefaults *userDefault = [NSUserDefaults standardUserDefaults];
                    NSInteger oldtime = [userDefault integerForKey:@"AppVersionUpdate"];
                    
                    if ( [[NSDate date]timeIntervalSince1970] - oldtime <= 60 * 60 * 24 )
                    {
                        return;
                    }
                }
                
                [alertView show];
                
                NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
                
                [userDefaults setInteger:[[NSDate date]timeIntervalSince1970] forKey:@"AppVersionUpdate"];
                [userDefaults synchronize];
                
                [AppVersionUpdate sharedInstance].isTipHidden = NO;
            }
        }
    }];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    //进入前台 ---applicationDidBecomeActive----
    // 只有在一级页面的时候，才会去
    if ([MPRootViewController sharedInstance].currentViewController.navigationController.viewControllers.count == 1) {
        [self versionAppUpdate];
    }
}

@end

