//
//  AppRealm.h
//  MotherPlanet
//
//  Created by GeekZooStudio on 16/10/28.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppRealm : NSObject

+ (void)setup;

@end
