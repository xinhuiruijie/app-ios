//
//  AppRealm.m
//  MotherPlanet
//
//  Created by GeekZooStudio on 16/10/28.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import "AppRealm.h"

@implementation AppRealm

+ (void)setup
{
    // Realm数据库配置
    RLMRealmConfiguration *configutation = [RLMRealmConfiguration defaultConfiguration];
    // realm构建版本号
    // TODO: 用于数据迁移，版本迭代时需要提示此版本号
    configutation.schemaVersion = 2;
    // realm加密key
    configutation.encryptionKey = [self getKey];
    
    // realm数据迁移
    configutation.migrationBlock = ^(RLMMigration *migration, uint64_t oldSchemaVersion) {
        // 目前我们还未进行数据迁移，因此 oldSchemaVersion == 0
//        if (oldSchemaVersion < 1) {
            // 什么都不要做！Realm 会自行检测新增和需要移除的属性，然后自动更新硬盘上的数据库架构
//        }
    };
    // 设置默认config
    [RLMRealmConfiguration setDefaultConfiguration:configutation];
    // 使用默认config打开数据库文件
    RLMRealm *realm = [RLMRealm defaultRealm];
    if (!realm) {
        NSLog(@"Error opening realm");
    }
}

// 加密key
+ (NSData *)getKey
{
    // 应用唯一的key标识符
    static const uint8_t kKeychainIdentifier[] = "io.Realm.com.motherplanet.planet";
    NSData *tag = [[NSData alloc] initWithBytesNoCopy:(void *)kKeychainIdentifier
                                               length:sizeof(kKeychainIdentifier)
                                         freeWhenDone:NO];
    
    // 检测key是否已存在
    NSDictionary *query = @{(__bridge id)kSecClass: (__bridge id)kSecClassKey,
                            (__bridge id)kSecAttrApplicationTag: tag,
                            (__bridge id)kSecAttrKeySizeInBits: @512,
                            (__bridge id)kSecReturnData: @YES};
    
    CFTypeRef dataRef = NULL;
    OSStatus status = SecItemCopyMatching((__bridge CFDictionaryRef)query, &dataRef);
    if (status == errSecSuccess) {
        return (__bridge NSData *)dataRef;
    }
    
    // key不存在，创建key
    uint8_t buffer[64];
    status = SecRandomCopyBytes(kSecRandomDefault, 64, buffer);
    NSAssert(status == 0, @"Failed to generate random bytes for key");
    NSData *keyData = [[NSData alloc] initWithBytes:buffer length:sizeof(buffer)];
    
    // 保存key
    query = @{(__bridge id)kSecClass: (__bridge id)kSecClassKey,
              (__bridge id)kSecAttrApplicationTag: tag,
              (__bridge id)kSecAttrKeySizeInBits: @512,
              (__bridge id)kSecValueData: keyData};
    
    status = SecItemAdd((__bridge CFDictionaryRef)query, NULL);
    NSAssert(status == errSecSuccess, @"Failed to insert new key in the keychain");
    
    return keyData;
}

@end
