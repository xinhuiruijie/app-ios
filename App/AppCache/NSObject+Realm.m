//
//  NSObject+Realm.m
//  MotherPlanet
//
//  Created by PURPLEPENG on 21/04/2017.
//  Copyright © 2017 GeekZooStudio. All rights reserved.
//

#import "NSObject+Realm.h"

@implementation NSObject (Realm)

- (id)loadCacheWithKey:(NSString *)key
           objectClass:(Class)clazz{
    return [[RealmManager sharedInstance] loadCacheWithKey:key
                                                  objectClass:clazz];
}

- (void)saveCache:(id)data
              key:(NSString *)key {
    [[RealmManager sharedInstance] saveCache:data
                                            key:key];
}

- (void)clearCacheWithKey:(NSString *)key {
    [[RealmManager sharedInstance] clearCacheWithKey:key];
}

@end
