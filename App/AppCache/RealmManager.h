//
//  RealmModel.h
//  MotherPlanet
//
//  Created by GeekZooStudio on 16/9/18.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import <Realm/Realm.h>
#import "RealmCacheModel.h"

@interface RealmManager : NSObject

+ (id)sharedInstance;

- (id)loadCacheWithKey:(NSString *)key
           objectClass:(Class)clazz;
- (void)saveCache:(id)data 
              key:(NSString *)key;
- (void)clearCacheWithKey:(NSString *)key;

@end

