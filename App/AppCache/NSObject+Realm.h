//
//  NSObject+Realm.h
//  MotherPlanet
//
//  Created by PURPLEPENG on 21/04/2017.
//  Copyright © 2017 GeekZooStudio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RealmCacheModel.h"
#import "RealmManager.h"

@interface NSObject (Realm)

- (id)loadCacheWithKey:(NSString *)key
           objectClass:(Class)clazz;
- (void)saveCache:(id)data
              key:(NSString *)key;
- (void)clearCacheWithKey:(NSString *)key;

@end
