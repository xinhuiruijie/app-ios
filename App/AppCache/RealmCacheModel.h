//
//  RealmCacheModel.h
//  MotherPlanet
//
//  Created by PURPLEPENG on 21/04/2017.
//  Copyright © 2017 GeekZooStudio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <Realm/Realm.h>

@interface RealmCacheModel : RLMObject

@property (nonatomic, strong) NSString *key;
@property (nonatomic, strong) NSData *data;

@end

RLM_ARRAY_TYPE(RealmCacheModel)
