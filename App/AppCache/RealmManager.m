//
//  RealmModel.m
//  MotherPlanet
//
//  Created by GeekZooStudio on 16/9/18.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import "RealmManager.h"

@implementation RealmManager

static RealmManager *defaultManager = nil;

#pragma mark -

+ (id)sharedInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if (defaultManager == nil) {
            defaultManager = [[self alloc] init];
        }
    });
    
    return defaultManager;
}

#pragma mark -

- (id)loadCacheWithKey:(NSString *)key
           objectClass:(Class)clazz {
    if (key == nil) {
        return nil;
    }
    
    RLMResults<RealmCacheModel *> *results = [RealmCacheModel objectsWhere:@"key == %@", key];
    RealmCacheModel *model = results.firstObject;
    if (model) {
        id item = [model.data JSONDecoded];
        
        if (item == nil || [item isKindOfClass:[NSDecimalNumber class]]){
            NSString *obj = [[NSString alloc] initWithData:model.data encoding:NSUTF8StringEncoding];
            return obj;
        }
        
        if ([item isKindOfClass:[NSArray class]]) {
            NSArray *obj = [NSObject ac_objectsWithArray:item objectClass:clazz];
            return obj;
        } else if ([item isKindOfClass:[NSDictionary class]]) {
            id obj = [clazz ac_objectWithDictionary:item];
            return obj;
        } else {
            return item;
        }
    }
    
    return nil;
}

- (void)saveCache:(id)obj
              key:(NSString *)key {
    if (key == nil || obj == nil) {
        return;
    }
    
//    RLMResults<RealmCacheModel *> *results = [RealmCacheModel objectsWhere:@"key == %@", key];
//    RealmCacheModel *model = results.firstObject;
//    
//    [[RLMRealm defaultRealm] transactionWithBlock:^{
//        
//        if (model) {
//            model.data = [self dataWithObj:obj];
//        } else {
//            RealmCacheModel *model = [[RealmCacheModel alloc] init];
//            model.key = key;
//            model.data = [self dataWithObj:obj];
//            [[RLMRealm defaultRealm] addObject:model];
//        }
//    }];
    
    // 优化: 列表缓存长度，只缓存10个。过长的缓存会导致realm文件过大
    if ([obj isKindOfClass:[NSMutableArray class]]) {
        NSMutableArray *items = (NSMutableArray *)obj;
        if (items.count > 10) {
            [items subarrayWithRange:NSMakeRange(0, 10)];
        }
        obj = items;
    }
    
    // 获取默认的 Realm 实例
    RLMRealm *realm = [RLMRealm defaultRealm]; // 每个线程只需要使用一次即可
    // 通过事务将数据添加到 Realm 中
    
    [realm transactionWithBlock:^{
        [RealmCacheModel createOrUpdateInDefaultRealmWithValue:@{@"key": key, @"data": [self dataWithObj:obj]}];
    }];
}

- (void)clearCacheWithKey:(NSString *)key {
    if (key == nil) {
        return;
    }
    
    RLMResults<RealmCacheModel *> *results = [RealmCacheModel objectsWhere:@"key == %@", key];
    RealmCacheModel *model = results.firstObject;
    
    if (model) {
        [[RLMRealm defaultRealm] transactionWithBlock:^{
            [[RLMRealm defaultRealm] deleteObject:model];
        }];
    }
}

#pragma mark -

- (NSData *)dataWithObj:(id)obj {
    return [obj toData];
}

@end
