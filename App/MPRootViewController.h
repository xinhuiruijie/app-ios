//
//  MPRootViewController.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/10.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

#define kBarHeight ([SDiOSVersion deviceSize] == Screen5Dot8inch ? 83 : 49)
#define kNavBarHeight ([SDiOSVersion deviceSize] == Screen5Dot8inch ? 88 : 64)

extern NSString * const UITabBarItemDoubleClickNotification;

@interface MPRootViewController : UITabBarController

@singleton(MPRootViewController)

/**
 *  当前展示的 ViewController
 */
@property (nonatomic, readonly) UIViewController *currentViewController;

@end
