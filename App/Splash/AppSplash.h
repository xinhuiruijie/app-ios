//
//  AppSplash.h
//  GOME
//  闪屏
//  Created by liuyadi on 16/10/11.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AppService.h"

@interface AppSplash : NSObject

@singleton( AppSplash )

- (void)setup;

- (void)updateSplash;

@end
