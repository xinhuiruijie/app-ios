//
//  UIImage+TPGif.h
//  Template
//
//  Created by GeekZooStudio on 2018/1/3.
//  Copyright © 2018年 yueli. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (TPGif)

+ (UIImage *)zl_animatedGIFNamed:(NSString *)name;

+ (UIImage *)zl_animatedGIFWithData:(NSData *)data;

@end
