//
//  AppSplash.m
//  GOME
//  闪屏
//  Created by liuyadi on 16/10/11.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import "AppSplash.h"
#import "SplashModel.h"
#import "SplashWindow.h"

@interface AppSplash ()

@property (nonatomic, strong) SPLASH * currentSplash;

@end

@implementation AppSplash

@def_singleton( AppSplash )

#pragma mark -

+ (void)load
{
    [AppService addService:[self sharedInstance]];
}

#pragma mark -

- (void)setup
{
    [self setupSplash];
    [self updateSplash];
}

#pragma mark -

// 加载splash
- (void)setupSplash
{
    //  存在加载项并且不是第一次加载
    if ( [[SplashModel sharedInstance] shouldShownSplash] )
    {
        SPLASH * splash = [[SplashModel sharedInstance] splash];
        
        if ( ![self.currentSplash.id isEqualToString:splash.id] )
        {
            // 不相同  那么显示
            self.currentSplash = [[SplashModel sharedInstance] splash];
            [[SplashWindow sharedInstance] show];
        }
    }
}

// 更新splash
- (void)updateSplash
{
    [[SplashModel sharedInstance] update];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    //进入前台 ---applicationDidBecomeActive----
    // 每次从后台进入前台  都去请求一遍闪屏幕 同时检查是否存在最新的闪屏  如果有  那么就显示最新的闪屏幕
    
    // 只有在一级页面的时候，才会去显示闪屏   并且不是第一个显示
    if ( [MPRootViewController sharedInstance].currentViewController.navigationController.viewControllers.count == 1 && [STISystemInfo launchedTimes] > 1 )
    {
        [self updateSplash];
//        [self setupSplash];   // 注释掉从后台进入前台更新闪屏的操作。1. 与开场动画冲突  2. 从后台进入前台应该不需要展示，除非是杀死App再进入
    }
}

@end
