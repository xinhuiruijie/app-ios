
#import "splash-api.h"

#pragma mark - Classes

@implementation SPLASH
@end
 

#pragma mark - API

#pragma mark - POST /v1/api.splash.preview 

@implementation V1_API_SPLASH_PREVIEW_REQUEST
@end

@implementation V1_API_SPLASH_PREVIEW_RESPONSE
@end

@implementation V1_API_SPLASH_PREVIEW_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_SPLASH_PREVIEW_REQUEST requestWithEndpoint:@"/v1/api.splash.preview" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_SPLASH_PREVIEW_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.splash.list 

@implementation V1_API_SPLASH_LIST_REQUEST
@end

@implementation V1_API_SPLASH_LIST_RESPONSE
@end

@implementation V1_API_SPLASH_LIST_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_SPLASH_LIST_REQUEST requestWithEndpoint:@"/v1/api.splash.list" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_SPLASH_LIST_RESPONSE class];
    }
    return self;
}

@end

