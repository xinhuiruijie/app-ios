
#import "STIHTTPNetwork.h"

#pragma mark - Missing

@class PHOTO;

@protocol PHOTO;

#pragma mark - 枚举类型

#pragma mark - Declaration

@class SPLASH;

#pragma mark - Protocols

@protocol SPLASH <NSObject> @end

#pragma mark - Classes

@interface SPLASH : NSObject
@property (nonatomic, strong) NSString * id;
@property (nonatomic, strong) NSString * splash_hash; // 唯一哈希值，通过id+url+update_time
@property (nonatomic, strong) NSString * begin_at; // 开始时间
@property (nonatomic, strong) NSString * finish_at; // 结束时间
@property (nonatomic, strong) PHOTO * photo; // 广告图
@property (nonatomic, strong) NSString * link; // deeplink
@property (nonatomic, strong) NSNumber * duration; // 广告展示时间
@end
 
#pragma mark - API

#pragma mark - POST /v1/api.splash.preview 

@interface V1_API_SPLASH_PREVIEW_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *id;
@end

@interface V1_API_SPLASH_PREVIEW_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) SPLASH * splash; // 闪屏
@end

@interface V1_API_SPLASH_PREVIEW_API : STIHTTPApi
@property (nonatomic, strong) V1_API_SPLASH_PREVIEW_REQUEST * req;
@property (nonatomic, strong) V1_API_SPLASH_PREVIEW_RESPONSE * resp;
@end

#pragma mark - POST /v1/api.splash.list 

@interface V1_API_SPLASH_LIST_REQUEST : STIHTTPRequest
@end

@interface V1_API_SPLASH_LIST_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray<SPLASH> * splashes; // 闪屏列表
@end

@interface V1_API_SPLASH_LIST_API : STIHTTPApi
@property (nonatomic, strong) V1_API_SPLASH_LIST_REQUEST * req;
@property (nonatomic, strong) V1_API_SPLASH_LIST_RESPONSE * resp;
@end

