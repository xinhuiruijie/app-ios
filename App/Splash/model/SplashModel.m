//
//  SplashModel.m
//  GOME
//
//  Created by liuyadi on 16/10/11.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import "SplashModel.h"
#import "UIImage+TPGif.h"
#import "YLGIFImage.h"

@interface SPLASH (State)

- (BOOL)expired;
- (BOOL)happening;
- (BOOL)waiting;

@end

#pragma mark -

@implementation SPLASH (State)

- (BOOL)expired
{
    NSDate * endDate = [NSDate dateWithTimeIntervalSince1970:self.finish_at.integerValue];
    NSDate * nowDate = [[NSDate date] dateByAddingTimeInterval:[[NSTimeZone systemTimeZone] secondsFromGMTForDate:[NSDate date]]];
    
    if ( NSOrderedDescending == [nowDate compare:endDate] )
    {
        return YES;
    }
    
    return NO;
}

- (BOOL)happening
{
    NSDate *startDate = [NSDate dateWithTimeIntervalSince1970:self.begin_at.integerValue];
    NSDate *endDate = [NSDate dateWithTimeIntervalSince1970:self.finish_at.integerValue];
    
    NSDate * nowDate = [NSDate date];//[[NSDate date] dateByAddingTimeInterval:[[NSTimeZone systemTimeZone] secondsFromGMTForDate:[NSDate date]]];
    
    if ( NSOrderedDescending == [nowDate compare:startDate] && NSOrderedAscending == [nowDate compare:endDate] )
    {
        return YES;
    }
    
    return NO;
}

- (BOOL)waiting
{
    NSDate * startDate = [NSDate dateWithTimeIntervalSince1970:self.begin_at.integerValue];
    NSDate * nowDate = [[NSDate date] dateByAddingTimeInterval:[[NSTimeZone systemTimeZone] secondsFromGMTForDate:[NSDate date]]];
    
    if ( NSOrderedAscending == [nowDate compare:startDate] )
    {
        return YES;
    }
    
    return NO;
}

@end

#pragma mark -

@interface SplashModel ()

@property (nonatomic, strong) SPLASH * previewSplash;

@end

@implementation SplashModel

@def_singleton( SplashModel )

- (instancetype)init
{
    self = [super init];
    
    if ( self )
    {
        self.splashes = [[NSMutableArray alloc] init];
        
        [self loadCache];
    }
    
    return self;
}

#pragma mark -

- (void)loadCache
{
    NSMutableArray * data = [NSMutableArray arrayWithArray:[self loadCacheWithKey:self.cacheKey objectClass:[SPLASH class]]];

    if ( data )
    {
        self.splashes = data;
    }
}

- (void)saveCache
{
    [self saveCache:self.splashes key:self.cacheKey];
}

- (void)clearCache
{
    [self clearCacheWithKey:self.cacheKey];
}

- (NSString *)cacheKey {
    return @"SplashModel-key";
}

#pragma mark -

- (UIImage *)splashImage
{
    if ( [self splash] )
    {
        SPLASH * splash = [self splash];
//        UIImage * image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:splash.photo.large];
        NSData *data = [[NSUserDefaults standardUserDefaults] objectForKey:splash.photo.large];
//        UIImage *image = [UIImage zl_animatedGIFWithData:data];
        UIImage *image = [YLGIFImage imageWithData:data];
        return image;
    }
    
    return nil;
}

- (NSString *)splashUrl
{
    if ( [self splash] )
    {
        SPLASH * splash = [self splash];
        return splash.link;
    }
    
    return nil;
}

- (NSNumber *)splashDuration
{
    if ( [self splash] )
    {
        SPLASH * splash = [self splash];
        return splash.duration;
    }
    
    return nil;
}

- (SPLASH *)splash
{
    // 如果当前是预览，那么就不更具时间，而是直接返回当前的预览splash
    if ( self.previewSplash )
    {
        return self.previewSplash;
    }
    
    NSMutableArray * orderedSplashes = [NSMutableArray array];
    NSMutableArray * timerSlots = [NSMutableArray array];
    
    for ( SPLASH * splash in _splashes )
    {
        if ( splash.happening )
        {
            [orderedSplashes addObject:splash];
            
            // 算出时间差 然后加入数组
            [timerSlots addObject:@(splash.begin_at.integerValue)];
        }
    }
    
    [orderedSplashes sortUsingComparator:^NSComparisonResult(SPLASH * obj1, SPLASH * obj2) {
        return [[NSDate dateWithTimeIntervalSince1970:obj1.begin_at.integerValue] compare:[NSDate dateWithTimeIntervalSince1970:obj2.begin_at.integerValue]];
    }];
    
    [timerSlots sortUsingComparator:^NSComparisonResult(NSNumber * obj1, NSNumber * obj2) {
        return [obj1 compare:obj2];
    }];
    
    // 得到距离当前时间最近的闪屏
    if ( timerSlots && timerSlots.count )
    {
        // 时间戳越大距离当前时间越近
        NSNumber * maxIndex = [timerSlots lastObject];
        
        for ( SPLASH * splash  in orderedSplashes )
        {
            if ( splash.begin_at.integerValue == maxIndex.integerValue )
            {
                return splash;
            }
        }
    }
    
    return nil;
}

#pragma mark -

- (void)update
{
    V1_API_SPLASH_LIST_API * api = [[V1_API_SPLASH_LIST_API alloc] init];
    
    api.whenUpdated = ^( V1_API_SPLASH_LIST_RESPONSE * response,NSDictionary * allHeaders, STIHTTPResponseError * error )
    {
        if ( error )
        {
            self.loaded = YES;
            PERFORM_BLOCK_SAFELY(self.whenUpdated, error);
        }
        else
        {
            if ( X_MPlanet_ErrorCode(allHeaders) == 0 )
            {
                self.loaded = YES;
                
                [self.splashes removeAllObjects];
                [self.splashes addObjectsFromArray:response.splashes];
                
                [self saveCache];
                [self download];
                
                // 请求完数据后紧接着判断是否有最新的，如果有那么就显示最新的
                PERFORM_BLOCK_SAFELY(self.whenUpdated, nil);
            }
            else
            {
                self.loaded = NO;
                
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc]init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY( self.whenUpdated, errors );
            }
        }
    };
    
    [api send];
}

#pragma mark - 

// 依据图片缓存key，检测是否需要下载splash图片
- (BOOL)compareSplashState:(SPLASH *)splash
{
    if ( /*[[SDImageCache sharedImageCache] imageFromDiskCacheForKey:splash.photo.large]*/
        [[NSUserDefaults standardUserDefaults] objectForKey:splash.photo.large])
    {
        return YES;
    }
    
    return NO;
}

#pragma mark - 

- (BOOL)shouldShownSplash
{
    if ( [self splashImage] )
    {
        return YES;
    }
    
    return NO;
}

- (void)download
{
    for ( SPLASH * splash in _splashes )
    {
        BOOL hasDownloaded = [self compareSplashState:splash];
        if ( hasDownloaded )
            continue;
        
        [self downloadPhoto:splash.photo.large];
    }
}

- (void)downloadPhoto:(NSString *)url
{
    if ( url && url.length )
    {
        [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:url] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
            
        } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
            if ( image )
            {
                self.splashImage = image;
//                [[SDImageCache sharedImageCache] storeImage:image forKey:url];
                [[NSUserDefaults standardUserDefaults] setObject:data forKey:url];
            }
        }];
    }
}

@end
