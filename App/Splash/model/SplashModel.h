//
//  SplashModel.h
//  GOME
//
//  Created by liuyadi on 16/10/11.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import "STIOnceModel.h"
#import "splash-api.h"

@interface SplashModel : STIOnceModel

@singleton( SplashModel )

@property (nonatomic, strong) NSMutableArray * splashes;
@property (nonatomic, strong) UIImage * splashImage;

- (void)update;

- (BOOL)shouldShownSplash;

- (UIImage *)splashImage;
- (NSString *)splashUrl;
- (NSNumber *)splashDuration;

- (SPLASH *)splash;

@end
