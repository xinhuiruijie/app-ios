//
//  SplashWindow.h
//  GOME
//
//  Created by liuyadi on 16/10/11.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SplashWindow : UIWindow

@singleton( SplashWindow )

- (void)show;
- (void)hide;

@end
