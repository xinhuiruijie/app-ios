//
//  SplashWindow.m
//  GOME
//
//  Created by liuyadi on 16/10/11.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import "SplashWindow.h"
#import "SplashModel.h"
//#import "YLGIFImage.h"
#import "YLImageView.h"

@interface SplashWindow ()
{
    NSTimer * _timer;
    NSUInteger _count;
}

@property (nonatomic, strong) YLImageView *splash;
@property (nonatomic, assign) BOOL hiding;
@property (nonatomic, assign) BOOL shouldAutoHide;
@property (nonatomic, strong) UIButton *bottomButton;
@property (nonatomic, strong) UIButton *countdownButton;

@property (nonatomic, strong) NSTimer *timer;

@end

#pragma mark -

@implementation SplashWindow

@def_singleton( SplashWindow )

#pragma mark -

- (instancetype)init
{
    self = [super init];
    
    if ( self )
    {
        self.frame = [UIScreen mainScreen].bounds;
        self.backgroundColor = [UIColor blackColor];
        self.windowLevel = UIWindowLevelAlert;
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if (self)
    {
        [self initialize];
    }
    
    return self;
}

- (void)initialize
{
    CGRect screenFrame = [UIScreen mainScreen].bounds;
    
    self.splash = [[YLImageView alloc] initWithFrame:CGRectMake( 0, 0, screenFrame.size.width, screenFrame.size.height)];
    
    UITapGestureRecognizer * tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapMask)];
    self.splash.backgroundColor = [UIColor blackColor];
    [self.splash addGestureRecognizer:tapGesture];
    self.splash.userInteractionEnabled = YES;
    self.splash.contentMode = UIViewContentModeScaleAspectFill;
    self.splash.layer.masksToBounds = YES;
    [self addSubview:self.splash];
    
    // 自动隐藏splashWindow，默认值为YES
    self.shouldAutoHide = YES;
    
    // UIWindow必须要有一个rootViewController才能显示，否则会报错
    self.rootViewController = [[UIViewController alloc] init];
    // UIWindow上的所有子试图都会被rootViewController覆盖，所有设置alpha为0(view的alpha小于0.01就会被隐藏)
    self.rootViewController.view.alpha = 0.0;
    
    [self addMaskView];
}

- (void)startTimer
{
    if ( nil == self.timer )
    {
        _count = 0;
        
        [self.timer invalidate];
        self.timer = nil;
        
        if ( [NSThread isMainThread] )
        {
            self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                          target:self
                                                        selector:@selector(countDown)
                                                        userInfo:nil
                                                         repeats:YES];
        }
        else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                // 回到主线程
                self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0f
                                                              target:self
                                                            selector:@selector(countDown)
                                                            userInfo:nil
                                                             repeats:YES];
            });
        }
    }
}

#pragma mark -

- (void)addMaskView
{
    if ( self.bottomButton == nil )
    {
        CGRect screenFrame = [UIScreen mainScreen].bounds;
        
        self.bottomButton = [[UIButton alloc] initWithFrame:CGRectMake(0, screenFrame.size.height - 60, screenFrame.size.width, 60.f )];
        [self.bottomButton addTarget:self action:@selector(hide) forControlEvents:UIControlEventTouchUpInside];
        self.bottomButton.backgroundColor = [UIColor clearColor];
        [self.bottomButton setTitle:@"进入应用" forState:UIControlStateNormal];
        [self.bottomButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.bottomButton.titleLabel setFont:[UIFont systemFontOfSize:12]];
        [self addSubview:self.bottomButton];
        
        NSInteger duration = [[SplashModel sharedInstance] splashDuration].integerValue;
        
        NSString * durationStr = [NSString stringWithFormat:@"%lds 跳过广告", (long)duration];
        
        self.countdownButton = [[UIButton alloc] initWithFrame:CGRectMake(screenFrame.size.width - 20 - 80, [UIApplication sharedApplication].statusBarFrame.size.height, 80, 20)];
        [self.countdownButton setTitle:durationStr forState:UIControlStateNormal];
        [self.countdownButton.titleLabel setFont:[UIFont systemFontOfSize:12]];
        [self.countdownButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [self.countdownButton.titleLabel setTextColor:[UIColor whiteColor]];
        [self.countdownButton addTarget:self action:@selector(hide) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.countdownButton];
        
        [self updateUI];
        
    }
}

#pragma mark -

- (void)updateUI
{
    NSInteger duration = [[SplashModel sharedInstance] splashDuration].integerValue;
    
    NSInteger count = duration - _count;
    
    if ( 1 >= count )
    {
        count = 1;
    }
    
    [self.countdownButton setTitle:[NSString stringWithFormat:@"%lds 跳过广告", (long)count] forState:UIControlStateNormal];
}

#pragma mark -

- (void)countDown
{
    _count += 1;
    
    [self updateUI];
    
    NSInteger duration = [[SplashModel sharedInstance] splashDuration].integerValue;
    
    if ( _count >= duration )
    {
        [self.timer invalidate];
        self.timer = nil;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self checkAutoHide];
        });
    }
}

- (void)show
{
    if ( !self.hidden )
    {
        return;
    }
    
    self.shouldAutoHide = YES;
    
    [self updateUI];
    [self startTimer];
    
    self.hidden = NO;
    
    [self loadSplashImage];
    
//        [self didShown];
}

- (void)didShown
{
    [self performSelector:@selector(checkAutoHide) withObject:nil afterDelay:3.0f];
}

- (void)hide
{
    [self hideAnimated:YES completion:^{
        [[NSNotificationCenter defaultCenter] postNotificationName:APP_SPLASH_HIDE object:nil];
    }];
}

- (void)hideWithCompletion:(void(^)(void))completion
{
    [self hideAnimated:YES completion:completion];
}

- (void)hideAnimated:(BOOL)animated completion:(void(^)(void))completion
{
    if ( self.hidden ) {
        return;
    }
    
    if ( self.hiding ) {
        return;
    }
    
    self.hiding = YES;
    
    if ( animated )
    {
        [self didHidden];
        [UIView animateWithDuration:0.35f
                              delay:0
                            options:UIViewAnimationOptionCurveLinear
                         animations:^{
                         }
                         completion:^(BOOL finished){
                             [self didHidden];
                             if ( completion ) {
                                 completion();
                             }
                         }];
        
    }
    else
    {
        [self didHidden];
    }
}

- (void)didHidden
{
    self.hiding = NO;
    self.hidden = YES;
    
    [self.timer invalidate];
    self.timer = nil;
    _count = 0;
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
}

#pragma mark -

- (void)checkAutoHide
{
    if ( self.shouldAutoHide )
    {
        [self hide];
    }
}

#pragma mark -

- (void)loadSplashImage
{
    UIImage * image = [[SplashModel sharedInstance] splashImage];
    
    if ( image )
    {
        self.splash.image = image;
    }
}

#pragma mark -

- (void)tapMask {
    NSString *urlString = [[SplashModel sharedInstance] splashUrl];
    
    if (urlString && urlString.length) {
        [self hideAnimated:YES completion:^{
            if (![DeepLink handleURLString:urlString withCompletion:nil]) {
                [[UIApplication sharedApplication].keyWindow presentFailureTips:@"无法打开的链接"];
            }
        }];
    }
}

@end
