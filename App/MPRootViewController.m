//
//  MPRootViewController.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/10.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "MPRootViewController.h"
#import "MPViewController.h"
#import "MPNavigationController.h"
#import "HomeViewController.h"
#import "PlanetViewController.h"
#import "LaboratoryController.h"
#import "SearchTabController.h"
#import "ProfileViewController.h"
#import "AppPusher.h"
#import "HomePageViewController.h"
#import <StoreKit/StoreKit.h>

NSString * const UITabBarItemDoubleClickNotification = @"UITabBarItemDoubleClickNotification";

@interface MPRootViewController () <AuthorizationDelegate, UITabBarControllerDelegate>
@property (nonatomic, assign) NSTimeInterval lastTapTabBarTime;
@end

@implementation MPRootViewController

@def_singleton(MPRootViewController)

- (instancetype)init {
    self = [super init];
    if (self) {
        self.delegate = self;
        [self setupViewControllers];
        [Authorization sharedInstance].delegate = self;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if ([STISystemInfo launchedTimes] == 10) {
        if (@available(iOS 10.3, *)) {
            if ([SKStoreReviewController respondsToSelector:@selector(requestReview)]) {
                [[UIApplication sharedApplication].keyWindow endEditing:YES];
                [SKStoreReviewController requestReview];
            }
        }
    }
}

- (void)viewWillLayoutSubviews {
    [super viewWillLayoutSubviews];
    
    CGRect tabFrame = self.tabBar.frame; //self.TabBar is IBOutlet of your TabBar
    tabFrame.size.height = kBarHeight;
    tabFrame.origin.y = self.view.frame.size.height - kBarHeight;
    self.tabBar.frame = tabFrame;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark -

- (void)setupViewControllers {
//    HomeViewController *home = [HomeViewController spawn];
    HomePageViewController *home = [HomePageViewController spawn];
    MPNavigationController *homeNav = [[MPNavigationController alloc] initWithRootViewController:home];
    homeNav.tabBarItem = [AppTheme itemWithImage:[UIImage imageNamed:@"icon_tab_star_nor"] selectImage:[UIImage imageNamed:@"icon_tab_star_sel"] title:@"STAR"];
    
    PlanetViewController *planet = [PlanetViewController spawn];
    MPNavigationController *planetNav = [[MPNavigationController alloc] initWithRootViewController:planet];
    planetNav.tabBarItem = [AppTheme itemWithImage:[UIImage imageNamed:@"icon_tab_starball_nor"] selectImage:[UIImage imageNamed:@"icon_tab_starball_sel"] title:@"星球"];
    
    LaboratoryController *laboratory = [LaboratoryController spawn];
    MPNavigationController *laboratoryNav = [[MPNavigationController alloc] initWithRootViewController:laboratory];
    laboratoryNav.tabBarItem = [AppTheme itemWithImage:[UIImage imageNamed:@"icon_tab_lab_nor"] selectImage:[UIImage imageNamed:@"icon_tab_lab_sel"] title:@"实验室"];
    
    SearchTabController *searchTab = [SearchTabController spawn];
    MPNavigationController *searchTabNav = [[MPNavigationController alloc] initWithRootViewController:searchTab];
    searchTabNav.tabBarItem = [AppTheme itemWithImage:[UIImage imageNamed:@"icon_tab_search_nor"] selectImage:[UIImage imageNamed:@"icon_tab_search_sel"] title:@"探索"];
    
    ProfileViewController *profile = [ProfileViewController spawn];
    MPNavigationController *profileNav = [[MPNavigationController alloc] initWithRootViewController:profile];
    profileNav.tabBarItem = [AppTheme itemWithImage:[UIImage imageNamed:@"icon_tab_profile_nor"] selectImage:[UIImage imageNamed:@"icon_tab_profile_sel"] title:@"我的"];
    
    self.viewControllers = @[homeNav, planetNav, laboratoryNav, profileNav];
}

#pragma mark -

- (UIViewController *)currentViewController {
    return ((UINavigationController *)self.selectedViewController).topViewController;
}

#pragma mark - AuthorizationDelegate

- (void)authorizationWasFailed {
    [AppPusher unBindToken];
    [AppPusher setBadge:0];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

- (void)authorizationWasSucceed {
    [AppPusher bindToken];
    [[Authorization sharedInstance] hideAuth];
}

- (void)authorizationWasInvalid:(NSString *)errorDesc {
    [AppPusher unBindToken];
    [AppPusher setBadge:0];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    if ([[self topViewController] isKindOfClass:[ProfileViewController class]]) {
        [(MPNavigationController *)self.selectedViewController popToRootViewControllerAnimated:NO];
        self.selectedIndex = 0;
    }
    [[Authorization sharedInstance] showAuthWithCompletion:^{
        if (errorDesc && errorDesc.length) {
            [[UIApplication sharedApplication].keyWindow presentMessage:errorDesc withTips:@"数据获取失败"];
        }
    }];
}

- (void)authorizationWasCancelled {
    [AppPusher unBindToken];
    [AppPusher setBadge:0];
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
    [(MPNavigationController *)self.selectedViewController popToRootViewControllerAnimated:NO];
    self.selectedIndex = 0;
}

#pragma mark - UITabBarControllerDelegate

- (void)tabBarController:(UITabBarController *)tabBarController didSelectViewController:(UIViewController *)viewController {
    [[ConfigModel sharedInstance] refresh];
    
    if (self.selectedIndex == 0) {
        [AppAnalytics clickEvent:@"star"];
    } else if (self.selectedIndex == 1) {
        [AppAnalytics clickEvent:@"planet"];
    } else if (self.selectedIndex == 2) {
        [AppAnalytics clickEvent:@"lab"];
    } else if (self.selectedIndex == 4) {
        [AppAnalytics clickEvent:@"profile"];
    }
}

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController {
    if ([viewController isKindOfClass:[MPNavigationController class]]) {
        if ([((MPNavigationController *)viewController).topViewController isKindOfClass:[ProfileViewController class]]) {
            if (![UserModel online]) {
                [[Authorization sharedInstance] showAuth];
                return NO;
            }
        }
    }
    
    
    NSInteger shouldSelectIndex = [self.viewControllers indexOfObject:viewController];
    if (shouldSelectIndex == self.selectedIndex) {
        // tabbarItem点击动效
        NSMutableArray *tabBarButtons = [NSMutableArray array];
        for (UIView *tabBarButton in self.tabBar.subviews) {
            if ([tabBarButton isKindOfClass:NSClassFromString(@"UITabBarButton")]) {
                if (shouldSelectIndex == tabBarButtons.count) {
                    // 当前选中的tabbaritem
                    for (UIView *imageView in tabBarButton.subviews) {
                        if ([imageView isKindOfClass:[UIImageView class]]) {
                            imageView.alpha = 0.0;
                            [UIView animateWithDuration:0.1 animations:^{
                                imageView.alpha = 1.0;
                            }];
                            break;
                        }
                    }
                    break;
                }
                [tabBarButtons addObject:tabBarButton];
            }
        }
        
        // 两次点击间隔小于0.25秒 视为双击, 双击通知当前页面刷新
        if ([[NSDate date] timeIntervalSince1970] - self.lastTapTabBarTime < 0.25) {
            [[NSNotificationCenter defaultCenter] postNotificationName:UITabBarItemDoubleClickNotification object:self];
        }
        self.lastTapTabBarTime = [[NSDate date] timeIntervalSince1970];
    }
    
    return YES;
}

#pragma mark - rotation

- (BOOL)shouldAutorotate
{
    return self.selectedViewController.shouldAutorotate;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return self.selectedViewController.supportedInterfaceOrientations;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return self.selectedViewController.preferredInterfaceOrientationForPresentation;
}

@end
