//
//  BaseView.m
//  GOME
//  论坛首页滑动父类
//  Created by GeekZooStudio on 16/4/28.
//  Copyright © 2016年 GeekZooStudio. All rights reserved.
//

#import "BaseListView.h"

#define selfWidth                        self.frame.size.width
#define selfHeight                      self.frame.size.height

@interface BaseListView ()

@property (nonatomic, weak) UIView *indexLine;
@property (nonatomic, weak) UIView *listMargeLine;

@property (nonatomic, assign) NSInteger lastItmeCount;
@end

@implementation BaseListView

@synthesize dataSource = _dataSource;
@synthesize delegate = _delegate;

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor whiteColor];
        
        [self setupContentView];
        [self addObserver];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)aDecoder{
	if (self = [super initWithCoder:aDecoder]) {
		self.backgroundColor = [UIColor whiteColor];
		
		[self setupContentView];
		[self addObserver];
	}
	return self;
}

- (void)dealloc {
    [self removeObserver];
}

- (void)layoutSubviews {
    [super layoutSubviews];
	
    CGFloat topListOriginX = [self topListOrigin].x;
    CGFloat topListOriginY = [self topListOrigin].y;
	CGFloat topListWidth = [self topListSize].width;
    CGFloat topListHeight = [self topListSize].height;
    CGFloat indexLineWidth = [self topListIndexLineWidth];
    CGFloat indexLineHeight = 2.0;
    CGFloat listMargeLineHeight = 0.5;
    
    _topList.frame = CGRectMake(topListOriginX, topListOriginY, topListWidth, topListHeight);
    _bottomList.frame = [self bottomListFrame];
    _listMargeLine.frame = CGRectMake(0, topListHeight - listMargeLineHeight, selfWidth, listMargeLineHeight);
	
	_indexLine.frame = CGRectMake([self indexLineOriginalX], topListHeight - indexLineHeight, indexLineWidth, indexLineHeight);
}

- (void)setupContentView {
    /**
     * topList
     */
    UICollectionViewFlowLayout *topViewLayout = [[UICollectionViewFlowLayout alloc] init];
    topViewLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    UICollectionView *topList = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:topViewLayout];
    topList.backgroundColor = [UIColor whiteColor];
    topList.showsHorizontalScrollIndicator = NO;
    topList.dataSource = self;
    topList.delegate = self;
    [self addSubview:topList];
    _topList = topList;

    /**
     * bottomList
     */
    UICollectionViewFlowLayout *bottomViewLayout = [[UICollectionViewFlowLayout alloc] init];
    bottomViewLayout.scrollDirection = UICollectionViewScrollDirectionHorizontal;
    
    UICollectionView *bottomList = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:bottomViewLayout];
    bottomList.backgroundColor = [UIColor whiteColor];
    bottomList.dataSource = self;
    bottomList.delegate = self;
    bottomList.pagingEnabled = YES;
    [self addSubview:bottomList];
    _bottomList = bottomList;
    
    /**
     * listMargeLine
     */
    UIView *listMargeLine = [[UIView alloc] init];
    listMargeLine.backgroundColor = [self listMargeLineColor];
    [self addSubview:listMargeLine];
    _listMargeLine = listMargeLine;
    listMargeLine.hidden = ![self showListMargeLine];
    
    /**
     * indexLine
     */
    UIView *indexLine = [[UIView alloc] init];
    indexLine.backgroundColor = [self indexLineColor];
    [self.topList addSubview:indexLine];
    _indexLine = indexLine;
    indexLine.hidden = ![self showIndexLine];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
	NSInteger itmeCount = [self.dataSource itemNumber];
	if (self.lastItmeCount != itmeCount) {
		self.lastItmeCount = itmeCount;
		[self setNeedsLayout];
	}
	return itmeCount ? 1 : 0;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return [self.dataSource itemNumber];
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    return [self.dataSource collectionView:collectionView cellForItemAtIndexPath:indexPath];
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if ( collectionView == self.topList ) {
        if ([self.delegate respondsToSelector:@selector(collectionView:didSelectTopListItemAtIndexPath:)]) {
            [self.delegate collectionView:collectionView didSelectTopListItemAtIndexPath:indexPath];
        }
        
        [self changeCurrentIndex:indexPath.item lastIndex:indexPath.item == 0 ? 0 : 1];
        
        [self.topList reloadData];
        [self scrollListToCenter:indexPath.item];
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if ( collectionView == self.topList ) {
        if ([self.delegate respondsToSelector:@selector(topList:sizeForItemAtIndexPath:)]) {
            return [self.delegate topList:self.topList sizeForItemAtIndexPath:indexPath];
        }
        return CGSizeMake([self topListCellWidth], collectionView.bounds.size.height);
    } else if ( collectionView == self.bottomList ) {
        if ([self.delegate respondsToSelector:@selector(collectionView:layout:sizeForItemAtIndexPath:)]) {
            return [self.delegate collectionView:self.bottomList layout:collectionViewLayout sizeForItemAtIndexPath:indexPath];
        }
        return CGSizeMake( collectionView.bounds.size.width, collectionView.bounds.size.height);
    }
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    return CGSizeZero;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (collectionView == self.bottomList) {
        if ([self.delegate respondsToSelector:@selector(collectionView:layout:insetForSectionAtIndex:)]) {
            return [self.delegate collectionView:collectionView layout:collectionViewLayout insetForSectionAtIndex:section];
        }
    }
    return UIEdgeInsetsZero;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    if (collectionView == self.bottomList) {
        if ([self.delegate respondsToSelector:@selector(collectionView:layout:minimumInteritemSpacingForSectionAtIndex:)]) {
            return [self.delegate collectionView:collectionView layout:collectionViewLayout minimumInteritemSpacingForSectionAtIndex:section];
        }
    }
    return 0.0;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    if (collectionView == self.bottomList) {
        if ([self.delegate respondsToSelector:@selector(collectionView:layout:minimumLineSpacingForSectionAtIndex:)]) {
            return [self.delegate collectionView:collectionView layout:collectionViewLayout minimumLineSpacingForSectionAtIndex:section];
        }
    }
    return 0.0;
}

#pragma mark - 

- (void)scrollListToCenter:(NSUInteger)index {
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:index inSection:0];
    
    [self.topList scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
    
    [self.bottomList scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:NO];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (scrollView == self.bottomList) {
        NSInteger index = scrollView.contentOffset.x / selfWidth;
        [self.topList scrollToItemAtIndexPath:[NSIndexPath indexPathForItem:index inSection:0] atScrollPosition:UICollectionViewScrollPositionCenteredHorizontally animated:YES];
        
		if ([self.delegate respondsToSelector:@selector(scrollListDidEndDecelerating:currentIndex:)]) {
			[self.delegate scrollListDidEndDecelerating:self.bottomList currentIndex:index];
		}
    }
}

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSString *,id> *)change context:(void *)context {
    if ( [keyPath isEqualToString:@"contentOffset"] ) {
        CGFloat offsetX = [change[@"new"] CGPointValue].x;
        CGFloat scale = offsetX / self.bottomList.contentSize.width;
        CGFloat x = scale * self.topList.contentSize.width;
        
        // 移动indexLine
        if ( !isnan(x) && self.indexLine ) {
            CGRect rect = self.indexLine.frame;
            rect.origin.x = x + [self indexLineOriginalX];
            self.indexLine.frame = rect;
        }
        
        // 计算移动位置
        CGFloat p = fmod(offsetX, self.bottomList.bounds.size.width);
        NSInteger index = offsetX / self.bottomList.bounds.size.width;
        
        if ( offsetX > [change[@"old"] CGPointValue].x ) { // 右滑
            if ( p > self.bottomList.bounds.size.width * 0.5 ) {
                NSInteger maxIndex = [self.dataSource itemNumber] - 1;
                if ( index >= maxIndex ) {
                    [self changeCurrentIndex:maxIndex lastIndex:maxIndex - 1];
                } else {
                    [self changeCurrentIndex:index + 1 lastIndex:index];
                }
            }
        } else if ( offsetX < [change[@"old"] CGPointValue].x ) { // 左滑
            if ( p < self.bottomList.bounds.size.width * 0.5 ) {
                [self changeCurrentIndex:index lastIndex:index + 1];
            }
        }
    }
}

- (void)changeCurrentIndex:(NSInteger)currentIndex lastIndex:(NSInteger)lastIndex {
    if ([self.delegate respondsToSelector:@selector(collectionView:currentIndex:lastIndex:)]) {
        [self.delegate collectionView:self.topList currentIndex:currentIndex lastIndex:lastIndex];
    }
}

#pragma mark - observer

- (void)addObserver {
    [self.bottomList addObserver:self forKeyPath:@"contentOffset" options:NSKeyValueObservingOptionNew | NSKeyValueObservingOptionOld context:nil];
}

- (void)removeObserver {
    [self.bottomList removeObserver:self forKeyPath:@"contentOffset"];
}

- (void)reloadData {
    [self.topList reloadData];
    [self.bottomList reloadData];
}

#pragma mark - 

- (BOOL)showIndexLine {
    return YES;
}

- (BOOL)showListMargeLine {
    return YES;
}

- (UIColor *)indexLineColor {
    return [UIColor redColor];
}

- (UIColor *)listMargeLineColor {
	return [UIColor grayColor];
}

- (CGPoint)topListOrigin {
    return CGPointZero;
}

- (CGSize)topListSize {
    return CGSizeMake([UIScreen mainScreen].bounds.size.width, 39);
}

- (CGRect)bottomListFrame {
    CGFloat topListHeight = [self topListSize].height;
    return CGRectMake(0, topListHeight, selfWidth, selfHeight - topListHeight);
}

- (CGFloat)topListCellWidth {
    NSInteger itemCount = [self.dataSource itemNumber];
    
    CGFloat width = itemCount >= 4 ? (selfWidth / 4) : (selfWidth / itemCount);
    
    return isnan(width) || isinf(width) ? 0 : width;
}

- (CGFloat)indexLineOriginalX {
    CGFloat x = ([self topListCellWidth] - [self topListIndexLineWidth]) / 2;
    
    return isnan(x) || isinf(x) ? 0 : x;
}

- (CGFloat)topListIndexLineWidth {
    return [self topListCellWidth];
}

@end
