//
//  BaseListViewTopCell.m
//  ParkviewGreen
//
//  Created by GeekZooStudio on 16/12/13.
//  Copyright © 2016年 GeekZooStudio. All rights reserved.
//

#import "BaseListViewTopCell.h"
#import "EnumListModel.h"

@interface BaseListViewTopCell ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@end

@implementation BaseListViewTopCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self customize];
}

- (void)customize {
    self.titleLabel.textColor = [AppTheme normalTextColor];
}

- (void)setSelected:(BOOL)selected {
    [super setSelected:selected];
    if (selected) {
        self.titleLabel.textColor = [AppTheme normalTextColor];
    } else {
        self.titleLabel.textColor = [AppTheme subTextColor];
    }
}

- (void)dataDidChange {
    if (!self.data) {
        return;
    }
    
    if ([self.data isKindOfClass:[EnumListModel class]]) {
        EnumListModel *data = self.data;
        self.titleLabel.text = data.displayName;
    } else if ([self.data isKindOfClass:[NSString class]]) {
        NSString *title = self.data;
        self.titleLabel.text = title;
    }
}

@end
