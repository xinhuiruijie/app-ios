//
//  MPBaseWebViewController.m
//  MotherPlanet
//
//  Created by 陈熙 on 2018/1/9.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "MPBaseWebViewController.h"
#import "WebViewJavascriptBridge.h"

@interface MPBaseWebViewController () <UIWebViewDelegate>

@property (nonatomic, strong) UIButton *backButton;
@property (nonatomic, strong) UIButton *closeButton;
@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, strong) UIView *progressView; // 进度条
@property (nonatomic, strong) WebViewJavascriptBridge *bridge;

@end

@implementation MPBaseWebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self navigationItemInit];
    [self addSubview];
    [self reloadView];
    [self reloadBridge];
    
    [self addObservers];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    self.webView.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
}

- (void)dealloc {
    [self removeObservers];
}

- (void)addObservers {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(authorizationWasSucceed) name:AuthorizationWasSucceedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(authorizationWasFailed) name:AuthorizationWasFailedNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(authorizationWasInvalid:) name:AuthorizationWasInvalidNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(authorizationWasCancelled) name:AuthorizationWasCancelledNotification object:nil];
}

- (void)removeObservers {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark -

- (void)addSubview {
    self.webView = [[UIWebView alloc] init];
    self.webView.opaque = NO;
    self.webView.backgroundColor = [AppTheme backgroundColor];
    [self.view addSubview:self.webView];
}

- (void)navigationItemInit {
    self.navigationItem.leftBarButtonItem = [self doubleBarButtonItems];
}

- (UIBarButtonItem *)doubleBarButtonItems {
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake( 0, 0, 70.f, 44.f)];
    UIButton *firstButton = [UIButton buttonWithType:UIButtonTypeCustom];
    firstButton.frame = CGRectMake( -15, 0, 40.f, 44.f );
    [firstButton setImage:[UIImage imageNamed:@"icon_back"] forState:UIControlStateNormal];
    [firstButton addTarget:self action:@selector(leftAction) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:firstButton];
    self.backButton = firstButton;
    
    UIButton *secondButton = [[UIButton alloc] init];
    secondButton.frame = CGRectMake( 25.f, 0.f, 40.f, 44.f );
    [secondButton setImage:[UIImage imageNamed:@"icon_close_comment"] forState:UIControlStateNormal];
    [secondButton addTarget:self action:@selector(rightAction) forControlEvents:UIControlEventTouchUpInside];
    [view addSubview:secondButton];
    self.closeButton = secondButton;
    self.closeButton.hidden = YES;
    
    UIBarButtonItem *item = [[UIBarButtonItem alloc] initWithCustomView:view];
    
    return item;
}

- (void)leftAction {
    if (self.webView.canGoBack) {
        self.closeButton.hidden = NO;
        [self.webView goBack];
    } else {
        self.closeButton.hidden = YES;
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)rightAction {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - progress view

- (void)addProgressView {
    // 获取当前网络状态
    Reachability *reach = [Reachability reachabilityForInternetConnection];
    NetworkStatus internetStatus = [reach currentReachabilityStatus];
    if (internetStatus == !NotReachable) {
        if (!self.progressView) {
            self.progressView = [[UIView alloc] init];
        }
        self.progressView.frame = CGRectMake(0, 0, 0, 3);
        self.progressView.backgroundColor = [AppTheme normalColor];
        [self.view addSubview:self.progressView];
        [UIView animateWithDuration:3 animations:^{
            self.progressView.width = kScreenWidth - 100;
        }];
    }
}

- (void)removeProgressView {
    // 进度条读完进度
    [self.progressView.layer removeAllAnimations];
    [UIView animateWithDuration:0.25 animations:^{
        self.progressView.width = kScreenWidth;
    } completion:^(BOOL finished) {
        [self.progressView removeFromSuperview];
    }];
}

#pragma mark - reload review

- (void)requestWithURL:(NSURL *)url {
    [self.webView stopLoading];
    [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
}

- (void)reloadView {
    if (self.url) {
        [self requestWithURL:[NSURL URLWithString:self.url]];
    }
}

- (void)reload {
    [self.webView reload];
}

- (void)reloadOnLogin {
    NSString *urlStr = nil;
    NSString *currentURL = [self currentURLStr];
    if (currentURL && currentURL.length) {
        NSURL *url = [NSURL URLWithString:urlStr];
        [self requestWithURL:url];
    }
}

- (void)reloadOnLogout {
    NSString *urlStr = nil;
    NSString *currentURL = [self currentURLStr];
    if (currentURL && currentURL.length) {
        NSURL *url = [NSURL URLWithString:urlStr];
        [self requestWithURL:url];
    }
}

- (void)reloadTitle:(NSString *)title {
    self.navigationItem.title = title;
}

#pragma mark - authorization

- (void)authorizationWasSucceed {
    [self reloadOnLogin];
    
    [self onLogin:[UserModel sharedInstance].token];
}

- (void)authorizationWasFailed {
}

- (void)authorizationWasInvalid:(NSString *)errorDesc {
    [self reloadOnLogout];
    [self onLogout];
}

- (void)authorizationWasCancelled {
    [self reloadOnLogout];
    [self onLogout];
}

#pragma mark - JS Bridge

- (void)reloadBridge {
    self.bridge = [WebViewJavascriptBridge bridgeForWebView:self.webView webViewDelegate:self handler:nil];
    [self addRegisterHandlers];
}

- (void)addRegisterHandlers {
    // registerHandler
    [self.bridge registerHandler:@"doLogin" handler:^(id data, WVJBResponseCallback responseCallback) {
        [[Authorization sharedInstance] showAuth];
    }];
    
    [self.bridge registerHandler:@"doSignup" handler:^(id data, WVJBResponseCallback responseCallback) {
        [[Authorization sharedInstance] showAuth];
    }];
    
    [self.bridge registerHandler:@"goto" handler:^(id data, WVJBResponseCallback responseCallback) {
        [self gotoPage:data];
    }];
    
    [self.bridge registerHandler:@"setTitle" handler:^(id data, WVJBResponseCallback responseCallback) {
        NSString *title = [data objectForKey:@"title"];
        [self reloadTitle:title];
    }];
    
    [self.bridge registerHandler:@"showRecordButton" handler:^(id data, WVJBResponseCallback responseCallback) {
        [self showRecordButton];
    }];
    
    [self.bridge registerHandler:@"hideRecordButton" handler:^(id data, WVJBResponseCallback responseCallback) {
        [self hideRecordButton];
    }];
    
    [self.bridge registerHandler:@"onWechatPay" handler:^(id data, WVJBResponseCallback responseCallback) {
        [self wechatAction:data];
    }];
    
    [self.bridge registerHandler:@"onAliPay" handler:^(id data, WVJBResponseCallback responseCallback) {
        [self alipayAction:data];
    }];
}

#pragma mark - token

- (void)setTokenHandler:(NSString *)token {
    if (token) {
        [self.bridge callHandler:@"setToken" data:@{@"token": token}];
    }
}

- (void)onLogin:(NSString *)token {
    if (token) {
        [self.bridge callHandler:@"onLogin" data:@{@"token": token}];
    }
}

- (void)onLogout {
    [self.bridge callHandler:@"onLogout" data:nil];
}

#pragma mark -

- (void)setupToken {
    if ([UserModel online]) {
        [self setTokenHandler:[UserModel sharedInstance].token];
    } else {
        [self setTokenHandler:@""];
    }
}

#pragma mark -

- (NSString *)currentURLStr {
    return [self.webView stringByEvaluatingJavaScriptFromString:@"document.location.href"];
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView {
    [self addProgressView];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    [self removeProgressView];
    [self setupToken];
    
    [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"WebKitCacheModelPreferenceKey"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    [self removeProgressView];
}

#pragma mark - payment Action

- (void)alipayAction:(NSDictionary *)dict {
    
}

- (void)wechatAction:(NSDictionary *)dict {
    
}

#pragma mark - Payment Results

- (void)onPaySucceed {
    [self.bridge callHandler:@"onPaySucceed" data:nil responseCallback:^(id responseData) {
    }];
}

- (void)onPayFailed {
    [self.bridge callHandler:@"onPayFailed" data:nil responseCallback:^(id responseData) {
    }];
}

- (void)onPayCancelled {
    [self.bridge callHandler:@"onPayCancelled" data:nil responseCallback:^(id responseData) {
    }];
}

#pragma mark -

- (void)showRecordButton {
    self.navigationItem.rightBarButtonItem = [AppTheme normalItemWithContent:@"兑换记录" handler:^(id sender) {
        NSString *recordUrl = [NSString stringWithFormat:@"%@/#/for-record", [ConfigModel sharedInstance].integral_mall_url];
        if (![DeepLink handleURLString:recordUrl withCompletion:nil]) {
            [[UIApplication sharedApplication].keyWindow presentFailureTips:@"无法打开的链接"];
        }
    }];
}

- (void)hideRecordButton {
    self.navigationItem.rightBarButtonItem = nil;
}

- (void)gotoPage:(NSDictionary *)dict {
    NSString *url = [dict objectForKey:@"url"];
    if (url && url.length) {
        if (![DeepLink handleURLString:url withCompletion:nil]) {
            [[UIApplication sharedApplication].keyWindow presentFailureTips:@"无法打开的链接"];
        }
    }
}

@end
