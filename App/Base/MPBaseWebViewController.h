//
//  MPBaseWebViewController.h
//  MotherPlanet
//
//  Created by 陈熙 on 2018/1/9.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface MPBaseWebViewController : UIViewController

@property (nonatomic, strong) NSString *url;

@end
