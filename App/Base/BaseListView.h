//
//  BaseView.h
//  GOME
//  论坛首页滑动父类
//  Created by GeekZooStudio on 16/4/28.
//  Copyright © 2016年 GeekZooStudio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BaseListViewTopCell.h"

#pragma mark - protocol

@protocol BaseListViewDataSource <NSObject>
@required
- (NSInteger)itemNumber;
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath;

@end

@protocol BaseListViewDelegate <UIScrollViewDelegate>
@optional

- (void)collectionView:(UICollectionView *)topList didSelectTopListItemAtIndexPath:(NSIndexPath *)indexPath;

- (void)collectionView:(UICollectionView *)topList currentIndex:(NSInteger)currentIndex lastIndex:(NSInteger)lastIndex;

- (void)scrollListDidEndDecelerating:(UICollectionView *)bottomList currentIndex:(NSInteger)currentIndex;

- (CGSize)topList:(UICollectionView *)topList sizeForItemAtIndexPath:(NSIndexPath *)indexPath;

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath;

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section;

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section;

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section;
@end


#pragma mark - interface

@interface BaseListView : UIScrollView <UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate>

@property (nonatomic, weak, readonly) UICollectionView *topList;
@property (nonatomic, weak, readonly) UICollectionView *bottomList;

@property (nonatomic, weak) id<BaseListViewDataSource> dataSource;
@property (nonatomic, weak) id<BaseListViewDelegate> delegate;

/**
 *  滚动list到中心
 */
- (void)scrollListToCenter:(NSUInteger)index;

// 相关设置类方法, 可由子类自定义
/** 是否显示索引线, 默认显示 */
- (BOOL)showIndexLine;

/** 是否显示分割线, 默认显示 */
- (BOOL)showListMargeLine;

/** 索引线颜色, 默认红色 */
- (UIColor *)indexLineColor;

/** top底部线颜色, 默认grayColor */
- (UIColor *)listMargeLineColor;

/** topListOrigin, 默认为(0, 0) */
- (CGPoint)topListOrigin;

/** topListSize, 默认屏幕宽度,39高度 */
- (CGSize)topListSize;

/** bottomListFrame, 默认为跟在topList的后面 */
- (CGRect)bottomListFrame;

/** topListCell的宽度, 默认随item个数等分, 最大view宽度的1/4 */
- (CGFloat)topListCellWidth;

/** 索引线的宽度, 默认等于item宽度 */
- (CGFloat)topListIndexLineWidth;

/** 索引线初始X值, 默认随宽度变使其与item居中 */
- (CGFloat)indexLineOriginalX;

@end
