//
//  EnumListModel.h
//  HemeiUnion
//
//  Created by GeekZooStudio on 2017/9/5.
//  Copyright © 2017年 GeekZooStudio. All rights reserved.
//

#import <Foundation/Foundation.h>

/// 适用于需要用枚举值做列表数据源的model
@interface EnumListModel : NSObject

/// 显示的名字
@property (nonatomic, strong) NSString *displayName;
/// 枚举值
@property (nonatomic, assign) NSNumber *enumType;
@end
