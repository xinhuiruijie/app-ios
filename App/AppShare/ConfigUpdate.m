//
//  ConfigUpdate.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "ConfigUpdate.h"
#import "AppShare.h"

@implementation ConfigUpdate

@def_singleton(ConfigUpdate)

- (void)updateConfig {
    [AppConfig setupConfigService:^(id data) {
        if ([data isKindOfClass:[ConfigModel class]]) {
            [AppShare setupWithConfig:data];
        }
    }];
}

@end
