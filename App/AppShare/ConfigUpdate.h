//
//  ConfigUpdate.h
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ConfigUpdate : NSObject

@singleton(ConfigUpdate)

- (void)updateConfig;

@end
