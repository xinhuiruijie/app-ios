//
//  AppShare.m
//  quchicha
//
//  Created by liuyadi on 15/8/26.
//  Copyright (c) 2015年 Vicky. All rights reserved.
//

#import "AppShare.h"
#import "SinaWeibo.h"
#import "WXChatShared.h"
#import "TencentOpenShared.h"

@implementation AppShare

@def_singleton(AppShare)

NSString * const kWeixinAppId       = @"wx1d61c470a450d93a";
NSString * const kWeixinAppSecret   = @"b50b372f38cf052aed1024fd4527dfc4";
NSString * const kWeixinOpenURL     = @"";

NSString * const kSinaAppKey        = @"4082907722";
NSString * const kSinaAppSecret     = @"28b6962597573aa8ebc03d4f1d98a0d7";
NSString * const kSinaRedirectURL   = @"https://api.weibo.com/oauth2/default.html";

NSString * const kQQAppId           = @"1106549818";
NSString * const kQQAppSecret       = @"GYOkjXlE9YU4ENeO";
NSString * const kQQOpenURL         = @"";

// 配置分享
+ (void)setupWithConfig:(ConfigModel *)model
{
    OAUTH_CONFIG *config = model.item;
    // 微信分享
    if ([AppConfig isShowWechat]) {
        [WXChatShared sharedInstance].appId = config.wechat_app.app_id;
        [WXChatShared sharedInstance].appKey = config.wechat_app.app_secret;
        [[WXChatShared sharedInstance] powerOn];
    } else {
        [WXChatShared sharedInstance].appId = kWeixinAppId;
        [WXChatShared sharedInstance].appKey = kWeixinAppSecret;
        [[WXChatShared sharedInstance] powerOn];
    }
    
    // 新浪微博分享
    // 微博特性：不需要注册kSinaAppSecret，AppKey即appId
    if ([AppConfig isShowWeibo]) {
        [SinaWeibo sharedInstance].appId = config.weibo_app.app_id;
        [SinaWeibo sharedInstance].appKey = config.weibo_app.app_id;
        [SinaWeibo sharedInstance].redirectUrl = kSinaRedirectURL;
        [[SinaWeibo sharedInstance] powerOn];
    } else {
        [SinaWeibo sharedInstance].appId = kSinaAppKey;
        [SinaWeibo sharedInstance].appKey = kSinaAppKey;
        [SinaWeibo sharedInstance].redirectUrl = kSinaRedirectURL;
        [[SinaWeibo sharedInstance] powerOn];
    }
    
    // QQ分享
    if ([AppConfig isShowQQ]) {
        [TencentOpenShared sharedInstance].appId = config.qq_app.app_id;
        [TencentOpenShared sharedInstance].appKey = config.qq_app.app_secret;
        [[TencentOpenShared sharedInstance] powerOn];
    } else {
        [TencentOpenShared sharedInstance].appId = kQQAppId;
        [TencentOpenShared sharedInstance].appKey = kQQAppSecret;
        [[TencentOpenShared sharedInstance] powerOn];
    }
}

@end
