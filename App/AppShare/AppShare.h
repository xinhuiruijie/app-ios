//
//  AppShare.h
//  quchicha
//
//  Created by liuyadi on 15/8/26.
//  Copyright (c) 2015年 Vicky. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppShare : NSObject

@singleton(AppShare)

+ (void)setupWithConfig:(ConfigModel *)model;

@end
