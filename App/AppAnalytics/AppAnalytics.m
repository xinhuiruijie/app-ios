//
//  AppAnalytics.m
//  MotherPlanet
//
//  Created by liuyadi on 2018/1/30.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "AppAnalytics.h"
#import <UMCommon/UMCommon.h>
#import <UMAnalytics/MobClick.h>

@implementation AppAnalytics

+ (void)setup {
    [UMConfigure initWithAppkey:@"5a15178a8f4a9d0c9900017b" channel:@"App Store"];
    [MobClick setScenarioType:E_UM_NORMAL];
}

+ (void)clickEvent:(NSString *)event {
    [MobClick event:event];
}

+ (void)clickUserEvent:(NSString *)event {
    if ([UserModel sharedInstance].user && [UserModel sharedInstance].user.id) {
        [MobClick event:event attributes:@{@"userId":[UserModel sharedInstance].user.id}];
    }
}

@end
