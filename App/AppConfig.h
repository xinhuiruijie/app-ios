//
//  AppConfig.h
//  bbm2
//
//  Created by PURPLEPENG on 5/8/16.
//  Copyright © 2016 PURPLEPENG. All rights reserved.
//

#import <Foundation/Foundation.h>

// TODO: 提交AppStore需要改为YES
#define JPUSH_ApsForProduction YES

#define APP_DEVELOPMENT 0

#define StatusBarHeight [UIApplication sharedApplication].statusBarFrame.size.height
#define NavigationBarHeight (self.navigationController.navigationBar.frame.size.height == 0 ? 44 : self.navigationController.navigationBar.frame.size.height)
#define TopBarHeight (StatusBarHeight + NavigationBarHeight)

/**
 *  错误信息
 */
#define X_MPlanet_ErrorCode(allHeaders) [[allHeaders objectAtPath:@"X-MPlanet-ErrorCode"] integerValue]
#define X_MPlanet_ErrorDesc(allHeaders) (__bridge_transfer NSString *)CFURLCreateStringByReplacingPercentEscapes(NULL,\
(__bridge CFStringRef)[allHeaders objectAtPath:@"X-MPlanet-ErrorDesc"],\
CFSTR(""));

#define X_MPlanet_Authorization(allHeaders) [allHeaders objectAtPath:@"X-MPlanet-Authorization"]

FOUNDATION_EXPORT NSString * const APP_SPLASH_HIDE;
FOUNDATION_EXPORT NSString * const APP_LAUNCH_ANIMATION_HIDE;

FOUNDATION_EXPORT NSString * const APP_ENCRYPTION_KEY;
FOUNDATION_EXPORT NSString * const TRACE_ENCRYPTION_KEY;

FOUNDATION_EXPORT NSString * const kAppServerAPIURL;

/**
 *  Image Upload Base URL
 */
FOUNDATION_EXTERN NSString * const kAppUploadImageBaseURL;

/**
 *  Image Upload Api URL
 */
FOUNDATION_EXTERN NSString * const kAppUploadImageApiURL;

/**
 * 极光推送APP KEY
 */
FOUNDATION_EXTERN NSString * const JPUSH_APP_KEY;

#pragma mark -

@interface AppConfig : NSObject

@singleton(AppConfig)

+ (void)setup;

- (NSString *)scheme;
+ (NSString *)schemeWithAction:(NSString *)action;

+ (NSString *)videoSrtCachePath;
+ (void)setupConfigService:(void (^)(id data))block;

#pragma mark -

+ (BOOL)isShowQQ;
+ (BOOL)isShowWechat;
+ (BOOL)isShowWeibo;

+ (BOOL)QQSharedShow;
+ (BOOL)wechatSharedShow;
+ (BOOL)weiboSharedShow;

@end
