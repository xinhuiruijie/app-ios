//
//  AppPusher.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/11/23.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "AppPusher.h"
#import "ConfigModel.h"

@implementation AppPusher

// 在 +load 里加载，获取缓存错误。需要等初始化完毕后再注册
+ (void)setup {
    JPush * push = [JPush sharedInstance];
    
    [self bindTag];
    [self bindToken];
    
    push.whenGotNofiticaition = ^(PushNotification * n) {
        [self handleContentData:n];
    };
}

+ (void)closePushNofiticaition {
    [JPush unregisterNotification];
}

+ (void)openPushNofiticaition {
    [JPush registerNotifiaction];
}

+ (void)bindToken {
    if ([UserModel online]) {
        NSString * uid = [UserModel sharedInstance].user.id;
        [[JPush sharedInstance] bindWithAlias:uid completion:nil];
    } else {
        [self unBindToken];
    }
}

+ (void)unBindToken {
    //不做操作
    [[JPush sharedInstance] unbindWithAlias:@"" completion:nil];
}

+ (void)bindTag {
    NSString *tag = @"cn";
    [[JPush sharedInstance] bindTag:tag completion:nil];
}

+ (void)handleContentData:(PushNotification *)n {
    NSDictionary *content = n.userInfo;
    // 在前台就不做处理 // TODO: ?
    if (!n.inApp) {
        NSString *link = [content objectForKey:@"link"];
        if (link && [link isKindOfClass:[NSString class]]) {
            if ([link hasPrefix:@"deeplink://"]) {
                // 是deeplink 替换前缀执行deeplink
                link = [link stringByReplacingOccurrencesOfString:@"deeplink" withString:[AppConfig sharedInstance].scheme];
            }
            if (link && link.length) {
                if (![DeepLink handleURLString:link withCompletion:nil]) {
                    [[UIApplication sharedApplication].keyWindow presentFailureTips:@"无法打开的链接"];
                }
            }
        }
    }
}

+ (void)setBadge:(NSInteger)value {
    [[JPush sharedInstance] setBadge:value];
}

+ (void)resetBadge {
    [[JPush sharedInstance] resetBadge];
}

@end
