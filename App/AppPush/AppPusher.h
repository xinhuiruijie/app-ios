//
//  AppPusher.h
//  MotherPlanet
//
//  Created by 陈熙 on 2017/11/23.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JPush.h"

@interface AppPusher : NSObject

+ (void)setup;
+ (void)bindToken;
+ (void)unBindToken;
+ (void)bindTag;
+ (void)closePushNofiticaition;
+ (void)openPushNofiticaition;

+ (void)setBadge:(NSInteger)value;
+ (void)resetBadge;

@end
