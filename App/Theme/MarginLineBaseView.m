//
//  MarginLineBaseView.m
//  MotherPlanet
//
//  Created by GeekZooStudio on 16/10/28.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import "MarginLineBaseView.h"

IB_DESIGNABLE
@implementation MarginLineBaseView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [AppTheme lineColor];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        self.backgroundColor = [AppTheme lineColor];
    }
    return self;
}

- (void)setBackgroundColor:(UIColor *)backgroundColor
{
    backgroundColor = [AppTheme lineColor];
    [super setBackgroundColor:backgroundColor];
}

@end
