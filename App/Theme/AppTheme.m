//
//  AppTheme.m
//  MotherPlanet
//
//  Created by liuyadi on 16/8/21.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import "AppTheme.h"

@implementation AppTheme

@def_singleton( AppTheme )

+ (void)load {
    [[self class] setupAppearance];
}

+ (void)setupAppearance {
    [[UINavigationBar appearance] setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]] forBarMetrics:UIBarMetricsDefault];
//    [[UINavigationBar appearance] setTitleTextAttributes:@{NSFontAttributeName: [UIFont systemFontOfSize:18], NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [[UINavigationBar appearance] setShadowImage:[UIImage imageWithColor:[UIColor colorWithRGBValue:0xEBEDF2]]];
    
//    [[UITabBar appearance] setTintColor:[self mainColor]];
//    [[UITabBar appearance] setBackgroundImage:[UIImage imageWithColor:[UIColor whiteColor]]];
//    [[UITabBar appearance] setTranslucent:YES];
//    [[UITabBar appearance] setShadowImage:[UIImage imageWithColor:[self lineColor]]];
//    [[UITabBar appearance] setClipsToBounds:YES];
}

#pragma mark - Default Placeholder

+ (NSString *)defaultPlaceholder {
    return @"";
}

#pragma mark - Color

+ (UIColor *)mainColor {
    return [UIColor colorWithRGBValue:0x3B3F44];
}

/// 343338
+ (UIColor *)normalTextColor {
    return [UIColor colorWithRGBValue:0x343338];
}
+ (UIColor *)lightGrayColor {
    return [UIColor colorWithRGBValue:0x999999];
}
/// A9AEB5
+ (UIColor *)subTextColor {
    return [UIColor colorWithRGBValue:0xA9AEB5];
}
/// 323E49
+ (UIColor *)textBackgroundColor {
    return [UIColor colorWithRGBValue:0x323E49];
}
/// EFB400
+ (UIColor *)typeLabelBackgroundColor {
    return [UIColor colorWithRGBValue:0xEFB400];
}
/// F2B300
+ (UIColor *)fractionLabelTextColor {
    return [UIColor colorWithRGBValue:0xF2B300];
}
+ (UIColor *)wormholeNavigationColor {
    return [UIColor colorWithRGBValue:0xF2B300];
}
/// 272B31
+ (UIColor *)nameTextColor {
    return [UIColor colorWithRGBValue:0x272B31];
}

+ (UIColor *)textSelectedColor {
    return [UIColor colorWithRGBValue:0xF2B300];
}

+ (UIColor *)subTitleTextColor {
    return [UIColor colorWithRGBValue:0xCBD2DB];
}

+ (UIColor *)subButtonTextColor {
    return [UIColor colorWithRGBValue:0xCBD2DB];
}

+ (UIColor *)noMoreFooterTextColor {
    return [UIColor colorWithRGBValue:0xCBD2DB];
}

+ (UIColor *)tabNormalColor {
    return [UIColor colorWithRGBValue:0xA4A4A4];
}

+ (UIColor *)tabSelectedColor {
    return [UIColor colorWithRGBValue:0xF2B300];
}

+ (UIColor *)verifyCodeButtonColor {
    return [UIColor colorWithRGBValue:0x343338];
}

+ (UIColor *)disabledButtonColor {
    return [UIColor colorWithRGBValue:0xB1B3B6];
}

+ (UIColor *)normalColor {
    return [UIColor colorWithRGBValue:0xB0B5C1];
}

+ (UIColor *)normalButtonColor {
    return [UIColor whiteColor];
}

+ (UIColor *)selectedColor {
    return [UIColor colorWithRGBValue:0xEFB400];
}

+ (UIColor *)qualityNormalColor {
    return [UIColor whiteColor];
}

+ (UIColor *)qualitySelectedColor {
    return [UIColor colorWithRGBValue:0xF2B300];
}

+ (UIColor *)lineColor {
    return [UIColor colorWithRGBValue:0xCBD2DB];
}

+ (UIColor *)searchLineColor {
    return [UIColor colorWithRGBValue:0xE9ECF0];
}

+ (UIColor *)borderColor {
    return [UIColor colorWithRGBValue:0xE6E6E6];
}

+ (UIColor *)shadowColor {
    return [UIColor colorWithWhite:0.6 alpha:0.4];
}

+ (UIColor *)backgroundColor {
    return [UIColor whiteColor];
}

+ (UIColor *)listBackgroundColor {
    return [UIColor colorWithRGBValue:0xF3F5F7];
}

+ (UIColor *)LoginPlaceholderColor {
    return [UIColor colorWithRGBValue:0xE9ECF0];
}

#pragma mark - Line

+ (CGFloat)onePixel {
    static CGFloat one;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        one = 1 / [UIScreen mainScreen].scale;
    });
    return one;
}

#pragma mark - Placeholder

+ (UIImage *)placeholderImage {
//    return [UIImage imageWithColor:[UIColor colorWithRGBValue:0xE9ECF0]];
    return [UIImage imageNamed:@"bg_prestrain"];
}

+ (UIImage *)navigationImage {
    return [UIImage imageWithColor:[UIColor colorWithRGBValue:0xF2B300]];
}

+ (UIImage *)avatarDefaultImage {
    return [UIImage imageNamed:@"icon_head_default"];
}

#pragma mark -

+ (CGSize)calculateCellSizeWithContent:(NSString *)content {
    CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
    
    CGSize size = [content sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:14]}];
    size.width += 30.f; // 文字左右边距均为15
    if ( size.width > screenWidth - 30 )
    {
        size.width = screenWidth - 30;
    }
    size.width = ceil(size.width);
    size.height = 27.f;
    
    return size;
}

+ (CGFloat)calculateLikeAnimateLabelWidthWithContent:(NSString *)content {
    CGRect frame = [content boundingRectWithSize:CGSizeMake(MAXFLOAT, 22) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName: [UIFont fontWithName:@"Gotham-Medium" size:12]} context:nil];
    
    return ceil(frame.size.width);
}

+ (CGFloat)calculateHeigthWithContent:(NSString *)content
                                width:(CGFloat)width {
    NSAttributedString *attributedStr = [[self class] detailAttributedString:content];
    CGRect frame = [attributedStr boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    return ceil(frame.size.height);
}

+ (CGFloat)calculateWidthWithContent:(NSString *)content
                              height:(CGFloat)height {
    NSAttributedString *attributedStr = [[self class] detailAttributedString:content];
    CGRect frame = [attributedStr boundingRectWithSize:CGSizeMake(MAXFLOAT, height) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    return ceil(frame.size.width);
}

+ (CGFloat)calculateHeigthWithAttributeContent:(NSAttributedString *)content
                                width:(CGFloat)width {
    CGRect frame = [content boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin context:nil];
    
    return ceil(frame.size.height);
}

+ (CGFloat)calculateMarkParkingTipsHeigthWithContent:(NSString *)content
                                               width:(CGFloat)width {
    CGRect frame = [content boundingRectWithSize:CGSizeMake(width, MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:@{NSFontAttributeName : [UIFont systemFontOfSize:14]} context:nil];
    return ceil(frame.size.height);
}

#pragma mark - UITabBarItem

+ (UITabBarItem *)itemWithImage:(UIImage *)image selectImage:(UIImage *)selectImage title:(NSString *)title {
    UITabBarItem * item = [[UITabBarItem alloc] initWithTitle:title image:image selectedImage:selectImage];
    item.image = image;
    item.selectedImage = selectImage;
    item.title = title;
    
    [item setTitleTextAttributes:@{NSForegroundColorAttributeName : [self tabNormalColor], NSFontAttributeName : [UIFont systemFontOfSize:12]} forState:UIControlStateNormal];
    [item setTitleTextAttributes:@{NSForegroundColorAttributeName : [self tabSelectedColor], NSFontAttributeName : [UIFont systemFontOfSize:12]} forState:UIControlStateSelected];

    item.image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    item.selectedImage = [selectImage imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    if ([SDiOSVersion deviceSize] == Screen5Dot8inch) {
        item.imageInsets = UIEdgeInsetsMake(2, 0, -2, 0);
        item.titlePositionAdjustment = UIOffsetMake(0, 2);
    } else {
        item.imageInsets = UIEdgeInsetsMake(-3, 0, 3, 0);
        item.titlePositionAdjustment = UIOffsetMake(0, -4);
    }
    
    return item;
}

+ (UITabBarItem *)itemWithImageName:(NSString *)imageName selectImageName:(NSString *)selectImageName title:(NSString *)title {
    UIImage * image  = [UIImage imageNamed:imageName];
    UIImage * selectImage = [UIImage imageNamed:selectImageName];
    
    UITabBarItem * item = [[self class] itemWithImage:image
                                          selectImage:selectImage
                                                title:title];
    
    return item;
}

#pragma mark - UINavigationBarItem

+ (UIBarButtonItem *)itemWithContent:(id)content
                           tintColor:(UIColor *)tintColor
                            fontSize:(UIFont *)fontSize
                             handler:(void (^)(id))handler {
    UIBarButtonItem * item = nil;
    
    if ( [content isKindOfClass:[NSString class]] ) {
        item = [[UIBarButtonItem alloc] bk_initWithTitle:content style:UIBarButtonItemStylePlain handler:handler];
        switch ([SDiOSVersion deviceSize]) {
            case Screen3Dot5inch:
                [item setTitlePositionAdjustment:UIOffsetZero forBarMetrics:UIBarMetricsDefault];
                break;
            case Screen4inch:
                [item setTitlePositionAdjustment:UIOffsetZero forBarMetrics:UIBarMetricsDefault];
                break;
            case Screen4Dot7inch:
                [item setTitlePositionAdjustment:UIOffsetMake(4, 0) forBarMetrics:UIBarMetricsDefault];
                break;
            case Screen5Dot5inch:
                [item setTitlePositionAdjustment:UIOffsetMake(8, 0) forBarMetrics:UIBarMetricsDefault];
                break;
            case Screen5Dot8inch:
                [item setTitlePositionAdjustment:UIOffsetMake(8, 0) forBarMetrics:UIBarMetricsDefault];
                break;
                
            default:
                break;
        }
        item.tintColor = tintColor;
        [item setTitleTextAttributes:@{NSFontAttributeName : fontSize} forState:UIControlStateNormal];
    } else if ( [content isKindOfClass:[UIImage class]] ) {
        item = [[UIBarButtonItem alloc] bk_initWithImage:[(UIImage *)content imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal] style:UIBarButtonItemStylePlain handler:handler];
    }
    
    return item;
}

+ (UIView *)customTitleView:(NSString *)title {
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 34)];
    titleLabel.textColor = [AppTheme normalTextColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = title;
    titleLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size:24];
    return titleLabel;
}

+ (UIBarButtonItem *)normalItemWithContent:(id)content handler:(void (^)(id))handler {
    return [self itemWithContent:content
                       tintColor:[AppTheme normalTextColor]
                        fontSize:[UIFont systemFontOfSize:14]
                         handler:handler];
}

+ (UIBarButtonItem *)lightGrayItemWithContent:(id)content handler:(void (^)(id))handler {
    return [self itemWithContent:content
                       tintColor:[AppTheme lightGrayColor]
                        fontSize:[UIFont systemFontOfSize:14]
                         handler:handler];
}

+ (UIBarButtonItem *)defaultItemWithContent:(id)content handler:(void (^)(id))handler {
    return [self itemWithContent:content
                       tintColor:[UIColor whiteColor]
                        fontSize:[UIFont systemFontOfSize:16]
                         handler:handler];
}

+ (UIBarButtonItem *)scanItemWithHandler:(void (^)(id sender))handler {
    UIBarButtonItem * item = [self defaultItemWithContent:[UIImage imageNamed:@"icon_scan"] handler:handler];
    return item;
}

+ (UIBarButtonItem *)backItemWithHandler:(void (^)(id sender))handler {
    UIBarButtonItem * item = [self defaultItemWithContent:[UIImage imageNamed:@"icon_arrow_back"] handler:handler];
    return item;
}

+ (UIBarButtonItem *)whiteBackItemWithHandler:(void (^)(id sender))handler {
    UIBarButtonItem * item = [self defaultItemWithContent:[UIImage imageNamed:@"icon_back_white"] handler:handler];
    return item;
}

+ (UIBarButtonItem *)shareItemWithHandler:(void (^)(id sender))handler {
    // icon_share_top
    UIBarButtonItem * item = [self defaultItemWithContent:[UIImage imageNamed:@"icon_daily_share_b"] handler:handler];
    return item;
}

+ (UIButton *)navigationItemCustomView:(UIImage *)icon {
    UIButton *itemButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [itemButton setImage:icon forState:UIControlStateNormal];
    [itemButton setFrame:CGRectMake(0, 0, 30, 30)];
    return itemButton;
}

#pragma mark -

+ (CGSize)calculateSearchListWithCount:(NSInteger)count {
    // 列数为4、行间距为0
    return [[self class] calculateCardListSizeWithCount:count
                                            columnCount:3
                                             itemHeight:[[self class] searchItemSize].height
                                              rowHeight:0.f];
}

+ (CGSize)searchItemSize {
    // 左右间距均为8、中间间距为1
    CGSize size = [[self class] calculateCardItemSizeWithRatio:80.f / 80.f
                                                        margin:8.f
                                                   columnSpace:10.f
                                                   columnCount:3];
    
    switch ( [SDiOSVersion deviceSize] )
    {
        case Screen3Dot5inch:
        case Screen4inch:
        {
            size.height += 7.f;
        }
            break;
        case Screen4Dot7inch:
        {
            size.height -= 5.f;
        }
            break;
        case Screen5Dot5inch:
        {
            size.height -= 14.f;
        }
            break;
            
        default:
            break;
    }
    
    return size;
}

+ (CGSize)predicateItemSize
{
    // 左右间距均为0、中间间距为0
    return [[self class] calculateCardItemSizeWithRatio:95.f / 375.f
                                                 margin:0.f
                                            columnSpace:0.f
                                            columnCount:1];
}

/**
 *  计算表格cell在不同屏幕中的尺寸
 *
 *  @param margin      表格距屏幕的边距
 *  @param columnCount 表格的列数
 *
 *  @return 表格cell的size
 */
+ (CGSize)calculateFormItemSizeWithMargin:(CGFloat)margin
                              columnCount:(NSInteger)columnCount
{
    if ( columnCount == 0 )
        return CGSizeZero;
    
    CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
    
    CGFloat ratio = 64.f / 375.f;
    CGFloat height = floorf(ratio * screenWidth);
    CGFloat width = (screenWidth - 2 * margin) / columnCount;
    return CGSizeMake( width, height );
}

+ (CGSize)calculateCardItemSizeWithRatio:(CGFloat)ratio
                                  margin:(CGFloat)margin
                             columnSpace:(CGFloat)columnSpace
                             columnCount:(NSInteger)columnCount {
    CGFloat contentWidth = [[UIScreen mainScreen] bounds].size.width;
    return [[self class] calculateCardItemSizeWithRatio:ratio
                                                 margin:margin
                                            columnSpace:columnSpace
                                            columnCount:columnCount
                                           contentWidth:contentWidth];
}

/**
 *  依据设计图cell高宽比、cell距屏幕左右边距、cell之间的列间距及屏幕中显示的cell的列数，
 *  计算cell在不同屏幕中的尺寸
 *
 *  @param ratio       设计图cell高宽比
 *  @param margin      cell距屏幕左右边距
 *  @param columnSpace cell之间的列间距
 *  @param columnCount 屏幕中显示的cell的列数
 *  @param contentWidth 内容区域宽度（即去掉边距部分）
 *
 *  @return cell在不同屏幕中的尺寸
 */
+ (CGSize)calculateCardItemSizeWithRatio:(CGFloat)ratio
                                  margin:(CGFloat)margin
                             columnSpace:(CGFloat)columnSpace
                             columnCount:(NSInteger)columnCount
                            contentWidth:(CGFloat)contentWidth {
    if (ratio == 0 || columnCount == 0)
        return CGSizeZero;
    
    CGFloat width = (contentWidth - 2 * margin - (columnCount - 1) * columnSpace) / columnCount;
    CGFloat height = floorf(ratio * width);
    
    return CGSizeMake(width, height);
}

/**
 *  依据设计图【中心view】高宽比、中心图距屏幕左右边距、上下边距
 *  计算cell在不同屏幕中的尺寸
 *
 *  @param ratio       设计图【中心view】的高宽比
 *
 *  @return cell在不同屏幕中的尺寸
 */
+ (CGSize)calculateCardItemSizeWithInsets:(UIEdgeInsets)inset
                                    ratio:(CGFloat)ratio {
    
    CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
    
    CGFloat itemWidth = screenWidth - inset.left - inset.right;
    CGFloat itemHeight = floorf(itemWidth / ratio);
    CGFloat height = itemHeight + inset.top + inset.bottom;
    
    return CGSizeMake(screenWidth, height);
}

+ (CGSize)calculateCardListSizeWithCount:(NSInteger)count
                             columnCount:(NSInteger)columnCount
                              itemHeight:(CGFloat)itemHeight
                               rowHeight:(CGFloat)rowHeight {
    if ( count == 0 ||
        columnCount == 0 )
        return CGSizeZero;
    
    CGFloat screenWidth = [[UIScreen mainScreen] bounds].size.width;
    
    NSInteger rowCount = [AppTheme calculateRowCountWithTotalCount:count columnCount:columnCount];
    
    CGFloat height = itemHeight * rowCount + ( rowCount + 1 ) * rowHeight; // 行间距为0
    
    return CGSizeMake( screenWidth, height ); // 左右间距均为0
}


+ (NSInteger)calculateRowCountWithTotalCount:(NSInteger)totalCount
                                 columnCount:(NSInteger)columnCount {
    if ( totalCount == 0 ||
        columnCount == 0 )
        return 0;
    
    return ( totalCount % columnCount == 0 ) ? ( totalCount / columnCount ) : (totalCount / columnCount + 1 );
}

#pragma mark -

+ (NSMutableAttributedString *)attributedString:(NSString *)str
                                   withLocation:(NSUInteger)location
                                      leadColor:(UIColor *)leadColor
                                       leadFont:(UIFont *)leadFont
                                     trailColor:(UIColor *)trailColor
                                      trailFont:(UIFont *)trailFont {
    if (str == nil) {
        return nil;
    }
    
    NSMutableAttributedString * placeholder = nil;
    if (str.length == location) {
        placeholder = [[NSMutableAttributedString alloc] initWithString:str];
        NSDictionary * attributes = @{NSForegroundColorAttributeName : leadColor, NSFontAttributeName : leadFont};
        [placeholder addAttributes:attributes range:NSMakeRange(0, location)];
    } else {
        placeholder = [[NSMutableAttributedString alloc] initWithString:str];
        NSDictionary * leadAttributes = @{NSForegroundColorAttributeName : leadColor, NSFontAttributeName : leadFont};
        [placeholder addAttributes:leadAttributes range:NSMakeRange(0, location)];
        NSDictionary * trialAttributes = @{NSForegroundColorAttributeName : trailColor, NSFontAttributeName : trailFont};
        [placeholder addAttributes:trialAttributes range:NSMakeRange(location , str.length - location)];
    }
    
    return placeholder;
}

#pragma mark - AttributedString

+ (NSMutableAttributedString *)highlightPattern:(NSString *)pattern
                                 withGoalString:(NSString *)goalString
                                    normalColor:(UIColor *)normalColor
                                     normalFont:(UIFont *)normalFont
                                     themeColor:(UIColor *)themeColor
                                      themeFont:(UIFont *)themeFont
                                 paragraphStyle:(NSMutableParagraphStyle *)paragraphStyle {
    NSMutableAttributedString *attributeString = [[NSMutableAttributedString alloc] initWithString:goalString];
    NSRegularExpression *iExpression;
    
    NSDictionary *normalAttributes = @{NSForegroundColorAttributeName : normalColor, NSFontAttributeName : normalFont, NSParagraphStyleAttributeName: paragraphStyle};
    [attributeString addAttributes:normalAttributes range:NSMakeRange(0, goalString.length)];
    NSDictionary *themeAttributes = @{NSForegroundColorAttributeName : themeColor, NSFontAttributeName : themeFont, NSParagraphStyleAttributeName: paragraphStyle};
    
    iExpression = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:NULL];
    NSRange paragaphRange = NSMakeRange(0, goalString.length);
    
    NSArray *matches = [iExpression matchesInString:goalString options:0 range:paragaphRange];
    if (matches) {
        for (NSTextCheckingResult *result in matches) {
            [attributeString addAttributes:themeAttributes range:NSMakeRange(result.range.location , result.range.length)];
        }

    }
    return attributeString;
}

// 搜索结果页，关键字标红
+ (NSMutableAttributedString *)markKeywords:(NSString *)keywords withGoalString:(NSString *)goalString {
    NSMutableAttributedString * attributeString = [[NSMutableAttributedString alloc] init];
    
    // 根据关键字截取字符串（会把关键字截掉，所以需要再拼接上）
    NSArray * substringsArray = [goalString componentsSeparatedByString:keywords];
    for (int i = 0; i < substringsArray.count; i++) {
        NSString * subString = substringsArray[i];
        // 最后一个不拼接关键字
        if (i == substringsArray.count - 1) {
            NSMutableAttributedString * subAttributeString = [[NSMutableAttributedString alloc] initWithString:subString];
            [attributeString appendAttributedString:subAttributeString];
            break;
        }
        
        subString = [subString stringByAppendingString:keywords];
        NSMutableAttributedString * subAttributeString = [[NSMutableAttributedString alloc] initWithString:subString];
        NSRange range = [subString rangeOfString:keywords];
        // 关键字位置标红
        [subAttributeString addAttributes:@{NSForegroundColorAttributeName: [AppTheme mainColor]} range:range];
        [attributeString appendAttributedString:subAttributeString];
    }
    return attributeString;
}

+ (NSMutableAttributedString *)attributedString:(NSString *)str
                                      symbolStr:(NSString *)symbolStr
                                        bigFont:(UIFont *)bigFont
                                      smallFont:(UIFont *)smallFont
                                      textColor:(UIColor *)textColor {
    if (str == nil)
        return nil;
    
    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] init];
    CGFloat capHeight = bigFont.pointSize - smallFont.pointSize;
    NSAttributedString *headStr = [[NSAttributedString alloc] initWithString:str attributes:@{NSFontAttributeName : bigFont,NSForegroundColorAttributeName : textColor,NSBaselineOffsetAttributeName : @(0)}];
    NSAttributedString *tailStr = [[NSAttributedString alloc] initWithString:symbolStr attributes:@{NSFontAttributeName : smallFont,NSForegroundColorAttributeName : textColor,NSBaselineOffsetAttributeName : @(capHeight)}];
    [attributedString appendAttributedString:headStr];
    [attributedString appendAttributedString:tailStr];
    
    return attributedString;
}

+ (NSMutableAttributedString *)attributedString:(NSString *)str
                             withParagraphStyle:(NSMutableParagraphStyle *)paragraphStyle {
    if ( str == nil )
        return nil;
    
    NSMutableAttributedString * attributedString = [[NSMutableAttributedString alloc] initWithString:str];
    [attributedString addAttribute:NSParagraphStyleAttributeName value:paragraphStyle range:NSMakeRange(0, str.length)];
    
    return attributedString;
}

#pragma mark - ParagraphStyle

+ (NSMutableParagraphStyle *)titleParagraphStyle:(CGFloat)lineSpacing {
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    [paragraphStyle setLineSpacing:lineSpacing];
    return paragraphStyle;
}

+ (NSMutableParagraphStyle *)detailParagraphStyle {
    return [[self class] lineSpaceParagraphStyle:2.f alignment:NSTextAlignmentLeft];
}

+ (NSMutableParagraphStyle *)titleParagraphStyle {
    return [[self class] lineSpaceParagraphStyle:2.f alignment:NSTextAlignmentLeft];
}

+ (NSMutableParagraphStyle *)commentParagraphStyle {
    return [[self class] lineSpaceParagraphStyle:8.f alignment:NSTextAlignmentLeft];
}

+ (NSMutableParagraphStyle *)lineSpaceParagraphStyle:(CGFloat)lineSpace alignment:(NSTextAlignment)alignment {
    // 设置段落样式
    NSMutableParagraphStyle * paragraphStyle = [[NSMutableParagraphStyle alloc] init];
    paragraphStyle.firstLineHeadIndent = 0.f; //开头缩排
    paragraphStyle.headIndent = 0.f; //左边 padding
    paragraphStyle.tailIndent = 0.f; //右边 padding
    paragraphStyle.lineSpacing = lineSpace; //行距
    paragraphStyle.alignment = alignment;
    paragraphStyle.paragraphSpacing = CGFLOAT_MIN;
    paragraphStyle.paragraphSpacingBefore = CGFLOAT_MIN;
    
    return paragraphStyle;
}


#pragma mark -

+ (NSAttributedString *)detailAttributedString:(NSString *)content {
    return [[self class] attributedStringWithContent:content
                                                font:[UIFont systemFontOfSize:12]
                                      paragraphStyle:[[self class] detailParagraphStyle]];
}

+ (NSAttributedString *)commentAttributedString:(NSString *)content {
    return [[self class] attributedStringWithContent:content
                                                font:[UIFont systemFontOfSize:16]
                                      paragraphStyle:[[self class] commentParagraphStyle]];
}

+ (NSAttributedString *)titleAttributedString:(NSString *)content {
    return [[self class] attributedStringWithContent:content
                                                font:[UIFont systemFontOfSize:16 weight:UIFontWeightMedium]
                                      paragraphStyle:[[self class] titleParagraphStyle]];
}

+ (NSAttributedString *)subtitleAttributedString:(NSString *)content {
    return [[self class] attributedStringWithContent:content
                                                font:[UIFont systemFontOfSize:16]
                                      paragraphStyle:[[self class] lineSpaceParagraphStyle:8.f alignment:NSTextAlignmentCenter]];
}

+ (NSAttributedString *)descAttributedString:(NSString *)content {
    return [[self class] attributedStringWithContent:content
                                                font:[UIFont systemFontOfSize:14]
                                      paragraphStyle:[[self class] lineSpaceParagraphStyle:6.f alignment:NSTextAlignmentCenter]];
}

+ (NSAttributedString *)leftDescAttributedString:(NSString *)content {
    return [[self class] attributedStringWithContent:content
                                                font:[UIFont systemFontOfSize:14]
                                      paragraphStyle:[[self class] lineSpaceParagraphStyle:6.f alignment:NSTextAlignmentLeft]];
}

+ (NSAttributedString *)attributedString:(NSString *)content fontSize:(UIFont *)font {
    return [[self class] attributedStringWithContent:content
                                                font:font
                                      paragraphStyle:[[self class] detailParagraphStyle]];
}

+ (NSAttributedString *)centerAttributedString:(NSString *)content fontSize:(UIFont *)font {
    return [[self class] attributedStringWithContent:content
                                                font:font
                                      paragraphStyle:[[self class] detailParagraphStyle]];
}

+ (NSAttributedString *)attributedStringWithContent:(NSString *)content
                                               font:(UIFont *)font
                                     paragraphStyle:(NSMutableParagraphStyle *)paragraphStyle {
    if (content == nil)
        return nil;
    
    NSAttributedString * attributedString = [[NSAttributedString alloc] initWithString:content attributes:@{NSFontAttributeName:font, NSParagraphStyleAttributeName: paragraphStyle}];
    
    return attributedString;
}

#pragma mark - Constellation Image

+ (UIImage *)imageOfConstellation:(NSString *)constellation {
    NSString *constellationImageName;
    if ([constellation isEqualToString:@"水瓶座"]) {
        constellationImageName = @"icon_pro_3shuiping";
    } else if ([constellation isEqualToString:@"双鱼座"]) {
        constellationImageName = @"icon_pro_4shuangyu";
    } else if ([constellation isEqualToString:@"白羊座"]) {
        constellationImageName = @"icon_pro_12baiyang";
    } else if ([constellation isEqualToString:@"金牛座"]) {
        constellationImageName = @"icon_pro_10jinniu";
    } else if ([constellation isEqualToString:@"双子座"]) {
        constellationImageName = @"icon_pro_11shuangzi";
    } else if ([constellation isEqualToString:@"巨蟹座"]) {
        constellationImageName = @"icon_pro_9juxie";
    } else if ([constellation isEqualToString:@"狮子座"]) {
        constellationImageName = @"icon_pro_7shizi";
    } else if ([constellation isEqualToString:@"处女座"]) {
        constellationImageName = @"icon_pro_8chunv";
    } else if ([constellation isEqualToString:@"天秤座"]) {
        constellationImageName = @"icon_pro_6tianchen";
    } else if ([constellation isEqualToString:@"天蝎座"]) {
        constellationImageName = @"icon_pro_2tianxie";
    } else if ([constellation isEqualToString:@"射手座"]) {
        constellationImageName = @"icon_pro_1shehsou";
    } else if ([constellation isEqualToString:@"摩羯座"]) {
        constellationImageName = @"icon_pro_5mojie";
    }
    return [UIImage imageNamed:constellationImageName];
}

@end
