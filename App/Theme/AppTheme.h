//
//  AppTheme.h
//  MotherPlanet
//
//  Created by liuyadi on 16/8/21.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppTheme : NSObject

@singleton( AppTheme )

#pragma mark - Default Placeholder

+ (NSString *)defaultPlaceholder;

#pragma mark - Color

+ (UIColor *)mainColor;

/// #343338
+ (UIColor *)normalTextColor;
/// #999999
+ (UIColor *)lightGrayColor;
/// #A9AEB5
+ (UIColor *)subTextColor;

+ (UIColor *)textSelectedColor;

/// 323E49
+ (UIColor *)textBackgroundColor;
/// EFB400
+ (UIColor *)typeLabelBackgroundColor;
/// F2B300
+ (UIColor *)fractionLabelTextColor;
+ (UIColor *)wormholeNavigationColor;
/// 272B31
+ (UIColor *)nameTextColor;

+ (UIColor *)mainButtonColor;
+ (UIColor *)subTitleTextColor;
+ (UIColor *)subButtonTextColor;
+ (UIColor *)noMoreFooterTextColor;

+ (UIColor *)verifyCodeButtonColor;
+ (UIColor *)disabledButtonColor;

+ (UIColor *)normalColor;
+ (UIColor *)normalButtonColor;
+ (UIColor *)selectedColor;
+ (UIColor *)qualityNormalColor;
+ (UIColor *)qualitySelectedColor;

+ (UIColor *)lineColor;
+ (UIColor *)searchLineColor;
+ (UIColor *)borderColor;
+ (UIColor *)shadowColor;
+ (UIColor *)backgroundColor;
+ (UIColor *)listBackgroundColor;
+ (UIColor *)LoginPlaceholderColor;

#pragma mark - Line

+ (CGFloat)onePixel;

#pragma mark - Placeholder

+ (UIImage *)placeholderImage;
+ (UIImage *)navigationImage;
+ (UIImage *)avatarDefaultImage;

#pragma mark -

+ (CGSize)calculateSearchListWithCount:(NSInteger)count;
+ (CGSize)searchItemSize;
+ (CGSize)predicateItemSize;

#pragma mark -

+ (CGSize)calculateCellSizeWithContent:(NSString *)content;
+ (CGFloat)calculateLikeAnimateLabelWidthWithContent:(NSString *)content;
+ (CGFloat)calculateHeigthWithContent:(NSString *)content
                                width:(CGFloat)width;
+ (CGFloat)calculateWidthWithContent:(NSString *)content
                                height:(CGFloat)height;
+ (CGFloat)calculateHeigthWithAttributeContent:(NSAttributedString *)content
                                         width:(CGFloat)width;
+ (CGFloat)calculateMarkParkingTipsHeigthWithContent:(NSString *)content width:(CGFloat)width;

#pragma mark - UITabBarItem
+ (UITabBarItem *)itemWithImage:(UIImage *)image selectImage:(UIImage *)selectImage title:(NSString *)title;
+ (UITabBarItem *)itemWithImageName:(NSString *)imageName selectImageName:(NSString *)selectImageName title:(NSString *)title;

#pragma mark - UINavigationBarItem
+ (UIButton *)navigationItemCustomView:(UIImage *)icon;
+ (UIView *)customTitleView:(NSString *)title;
+ (UIBarButtonItem *)normalItemWithContent:(id)content handler:(void (^)(id))handler;
+ (UIBarButtonItem *)lightGrayItemWithContent:(id)content handler:(void (^)(id))handler;
+ (UIBarButtonItem *)defaultItemWithContent:(id)content handler:(void (^)(id sender))handler;
+ (UIBarButtonItem *)scanItemWithHandler:(void (^)(id sender))handler;
+ (UIBarButtonItem *)backItemWithHandler:(void (^)(id sender))handler;
+ (UIBarButtonItem *)whiteBackItemWithHandler:(void (^)(id sender))handler;
+ (UIBarButtonItem *)shareItemWithHandler:(void (^)(id sender))handler;

#pragma mark -

// 搜索结果页，关键字标红
+ (NSMutableAttributedString *)markKeywords:(NSString *)keywords
                             withGoalString:(NSString *)goalString;
+ (NSMutableAttributedString *)attributedString:(NSString *)str
                                      symbolStr:(NSString *)symbolStr
                                        bigFont:(UIFont *)bigFont
                                      smallFont:(UIFont *)smallFont
                                      textColor:(UIColor *)textColor;
+ (NSMutableAttributedString *)attributedString:(NSString *)str
                             withParagraphStyle:(NSMutableParagraphStyle *)paragraphStyle;

#pragma mark -

+ (NSMutableParagraphStyle *)titleParagraphStyle:(CGFloat)lineSpacing;

+ (NSAttributedString *)detailAttributedString:(NSString *)content;
+ (NSAttributedString *)titleAttributedString:(NSString *)content;
+ (NSAttributedString *)commentAttributedString:(NSString *)content;
+ (NSAttributedString *)subtitleAttributedString:(NSString *)content;
+ (NSAttributedString *)descAttributedString:(NSString *)content;
+ (NSAttributedString *)leftDescAttributedString:(NSString *)content;
+ (NSAttributedString *)attributedString:(NSString *)content fontSize:(UIFont *)font;
+ (NSAttributedString *)centerAttributedString:(NSString *)content fontSize:(UIFont *)font;

#pragma mark - Constellation Image

+ (UIImage *)imageOfConstellation:(NSString *)constellation;

@end
