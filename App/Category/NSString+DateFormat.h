//
//  NSString+DateFormat.h
//  ParkviewGreen
//
//  Created by liuyadi on 2016/12/29.
//  Copyright © 2016年 GeekZooStudio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (DateFormat)

// 输入格式 输出格式
- (NSString *)formatDateWithInputFormat:(NSString *)inputFormat outputFormat:(NSString *)outputFormat;

/**
 *  判断 如果是纯数字 就在前面拼上￥
 */
+ (NSString *)formatForPrice:(NSString *)price;

// 字符串转时间戳
- (NSString *)stringFromTimestamp;
- (NSString *)conversionDateWithString;

- (NSString *)strToamount;

@end
