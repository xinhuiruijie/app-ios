//
//  UIView+SingleLayerAnimated.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/24.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "UIView+SingleLayerAnimated.h"

@implementation UIView (SingleLayerAnimated)

- (void)startSingleViewAnimation {
    CATransform3D rotation = CATransform3DMakeTranslation(0 ,20 ,0);//3D旋转
    rotation.m34 = 1.0/ -600;
    
    self.layer.transform = rotation;
    self.alpha = 0;
    
    [UIView beginAnimations:@"rotation" context:NULL];
    //旋转时间
    [UIView setAnimationDuration:0.6];
    self.layer.transform = CATransform3DIdentity;
    self.alpha = 1;
    [UIView commitAnimations];
}

- (void)startSingleViewAnimationDelay:(CGFloat)delay {
    CATransform3D rotation = CATransform3DMakeTranslation(0 ,20 ,0);//3D旋转
    rotation.m34 = 1.0/ -600;
    
    self.layer.transform = rotation;
    self.alpha = 0;
    
    [UIView beginAnimations:@"rotation" context:NULL];
    //旋转时间
    [UIView setAnimationDuration:0.6];
    [UIView setAnimationDelay:delay];
    self.layer.transform = CATransform3DIdentity;
    self.alpha = 1;
    [UIView commitAnimations];
}

- (void)followAuthorButtonAnimated {
    self.layer.transform = CATransform3DMakeScale(1.2, 1.2, 1.2);
    [UIView animateWithDuration:0.4 animations:^{
        self.layer.transform = CATransform3DMakeScale(1.0, 1.0, 1.0);
    }];
}

@end
