//
//  NSNumber+CountFormat.h
//  MotherPlanet
//
//  Created by liuyadi on 2018/1/29.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (CountFormat)

- (NSString *)setupCountNumber;

- (NSString *)playCountNumber;

- (NSString *)videoCountNumber;

@end
