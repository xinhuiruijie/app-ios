//
//  NSObject+Download.h
//  MotherPlanet
//
//  Created by liuyadi on 2018/1/17.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Download)

- (void)imageWithUrl:(NSString *)url completion:(void(^)(UIImage * image))then;

@end
