//
//  UIScrollView+ContentInsets.m
//  MotherPlanet
//
//  Created by GeekZooStudio on 2018/4/19.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "UIScrollView+ContentInsets.h"

@implementation UIScrollView (ContentInsets)

- (void)setContentInsets_top:(CGFloat)contentInsets_top {
    UIEdgeInsets insets = self.contentInset;
    insets.top = contentInsets_top;
    self.contentInset = insets;
}

- (CGFloat)contentInsets_top {
    return self.contentInset.top;
}
///////////////////////////////////////////////////////////////////////////
- (void)setContentInsets_left:(CGFloat)contentInsets_left {
    UIEdgeInsets insets = self.contentInset;
    insets.left = contentInsets_left;
    self.contentInset = insets;
}

- (CGFloat)contentInsets_left {
    return self.contentInset.left;
}
///////////////////////////////////////////////////////////////////////////
- (void)setContentInsets_bottom:(CGFloat)contentInsets_bottom {
    UIEdgeInsets insets = self.contentInset;
    insets.bottom = contentInsets_bottom;
    self.contentInset = insets;
}

- (CGFloat)contentInsets_bottom {
    return self.contentInset.bottom;
}
///////////////////////////////////////////////////////////////////////////
- (void)setContentInsets_right:(CGFloat)contentInsets_right {
    UIEdgeInsets insets = self.contentInset;
    insets.right = contentInsets_right;
    self.contentInset = insets;
}

- (CGFloat)contentInsets_right {
    return self.contentInset.right;
}
///////////////////////////////////////////////////////////////////////////
@end
