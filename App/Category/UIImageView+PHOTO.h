//
//  UIImageView+GMExtension.h
//  MotherPlanet
//
//  Created by GeekZooStudio on 16/8/30.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImageView (PHOTO)

- (void)setImageWithString:(NSString *)imageName;

- (void)setImageWithString:(NSString *)imageName placeholder:(NSString *)placeholder;

- (void)setImageWithPhoto:(PHOTO *)photo;

- (void)setImageWithPhoto:(PHOTO *)photo placeholder:(NSString *)placeholder;

- (void)setImageWithPhoto:(PHOTO *)photo placeholderImage:(UIImage *)placeholderImage;

@end

@interface UIButton (PHOTO)

- (void)setImageWithPhoto:(PHOTO *)photo;

- (void)setImageWithPhoto:(PHOTO *)photo placeholderImage:(UIImage *)placeholderImage;

- (void)setImageWithPhoto:(PHOTO *)photo forState:(UIControlState)state placeholder:(UIImage *)placeholder;

@end
