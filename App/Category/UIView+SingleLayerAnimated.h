//
//  UIView+SingleLayerAnimated.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/24.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (SingleLayerAnimated)

- (void)startSingleViewAnimation;
- (void)startSingleViewAnimationDelay:(CGFloat)delay;

- (void)followAuthorButtonAnimated;

@end
