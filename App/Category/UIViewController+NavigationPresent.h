//
//  UIViewController+NavigationPresent.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/2/16.
//  Copyright © 2017年 GeekZooStudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (NavigationPresent)

- (void)presentNavigationController:(UIViewController *)controller;

- (void)closeController;

@end
