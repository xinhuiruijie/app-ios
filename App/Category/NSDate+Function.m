//
//  NSDate+Function.m
//  tongchuang
//
//  Created by Chenyun on 15/7/1.
//  Copyright (c) 2015年 geek-zoo. All rights reserved.
//

#import "NSDate+Function.h"
#import <EventKit/EventKit.h>

@implementation NSDate (Function)

- (NSString *)timeAgo
{
    NSTimeInterval delta = [[NSDate date] timeIntervalSinceDate:self];
    
    if (delta < 1 * MINUTE)
    {
        return @"刚刚";
    }
    else if (delta < 2 * MINUTE)
    {
        return @"1分钟前";
    }
    else if (delta < 45 * MINUTE)
    {
        int minutes = floor((double)delta/MINUTE);
        return [NSString stringWithFormat:@"%d分钟前", minutes];
    }
    else if (delta < 90 * MINUTE)
    {
        return @"1小时前";
    }
    else if (delta < 24 * HOUR)
    {
        int hours = floor((double)delta/HOUR);
        return [NSString stringWithFormat:@"%d小时前", hours];
    }
    else if (delta < 48 * HOUR)
    {
        return @"1天前";
    }
    else if (delta < 72 * HOUR)
    {
        return @"2天前";
    }
    else if (delta < 12 * MONTH)
    {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"MM-dd HH:mm:ss"];
        
        return [formatter stringFromDate:self];
    }
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    return [formatter stringFromDate:self];
}

- (NSString *)formatterOfMessageAndComment {
    
    NSString *formatterStr = ([NSDate date].year == self.year) ? @"MM/dd" : @"yyyy/MM/dd";
    
    NSString *dateStr = [self formatter:formatterStr];
    
    return dateStr;
}

- (NSString *)formatter:(NSString *)formatterStr {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:formatterStr];
    
    NSString *dateStr = [formatter stringFromDate:self];
    
    return dateStr;
}

+ (NSString *)formatter:(NSString *)formatter dateStr:(NSString *)dateStr {
    
    NSDate *date = nil;
    
    if ([dateStr isNumber]) { // 时间戳
        NSTimeInterval time = dateStr.integerValue;
        date = [NSDate dateWithTimeIntervalSince1970:time];
    } else { // 格式化时间字符串
        date = dateStr.toDate;
    }
    
    if (!date) {
        return dateStr;
    }
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:formatter];
    return [dateFormatter stringFromDate:date];
}

@end
