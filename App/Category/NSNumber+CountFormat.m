//
//  NSNumber+CountFormat.m
//  MotherPlanet
//
//  Created by liuyadi on 2018/1/29.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "NSNumber+CountFormat.h"

@implementation NSNumber (CountFormat)

- (NSString *)setupCountNumber {
    NSString * str = @"0";
    
    if (self.doubleValue > 999) {
        return @"999+";
    } else {
        return self.stringValue;
    }
    
    return str;
}

- (NSString *)playCountNumber {
    NSString * str = @"0";
    
    if ( self.doubleValue >= 100000000 )
    {
        // 大于等于1亿的时候精确到千万位 保留小数点后一位
        str = [NSString stringWithFormat:@"%.1f", self.doubleValue / 100000000.0];
        return [NSString stringWithFormat:@"%@亿",str];
    }
    else if ( self.doubleValue >= 10000 )
    {
        str = [NSString stringWithFormat:@"%.1f", self.doubleValue / 10000.0];
        return [NSString stringWithFormat:@"%@万",str];
    }
    else
    {
        return self.stringValue;
    }
    
    return str;
}

- (NSString *)videoCountNumber {
    NSString * str = @"0";
    if (self.doubleValue >= 100000) {
        str = [NSString stringWithFormat:@"%.0f万", round(self.doubleValue / 10000.0)];
    } else if (self.doubleValue >= 10000) {
        str = [NSString stringWithFormat:@"%.1f万", self.doubleValue / 10000.0];
        if ([str isEqualToString:@"10.0万"]) {
            str = @"10万";
        }
    } else {
        
        NSInteger i = (NSInteger)round(self.doubleValue);
        str = [NSString stringWithFormat:@"%ld", i];
    }
    return str;
}

@end
