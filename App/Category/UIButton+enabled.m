//
//  UIButton+enabled.m
//  MotherPlanet
//
//  Created by liuyadi on 2018/1/19.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "UIButton+enabled.h"

@implementation UIButton (enabled)

- (void)disableSelf {
    self.userInteractionEnabled = NO;
}

- (void)enableSelf {
    self.userInteractionEnabled = YES;
}

- (void)enableSelfDelay {
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.userInteractionEnabled = YES;
    });
}

@end
