//
//  NSObject+TopController.h
//  MotherPlanet
//
//  Created by liuyadi on 2018/1/9.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (TopController)

- (UIViewController *)topViewController;

@end
