//
//  NSString+FormatString.m
//  MotherPlanet
//
//  Created by GeekZooStudio on 2018/4/23.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "NSString+FormatString.h"

@implementation NSString (FormatString)
- (NSString *)formatNameWithBy {
    return [NSString stringWithFormat:@"by%@", self];
}
@end
