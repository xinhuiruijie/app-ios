//
//  NSString+FormatString.h
//  MotherPlanet
//
//  Created by GeekZooStudio on 2018/4/23.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (FormatString)
- (NSString *)formatNameWithBy;
@end
