//
//  NSDate+Function.h
//  tongchuang
//
//  Created by Chenyun on 15/7/1.
//  Copyright (c) 2015年 geek-zoo. All rights reserved.
//


@interface NSDate (Function)

/**
 *  1分钟内显示刚刚，60分钟内显示%d分钟前，24小时内显示%d小时前，30天内显示%d天前，超出30天显示日期 月-日；超过12个月显示年-月-日
 */
- (NSString *)timeAgo;

/**
 *  消息列表和评论列表的时间规则
 */
- (NSString *)formatterOfMessageAndComment;
- (NSString *)formatter:(NSString *)formatterStr;

/**
 *  时间格式化
 *
 *  @param formatter    输出的格式
 *  @param dateStr      要转化的时间，时间戳、格式化时间
 *  @return 格式化时间
 */
+ (NSString *)formatter:(NSString *)formatter dateStr:(NSString *)dateStr;

@end
