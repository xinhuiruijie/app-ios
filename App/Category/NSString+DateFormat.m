//
//  NSString+DateFormat.m
//  ParkviewGreen
//
//  Created by liuyadi on 2016/12/29.
//  Copyright © 2016年 GeekZooStudio. All rights reserved.
//

#import "NSString+DateFormat.h"

@implementation NSString (DateFormat)

- (NSString *)formatDateWithInputFormat:(NSString *)inputFormat outputFormat:(NSString *)outputFormat {
    
    NSDateFormatter *inputFormatter = [[NSDateFormatter alloc] init];
    [inputFormatter setLocale:[[NSLocale alloc] initWithLocaleIdentifier:@"en_US"]];
    [inputFormatter setDateFormat:inputFormat];
    NSDate* inputDate = [inputFormatter dateFromString:self];
    
    NSDateFormatter *outputFormatter = [[NSDateFormatter alloc] init];
    [outputFormatter setLocale:[NSLocale currentLocale]];
    [outputFormatter setDateFormat:outputFormat];
    NSString *outputDate = [outputFormatter stringFromDate:inputDate];
    
    if (outputDate == nil || outputDate.length == 0) {
        return [AppTheme defaultPlaceholder];
    }
    
    return outputDate;
}

+ (NSString *)formatForPrice:(NSString *)price {
    if (price == nil || price.length <= 0) {
        return @"￥0.00";
    }
    
    if ([price containsString:@"￥"] || ![price isNumberWithDecimal]) {
        return price;
    }
    
    return [@"￥" stringByAppendingString:[NSString stringWithFormat:@"%.2f", price.doubleValue]];
}

// 数字和‘.’
- (BOOL)isNumberWithDecimal {
    NSString *		regex = @"[0-9.]+";
    NSPredicate *	pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", regex];
    return [pred evaluateWithObject:self];
}

- (NSString *)stringFromTimestamp {
    if (self == nil) {
        return [AppTheme defaultPlaceholder];
    }
    //时间戳转时间的方法
    NSDate *timeData = [NSDate dateWithTimeIntervalSince1970:[self intValue]];
    NSDateFormatter *dateFormatter =[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd"];
    NSString *strTime = [dateFormatter stringFromDate:timeData];
    return strTime;
}

- (NSString *)conversionDateWithString {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm"];
    NSDate *date = [NSDate dateWithTimeIntervalSince1970:self.integerValue];
    NSString *dateString = [formatter stringFromDate:date];
    return dateString;
}

- (NSString *)strToamount
{
    static NSNumberFormatter * numFormatter = nil;
    
    NSString * strNum = [NSString stringWithFormat:@"%.1f", self.floatValue];
    
    if ( numFormatter == nil )
    {
        numFormatter = [[NSNumberFormatter alloc] init];
    }
    
    numFormatter.numberStyle = NSNumberFormatterDecimalStyle;
    
    NSString *str = @"";
    
    if ( strNum.doubleValue )
    {
        str = [numFormatter stringFromNumber:[NSNumber numberWithDouble:strNum.doubleValue]];
    }
    else
    {
        str = self;
    }
    
    return str;
}

@end
