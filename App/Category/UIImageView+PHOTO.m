//
//  UIImageView+GMExtension.m
//  MotherPlanet
//
//  Created by GeekZooStudio on 16/8/30.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import "UIImageView+PHOTO.h"

@implementation UIImageView (PHOTO)

/**
 *  相同URL不同图片且后台不可配置response header时，才用此方法下载图片
 */
- (void)setImageWithString:(NSString *)imageName placeholderImageName:(NSString *)placeholderImageName {
    if (imageName) {
        // 默认图片
        UIImage *pImage = placeholderImageName.length > 0 ? [UIImage imageNamed:placeholderImageName] : nil;
        
        // 如果是头像需要单独处理缓存
        if (![imageName hasSuffix:@"jpg"] &&
            ![imageName hasSuffix:@"png"] &&
            ![imageName hasSuffix:@"gif"] &&
            ![imageName hasSuffix:@"jpeg"]) {
            @weakify(self)
            [[SDImageCache sharedImageCache] queryDiskCacheForKey:imageName done:^(UIImage *image, SDImageCacheType cacheType) {
                @strongify(self)
                
                // 设置缓存图片
                self.image = image ? image : pImage;
                
                // 下载图片
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
                    
                    NSURL *url = [NSURL URLWithString:imageName];
                    NSData *imageData = [NSData dataWithContentsOfURL:url];
                    UIImage *image = [UIImage imageWithData:imageData];
                    if (image) {
                        dispatch_async(dispatch_get_main_queue(), ^{
                            self.image = image;
                            // 缓存图片
                            [[SDImageCache sharedImageCache] storeImage:self.image forKey:imageName toDisk:YES];
                        });
                    }
                });
            }];
            
            return;
        }
        
        [self sd_setImageWithURL:[NSURL URLWithString:imageName] placeholderImage:pImage];
    } else {
        self.image = placeholderImageName.length ? [UIImage imageNamed:placeholderImageName] : nil;
    }
}

#pragma mark -

- (void)setImageWithString:(NSString *)imageName {
    [self setImageWithString:imageName placeholder:nil];
}

- (void)setImageWithString:(NSString *)imageName placeholder:(NSString *)placeholder {
    UIImage *placeholderImg = placeholder.length > 0 ? [UIImage imageNamed:placeholder] : [AppTheme placeholderImage];
    if (imageName && imageName.length) {
        [self sd_setImageWithURL:[NSURL URLWithString:imageName] placeholderImage:placeholderImg options:SDWebImageRefreshCached];
    } else {
        [self sd_setImageWithURL:nil placeholderImage:placeholderImg];
    }
}

- (void)setImageWithPhoto:(PHOTO *)photo {
    [self setImageWithPhoto:photo placeholder:nil];
}

- (void)setImageWithPhoto:(PHOTO *)photo placeholder:(NSString *)placeholder {
    UIImage *placeholderImg = placeholder.length > 0 ? [UIImage imageNamed:placeholder] : [AppTheme placeholderImage];
    if ( photo == nil ||
        ![photo isKindOfClass:[PHOTO class]] ||
        (photo.large.length == 0 && photo.thumb.length == 0)) {
        self.image = placeholderImg;
        return;
    }
    
    NSURL *url = nil;
    if (photo.thumb.length) {
        url = [NSURL URLWithString:photo.thumb];
    } else if (photo.large.length) {
        url = [NSURL URLWithString:photo.large];
    }
    [self sd_setImageWithURL:url placeholderImage:self.image?:placeholderImg];
}

- (void)setImageWithPhoto:(PHOTO *)photo placeholderImage:(UIImage *)placeholderImage {
    if ( photo == nil ||
        ![photo isKindOfClass:[PHOTO class]] ||
        (photo.large.length == 0 && photo.thumb.length == 0)) {
        self.image = placeholderImage;
        return;
    }
    
    NSURL *url = nil;
    if (photo.thumb.length) {
        url = [NSURL URLWithString:photo.thumb];
    } else if (photo.large.length) {
        url = [NSURL URLWithString:photo.large];
    }
    
    if (placeholderImage == nil) {
        placeholderImage = [AppTheme placeholderImage];
    }
    
    [self sd_setImageWithURL:url placeholderImage:placeholderImage];
}

@end

@implementation UIButton (PHOTO)

- (void)setImageWithPhoto:(PHOTO *)photo {
    [self setImageWithPhoto:photo forState:UIControlStateNormal placeholder:nil];
}

- (void)setImageWithPhoto:(PHOTO *)photo placeholderImage:(UIImage *)placeholderImage {
    [self setImageWithPhoto:photo forState:UIControlStateNormal placeholder:placeholderImage];
}

- (void)setImageWithPhoto:(PHOTO *)photo forState:(UIControlState)state placeholder:(UIImage *)placeholder {
    if (photo == nil ||
        ![photo isKindOfClass:[PHOTO class]] ||
        (photo.large.length == 0 && photo.thumb.length == 0)) {
        return;
    }
    
    NSURL *url = nil;
    if (photo.thumb.length) {
        url = [NSURL URLWithString:photo.thumb];
    } else if (photo.large.length) {
        url = [NSURL URLWithString:photo.large];
    }
    
//    UIImage *placeholderImg = placeholder.length > 0 ? [UIImage imageNamed:placeholder] : nil;
    [self sd_setImageWithURL:url forState:state placeholderImage:placeholder options:SDWebImageRefreshCached]; // 头像
}

@end

