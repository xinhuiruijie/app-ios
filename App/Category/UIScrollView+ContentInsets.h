//
//  UIScrollView+ContentInsets.h
//  MotherPlanet
//
//  Created by GeekZooStudio on 2018/4/19.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (ContentInsets)
@property (nonatomic, assign) CGFloat contentInsets_top;
@property (nonatomic, assign) CGFloat contentInsets_left;
@property (nonatomic, assign) CGFloat contentInsets_bottom;
@property (nonatomic, assign) CGFloat contentInsets_right;
@end
