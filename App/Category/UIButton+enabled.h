//
//  UIButton+enabled.h
//  MotherPlanet
//
//  Created by liuyadi on 2018/1/19.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIButton (enabled)

- (void)disableSelf;
- (void)enableSelf;
- (void)enableSelfDelay;

@end
