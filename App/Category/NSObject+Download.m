//
//  NSObject+Download.m
//  MotherPlanet
//
//  Created by liuyadi on 2018/1/17.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "NSObject+Download.h"

@implementation NSObject (Download)

- (void)imageWithUrl:(NSString *)url completion:(void(^)(UIImage * image))then {
    if (![self compareImageState:url]) {
        [self downloadPhoto:url completion:^(UIImage *image) {
            if (image) {
                then(image);
            } else {
                then(nil);
            }
        }];
    } else {
        UIImage * image = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:url];
        then(image);
    }
}

- (void)downloadPhoto:(NSString *)url completion:(void(^)(UIImage * image))then
{
    if ( url && url.length )
    {
        [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:url] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
            
        } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
            if ( image )
            {
                [[SDImageCache sharedImageCache] storeImage:image forKey:url];
                then(image);
            } else {
                then(nil);
            }
        }];
    }
}

- (BOOL)compareImageState:(NSString *)url
{
    if ([[SDImageCache sharedImageCache] imageFromDiskCacheForKey:url])
    {
        return YES;
    }
    
    return NO;
}

@end
