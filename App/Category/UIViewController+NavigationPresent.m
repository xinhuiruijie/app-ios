//
//  UIViewController+NavigationPresent.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/2/16.
//  Copyright © 2017年 GeekZooStudio. All rights reserved.
//

#import "UIViewController+NavigationPresent.h"
#import "MPNavigationController.h"

@implementation UIViewController (NavigationPresent)

- (void)presentNavigationController:(UIViewController *)controller {
    MPNavigationController *nav = [[MPNavigationController alloc] initWithRootViewController:controller];
    [self.navigationController presentViewController:nav animated:YES completion:nil];
}

- (void)closeController {
    int n = (int)[self.navigationController.childViewControllers count];
    if ( n > 1 ) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

@end
