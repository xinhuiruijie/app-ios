//
//  AppDelegate.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/11/16.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "AppDelegate.h"
#import "AppRealm.h"
#import "AppService.h"
#import "AppShare.h"
#import "ConfigUpdate.h"
#import "MPRootViewController.h"
#import "VideoInfoController.h"
#import "NewTopicVideoController.h"
#import "TopicVideoListController.h"
#import "AppPusher.h"
#import "AppLaunchAnimation.h"
#import "AppVersionUpdate.h"
#import "AppAnalytics.h"
#import "AppBugly.h"
#import "AppSplash.h"

@interface AppDelegate ()

@property (nonatomic, assign) UIBackgroundTaskIdentifier backgroundTask;
@property (nonatomic, assign) Boolean inBackground;

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [STISystemInfo everLaunched];   // 同步一下当前的加载次数
    
    [AppBugly setup];
    [AppAnalytics setup];
    
    self.backgroundTask = UIBackgroundTaskInvalid;
    self.inBackground = YES;
    [self setupUserAgent];
    
    // 初始化Realm
    [AppRealm setup];
    // 初始化deeplink
    [DeepLink powerOn];
    
    [AppConfig setup];
    
    // 启动动画
    [AppLaunchAnimation setup];
    
    // 闪屏
    [[AppSplash sharedInstance] setup];

    //第三方登录、分享
    [[ConfigUpdate sharedInstance] updateConfig];
    
    //推送
    [AppPusher setup];
    
    // 版本更新
    [AppVersionUpdate versionAppUpdate];
    
    [AppService application:application didFinishLaunchingWithOptions:launchOptions];
    
    self.window.rootViewController = [MPRootViewController sharedInstance];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    return YES;
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey,id> *)options {
    [DeepLink handleURL:url withCompletion:nil];
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    [self extendBackgroundRunningTime];
    _inBackground = YES;
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


- (UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(nullable UIWindow *)window {
//    UIViewController * vc = [MPRootViewController sharedInstance].currentViewController;
    UIViewController * vc = [self topViewController];

    if ([vc isKindOfClass:[VideoInfoController class]]) {
        VideoInfoController *board = (VideoInfoController *)vc;
        if (board.isLandscape) {
            return UIInterfaceOrientationMaskLandscape;
        } else {
            if (board.allowOritation) {
                return UIInterfaceOrientationMaskAllButUpsideDown;
            } else {
                return UIInterfaceOrientationMaskPortrait;
            }
        }
    }
    
    if ([vc isKindOfClass:[NewTopicVideoController class]]) {
        NewTopicVideoController *board = (NewTopicVideoController *)vc;
        if (board.isLandscape) {
            return UIInterfaceOrientationMaskLandscape;
        } else {
            if (board.allowOritation) {
                return UIInterfaceOrientationMaskAllButUpsideDown;
            } else {
                return UIInterfaceOrientationMaskPortrait;
            }
        }
    }
    
    if ([vc isKindOfClass:[TopicVideoListController class]]) {
        TopicVideoListController *board = (TopicVideoListController *)vc;
        if (board.isLandscape) {
            return UIInterfaceOrientationMaskLandscape;
        } else {
            if (board.allowOritation) {
                return UIInterfaceOrientationMaskAllButUpsideDown;
            } else {
                return UIInterfaceOrientationMaskPortrait;
            }
        }
    }
    
    return UIInterfaceOrientationMaskPortrait;
}


- (void)extendBackgroundRunningTime {
    if (self.backgroundTask != UIBackgroundTaskInvalid) {
        // if we are in here, that means the background task is already running.
        // don't restart it.
        return;
    }
    
    self.backgroundTask = [[UIApplication sharedApplication] beginBackgroundTaskWithName:@"RegionRangingTask" expirationHandler:^{
        [[UIApplication sharedApplication] endBackgroundTask:_backgroundTask];
        self.backgroundTask = UIBackgroundTaskInvalid;
    }];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        while (true) {
            [NSThread sleepForTimeInterval:1];
        }
        
    });
}

- (void)extendRunningTime {
    if (self.inBackground) {
        [self extendBackgroundRunningTime];
    }
}

- (void)setupUserAgent {
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectZero];
    
    NSString *secretAgent = [webView stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
    
    NSString *newUagent = [NSString stringWithFormat:@"%@ mplanet/%@",secretAgent,[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"]];
    
    [[NSUserDefaults standardUserDefaults] registerDefaults:@{@"UserAgent": newUagent}];
}


/** 监听音频远程事件 */
- (void)remoteControlReceivedWithEvent:(UIEvent *)event {
    switch (event.subtype) {
        case UIEventSubtypeRemoteControlPlay: // 播放
            [[AudioPlayManager sharedInstance] play];
            break;
        case UIEventSubtypeRemoteControlPause: // 暂停
            [[AudioPlayManager sharedInstance] pause];
            break;
        case UIEventSubtypeRemoteControlStop:
            [[AudioPlayManager sharedInstance] stop];
            break;
        case UIEventSubtypeRemoteControlNextTrack: // 下一首
            [[AudioPlayManager sharedInstance] next]; // 控制播放下一首
            break;
        case UIEventSubtypeRemoteControlPreviousTrack: // 上一首
            [[AudioPlayManager sharedInstance] previous]; // 控制播放上一首
            break;
        default:
            break;
    }
}

@end
