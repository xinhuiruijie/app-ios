//
//  AppLaunchAnimation.h
//  MotherPlanet
//
//  Created by liuyadi on 2018/1/16.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppLaunchAnimation : NSObject

+ (void)setup;

@end
