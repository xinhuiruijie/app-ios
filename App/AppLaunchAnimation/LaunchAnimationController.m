//
//  LaunchAnimationController.m
//  MotherPlanet
//
//  Created by liuyadi on 2018/1/16.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "LaunchAnimationController.h"

@interface LaunchAnimationController ()

@property (nonatomic, strong) UIWindow * window;
@property (weak, nonatomic) IBOutlet UIImageView *animatedImage;

@property (nonatomic, assign) int imageIndex;
@property (nonatomic, strong) NSTimer *animatedTimer;

@end

@implementation LaunchAnimationController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"LaunchAnimation"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.imageIndex = 2;
}

- (void)dealloc {
    [self.animatedTimer invalidate];
    self.animatedTimer = nil;
    self.animatedImage.image = nil;
}

- (IBAction)skipGuideAction:(id)sender {
    [self hide];
}

#pragma mark -

- (void)show
{
    if ( self.window == nil )
    {
        UIWindow * window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        window.windowLevel = UIWindowLevelAlert;
        window.rootViewController = self;
        self.window = window;
    }
    self.window.hidden = NO;

    self.animatedTimer = [NSTimer scheduledTimerWithTimeInterval:0.0632 target:self selector:@selector(setAnimatedImages) userInfo:nil repeats:YES];
    [self performSelector:@selector(hide) withObject:nil afterDelay:11.0];
}

- (void)setAnimatedImages {
    if (self.imageIndex >= 176) {
        [self.animatedTimer invalidate];
        self.animatedTimer = nil;
        self.animatedImage.image = nil;
        return;
    }
    NSString *imageName = [NSString stringWithFormat:@"guide_%d.png", self.imageIndex];
    NSString *path = [[NSBundle mainBundle] pathForResource:imageName ofType:nil];
    UIImage *image = [UIImage imageWithContentsOfFile:path];
    self.animatedImage.image = image;
    self.imageIndex ++;
}

- (void)hide
{
    [self.animatedTimer invalidate];
    self.animatedTimer = nil;
    self.animatedImage.image = nil;
    
    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hide) object:nil];
    [self hideAnimated:YES];
}

- (void)hideAnimated:(BOOL)animated
{
    [UIView animateWithDuration:animated ? 0.3 : 0 animations:^{
        self.window.transform = CGAffineTransformMakeScale(1.5, 1.5);
        self.window.alpha = 0;
    } completion:^(BOOL finished) {
        self.animatedImage.animationImages = nil;
        self.animatedImage = nil;
        self.window.hidden = YES;
        self.window.alpha = 1;
        self.window.transform = CGAffineTransformIdentity;
        [self.window removeFromSuperview];
        [[NSNotificationCenter defaultCenter] postNotificationName:APP_LAUNCH_ANIMATION_HIDE object:nil];
    }];
}
@end
