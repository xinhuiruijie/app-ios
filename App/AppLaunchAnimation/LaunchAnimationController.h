//
//  LaunchAnimationController.h
//  MotherPlanet
//
//  Created by liuyadi on 2018/1/16.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LaunchAnimationController : UIViewController

- (void)show;
- (void)hide;
- (void)hideAnimated:(BOOL)animated;

@end
