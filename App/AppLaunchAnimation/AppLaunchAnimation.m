//
//  AppLaunchAnimation.m
//  MotherPlanet
//
//  Created by liuyadi on 2018/1/16.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "AppLaunchAnimation.h"
#import "LaunchAnimationController.h"

@implementation AppLaunchAnimation

+ (void)setup {
    if (![STISystemInfo everLaunched]) {
        LaunchAnimationController *launchAnimation = [LaunchAnimationController spawn];
        [launchAnimation show];
    }
}

@end
