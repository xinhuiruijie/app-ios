//
//  AppConfig.m
//  bbm2
//
//  Created by PURPLEPENG on 5/8/16.
//  Copyright © 2016 PURPLEPENG. All rights reserved.
//

#import "AppConfig.h"
#import "NetworkReachabilityManager.h"

NSString * const APP_SPLASH_HIDE = @"kAppSplashHide";
NSString * const APP_LAUNCH_ANIMATION_HIDE = @"kAppLaunchAnimationHide";

#if APP_DEVELOPMENT
// 开发环境
//NSString * const kAppServerAPIURL = @"http://api.motherplanet.dev.geek-zoo.cn/";
// 测试环境
NSString * const kAppServerAPIURL = @"http://api.wtuan.top/";

NSString * const kAppUploadImageBaseURL = @"http://api.wtuan.top";
NSString * const kAppUploadImageApiURL = @"/v1/api.photo.upload";

// GeekZooJPush测试KEY
NSString * const JPUSH_APP_KEY = @"9bc79469f96cdf13cf341fbb";

#else

NSString * const kAppServerAPIURL = @"http://api.motherplanet.cn";
// TODO: 补充正式环境信息
NSString * const kAppUploadImageBaseURL = @"http://api.motherplanet.cn";
NSString * const kAppUploadImageApiURL = @"/v1/api.photo.upload";

// 母星系JPush正式KEY
NSString * const JPUSH_APP_KEY = @"8400f34f289340522af806e7";

#endif

NSString * const APP_ENCRYPTION_KEY = @"8UfkRWocQr6vjgv9";
NSString * const TRACE_ENCRYPTION_KEY = @"7yaYA368XgqJc4X7";

#pragma mark -

@implementation AppConfig

@def_singleton(AppConfig)

+ (void)setup {
    // 初始化
    [[AFNetworkReachabilityManager sharedManager] startMonitoring];
    [NetworkReachabilityManager sharedInstance];
    
    // 第一次安装APP，默认自动播放模式为“仅WIFI”
    if (![STISystemInfo everLaunched]) {
        [SettingModel sharedInstance].autoPlay = YES;
        [SettingModel sharedInstance].defaultQuality = VIDEO_QUALITY_HD;
    }
}

- (NSString *)scheme
{
    return @"mplanet";
}

+ (NSString *)schemeWithAction:(NSString *)action
{
    return [[AppConfig sharedInstance].scheme stringByAppendingString:action];
}

+ (NSString *)videoSrtCachePath
{
    return [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0] stringByAppendingString:@"/srts"];
}

+ (void)setupConfigService:(void (^)(id data))block {
    ConfigModel * model = [ConfigModel sharedInstance];
    
    __weak ConfigModel * weakModel = model;
    model.whenUpdated = ^( STIHTTPResponseError * e ){
        if ( e == nil )
        {
            if ( block )
            {
                block(weakModel);
            }
        }
    };
    
    [model refresh];
}

#pragma mark -

+ (BOOL)isShowQQ {
    if ([AppConfig QQSharedShow] && [TencentOAuth iphoneQQInstalled]) {
        return YES;
    }
    return NO;
}

+ (BOOL)isShowWechat {
    if ([AppConfig wechatSharedShow] && [WXApi isWXAppInstalled]) {
        return YES;
    }
    return NO;
}

+ (BOOL)isShowWeibo {
    if ([AppConfig weiboSharedShow] && [WeiboSDK isWeiboAppInstalled]) {
        return YES;
    }
    return NO;
}

+ (BOOL)QQSharedShow {
    OAUTH_CONFIG *config = [ConfigModel sharedInstance].item;
    if (config.qq_app) {
        if (config.qq_app.app_id && config.qq_app.app_id.length && config.qq_app.app_secret && config.qq_app.app_secret.length) {
            return YES;
        }
    }
    return NO;
}

+ (BOOL)wechatSharedShow {
    OAUTH_CONFIG *config = [ConfigModel sharedInstance].item;
    if (config.wechat_app) {
        if (config.wechat_app.app_id && config.wechat_app.app_id.length && config.wechat_app.app_secret && config.wechat_app.app_secret.length) {
            return YES;
        }
    }
    return NO;
}

+ (BOOL)weiboSharedShow {
    OAUTH_CONFIG *config = [ConfigModel sharedInstance].item;
    if (config.weibo_app) {
        if (config.weibo_app.app_id && config.weibo_app.app_id.length && config.weibo_app.app_secret && config.weibo_app.app_secret.length) {
            return YES;
        }
    }
    return NO;
}

@end
