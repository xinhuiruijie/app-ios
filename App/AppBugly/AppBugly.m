//
//  AppBugly.m
//  MotherPlanet
//
//  Created by liuyadi on 2018/1/30.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "AppBugly.h"
#import <Bugly/Bugly.h>

@implementation AppBugly

+ (void)setup {
    [Bugly startWithAppId:@"0b191cc098"];
}

@end
