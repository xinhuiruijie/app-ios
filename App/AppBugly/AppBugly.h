//
//  AppBugly.h
//  MotherPlanet
//
//  Created by liuyadi on 2018/1/30.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface AppBugly : NSObject

+ (void)setup;

@end
