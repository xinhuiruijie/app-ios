//
//  CYWXChatShared_Config.h
//  WechatPayDemo
//
//  Created by Chenyun on 15/6/12.
//  Copyright (c) 2015年 geek-zoo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WXChatShared_Config : NSObject

@singleton( WXChatShared_Config )

@property (nonatomic, retain) NSString *	appId;
@property (nonatomic, retain) NSString *	appKey;
@property (nonatomic, retain) NSString *	payUrl;

@end