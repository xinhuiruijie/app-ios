//
//  WXChat.m
//  CYShared
//
//  Created by Chenyun on 14/11/20.
//  Copyright (c) 2014年 geek-zoo. All rights reserved.
//

#import "WXChatShared.h"

#pragma mark -

@interface WXChatShared ()

@property (nonatomic, strong) NSNumber *		errorCode;
@property (nonatomic, strong) NSString *		errorDesc;

- (void)openWeixin;
- (void)openStore;

- (void)clearPost;
- (void)clearError;

@end

@implementation WXChatShared

@def_singleton( WXChatShared )

#pragma mark -

+ (void)load
{
	[AppService addService:[self sharedInstance]];
}

- (void)powerOn
{
	[WXApi registerApp:self.appId];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
	if ( url )
	{
		if ( [sourceApplication hasPrefix:@"com.tencent.xin"] )
		{
            [WXApi handleOpenURL:url delegate:self];
		}
	}

	return YES;
}

- (BOOL)application:(UIApplication *)application handleOpenURL:(NSURL *)url {
    return [WXApi handleOpenURL:url delegate:self];
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options {
    return [WXApi handleOpenURL:url delegate:self];
}

#pragma mark -

- (WXChatShared_Config *)config
{
    return [WXChatShared_Config sharedInstance];
}

#pragma mark - 得到用户信息

- (void)getuserInfo
{
	if ( NO == [WXApi isWXAppInstalled] )
	{
		return;
	}

	SendAuthReq* req =[[SendAuthReq alloc ] init];
	req.scope = @"snsapi_userinfo";
	req.state = @"1024";

	// 第三方向微信终端发送一个SendAuthReq消息结构
	[WXApi sendReq:req];
}

#pragma mark -

- (void)shareFriend
{
	[self share:WXSceneSession];
}

- (void)shareTimeline
{
	[self share:WXSceneTimeline];
}

+ (BOOL)isWxAppInstalled
{
	return [WXApi isWXAppInstalled];
}

- (void)share:(enum WXScene)scene
{
	if ( NO == [WXApi isWXAppInstalled] )
	{
        [[[UIAlertView alloc] initWithTitle:@"温馨提示"
                                    message:@"您尚未安装微信客户端，无法分享"
                                   delegate:nil
                          cancelButtonTitle:@"确定"
                          otherButtonTitles:nil] show];

		return;
	}

	if ( self.post.photo || self.post.thumb || self.post.url )
	{
		SendMessageToWXReq *	req = [[SendMessageToWXReq alloc] init];
		WXMediaMessage *		message = [WXMediaMessage message];

		if ( self.post.thumb )
		{
			NSData * thumbData = nil;

			if ( [self.post.thumb isKindOfClass:[UIImage class]] )
			{
				thumbData = UIImagePNGRepresentation(self.post.thumb);
			}
			else if ( [self.post.thumb isKindOfClass:[NSData class]] )
			{
				thumbData = (NSData *)self.post.thumb;
			}

			[message setThumbData:thumbData];
		}
		
		if ( self.post.url )
		{
			WXWebpageObject * webObject = [WXWebpageObject object];
			
			webObject.webpageUrl = self.post.url;
			
			message.mediaObject = webObject;
		}
	
		message.title = self.post.title && self.post.title.length ? self.post.title : self.post.text;
		message.description = self.post.text;

		req.message = message;
		req.bText = NO;
		req.scene = scene;

		BOOL succeed = [WXApi sendReq:req];

		if ( succeed )
		{
			[self notifyShareBegin];
		}
		else
		{
			[self notifyShareFailed];
		}
	}
	else if ( self.post.text )
	{
		SendMessageToWXReq * req = [[SendMessageToWXReq alloc] init];
		
		if ( self.post.title && self.post.title.length )
		{
			req.text = self.post.title;
		}
		
		if ( self.post.text && self.post.text.length )
		{
			req.text = self.post.text;
		}
		
		req.bText = YES;
		req.scene = scene;
		
		BOOL succeed = [WXApi sendReq:req];
		if ( succeed )
		{
			[self notifyShareBegin];
		}
		else
		{
			[self notifyShareFailed];
		}
	}
	else
	{
		[self notifyShareFailed];
	}

}

- (void)clearPost
{
	[self.post clear];
}

#pragma mark -

- (void)notifyShareBegin
{
	if ( self.whenShareBegin )
	{
		self.whenShareBegin();
	}
	else if ( [ServiceShare sharedInstance].whenShareBegin )
	{
		[ServiceShare sharedInstance].whenShareBegin();
	}
}

- (void)notifyShareSucceed
{
	if ( self.whenShareSucceed )
	{
		self.whenShareSucceed();
	}
	else if ( [ServiceShare sharedInstance].whenShareSucceed )
	{
		[ServiceShare sharedInstance].whenShareSucceed();
	}

	[self clearPost];
}

- (void)notifyShareFailed
{
	if ( self.whenShareFailed )
	{
		self.whenShareFailed();
	}
	else if ( [ServiceShare sharedInstance].whenShareFailed )
	{
		[ServiceShare sharedInstance].whenShareFailed();
	}
	
	[self clearPost];
}

#pragma mark -

- (void)notifyGetUserInfoBegin
{
	if ( self.whenGetUserInfoBegin )
	{
		self.whenGetUserInfoBegin();
	}
	else if ( [ServiceShare sharedInstance].whenGetUserInfoBegin )
	{
		[ServiceShare sharedInstance].whenGetUserInfoBegin();
	}
}

- (void)notifyGetUserInfoSucceed
{
    ACCOUNT * account = [[ACCOUNT alloc] init];
    account.auth_key = self.appKey;
    account.auth_token = self.accessToken;
    account.user_id = self.openid;
    account.expires = self.expiresIn;

    if ( self.whenGetUserInfoSucceed )
    {
        self.whenGetUserInfoSucceed(account);
    }
    else if ( [ServiceShare sharedInstance].whenGetUserInfoSucceed )
    {
        [ServiceShare sharedInstance].whenGetUserInfoSucceed(account);
    }
}

- (void)notifyGetUserInfoFailed
{
	if ( self.whenGetUserInfoFailed )
	{
		self.whenGetUserInfoFailed();
	}
	else if ( [ServiceShare sharedInstance].whenGetUserInfoFailed )
	{
		[ServiceShare sharedInstance].whenGetUserInfoFailed();
	}
}

#pragma mark - WXApiDelegate

- (void)onReq:(BaseReq*)req
{
	
}

- (void)onResp:(BaseResp*)resp
{
	if ( [resp isKindOfClass:[SendMessageToWXResp class]] )
	{
		if ( WXSuccess == resp.errCode )
		{
			[self notifyShareSucceed];
		}
		else if ( WXErrCodeUserCancel == resp.errCode )
		{
			[self notifyShareFailed];
		}
		else
		{
			[self notifyShareFailed];
		}
	}
	else if ( [resp isKindOfClass:[SendAuthResp class]] )
	{
		SendAuthResp *aresp = (SendAuthResp *)resp;

		if ( aresp.errCode== 0)
		{
			self.weiXincode = aresp.code;

			if ( self.weiXincode )
			{
				[self getAccessToken];
			}
			else
			{
				[self notifyGetUserInfoFailed];
			}
		}
		else
		{			
			[self notifyGetUserInfoFailed];
		}
	}
}

-(void)getAccessToken
{
	NSString *url =[NSString stringWithFormat:@"https://api.weixin.qq.com/sns/oauth2/access_token?appid=%@&secret=%@&code=%@&grant_type=authorization_code", self.appId, self.appKey, self.weiXincode];

	dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

		NSURL *zoneUrl = [NSURL URLWithString:url];
		NSString *zoneStr = [NSString stringWithContentsOfURL:zoneUrl encoding:NSUTF8StringEncoding error:nil];
		NSData *data = [zoneStr dataUsingEncoding:NSUTF8StringEncoding];

		dispatch_async(dispatch_get_main_queue(), ^{
			if ( data )
			{
				NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableContainers error:nil];
				
				self.accessToken = [dic objectForKey:@"access_token"];
				self.openid = [dic objectForKey:@"openid"];
				self.expiresIn = [dic objectForKey:@"expires_in"];
				if ( self.accessToken )
				{
					[self notifyGetUserInfoSucceed];
				}
				else
				{
					[self notifyGetUserInfoFailed];
				}
			}
			else
			{
				[self notifyGetUserInfoFailed];
			}
		});
	});
}

#pragma mark -

- (void)openWeixin
{
	[WXApi openWXApp];
}

- (void)openStore
{
#if !TARGET_IPHONE_SIMULATOR
	
	NSString * url = [WXApi getWXAppInstallUrl];
	if ( url && url.length )
	{
		[[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
	}
	
#endif	// #if !TARGET_IPHONE_SIMULATOR
}

- (void)clearError
{
	self.errorCode = nil;
	self.errorDesc = nil;
}

@end
