//
//  WXChat.h
//  CYShared
//
//  Created by Chenyun on 14/11/20.
//  Copyright (c) 2014年 geek-zoo. All rights reserved.
//

#import "ServiceShare.h"
#import "WXApi.h"
#import "WXChatShared_Config.h"

#pragma mark -

@interface WXChatShared : ServiceShare <WXApiDelegate>

@singleton ( WXChatShared )

@property (nonatomic, retain) WXChatShared_Config * config;

@property (nonatomic, retain) NSString *					weiXincode;
@property (nonatomic, retain) NSString *					accessToken;
@property (nonatomic, retain) NSString *					openid;
@property (nonatomic, retain) NSString *					expiresIn;


- (void)shareFriend;
- (void)shareTimeline;

+ (BOOL)isWxAppInstalled;

- (void)getuserInfo;

@end