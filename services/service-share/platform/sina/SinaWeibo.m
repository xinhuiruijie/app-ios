
//  singanWeibo.m
//  CYShared
//
//  Created by Chenyun on 14/11/19.
//  Copyright (c) 2014年 geek-zoo. All rights reserved.
//

#define KAppKey @"735351141"
#define KRedirectURI @"https://api.weibo.com/oauth2/default.html"

#import "SinaWeibo.h"

@implementation SinaWeibo

@def_singleton( SinaWeibo )

#pragma mark -

+ (void)load
{
	[AppService addService:[self sharedInstance]];
}

- (void)powerOn
{
	[WeiboSDK enableDebugMode:YES];
	[WeiboSDK registerApp:self.appKey];

	[self loadCache];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
	[WeiboSDK handleOpenURL:url delegate:self];
	return YES;
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options {
    [WeiboSDK handleOpenURL:url delegate:self];
    return YES;
}

#pragma mark -

- (void)share
{
	if ( [WeiboSDK isWeiboAppInstalled] )
	{
		WBSendMessageToWeiboRequest *request = [WBSendMessageToWeiboRequest requestWithMessage:[self messageToShare]];

		request.userInfo = @{@"ShareMessageFrom": @"SendMessageToWeiboViewController",
							 @"Other_Info_1": [NSNumber numberWithInt:123],
							 @"Other_Info_2": @[@"obj1", @"obj2"],
							 @"Other_Info_3": @{@"key1": @"obj1", @"key2": @"obj2"}};
		[WeiboSDK sendRequest:request];

		return;
	}
	else
	{
		[[[UIAlertView alloc] initWithTitle:@"温馨提示"
								   message:@"您尚未安装新浪微博客户端，无法分享"
								  delegate:nil
						 cancelButtonTitle:@"确定"
						 otherButtonTitles:nil] show];
	}
}

+ (BOOL)isWeiboAppInstalled
{
	return [WeiboSDK isWeiboAppInstalled];
}

#pragma mark - 

- (BOOL)isAuthorized
{
	return self.isExpired && self.accessToken && self.uid;
}

- (BOOL)isExpired
{
	if ( self.expiresIn )
	{
	}

	return YES;
}

#pragma mark - 关注账号

- (void)followWithName:(NSString *)name
{
	if ( [self isAuthorized] )
	{
		[self followName:name];
	}
	else
	{
		WBAuthorizeRequest *request = [WBAuthorizeRequest request];
		request.redirectURI = self.redirectUrl;
		request.scope = @"all";
		request.userInfo = @{@"SSO_From": @"SendMessageToWeiboViewController",
							 @"Other_Info_1": [NSNumber numberWithInt:123],
							 @"Other_Info_2": @[@"obj1", @"obj2"],
							 @"Other_Info_3": @{@"key1": @"obj1", @"key2": @"obj2"}};
		[WeiboSDK sendRequest:request];
	}
}

- (void)followName:(NSString *)name
{
	[self notifyFollowBegin];

	NSDictionary * dict = @{@"screen_name":name,
							@"access_token":_accessToken};

	[WBHttpRequest requestWithURL:@"https://api.weibo.com/2/friendships/create.json"
					   httpMethod:@"POST"
						   params:dict
						 delegate:self
						  withTag:nil];
}

#pragma mark - 得到用户信息

- (void)getUserInfo
{
	if ( [WeiboSDK isWeiboAppInstalled] )
	{
//		if ( [self isAuthorized] )
//		{
//			[self notifyGetUserInfoSucceed];
//		}
//		else
//		{
			WBAuthorizeRequest *request = [WBAuthorizeRequest request];
			request.redirectURI = self.redirectUrl;
			request.scope = @"all";
			request.userInfo = @{@"SSO_From": @"SendMessageToWeiboViewController",
								 @"Other_Info_1": [NSNumber numberWithInt:123],
								 @"Other_Info_2": @[@"obj1", @"obj2"],
								 @"Other_Info_3": @{@"key1": @"obj1", @"key2": @"obj2"}};
			
			[WeiboSDK sendRequest:request];
//		}
	}
	else
	{
		NSLog(@"请安装新浪微博客户端");
	}
}

- (WBMessageObject *)messageToShare
{
	WBMessageObject *message = [WBMessageObject message];

	NSString * text = @"";

	if ( self.post.title && self.post.title.length )
	{
		text = self.post.title;
	}

	if ( self.post.text && self.post.text.length )
	{
		text = [NSString stringWithFormat:@"%@\r%@", text, self.post.text];
	}

	if ( self.post.url && self.post.url.length )
	{
//        NSString * shareText = [NSString stringWithFormat:@"%@ %@", text, self.post.url];
//        if (shareText.length > 140) {
//            NSInteger index = (140 - self.post.url.length);
//            if (index >= 0) {
//                shareText = [shareText substringToIndex:index];
//                text = [NSString stringWithFormat:@"%@ %@", shareText, self.post.url];
//            }
//        } else {
            text = [NSString stringWithFormat:@"%@ %@", text, self.post.url];
//        }
	}

    if ( self.post.photo && ![self.post.photo isKindOfClass:[NSString class]] )
    {
		WBImageObject * imageObject = [WBImageObject object];

		imageObject.imageData = self.post.photo;

		if ( [self.post.photo isKindOfClass:[UIImage class]] )
		{
			imageObject.imageData = UIImagePNGRepresentation( self.post.photo );
		}
		else if ( [self.post.photo isKindOfClass:[NSData class]] )
		{
			imageObject.imageData = self.post.photo;
		}

		// 分享图片
		message.imageObject = imageObject;
	}

	// 分享文字
	message.text = text;

	return message;
}

#pragma mark -

- (void)notifyShareBegin
{
	if ( self.whenShareBegin )
	{
		self.whenShareBegin();
	}
	else if ( [ServiceShare sharedInstance].whenShareBegin )
	{
		[ServiceShare sharedInstance].whenShareBegin();
	}
}

- (void)notifyShareSucceed
{
	if ( self.whenShareSucceed )
	{
		self.whenShareSucceed();
	}
	else if ( [ServiceShare sharedInstance].whenShareSucceed )
	{
		[ServiceShare sharedInstance].whenShareSucceed();
	}
}

- (void)notifyShareFailed
{
	[self clearPost];

	if ( self.whenShareFailed )
	{
		self.whenShareFailed();
	}
	else if ( [ServiceShare sharedInstance].whenShareFailed )
	{
		[ServiceShare sharedInstance].whenShareFailed();
	}
}

#pragma mark -

- (void)notifyFollowBegin
{
	if ( self.whenFollowBegin )
	{
		self.whenFollowBegin();
	}
	else if ( [ServiceShare sharedInstance].whenFollowBegin )
	{
		[ServiceShare sharedInstance].whenFollowBegin();
	}
}

- (void)notifyFollowSucceed
{	
	if ( self.whenFollowSucceed )
	{
		self.whenFollowSucceed();
	}
	else if ( [ServiceShare sharedInstance].whenFollowSucceed )
	{
		[ServiceShare sharedInstance].whenFollowSucceed();
	}
}

- (void)notifyFollowFailed
{
	if ( self.whenFollowFailed )
	{
		self.whenFollowFailed();
	}
	else if ( [ServiceShare sharedInstance].whenFollowFailed )
	{
		[ServiceShare sharedInstance].whenFollowFailed();
	}
}

#pragma mark - 

- (void)notifyGetUserInfoBegin
{
	if ( self.whenGetUserInfoBegin )
	{
		self.whenGetUserInfoBegin();
	}
	else if ( [ServiceShare sharedInstance].whenGetUserInfoBegin )
	{
		[ServiceShare sharedInstance].whenGetUserInfoBegin();
	}
}

- (void)notifyGetUserInfoSucceed
{
    ACCOUNT * account = [[ACCOUNT alloc] init];
    account.auth_key = self.appKey;
    account.auth_token = self.accessToken;
    account.user_id = self.uid;
    account.expires = self.expiresIn;

    if ( self.whenGetUserInfoSucceed )
    {
        self.whenGetUserInfoSucceed(account);
    }
    else if ( [ServiceShare sharedInstance].whenGetUserInfoSucceed )
    {
        [ServiceShare sharedInstance].whenGetUserInfoSucceed(account);
    }
}

- (void)notifyGetUserInfoFailed
{
	if ( self.whenGetUserInfoFailed )
	{
		self.whenGetUserInfoFailed();
	}
	else if ( [ServiceShare sharedInstance].whenGetUserInfoFailed )
	{
		[ServiceShare sharedInstance].whenGetUserInfoFailed();
	}
}

#pragma mark - 

- (void)clearPost
{
	[self.post clear];
}

#pragma mark - weiboDelegate

- (void)didReceiveWeiboRequest:(WBBaseRequest *)request
{
	if ([request isKindOfClass:WBProvideMessageForWeiboRequest.class])
	{
	}
}

- (void)didReceiveWeiboResponse:(WBBaseResponse *)response
{
	if ( [response isKindOfClass:WBSendMessageToWeiboResponse.class] )
	{
		if ( WeiboSDKResponseStatusCodeSuccess == response.statusCode )
		{
			[self notifyShareSucceed];
		}
		else
		{
			[self failedDescWithresponse:response];
			[self notifyShareFailed];
		}
	}
	else if ( [response isKindOfClass:WBAuthorizeResponse.class] )
	{
		if ( WeiboSDKResponseStatusCodeSuccess == response.statusCode )
		{
			self.uid = response.userInfo[@"uid"];
			self.accessToken = response.userInfo[@"access_token"];
			//		NSString * date = response.userInfo[@"expires_in"];
			self.expiresIn = response.userInfo[@"expires_in"];//[self asNSString:[NSDate dateWithTimeIntervalSinceNow:date.integerValue]];
			self.remindIn = response.userInfo[@"remind_in"];
			
			[self saveCache];
			
			// 获取信息成功
			[self notifyGetUserInfoSucceed];
		}
		else
		{
			// 获取信息失败
			[self notifyGetUserInfoFailed];
		}
	}
}

#pragma mark -

- (void)request:(WBHttpRequest *)request didFinishLoadingWithResult:(NSString *)result
{
	NSDictionary * resultDic = [self dictionaryWithJsonString:result];

	NSString * error = resultDic[@"error"];

	if ( error && error.length )
	{
		UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:@"关注提示" message:error delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
		[alertView show];
		[self notifyFollowFailed];
	}
	else
	{
		UIAlertView * alertView = [[UIAlertView alloc]initWithTitle:@"关注提示" message:@"成功" delegate:self cancelButtonTitle:@"确定" otherButtonTitles:nil, nil];
		[alertView show];
		[self notifyFollowSucceed];
	}
}

- (void)request:(WBHttpRequest *)request didFailWithError:(NSError *)error;
{
	NSLog(@"请求失败");
}

- (NSDictionary *)dictionaryWithJsonString:(NSString *)jsonString {
	if (jsonString == nil) {
		return nil;
	}
	
	NSData *jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
	NSError *err;
	NSDictionary *dic = [NSJSONSerialization JSONObjectWithData:jsonData
														options:NSJSONReadingMutableContainers
														  error:&err];
	if(err) {
		NSLog(@"json解析失败：%@", err);
		return nil;
	}
	return dic;
}

#pragma mark -

- (void)failedDescWithresponse:(WBBaseResponse *)response
{
	switch (response.statusCode) {
		case WeiboSDKResponseStatusCodeUserCancel:
		{
			NSLog(@"用户取消");
		}
			break;
		case WeiboSDKResponseStatusCodeSentFail:
		{
			NSLog(@"发送失败");
		}
			break;
		case WeiboSDKResponseStatusCodeAuthDeny:
		{
			NSLog(@"授权失败");
		}
			break;
		case WeiboSDKResponseStatusCodeUserCancelInstall:
		{
			NSLog(@"用户取消安装微博客户端");
		}
			break;
		case WeiboSDKResponseStatusCodeUnsupport:
		{
			NSLog(@"不支持的请求");
		}
			break;
		default:
			break;
	}
}

- (NSDate *)asNSDate:(NSString *)date
{
	NSDateFormatter *format = [[NSDateFormatter alloc] init];
	NSLocale *enLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en-US"];
	[format setLocale:enLocale];

	[format setDateFormat:@"yyyy-HH-dd HH:mm:ss"];
	NSDate *dateTime = [format dateFromString:date];

	return dateTime;
}

- (NSString *)asNSString:(NSDate *)date
{
	NSDateFormatter *format = [[NSDateFormatter alloc] init];
	NSLocale *enLocale = [[NSLocale alloc] initWithLocaleIdentifier:@"en-US"];
	[format setLocale:enLocale];

	[format setDateFormat:@"yyyy-HH-dd HH:mm:ss"];
	NSString * dateString = [format stringFromDate:date];
	return dateString;
}

#pragma mark -

- (void)saveCache
{
	[[NSUserDefaults standardUserDefaults] setObject:self.accessToken forKey:@"accessToken"];
	[[NSUserDefaults standardUserDefaults] synchronize];

	[[NSUserDefaults standardUserDefaults] setObject:self.expiresIn forKey:@"expiresIn"];
	[[NSUserDefaults standardUserDefaults] synchronize];

	[[NSUserDefaults standardUserDefaults] setObject:self.remindIn forKey:@"remindIn"];
	[[NSUserDefaults standardUserDefaults] synchronize];

	[[NSUserDefaults standardUserDefaults] setObject:self.uid forKey:@"uid"];
	[[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)loadCache
{
	self.accessToken = [[NSUserDefaults standardUserDefaults] objectForKey:@"accessToken"];
	self.expiresIn   = [[NSUserDefaults standardUserDefaults] objectForKey:@"expiresIn"];
	self.remindIn    = [[NSUserDefaults standardUserDefaults] objectForKey:@"remindIn"];
	self.uid		 = [[NSUserDefaults standardUserDefaults] objectForKey:@"uid"];
}

@end
