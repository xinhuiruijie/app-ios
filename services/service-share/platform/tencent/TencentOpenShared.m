//
//  TencentOpenShared.m
//  CYShared
//
//  Created by Chenyun on 14/11/20.
//  Copyright (c) 2014年 geek-zoo. All rights reserved.
//

#import "TencentOpenShared.h"

@implementation TencentOpenShared

@def_singleton( TencentOpenShared )

#pragma mark -

+ (void)load
{
	[AppService addService:[self sharedInstance]];
}

- (void)powerOn
{
	self.tencentOAuth = [[TencentOAuth alloc] initWithAppId:self.appId andDelegate:self];
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation
{
    if ( [sourceApplication isEqualToString:@"com.tencent.mqq"] ||
        [sourceApplication isEqualToString:@"com.tencent.mipadqq"])
	{
		[QQApiInterface handleOpenURL:url delegate:(id<QQApiInterfaceDelegate>)self];
		[TencentOAuth CanHandleOpenURL:url];
		[TencentOAuth HandleOpenURL:url];
	}
	return YES;
}

- (BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<UIApplicationOpenURLOptionsKey, id> *)options {
    [QQApiInterface handleOpenURL:url delegate:(id<QQApiInterfaceDelegate>)self];
    [TencentOAuth CanHandleOpenURL:url];
    return [TencentOAuth HandleOpenURL:url];
}

#pragma mark - 得到用户信息

- (void)getUserInfo
{
	if ( NO == [TencentOAuth iphoneQQInstalled] )
	{
		return;
	}

	NSArray * permissions = @[kOPEN_PERMISSION_GET_INFO,
							  kOPEN_PERMISSION_GET_USER_INFO,
							  kOPEN_PERMISSION_GET_SIMPLE_USER_INFO
							 ];

	[self.tencentOAuth authorize:permissions inSafari:NO];
}

#pragma mark -

- (void)shareQq
{
	[self share:TencentOpenSenceQQ];
}

- (void)shareQzone
{
	[self share:TencentOpenSenceQZone];
}

+ (BOOL)isTencentAppInstalled
{
	return [TencentOAuth iphoneQQInstalled];
}

#pragma mark -

- (void)share:(TencentOpenSence)scene
{
	//分享到QQ
    if ( NO == [TencentOAuth iphoneQQInstalled] )
    {
        [[[UIAlertView alloc] initWithTitle:@"温馨提示"
                                    message:@"您尚未安装QQ客户端，无法分享"
                                   delegate:nil
                          cancelButtonTitle:@"确定"
                          otherButtonTitles:nil] show];
        return;
    }
	if ( self.post.photo || self.post.thumb || self.post.url )
	{
		NSData * imageData = nil;
		
		if ( [self.post.photo isKindOfClass:[NSData class]] )
		{
			imageData = self.post.photo;
		}
		else if ( [self.post.photo isKindOfClass:[UIImage class]] )
		{
			imageData = UIImageJPEGRepresentation(self.post.photo, 0.6);
        }

		NSString * title = self.post.title;

		if ( title.length > 140 )
		{
			title = [title substringToIndex:140];
		}
		
		NSString * text = self.post.text;
		
		if ( text.length > 140 )
		{
			text = [text substringToIndex:140];
		}

		QQApiNewsObject *newsObj ;

		if ( [self.post.photo isKindOfClass:[NSString class]] )
		{
			newsObj = [QQApiNewsObject objectWithURL:[NSURL URLWithString:self.post.url ? : @""]
											   title:title ? : @"分享"
										 description:text ? : @"分享"
									 previewImageURL:[NSURL URLWithString:self.post.photo]];
        } else {
			newsObj = [QQApiNewsObject objectWithURL:[NSURL URLWithString:self.post.url ? : @""]
											   title:title ? : @"分享"
										 description:text ? : @"分享"
									previewImageData:imageData];
		}

		SendMessageToQQReq *req = [SendMessageToQQReq reqWithContent:newsObj];

		QQApiSendResultCode sent = 0;

		if ( TencentOpenSenceQZone == scene )
		{
			//分享到QZone
			sent = [QQApiInterface SendReqToQZone:req];
		}
		else if ( TencentOpenSenceQQ == scene )
		{
			//分享到QQ
			sent = [QQApiInterface sendReq:req];
		}

		[self handleSendResult:sent];
	}
	else
	{
		[self notifyShareFailed];
	}

}

- (void)handleSendResult:(QQApiSendResultCode)sendResult
{
	switch (sendResult)
	{
		case EQQAPIAPPNOTREGISTED:
		{
			[self notifyShareFailed];
			break;
		}
		case EQQAPIMESSAGECONTENTINVALID:
		case EQQAPIMESSAGECONTENTNULL:
		case EQQAPIMESSAGETYPEINVALID:
		{
			[self notifyShareFailed];
			break;
		}
		case EQQAPIQQNOTINSTALLED:
		{
			[self notifyShareFailed];
			break;
		}
		case EQQAPIQQNOTSUPPORTAPI:
		{
			[self notifyShareFailed];
			break;
		}
		case EQQAPISENDFAILD:
		{
			[self notifyShareFailed];
			break;
		}
		case EQQAPIQZONENOTSUPPORTTEXT:
		case EQQAPIQZONENOTSUPPORTIMAGE:
		{
			[self notifyShareFailed];
			break;
		}
		default:
			break;
	}
}

#pragma mark - 

#pragma mark -

- (void)notifyShareBegin
{
	if ( self.whenShareBegin )
	{
		self.whenShareBegin();
	}
	else if ( [ServiceShare sharedInstance].whenShareBegin )
	{
		[ServiceShare sharedInstance].whenShareBegin();
	}
}

- (void)notifyShareSucceed
{
	if ( self.whenShareSucceed )
	{
		self.whenShareSucceed();
	}
	else if ( [ServiceShare sharedInstance].whenShareSucceed )
	{
		[ServiceShare sharedInstance].whenShareSucceed();
	}
	[self clearPost];
}

- (void)notifyShareFailed
{
	if ( self.whenShareFailed )
	{
		self.whenShareFailed();
	}
	else if ( [ServiceShare sharedInstance].whenShareFailed )
	{
		[ServiceShare sharedInstance].whenShareFailed();
	}
	[self clearPost];
}

- (void)clearPost
{
	[self.post clear];
}

#pragma mark - 

- (void)notifyGetUserInfoBegin
{
	if ( self.whenGetUserInfoBegin )
	{
		self.whenGetUserInfoBegin();
	}
	else if ( [ServiceShare sharedInstance].whenGetUserInfoBegin )
	{
		[ServiceShare sharedInstance].whenGetUserInfoBegin();
	}
}

- (void)notifyGetUserInfoSucceed
{
    ACCOUNT * account = [[ACCOUNT alloc] init];
//    account.vendor = VENDOR_PT;
    account.auth_key = self.appId;
//    account.auth_key = self.appKey;
    account.auth_token = self.tencentOAuth.accessToken;
    account.user_id = self.tencentOAuth.openId;
    NSString *timeSp = [NSString stringWithFormat:@"%ld", (long)[self.tencentOAuth.expirationDate timeIntervalSince1970]];
    account.expires = timeSp;

    if ( self.whenGetUserInfoSucceed )
    {
        self.whenGetUserInfoSucceed(account);
//        self.whenGetUserInfoSucceed(nil);
    }
    else if ( [ServiceShare sharedInstance].whenGetUserInfoSucceed )
    {
        [ServiceShare sharedInstance].whenGetUserInfoSucceed(account);
//        [ServiceShare sharedInstance].whenGetUserInfoSucceed(nil);
    }
}

- (void)notifyGetUserInfoFailed
{
	if ( self.whenGetUserInfoFailed )
	{
		self.whenGetUserInfoFailed();
	}
	else if ( [ServiceShare sharedInstance].whenGetUserInfoFailed )
	{
		[ServiceShare sharedInstance].whenGetUserInfoFailed();
	}
}

#pragma mark - QQApiInterfaceDelegate

- (void)onReq:(QQBaseReq*)req
{
	
}

- (void)onResp:(QQBaseReq*)resp
{
	switch ( resp.type )
	{
		case ESENDMESSAGETOQQRESPTYPE:
		{
			SendMessageToQQResp* sendResp = (SendMessageToQQResp*)resp;

			if ( sendResp.result.integerValue == 0 )
			{
				[self notifyShareSucceed];
			}
			else
			{
				[self notifyShareFailed];
			}
			break;
		}
		default:
		{
			break;
		}
	}
}

- (void)isOnlineResponse:(NSDictionary *)response
{
	
}

- (void)tencentDidLogin
{
	if ( self.tencentOAuth.accessToken && self.tencentOAuth.accessToken.length )
	{
		// 获取用户具体信息
		[self.tencentOAuth getUserInfo];
	}
	else
	{
		[self  notifyGetUserInfoFailed];
	}
}

// 没有网络
- (void)tencentDidNotNetWork
{
	[self  notifyGetUserInfoFailed];
}

// 登录失败  用户取消
- (void)tencentDidNotLogin:(BOOL)cancelled
{
	if ( cancelled )
	{
		[self  notifyGetUserInfoFailed];
	}
	else
	{		
		[self  notifyGetUserInfoFailed];
	}
}

// 获取用户具体信息的回调
- (void)getUserInfoResponse:(APIResponse*) response
{
	if ( response.detailRetCode == kOpenSDKErrorSuccess )
	{
		self.responseDic = response.jsonResponse;
		[self  notifyGetUserInfoSucceed];
	}
	else
	{
		[self  notifyGetUserInfoFailed];
	}
}

@end
