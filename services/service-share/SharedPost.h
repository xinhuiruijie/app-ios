//
//  Service_Post.h
//  CYShared
//
//  Created by Chenyun on 14/11/19.
//  Copyright (c) 2014年 geek-zoo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SharedPost : NSObject
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSString * text;
@property (nonatomic, strong) id		 photo;
@property (nonatomic, strong) id		 thumb;
@property (nonatomic, strong) NSString * url;

- (void)copyFrom:(SharedPost *)post;
- (void)clear;

@end