//
//  Service_Post.m
//  CYShared
//
//  Created by Chenyun on 14/11/19.
//  Copyright (c) 2014年 geek-zoo. All rights reserved.
//

#import "SharedPost.h"

@implementation SharedPost

- (void)copyFrom:(SharedPost *)post
{
	if ( post.title )
	{
		self.title = post.title;
	}
	
	if ( post.text )
	{
		self.text = post.text;
	}
	
	if ( post.photo )
	{
		self.photo = post.photo;
	}
	
	if ( post.thumb )
	{
		self.thumb = post.thumb;
	}
	
	if ( post.url )
	{
		self.url = post.url;
	}
}

- (void)clear
{
	self.title = nil;
	self.text = nil;
	self.photo = nil;
	self.thumb = nil;
	self.url = nil;
}
@end
