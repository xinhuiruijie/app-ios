//
//  ServiceShare.m
//  CYShared
//
//  Created by Chenyun on 14/11/20.
//  Copyright (c) 2014年 geek-zoo. All rights reserved.
//

#import "ServiceShare.h"

@interface ServiceShare ()
@end

@implementation ServiceShare

@def_singleton( ServiceShare )

- (void)dealloc
{
	self.whenShareBegin		= nil;
	self.whenShareSucceed	= nil;
	self.whenShareFailed	= nil;
	self.whenShareCancelled	= nil;

	self.whenFollowBegin	 = nil;
	self.whenFollowSucceed	 = nil;
	self.whenFollowFailed	 = nil;
	self.whenFollowCancelled = nil;

	self.whenGetUserInfoBegin     = nil;
	self.whenGetUserInfoSucceed   = nil;
	self.whenGetUserInfoFailed	  = nil;
	self.whenGetUserInfoCancelled = nil;
}

- (void)powerOn
{
	
}

@end
