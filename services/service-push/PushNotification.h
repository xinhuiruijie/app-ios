//
//  PushNotification.h
//  yichao
//
//  Created by Chenyun on 15/5/12.
//  Copyright (c) 2015年 geek-zoo. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 {
	 "aps": {
		 "alert": "Testing.. (0)",
		 "badge": 1,
		 "sound": "default"
	 },
	 "content": {
		 "type": 2,
		 "url": "gaibianjia://plans/17"
	 }
 }
 */

@interface PushNotification : NSObject

@property (nonatomic, assign) BOOL				inApp;

@property (nonatomic, retain) NSDate *			date;
@property (nonatomic, retain) NSString *		alert;
@property (nonatomic, retain) NSString *		sound;
@property (nonatomic, retain) NSDictionary *	userInfo;

@end