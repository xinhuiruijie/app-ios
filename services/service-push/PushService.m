//
//  PushService.m
//  gaibianjia
//
//  Created by QFish on 6/27/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import "PushService.h"
#import <UserNotifications/UserNotifications.h>

@interface PushService () <UNUserNotificationCenterDelegate>
@property (nonatomic, strong) OUT NSString * token;
@property (nonatomic, strong) OUT NSData * deviceToken;
@end

@implementation PushService

+ (void)registerNotifiaction
{
	// iOS8 下需要使用新的 API
	if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0) {
		UIUserNotificationType myTypes = UIUserNotificationTypeBadge | UIUserNotificationTypeSound | UIUserNotificationTypeAlert;
		
		UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:myTypes categories:nil];
		[[UIApplication sharedApplication] registerUserNotificationSettings:settings];
	} else {
		UIRemoteNotificationType myTypes = UIRemoteNotificationTypeBadge|UIRemoteNotificationTypeAlert|UIRemoteNotificationTypeSound;
		[[UIApplication sharedApplication] registerForRemoteNotificationTypes:myTypes];
	}
}

+ (void)cleanBadge
{
	[[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

+ (void)unregisterNotification
{
	[[UIApplication sharedApplication] unregisterForRemoteNotifications];
}

#pragma mark -

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	[[self class] cleanBadge];

	if ( self.isClose )
	{
		[[self class] unregisterNotification];
	}
	else
	{
		[[self class] registerNotifiaction];
	}

    UILocalNotification * localNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    if ( localNotification )
    {
        NSDictionary * dict = [NSDictionary dictionaryWithObjectsAndKeys:localNotification.userInfo, @"userInfo", [NSNumber numberWithBool:NO], @"inApp", nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LocalNotification" object:dict];
        PERFORM_BLOCK_SAFELY(self.whenGotLocalNofiticaition, [self notificationFromUserInfo:dict]);
    }
    
	NSDictionary * userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
	if ( userInfo ) {
		PERFORM_BLOCK_SAFELY(self.whenGotNofiticaition, [self notificationFromUserInfo:userInfo]);
	}
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 10.0) {
        UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
        center.delegate = self;
        [center requestAuthorizationWithOptions:(UNAuthorizationOptionAlert + UNAuthorizationOptionSound)
                              completionHandler:^(BOOL granted, NSError * _Nullable error) {
                                  // Enable or disable features based on authorization.
                              }];
        [center getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
            
        }];
    }
	
	return YES;
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
	[[self class] cleanBadge];
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
	NSString * token = [deviceToken description];
	token = [token stringByReplacingOccurrencesOfString:@"<" withString:@""];
	token = [token stringByReplacingOccurrencesOfString:@">" withString:@""];
	token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
	self.token = token;
	self.deviceToken = deviceToken;
	
	PERFORM_BLOCK_SAFELY(self.whenGotDeviceToken, nil);
}

- (void)application:(UIApplication *)application didRegisterUserNotificationSettings:(UIUserNotificationSettings *)notificationSettings
{
	[application registerForRemoteNotifications];
}

- (void)application:(UIApplication *)application didFailToRegisterForRemoteNotificationsWithError:(NSError *)error
{
	PERFORM_BLOCK_SAFELY(self.whenGotDeviceToken, error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
	PERFORM_BLOCK_SAFELY(self.whenGotNofiticaition, [self notificationFromUserInfo:userInfo]);
    
	completionHandler(UIBackgroundFetchResultNoData);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
	PERFORM_BLOCK_SAFELY(self.whenGotNofiticaition, [self notificationFromUserInfo:userInfo]);
}

- (void)application:(UIApplication *)application didReceiveLocalNotification:(UILocalNotification *)notification
{
    UIApplicationState state = [UIApplication sharedApplication].applicationState;
    if ( UIApplicationStateInactive == state || UIApplicationStateBackground == state )
    {
        NSDictionary * dict = [NSDictionary dictionaryWithObjectsAndKeys:notification.userInfo, @"userInfo", [NSNumber numberWithBool:NO], @"inApp", nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LocalNotification" object:dict];
    }
    else
    {
        NSDictionary * dict = [NSDictionary dictionaryWithObjectsAndKeys:notification.userInfo, @"userInfo", [NSNumber numberWithBool:YES], @"inApp", nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"LocalNotification" object:dict];
    }
}

#pragma mark -

- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler {
//    UNNotificationContent *content = notification.request.content;
    completionHandler(UNNotificationPresentationOptionAlert);
}

- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)())completionHandler {
    UNNotificationContent *content = response.notification.request.content;
    PERFORM_BLOCK_SAFELY(self.whenGotLocalNofiticaition, [self notificationFromUserInfo:content.userInfo]);
}

#pragma mark - PushNotification

- (PushNotification *)notificationFromUserInfo:(NSDictionary *)userInfo
{
	UIApplicationState state = [UIApplication sharedApplication].applicationState;
	
	PushNotification * n = [[PushNotification alloc] init];
	
	if ( UIApplicationStateInactive == state || UIApplicationStateBackground == state )
	{
		n.inApp = NO;
	}
	else
	{
		n.inApp = YES;
	}
	
	n.alert = [userInfo valueForKeyPath:@"aps.alert"];
	n.sound = [userInfo valueForKeyPath:@"aps.sound"];
    n.userInfo = userInfo;
	
	return n;
}

@end
