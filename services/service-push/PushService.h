//
//  PushService.h
//  gaibianjia
//
//  Created by QFish on 6/27/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AppService.h"
#import "PushNotification.h"

@interface PushService : NSObject<AppService>

@property (nonatomic, assign) IN BOOL isClose;
@property (nonatomic, strong, readonly) OUT NSData * deviceToken;
@property (nonatomic, strong, readonly) OUT NSString * token;

@property (nonatomic, copy) void (^whenGotDeviceToken)(id error);
@property (nonatomic, copy) void (^whenGotNofiticaition)(PushNotification *);
@property (nonatomic, copy) void (^whenGotLocalNofiticaition)(PushNotification *);

+ (void)registerNotifiaction;
+ (void)unregisterNotification;
+ (void)cleanBadge;
- (PushNotification *)notificationFromUserInfo:(NSDictionary *)userInfo;
@end
