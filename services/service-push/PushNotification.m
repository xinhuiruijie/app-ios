//
//  PushNotification.m
//  yichao
//
//  Created by Chenyun on 15/5/12.
//  Copyright (c) 2015年 geek-zoo. All rights reserved.
//

#import "PushNotification.h"

@implementation PushNotification
@synthesize inApp = _inApp;
@synthesize date = _date;
@synthesize alert = _alert;
@synthesize sound = _sound;
@synthesize userInfo = _userInfo;

- (id)init
{
	self = [super init];
	if ( self )
	{
		self.inApp = NO;
		self.date = [NSDate date];
		self.alert = nil;
		self.sound = nil;
		self.userInfo = nil;
	}
	return self;
}

- (NSString *)description
{
	return [self JSONStringRepresentation];
}

@end