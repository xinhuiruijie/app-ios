//
//  AppService.m
//  gaibianjia
//
//  Created by QFish on 6/24/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import "AppService.h"
#import <objc/runtime.h>

@interface AppServiceDelegate : NSProxy

@property (nonatomic, readonly, weak) id middleMan;	 //  the middle man
@property (nonatomic, strong) id origin; // the origin delegate

- (instancetype)initWithMiddleMan:(id)middleMan;

@end

@implementation AppServiceDelegate

- (instancetype)initWithMiddleMan:(id)middleMan;
{
    if (self)
    {
        _middleMan = middleMan;
    }
    
    return self;
}

- (NSInvocation *)_copyInvocation:(NSInvocation *)invocation
{
    NSInvocation *copy = [NSInvocation invocationWithMethodSignature:[invocation methodSignature]];
    NSUInteger argCount = [[invocation methodSignature] numberOfArguments];
    
    for (int i = 0; i < argCount; i++)
    {
        char buffer[sizeof(intmax_t)];
        [invocation getArgument:(void *)&buffer atIndex:i];
        [copy setArgument:(void *)&buffer atIndex:i];
    }
    
    return copy;
}

- (void)forwardInvocation:(NSInvocation *)invocation
{
    if ([self.origin respondsToSelector:invocation.selector])
    {
        [invocation invokeWithTarget:self.origin];
    }
    
    if ([self.middleMan respondsToSelector:invocation.selector])
    {
        NSInvocation *invocationCopy = [self _copyInvocation:invocation];
        [invocationCopy invokeWithTarget:self.middleMan];
    }
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)sel
{
    id result = [self.origin methodSignatureForSelector:sel];
    if (!result) {
        result = [self.middleMan methodSignatureForSelector:sel];
    }
    
    return result;
}

- (BOOL)respondsToSelector:(SEL)aSelector
{
    return ([self.origin respondsToSelector:aSelector]
            || [self.middleMan respondsToSelector:aSelector]);
}

@end

#pragma mark -

@interface AppService ()
@property (nonatomic, strong) AppServiceDelegate * delegate;
@property (nonatomic, strong) NSMutableArray * services;
@end

@implementation AppService

@def_singleton(AppService)

- (instancetype)init
{
	self = [super init];
	if (self) {
		_delegate = [[AppServiceDelegate alloc] initWithMiddleMan:self];
		_services = [NSMutableArray array];
	}
	return self;
}

+ (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	return [[self sharedInstance] application:application didFinishLaunchingWithOptions:launchOptions];
}

+ (void)addService:(id)service
{
	[[self sharedInstance] addService:service];
}

+ (void)removeService:(id)service
{
	[[self sharedInstance] removeService:service];
}

#pragma mark - 

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
	self.delegate.origin = application.delegate;
	application.delegate = (id<UIApplicationDelegate>)self.delegate;
	
	[self.services enumerateObjectsUsingBlock:^(id<AppService> obj, NSUInteger idx, BOOL *stop) {
		if ( [obj respondsToSelector:@selector(application:didFinishLaunchingWithOptions:)] ) {
			[obj application:application didFinishLaunchingWithOptions:launchOptions];
		}
	}];
	
	return YES;
}

- (void)addService:(id)service
{
	[_services addObject:service];
}

- (void)removeService:(id)service
{
	[_services removeObject:service];
}

#pragma mark - Dispatch messages

- (NSInvocation *)_copyInvocation:(NSInvocation *)invocation
{
	NSInvocation *copy = [NSInvocation invocationWithMethodSignature:[invocation methodSignature]];
	NSUInteger argCount = [[invocation methodSignature] numberOfArguments];
	
	for (int i = 0; i < argCount; i++)
	{
		char buffer[sizeof(intmax_t)];
		[invocation getArgument:(void *)&buffer atIndex:i];
		[copy setArgument:(void *)&buffer atIndex:i];
	}
	
	return copy;
}

- (void)forwardInvocation:(NSInvocation *)invocation
{
	[self.services enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		if ([obj respondsToSelector:invocation.selector])
		{
			NSInvocation *invocationCopy = [self _copyInvocation:invocation];
			[invocationCopy invokeWithTarget:obj];
		}
	}];
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)sel
{
	__block id result = nil;
	
	[self.services enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
		result = [obj methodSignatureForSelector:sel];
		*stop = result != nil;
	}];
	
	return result;
}

- (BOOL)respondsToSelector:(SEL)aSelector
{
	__block BOOL couldRespond = NO;
    
	if ( [self.class shouldHandleSelector:aSelector] )
    {
		[self.services enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
			couldRespond = [obj respondsToSelector:aSelector];
			*stop = couldRespond;
		}];
    }
    
	return couldRespond;
}

#pragma mark -

+ (BOOL)shouldHandleSelector:(SEL)aSelector
{
    struct objc_method_description desc = protocol_getMethodDescription(@protocol(UIApplicationDelegate), aSelector, NO, YES);
    return desc.name != NULL;
}

@end
