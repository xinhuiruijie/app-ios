//
//  AppService.h
//  gaibianjia
//
//  Created by QFish on 6/24/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol AppService <NSObject, UIApplicationDelegate>
@end

@interface AppService : NSObject

@singleton(AppService)

// @important 这个函数要在 AppDeleagete 里调用
+ (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions;

+ (void)addService:(id)service;
+ (void)removeService:(id)service;

@end
