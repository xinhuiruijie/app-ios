##service-push-JPush


设置 Search Paths 下的 User Header Search Paths 和 Library Search Paths，比如SDK文件夹（默认为vendor）与工程文件在同一级目录下，则都设置为"$(SRCROOT)/[文件夹名称]"即可。
eg. $(SRCROOT)/app/push/vendor
 
支持iOS版本为10.0以上的版本时需知
* Notification Service Extension证书配置时需要注意BundleID不能与Main Target一致，证书需要单独额外配置。
* 请将Notification Service Extension中的Deployment Target设置为10.0。
* 在XCode7或者更低的版本中删除Notification Service Extension所对应的Target。
* 在XCode7或者更低的版本中请将引入的'UserNotifications.framework'删除。

参考JPush文档

注意事项
* 上线前请检查JPUSH_APP_KEY和JPUSH_ApsForProduction
* JPUSH_ApsForProduction为宏定义，AppStore版本需要改为YES
