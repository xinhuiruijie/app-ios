//
//  JPush.h
//  quchicha
//
//  Created by liuyadi on 15/9/19.
//  Copyright (c) 2015年 Vicky. All rights reserved.
//

#import "PushService.h"

// 设置 Search Paths 下的 User Header Search Paths 和 Library Search Paths，比如SDK文件夹（默认为vendor）与工程文件在同一级目录下，则都设置为"$(SRCROOT)/[文件夹名称]"即可。

// eg. $(SRCROOT)/app/push/vendor

// 创建并配置PushConfig.plist文件 参考JPush文档

@interface JPush : PushService

+ (instancetype)sharedInstance;

- (void)bindTag:(NSString *)tag completion:(void (^)(id error))completion;

- (void)bindWithAlias:(NSString *)alias completion:(void (^)(id error))completion;
- (void)unbindWithAlias:(NSString *)alias completion:(void (^)(id error))completion;

- (BOOL)setBadge:(NSInteger)value;
- (void)resetBadge;

@end
