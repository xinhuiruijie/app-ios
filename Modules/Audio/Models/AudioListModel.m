//
//  AudioListModel.m
//  MotherPlanet
//
//  Created by GeekZooStudio on 2018/4/17.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "AudioListModel.h"

@implementation AudioListModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self loadCache];
    }
    return self;
}

- (void)loadCache {
    NSArray *items = [self loadCacheWithKey:self.cacheKey objectClass:[AUDIO class]];
    
    [self.items removeAllObjects];
    if (items.count) {
        [self.items addObjectsFromArray:items];
    }
}

- (void)saveCache {
    [self saveCache:self.items key:self.cacheKey];
}

#pragma mark -

- (void)refresh {
    V1_API_AUDIO_LIST_API * api = [[V1_API_AUDIO_LIST_API alloc] init];
    
    api.whenUpdated = ^(V1_API_AUDIO_LIST_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        self.loaded = YES;
        if ( error ) {
            PERFORM_BLOCK_SAFELY(self.whenUpdated, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                [self.items removeAllObjects];
                
                [self.items addObjectsFromArray:response.audios];
                [self saveCache];
                PERFORM_BLOCK_SAFELY(self.whenUpdated, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(self.whenUpdated, errors);
            }
        }
    };
    
    [api send];
}


+ (void)reportAudio:(NSString *)audioID withType:(REPORT_TYPE)type completed:(void (^)(STIHTTPResponseError *))completed {
    V1_API_AUDIO_REPORT_API *api = [[V1_API_AUDIO_REPORT_API alloc] init];
    api.req.audio_id = audioID;
    api.req.type = type;
    
    api.whenUpdated = ^(V1_API_AUDIO_REPORT_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if ( error ) {
            PERFORM_BLOCK_SAFELY(completed, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                PERFORM_BLOCK_SAFELY(completed, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(completed, errors);
            }
        }
    };
    
    [api send];
}

@end
