//
//  AudioListModel.h
//  MotherPlanet
//
//  Created by GeekZooStudio on 2018/4/17.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "STIStreamModel.h"

@interface AudioListModel : STIStreamModel

+ (void)reportAudio:(NSString *)audioID withType:(REPORT_TYPE)type completed:(void (^)(STIHTTPResponseError *))completed;

@end
