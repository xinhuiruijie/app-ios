//
//  AudioPlayManager.m
//  MotherPlanet
//
//  Created by GeekZooStudio on 2018/4/19.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "AudioPlayManager.h"
#import <MediaPlayer/MediaPlayer.h>

NSString * const AudioPlayManagerStatusChangedNotification = @"AudioPlayManagerStatusChangedNotification";

@interface AudioPlayManager()
{
    AVPlayer *_avPlayer;
    NSInteger _currentIndex;
    AudioPlayerStatus _status;
    BOOL _isInterruption;
    UIBackgroundTaskIdentifier _bgTaskId;
}
@property (nonatomic, strong) id timeObserver;
@end

@implementation AudioPlayManager
@def_singleton(AudioPlayManager)

- (instancetype)init
{
    self = [super init];
    if (self) {
        _currentIndex = 0;
        
        _avPlayer = [[AVPlayer alloc] init];
        _avPlayer.volume = 1.0;
        
        [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
        [[AVAudioSession sharedInstance] setActive:YES error:nil];
        
        // 将要进入后台
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
        // 进入前台
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
        // 电话中断
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handleInterreption:) name:AVAudioSessionInterruptionNotification object:[AVAudioSession sharedInstance]];
    }
    return self;
}

#pragma mark - notification

- (void)handleInterreption:(NSNotification *)sender {
    if(_isInterruption) {
        [self play];
        _isInterruption = NO;
    } else {
        [self pause];
        _isInterruption = YES;
    }
}

- (void)onAppDidBecomeActive:(NSNotification *)sender {
    
}

- (void)onAppWillResignActive:(NSNotification *)sender {
    // 若需要持续后台播放网络歌曲，需要申请后台任务id，其中的_bgTaskId是后台任务UIBackgroundTaskIdentifier _bgTaskId;
    _bgTaskId = [self backgroundPlayerID:_bgTaskId];
}

- (UIBackgroundTaskIdentifier)backgroundPlayerID:(UIBackgroundTaskIdentifier)backTaskId {
    //设置并激活音频会话类别
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    //允许应用程序接收远程控制
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
    //设置后台任务ID
    UIBackgroundTaskIdentifier newTaskId = UIBackgroundTaskInvalid;
    
    newTaskId = [[UIApplication sharedApplication] beginBackgroundTaskWithExpirationHandler:nil];
    
    if (newTaskId != UIBackgroundTaskInvalid && backTaskId != UIBackgroundTaskInvalid) {
        [[UIApplication sharedApplication] endBackgroundTask:backTaskId];
    }
    return newTaskId;
}

#pragma mark - getting

- (AVPlayer *)player {
    return _avPlayer;
}

- (NSTimeInterval)duration {
    return CMTimeGetSeconds(_avPlayer.currentItem.duration);
}

- (NSTimeInterval)currentTime {
    return CMTimeGetSeconds(_avPlayer.currentItem.currentTime);
}

- (BOOL)isPlaying {
    return _avPlayer.rate != 0.0;
}

- (AudioPlayerStatus)status {
    return _status;
}

- (NSInteger)currentIndex {
    return _currentIndex;
}

#pragma mark - action

// 指定播放某一首
- (void)playWithIndex:(NSInteger)index {
    if (index < 0 || index >= [self.dataSource numberOfAudios]) {
        return;
    }
    _currentIndex = index;
    [self replay];
}

// 播放
- (void)play {
    if (_avPlayer.currentItem.status == AVPlayerItemStatusReadyToPlay) {
        [_avPlayer play];
        [self setupAudioPlayerStatus:AudioPlayerStatusRestartToPlay];
    } else {
        [self replay];
    }
}

// 暂停
- (void)pause {
    [_avPlayer pause];
    [self setupAudioPlayerStatus:AudioPlayerStatusPause];
}

- (void)stop {
    [_avPlayer replaceCurrentItemWithPlayerItem:nil];
    [self setupAudioPlayerStatus:AudioPlayerStatusPause];
}

/// 快进 正数 / 快退 负数
- (void)seekToTime:(NSTimeInterval)time {
    [_avPlayer seekToTime:CMTimeMake(time, 1)];
}

// 下一首
- (void)next {
    _currentIndex++;
    if (_currentIndex >= [self.dataSource numberOfAudios]) {
        // 如果当前是最后一首，就开始播放第一首，实现列表循环
        _currentIndex = 0;
    }
    
    [self replay];
}

// 上一首
- (void)previous {
    _currentIndex--;
    if (_currentIndex < 0) {
        // 如果当前是第一首，就开始播放最后一首，实现列表循环
        _currentIndex = [self.dataSource numberOfAudios] - 1;
    }
    
    [self replay];
}

- (void)replay {
    if (_currentIndex >= 0 && _currentIndex < [self.dataSource numberOfAudios]) {
        AVPlayerItem *nextItem = [self.dataSource audioWithIndex:_currentIndex];
        
        [self currentItemRemoveObserver];
        [_avPlayer replaceCurrentItemWithPlayerItem:nextItem];
        [self currentItemAddObserver];
    }
}

- (void)currentItemRemoveObserver {
    [_avPlayer.currentItem removeObserver:self forKeyPath:@"status"];
    [_avPlayer.currentItem removeObserver:self forKeyPath:@"loadedTimeRanges"];
    [_avPlayer.currentItem removeObserver:self forKeyPath:@"playbackLikelyToKeepUp"];
    [_avPlayer.currentItem removeObserver:self forKeyPath:@"playbackBufferEmpty"];
    [_avPlayer removeTimeObserver:self.timeObserver];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
}

- (void)currentItemAddObserver {
    // 监控状态属性，注意AVPlayer也有一个status属性，通过监控它的status也可以获得播放状态
    [_avPlayer.currentItem addObserver:self forKeyPath:@"status" options:(NSKeyValueObservingOptionOld|NSKeyValueObservingOptionNew) context:nil];
    
    // 监控缓冲加载情况属性
    [_avPlayer.currentItem addObserver:self forKeyPath:@"loadedTimeRanges" options:NSKeyValueObservingOptionOld|NSKeyValueObservingOptionNew context:nil];
    
    // 监控缓冲达到可播放程度了
    [_avPlayer.currentItem addObserver:self forKeyPath:@"playbackLikelyToKeepUp" options:NSKeyValueObservingOptionOld|NSKeyValueObservingOptionNew context:nil];
    
    // 监控缓冲数据的状态-缓冲不足暂停
    [_avPlayer.currentItem addObserver:self forKeyPath:@"playbackBufferEmpty" options:NSKeyValueObservingOptionOld|NSKeyValueObservingOptionNew context:nil];
    
    // 监控时间进度
    @weakify(self)
    @weakify(_avPlayer);
    self.timeObserver = [_avPlayer addPeriodicTimeObserverForInterval:CMTimeMake(1, 1) queue:dispatch_get_main_queue() usingBlock:^(CMTime time) {
        @strongify(self)
        @strongify(_avPlayer);
        
        // 在这里将监听到的播放进度代理出去，对进度条进行设置
        if (self.delegate && [self.delegate respondsToSelector:@selector(updateProgress:total:)]) {
            [self.delegate updateProgress:CMTimeGetSeconds(time) total:CMTimeGetSeconds(_avPlayer.currentItem.duration)];
        }
    }];
    
    // 监控播放完成通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playbackFinished:) name:AVPlayerItemDidPlayToEndTimeNotification object:_avPlayer.currentItem];
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    
    if ([keyPath isEqualToString:@"status"]) {
        AVPlayerItemStatus status = [change[@"new"] integerValue];
        switch (status) {
            case AVPlayerItemStatusReadyToPlay:
                // 开始播放
                [_avPlayer play];
                [self setupAudioPlayerStatus:AudioPlayerStatusReadyToPlay];
                break;
            case AVPlayerItemStatusFailed:
                // 失败暂停播放
                [_avPlayer pause];
                [self setupAudioPlayerStatus:AudioPlayerStatusFailed];
                break;
            case AVPlayerItemStatusUnknown:
                // 未知状态
                [_avPlayer pause];
                [self setupAudioPlayerStatus:AudioPlayerStatusUnknown];
                break;
            default:
                break;
        }
    } else if([keyPath isEqualToString:@"loadedTimeRanges"]) {
        // 缓冲进度
        AVPlayerItem *playerItem = object;
        
        NSArray *array = playerItem.loadedTimeRanges;
        // 本次缓冲时间范围
        CMTimeRange timeRange = [array.firstObject CMTimeRangeValue];
        
        float startSeconds = CMTimeGetSeconds(timeRange.start);
        
        float durationSeconds = CMTimeGetSeconds(timeRange.duration);
        //缓冲总长度
        NSTimeInterval totalBuffer = startSeconds + durationSeconds;
        
        NSLog(@"共缓冲：%.2f",totalBuffer);
        if (self.delegate && [self.delegate respondsToSelector:@selector(updateBufferProgress:)]) {
            [self.delegate updateBufferProgress:totalBuffer];
        }
    } else if ([keyPath isEqualToString:@"playbackBufferEmpty"]) { // 监听播放器在缓冲数据的状态
        // 缓冲不足暂停了
        [self setupAudioPlayerStatus:AudioPlayerStatusBufferEmpty];
        
    } else if ([keyPath isEqualToString:@"playbackLikelyToKeepUp"]) {
        // 缓冲达到可播放程度 由于 AVPlayer 缓存不足就会自动暂停，所以缓冲足了需要手动播放，才能继续播放
        BOOL playbackLikelyToKeepUp = [change[@"new"] integerValue];
        if (playbackLikelyToKeepUp) {
            [_avPlayer play];
            [self setupAudioPlayerStatus:AudioPlayerStatusRestartToPlay];
        }
    }
}

- (void)setupAudioPlayerStatus:(AudioPlayerStatus)status {
    _status = status;
    if ([self.delegate respondsToSelector:@selector(audioPlayerStatusChanged:)]) {
        [self.delegate audioPlayerStatusChanged:status];
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:AudioPlayManagerStatusChangedNotification object:self];
    
    if (status == AudioPlayerStatusReadyToPlay ||
        status == AudioPlayerStatusRestartToPlay) {
        [self setupLockScreenInfo:status];
    }
}

- (void)playbackFinished:(NSNotification *)notifi {
    [self next];
    if ([self.delegate respondsToSelector:@selector(audioPlayerFinish)]) {
        [self.delegate audioPlayerFinish];
    }
}

#pragma mark - MediaPlayer

- (void)setupLockScreenInfo:(AudioPlayerStatus)status {
    // 0.获取当前正在播放的歌曲
    AudioListModel *model = [[AudioListModel alloc] init];
    AUDIO *audio = model.items[_currentIndex];
    
    // 1.获取锁屏界面中心
    MPNowPlayingInfoCenter *playingInfoCenter = [MPNowPlayingInfoCenter defaultCenter];
    
    // 2.设置展示的信息
    NSMutableDictionary *playingInfo = [NSMutableDictionary dictionary];
    // 设置专辑名
    [playingInfo setObject:audio.title forKey:MPMediaItemPropertyAlbumTitle];
    // 设置歌手
    [playingInfo setObject:audio.owner.name forKey:MPMediaItemPropertyArtist];
    // 设置标题
    [playingInfo setObject:audio.title forKey:MPMediaItemPropertyTitle];
    // 设置封面
    [[SDImageCache sharedImageCache] queryDiskCacheForKey:audio.photo.thumb?:audio.photo.large done:^(UIImage *image, SDImageCacheType cacheType) {
        // 设置缓存图片
        if (image) {
            MPMediaItemArtwork *artWork = [[MPMediaItemArtwork alloc] initWithImage:image];
            [playingInfo setObject:artWork forKey:MPMediaItemPropertyArtwork];
        }
    }];
    
    if (status == AudioPlayerStatusRestartToPlay) {
        // 设置歌曲播放的总时长
        [playingInfo setObject:@(audio.duration.integerValue) forKey:MPMediaItemPropertyPlaybackDuration];
        [playingInfo setObject:@(self.currentTime) forKey:MPNowPlayingInfoPropertyElapsedPlaybackTime];
    }
    
    playingInfoCenter.nowPlayingInfo = playingInfo;
    
    // 3.让应用程序可以接受远程事件
    [[UIApplication sharedApplication] beginReceivingRemoteControlEvents];
}

@end
