//
//  AudioPlayManager.h
//  MotherPlanet
//
//  Created by GeekZooStudio on 2018/4/19.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const AudioPlayManagerStatusChangedNotification;

typedef NS_ENUM(NSInteger, AudioPlayerStatus) {
    /// 未知状态
    AudioPlayerStatusUnknown = 0,
    
    /// 加载失败
    AudioPlayerStatusFailed,
    
    /// 加载成功，准备播放，开始缓冲
    AudioPlayerStatusReadyToPlay,
    
    /// 缓冲到足够播放，开始播放
    AudioPlayerStatusRestartToPlay,
    
    /// 缓冲不足导致暂停
    AudioPlayerStatusBufferEmpty,
    
    /// 暂停
    AudioPlayerStatusPause,
};

@protocol AudioPlayManagerDataSource <NSObject>
@required
- (NSInteger)numberOfAudios;
- (AVPlayerItem *)audioWithIndex:(NSInteger)index;
@end

@protocol AudioPlayManagerDelegate <NSObject>
@optional
/// 播放状态
- (void)audioPlayerStatusChanged:(AudioPlayerStatus)status;
/// 播放进度
- (void)updateProgress:(NSTimeInterval)currentTime total:(NSTimeInterval)totalTime;
/// 缓冲进度
- (void)updateBufferProgress:(NSTimeInterval)progress;
/// 播放完后会自动连续播放，delegate无需再手动播放下一首
- (void)audioPlayerFinish;
@end

@interface AudioPlayManager : NSObject
@singleton(AudioPlayManager)

@property (nonatomic, weak) id<AudioPlayManagerDataSource> dataSource;
@property (nonatomic, weak) id<AudioPlayManagerDelegate> delegate;

@property (nonatomic, readonly) AVPlayer *player;
@property (nonatomic, readonly) NSTimeInterval duration;
@property (nonatomic, readonly) NSTimeInterval currentTime;
@property (nonatomic, readonly) BOOL isPlaying;
@property (nonatomic, readonly) NSInteger currentIndex;
@property (nonatomic, readonly) AudioPlayerStatus status;

/// 指定播放某一首
- (void)playWithIndex:(NSInteger)index;
/// 播放
- (void)play;
/// 暂停
- (void)pause;
/// 取消播放
- (void)stop;
/// 下一首
- (void)next;
/// 上一首
- (void)previous;
/// 快进/快退 0 - 
- (void)seekToTime:(NSTimeInterval)time;
@end
