//
//  AudioViewController.m
//  MotherPlanet
//
//  Created by GeekZooStudio on 2018/4/17.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "AudioViewController.h"
#import "AudioListModel.h"
#import "ZLBlurView.h"
#import "AudioViewCell.h"
#import "PopupMenu.h"
#import "FullScreenShareView.h"
#import "DarkShareView.h"
#import "AudioPlayButton.h"

@interface AudioViewController () <AudioPlayManagerDataSource, AudioPlayManagerDelegate, UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, strong) AudioListModel *model;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topBarHeightLC;
@property (weak, nonatomic) IBOutlet ZLBlurView *blurView;
@property (weak, nonatomic) IBOutlet UIImageView *bgImgView;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIButton *moreBtn;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (weak, nonatomic) IBOutlet UIImageView *currentAudioImgView;
@property (weak, nonatomic) IBOutlet UILabel *currentAudioTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentAudioSubtitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *currentAudioSourceLabel;

@property (weak, nonatomic) IBOutlet UISlider *audioPlayProgressSlider;
@property (weak, nonatomic) IBOutlet UILabel *audioPlayCurrentTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *audioPlaySurplusTimeLabel;

@property (weak, nonatomic) IBOutlet AudioPlayButton *playBtn;
@property (weak, nonatomic) IBOutlet UIButton *previousBtn;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;
@property (weak, nonatomic) IBOutlet UIButton *back30secondBtn;
@property (weak, nonatomic) IBOutlet UIButton *push30SecondBtn;

@property (nonatomic, assign) BOOL isProgressSliderValueChanging;

@property (nonatomic, strong) AudioPlayManager *audioManager;
@property (nonatomic, strong) AudioListModel *audioModel;

@property (nonatomic, strong) DarkShareView *smallShareView;
@property (nonatomic, strong) FullScreenShareView *largeShareView;
@property (nonatomic, strong) AlertView *reportTypeAlertView;

// 记录之前的DataSource和delegate，在页面消失时还原
@property (nonatomic, weak) id lastDataSource;
@property (nonatomic, weak) id lastDelegate;
@end

@implementation AudioViewController

+ (instancetype)spawn {
    return [self loadFromNib];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self initModel];
    [self customize];
}

- (void)customize {
    self.topBarHeightLC.constant = TopBarHeight;
    self.titleLabel.text = @"FM爱音斯坦";
    
    self.blurView.toolBar.barStyle = UIBarStyleBlack;
    self.blurView.toolBar.translucent = YES;
    
    [self.audioPlayProgressSlider setThumbImage:[UIImage imageNamed:@"icon_video_small_point"] forState:UIControlStateNormal];
    [self.audioPlayProgressSlider setThumbImage:[UIImage imageNamed:@"icon_video_small_point"] forState:UIControlStateHighlighted];
    
    self.audioPlayProgressSlider.value = 0.0;
    
    self.audioManager = [AudioPlayManager sharedInstance];
    
    self.lastDataSource = self.audioManager.dataSource;
    self.lastDelegate = self.audioManager.delegate;
    
    self.audioManager.dataSource = self;
    self.audioManager.delegate = self;
    
    [self setupInfo:self.audioManager.currentIndex];
    
    [self setupTableView];
    
    if (self.currentAudio) {
        NSInteger shouldPlayIndex = -1;
        for (int i = 0; i < self.audioModel.items.count; i++) {
            AUDIO *audio = self.audioModel.items[i];
            if ([self.currentAudio.id isEqualToString:audio.id]) {
                shouldPlayIndex = i;
                break;
            }
        }
        if (shouldPlayIndex >= 0) {
            [self.audioManager playWithIndex:shouldPlayIndex];
        }
    }
}

- (void)setupTableView {
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.rowHeight = 77;
    
    [self.tableView registerNib:NSStringFromClass([AudioViewCell class])];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    self.audioManager.dataSource = self.lastDataSource;
    self.audioManager.delegate = self.lastDelegate;
    [super viewDidDisappear:animated];
}

- (void)initModel {
    self.audioModel = [[AudioListModel alloc] init];
    @weakify(self)
    self.audioModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        [self dismissTips];
        if (error) {
            [self presentMessage:error.message withTips:@"Load data failed"];
        } else {
            [self.tableView reloadData];
        }
        
    };
    if (self.audioModel.items.count == 0) {
        [self presentLoadingTips:nil];
        [self.audioModel refresh];
    }
}

- (void)setupInfo:(NSInteger)index {
    if (index >= self.audioModel.items.count) return;
    
    AUDIO *audio = self.audioModel.items[index];
    
    // 节目信息
    [self.bgImgView setImageWithPhoto:audio.photo placeholderImage:[AppTheme placeholderImage]];
    [self.currentAudioImgView setImageWithPhoto:audio.photo placeholderImage:[AppTheme placeholderImage]];
    self.currentAudioTitleLabel.text = audio.title?:@"--";
    self.currentAudioSubtitleLabel.text = audio.subtitle?:@"--";
    self.currentAudioSourceLabel.text = [NSString stringWithFormat:@"by%@", audio.owner.name?:@"--"];
    
    // 根据状态设置
    if (self.audioManager.status == AudioPlayerStatusRestartToPlay) {
        self.playBtn.status = AudioPlayButtonStatusPlay;
    } else if (self.audioManager.status == AudioPlayerStatusBufferEmpty ||
               self.audioManager.status == AudioPlayerStatusReadyToPlay) {
        self.playBtn.status = AudioPlayButtonStatusLoading;
    } else {
        self.playBtn.status = AudioPlayButtonStatusPause;
    }
    
    // 播放进度条
    CGFloat value = self.audioManager.player.currentItem ? self.audioManager.currentTime / self.audioManager.duration : 0.0;
    self.audioPlayProgressSlider.value = value;
    
    // 播放进度时间
    self.audioPlayCurrentTimeLabel.text = self.audioManager.currentTime > 0 ? [self timeFormat:self.audioManager.currentTime] : @"00:00";
    self.audioPlaySurplusTimeLabel.text = [NSString stringWithFormat:@"%@", [self timeFormat:audio.duration.integerValue]];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.audioModel.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AudioViewCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([AudioViewCell class]) forIndexPath:indexPath];
    
    cell.isSelected = indexPath.row == self.audioManager.currentIndex;
    cell.isPlaying = self.audioManager.status == AudioPlayerStatusRestartToPlay;
    
    AUDIO *audio = self.audioModel.items[indexPath.row];
    cell.data = audio;
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row != self.audioManager.currentIndex) {
        [self.audioManager playWithIndex:indexPath.row];
        [tableView reloadData];
    } else if (self.audioManager.status == AudioPlayerStatusPause) {
        [self.audioManager play];
        [tableView reloadData];
    }
}

#pragma mark - AudioPlayManagerDataSource

- (NSInteger)numberOfAudios {
    return self.audioModel.items.count;
}

- (AVPlayerItem *)audioWithIndex:(NSInteger)index {
    if (index >= self.audioModel.items.count) return nil;
    
    [self setupInfo:index];
    
    AUDIO *audio = self.audioModel.items[index];
    
    NSURL *url = nil;
    if ([audio.audio_url hasPrefix:@"http://"] || [audio.audio_url hasPrefix:@"https://"]) {
        url = [NSURL URLWithString:audio.audio_url];
    } else {
        NSString *path = [[[NSBundle mainBundle] pathForResource:audio.audio_url ofType:nil] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        url = [NSURL fileURLWithPath:path];
    }
    
    AVPlayerItem *item = [AVPlayerItem playerItemWithURL:url];
    return item;
}

#pragma mark - AudioPlayManagerDelegate

- (void)audioPlayerStatusChanged:(AudioPlayerStatus)status {
    switch (status) {
        case AudioPlayerStatusReadyToPlay:
            NSLog(@"加载成功，准备播放，开始缓冲");
            self.playBtn.status = AudioPlayButtonStatusLoading;
            break;
        case AudioPlayerStatusUnknown:
        case AudioPlayerStatusFailed:
            NSLog(@"加载失败");
            self.playBtn.status = AudioPlayButtonStatusPause;
            break;
        case AudioPlayerStatusRestartToPlay:
            NSLog(@"开始播放");
            self.playBtn.status = AudioPlayButtonStatusPlay;
            break;
        case AudioPlayerStatusPause:
            NSLog(@"暂停");
            self.playBtn.status = AudioPlayButtonStatusPause;
            break;
        case AudioPlayerStatusBufferEmpty:
            NSLog(@"缓冲不足，暂停");
            self.playBtn.status = AudioPlayButtonStatusLoading;
            break;
        default:
            break;
    }
    
    // 播放状态变更，更新列表UI
    [self.tableView reloadData];
}

- (void)updateProgress:(NSTimeInterval)currentTime total:(NSTimeInterval)totalTime; {
    if (!self.isProgressSliderValueChanging) {
        self.audioPlayProgressSlider.value = currentTime / totalTime;
    }
    
    self.audioPlayCurrentTimeLabel.text = [self timeFormat:currentTime];
}

- (NSString *)timeFormat:(NSInteger)secounds {
    NSInteger m = secounds / 60;
    NSInteger s = secounds % 60;
    return [NSString stringWithFormat:@"%02ld:%02ld", m, s];
}

#pragma mark - action

- (IBAction)backBtnAction:(UIButton *)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)moreBtnAction:(UIButton *)sender {
    @weakify(self)
    PopupMenuItem *shareItem = [[PopupMenuItem alloc] initWithTitle:@"分享节目" icon:[UIImage imageNamed:@"icon_more_share"] action:^(id<MenuAbleItem> menu) {
        @strongify(self)
        
        if (self.audioManager.currentIndex >= self.audioModel.items.count) return;
        
        AUDIO *audio = self.audioModel.items[self.audioManager.currentIndex];
        [self shareAudio:audio isFullScreen:NO];
    }];
    
    PopupMenuItem *reportItem = [[PopupMenuItem alloc] initWithTitle:@"内容举报" icon:[UIImage imageNamed:@"icon_more_report"] action:^(id<MenuAbleItem> menu) {
        @strongify(self)
        
        if (self.audioManager.currentIndex >= self.audioModel.items.count) return;
        
        AUDIO *audio = self.audioModel.items[self.audioManager.currentIndex];
        [self reportAudio:audio];
    }];
    
    CGRect fromRect = CGRectMake(sender.x, sender.superview.y + sender.y, sender.width, sender.height);
    [PopupMenu showMenuInView:self.view fromRect:fromRect menuItems:@[shareItem, reportItem]];
}

- (void)seekToTime:(NSTimeInterval)time {
    [self.audioManager seekToTime:time];
}

- (IBAction)audioPlayProgressSliderValueChange:(UISlider *)sender {
    NSLog(@"ValueChange - %f", sender.value);
}
- (IBAction)audioPlayProgressSliderTouchDown:(UISlider *)sender {
    // 开始拖动进度条, 记录，在代理更新进度时判断，解决进度条跳动问题
    self.isProgressSliderValueChanging = YES;
}

- (IBAction)audioPlayProgressSliderTouchUpInside:(UISlider *)sender {
    [self seekToTime:sender.value * self.audioManager.duration];
    self.isProgressSliderValueChanging = NO;
}

- (IBAction)audioPlayProgressSliderTouchUpOutside:(UISlider *)sender {
    [self seekToTime:sender.value * self.audioManager.duration];
    self.isProgressSliderValueChanging = NO;
}

- (IBAction)audioPlayProgressSliderTouchCancel:(UISlider *)sender {
    self.isProgressSliderValueChanging = NO;
}

- (IBAction)back30SecondBtnAction:(id)sender {
    [self seekToTime:self.audioManager.currentTime - 30];
}

- (IBAction)push30SecondBtnAction:(id)sender {
    [self seekToTime:self.audioManager.currentTime + 30];
}

- (IBAction)previousBtnAction:(UIButton *)sender {
    [self.audioManager previous];
}

- (IBAction)nextBtnAction:(id)sender {
    [self.audioManager next];
}

- (IBAction)playBtnAction:(AudioPlayButton *)sender {
    if (self.audioManager.currentIndex >= self.audioModel.items.count) return;
    
    if (sender.status == AudioPlayButtonStatusPlay || sender.status == AudioPlayButtonStatusLoading) {
        [self.audioManager pause];
    } else {
        [self.audioManager play];
    }
}

#pragma mark -

- (void)shareAudio:(AUDIO *)audio isFullScreen:(BOOL)isFullScreen {
//    [AppAnalytics clickEvent:@"click_video_share"];
    [self.view endEditing:YES];
    
    if (isFullScreen) {
        FullScreenShareView *largeShareView = [FullScreenShareView loadFromNib];
        self.largeShareView = largeShareView;
    } else {
        DarkShareView *smallShareView = [DarkShareView loadFromNib];
        self.smallShareView = smallShareView;
    }
    
    @weakify(self)
    if (audio.photo && audio.photo.thumb) {
        [self presentLoadingTips:nil];
        [self imageWithUrl:audio.photo.thumb completion:^(UIImage *image) {
            @strongify(self)
            dispatch_async(dispatch_get_main_queue(), ^{
                [self dismissTips];
//                audio.shareImage = image;
                if (isFullScreen) {
                    self.largeShareView.data = audio;
                    [self.largeShareView show];
                } else {
                    self.smallShareView.data = audio;
                    [self.smallShareView show];
                }
            });
        }];
    } else {
        if (isFullScreen) {
            self.largeShareView.data = audio;
            [self.largeShareView show];
        } else {
            self.smallShareView.data = audio;
            [self.smallShareView show];
        }
    }
}

- (void)reportAudio:(AUDIO *)audio {
    GKZActionSheet *actionSheet = [GKZActionSheet loadFromNib];
    
    self.reportTypeAlertView = [[AlertView alloc] initWithContent:actionSheet type:AlertViewTypeMonospaced];
    actionSheet.data = @[@"垃圾广告营销", @"恶意攻击谩骂", @"淫秽色情信息", @"违法有害信息", @"其它不良信息"];
    
    @weakify(self)
    actionSheet.whenHide = ^(BOOL hide) {
        @strongify(self)
        [self.reportTypeAlertView hide];
    };
    
    actionSheet.whenRegistered = ^(id data, NSInteger index) {
        @strongify(self)
        [self.reportTypeAlertView hide];
        REPORT_TYPE type = index + 1;
        [AudioListModel reportAudio:audio.id withType:type completed:^(STIHTTPResponseError *error) {
            if (error == nil) {
                [self presentMessageTips:@"您的举报我们会在24小时内进行处理"];
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
        }];
    };
    
    [self.reportTypeAlertView showSharedView];
}

#pragma mark -

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

@end
