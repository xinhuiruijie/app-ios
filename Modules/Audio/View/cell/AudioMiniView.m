//
//  AudioMiniView.m
//  MotherPlanet
//
//  Created by GeekZooStudio on 2018/4/18.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "AudioMiniView.h"
#import "AudioPlayButton.h"

@interface AudioMiniView() <AudioPlayManagerDataSource, AudioPlayManagerDelegate>
@property (weak, nonatomic) IBOutlet UIButton *backBtn;
@property (weak, nonatomic) IBOutlet UIImageView *iconImgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;
@property (weak, nonatomic) IBOutlet UIButton *nextBtn;
@property (weak, nonatomic) IBOutlet AudioPlayButton *playBtn;

@property (nonatomic, strong) AudioPlayManager *audioManager;
@end

@implementation AudioMiniView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.iconImgView.layer.cornerRadius = 18;
    self.iconImgView.layer.masksToBounds = YES;
    self.progressView.progress = 0.0;
    
    self.audioManager = [AudioPlayManager sharedInstance];
    self.audioManager.dataSource = self;
    self.audioManager.delegate = self;
    
    [self checkPlayStatus];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.frame = CGRectMake(self.x, self.y, kScreenWidth, 60);
}

- (void)resetDelegate {
    self.audioManager.dataSource = self;
    self.audioManager.delegate = self;
}

- (void)setDatas:(NSArray<AUDIO *> *)datas {
    _datas = datas;
    [self checkPlayStatus];
}

- (IBAction)backBtnAction:(UIButton *)sender {
    if (self.onBack) {
        self.onBack();
    }
}

- (IBAction)nextBtnAction:(UIButton *)sender {
    [self.audioManager next];
}

- (IBAction)playBtnAction:(AudioPlayButton *)sender {
//    if (sender.status == AudioPlayButtonStatusPlay || sender.status == AudioPlayButtonStatusLoading) {
    if (self.audioManager.status == AudioPlayerStatusRestartToPlay) {
        [self.audioManager pause];
    } else {
        [self.audioManager play];
    }
}

- (void)setupInfo:(NSInteger)index {
    if (index >= self.datas.count) return;
    
    AUDIO *audio = self.datas[index];
    self.titleLabel.text = audio.title;
    [self.iconImgView setImageWithPhoto:audio.photo placeholderImage:[AppTheme avatarDefaultImage]];
}

- (void)checkPlayStatus {
    [self setupInfo:self.audioManager.currentIndex];
    
    // 根据状态设置
    if (self.audioManager.status == AudioPlayerStatusRestartToPlay) {
        self.playBtn.status = AudioPlayButtonStatusPlay;
    } else if (self.audioManager.status == AudioPlayerStatusBufferEmpty ||
               self.audioManager.status == AudioPlayerStatusReadyToPlay) {
        self.playBtn.status = AudioPlayButtonStatusLoading;
    } else {
        self.playBtn.status = AudioPlayButtonStatusPause;
    }
    
    CGFloat progress = self.audioManager.player.currentItem ? self.audioManager.currentTime / self.audioManager.duration : 0.0;
    self.progressView.progress = progress;
}

#pragma mark - AudioPlayManagerDataSource

- (NSInteger)numberOfAudios {
    return self.datas.count;
}

- (AVPlayerItem *)audioWithIndex:(NSInteger)index {
    if (index >= self.datas.count) return nil;
    
    [self setupInfo:index];
    
    AUDIO *audio = self.datas[index];
    
    NSURL *url = nil;
    if ([audio.audio_url hasPrefix:@"http://"] || [audio.audio_url hasPrefix:@"https://"]) {
        url = [NSURL URLWithString:audio.audio_url];
    } else {
        NSString *path = [[[NSBundle mainBundle] pathForResource:audio.audio_url ofType:nil] stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        url = [NSURL fileURLWithPath:path];
    }
    
    AVPlayerItem *item = [AVPlayerItem playerItemWithURL:url];
    return item;
}

#pragma mark - AudioPlayManagerDelegate

- (void)audioPlayerStatusChanged:(AudioPlayerStatus)status {
    switch (status) {
        case AudioPlayerStatusReadyToPlay:
            NSLog(@"加载成功，准备播放，开始缓冲");
            self.playBtn.status = AudioPlayButtonStatusLoading;
            break;
        case AudioPlayerStatusUnknown:
        case AudioPlayerStatusFailed:
            NSLog(@"加载失败");
            self.playBtn.status = AudioPlayButtonStatusPause;
            break;
        case AudioPlayerStatusRestartToPlay:
            NSLog(@"开始播放");
            self.playBtn.status = AudioPlayButtonStatusPlay;
            break;
        case AudioPlayerStatusPause:
            NSLog(@"暂停");
            self.playBtn.status = AudioPlayButtonStatusPause;
            break;
        case AudioPlayerStatusBufferEmpty:
            NSLog(@"缓冲不足，暂停");
            self.playBtn.status = AudioPlayButtonStatusLoading;
            break;
        default:
            break;
    }
}

- (void)updateProgress:(NSTimeInterval)currentTime total:(NSTimeInterval)totalTime; {
    self.progressView.progress = currentTime <= totalTime ? currentTime / totalTime : 0;
    
//    if (!self.playBtn.selected) {
//        [self checkPlayStatus];
//    }
}

@end
