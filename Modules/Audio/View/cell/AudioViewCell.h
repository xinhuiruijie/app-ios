//
//  AudioViewCell.h
//  MotherPlanet
//
//  Created by GeekZooStudio on 2018/4/28.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AudioViewCell : UITableViewCell
@property (nonatomic, assign) BOOL isSelected;
@property (nonatomic, assign) BOOL isPlaying;
@end
