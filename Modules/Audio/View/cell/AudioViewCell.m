//
//  AudioViewCell.m
//  MotherPlanet
//
//  Created by GeekZooStudio on 2018/4/28.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "AudioViewCell.h"

@interface AudioViewCell()
@property (weak, nonatomic) IBOutlet PlayingLineAnimatedView *animationView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *sourceLabel;
@property (weak, nonatomic) IBOutlet UILabel *durationLabel;
@end

@implementation AudioViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.animationView.hidden = YES;
    self.animationView.backgroundColor = [UIColor clearColor];
}

- (void)setIsSelected:(BOOL)isSelected {
    _isSelected = isSelected;
    self.animationView.hidden = !isSelected;
    self.durationLabel.hidden = isSelected;
}

- (void)setIsPlaying:(BOOL)isPlaying {
    _isPlaying = isPlaying;
    if (isPlaying && !self.animationView.hidden) {
        [self.animationView addAnimation];
    } else {
        [self.animationView removeAnimation];
    }
}

- (void)dataDidChange {
    if ([self.data isKindOfClass:[AUDIO class]]) {
        AUDIO *audio = self.data;
        self.titleLabel.text = audio.title?:@"--";
        self.subtitleLabel.text = audio.subtitle?:@"--";
        self.sourceLabel.text = [NSString stringWithFormat:@"by%@", audio.owner.name?:@"--"];
        self.durationLabel.text = [self timeFormat:audio.duration.integerValue];
    }
}

- (NSString *)timeFormat:(NSInteger)secounds {
    NSInteger m = secounds / 60;
    NSInteger s = secounds % 60;
    return [NSString stringWithFormat:@"%02ld’%02ld”", m, s];
}

@end
