//
//  AudioMiniView.h
//  MotherPlanet
//
//  Created by GeekZooStudio on 2018/4/18.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AudioMiniView : UIView
@property (nonatomic, copy) void (^onBack)(void);

@property (nonatomic, strong) NSArray<AUDIO *> *datas;

- (void)checkPlayStatus;
- (void)resetDelegate; // 这个方法在每一次重新显示的时候要调用，重新设置代理方法，很重要，不然收不到代理时间，因为audioPlayManager是单例，会保留上次的代理
@end
