//
//  AudioPlayButton.m
//  MotherPlanet
//
//  Created by GeekZooStudio on 2018/5/17.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "AudioPlayButton.h"

@implementation AudioPlayButton

- (void)setStatus:(AudioPlayButtonStatus)status {
    _status = status;
    
    switch (status) {
        case AudioPlayButtonStatusPlay:
            [self setImage:[UIImage imageNamed:@"icon_fm_pause"] forState:UIControlStateNormal];
            self.userInteractionEnabled = YES; // 不用enabled, 会有状态
            [self stopAnimation];
            break;
        case AudioPlayButtonStatusPause:
            [self setImage:[UIImage imageNamed:@"icon_fm_play"] forState:UIControlStateNormal];
            self.userInteractionEnabled = YES;
            [self stopAnimation];
            break;
        case AudioPlayButtonStatusLoading:
            [self setImage:[UIImage imageNamed:@"icon_fm_loading"] forState:UIControlStateNormal];
            self.userInteractionEnabled = NO;
            [self startAnimation];
            break;
        default:
            break;
    }
}

#pragma mark -

// 开始旋转
- (void)startAnimation {
    [self.layer addAnimation:[self rotationAuto] forKey:@"rotationAnimation"];
}

// 停止旋转
- (void)stopAnimation {
    [self.layer removeAnimationForKey:@"rotationAnimation"];
}

// 旋转动画
- (CABasicAnimation *)rotationAuto {
    CABasicAnimation *rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 ];
    rotationAnimation.duration = 2;
    rotationAnimation.repeatCount = NSIntegerMax;
    return rotationAnimation;
}

@end
