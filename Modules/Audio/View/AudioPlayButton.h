//
//  AudioPlayButton.h
//  MotherPlanet
//
//  Created by GeekZooStudio on 2018/5/17.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, AudioPlayButtonStatus) {
    AudioPlayButtonStatusPlay = 0,
    AudioPlayButtonStatusPause,
    AudioPlayButtonStatusLoading,
};

@interface AudioPlayButton : UIButton
@property (nonatomic, assign) AudioPlayButtonStatus status;
@end
