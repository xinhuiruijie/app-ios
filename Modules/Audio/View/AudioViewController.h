//
//  AudioViewController.h
//  MotherPlanet
//
//  Created by GeekZooStudio on 2018/4/17.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "MPViewController.h"

@interface AudioViewController : MPViewController
@property (nonatomic, strong) AUDIO *currentAudio;
@end
