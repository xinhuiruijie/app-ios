//
//  LaboratoryModel.h
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/6.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "STIStreamModel.h"

@interface LaboratoryModel : STIStreamModel

@property (nonatomic, strong) NSArray *wormholes;
@property (nonatomic, strong) NSNumber *wormhole_owner_count;
@property (nonatomic, strong) NSArray *audios;
@property (nonatomic, strong) NSNumber *audio_latest_count;

@end
