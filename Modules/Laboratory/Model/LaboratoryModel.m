//
//  LaboratoryModel.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/6.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "LaboratoryModel.h"

@implementation LaboratoryModel

- (void)loadCache {
    NSArray *wormholes = [self loadCacheWithKey:[self laboratoryWormholes] objectClass:[WORMHOLE class]];
    NSArray *audios = [self loadCacheWithKey:[self laboratoryAudios] objectClass:[AUDIO class]];
    NSNumber *wormholeCount = [self loadCacheWithKey:[self laboratoryWormholesCount] objectClass:[NSNumber class]];
    NSNumber *audioCount = [self loadCacheWithKey:[self laboratoryAudiosCount] objectClass:[NSNumber class]];

    if (wormholes && wormholes.count) {
        self.wormholes = wormholes;
    }
    if (audios && audios.count) {
        self.audios = audios;
    }
    self.wormhole_owner_count = wormholeCount;
    self.audio_latest_count = audioCount;
}

- (void)saveCache {
    [self saveCache:self.wormholes key:[self laboratoryWormholes]];
    [self saveCache:self.wormhole_owner_count key:[self laboratoryWormholesCount]];
    [self saveCache:self.audios key:[self laboratoryAudios]];
    [self saveCache:self.audio_latest_count key:[self laboratoryAudiosCount]];
}

- (NSString *)laboratoryWormholes {
    return @"laboratoryWormholes";
}

- (NSString *)laboratoryWormholesCount {
    return @"laboratoryWormholesCount";
}

- (NSString *)laboratoryAudios {
    return @"laboratoryAudios";
}

- (NSString *)laboratoryAudiosCount {
    return @"laboratoryAudiosCount";
}

- (void)refresh {
    V1_API_LABORATORY_LIST_API *api = [[V1_API_LABORATORY_LIST_API alloc] init];
    
    api.whenUpdated = ^(V1_API_LABORATORY_LIST_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if (error) {
            PERFORM_BLOCK_SAFELY(self.whenUpdated, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                self.loaded = YES;
                
                self.wormholes = [NSArray arrayWithArray:response.wormholes];
                self.wormhole_owner_count = response.wormhole_owner_count;
                self.audios = [NSArray arrayWithArray:response.audios];
                self.audio_latest_count = response.audio_latest_count;
                [self saveCache];
                PERFORM_BLOCK_SAFELY(self.whenUpdated, nil);
            } else {
                STIHTTPResponseError *errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(self.whenUpdated, errors);
            }
        }
    };
    [api send];
}

@end
