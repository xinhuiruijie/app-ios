//
//  LaboratoryWormholesItem.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/6.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "LaboratoryWormholesItem.h"

@interface LaboratoryWormholesItem ()

@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIImageView *wormholePhoto;
@property (weak, nonatomic) IBOutlet UILabel *wormholeSubtitle;
@property (weak, nonatomic) IBOutlet UILabel *wormholeUserCount;
@property (weak, nonatomic) IBOutlet UILabel *wormholeTitle;
@property (weak, nonatomic) IBOutlet UIView *tagView;

@end

@implementation LaboratoryWormholesItem

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backView.layer.cornerRadius = 5;
    self.backView.clipsToBounds = YES;
    self.tagView.layer.cornerRadius = 2;
    self.tagView.clipsToBounds = YES;
}

- (void)dataDidChange {
    WORMHOLE *wormhole = self.data;
    
    self.wormholePhoto.image = [AppTheme placeholderImage];
    if (wormhole.photo) {
        [self.wormholePhoto setImageWithPhoto:wormhole.photo];
    } else {
        self.wormholePhoto.image = [AppTheme placeholderImage];
    }
    self.wormholeSubtitle.text = wormhole.subtitle ? [NSString stringWithFormat:@"%@", wormhole.subtitle] : @"";
    self.wormholeUserCount.text = [NSString stringWithFormat:@"%@", wormhole.user_count?:@""];
    self.wormholeTitle.text = wormhole.title?:@"";
}

@end
