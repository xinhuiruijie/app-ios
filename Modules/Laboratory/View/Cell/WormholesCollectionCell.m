//
//  WormholesCollectionCell.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/7.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "WormholesCollectionCell.h"

#import "LaboratoryWormholesItem.h"

#import "LaboratoryModel.h"

@interface WormholesCollectionCell () <UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *wormholeOwnerCount;

@property (nonatomic, strong) LaboratoryModel *laboratoryModel;

@property (nonatomic, strong) NSArray *dataSource;

@end

@implementation WormholesCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backView.layer.cornerRadius = 5;
    self.backView.clipsToBounds = YES;
    
    [self.collectionView registerNib:[LaboratoryWormholesItem nib] forCellWithReuseIdentifier:@"LaboratoryWormholesItem"];
    
    CGFloat itemWidth = kScreenWidth - 40;
    CGFloat itemHeight = ceilf(itemWidth * 120 / 375);
    
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    layout.itemSize = CGSizeMake(itemWidth, itemHeight);
    layout.minimumLineSpacing = 15;
    layout.minimumInteritemSpacing = CGFLOAT_MIN;
}

- (void)dataDidChange {
    self.laboratoryModel = self.data;
    
    self.wormholeOwnerCount.text = [NSString stringWithFormat:@"%@人参与", self.laboratoryModel.wormhole_owner_count?:@"0"];
    
    self.dataSource = self.laboratoryModel.wormholes;
    if (self.dataSource && self.dataSource.count > 3) {
        self.dataSource = [self.dataSource subarrayWithRange:NSMakeRange(0, 3)];
    }

    [self.collectionView reloadData];
}

#pragma mark - Collection View Data Source

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LaboratoryWormholesItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LaboratoryWormholesItem" forIndexPath:indexPath];
    cell.data = self.dataSource[indexPath.row];
    return cell;
}

#pragma mark - Collection View Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell * cell = [collectionView cellForItemAtIndexPath:indexPath];
    WORMHOLE *wormhole = cell.data;
    if (self.pushWormhole) {
        self.pushWormhole(wormhole);
    }
}

@end
