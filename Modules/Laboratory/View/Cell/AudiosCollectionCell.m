//
//  AudiosCollectionCell.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/7.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "AudiosCollectionCell.h"

#import "LaboratoryAudiosItem.h"

#import "LaboratoryModel.h"

@interface AudiosCollectionCell () <UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UICollectionView *audosCollectionView;
@property (weak, nonatomic) IBOutlet UILabel *audioOwnerCount;

@property (weak, nonatomic) IBOutlet UIImageView *firstAvatarImage;
@property (weak, nonatomic) IBOutlet UIView *firstPlayingView;
@property (weak, nonatomic) IBOutlet UIImageView *secondAvatarImage;
@property (weak, nonatomic) IBOutlet UIView *secondPlayingView;
@property (weak, nonatomic) IBOutlet UIImageView *thirdAvatarImage;
@property (weak, nonatomic) IBOutlet UIView *thirdPlayingView;

@property (nonatomic, strong) LaboratoryModel *laboratoryModel;

@property (nonatomic, strong) NSMutableArray *dataSource;
@property (nonatomic, strong) NSMutableArray *audioOwnersArray;

@end

@implementation AudiosCollectionCell

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backView.layer.cornerRadius = 2;
    self.backView.clipsToBounds = YES;
    self.firstAvatarImage.layer.cornerRadius = 12;
    self.firstAvatarImage.clipsToBounds = YES;
    self.firstPlayingView.layer.cornerRadius = 5;
    self.firstPlayingView.clipsToBounds = YES;
    self.firstPlayingView.hidden = YES;
    self.secondAvatarImage.layer.cornerRadius = 12;
    self.secondAvatarImage.clipsToBounds = YES;
    self.secondPlayingView.layer.cornerRadius = 5;
    self.secondPlayingView.clipsToBounds = YES;
    self.secondPlayingView.hidden = YES;
    self.thirdAvatarImage.layer.cornerRadius = 12;
    self.thirdAvatarImage.clipsToBounds = YES;
    self.thirdPlayingView.layer.cornerRadius = 5;
    self.thirdPlayingView.clipsToBounds = YES;
    self.thirdPlayingView.hidden = YES;
    
    [self.audosCollectionView registerNib:[LaboratoryAudiosItem nib] forCellWithReuseIdentifier:@"LaboratoryAudiosItem"];
    
    // 监听电台播放状态
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(audioPlayerStatusChangedNotification:) name:AudioPlayManagerStatusChangedNotification object:nil];
}

// 监听电台播放状态改变通知
- (void)audioPlayerStatusChangedNotification:(NSNotification *)noti {
    if ([noti.object isEqual:[AudioPlayManager sharedInstance]]) {
        [self setupPlayingView];
    }
}

- (void)dataDidChange {
    self.laboratoryModel = self.data;
    self.dataSource = [NSMutableArray array];
    
    self.audioOwnerCount.text = [NSString stringWithFormat:@"节目上新：%@", self.laboratoryModel.audio_latest_count?:@"0"];
    
    self.audioOwnersArray = [NSMutableArray array];
    for (AUDIO *audio in self.laboratoryModel.audios) {
        [self.audioOwnersArray addObject:audio.owner];
    }
    
    for (NSInteger indexPath = self.audioOwnersArray.count - 1; indexPath >= 0; indexPath--) {
        USER *owner = self.audioOwnersArray[indexPath];
        
        for (NSInteger indexPathRange = indexPath - 1; indexPathRange >= 0; indexPathRange--) {
            USER *owners = self.audioOwnersArray[indexPathRange];
            
            if (owner.id == owners.id) {
                [self.audioOwnersArray removeObjectAtIndex:indexPath];
                break;
            }
        }
    }
    
    if (self.laboratoryModel.wormholes.count <= 3) {
        self.dataSource = [NSMutableArray arrayWithArray:self.laboratoryModel.audios];
    } else {
        [self.dataSource addObject:self.laboratoryModel.audios[0]];
        [self.dataSource addObject:self.laboratoryModel.audios[1]];
        [self.dataSource addObject:self.laboratoryModel.audios[2]];
    }
    
    NSInteger laboratoryAudiosCount = self.audioOwnersArray.count;
    USER *firstOwner = laboratoryAudiosCount > 0 ? self.audioOwnersArray[0] : nil;
    USER *secondOwner = laboratoryAudiosCount > 1 ? self.audioOwnersArray[1] : nil;
    USER *thirdOwner = laboratoryAudiosCount > 2 ? self.audioOwnersArray[2] : nil;
    
    self.firstAvatarImage.hidden = !firstOwner;
    self.secondAvatarImage.hidden = !secondOwner;
    self.thirdAvatarImage.hidden = !thirdOwner;
    
    [self.firstAvatarImage setImageWithPhoto:firstOwner.avatar placeholderImage:[AppTheme avatarDefaultImage]];
    [self.secondAvatarImage setImageWithPhoto:secondOwner.avatar placeholderImage:[AppTheme avatarDefaultImage]];
    [self.thirdAvatarImage setImageWithPhoto:thirdOwner.avatar placeholderImage:[AppTheme avatarDefaultImage]];
    
    [self setupPlayingView];
    
    [self.audosCollectionView reloadData];
}

// 设置头像播放状态
- (void)setupPlayingView {
    if ([AudioPlayManager sharedInstance].status != AudioPlayerStatusRestartToPlay) {
        self.firstPlayingView.hidden = YES;
        self.secondPlayingView.hidden = YES;
        self.thirdPlayingView.hidden = YES;
        return;
    }
    
    AudioListModel *audioModel = [[AudioListModel alloc] init];
    NSInteger currentIndex = [AudioPlayManager sharedInstance].currentIndex;
    if (currentIndex >= 0 && currentIndex < audioModel.items.count) {
        AUDIO *currentAudio = audioModel.items[currentIndex];
        NSInteger laboratoryAudiosCount = self.audioOwnersArray.count; // 需要注意数组越界问题
        if (laboratoryAudiosCount < 1) return;
        USER *firstOwner = self.audioOwnersArray[0];
        self.firstPlayingView.hidden = ![currentAudio.owner.id isEqualToString:firstOwner.id];
        if (laboratoryAudiosCount < 2) return;
        USER *secondOwner = self.audioOwnersArray[1];
        self.secondPlayingView.hidden = ![currentAudio.owner.id isEqualToString:secondOwner.id];
        if (laboratoryAudiosCount < 3) return;
        USER *thirdOwner = self.audioOwnersArray[2];
        self.thirdPlayingView.hidden = ![currentAudio.owner.id isEqualToString:thirdOwner.id];
    }
}

#pragma mark - Collection View Data Source

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LaboratoryAudiosItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"LaboratoryAudiosItem" forIndexPath:indexPath];
    cell.data = self.dataSource[indexPath.row];
    return cell;
}

#pragma mark - Collection View Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell * cell = [collectionView cellForItemAtIndexPath:indexPath];
    AUDIO *audio = cell.data;
    if (self.pushAudio) {
        self.pushAudio(audio);
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat itemWidth = ceilf((kScreenWidth - 38) / 2);
    CGFloat itemHeight = ceilf(itemWidth * 95 / 169);
    return CGSizeMake(itemWidth, itemHeight);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    return CGSizeZero;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(6, 6, 6, 6);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 6;
}

@end
