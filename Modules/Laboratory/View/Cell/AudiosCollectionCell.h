//
//  AudiosCollectionCell.h
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/7.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AudiosCollectionCell : UICollectionViewCell

@property (nonatomic, copy) void(^pushAudio)(AUDIO *audio);

@end
