//
//  LaboratoryAudiosItem.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/6.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "LaboratoryAudiosItem.h"

@interface LaboratoryAudiosItem ()

@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIImageView *audioPhoto;
@property (weak, nonatomic) IBOutlet UILabel *audioTitle;

@end

@implementation LaboratoryAudiosItem

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backView.layer.cornerRadius = 2;
    self.backView.clipsToBounds = YES;
}

-(void)dataDidChange {
    AUDIO *audio = self.data;
    self.audioPhoto.image = [AppTheme placeholderImage];
    if (audio.owner.avatar) {
        [self.audioPhoto setImageWithPhoto:audio.photo];
    } else {
        self.audioPhoto.image = [AppTheme placeholderImage];
    }
    self.audioTitle.text = [NSString stringWithFormat:@"%@", audio.title?:@""];
}

@end
