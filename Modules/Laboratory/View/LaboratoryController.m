//
//  LaboratoryController.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/6.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "LaboratoryController.h"
#import "AudioViewController.h"
#import "UserWormholeListController.h"
#import "WormholeVideoListController.h"

#import "LaboratoryModel.h"

#import "AudiosCollectionCell.h"
#import "WormholesCollectionCell.h"
#import "LaboratoryFooterCell.h"

typedef NS_ENUM(NSInteger, LABORATORY_TYPE) {
    LABORATORY_TYPE_WORMHOLE = 0,
    LABORATORY_TYPE_AUDIO = 1,
};

@interface LaboratoryController () <UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *headerImageHLC;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic, strong) LaboratoryModel *laboratoryModel;

@property (nonatomic, strong) NSMutableArray *dataSource;

@end

@implementation LaboratoryController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Laboratory"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    if ([SDiOSVersion deviceSize] == Screen5Dot8inch) {
        self.headerImageHLC.constant = 249;
        self.imageView.highlighted = YES;
    } else {
        self.headerImageHLC.constant = 211;
        self.imageView.highlighted = NO;
    }
    
    self.dataSource = [NSMutableArray array];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onTabBarDoubleClick:) name:UITabBarItemDoubleClickNotification object:nil];

    [self modelInit];
    [self registerNibs];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.laboratoryModel refresh];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

- (void)onTabBarDoubleClick:(NSNotification *)notification {
    // tabbar双击时刷新
    if ([notification.object isKindOfClass:[MPRootViewController class]]) {
        MPRootViewController *rootController = (MPRootViewController *)notification.object;
        if ([self.navigationController isEqual:rootController.selectedViewController]) {
            [self.collectionView.header beginRefreshing];
        }
    }
}

- (void)registerNibs {
    [self.collectionView registerNib:[AudiosCollectionCell nib] forCellWithReuseIdentifier:@"AudiosCollectionCell"];
    [self.collectionView registerNib:[WormholesCollectionCell nib] forCellWithReuseIdentifier:@"WormholesCollectionCell"];
    [self.collectionView registerNib:[LaboratoryFooterCell nib] forSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"LaboratoryFooterCell"];
}

- (void)modelInit {
    @weakify(self)
    self.laboratoryModel = [[LaboratoryModel alloc] init];
    self.laboratoryModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        [self.collectionView.header endRefreshing];
        if (error == nil) {
//            [self setupRefreshFooterView];
            [self.dataSource removeAllObjects];
            [self.dataSource addObject:@(LABORATORY_TYPE_WORMHOLE)];
//            [self.dataSource addObject:@(LABORATORY_TYPE_AUDIO)];
            [self.collectionView reloadData];
        } else {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
    };
    [self.laboratoryModel loadCache];
    [self.collectionView reloadData];
    [self setupRefreshHeaderView];
}

#pragma mark - action

- (void)gotoAudioList:(AUDIO *)audio {
    AudioViewController *audioController = [AudioViewController spawn];
    audioController.currentAudio = audio;
    [self presentViewController:audioController animated:YES completion:nil];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    LABORATORY_TYPE laboratory = [self.dataSource[indexPath.item] integerValue];
    switch (laboratory) {
        case LABORATORY_TYPE_WORMHOLE: {
            WormholesCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"WormholesCollectionCell" forIndexPath:indexPath];
            cell.data = self.laboratoryModel;
            cell.pushWormhole = ^(WORMHOLE *wormhole) {
                WormholeVideoListController *wormholeVideoListController = [WormholeVideoListController spawn];
                wormholeVideoListController.wormholeID = wormhole.id;
                wormholeVideoListController.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:wormholeVideoListController animated:YES];
            };
            return cell;
        }
            break;
        case LABORATORY_TYPE_AUDIO: {
            AudiosCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AudiosCollectionCell" forIndexPath:indexPath];
            cell.data = self.laboratoryModel;
            cell.pushAudio = ^(AUDIO *audio) {
                [self gotoAudioList:audio];
            };
            return cell;
        }
            break;
            
        default:
            break;
    }
    return [[UICollectionViewCell alloc] init];
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    UICollectionReusableView *supplementaryView;
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        supplementaryView = nil;
    } else if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {
        LaboratoryFooterCell *footerView = (LaboratoryFooterCell *)[collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionFooter withReuseIdentifier:@"LaboratoryFooterCell" forIndexPath:indexPath];
        supplementaryView = footerView;
    }
    return supplementaryView;
    
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    LABORATORY_TYPE laboratory = [self.dataSource[indexPath.item] integerValue];
    switch (laboratory) {
        case LABORATORY_TYPE_WORMHOLE: {
            UserWormholeListController *wormholeListController = [UserWormholeListController spawn];
            wormholeListController.wormholeUser = WORMHOLE_USER_ALL;
            wormholeListController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:wormholeListController animated:YES];
        }
            break;
        case LABORATORY_TYPE_AUDIO: {
            [self gotoAudioList:nil];
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    LABORATORY_TYPE laboratory = [self.dataSource[indexPath.item] integerValue];
    switch (laboratory) {
        case LABORATORY_TYPE_WORMHOLE: {
            CGFloat count = MIN(self.laboratoryModel.wormholes.count, 3);
            CGFloat height = count * (ceil((kScreenWidth - 40) * 120 / 375) + 15);
            return CGSizeMake(kScreenWidth - 20, height + 75);
        }
            break;
        case LABORATORY_TYPE_AUDIO: {
            CGFloat itemHeight = ceilf(kScreenWidth * 107 / 375);
            return CGSizeMake(kScreenWidth - 20, itemHeight + 68);
        }
            break;
            
        default:
            break;
    }
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    return CGSizeMake(kScreenWidth, 37);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 10, 0, 10);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 10;
}

#pragma mark - Header && Footer

- (void)setupRefreshHeaderView {
    @weakify(self)
    [self.collectionView addHeaderPullLoader:^{
        @strongify(self)
        [self.laboratoryModel refresh];
    }];
}

- (void)setupRefreshFooterView {
    if (self.collectionView.footer == nil) {
        if (!self.laboratoryModel.isEmpty) {
            @weakify(self)
            [self.collectionView addFooterPullLoader:^{
                @strongify(self)
                [self.laboratoryModel loadMore];
            }];
        }
    } else {
        if (self.laboratoryModel.isEmpty) {
            [self.collectionView removeFooter];
        }
    }
}

@end
