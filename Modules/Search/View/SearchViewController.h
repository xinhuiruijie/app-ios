//
//  SearchViewController.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SearchHotModel.h"

#pragma mark -

typedef NS_ENUM(NSInteger, SearchActionState)
{
    SearchActionStatePending = 0, // 将要搜索-展示历史搜索
    SearchActionStateDone,    // 搜索完成-展示搜索结果列表，键盘收回
};

#pragma mark -

@interface SearchViewController : UIViewController

@property (nonatomic, strong) SearchHotModel *searchHotModel;

@property (nonatomic, assign) SearchActionState actionState;
@property (nonatomic, assign) SEARCH_TYPE searchType;
@property (nonatomic, strong) NSString *keyword;

- (void)reloadSearch;

@end
