//
//  SearchResultController.h
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/4/17.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchResultController : UIViewController

@property (nonatomic, copy) NSString *keyword;
@property (nonatomic, assign) SEARCH_TYPE searchType;

@property (nonatomic, copy) void(^searchBackBlock)(void);

@end
