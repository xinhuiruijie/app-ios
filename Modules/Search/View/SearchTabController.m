//
//  SearchTabController.m
//  MotherPlanet
//
//  Created by liuyadi on 2018/4/9.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "SearchTabController.h"
#import "SearchViewController.h"

#import "SearchHotModel.h"

#import "SearchSectionCell.h"
#import "SearchHotCell.h"
#import "SearchTabHeaderView.h"

#import "MPBackGuideView.h"

@interface SearchTabController () <UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic, strong) SearchHotModel *searchHotModel;

@property (nonatomic, strong) NSMutableArray *tableDataArray;
    
@property (nonatomic, strong) SearchTabHeaderView *tabHeaderView;

@end

@implementation SearchTabController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Search"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [AppAnalytics clickEvent:@"clicksearch"];
    
    [self modelInit];
    [self customize];
    
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:[self customTitleView:@"探索"]];
    self.navigationItem.leftBarButtonItem = leftItem;
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    self.view.backgroundColor = [UIColor whiteColor];
    
    self.tableDataArray = [NSMutableArray array];
    
}

- (UIView *)customTitleView:(NSString *)title {
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 50, 34)];
    titleLabel.textColor = [AppTheme normalTextColor];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.text = title;
    titleLabel.font = [UIFont systemFontOfSize:24 weight:UIFontWeightMedium];
    return titleLabel;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.searchHotModel refresh];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self showFeatureGuideViewIfNeeded];
    if (self.view.y < 64) {
        int cost = 64 - abs((int)self.view.y);
        self.tabHeaderView.frame = CGRectMake(0, cost, kScreenWidth, 50);
    } else {
        self.tabHeaderView.frame = CGRectMake(0, 0, kScreenWidth, 50);
    }
}

#pragma mark - Search Hot Model Data

- (void)modelInit {
    @weakify(self)
    self.searchHotModel = [[SearchHotModel alloc] init];
    self.searchHotModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        
        [self.collectionView reloadData];
        if (error) {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
    };
    [self.searchHotModel loadCache];
    [self.collectionView reloadData];
}

#pragma mark - View

- (void)customize {
    SearchTabHeaderView *tabHeaderView = [[SearchTabHeaderView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 50)];
    @weakify(self)
    tabHeaderView.searchActionBlock = ^{
        @strongify(self)
        SearchViewController *searchView = [SearchViewController spawn];
        searchView.hidesBottomBarWhenPushed = YES;
        searchView.searchType = SEARCH_TYPE_ALL;
        searchView.searchHotModel = self.searchHotModel;
        [self.navigationController pushViewController:searchView animated:NO];
    };
    [self.view addSubview:tabHeaderView];
    self.tabHeaderView = tabHeaderView;
    
    [self.collectionView registerNib:[SearchSectionCell nib] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"SearchSectionCell"];
    [self.collectionView registerNib:[SearchHotCell nib] forCellWithReuseIdentifier:@"SearchHotCell"];
}

#pragma mark - Collection View Data Source

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.searchHotModel.items.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SearchHotCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SearchHotCell" forIndexPath:indexPath];
    cell.data = self.searchHotModel.items[indexPath.row];
    return cell;
}

#pragma mark - Collection View Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell * cell = [collectionView cellForItemAtIndexPath:indexPath];
    
    HOT_SERACH *hotSearch = cell.data;
    
    SearchViewController *searchView = [SearchViewController spawn];
    searchView.hidesBottomBarWhenPushed = YES;
    searchView.actionState = SearchActionStateDone;
    searchView.searchType = hotSearch.type;
    searchView.keyword = hotSearch.title;
    searchView.searchHotModel = self.searchHotModel;
    [self.navigationController pushViewController:searchView animated:NO];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(kScreenWidth, 44);
}

- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
        if (![self isKeywordsListEmpty]) {
            SearchSectionCell * cell = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"SearchSectionCell" forIndexPath:indexPath];
            cell.needLineHid = YES;
            cell.titleType = TITLE_TYPE_HOTSEARCH;
            return cell;
        }
    }
    return nil;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    if (![self isKeywordsListEmpty]) {
        return CGSizeMake(collectionView.width, 45.f);
    }
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    return CGSizeZero;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return CGFLOAT_MIN;
}

#pragma mark -

- (BOOL)isKeywordsListEmpty {
    if (self.searchHotModel.items.count > 0) {
        return NO;
    }
    return YES;
}

#pragma mark - Guide

// 显示新手引导图
- (void)showFeatureGuideViewIfNeeded {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    BOOL showGuide = [ud boolForKey:@"showSearchGuide"];
    if (!showGuide) {
        [self showFeatureGuideView];
    }
}

- (void)showFeatureGuideView {
    MPBackGuideView *guideView = [MPBackGuideView showBackGuide];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (guideView) {
            [guideView dismiss];
        }
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        [ud setBool:YES forKey:@"showSearchGuide"];
        [ud synchronize];
    });
}

@end
