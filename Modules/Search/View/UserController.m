//
//  UserController.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/4/20.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "UserController.h"
#import "UserWormholeListController.h"
#import "ProfileEditInfoController.h"
#import "CouponListController.h"
#import "MyFollowListController.h"
#import "MyCollectionController.h"
#import "MyWatchListController.h"
#import "ProfileSettingController.h"
#import "IntegralMallWebController.h"
#import "WatchLaterListController.h"
#import "WormholeVideoListController.h"

#import "UserTableHeaderView.h"
#import "ProfileWormholeCell.h"
#import "ProfileTimelineCell.h"
#import "TimelineHeaderView.h"

#import "MyTimelineModel.h"
#import "UserWormholeListModel.h"
#import "AuthorInfoModel.h"

@interface UserController () <UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) UserTableHeaderView *headerView;
@property (nonatomic, strong) USER *user;
@property (nonatomic, strong) MyTimelineModel *timelineModel;
@property (nonatomic, strong) UserWormholeListModel *userWormholeModel;
@property (nonatomic, strong) AuthorInfoModel *infoModel;
@property (nonatomic, strong) NSMutableArray *cellsArray;
@property (nonatomic, assign) CGFloat headerHight;

@end

@implementation UserController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Search"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [AppTheme backgroundColor];
    
    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
    @weakify(self)
    self.navigationItem.leftBarButtonItem = [AppTheme whiteBackItemWithHandler:^(id sender) {
        @strongify(self)
        [self closeController];
    }];
    
    self.cellsArray = [NSMutableArray array];
    self.tableView.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
//    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
//    self.tableView.backgroundColor = [UIColor whiteColor];
//    self.tableView.showsVerticalScrollIndicator = NO;
//    self.tableView.dataSource = self;
//    self.tableView.delegate = self;
//    [self.view addSubview:self.tableView];
    CGFloat topInset = [SDiOSVersion deviceSize] == Screen5Dot8inch ? -88 : -64;
    if (iOSVersionGreaterThanOrEqualTo(@"11.0")) {
        self.tableView.estimatedRowHeight = 0;
        self.tableView.estimatedSectionHeaderHeight = 0;
        self.tableView.estimatedSectionFooterHeight = 0;
        self.tableView.contentInset = UIEdgeInsetsMake(topInset, 0, 0, 0);
    }
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    CGFloat height = 119 + (kScreenWidth * 520 / 750);
    self.headerHight = height;
    self.headerView = [UserTableHeaderView loadFromNib];
    self.headerView.frame = CGRectMake(0, 0, self.view.frame.size.width, height);
    
    [self registerNib];
    [self validCell];
    [self setupUserModel];
    [self userWormholeModelInit];
    [self timelineModelInit];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self scrollViewDidScroll:self.tableView];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
    [self.infoModel refresh];
    [self.userWormholeModel refresh];
    [self runAnimation:self.headerView.refreshImageView];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar lt_reset];
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    if (self.tableView.contentOffset.y > 50) {
        return UIStatusBarStyleDefault;
    }
    return UIStatusBarStyleLightContent;
}

#pragma mark - CustomMethod

- (void)registerNib {
    [self.tableView registerNib:[ProfileWormholeCell nib] forCellReuseIdentifier:@"ProfileWormholeCell"];
    [self.tableView registerNib:[ProfileTimelineCell nib] forCellReuseIdentifier:@"ProfileTimelineCell"];
}

- (void)validCell {
    [self.cellsArray removeAllObjects];
    [self.cellsArray addObject:@"Header"];
    if (self.timelineModel.loaded && self.timelineModel.items.count) {
        [self.cellsArray addObject:@"ProfileWormholeCell"];
    }
    if (self.timelineModel.loaded && self.timelineModel.items.count) {
        [self.cellsArray addObject:@"ProfileTimelineCell"];
    }
}

#pragma mark - Model

- (void)setupUserModel {
    @weakify(self)
    self.infoModel = [[AuthorInfoModel alloc] init];
    self.infoModel.author_id = self.userID;
    self.infoModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        self.user = self.infoModel.item;
        self.headerView.data = self.user;
        [self.tableView.header endRefreshing];
        [self.tableView reloadData];
        
        if (error) {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
    };
}

- (void)userWormholeModelInit {
    @weakify(self);
    self.userWormholeModel = [[UserWormholeListModel alloc] init];
    self.userWormholeModel.userId = self.userID;
    self.userWormholeModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self);
        if (error) {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
        [self validCell];
        [self.tableView reloadData];
    };
}

- (void)timelineModelInit {
    self.timelineModel = [[MyTimelineModel alloc] init];
    self.timelineModel.userID = self.userID;
    @weakify(self)
    self.timelineModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        if (error == nil) {
            [self validCell];
            [self setupRefreshFooterView];
            if (self.timelineModel.items.count) {
                if (self.timelineModel.more) {
                    [self.tableView.footer endRefreshing];
                } else {
                    [self.tableView.footer noticeNoMoreData];
                }
            }
        } else {
            [self.view presentMessage:error.message withTips:@"获取数据失败"];
        }
        [self.tableView reloadData];
        [self performSelector:@selector(stopAnimation:) withObject:self.headerView.refreshImageView afterDelay:1];
    };
}

//下拉动画
- (void)runAnimation:(UIImageView *)imageView {
    CGRect frame = imageView.frame;
    CGFloat orginY = [SDiOSVersion deviceSize] == Screen5Dot8inch ? 55 : 30;
    frame.origin.y = orginY;
    imageView.frame = frame;
    imageView.alpha = 1;
    [UIView animateWithDuration:1 animations:^{
        if (imageView.layer.animationKeys) {
            return;
        }
        [self.timelineModel refresh];
        NSMutableArray *allImages = [NSMutableArray array];
        for (NSInteger i = 1; i < 35; i++) {
            UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"icon_refresh_%ld", i]];
            [allImages addObject:image];
        }
        imageView.animationImages = allImages;
        imageView.animationDuration = 1;
        imageView.animationRepeatCount = 0;
        [imageView startAnimating];
    }];
}

- (void)stopAnimation:(UIImageView *)imageView {
    [imageView stopAnimating];
    imageView.image = [UIImage imageNamed:@"icon_refresh_ok"];
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = imageView.frame;
        frame.origin.y = -15;
        imageView.frame = frame;
    } completion:^(BOOL finished) {
        imageView.alpha = 0;
    }];
}

- (void)setupRefreshFooterView {
    if (self.tableView.footer == nil) {
        @weakify(self)
        [self.tableView addFooterPullLoader:^{
            @strongify(self)
            [self.timelineModel loadMore];
        }];
    } else {
        if (self.timelineModel.isEmpty) {
            [self.tableView removeFooter];
        }
    }
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (scrollView.contentOffset.y < 0) {
        CGFloat imageHeight = (kScreenWidth * 260 / 375);
        CGFloat topInset = [SDiOSVersion deviceSize] == Screen5Dot8inch ? -44 : -20;
        CGFloat height = imageHeight - scrollView.contentOffset.y - topInset;
        CGRect frame = CGRectMake(0, scrollView.contentOffset.y, self.view.frame.size.width, height);
        self.headerView.backgroundImageView.frame = frame;
        self.headerView.shadowView.frame = frame;
    }

    CGFloat offsetY = scrollView.contentOffset.y;
    
    @weakify(self)
    CGFloat navigationHeight = [SDiOSVersion deviceSize] == Screen5Dot8inch ? 88 : 64;
    CGFloat changePoint = 50;
    if (offsetY > changePoint) {
        CGFloat alpha = MIN(1, 1 - ((changePoint + navigationHeight - offsetY) / navigationHeight));
        [self.navigationController.navigationBar lt_setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:alpha]];
        self.navigationItem.title = self.user.name;
        self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
            @strongify(self)
            [self closeController];
        }];
        [self setNeedsStatusBarAppearanceUpdate];
    } else {
        [self.navigationController.navigationBar lt_setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0]];
        self.navigationItem.title = nil;
        self.navigationItem.leftBarButtonItem = [AppTheme whiteBackItemWithHandler:^(id sender) {
            @strongify(self)
            [self closeController];
        }];
        [self setNeedsStatusBarAppearanceUpdate];
    }
    
//    if (offsetY < -44) {
//        [self refreshAnimation];
//    }
//
//    CGFloat currentPostionY = scrollView.contentOffset.y;
//
//    if (currentPostionY - _lastPosition > 0) {
//        // 向下
//        _lastPosition = currentPostionY;
//        self.isDownScroll = YES;
//    } else if (_lastPosition - currentPostionY > 0) {
//        // 向上
//        _lastPosition = currentPostionY;
//        self.isDownScroll = NO;
//    } else {
//        self.isDownScroll = NO;
//    }
//
//    if (scrollView.contentOffset.y < 0) {
//        self.isDownScroll = NO;
//    }
//
//    if (scrollView.contentOffset.y == 0) {
//        if (self.isFirstLoad) {
//            self.isDownScroll = YES;
//        } else {
//            self.isDownScroll = NO;
//        }
//    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    CGFloat lastContentOffset = 0;
    CGFloat offset = scrollView.contentOffset.y - lastContentOffset;
    lastContentOffset = scrollView.contentOffset.y;
    if (offset < 0) {
        [self runAnimation:self.headerView.refreshImageView];
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.cellsArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *identifier = self.cellsArray[section];
    if ([identifier isEqualToString:@"ProfileWormholeCell"]) {
        return 1;
    } else if ([identifier isEqualToString:@"ProfileTimelineCell"]) {
        return self.timelineModel.items.count;
    } else {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = self.cellsArray[indexPath.section];
    if ([identifier isEqualToString:@"ProfileWormholeCell"]) {
        ProfileWormholeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProfileWormholeCell" forIndexPath:indexPath];
        cell.wormholeUser = WORMHOLE_USER_TYPE_TA;
        cell.user = self.user;
        cell.data = self.userWormholeModel;
        cell.wormholeAction = ^(WORMHOLE *wormhole) {
            WormholeVideoListController *wormholeVideoListController = [WormholeVideoListController spawn];
            wormholeVideoListController.wormholeID = wormhole.id;
            wormholeVideoListController.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:wormholeVideoListController animated:YES];
        };
        return cell;
    } else if ([identifier isEqualToString:@"ProfileTimelineCell"]) {
        ProfileTimelineCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProfileTimelineCell" forIndexPath:indexPath];
        cell.isFirstCell = (indexPath.row == 0);
        cell.isLastCell = (indexPath.row == self.timelineModel.items.count - 1);
        cell.user = self.user;
        cell.data = self.timelineModel.items[indexPath.row];
        return cell;
    }
    return [UITableViewCell new];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = self.cellsArray[indexPath.section];
    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    if ([identifier isEqualToString:@"ProfileWormholeCell"]) {
        UserWormholeListModel *userWormholeModel = cell.data;
        if (userWormholeModel.items.count == 0) {
            return;
        }
        UserWormholeListController *wormholeListController = [UserWormholeListController spawn];
        wormholeListController.userId = self.user.id;
        wormholeListController.wormholeUser = WORMHOLE_USER_TA;
        wormholeListController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:wormholeListController animated:YES];
    } else if ([identifier isEqualToString:@"ProfileTimelineCell"]) {
        TIMELINE *timeline = self.timelineModel.items[indexPath.row];
        NSString *link = timeline.link;
        if (link && link.length) {
            if (![DeepLink handleURLString:link withCompletion:nil]) {
                [[UIApplication sharedApplication].keyWindow presentFailureTips:@"无法打开的链接"];
            }
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    NSString *identifier = self.cellsArray[section];
    if ([identifier isEqualToString:@"Header"]) {
        return self.headerHight;
    } else if ([identifier isEqualToString:@"ProfileTimelineCell"]) {
        return 60;
    } else {
        return 10;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *identifier = self.cellsArray[section];
    if ([identifier isEqualToString:@"Header"]) {
        return self.headerView;
    } else if ([identifier isEqualToString:@"ProfileTimelineCell"]) {
        TimelineHeaderView *header = [TimelineHeaderView loadFromNib];
        header.isUserMine = NO;
        return header;
    } else {
        UIView *header = [[UIView alloc] init];
        header.backgroundColor = [AppTheme LoginPlaceholderColor];
        return header;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = self.cellsArray[indexPath.section];
    if ([identifier isEqualToString:@"Header"]) {
        return CGFLOAT_MIN;
    } else if ([identifier isEqualToString:@"ProfileWormholeCell"]) {
        if (self.userWormholeModel.items.count > 0) {
            return 178;
        } else {
            return 48;
        }
        
    } else if ([identifier isEqualToString:@"ProfileTimelineCell"]) {
        TIMELINE *timeline = self.timelineModel.items[indexPath.row];
        return [ProfileTimelineCell heightForRowWithTimeline:timeline];
    }
    return MAX(ceil(kScreenWidth * 207 / 750), 104);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

#pragma mark - UINavigationControllerDelegate

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if (navigationController.viewControllers.count > 1) {
        UIViewController *imageVC = navigationController.viewControllers[1];
        if ([imageVC isEqual:viewController]) {
            __weak typeof(imageVC) weakImageVC = imageVC;
            imageVC.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
                [weakImageVC.navigationController popViewControllerAnimated:YES];
            }];
        }
    }
}

@end
