//
//  SearchViewController.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "SearchViewController.h"
#import "UICollectionViewLeftAlignedLayout.h"
#import "SearchResultController.h"
#import "VideoInfoController.h"
#import "TopicVideoListController.h"
#import "AuthorInfoController.h"
#import "UserController.h"
#import "WormholeVideoListController.h"

#import "SearchNavTitleView.h"
#import "SearchTypeListView.h"
#import "SearchSectionCell.h"
#import "SearchHistoryItemCell.h"
#import "SearchHotCell.h"
#import "PredicateListHeaderCell.h"
#import "SearchVideosResultCell.h"
#import "SearchTopicsResultCell.h"
#import "SearchWormholeCollectionCell.h"
#import "SearchUsersResuleCell.h"

#import "SearchModel.h"
#import "KeywordsModel.h"
#import "AuthorModel.h"

#import "MPBackGuideView.h"

typedef NS_ENUM(NSInteger, LIST_NAME) {
    LIST_NAME_KEYWORDS = 0,
    LIST_NAME_PREDICATE = 1,
};

@interface SearchViewController () <UICollectionViewDelegate, UICollectionViewDataSource, UIGestureRecognizerDelegate, SearchNavTitleViewDelegate>

@property (weak, nonatomic) IBOutlet UICollectionView *keywordsList;
@property (weak, nonatomic) IBOutlet UICollectionView *predicateList;
@property (weak, nonatomic) IBOutlet UIView *loadingView;
@property (weak, nonatomic) IBOutlet UIImageView *loadingImage;

@property (nonatomic, strong) SearchNavTitleView *searchTitleView;
@property (nonatomic, strong) SearchTypeListView *searchTypeList;

@property (nonatomic, strong) SearchModel *searchModel;
@property (nonatomic, strong) KeywordsModel *keywordsModel;
@property (nonatomic, strong) NSMutableArray *itemIndexs;
@property (nonatomic, strong) NSMutableArray *keywordsListDataSource;
@property (nonatomic, strong) NSMutableArray *predicateListDataSource;

@property (nonatomic, assign) int lastPosition;
@property (nonatomic, assign) BOOL isDownScroll;
@property (nonatomic, assign) BOOL isFirstLoad;

@end

@implementation SearchViewController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Search"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self customize];
    [self setupList];
    [self modelInit];
    
    self.isDownScroll = YES;
    self.isFirstLoad = YES;
    
    NSMutableArray *animateImages = [NSMutableArray array];
    for (int i = 1; i < 7; i++) {
        NSString *imageName = [NSString stringWithFormat:@"icon_search_sep%d", i];
        UIImage *image = [UIImage imageNamed:imageName];
        [animateImages addObject:image];
    }
    self.loadingImage.animationImages = animateImages;
    self.loadingImage.animationRepeatCount = 0;
    self.loadingImage.animationDuration = 1.2;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.searchTitleView.keyword = self.keyword;
    if (self.actionState == SearchActionStatePending) {
        // 默认为开始搜索状态
        [self.searchTitleView becomeFirstResponder];
        [self prepareSearch];
    } else if (self.actionState == SearchActionStateDone) {
        // 搜索完成状态
        [self loadKeywordsData];
        if (self.searchModel.loaded == NO) {
            [self search];
            [self updateList];
        }
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    if (self.isFirstLoad) {
        [self.searchTitleView startAnimation];
        self.isFirstLoad = NO;
    }
    
    // 结果页不显示引导
//    [self showFeatureGuideViewIfNeeded];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    self.searchTitleView.searchTypeButton.selected = NO;
    self.searchTypeList.hidden = YES;
    self.isDownScroll = NO;
    [self.searchTitleView resignFirstResponder];
}

#pragma mark -

- (void)customize {
    @weakify(self);
    self.navigationItem.backBarButtonItem = nil;
    self.navigationItem.hidesBackButton = YES;
    self.navigationItem.leftBarButtonItem = nil;
    self.navigationItem.rightBarButtonItem = [AppTheme normalItemWithContent:@"取消" handler:^(id sender) {
        @strongify(self);
        self.searchTitleView.searchTypeButton.selected = NO;
        self.searchTypeList.hidden = YES;
        [self.navigationController popViewControllerAnimated:NO];
    }];
    
    self.searchTitleView = [[SearchNavTitleView alloc] initWithFrame:CGRectMake(12, 15, self.view.width - 60, 30)];
    [self.searchTitleView setType:self.searchType];
    self.searchTitleView.delegate = self;
    self.navigationItem.titleView = self.searchTitleView;
    self.searchTitleView.typeButtonBlock = ^(BOOL buttonSelected) {
        @strongify(self)
        if (buttonSelected) {
            self.searchTypeList.hidden = NO;
        } else {
            self.searchTypeList.hidden = YES;
        }
    };
    
//    self.searchTypeList = [[SearchTypeListView alloc] initWithFrame:CGRectMake(12, 60, 90, 233)];
    self.searchTypeList = [[SearchTypeListView alloc] initWithFrame:CGRectMake(12, -4, 90, 233)];
    self.searchTypeList.hidden = YES;
//    [[UIApplication sharedApplication].keyWindow addSubview:self.searchTypeList];
    [self.view addSubview:self.searchTypeList];
    self.searchTypeList.searchTypeBlock = ^(SEARCH_TYPE searchType) {
        @strongify(self)
        [self.searchTitleView setType:searchType];
        self.searchType = searchType;
        self.searchTitleView.searchTypeButton.selected = NO;
        self.searchTypeList.hidden = YES;
        if (self.keywordsList.hidden == YES && self.predicateList.hidden == NO) {
            [self search];
        }
    };
}

- (void)setupList {
    self.keywordsList.alwaysBounceVertical = YES;
    
    self.predicateList.backgroundColor = [AppTheme backgroundColor];
    self.predicateList.alwaysBounceVertical = YES;
    self.predicateList.contentInset = UIEdgeInsetsMake(0, 0, 20, 0);
    
    UICollectionViewLeftAlignedLayout *layout = [UICollectionViewLeftAlignedLayout new];
    self.keywordsList.collectionViewLayout = layout;
    
    [self registerNibs];
    UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapGestureAction)];
    tapGestureRecognizer.delegate = self;
    [self.view addGestureRecognizer:tapGestureRecognizer];
}

- (void)reloadSearch {
    [self search];
    [self updateList];
}

#pragma mark -

- (void)registerNibs {
    [self.keywordsList registerNib:[SearchSectionCell nib] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"SearchSectionCell"];
    [self.keywordsList registerNib:[SearchHistoryItemCell nib] forCellWithReuseIdentifier:@"SearchHistoryItemCell"];
    [self.keywordsList registerNib:[SearchHotCell nib] forCellWithReuseIdentifier:@"SearchHotCell"];
    
    [self.predicateList registerNib:[PredicateListHeaderCell nib] forSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"PredicateListHeaderCell"];
    [self.predicateList registerNib:[EmptyCollectionCell nib] forCellWithReuseIdentifier:@"EmptyCollectionCell"];
    [self.predicateList registerNib:[SearchVideosResultCell nib] forCellWithReuseIdentifier:@"SearchVideosResultCell"];
    [self.predicateList registerNib:[SearchTopicsResultCell nib] forCellWithReuseIdentifier:@"SearchTopicsResultCell"];
    [self.predicateList registerClass:[SearchWormholeCollectionCell class] forCellWithReuseIdentifier:@"SearchWormholeCollectionCell"];
    [self.predicateList registerNib:[SearchUsersResuleCell nib] forCellWithReuseIdentifier:@"SearchUsersResuleCell"];
}

#pragma mark -

- (void)keywordsListData {
    self.keywordsListDataSource = [NSMutableArray arrayWithCapacity:2];
    if (self.keywordsModel.historyKeywords.count > 0) {
        [self.keywordsListDataSource addObject:@(LIST_NAME_KEYWORDS)];
    }
    if (self.searchHotModel.items.count > 0) {
        [self.keywordsListDataSource addObject:@(LIST_NAME_PREDICATE)];
    }
}

- (void)modelInit {
    self.itemIndexs = [NSMutableArray array];
    
    @weakify(self);
    self.searchModel = [[SearchModel alloc] init];
    self.searchModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self);
        [self.predicateList.header endRefreshing];
        [self updateList];
        
        self.loadingView.hidden = YES;
        [self.loadingImage stopAnimating];
        [self dismissTips];
        
        if (error == nil) {
            [self setupRefreshFooterView];
            if (self.searchModel.more) {
                [self.predicateList.footer endRefreshing];
            } else {
                [self.predicateList.footer noticeNoMoreData];
            }
        } else {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
        self.predicateListDataSource = [NSMutableArray array];
        
        if (self.searchModel.videoArray.count > 0 && (self.searchType == SEARCH_TYPE_ALL || self.searchType == SEARCH_TYPE_VIDEO)) {
            [self.predicateListDataSource addObject:@(SEARCH_TYPE_VIDEO)];
        }
        if (self.searchModel.topicArray.count > 0 && (self.searchType == SEARCH_TYPE_ALL || self.searchType == SEARCH_TYPE_TOPIC)) {
            [self.predicateListDataSource addObject:@(SEARCH_TYPE_TOPIC)];
        }
        if (self.searchModel.wormholeArray.count > 0 && (self.searchType == SEARCH_TYPE_ALL || self.searchType == SEARCH_TYPE_WORMHOLE)) {
            [self.predicateListDataSource addObject:@(SEARCH_TYPE_WORMHOLE)];
        }
        if (self.searchModel.authorArray.count > 0 && (self.searchType == SEARCH_TYPE_ALL || self.searchType == SEARCH_TYPE_AUTHOR)) {
            [self.predicateListDataSource addObject:@(SEARCH_TYPE_AUTHOR)];
        }
        if (self.searchModel.userArray.count > 0 && (self.searchType == SEARCH_TYPE_ALL || self.searchType == SEARCH_TYPE_USER)) {
            [self.predicateListDataSource addObject:@(SEARCH_TYPE_USER)];
        }
        
        [self.predicateList reloadData];
    };
    
    self.keywordsModel = [[KeywordsModel alloc] init];
    
    [self setupRefreshHeaderView];
}

#pragma mark -

- (void)updateList {
    if (self.actionState == SearchActionStatePending) {
        self.keywordsList.hidden = NO;
        self.predicateList.hidden = YES;
    } else if (self.actionState == SearchActionStateDone) {
        self.keywordsList.hidden = YES;
        self.predicateList.hidden = NO;
    }
}

#pragma mark -

- (void)prepareSearch {
    [self updateList];
    
    [self loadKeywordsData];
    
    [self keywordsListData];
    [self.keywordsList reloadData];
}

#pragma mark - SearchTitleViewDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    return YES;
}

- (void)textFieldTextDidChange:(UITextField *)textField{
    NSString *searchText = textField.text;
    if (searchText && searchText.length) {
        if (searchText.length > 30) {
            textField.text = [searchText substringToIndex:30];
        }
//        self.actionState = SearchActionStateSearching;
//        [self updateList];
//        [self.keywordsList reloadData];
//        self.keyword = searchText;
//        [self search];
    } else {
        self.actionState = SearchActionStatePending;
        [self prepareSearch];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    NSString *searchText = textField.text;
    if (searchText && searchText.length) {
        [self.searchTitleView resignFirstResponder];
        self.actionState = SearchActionStateDone;
        if (self.searchType != SEARCH_TYPE_ALL) {
            [self updateList];
        }
        [self keywordsListData];
        [self.keywordsList reloadData];
        self.keyword = textField.text;
        [self search];
        
        return YES;
    } else {
        [textField endEditing:YES];
        [self presentMessageTips:@"请输入要搜索的内容"];
        return NO;
    }
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    self.keyword = nil;
    self.actionState = SearchActionStatePending;
    [self updateList];
    return YES;
}

#pragma mark - history keywords

- (void)loadHistoryKeywords {
    [self.keywordsModel loadHistoryKeywordsCache];
}

- (void)saveHistoryKeywords {
    if (self.keyword && self.keyword.length) {
        // 搜索时，存入历史搜索
        [self.keywordsModel addKeyword:self.keyword];
        self.itemIndexs = [self lastItemIndexs];
    }
}

#pragma mark -

- (void)loadKeywordsData {
    [self loadHistoryKeywords];
}

handleSignal(delete) {
    [self delete];
}

- (void)delete {
    [self.keywordsModel removeKeywords];
    [self keywordsListData];
    [self.keywordsList reloadData];
}

#pragma mark - keyword search

- (void)search {
    [self.searchTitleView resignFirstResponder];
    [self saveHistoryKeywords];
    self.searchTypeList.hidden = YES;
    self.actionState = SearchActionStateDone;
    
//    if (self.searchModel.isEmpty) {
//        [self presentLoadingTips:nil];
    self.loadingView.hidden = NO;
    [self.loadingImage startAnimating];
//    }
    self.searchModel.keyword = self.keyword;
    self.searchModel.type = self.searchType;
    self.searchTitleView.keyword = self.keyword;
    [self.searchTitleView setType:self.searchType];
    
    [self.predicateList setContentOffset:CGPointZero];
    self.isDownScroll = YES;
    [self.searchModel refresh];
}

- (void)reset {
//    self.searchTitleView.keyword = nil;
//    [self.searchTitleView resignFirstResponder];
    self.keyword = nil;
    self.actionState = SearchActionStatePending;
    [self updateList];
}

#pragma mark -

#pragma mark - UICollectionViewDataSource

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    if (self.keywordsList == collectionView) {
        return self.keywordsListDataSource.count;
    } else {
        if ([self isPredicateListEmpty]) {
            return 1;
        } else {
            return self.predicateListDataSource.count;
        }
    }
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (self.keywordsList == collectionView) {
        LIST_NAME listName = [self.keywordsListDataSource[section] integerValue];
        switch (listName) {
            case LIST_NAME_KEYWORDS:
                return self.keywordsModel.historyKeywords.count;
                break;
            case LIST_NAME_PREDICATE:
                return self.searchHotModel.items.count;
                break;
                
            default:
                break;
        }
    } else if (self.predicateList == collectionView) {
        if ([self isPredicateListEmpty]) {
            return 1;
        } else {
            SEARCH_TYPE searchType = [self.predicateListDataSource[section] integerValue];
            switch (searchType) {
                case SEARCH_TYPE_VIDEO:
                    return 1;
                    break;
                case SEARCH_TYPE_TOPIC:
                    return 1;
                    break;
                case SEARCH_TYPE_WORMHOLE:
                    return 1;
                    break;
                case SEARCH_TYPE_AUTHOR: {
                    if (self.searchModel.authorArray.count < 3) {
                        return self.searchModel.authorArray.count;
                    } else {
                        return 2;
                    }
                }
                    break;
                case SEARCH_TYPE_USER: {
                    if (self.searchModel.userArray.count < 3) {
                        return self.searchModel.userArray.count;
                    } else {
                        return 2;
                    }
                }
                    break;
                    
                default:
                    break;
            }
        }
    }
    return 0;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.keywordsList == collectionView) {
        LIST_NAME listName = [self.keywordsListDataSource[indexPath.section] integerValue];
        switch (listName) {
            case LIST_NAME_KEYWORDS: {
                SearchHistoryItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SearchHistoryItemCell" forIndexPath:indexPath];
                cell.data = [self.keywordsModel.historyKeywords objectAtIndex:indexPath.item];
                return cell;
            }
                break;
            case LIST_NAME_PREDICATE: {
                SearchHotCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SearchHotCell" forIndexPath:indexPath];
                cell.data = self.searchHotModel.items[indexPath.item];
                return cell;
            }
                break;
                
            default:
                break;
        }
    } else if (self.predicateList == collectionView) {
        if ([self isPredicateListEmpty]) {
            EmptyCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"EmptyCollectionCell" forIndexPath:indexPath];
            return cell;
        } else {
            SEARCH_TYPE searchType = [self.predicateListDataSource[indexPath.section] integerValue];
            switch (searchType) {
                case SEARCH_TYPE_VIDEO: {
                    SearchVideosResultCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SearchVideosResultCell" forIndexPath:indexPath];
                    cell.data = self.searchModel.videoArray[indexPath.item];
                    cell.followAuthor = ^(USER *author, UIButton *sender) {
                        [self followAuthor:author sender:sender];
                    };
                    cell.pushAuthor = ^{
                        AuthorInfoController *authorInfo = [AuthorInfoController spawn];
                        VIDEO *video = self.searchModel.videoArray[indexPath.item];
                        USER *author = video.owner;
                        authorInfo.author = author;
                        authorInfo.followAuthor = ^(USER *author) {
                            [self.searchModel.items replaceObjectAtIndex:indexPath.row withObject:author];
                            [self.predicateList reloadData];
                        };
                        [self presentNavigationController:authorInfo];
                    };
                    return cell;
                }
                    break;
                case SEARCH_TYPE_TOPIC: {
                    SearchTopicsResultCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SearchTopicsResultCell" forIndexPath:indexPath];
                    cell.data = self.searchModel.topicArray[indexPath.item];
                    return cell;
                }
                    break;
                case SEARCH_TYPE_WORMHOLE: {
                    SearchWormholeCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SearchWormholeCollectionCell" forIndexPath:indexPath];
                    cell.data = self.searchModel.wormholeArray;
                    @weakify(self);
                    cell.pushInWormhole = ^(WORMHOLE *wormhole) {
                        @strongify(self);
                        WormholeVideoListController *wormholeController = [WormholeVideoListController spawn];
                        wormholeController.wormholeID = wormhole.id;
                        [self.navigationController pushViewController:wormholeController animated:YES];
                    };
                    return cell;
                }
                    break;
                case SEARCH_TYPE_AUTHOR: {
                    SearchUsersResuleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SearchUsersResuleCell" forIndexPath:indexPath];
                    cell.userType = USER_TYPE_AUTHOR;
                    cell.data = self.searchModel.authorArray[indexPath.item];
                    cell.followAuthor = ^(USER *author, UIButton *sender) {
                        [self followAuthor:author sender:sender];
                    };
                    return cell;
                }
                    break;
                case SEARCH_TYPE_USER: {
                    SearchUsersResuleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SearchUsersResuleCell" forIndexPath:indexPath];
                    cell.userType = USER_TYPE_USER;
                    cell.data = self.searchModel.userArray[indexPath.item];
                    return cell;
                }
                    break;
                    
                default:
                    break;
            }
        }
        
    }
    return [[UICollectionViewCell alloc] init];
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell * cell = [collectionView cellForItemAtIndexPath:indexPath];
    
    if (self.keywordsList == collectionView) {
        LIST_NAME listName = [self.keywordsListDataSource[indexPath.section] integerValue];
        switch (listName) {
            case LIST_NAME_KEYWORDS: {
                if (cell && [cell.data isKindOfClass:[NSString class]]) {
                    self.keyword = cell.data;
                    [self search];
                }
            }
                break;
            case LIST_NAME_PREDICATE: {
                HOT_SERACH *hotSearch = cell.data;
                self.keyword = hotSearch.title;
                self.searchType = hotSearch.type;
                [self search];
            }
                break;
                
            default:
                break;
        }
    } else if (self.predicateList == collectionView) {
        if ([self isPredicateListEmpty]) {
            return;
        }
        SEARCH_TYPE searchType = [self.predicateListDataSource[indexPath.section] integerValue];
        switch (searchType) {
            case SEARCH_TYPE_VIDEO: {
                VIDEO *video = self.searchModel.videoArray[indexPath.item];
                VideoInfoController *videoInfo = [VideoInfoController spawn];
                videoInfo.video = video;
                
                videoInfo.reloadVideo = ^(VIDEO *video) {
                    [self.searchModel.videoArray replaceObjectAtIndex:indexPath.item withObject:video];
                    [self.predicateList reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section]];
                };
//                [self presentNavigationController:videoInfo];
                videoInfo.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:videoInfo animated:YES];
            }
                break;
            case SEARCH_TYPE_TOPIC: {
                TopicVideoListController *topicVideoList = [TopicVideoListController spawn];
                topicVideoList.topic = self.searchModel.topicArray[indexPath.item];
                topicVideoList.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:topicVideoList animated:YES];
            }
                break;
            case SEARCH_TYPE_WORMHOLE: {
                
            }
                break;
            case SEARCH_TYPE_AUTHOR: {
                USER *author = self.searchModel.authorArray[indexPath.item];
                AuthorInfoController *authorInfo = [AuthorInfoController spawn];
                authorInfo.author = author;
                authorInfo.followAuthor = ^(USER *author) {
                    [self.searchModel.authorArray replaceObjectAtIndex:indexPath.item withObject:author];
                    [self.predicateList reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section]];
                };
                [self presentNavigationController:authorInfo];
            }
                break;
            case SEARCH_TYPE_USER: {
                USER *user = self.searchModel.userArray[indexPath.item];
                UserController *userController = [UserController spawn];
                userController.userID = user.id;
                [self.navigationController pushViewController:userController animated:YES];
            }
                break;
                
            default:
                break;
        }
    }
}

//- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
//    if (self.predicateList == collectionView && self.isDownScroll) {
//        if (![self isPredicateListEmpty]) {
//            if (indexPath.item < 3) {
//                NSInteger indexDelay = indexPath.item;
//                cell.alpha = 0;
//                [cell performSelector:@selector(startSingleViewAnimation) withObject:self afterDelay:indexDelay * 0.2];
//            } else {
//                [cell startSingleViewAnimation];
//            }
//        }
//    }
//}
//
//- (void)collectionView:(UICollectionView *)collectionView willDisplaySupplementaryView:(UICollectionReusableView *)view forElementKind:(NSString *)elementKind atIndexPath:(NSIndexPath *)indexPath {
//    if (self.predicateList == collectionView && self.isDownScroll) {
//        if (![self isPredicateListEmpty]) {
//            [view startSingleViewAnimation];
//        }
//    }
//}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.keywordsList == collectionView) {
        LIST_NAME listName = [self.keywordsListDataSource[indexPath.section] integerValue];
        switch (listName) {
            case LIST_NAME_KEYWORDS: {
                NSString *keyword = [self.keywordsModel.historyKeywords objectAtIndex:indexPath.item];
                if (keyword && keyword.length) {
                    return [AppTheme calculateCellSizeWithContent:keyword];
                }
            }
                break;
            case LIST_NAME_PREDICATE: {
                return CGSizeMake(kScreenWidth, 44);
            }
                break;
                
            default:
                break;
        }
    } else if (self.predicateList == collectionView) {
        if ([self isPredicateListEmpty]) {
            return CGSizeMake(collectionView.width, collectionView.height);
        } else {
            SEARCH_TYPE searchType = [self.predicateListDataSource[indexPath.section] integerValue];
            switch (searchType) {
                case SEARCH_TYPE_VIDEO: {
                    CGFloat cellHeight = ceil(kScreenWidth * 211 / 375);
                    return CGSizeMake(collectionView.width, cellHeight + 67);
                }
                    break;
                case SEARCH_TYPE_TOPIC: {
                    CGFloat cellHeight = ceil(kScreenWidth * 194 / 345);
                    return CGSizeMake(collectionView.width, cellHeight + 7);
                }
                    break;
                case SEARCH_TYPE_WORMHOLE:
                    return CGSizeMake(collectionView.width, 119);
                    break;
                case SEARCH_TYPE_AUTHOR:
                    return CGSizeMake(collectionView.width, 66);
                    break;
                case SEARCH_TYPE_USER:
                    return CGSizeMake(collectionView.width, 66);
                    break;
                    
                default:
                    break;
            }
        }
    }
    
    return CGSizeZero;
}


- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
    if (self.keywordsList == collectionView) {
        if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
            SearchSectionCell * cell = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"SearchSectionCell" forIndexPath:indexPath];
            LIST_NAME listName = [self.keywordsListDataSource[indexPath.section] integerValue];
            switch (listName) {
                case LIST_NAME_KEYWORDS:
                    cell.titleType = TITLE_TYPE_HISTORY;
                    break;
                case LIST_NAME_PREDICATE:
                    cell.titleType = TITLE_TYPE_HOTSEARCH;
                    break;
                    
                default:
                    break;
            }
            return cell;
        } else if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {
            return nil;
        }
    } else if (self.predicateList == collectionView) {
        if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
            if (![self isPredicateListEmpty]) {
                PredicateListHeaderCell * cell = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"PredicateListHeaderCell" forIndexPath:indexPath];
                cell.data = self.predicateListDataSource[indexPath.section];
                @weakify(self);
                cell.searchResultBlock = ^(SEARCH_TYPE searchType) {
                    @strongify(self);
                    SearchResultController *searchResult = [SearchResultController spawn];
                    searchResult.hidesBottomBarWhenPushed = YES;
                    searchResult.keyword = self.keyword;
                    searchResult.searchType = searchType;
                    [self.navigationController pushViewController:searchResult animated:YES];
                };
                return cell;
            }
        } else if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {
            return nil;
        }
    }
    
    return nil;
}


- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
    if (self.keywordsList == collectionView) {
        return CGSizeMake(collectionView.width, 45.f);
    } else if (self.predicateList == collectionView) {
        if (![self isPredicateListEmpty]) {
            return CGSizeMake(collectionView.width, 53.f);
            
        }
    }
    
    return CGSizeZero;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    return CGSizeZero;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (self.keywordsList == collectionView) {
        LIST_NAME listName = [self.keywordsListDataSource[section] integerValue];
        switch (listName) {
            case LIST_NAME_KEYWORDS:
                return UIEdgeInsetsMake(0, 15, 15, 15);
                break;
            case LIST_NAME_PREDICATE:
                return UIEdgeInsetsMake(0, 0, 0, 0);
                break;
                
            default:
                break;
        }
    } else if (self.predicateList == collectionView) {
        return UIEdgeInsetsMake(0, 0, 0, 0);
    }
    
    return UIEdgeInsetsZero;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    if (self.keywordsList == collectionView) {
        LIST_NAME listName = [self.keywordsListDataSource[section] integerValue];
        switch (listName) {
            case LIST_NAME_KEYWORDS:
                return 12;
                break;
                
            default:
                break;
        }
    }
    return CGFLOAT_MIN;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    if (self.keywordsList == collectionView) {
        LIST_NAME listName = [self.keywordsListDataSource[section] integerValue];
        switch (listName) {
            case LIST_NAME_KEYWORDS:
                return 12;
                break;
                
            default:
                break;
        }
    }
    return CGFLOAT_MIN;
}


//- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    CGFloat currentPostionY = scrollView.contentOffset.y;
//
//    if (currentPostionY - _lastPosition > 0) {
//        // 向下
//        _lastPosition = currentPostionY;
//        self.isDownScroll = YES;
//    } else if (_lastPosition - currentPostionY > 0) {
//        // 向上
//        _lastPosition = currentPostionY;
//        self.isDownScroll = NO;
//    }
//
//    if (scrollView.contentOffset.y < 0) {
//        self.isDownScroll = NO;
//    }
//
//    if (scrollView.contentOffset.y == 0) {
//        self.isDownScroll = NO;
//    }
//}

#pragma mark - Gesture Recognizer Action

- (void)tapGestureAction {
    self.searchTypeList.hidden = YES;
}

#pragma mark - Gesture Recognizer Delegate

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldReceiveTouch:(UITouch *)touch {
    if (![NSStringFromClass([touch.view class]) isEqualToString:@"UICollectionView"]) {
        return NO;
    }
    return YES;
}

#pragma mark -

- (BOOL)isKeywordsListEmpty {
    if (self.keywordsModel.historyKeywords && self.keywordsModel.historyKeywords.count) {
        return NO;
    }
    
    return YES;
}

- (BOOL)isPredicateListEmpty {
    switch (self.searchType) {
        case SEARCH_TYPE_ALL: {
            if (self.predicateListDataSource.count == 0) {
                return YES;
            }
        }
            break;
        case SEARCH_TYPE_VIDEO: {
            if (self.searchModel.videoArray.count == 0) {
                return YES;
            }
        }
            break;
        case SEARCH_TYPE_TOPIC: {
            if (self.searchModel.topicArray.count == 0) {
                return YES;
            }
        }
            break;
        case SEARCH_TYPE_WORMHOLE: {
            if (self.searchModel.wormholeArray.count == 0) {
                return YES;
            }
        }
            break;
        case SEARCH_TYPE_AUTHOR: {
            if (self.searchModel.authorArray.count == 0) {
                return YES;
            }
        }
            break;
        case SEARCH_TYPE_USER: {
            if (self.searchModel.userArray.count == 0) {
                return YES;
            }
        }
            break;
            
        default:
            break;
    }
    
    return NO;
}

- (BOOL)isLastItemInRow:(NSInteger)index {
    if (self.keywordsModel.historyKeywords && self.keywordsModel.historyKeywords.count) {
        if (self.keywordsModel.historyKeywords.count == 1) {
            return YES;
        } else {
            NSMutableArray *indexs = self.itemIndexs;
            if (indexs && indexs.count) {
                if ([indexs containsObject:@(index)]) {
                    return YES;
                }
            }
        }
    }
    return NO;
}

- (NSMutableArray *)lastItemIndexs {
    NSMutableArray *indexs = [NSMutableArray array];
    CGFloat width = 0.f;
    for (int i = 0; i < self.keywordsModel.historyKeywords.count; i++) {
        NSString * keyword = [self.keywordsModel.historyKeywords objectAtIndex:i];
        if (keyword && keyword.length) {
            CGFloat itemWidth = [AppTheme calculateCellSizeWithContent:keyword].width;
            width += itemWidth;
            CGFloat itemMargin = 4.f;
            if (width >= (self.view.width - 2 * itemMargin)) {
                [indexs addObject:@(i - 1)];
                width = itemWidth;
            }
        }
    }
    if (![indexs containsObject:@(self.keywordsModel.historyKeywords.count - 1)]) {
        [indexs addObject:@(self.keywordsModel.historyKeywords.count - 1)];
    }
    return indexs;
}

#pragma mark - Header

- (void)setupRefreshHeaderView {
    @weakify(self)
    [self.predicateList addHeaderPullLoader:^{
        @strongify(self)
        [self.searchModel refresh];
    }];
}

- (void)setupRefreshFooterView {
    if (self.predicateList.footer == nil) {
        if (!self.searchModel.isEmpty) {
            @weakify(self)
            [self.predicateList addFooterPullLoader:^{
                @strongify(self)
                [self.searchModel loadMore];
            }];
        }
    } else {
        if (self.searchModel.isEmpty) {
            [self.predicateList removeFooter];
        }
    }
}

#pragma mark - Author Follow

- (void)followAuthor:(USER *)author sender:(UIButton *)sender {
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }
    
    [sender followAuthorButtonAnimated];
    
    [sender disableSelf];
    [self presentLoadingTips:nil];
    if (author.is_follow) {
        [AuthorModel unfollow:author then:^(STIHTTPResponseError *error) {
            if (error == nil) {
                author.is_follow = NO;
                [self.predicateList reloadData];
//                [self.model refresh];
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
            [self dismissTips];
            [sender enableSelf];
        }];
    } else {
        if (!author.is_author) {
            [self dismissTips];
            [[UIApplication sharedApplication].keyWindow presentMessageTips:@"作者不存在！"];
            return;
        }
        
        [AuthorModel follow:author then:^(STIHTTPResponseError *error) {
            if (error == nil) {
                author.is_follow = YES;
                [self.predicateList reloadData];
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
            [self dismissTips];
            [sender enableSelf];
        }];
    }
}

#pragma mark - Guide

// 显示新手引导图
- (void)showFeatureGuideViewIfNeeded {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    BOOL showGuide = [ud boolForKey:@"showSearchResultGuide"];
    if (!showGuide) {
        [self showFeatureGuideView];
    }
}

- (void)showFeatureGuideView {
    MPBackGuideView *guideView = [MPBackGuideView showBackGuide];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (guideView) {
            [guideView dismiss];
        }
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        [ud setBool:YES forKey:@"showSearchResultGuide"];
        [ud synchronize];
    });
}

@end
