//
//  UserController.h
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/4/20.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserController : UIViewController

@property (nonatomic, copy) NSString *userID;

@end
