//
//  SearchResultController.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/4/17.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "SearchResultController.h"
#import "VideoInfoController.h"
#import "TopicVideoListController.h"
#import "AuthorInfoController.h"
#import "UserController.h"
#import "WormholeVideoListController.h"

#import "SearchTopicsResultCell.h"
#import "SearchUsersResuleCell.h"
#import "SearchWormholeResultItem.h"
#import "SearchVideosResultCell.h"

#import "SearchResultModel.h"
#import "AuthorModel.h"

@interface SearchResultController () <UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *searchResultList;
@property (weak, nonatomic) IBOutlet UIView *loadingView;
@property (weak, nonatomic) IBOutlet UIImageView *loadingImage;

@property (nonatomic, strong) SearchResultModel *searchResultModel;

@property (nonatomic, assign) int lastPosition;
@property (nonatomic, assign) BOOL isDownScroll;

@end

@implementation SearchResultController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Search"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self customize];
    [self setupList];
    [self modelInit];
    
    self.isDownScroll = YES;
    
    NSMutableArray *animateImages = [NSMutableArray array];
    for (int i = 1; i < 7; i++) {
        NSString *imageName = [NSString stringWithFormat:@"icon_search_sep%d", i];
        UIImage *image = [UIImage imageNamed:imageName];
        [animateImages addObject:image];
    }
    self.loadingImage.animationImages = animateImages;
    self.loadingImage.animationRepeatCount = 0;
    self.loadingImage.animationDuration = 1.2;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self search];
}

- (void)customize {
    NSString *titleString;
    switch (self.searchType) {
        case SEARCH_TYPE_VIDEO:
            titleString = @"全部短片探索结果";
            break;
        case SEARCH_TYPE_TOPIC:
            titleString = @"全部话题探索结果";
            break;
        case SEARCH_TYPE_WORMHOLE:
            titleString = @"全部虫洞探索结果";
            break;
        case SEARCH_TYPE_AUTHOR:
            titleString = @"全部作者探索结果";
            break;
        case SEARCH_TYPE_USER:
            titleString = @"全部用户探索结果";
            break;
            
        default:
            break;
    }
    
    @weakify(self)
    self.navigationItem.title = titleString;
    self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
        @strongify(self)
        if (self.searchBackBlock) {
            self.searchBackBlock();
        }
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

#pragma mark - keyword search

- (void)search {
    self.searchResultModel.loaded = YES;
//    if (self.searchResultModel.isEmpty) {
//        [self presentLoadingTips:nil];
    self.loadingView.hidden = NO;
    [self.loadingImage startAnimating];
//    }
    self.searchResultModel.keyword = self.keyword;
    self.searchResultModel.type = self.searchType;
    
    [self.searchResultList setContentOffset:CGPointZero];
    self.isDownScroll = YES;
    [self.searchResultModel refresh];
}

#pragma mark -

- (void)modelInit {
    @weakify(self);
    self.searchResultModel = [[SearchResultModel alloc] init];
    self.searchResultModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self);
        
        [self.searchResultList.header endRefreshing];
        self.loadingView.hidden = YES;
        [self.loadingImage stopAnimating];
        [self dismissTips];
        
        if (error == nil) {
            [self setupRefreshFooterView];
            if (self.searchResultModel.more) {
                [self.searchResultList.footer endRefreshing];
            } else {
                [self.searchResultList.footer setTitle:MJRefreshFooterStateNoMoreDataText forState:MJRefreshFooterStateNoMoreData];
                [self.searchResultList.footer noticeNoMoreData];
            }
        } else {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
        [self.searchResultList reloadData];
    };
    
    [self setupRefreshHeaderView];
}

- (void)setupList {
    self.searchResultList.backgroundColor = [AppTheme backgroundColor];
    self.searchResultList.alwaysBounceVertical = YES;
    self.searchResultList.contentInset = UIEdgeInsetsMake(0, 0, 20, 0);
    [self registerNibs];
}

- (void)registerNibs {
    [self.searchResultList registerNib:[EmptyCollectionCell nib] forCellWithReuseIdentifier:@"EmptyCollectionCell"];
    [self.searchResultList registerNib:[SearchVideosResultCell nib] forCellWithReuseIdentifier:@"SearchVideosResultCell"];
    [self.searchResultList registerNib:[SearchTopicsResultCell nib] forCellWithReuseIdentifier:@"SearchTopicsResultCell"];
    [self.searchResultList registerNib:[SearchWormholeResultItem nib] forCellWithReuseIdentifier:@"SearchWormholeResultItem"];
    [self.searchResultList registerNib:[SearchUsersResuleCell nib] forCellWithReuseIdentifier:@"SearchUsersResuleCell"];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if ([self isPredicateListEmpty]) {
        return 1;
    } else {
        return self.searchResultModel.items.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self isPredicateListEmpty]) {
        EmptyCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"EmptyCollectionCell" forIndexPath:indexPath];
        return cell;
    } else {
        switch (self.searchType) {
            case SEARCH_TYPE_VIDEO: {
                SearchVideosResultCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SearchVideosResultCell" forIndexPath:indexPath];
                cell.data = [self.searchResultModel.items objectAtIndex:indexPath.item];
                cell.followAuthor = ^(USER *author, UIButton *sender) {
                    [self followAuthor:author sender:sender];
                };
                cell.pushAuthor = ^{
                    AuthorInfoController *authorInfo = [AuthorInfoController spawn];
                    VIDEO *video = [self.searchResultModel.items objectAtIndex:indexPath.item];
                    USER *author = video.owner;
                    authorInfo.author = author;
                    authorInfo.followAuthor = ^(USER *author) {
                        [self.searchResultModel.items replaceObjectAtIndex:indexPath.row withObject:author];
                        [self.searchResultList reloadData];
                    };
                    [self presentNavigationController:authorInfo];
                };
                return cell;
            }
                break;
            case SEARCH_TYPE_TOPIC: {
                SearchTopicsResultCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SearchTopicsResultCell" forIndexPath:indexPath];
                cell.data = [self.searchResultModel.items objectAtIndex:indexPath.item];
                return cell;
            }
                break;
            case SEARCH_TYPE_WORMHOLE: {
                SearchWormholeResultItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SearchWormholeResultItem" forIndexPath:indexPath];
                cell.data = [self.searchResultModel.items objectAtIndex:indexPath.item];
                return cell;
            }
                break;
            case SEARCH_TYPE_AUTHOR: {
                SearchUsersResuleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SearchUsersResuleCell" forIndexPath:indexPath];
                cell.userType = USER_TYPE_AUTHOR;
                cell.data = [self.searchResultModel.items objectAtIndex:indexPath.item];
                cell.followAuthor = ^(USER *author, UIButton *sender) {
                    [self followAuthor:author sender:sender];
                };
                return cell;
            }
                break;
            case SEARCH_TYPE_USER: {
                SearchUsersResuleCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SearchUsersResuleCell" forIndexPath:indexPath];
                cell.userType = USER_TYPE_USER;
                cell.data = [self.searchResultModel.items objectAtIndex:indexPath.item];
                return cell;
            }
                break;
                
            default:
                break;
        }
    }
    
    return [[UICollectionViewCell alloc] init];
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell * cell = [collectionView cellForItemAtIndexPath:indexPath];
    
    switch (self.searchType) {
        case SEARCH_TYPE_VIDEO: {
            VIDEO *video = cell.data;
            VideoInfoController *videoInfo = [VideoInfoController spawn];
            videoInfo.video = video;
            
            videoInfo.reloadVideo = ^(VIDEO *video) {
                [self.searchResultModel.items replaceObjectAtIndex:indexPath.row withObject:video];
                [self.searchResultList reloadData];
            };
//            [self presentNavigationController:videoInfo];
            videoInfo.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:videoInfo animated:YES];
        }
            break;
        case SEARCH_TYPE_TOPIC: {
            TopicVideoListController *topicVideoList = [TopicVideoListController spawn];
            topicVideoList.topic = cell.data;
            topicVideoList.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:topicVideoList animated:YES];
        }
            break;
        case SEARCH_TYPE_WORMHOLE: {
            WORMHOLE *wormhole = cell.data;
            WormholeVideoListController *wormholeController = [WormholeVideoListController spawn];
            wormholeController.wormholeID = wormhole.id;
            [self.navigationController pushViewController:wormholeController animated:YES];
        }
            break;
        case SEARCH_TYPE_AUTHOR: {
            AuthorInfoController *authorInfo = [AuthorInfoController spawn];
            USER *author = cell.data;
            authorInfo.author = author;
            authorInfo.followAuthor = ^(USER *author) {
                [self.searchResultModel.items replaceObjectAtIndex:indexPath.row withObject:author];
                [self.searchResultList reloadData];
            };
            [self presentNavigationController:authorInfo];
        }
            break;
        case SEARCH_TYPE_USER: {
            USER *user = cell.data;
            UserController *otherUsers = [UserController spawn];
            otherUsers.userID = user.id;
            otherUsers.hidesBottomBarWhenPushed = YES;
            [self.navigationController pushViewController:otherUsers animated:YES];
        }
            break;
        default:
            break;
    }
}

//- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
//    if ( self.isDownScroll) {
//        if (![self isPredicateListEmpty]) {
//            if (indexPath.item < 3) {
//                NSInteger indexDelay = indexPath.row;
//                cell.alpha = 0;
//                [cell performSelector:@selector(startSingleViewAnimation) withObject:self afterDelay:indexDelay * 0.2];
//            } else {
//                [cell startSingleViewAnimation];
//            }
//        }
//    }
//}
//
//- (void)collectionView:(UICollectionView *)collectionView willDisplaySupplementaryView:(UICollectionReusableView *)view forElementKind:(NSString *)elementKind atIndexPath:(NSIndexPath *)indexPath {
//    if ( self.isDownScroll) {
//        if (![self isPredicateListEmpty]) {
//            [view startSingleViewAnimation];
//        }
//    }
//}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self isPredicateListEmpty]) {
        return CGSizeMake(collectionView.width, collectionView.height);
    } else {
        switch (self.searchType) {
            case SEARCH_TYPE_VIDEO: {
                CGFloat cellHeight = ceil(kScreenWidth * 211 / 375);
                return CGSizeMake(collectionView.width, cellHeight + 67);
            }
                break;
            case SEARCH_TYPE_TOPIC: {
                CGFloat cellHeight = ceil(kScreenWidth * 194 / 345);
                return CGSizeMake(collectionView.width, cellHeight + 7);
            }
                break;
            case SEARCH_TYPE_WORMHOLE: {
                CGFloat cellHeight = ceil(kScreenWidth * 211 / 375);
                return CGSizeMake(collectionView.width, cellHeight + 67);
            }
                break;
            case SEARCH_TYPE_AUTHOR:
                return CGSizeMake(collectionView.width, 67);
                break;
            case SEARCH_TYPE_USER:
                return CGSizeMake(collectionView.width, 67);
                break;
            default:
                break;
        }
    }
    return CGSizeZero;
}

//- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
//    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
//        PredicateListHeaderCell * cell = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"PredicateListHeaderCell" forIndexPath:indexPath];
//        cell.data = self.searchResultModel.total;
//        return cell;
//    } else if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {
//        return nil;
//    }
//
//    return nil;
//}


//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
//    return CGSizeMake(collectionView.width, 53.f);
//}

//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
//    return CGSizeZero;
//}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0, 0, 0, 0);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return CGFLOAT_MIN;
}

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    CGFloat currentPostionY = scrollView.contentOffset.y;
//
//    if (currentPostionY - _lastPosition > 0) {
//        // 向下
//        _lastPosition = currentPostionY;
//        self.isDownScroll = YES;
//    } else if (_lastPosition - currentPostionY > 0) {
//        // 向上
//        _lastPosition = currentPostionY;
//        self.isDownScroll = NO;
//    }
//
//    if (scrollView.contentOffset.y < 0) {
//        self.isDownScroll = NO;
//    }
//
//    if (scrollView.contentOffset.y == 0) {
//        self.isDownScroll = NO;
//    }
//}

#pragma mark -

- (BOOL)isPredicateListEmpty {
    if (self.searchResultModel.items.count == 0) {
        return YES;
    }
    return NO;
}

#pragma mark - Header && Footer

- (void)setupRefreshHeaderView {
    @weakify(self)
    [self.searchResultList addHeaderPullLoader:^{
        @strongify(self)
        [self.searchResultModel refresh];
    }];
}

- (void)setupRefreshFooterView {
    if (self.searchResultList.footer == nil) {
        if (!self.searchResultModel.isEmpty) {
            @weakify(self)
            [self.searchResultList addFooterPullLoader:^{
                @strongify(self)
                [self.searchResultModel loadMore];
            }];
        }
    } else {
        if (self.searchResultModel.isEmpty) {
            [self.searchResultList removeFooter];
        }
    }
}

#pragma mark - Author Follow

- (void)followAuthor:(USER *)author sender:(UIButton *)sender {
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }
    
    [sender followAuthorButtonAnimated];
    
    [sender disableSelf];
    [self presentLoadingTips:nil];
    if (author.is_follow) {
        [AuthorModel unfollow:author then:^(STIHTTPResponseError *error) {
            if (error == nil) {
                author.is_follow = NO;
                [self.searchResultList reloadData];
//                [self.model refresh];
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
            [self dismissTips];
            [sender enableSelf];
        }];
    } else {
        if (!author.is_author) {
            [self dismissTips];
            [[UIApplication sharedApplication].keyWindow presentMessageTips:@"作者不存在！"];
            return;
        }
        
        [AuthorModel follow:author then:^(STIHTTPResponseError *error) {
            if (error == nil) {
                author.is_follow = YES;
                [self.searchResultList reloadData];
//                [self.model refresh];
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
            [self dismissTips];
            [sender enableSelf];
        }];
    }
}

@end
