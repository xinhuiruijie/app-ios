//
//  SearchHotCell.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/4/17.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "SearchHotCell.h"

@interface SearchHotCell()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *pointLabel;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UIView *lineView;

@end

@implementation SearchHotCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.nameLabel.textColor = [AppTheme normalTextColor];
    self.pointLabel.backgroundColor = [AppTheme subTitleTextColor];
    self.typeLabel.textColor = [AppTheme subTitleTextColor];
    self.lineView.backgroundColor = [AppTheme searchLineColor];
}

- (void)dataDidChange {
    HOT_SERACH *hotSearch = self.data;
    self.nameLabel.text = hotSearch.title;
    switch (hotSearch.type) {
        case SEARCH_TYPE_ALL:
            self.typeLabel.text = @"全部";
            break;
        case SEARCH_TYPE_VIDEO:
            self.typeLabel.text = @"短片";
            break;
        case SEARCH_TYPE_TOPIC:
            self.typeLabel.text = @"话题";
            break;
        case SEARCH_TYPE_WORMHOLE:
            self.typeLabel.text = @"虫洞";
            break;
        case SEARCH_TYPE_AUTHOR:
            self.typeLabel.text = @"作者";
            break;
        case SEARCH_TYPE_USER:
            self.typeLabel.text = @"用户";
            break;
            
        default:
            break;
    }
}

@end
