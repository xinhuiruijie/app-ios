//
//  UserTableHeaderView.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/18.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

typedef NS_ENUM(NSUInteger, COUNT_TYPE) {
    COUNT_TYPE_FOLLOW = 1, // 关注
    COUNT_TYPE_COLLECT = 2, // 收藏
};

#import "UserTableHeaderView.h"
#import "MSNumberScrollAnimatedView.h"

@interface UserTableHeaderView ()

@property (weak, nonatomic) IBOutlet UIView *headImageBackgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImage;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *genderImageView;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UIImageView *locationImage;
@property (weak, nonatomic) IBOutlet UILabel *constellationLabel;
@property (weak, nonatomic) IBOutlet UIImageView *constellationImage;
@property (weak, nonatomic) IBOutlet UILabel *proPageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *proPageImage;
@property (weak, nonatomic) IBOutlet UILabel *introduceLabel;
@property (weak, nonatomic) IBOutlet UILabel *followCount;
@property (weak, nonatomic) IBOutlet UILabel *collectCount;

@property (weak, nonatomic) IBOutlet MSNumberScrollAnimatedView *followCountNumberAnimated;
@property (weak, nonatomic) IBOutlet MSNumberScrollAnimatedView *collectCountNumberAnimated;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *followCountNAWLC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectCountNAWLC;

@end

@implementation UserTableHeaderView

- (void)awakeFromNib {
    [super awakeFromNib];
    [self customize];
    self.refreshImageView = [[UIImageView alloc] initWithFrame:CGRectMake(30, 15, 22, 22)];
    [self addSubview:self.refreshImageView];
    // 页面无刷新，动画隐藏
    self.refreshImageView.hidden = YES;

    [self customAnimatedView:self.followCountNumberAnimated];
    [self customAnimatedView:self.collectCountNumberAnimated];
}

- (void)customize {
    self.headImageBackgroundView.layer.cornerRadius = self.headImageBackgroundView.frame.size.width / 2;
    self.headImageBackgroundView.layer.masksToBounds = YES;
    self.avatarImage.layer.cornerRadius = self.avatarImage.frame.size.width / 2;
    self.avatarImage.layer.masksToBounds = YES;
    
    self.backgroundImageView.image = [UIImage imageNamed:@"bg_lab"];
    self.avatarImage.image = [AppTheme avatarDefaultImage];
    self.locationImage.hidden = YES;
    self.constellationImage.hidden = YES;
    self.proPageImage.hidden = YES;
}

- (void)dataDidChange {
    if ([self.data isKindOfClass:[USER class]]) {
        USER *user = self.data;
        [self.backgroundImageView setImageWithPhoto:user.cover_photo placeholderImage:[UIImage imageNamed:@"bg_lab"]];
        [self.avatarImage setImageWithPhoto:user.avatar placeholderImage:[AppTheme avatarDefaultImage]];
        self.nameLabel.text = user.name ?: [AppTheme defaultPlaceholder];
        if (user.gender == USER_GENDER_MALE) {
            self.genderImageView.image = [UIImage imageNamed:@"icon_man_sel"];
        } else {
            self.genderImageView.image = [UIImage imageNamed:@"icon_woman_sel"];
        }
        
        self.locationLabel.text = user.location ?: [AppTheme defaultPlaceholder];
        if (!user.location) {
            self.locationImage.hidden = YES;
        } else {
            self.locationImage.hidden = NO;
        }
        
        self.constellationLabel.text = user.constellation ?: [AppTheme defaultPlaceholder];
        if (!user.constellation) {
            self.constellationImage.hidden = YES;
        } else {
            self.constellationImage.hidden = NO;
            self.constellationImage.image = [AppTheme imageOfConstellation:user.constellation];
        }
        
        if (!user.birthday) {
            self.proPageImage.hidden = YES;
        } else {
            self.proPageLabel.text = [self memberOfProPageLabel:user.birthday];
            self.proPageImage.hidden = NO;
        }
        
        self.introduceLabel.text = user.introduce ?: [AppTheme defaultPlaceholder];
        
        [self animationActionWithCount:user.follow_count countType:COUNT_TYPE_FOLLOW];
        [self animationActionWithCount:user.collect_count countType:COUNT_TYPE_COLLECT];
    }
}

- (NSString *)memberOfProPageLabel:(NSString *)birthday {
    if (birthday && ![birthday isEqualToString:@""] && birthday.length >= 3) {
        NSString *stringRange2 = [birthday substringWithRange:NSMakeRange(2, 1)];
        return [NSString stringWithFormat:@"%@0后", stringRange2];
    } else {
        return [AppTheme defaultPlaceholder];
    }
}

#pragma mark - Custom Number Animated View

- (void)customAnimatedView:(MSNumberScrollAnimatedView *)view {
    view.font = [UIFont fontWithName:@"Gotham-Medium" size:20];
    view.textColor = [AppTheme normalTextColor];
    view.minLength = 1;
    view.isIntager = YES;
    view.number = 0;
    [view startAnimation];
    [view stopAnimation];
}

#pragma mark - Animation Action

- (void)animationActionWithCount:(NSNumber *)count countType:(COUNT_TYPE)countType {
    if (!count) {
        count = [NSNumber numberWithInteger:0];
    }
    
    NSString *countString;
    NSLayoutConstraint *constraint;
    MSNumberScrollAnimatedView *animatedView;
    
    switch (countType) {
        case COUNT_TYPE_FOLLOW: {
            constraint = self.followCountNAWLC;
            animatedView = self.followCountNumberAnimated;
        }
            break;
            
        case COUNT_TYPE_COLLECT: {
            constraint = self.collectCountNAWLC;
            animatedView = self.collectCountNumberAnimated;
        }
            break;
            
        default:
            break;
    }
    
    countString = [NSString stringWithFormat:@"%@", count];
    constraint.constant = countString.length * 13;
    
    // 延迟动画加载，怀疑修改 constraint.constant 为异步，造成 frame 未修改结束便开始动画，造成布局错误，其他两处亦同
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        animatedView.number = count;
        [animatedView startAnimation];
        if ([count integerValue] == 0) {
            [animatedView stopAnimation];
        }
    });
    
}

@end
