//
//  SearchTitleView.h
//  GeekMall
//
//  Created by liuyadi on 2016/12/13.
//  Copyright © 2016年 GeekZooStudio. All rights reserved.
//

#import <UIKit/UIKit.h>

#pragma mark -

#pragma mark -

@protocol SearchTitleViewDelegate <NSObject>

@optional

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;        // return NO to disallow editing.
- (void)textFieldDidBeginEditing:(UITextField *)textField;           // became first responder
- (void)textFieldDidEndEditing:(UITextField *)textField;             // may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;   // return NO to not change text

- (BOOL)textFieldShouldReturn:(UITextField *)textField;              // called when 'return' key pressed. return NO to ignore.
- (BOOL)textFieldShouldClear:(UITextField *)textField;               // called when clear button pressed. return NO to ignore (no notifications)
- (void)textFieldTextDidChange:(UITextField *)textField;

@end

#pragma mark -

@interface SearchTitleView : UIView

@property (nonatomic, weak) id<SearchTitleViewDelegate> delegate;

@property (nonatomic, strong) NSString *keyword;
@property (nonatomic, strong) NSString *placeholder;

- (BOOL)becomeFirstResponder;
- (BOOL)resignFirstResponder;
- (void)startAnimation;

@end
