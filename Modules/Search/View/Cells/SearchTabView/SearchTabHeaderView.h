//
//  SearchTabHeaderView.h
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/4/24.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchTabHeaderView : UIView

@property (nonatomic, copy) void(^searchActionBlock)(void);

@end
