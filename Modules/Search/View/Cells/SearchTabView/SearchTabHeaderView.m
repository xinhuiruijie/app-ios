//
//  SearchTabHeaderView.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/4/24.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "SearchTabHeaderView.h"
#import "SearchTitleView.h"

@interface SearchTabHeaderView () <SearchTitleViewDelegate>

@end

@implementation SearchTabHeaderView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        SearchTitleView *searchTitleView = [[SearchTitleView alloc] initWithFrame:CGRectMake(15, 10, frame.size.width - 30, 30)];
        searchTitleView.delegate = self;
        [self addSubview:searchTitleView];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(15, 10, frame.size.width - 30, 30);
        [button addTarget:self action:@selector(buttonAction) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
    }
    
    return self;
}

- (void)buttonAction {
    if (self.searchActionBlock) {
        self.searchActionBlock();
    }
}

@end
