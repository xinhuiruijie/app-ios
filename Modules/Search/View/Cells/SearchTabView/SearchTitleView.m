//
//  SearchTitleView.m
//  GeekMall
//
//  Created by liuyadi on 2016/12/13.
//  Copyright © 2016年 GeekZooStudio. All rights reserved.
//

#import "SearchTitleView.h"

@interface SearchTitleView() <UITextFieldDelegate>

@property (nonatomic, strong) UIImageView *searchImage;
@property (nonatomic, strong) UITextField *searchTextField;
@property (nonatomic, strong) UIButton *clearButton;

@end

@implementation SearchTitleView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRGBValue:0xE9ECF0];
        self.layer.cornerRadius = 3;
        self.layer.masksToBounds = YES;
        
        self.searchImage = [[UIImageView alloc] initWithFrame:CGRectMake(6, 8, 15, 15)];
        self.searchImage.contentMode = UIViewContentModeCenter;
        self.searchImage.image = [UIImage imageNamed:@"icon_search_input"];
        [self addSubview:self.searchImage];
        
        
        self.searchTextField = [[UITextField alloc] initWithFrame:CGRectMake(26, 0, (frame.size.width - 26), frame.size.height)];
        self.searchTextField.placeholder = @"#发现不可思议#";
        self.searchTextField.textColor = [AppTheme normalTextColor];
        self.searchTextField.font = [UIFont systemFontOfSize:12];
        self.searchTextField.delegate = self;
        self.searchTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        self.searchTextField.returnKeyType = UIReturnKeySearch;
        [self addSubview:self.searchTextField];
        [self.searchTextField addTarget:self action:@selector(textFieldTextDidChange:) forControlEvents:UIControlEventEditingChanged];
        
//        self.searchImage.alpha = 0;
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
//    self.searchTextField.frame = CGRectMake(26, 0, (self.width - 26), self.height);
}

#pragma mark -

- (void)setKeyword:(NSString *)keyword {
    _keyword = keyword;
    self.searchTextField.text = keyword;
}

- (void)setPlaceholder:(NSString *)placeholder {
    _placeholder = placeholder;
    self.searchTextField.placeholder = placeholder;
}

- (BOOL)becomeFirstResponder {
    return [self.searchTextField becomeFirstResponder];
}

- (BOOL)resignFirstResponder {
    return [self.searchTextField resignFirstResponder];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (self.delegate && [self.delegate respondsToSelector:@selector(textFieldShouldBeginEditing:)]) {
        return [self.delegate textFieldShouldBeginEditing:textField];
    }
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (self.delegate && [self.delegate respondsToSelector:@selector(textFieldDidBeginEditing:)]) {
        [self.delegate textFieldDidBeginEditing:textField];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (self.delegate && [self.delegate respondsToSelector:@selector(textFieldDidEndEditing:)]) {
        [self.delegate textFieldDidEndEditing:textField];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (self.delegate && [self.delegate respondsToSelector:@selector(textField:shouldChangeCharactersInRange:replacementString:)]) {
        if (!self.searchTextField.rightView) {
            self.searchTextField.rightView = self.clearButton;
            self.searchTextField.rightViewMode = UITextFieldViewModeWhileEditing;
        }
        return [self.delegate textField:textField shouldChangeCharactersInRange:range replacementString:string];
    }
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (self.delegate && [self.delegate respondsToSelector:@selector(textFieldShouldReturn:)]) {
        return [self.delegate textFieldShouldReturn:textField];
    }
    return NO;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    if (self.delegate && [self.delegate respondsToSelector:@selector(textFieldShouldClear:)]) {
        return [self.delegate textFieldShouldClear:textField];
    }
    return NO;
}

- (void)textFieldTextDidChange:(UITextField *)textField {
    if (self.delegate && [self.delegate respondsToSelector:@selector(textFieldTextDidChange:)]) {
        [self.delegate textFieldTextDidChange:textField];
    }
}

- (void)startAnimation {
    // 这个只能操作frame，操作layer失败
//    self.layer.anchorPoint = CGPointMake(0.5, 0.5);
//    [UIView animateWithDuration:0.3 animations:^{
//        self.searchImage.alpha = 1;
//        self.width = kScreenWidth - 50;
//        self.x = -90;
//    } completion:nil];
    
    self.alpha = 0;
    [UIView animateWithDuration:0.6 delay:0.1 options:UIViewAnimationOptionCurveLinear animations:^{
        self.alpha = 1.0;
    } completion:nil];
}

@end
