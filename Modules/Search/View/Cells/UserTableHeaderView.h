//
//  UserTableHeaderView.h
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/18.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UserTableHeaderView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (strong, nonatomic)  UIImageView *refreshImageView;

@end
