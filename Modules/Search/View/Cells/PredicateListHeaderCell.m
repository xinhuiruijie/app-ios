//
//  PredicateListHeaderCell.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/4/17.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "PredicateListHeaderCell.h"

@interface PredicateListHeaderCell()

@property (weak, nonatomic) IBOutlet UILabel *typeTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *moreLabel;
@property (weak, nonatomic) IBOutlet UIButton *moreButton;

@property (nonatomic, assign) SEARCH_TYPE searchType;

@end

@implementation PredicateListHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.typeTitleLabel.textColor = [AppTheme normalTextColor];
    self.typeTitleLabel.font = [UIFont fontWithName:@"PingFangSC-Semibold" size:20];
    self.moreLabel.textColor = [AppTheme subButtonTextColor];
}

- (void)dataDidChange {
    SEARCH_TYPE searchType = [self.data integerValue];
    self.searchType = searchType;
    switch (searchType) {
        case SEARCH_TYPE_VIDEO:{
            self.typeTitleLabel.text = @"短片";
        }
            break;
        case SEARCH_TYPE_TOPIC:{
            self.typeTitleLabel.text = @"话题";
        }
            break;
        case SEARCH_TYPE_WORMHOLE:{
            self.typeTitleLabel.text = @"虫洞";
        }
            break;
        case SEARCH_TYPE_AUTHOR:{
            self.typeTitleLabel.text = @"作者";
        }
            break;
        case SEARCH_TYPE_USER:{
            self.typeTitleLabel.text = @"用户";
        }
            break;
            
        default:
            break;
    }
}

#pragma mark - Action

- (IBAction)moreButtonAction:(UIButton *)sender {
    if (self.searchResultBlock) {
        self.searchResultBlock(self.searchType);
    }
}

@end
