//
//  SearchTypeListView.h
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/4/16.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchTypeListView : UIView

@property (nonatomic, copy) void(^searchTypeBlock)(SEARCH_TYPE searchType);

@end
