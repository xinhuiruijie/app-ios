//
//  SearchTypeListView.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/4/16.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "SearchTypeListView.h"
#import "SearchTypeListCell.h"

@interface SearchTypeListView() <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UIImageView *typeListBackImage;
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, copy) NSArray *typeListData;

@end

@implementation SearchTypeListView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        self.typeListData = @[@"全部", @"短片", @"话题", @"虫洞", @"作者", @"用户"];
        
        self.typeListBackImage = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        self.typeListBackImage.alpha = 0.5f;
        self.typeListBackImage.image = [UIImage imageNamed:@"bg_all_search_bubble"];
        [self addSubview:self.typeListBackImage];
        
        UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:UIBlurEffectStyleExtraLight];
        UIVisualEffectView *effectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
        [self.typeListBackImage addSubview:effectView];
        effectView.alpha = 0.8f;
        effectView.frame = CGRectMake(0, 5, frame.size.width, frame.size.height - 5);
        [self addSubview:effectView];
        
        self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 5, frame.size.width, frame.size.height - 5) style:UITableViewStylePlain];
        self.tableView.backgroundColor = [UIColor clearColor];
        self.tableView.scrollEnabled = NO;
        self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        self.tableView.delegate = self;
        self.tableView.dataSource = self;
        [self.tableView registerClass:[SearchTypeListCell class] forCellReuseIdentifier:@"SearchTypeListCell"];
        self.tableView.tableFooterView = [UIView new];
        [self addSubview:self.tableView];
    }
    
    return self;
}

#pragma mark - Table View Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.typeListData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SearchTypeListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SearchTypeListCell" forIndexPath:indexPath];
    cell.data = self.typeListData[indexPath.row];
    return cell;
}

#pragma mark - Table View Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 38;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.searchTypeBlock) {
        SEARCH_TYPE searchType = indexPath.row + 1;
        self.searchTypeBlock(searchType);
    }
}

@end
