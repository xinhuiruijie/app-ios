//
//  SearchNavTitleView.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/4/13.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "SearchNavTitleView.h"

@interface SearchNavTitleView() <UITextFieldDelegate>

@property (nonatomic, strong) UIImageView *searchImage;
@property (nonatomic, strong) UILabel *searchTypeLabel;
@property (nonatomic, strong) UITextField *searchTextField;
@property (nonatomic, strong) UIButton *clearButton;

@end

@implementation SearchNavTitleView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor colorWithRGBValue:0xE9ECF0];
        self.layer.cornerRadius = 3;
        self.layer.masksToBounds = YES;
        
        self.searchTypeLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, 30, frame.size.height)];
        self.searchTypeLabel.text = @"全部";
        self.searchTypeLabel.font = [UIFont systemFontOfSize:14];
        self.searchTypeLabel.textColor = [AppTheme normalTextColor];
        [self addSubview:self.searchTypeLabel];
        
        UIImageView *searchImage = [[UIImageView alloc] initWithFrame:CGRectMake(45, 13, 6, 4)];
        searchImage.contentMode = UIViewContentModeCenter;
        searchImage.image = [UIImage imageNamed:@"icon_arrow_fold_down"];
        [self addSubview:searchImage];
        self.searchImage = searchImage;
        
        UIButton *searchTypeButton = [UIButton buttonWithType:UIButtonTypeCustom];
        searchTypeButton.frame = CGRectMake(0, 0, 60, frame.size.height);
        searchTypeButton.selected = NO;
        [searchTypeButton addTarget:self action:@selector(searchTypeButtonAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:searchTypeButton];
        self.searchTypeButton = searchTypeButton;
        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(CGRectGetMaxX(searchTypeButton.frame), 0, [AppTheme onePixel], frame.size.height)];
        lineView.backgroundColor = [AppTheme lineColor];
        [self addSubview:lineView];
        
        UITextField *searchTextField = [[UITextField alloc] initWithFrame:CGRectMake(CGRectGetMaxX(searchTypeButton.frame) + 10, 0, (frame.size.width - CGRectGetMaxX(searchTypeButton.frame) - 6), frame.size.height)];
        searchTextField.placeholder = @"#发现不可思议#";
        searchTextField.textColor = [AppTheme normalTextColor];
        searchTextField.font = [UIFont systemFontOfSize:14];
        searchTextField.delegate = self;
        searchTextField.clearButtonMode = UITextFieldViewModeWhileEditing;
        searchTextField.returnKeyType = UIReturnKeySearch;
        [self addSubview:searchTextField];
        self.searchTextField = searchTextField;
        [self.searchTextField addTarget:self action:@selector(textFieldTextDidChange:) forControlEvents:UIControlEventEditingChanged];
    }
    return self;
}

- (void)setType:(SEARCH_TYPE)type {
    switch (type) {
        case SEARCH_TYPE_ALL:
            self.searchTypeLabel.text = @"全部";
            break;
        case SEARCH_TYPE_VIDEO:
            self.searchTypeLabel.text = @"短片";
            break;
        case SEARCH_TYPE_TOPIC:
            self.searchTypeLabel.text = @"话题";
            break;
        case SEARCH_TYPE_WORMHOLE:
            self.searchTypeLabel.text = @"虫洞";
            break;
        case SEARCH_TYPE_AUTHOR:
            self.searchTypeLabel.text = @"作者";
            break;
        case SEARCH_TYPE_USER:
            self.searchTypeLabel.text = @"用户";
            break;
            
        default:
            break;
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.searchTextField.frame = CGRectMake(CGRectGetMaxX(self.searchTypeButton.frame) + 10, 0, (self.width - CGRectGetMaxX(self.searchTypeButton.frame) - 6), self.height);
}

#pragma mark -

- (void)setKeyword:(NSString *)keyword {
    _keyword = keyword;
    self.searchTextField.text = keyword;
}

- (void)setPlaceholder:(NSString *)placeholder {
    _placeholder = placeholder;
    self.searchTextField.placeholder = placeholder;
}

- (BOOL)becomeFirstResponder {
    return [self.searchTextField becomeFirstResponder];
}

- (BOOL)resignFirstResponder {
    return [self.searchTextField resignFirstResponder];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (self.delegate && [self.delegate respondsToSelector:@selector(textFieldShouldBeginEditing:)]) {
        return [self.delegate textFieldShouldBeginEditing:textField];
    }
    return NO;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (self.delegate && [self.delegate respondsToSelector:@selector(textFieldDidBeginEditing:)]) {
        [self.delegate textFieldDidBeginEditing:textField];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (self.delegate && [self.delegate respondsToSelector:@selector(textFieldDidEndEditing:)]) {
        [self.delegate textFieldDidEndEditing:textField];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (self.delegate && [self.delegate respondsToSelector:@selector(textField:shouldChangeCharactersInRange:replacementString:)]) {
        if (!self.searchTextField.rightView) {
            self.searchTextField.rightView = self.clearButton;
            self.searchTextField.rightViewMode = UITextFieldViewModeWhileEditing;
        }
        return [self.delegate textField:textField shouldChangeCharactersInRange:range replacementString:string];
    }
    return NO;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (self.delegate && [self.delegate respondsToSelector:@selector(textFieldShouldReturn:)]) {
        return [self.delegate textFieldShouldReturn:textField];
    }
    return NO;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    if (self.delegate && [self.delegate respondsToSelector:@selector(textFieldShouldClear:)]) {
        return [self.delegate textFieldShouldClear:textField];
    }
    return NO;
}

- (void)textFieldTextDidChange:(UITextField *)textField {
    if (self.delegate && [self.delegate respondsToSelector:@selector(textFieldTextDidChange:)]) {
        [self.delegate textFieldTextDidChange:textField];
    }
}

- (void)startAnimation {
// 这个只能操作frame，操作layer失败
//    self.layer.anchorPoint = CGPointMake(0.5, 0.5);
//    [UIView animateWithDuration:0.3 animations:^{
//        self.searchImage.alpha = 1;
//        self.width = kScreenWidth - 50;
//        self.x = -90;
//    } completion:nil];
    
    self.alpha = 0;
    [UIView animateWithDuration:0.6 delay:0.1 options:UIViewAnimationOptionCurveLinear animations:^{
        self.alpha = 1.0;
    } completion:nil];
}

#pragma mark - Button Action

- (void)searchTypeButtonAction:(UIButton *)sender {
    if (sender.selected) {
        sender.selected = NO;
    } else {
        sender.selected = YES;
    }
    
    if (self.typeButtonBlock) {
        self.typeButtonBlock(sender.selected);
    }
}

@end
