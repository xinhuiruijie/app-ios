//
//  SearchNavTitleView.h
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/4/13.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

#pragma mark -

@protocol SearchNavTitleViewDelegate <NSObject>

@optional

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField;        // return NO to disallow editing.
- (void)textFieldDidBeginEditing:(UITextField *)textField;           // became first responder
- (void)textFieldDidEndEditing:(UITextField *)textField;             // may be called if forced even if shouldEndEditing returns NO (e.g. view removed from window) or endEditing:YES called

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;   // return NO to not change text

- (BOOL)textFieldShouldReturn:(UITextField *)textField;              // called when 'return' key pressed. return NO to ignore.
- (BOOL)textFieldShouldClear:(UITextField *)textField;               // called when clear button pressed. return NO to ignore (no notifications)
- (void)textFieldTextDidChange:(UITextField *)textField;

@end

#pragma mark -

@interface SearchNavTitleView : UIView

@property (nonatomic, weak) id<SearchNavTitleViewDelegate> delegate;

@property (nonatomic, assign) SEARCH_TYPE type;

@property (nonatomic, strong) UIButton *searchTypeButton;

@property (nonatomic, strong) NSString *keyword;
@property (nonatomic, strong) NSString *placeholder;

@property (nonatomic, copy) void(^typeButtonBlock)(BOOL buttonSelected);

- (BOOL)becomeFirstResponder;
- (BOOL)resignFirstResponder;
- (void)startAnimation;

- (void)setType:(SEARCH_TYPE)type;

@end
