//
//  SearchTypeListCell.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/4/16.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "SearchTypeListCell.h"

@interface SearchTypeListCell()

@property (nonatomic, strong) UILabel *typeTitleLabel;

@end

@implementation SearchTypeListCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UILabel *typeTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(16, 0, 90, 38)];
        typeTitleLabel.textColor = [AppTheme normalTextColor];
        typeTitleLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:typeTitleLabel];
        self.typeTitleLabel = typeTitleLabel;
        
        UIView *lineView = [[UIView alloc] initWithFrame:CGRectMake(0, 38, 90, [AppTheme onePixel])];
        lineView.backgroundColor = [AppTheme lineColor];
        [self addSubview:lineView];
    }
    
    return self;
}

- (void)dataDidChange {
    self.typeTitleLabel.text = self.data;
}

@end
