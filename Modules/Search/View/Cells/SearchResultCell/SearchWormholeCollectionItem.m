//
//  SearchWormholeCollectionItem.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/4/18.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "SearchWormholeCollectionItem.h"

@interface SearchWormholeCollectionItem()

@property (weak, nonatomic) IBOutlet UIImageView *wormholePhoto;
@property (weak, nonatomic) IBOutlet UIView *maskView;
@property (weak, nonatomic) IBOutlet UILabel *wormholeUserCountLabel;
@property (weak, nonatomic) IBOutlet UIView *subtitleView;
@property (weak, nonatomic) IBOutlet UILabel *wormholeSubtitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *wormholeTitleLabel;

@end

@implementation SearchWormholeCollectionItem

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.wormholePhoto.layer.cornerRadius = 2;
    self.wormholePhoto.clipsToBounds = YES;
    self.maskView.layer.cornerRadius = 2;
    self.maskView.clipsToBounds = YES;
    
    self.subtitleView.backgroundColor = [AppTheme fractionLabelTextColor];
    self.subtitleView.layer.cornerRadius = 2;
    
    self.wormholeTitleLabel.backgroundColor = [AppTheme textBackgroundColor];
    self.wormholeTitleLabel.alpha = 0.8;
}

- (void)dataDidChange {
    WORMHOLE *wormhole = self.data;
    
    self.wormholePhoto.image = [AppTheme placeholderImage];
    if (wormhole.photo) {
        [self.wormholePhoto setImageWithPhoto:wormhole.photo];
    } else {
        self.wormholePhoto.image = [AppTheme placeholderImage];
    }
    self.wormholeTitleLabel.text = [NSString stringWithFormat:@" %@",wormhole.title];
    self.wormholeUserCountLabel.text = [NSString stringWithFormat:@"%@",wormhole.user_count];
    self.wormholeSubtitleLabel.text = [NSString stringWithFormat:@" %@ ", wormhole.subtitle];
}

@end
