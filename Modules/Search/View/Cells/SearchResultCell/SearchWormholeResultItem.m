//
//  SearchWormholeResultItem.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/4/21.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "SearchWormholeResultItem.h"

@interface SearchWormholeResultItem ()

@property (weak, nonatomic) IBOutlet UIImageView *ownerAvatar;
@property (weak, nonatomic) IBOutlet UILabel *ownerNameLabel;
@property (weak, nonatomic) IBOutlet UIView *ownerLine;
@property (weak, nonatomic) IBOutlet UILabel *ownerIntroductionLabel;

@property (weak, nonatomic) IBOutlet UIImageView *wormholePhoto;
@property (weak, nonatomic) IBOutlet UILabel *wormholeTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *wormholeUserCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *wormholeSubtitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *wormholeContentLabel;
@property (weak, nonatomic) IBOutlet UILabel *wormholeCreatedAtLabel;

@end

@implementation SearchWormholeResultItem

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor = [UIColor whiteColor];
    
    self.ownerAvatar.layer.cornerRadius = 18;
    self.ownerAvatar.clipsToBounds = YES;
}

- (void)dataDidChange {
    WORMHOLE *wormhole = self.data;
    [self.ownerAvatar setImageWithPhoto:wormhole.owner.avatar placeholderImage:[AppTheme avatarDefaultImage]];
    self.ownerNameLabel.text = wormhole.owner.name;
    self.ownerIntroductionLabel.text = wormhole.owner.introduce;

    [self.wormholePhoto setImageWithPhoto:wormhole.photo placeholderImage:[AppTheme placeholderImage]];
    self.wormholeTitleLabel.text = wormhole.title;
    self.wormholeUserCountLabel.text = [NSString stringWithFormat:@"%@",wormhole.user_count?:@""];
    self.wormholeSubtitleLabel.text = wormhole.subtitle ? [NSString stringWithFormat:@"# %@", wormhole.subtitle] : @"";
    self.wormholeContentLabel.text = [NSString stringWithFormat:@"%ld条短片", wormhole.videos.count];
    self.wormholeCreatedAtLabel.text = [wormhole.created_at stringFromTimestamp];
}

- (IBAction)pushAuthorInfoAction:(UIButton *)sender {
    WORMHOLE *wormhole = self.data;
    if (self.authorInfoAction) {
        self.authorInfoAction(wormhole.owner);
    }
}

@end
