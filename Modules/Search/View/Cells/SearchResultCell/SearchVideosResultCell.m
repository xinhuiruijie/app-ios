//
//  SSearchVideosResultCell.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/4/17.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "SearchVideosResultCell.h"

@interface SearchVideosResultCell()

@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIImageView *vidoePhoto;
@property (weak, nonatomic) IBOutlet UILabel *videoPlayCount;
@property (weak, nonatomic) IBOutlet UILabel *videoCategory;
@property (weak, nonatomic) IBOutlet UILabel *videoTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *videoTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *videoStarCount;
@property (weak, nonatomic) IBOutlet UIImageView *ownerAvatar;
@property (weak, nonatomic) IBOutlet UILabel *ownerNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *ownerIntroduceLabel;
@property (weak, nonatomic) IBOutlet UIButton *followButton;

@end

@implementation SearchVideosResultCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.videoCategory.backgroundColor = [AppTheme typeLabelBackgroundColor];
    self.videoCategory.layer.cornerRadius = 2;
    self.videoCategory.clipsToBounds = YES;
    
    self.ownerAvatar.layer.cornerRadius = 18;
    self.ownerAvatar.clipsToBounds = YES;
}

- (void)dataDidChange {
    VIDEO *video = self.data;
    
    self.vidoePhoto.image = [AppTheme placeholderImage];
    if (video.photo) {
        [self.vidoePhoto setImageWithPhoto:video.photo];
    } else {
        self.vidoePhoto.image = [AppTheme placeholderImage];
    }
    
    float videoPlayCount = [video.play_count floatValue];
    if (videoPlayCount >= 10000) {
        self.videoPlayCount.text = [NSString stringWithFormat:@"%0.1fW", videoPlayCount / 10000];
    } else {
        self.videoPlayCount.text = [NSString stringWithFormat:@"%@", video.play_count?:@(0)];
    }
    
    self.videoCategory.text = video.category.title ? [NSString stringWithFormat:@" #%@  ", video.category.title] : @"";
    self.videoTimeLabel.text = video.video_time;
    self.videoTitleLabel.text = video.title;
    self.videoStarCount.text = [NSString stringWithFormat:@"%0.1f", video.star?:0.0];
    
    self.ownerAvatar.image = [AppTheme avatarDefaultImage];
    if (video.owner.avatar) {
        [self.ownerAvatar setImageWithPhoto:video.owner.avatar];
    } else {
        self.ownerAvatar.image = [AppTheme avatarDefaultImage];
    }
    
    self.ownerNameLabel.text = video.owner.name;
    self.ownerIntroduceLabel.text = video.owner.introduce;
    
    if (video.owner == nil || [video.owner.id isEqualToString:[UserModel sharedInstance].user.id]) {
        self.followButton.hidden = YES;
    } else {
        self.followButton.hidden = NO;
        if (video.owner.is_follow) {
            [self.followButton setImage:[UIImage imageNamed:@"icon_following"] forState:UIControlStateNormal];
        } else {
            [self.followButton setImage:[UIImage imageNamed:@"icon_follow"] forState:UIControlStateNormal];
        }
    }
}

#pragma mark - Action

- (IBAction)followButtonAction:(UIButton *)sender {
    VIDEO *video = self.data;
    USER *author = video.owner;
    if (self.followAuthor) {
        self.followAuthor(author, sender);
    }
}

- (IBAction)pushAuthorAction:(UIButton *)sender {
    if (self.pushAuthor) {
        self.pushAuthor();
    }
}

@end
