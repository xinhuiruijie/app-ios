//
//  SearchUsersResuleCell.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/4/17.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "SearchUsersResuleCell.h"

@interface SearchUsersResuleCell()

@property (weak, nonatomic) IBOutlet UIImageView *userAvatar;
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *userIntroduceLabel;
@property (weak, nonatomic) IBOutlet UIButton *followButton;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineHLC;

@end

@implementation SearchUsersResuleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor = [UIColor whiteColor];
    self.userAvatar.layer.cornerRadius = 18;
    self.userAvatar.clipsToBounds = YES;
    
    self.lineHLC.constant = [AppTheme onePixel];
}

- (void)dataDidChange {
    USER *user = self.data;
    
    if (self.userType == USER_TYPE_USER || [user.id isEqualToString:[UserModel sharedInstance].user.id]) {
        self.followButton.hidden = YES;
    } else {
        self.followButton.hidden = NO;
        
        if (user.is_follow && user.is_author) {
            [self.followButton setImage:[UIImage imageNamed:@"icon_following"] forState:UIControlStateNormal];
        } else {
            [self.followButton setImage:[UIImage imageNamed:@"icon_follow"] forState:UIControlStateNormal];
        }
    }
    
    self.userAvatar.image = [AppTheme avatarDefaultImage];
    if (user.avatar) {
        [self.userAvatar setImageWithPhoto:user.avatar];
    } else {
        self.userAvatar.image = [AppTheme avatarDefaultImage];
    }
    self.userNameLabel.text = user.name;
    self.userIntroduceLabel.text = user.introduce;
}

#pragma mark - Action

- (IBAction)followButtonAction:(UIButton *)sender {
    USER *author = self.data;
    if (self.followAuthor) {
        self.followAuthor(author, sender);
    }
}

@end
