//
//  SearchWormholeCollectionCell.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/4/18.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "SearchWormholeCollectionCell.h"
#import "SearchWormholeCollectionItem.h"

@interface SearchWormholeCollectionCell() <UICollectionViewDelegate, UICollectionViewDataSource>

@property (nonatomic, strong) UICollectionView *wormHoleCollection;

@property (nonatomic, strong) NSMutableArray *dataArray;

@end

@implementation SearchWormholeCollectionCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [AppTheme backgroundColor];
        self.dataArray = [NSMutableArray array];
        
        UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
        [layout setScrollDirection:UICollectionViewScrollDirectionHorizontal];
        
        self.wormHoleCollection = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 119) collectionViewLayout:layout];
        [self addSubview:self.wormHoleCollection];
        [self.wormHoleCollection setDelegate:self];
        [self.wormHoleCollection setDataSource:self];
        self.wormHoleCollection.backgroundColor = [AppTheme backgroundColor];
        self.wormHoleCollection.showsVerticalScrollIndicator = FALSE;
        self.wormHoleCollection.showsHorizontalScrollIndicator = FALSE;

        
        [self.wormHoleCollection registerNib:[SearchWormholeCollectionItem nib] forCellWithReuseIdentifier:@"SearchWormholeCollectionItem"];
    }
    return self;
}

- (void)dataDidChange {
    self.dataArray = self.data;
    [self.wormHoleCollection reloadData];
}

#pragma mark - Collection View Data Source

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView {
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.dataArray.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    SearchWormholeCollectionItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SearchWormholeCollectionItem" forIndexPath:indexPath];
    cell.data = [self.dataArray objectAtIndex:indexPath.item];
    return cell;
}

#pragma mark - Collection View Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell *cell = [collectionView cellForItemAtIndexPath:indexPath];
    WORMHOLE *wormhole = cell.data;
    
    if (self.pushInWormhole) {
        self.pushInWormhole(wormhole);
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(140, 109);
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    
    return UIEdgeInsetsMake(5, 15, 5, 15);
    
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 10;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return CGFLOAT_MIN;
}

@end
