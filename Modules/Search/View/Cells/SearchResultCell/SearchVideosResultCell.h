//
//  SearchVideosResultCell.h
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/4/17.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchVideosResultCell : UICollectionViewCell

@property (nonatomic, copy) void(^followAuthor)(USER *author, UIButton *sender);
@property (nonatomic, copy) void(^pushAuthor)(void);

@end
