//
//  SearchWormholeResultItem.h
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/4/21.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchWormholeResultItem : UICollectionViewCell

@property (nonatomic, copy) void(^authorInfoAction)(USER *author);

@end
