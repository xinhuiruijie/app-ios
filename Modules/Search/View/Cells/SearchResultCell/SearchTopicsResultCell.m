//
//  SearchTopicsResultCell.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/4/17.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "SearchTopicsResultCell.h"

@interface SearchTopicsResultCell()

@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIImageView *coverPhoto;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLable;

@end

@implementation SearchTopicsResultCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backView.layer.cornerRadius = 2;
    self.backView.clipsToBounds = YES;
}

- (void)dataDidChange {
    TOPIC *topic = self.data;
    [self.coverPhoto setImageWithPhoto:topic.cover_photo placeholderImage:[AppTheme placeholderImage]];
    self.titleLabel.text = topic.title ? [NSString stringWithFormat:@"# %@", topic.title] : @"";
    self.subtitleLable.text = topic.subtitle?:@"";
}

@end
