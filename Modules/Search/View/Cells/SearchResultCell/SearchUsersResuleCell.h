//
//  SearchUsersResuleCell.h
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/4/17.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, USER_TYPE) {
    USER_TYPE_AUTHOR = 1,
    USER_TYPE_USER = 2,
};

@interface SearchUsersResuleCell : UICollectionViewCell

@property (nonatomic, assign) USER_TYPE userType;
@property (nonatomic, copy) void(^followAuthor)(USER *author, UIButton *sender);

@end
