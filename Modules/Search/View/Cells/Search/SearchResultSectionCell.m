//
//  SearchResultSectionCell.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/21.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "SearchResultSectionCell.h"

@interface SearchResultSectionCell ()

@property (weak, nonatomic) IBOutlet UILabel *resultLabel;

@end

@implementation SearchResultSectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.backgroundColor = [UIColor whiteColor];
}

- (void)dataDidChange {
    NSNumber *total = self.data;
    self.resultLabel.text = [NSString stringWithFormat:@"%@条相关内容", total ? : @(0)];
}

@end
