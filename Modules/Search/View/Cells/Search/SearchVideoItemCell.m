//
//  SearchVideoItemCell.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "SearchVideoItemCell.h"
#import "VideoTagView.h"

@interface SearchVideoItemCell ()

@property (weak, nonatomic) IBOutlet UIImageView *videoPhoto;
@property (weak, nonatomic) IBOutlet UILabel *videoTitle;
@property (weak, nonatomic) IBOutlet UILabel *videoStar;
@property (weak, nonatomic) IBOutlet UIView *videoStarView;
@property (weak, nonatomic) IBOutlet UILabel *playCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *videoTimeLabel;
@property (weak, nonatomic) IBOutlet UIView *tagView;

@end

@implementation SearchVideoItemCell

+ (CGFloat)heightForSearchVideoItemRow {
    CGFloat imageHeight = ceil(kScreenWidth * 33 / 71);
    return (imageHeight + 60);
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
//    [self.contentView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        obj.backgroundColor = [UIColor randomColor];
//    }];
}

- (void)dataDidChange {
    VIDEO *video = self.data;
    
    self.videoTitle.text = video.title;
    [self.tagView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj removeFromSuperview];
    }];
    
//    CGFloat tagOriginX = 0;
//    CGFloat margin = 10;
//    CGFloat tagWidth = kScreenWidth - 120;
//    CGFloat maxWidth = floor((tagWidth - (margin * video.tags.count)) / (video.tags.count + 1));
//    for (int i = 0; i < video.tags.count + 1; i++) {
//        NSString *title = @"#";
//        if (i == 0) {
//            if (video.category) {
//                CATEGORY *category = video.category;
//                title = [title stringByAppendingString:category.title];
//            } else {
//                continue;
//            }
//        } else {
//            TAG *tag = video.tags[i - 1];
//            title = [title stringByAppendingString:tag.name];
//        }
//
//        CGFloat width = [AppTheme calculateWidthWithContent:title height:18] + 12;
//        width = MIN(maxWidth, width);
//
//        VideoTagView *tagSubview = [[VideoTagView alloc] initWithFrame:CGRectMake(tagOriginX, 0, width, 18)];
//        if (i == 0) {
//            tagSubview.type = VIDEO_CATEGORY;
//        } else {
//            tagSubview.type = VIDEO_TAG;
//        }
//        tagSubview.title = title;
//        tagOriginX += width;
//        tagOriginX += margin;
//
//        [self.tagView addSubview:tagSubview];
//    }
    
    CGFloat tagOriginX = 0;
    CGFloat tagWidth = kScreenWidth - 120;
    if (video.category) {
        CATEGORY *category = video.category;
        NSString *title = [NSString stringWithFormat:@"#%@", category.title ? : [AppTheme defaultPlaceholder]];
        
        CGFloat width = [AppTheme calculateWidthWithContent:title height:18] + 12;
        width = MIN(tagWidth, width);
        
        VideoTagView *tagSubview = [[VideoTagView alloc] initWithFrame:CGRectMake(tagOriginX, 0, width, 18)];
        tagSubview.type = VIDEO_CATEGORY;
        tagSubview.title = title;
        [self.tagView addSubview:tagSubview];
    }
    
    self.videoPhoto.image = [AppTheme placeholderImage];
    if (video.photo) {
        [self.videoPhoto setImageWithPhoto:video.photo];
    } else {
        self.videoPhoto.image = [AppTheme placeholderImage];
    }
    float videoPlayCount = [video.play_count floatValue];
    if (videoPlayCount >= 10000) {
        self.playCountLabel.text = [NSString stringWithFormat:@"%0.1fW", videoPlayCount / 10000];
    } else {
        self.playCountLabel.text = [NSString stringWithFormat:@"%@", video.play_count?:@(0)];
    }
    self.videoStar.text = [NSString stringWithFormat:@"%.1f", video.star?:0.0];
    self.videoTimeLabel.text = video.video_time;
    
    [self.videoStarView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj removeFromSuperview];
    }];
    
    NSInteger star = roundf(video.star);
    CGFloat originX = 0;
    for (int i = 1; i < 10; i+=2) {
        originX = (i/2) * (10 + 3);
        NSString *starIcon;
        if (i < star) {
            starIcon = @"icon_star_sel";
        } else if (i > star) {
            starIcon = @"icon_star_nor";
        } else {
            starIcon = @"icon_star_half";
        }
        UIImageView *starImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:starIcon]];
        starImage.frame = CGRectMake(originX, 0, 10, 10);
        [self.videoStarView addSubview:starImage];
    }
}

@end
