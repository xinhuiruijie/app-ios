//
//  CardSectionCell.m
//  bbm2
//
//  Created by PURPLEPENG on 10/31/15.
//  Copyright © 2015 PURPLEPENG. All rights reserved.
//

#import "SearchSectionCell.h"

@interface SearchSectionCell()

@property (weak, nonatomic) IBOutlet UILabel *title;
@property (weak, nonatomic) IBOutlet UIButton *deleteMask;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineViewHLC;

@end

@implementation SearchSectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.lineView.backgroundColor = [AppTheme searchLineColor];
    self.lineViewHLC.constant = [AppTheme onePixel];
//    [self startAnimation];
}

- (void)setTitleType:(TITLE_TYPE)titleType {
    if (titleType == TITLE_TYPE_HISTORY) {
        self.title.text = @"历史探索";
        self.deleteMask.hidden = NO;
        self.lineView.hidden = YES;
    } else if (titleType == TITLE_TYPE_HOTSEARCH) {
        self.title.text = @"热门探索";
        self.deleteMask.hidden = YES;
        self.lineView.hidden = self.needLineHid;
    }
}

- (void)startAnimation {
    [self startSingleViewAnimation];
}

@end
