//
//  SearchVideoItemCell.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SearchVideoItemCell : UICollectionViewCell

+ (CGFloat)heightForSearchVideoItemRow;

@end
