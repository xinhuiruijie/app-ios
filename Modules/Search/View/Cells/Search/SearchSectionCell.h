//
//  CardSectionCell.h
//  bbm2
//
//  Created by PURPLEPENG on 10/31/15.
//  Copyright © 2015 PURPLEPENG. All rights reserved.
//

#import <UIKit/UIKit.h>

#pragma mark - 枚举类型

typedef NS_ENUM(NSUInteger, TITLE_TYPE) {
    TITLE_TYPE_HISTORY = 1, // 历史探索
    TITLE_TYPE_HOTSEARCH = 2, // 热门探索
};

@interface SearchSectionCell : UICollectionViewCell

@property (nonatomic, assign) TITLE_TYPE titleType;
@property (nonatomic, assign) BOOL needLineHid;

@end
