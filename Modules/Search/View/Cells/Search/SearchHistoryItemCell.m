//
//  SearchHistoryItemCell.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "SearchHistoryItemCell.h"

@interface SearchHistoryItemCell ()

@property (weak, nonatomic) IBOutlet UILabel *title;

@end

@implementation SearchHistoryItemCell

- (void)dataDidChange {
    if ([self.data isKindOfClass:[NSString class]]) {
        NSString *title = self.data;
        if (title) {
            self.title.text = title;
//            [self startAnimation];
        }
    }
}

- (void)startAnimation {
    [self startSingleViewAnimation];
}

@end
