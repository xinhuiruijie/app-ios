//
//  SearchModel.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "STIStreamModel.h"

@interface SearchModel : STIStreamModel

@property (nonatomic, strong) NSString *keyword;
@property (nonatomic, assign) SEARCH_TYPE type;
@property (nonatomic, strong) NSNumber *total;
@property (nonatomic, strong) NSMutableArray *videoArray;
@property (nonatomic, strong) NSMutableArray *topicArray;
@property (nonatomic, strong) NSMutableArray *wormholeArray;
@property (nonatomic, strong) NSMutableArray *authorArray;
@property (nonatomic, strong) NSMutableArray *userArray;

@end
