//
//  SearchResultModel.h
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/4/24.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "STIStreamModel.h"

@interface SearchResultModel : STIStreamModel

@property (nonatomic, strong) NSString *keyword;
@property (nonatomic, assign) SEARCH_TYPE type;
@property (nonatomic, strong) NSNumber *total;

@end
