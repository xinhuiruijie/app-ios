//
//  SearchModel.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "SearchModel.h"

@implementation SearchModel

- (void)refresh {
    [self fetchForFirstTime:YES];
}

- (void)loadMore {
    [self fetchForFirstTime:NO];
}

- (void)fetchForFirstTime:(BOOL)isFirstTime {
    if (isFirstTime) {
        self.currentPage = 1;
    } else {
        self.currentPage += 1;
    }
    
    V1_API_SEARCH_HOME_API *api = [[V1_API_SEARCH_HOME_API alloc] init];
    
    api.req.keyword = self.keyword;
    api.req.type = self.type;
    
    api.whenUpdated = ^(V1_API_SEARCH_HOME_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if (error) {
            PERFORM_BLOCK_SAFELY(self.whenUpdated, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                self.loaded = YES;
                if (isFirstTime) {
                    [self.videoArray removeAllObjects];
                    [self.topicArray removeAllObjects];
                    [self.wormholeArray removeAllObjects];
                    [self.authorArray removeAllObjects];
                    [self.items removeAllObjects];
                }
                self.videoArray = [NSMutableArray arrayWithArray:response.videos];
                self.topicArray = [NSMutableArray arrayWithArray:response.topics];
                self.wormholeArray = [NSMutableArray arrayWithArray:response.wormholes];
                self.authorArray = [NSMutableArray arrayWithArray:response.authors];
                self.userArray = [NSMutableArray arrayWithArray:response.users];
                
                PERFORM_BLOCK_SAFELY(self.whenUpdated, nil);
            } else {
                STIHTTPResponseError *errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(self.whenUpdated, errors);
            }
        }
    };
    
    [api send];
}

@end
