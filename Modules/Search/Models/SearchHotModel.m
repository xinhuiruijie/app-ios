//
//  SearchHotModel.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/4/11.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "SearchHotModel.h"

@implementation SearchHotModel

- (void)loadCache {
    NSArray *items = [self loadCacheWithKey:self.cacheKey objectClass:[HOT_SERACH class]];
    
    [self.items removeAllObjects];
    if (items.count) {
        [self.items addObjectsFromArray:items];
    }
}

- (void)saveCache {
    [self saveCache:self.items key:self.cacheKey];
}

- (void)refresh {
    V1_API_SEARCH_HOT_API *api = [[V1_API_SEARCH_HOT_API alloc] init];
    
    api.whenUpdated = ^(V1_API_SEARCH_HOT_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        self.loaded = YES;
        if ( error ) {
            PERFORM_BLOCK_SAFELY(self.whenUpdated, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                [self.items removeAllObjects];
                [self.items addObjectsFromArray:response.hot_searches];
                [self saveCache];
                PERFORM_BLOCK_SAFELY(self.whenUpdated, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(self.whenUpdated, errors);
            }
        }
    };
    
    [api send];
}

@end
