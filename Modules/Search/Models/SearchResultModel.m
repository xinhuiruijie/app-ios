//
//  SearchResultModel.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/4/24.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "SearchResultModel.h"

@implementation SearchResultModel

- (void)refresh {
    [self fetchForFirstTime:YES];
}

- (void)loadMore {
    [self fetchForFirstTime:NO];
}

- (void)fetchForFirstTime:(BOOL)isFirstTime {
    if (isFirstTime) {
        self.currentPage = 1;
    } else {
        self.currentPage += 1;
    }
    
    V1_API_SEARCH_RESULT_API *api = [[V1_API_SEARCH_RESULT_API alloc] init];
    
    api.req.keyword = self.keyword;
    api.req.type = self.type;
    api.req.page = @(self.currentPage);
    api.req.per_page = @(10);
    
    api.whenUpdated = ^(V1_API_SEARCH_RESULT_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if (error) {
            
            PERFORM_BLOCK_SAFELY(self.whenUpdated, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                self.loaded = YES;
                if (isFirstTime) {
                    [self.items removeAllObjects];
                }
                
                switch (self.type) {
                    case SEARCH_TYPE_VIDEO:
                        [self.items addObjectsFromArray:response.videos];
                        break;
                    case SEARCH_TYPE_TOPIC:
                        [self.items addObjectsFromArray:response.topics];
                        break;
                    case SEARCH_TYPE_WORMHOLE:
                        [self.items addObjectsFromArray:response.wormholes];
                        break;
                    case SEARCH_TYPE_AUTHOR:
                        [self.items addObjectsFromArray:response.authors];
                        break;
                    case SEARCH_TYPE_USER:
                        [self.items addObjectsFromArray:response.users];
                        break;
                        
                    default:
                        break;
                }
                self.more = response.paged.more.boolValue;
                self.total = response.paged.total;
                PERFORM_BLOCK_SAFELY(self.whenUpdated, nil);
            } else {
                STIHTTPResponseError *errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(self.whenUpdated, errors);
            }
        }
    };
    
    [api send];
}

@end
