//
//  WormholeCreateController.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/1.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "WormholeCreateController.h"

#import "WormholeCreateModel.h"

#import "MPAlertView.h"
#import "WormholeCreateUploadCoverCell.h"
#import "WormholeCreateTitleSubtitleCell.h"
#import "WormholeCreateContentCell.h"

@interface WormholeCreateController () <UITableViewDelegate, UITableViewDataSource, TitleSubtitleCellDelegate, ContentCellDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) UITextField *titleTextField;
@property (nonatomic, strong) UITextField *SubtitleTextField;

@property (nonatomic, copy) NSString *wormeholePhoto;
@property (nonatomic, copy) NSString *wormeholeTitle;
@property (nonatomic, copy) NSString *wormeholeSubtitle;
@property (nonatomic, copy) NSString *wormeholeContent;

@end

@implementation WormholeCreateController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Wormhole"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self customize];
    [self setupList];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController enabledMLBlackTransition:NO];
    [self.navigationController.navigationBar lt_setBackgroundColor:[AppTheme wormholeNavigationColor]];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController enabledMLBlackTransition:YES];
    [self.navigationController.navigationBar lt_reset];
}

- (void)customize {
    self.navigationItem.title = @"新建虫洞";
    
    @weakify(self)
    self.navigationItem.rightBarButtonItem = [AppTheme normalItemWithContent:@"保存" handler:^(id sender) {
        @strongify(self);
        // 保存
        [self.view endEditing:YES];
        [self wormholeSave];
    }];
    
    self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
        @strongify(self)
        if (self.wormholeCreateBack) {
            self.wormholeCreateBack();
        }
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)setupList {
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 10)];
    headerView.backgroundColor = [UIColor clearColor];
    self.tableView.tableHeaderView = headerView;
    self.tableView.sectionHeaderHeight = 10;
    self.tableView.tableFooterView = [UIView new];
    self.tableView.sectionFooterHeight = CGFLOAT_MIN;
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    [self registerNibs];
}

- (void)registerNibs {
    [self.tableView registerNib:[WormholeCreateUploadCoverCell nib] forCellReuseIdentifier:@"WormholeCreateUploadCoverCell"];
    [self.tableView registerNib:[WormholeCreateTitleSubtitleCell nib] forCellReuseIdentifier:@"WormholeCreateTitleSubtitleCell"];
    [self.tableView registerNib:[WormholeCreateContentCell nib] forCellReuseIdentifier:@"WormholeCreateContentCell"];
}

#pragma mark - Tableview Data Source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        WormholeCreateUploadCoverCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WormholeCreateUploadCoverCell" forIndexPath:indexPath];
        cell.uploadCover = ^(UIButton *uploadCoverButton) {
            [self modifyHeadImageView:uploadCoverButton];
        };
        return cell;
    } else if (indexPath.section == 1) {
        WormholeCreateTitleSubtitleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WormholeCreateTitleSubtitleCell" forIndexPath:indexPath];
        self.titleTextField = cell.titleTextField;
        self.SubtitleTextField = cell.SubtitleTextField;
        cell.delegate = self;
        return cell;
    } else {
        WormholeCreateContentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WormholeCreateContentCell" forIndexPath:indexPath];
        cell.delegate = self;
        return cell;
    }
    return [UITableViewCell new];
}

#pragma mark - Tableview Delegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 70;
    } else if (indexPath.section == 1) {
        return 93;
    } else {
        return 110;
    }
}

#pragma mark - ScrollView Delegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.view endEditing:YES];
}

#pragma mark - UploadCoverCell Delegate

- (void)modifyHeadImageView:(UIButton *)sender {
    [self.view endEditing:YES];
    GKZActionSheet *actionSheet = [GKZActionSheet loadFromNib];
    AlertView * alertView = [[AlertView alloc] initWithContent:actionSheet type:AlertViewTypeMonospaced];
    actionSheet.data = @[@"拍照", @"图库"];
    [alertView showSharedView];
    actionSheet.whenHide = ^(BOOL hide) {
        [alertView hide];
    };
    @weakify(self)
    actionSheet.whenRegistered = ^(id data, NSInteger index) {
        [alertView hide];
        @strongify(self)
        if (index == 0) {
            // 拍照
            
            if ( [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] ) {
                UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                imagePickerController.delegate = self;
                imagePickerController.navigationBar.tintColor = [UIColor blackColor];
                [imagePickerController.navigationBar setTitleTextAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:17], NSForegroundColorAttributeName : [UIColor blackColor]}];
                imagePickerController.allowsEditing = YES;
                imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:imagePickerController animated:YES completion:NULL];
            } else {
                [self presentFailureTips:@"请在设置中打开摄像头权限"];
            }
        } else if (index == 1) {
            // 选照片
            
            if ( [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary] ) {
                UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                imagePickerController.delegate = self;
                imagePickerController.navigationBar.tintColor = [UIColor blackColor];
                [imagePickerController.navigationBar setTitleTextAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:17], NSForegroundColorAttributeName : [UIColor blackColor]}];
                imagePickerController.allowsEditing = YES;
                imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                [self presentViewController:imagePickerController animated:YES completion:NULL];
            } else {
                [self presentFailureTips:@"请在设置中打开相册权限"];
            }
        }
    };
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    image = [image fixOrientation];
    CGRect crop = [[info valueForKey:@"UIImagePickerControllerCropRect"] CGRectValue];
    image = [self ordinaryCrop:image toRect:crop];
    @weakify(self);
    [picker dismissViewControllerAnimated:YES completion:^{
        @strongify(self);
        [self uploadPhotoWithImage:image];
    }];
}

-(UIImage *)ordinaryCrop:(UIImage *)imageToCrop toRect:(CGRect)cropRect {
    CGImageRef imageRef = CGImageCreateWithImageInRect([imageToCrop CGImage], cropRect);
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return cropped;
}

- (void)uploadPhotoWithImage:(UIImage *)image {
    @weakify(self)
//    [image fixedCameraImageToSize:CGSizeMake(kScreenWidth, kScreenWidth) then:^(UIImage * fixedImg) {
    [UploadPhotoModel uploadWithFile:image completion:^(NSString *url, STIHTTPResponseError *e) {
        @strongify(self)
        if (e) {
            [self presentMessage:e.message withTips:@"上传失败"];
        } else {
            // 强行获取一波，有待优化
            NSIndexPath *path=[NSIndexPath indexPathForRow:0 inSection:0];
            WormholeCreateUploadCoverCell *cell = [self.tableView cellForRowAtIndexPath:path];
            [cell.uploadCoverImage setImageWithString:url];
            self.wormeholePhoto = url;
        }
    }];
//    }];
}

#pragma mark - WormholeCreateUploadCoverCell Delegate

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if ([textField isEqual:self.titleTextField]) {
        self.wormeholeTitle = textField.text;
    } else if ([textField isEqual:self.SubtitleTextField]) {
        self.wormeholeSubtitle = textField.text;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [textField endEditing:YES];
    return YES;
}

#pragma mark - WormholeCreateContentCell Delegate

- (void)textViewDidBeginEditing:(UITextView *)textView {
    CGFloat height = [SDiOSVersion deviceSize] == Screen4inch ? 64 : 0;
    [UIView animateWithDuration:0.25f animations:^{
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y - height, self.view.frame.size.width, self.view.frame.size.height)];
    }];
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    CGFloat height = [SDiOSVersion deviceSize] == Screen4inch ? 64 : 0;
    [UIView animateWithDuration:0.25f animations:^{
        [self.view setFrame:CGRectMake(self.view.frame.origin.x, self.view.frame.origin.y + height, self.view.frame.size.width, self.view.frame.size.height)];
    }];
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    self.wormeholeContent = textView.text;
}

#pragma mark - Wormhole Create Action

- (void)wormholeSave {
    if (!self.wormeholePhoto || self.wormeholePhoto.length == 0) {
        [self presentMessageTips:@"封面不能为空！"];
        return;
    } else if (!self.wormeholeTitle || self.wormeholeTitle.length == 0) {
        [self presentMessageTips:@"名称不能为空！"];
        return;
    } else if (!self.wormeholeSubtitle || self.wormeholeSubtitle.length == 0) {
        [self presentMessageTips:@"标签不能为空！"];
        return;
    } else if (!self.wormeholeContent || self.wormeholeContent.length == 0) {
        [self presentMessageTips:@"简介不能为空！"];
        return;
    }
    
    MPAlertView *alert = [MPAlertView alertWithTitle:@"新建虫洞" message:@"虫洞创建之后内容不可修改， 请确认虫洞信息" cancelButton:@"取消" confirmButton:@"确认"];
    [alert show];
    alert.confirmAction = ^{
        [self wormholeCreate];
    };
}

- (void)wormholeCreate {
    [self presentLoadingTips:nil];
    [WormholeCreateModel wormholeTitle:self.wormeholeTitle
                      wormholeSubtitle:self.wormeholeSubtitle
                       wormholeContent:self.wormeholeContent
                         wormholePhoto:self.wormeholePhoto
                                  then:^(STIHTTPResponseError *error) {
                                      if (error == nil) {
                                          [[UIApplication sharedApplication].keyWindow presentMessage:error.message withTips:@"创建虫洞成功"];
                                          if (self.wormholeCreateBack) {
                                              self.wormholeCreateBack();
                                          }
                                          [self.navigationController popViewControllerAnimated:YES];
                                      } else {
                                          [self presentMessage:error.message withTips:@"创建虫洞失败"];
                                      }
                                      [self dismissTips];
                                  }];
}

@end
