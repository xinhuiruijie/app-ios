//
//  UserWormholeListController.h
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/1.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, WORMHOLE_USER) {
    WORMHOLE_USER_MY = 1,
    WORMHOLE_USER_TA = 2,
    WORMHOLE_USER_ALL = 3,
};

@interface UserWormholeListController : UIViewController

@property (nonatomic, copy) NSString *userId;

@property (nonatomic, assign) WORMHOLE_USER wormholeUser;

@end
