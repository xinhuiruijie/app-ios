//
//  WormholeVideoListController.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/2.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "WormholeVideoListController.h"
#import "AuthorInfoController.h"
#import "VideoInfoController.h"
#import "WormholeCreateController.h"
#import "UserController.h"

#import "WormholeVideoListCell.h"
#import "DarkShareView.h"
#import "AddWormholeVideoView.h"

#import "WormholeModel.h"
#import "DelWormholeVIdeoModel.h"
#import "AddWormholeVideoModel.h"

@interface WormholeVideoListController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewHLC;

@property (weak, nonatomic) IBOutlet UIImageView *backImage;
@property (weak, nonatomic) IBOutlet UIImageView *photo;
@property (weak, nonatomic) IBOutlet UILabel *userCount;
@property (weak, nonatomic) IBOutlet UILabel *subtitle;
@property (weak, nonatomic) IBOutlet UILabel *wormholeTitle;
@property (weak, nonatomic) IBOutlet UIImageView *ownerAvatar;
@property (weak, nonatomic) IBOutlet UILabel *ownerName;
@property (weak, nonatomic) IBOutlet UIImageView *ownerPushImage;
@property (weak, nonatomic) IBOutlet UITableView *table;

@property (weak, nonatomic) IBOutlet UIView *moreMenuView;
@property (weak, nonatomic) IBOutlet UIImageView *moreMenuImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *moreLineHLC;
@property (weak, nonatomic) IBOutlet UIView *deleteView;
@property (weak, nonatomic) IBOutlet UIView *addToView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *moreMenuViewTLC;

@property (nonatomic, strong) WormholeModel *wormholeModel;

@property (nonatomic, strong) DarkShareView *smallShareView;

@property (nonatomic, strong) WORMHOLE *wormhole;
@property (nonatomic, strong) VIDEO *video;

@end

@implementation WormholeVideoListController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Wormhole"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    @weakify(self)
    self.navigationItem.leftBarButtonItem = [AppTheme whiteBackItemWithHandler:^(id sender) {
        @strongify(self)
        [self closeController];
    }];
    
    [self setupList];
    [self dataDidChange];
    [self initModel];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [self.navigationController.navigationBar setTitleTextAttributes: @{NSForegroundColorAttributeName:[UIColor whiteColor]}];
    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
    [self.wormholeModel refresh];
    self.moreMenuView.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController.navigationBar setTitleTextAttributes: @{NSForegroundColorAttributeName:[AppTheme normalTextColor]}];
    [self.navigationController.navigationBar lt_reset];
    
    if (self.popBack) {
        self.popBack();
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

- (void)setupList {
    if ([SDiOSVersion deviceSize] == Screen5Dot8inch) {
        self.viewHLC.constant = 44 + self.viewHLC.constant;
    } else {
        self.viewHLC.constant = self.viewHLC.constant;
    }
    
    self.moreLineHLC.constant = [AppTheme onePixel];
    
    self.table.tableFooterView = [UIView new];
    self.table.sectionHeaderHeight = CGFLOAT_MIN;
    self.table.sectionFooterHeight = CGFLOAT_MIN;
    [self registerNibs];
}

- (void)registerNibs {
    [self.table registerNib:[WormholeVideoListCell nib] forCellReuseIdentifier:@"WormholeVideoListCell"];
}

#pragma mark - Data

- (void)initModel {
    @weakify(self);
    self.wormholeModel = [[WormholeModel alloc] init];
    self.wormholeModel.wormholeID = self.wormholeID;
    self.wormholeModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self);
        
        [self dismissTips];
        
        if (error == nil) {
            self.wormhole = self.wormholeModel.items[0];
        } else {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
        
        USER *owner = self.wormhole.owner;
        USER *user = [UserModel sharedInstance].user;
        if ([user.id isEqualToString:owner.id]) {
            self.deleteView.hidden = NO;
            self.addToView.hidden = YES;
        } else {
            self.deleteView.hidden = YES;
            self.addToView.hidden = NO;
        }
        
        [self customView];
        [self.table reloadData];
    };
}

- (void)customView {
    self.navigationItem.title = self.wormhole.title;
    
    self.backImage.image = [AppTheme placeholderImage];
    if (self.wormhole.photo) {
        [self.backImage setImageWithPhoto:self.wormhole.photo];
    } else {
        self.backImage.image = [AppTheme placeholderImage];
    }
    
    self.photo.image = [AppTheme placeholderImage];
    if (self.wormhole.photo) {
        [self.photo setImageWithPhoto:self.wormhole.photo];
    } else {
        self.photo.image = [AppTheme placeholderImage];
    }
    
    self.userCount.text = [NSString stringWithFormat:@"%@", self.wormhole.user_count];
    self.subtitle.text = self.wormhole.subtitle ? [NSString stringWithFormat:@"#%@", self.wormhole.subtitle] : @"";
    self.wormholeTitle.text = self.wormhole.content;
    
    self.ownerAvatar.layer.cornerRadius = 12;
    self.ownerAvatar.clipsToBounds = YES;
    self.ownerAvatar.image = [AppTheme avatarDefaultImage];
    if (self.wormhole.owner.avatar) {
        [self.ownerAvatar setImageWithPhoto:self.wormhole.owner.avatar];
    } else {
        self.ownerAvatar.image = [AppTheme avatarDefaultImage];
    }
    
    self.ownerName.text = self.wormhole.owner.name;
}

#pragma mark - Tableview Data Source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.wormhole.videos.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    WormholeVideoListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WormholeVideoListCell" forIndexPath:indexPath];
    cell.indexNumber = [NSNumber numberWithInteger:indexPath.row + 1];
    cell.data = self.wormhole.videos[indexPath.row];
    
    @weakify(cell);
    cell.playVideo = ^{
        @strongify(cell);
        VIDEO *video = cell.data;
        VideoInfoController *videoInfo = [VideoInfoController spawn];
        videoInfo.video = video;
        
        videoInfo.reloadVideo = ^(VIDEO *video) {
//        [self.searchResultModel.items replaceObjectAtIndex:indexPath.row withObject:video];
//        [self.searchResultList reloadData];
        };
//        [self presentNavigationController:videoInfo];
        videoInfo.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:videoInfo animated:YES];
    };
    
    cell.moreMenuViewHidden = ^(VIDEO *video) {
        if (self.video.id == video.id) {
            self.moreMenuView.hidden = !self.moreMenuView.hidden;
        } else {
            self.moreMenuView.hidden = NO;
        }
        self.video = video;
        CGRect rect = [tableView convertRect:[tableView rectForRowAtIndexPath:indexPath] toView:[tableView superview]];
        if (rect.origin.y + 115 > self.view.height) {
            self.moreMenuViewTLC.constant = rect.origin.y - 35;
            self.moreMenuImage.highlighted = YES;
        } else {
            self.moreMenuViewTLC.constant = rect.origin.y + 22;
            self.moreMenuImage.highlighted = NO;
        }
        
    };
    
    return cell;
//    return [UITableViewCell new];
}

#pragma mark - Tableview Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell * cell = [tableView cellForRowAtIndexPath:indexPath];
    
    VIDEO *video = cell.data;
    VideoInfoController *videoInfo = [VideoInfoController spawn];
    videoInfo.video = video;
    
    videoInfo.reloadVideo = ^(VIDEO *video) {
//        [self.searchResultModel.items replaceObjectAtIndex:indexPath.row withObject:video];
//        [self.searchResultList reloadData];
    };
//    [self presentNavigationController:videoInfo];
    [self.navigationController pushViewController:videoInfo animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 72;
}

#pragma mark - Buton Action

- (IBAction)moreMenuViewHiddenAction:(UIButton *)sender {
    self.moreMenuView.hidden = YES;
}

- (IBAction)pushOwnerAction:(UIButton *)sender {
    UserController *userController = [UserController spawn];
    USER *author = self.wormhole.owner;
    userController.userID = author.id;
    [self.navigationController pushViewController:userController animated:YES];
}

- (IBAction)shareVideoAction:(id)sender {
    self.moreMenuView.hidden = YES;
    [self shareVideo:self.video];
}

- (IBAction)delVideoAction:(id)sender {
    self.moreMenuView.hidden = YES;
    
    NSString *message = [NSString stringWithFormat:@"确认删除短片 %@？", self.video.title];
    MPAlertView *alertView = [MPAlertView alertWithTitle:@"删除" message:message cancelButton:@"取消" confirmButton:@"确定"];
    alertView.confirmAction = ^{
        [self deleteVideoFromWormhole];
    };
    [alertView show];
}

- (IBAction)addToWormholeAction:(id)sender {
    [self addVideoToWormhole];
    self.moreMenuView.hidden = YES;
}

- (void)shareVideo:(VIDEO *)video {
    [AppAnalytics clickEvent:@"click_video_share"];
    [self.view endEditing:YES];
    
    DarkShareView *smallShareView = [DarkShareView loadFromNib];
    self.smallShareView = smallShareView;
    
    @weakify(self)
    if (video.photo && video.photo.thumb) {
        [self presentLoadingTips:nil];
        [self imageWithUrl:video.photo.thumb completion:^(UIImage *image) {
            @strongify(self)
            dispatch_async(dispatch_get_main_queue(), ^{
                [self dismissTips];
                video.shareImage = image;
                self.smallShareView.data = video;
                [self.smallShareView show];
            });
        }];
    } else {
        self.smallShareView.data = video;
        [self.smallShareView show];
    }
}

#pragma mark - More Menu Models

- (void)deleteVideoFromWormhole {
    DelWormholeVIdeoModel *delWormholeVideo = [[DelWormholeVIdeoModel alloc] init];
    delWormholeVideo.videoId = self.video.id;
    delWormholeVideo.wormholeId = self.wormhole.id;
    [delWormholeVideo refresh];
    delWormholeVideo.whenUpdated = ^(STIHTTPResponseError *error) {
        if (error) {
            [self presentMessage:error.message withTips:@"删除短片失败"];
        } else {
            [self.wormholeModel refresh];
            [self presentMessageTips:@"删除成功"];
        }
    };
    
    if (delWormholeVideo.items.count > 0) {
        self.data = delWormholeVideo.items.firstObject;
    }
}

- (void)addVideoToWormhole {
    AddWormholeVideoView *addWormholeVideoView = [[AddWormholeVideoView alloc] initWithFrame:kScreenRect];
    
    @weakify(self)
    @weakify(addWormholeVideoView)
    addWormholeVideoView.wormholeCreate = ^{
        @strongify(self);
        // 创建虫洞
        WormholeCreateController *wormholeCreate = [WormholeCreateController spawn];
        [self.navigationController pushViewController:wormholeCreate animated:YES];
        wormholeCreate.wormholeCreateBack = ^{
            @strongify(addWormholeVideoView);
            [addWormholeVideoView.wormholeListModel refresh];
        };
    };
    addWormholeVideoView.addWormholeVideo = ^(NSString *wormholeId) {
        @strongify(self);
        [self addToWormholeWithVideo:wormholeId];
    };
    
    [self.view addSubview:addWormholeVideoView];
}

- (void)addToWormholeWithVideo:(NSString *)wormholeId {
    AddWormholeVideoModel *addWormholeVideo = [[AddWormholeVideoModel alloc] init];
    addWormholeVideo.videoId = self.video.id;
    addWormholeVideo.wormholeId = wormholeId;
    [addWormholeVideo refresh];
    addWormholeVideo.whenUpdated = ^(STIHTTPResponseError *error) {
        if (error) {
            [self presentMessage:error.message withTips:@"添加短片失败"];
        } else {
            [self presentMessageTips:@"添加成功"];
        }
    };
    
    if (addWormholeVideo.items.count > 0) {
        self.data = addWormholeVideo.items.firstObject;
    }
}

@end
