//
//  MyWormholeItem.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/1.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "MyWormholeItem.h"

@interface MyWormholeItem ()

@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIImageView *wormholePhoto;
@property (weak, nonatomic) IBOutlet UILabel *videosCount;
@property (weak, nonatomic) IBOutlet UILabel *userCount;
@property (weak, nonatomic) IBOutlet UIView *subtitleView;
@property (weak, nonatomic) IBOutlet UILabel *subtitle;
@property (weak, nonatomic) IBOutlet UILabel *wormholeTitle;
@property (weak, nonatomic) IBOutlet UILabel *wormholeCreatedAt;

@end

@implementation MyWormholeItem

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.backView.layer.cornerRadius = 2;
    self.backView.clipsToBounds = YES;
    self.subtitleView.layer.cornerRadius = 2;
    self.subtitleView.clipsToBounds = YES;
}

- (void)dataDidChange {
    WORMHOLE *wormhole = self.data;
    
    self.wormholePhoto.image = [AppTheme placeholderImage];
    if (wormhole.photo) {
        [self.wormholePhoto setImageWithPhoto:wormhole.photo];
    } else {
        self.wormholePhoto.image = [AppTheme placeholderImage];
    }
    self.videosCount.text = [NSString stringWithFormat:@"%lu条短片", (unsigned long)wormhole.videos.count];
    self.userCount.text = [NSString stringWithFormat:@"%@", wormhole.user_count];
    self.subtitle.text = [NSString stringWithFormat:@" %@ ", wormhole.subtitle];
    self.wormholeTitle.text = wormhole.title;
    self.wormholeCreatedAt.text = [wormhole.created_at stringFromTimestamp];
}

#pragma mark - Button Action

- (IBAction)moreMenuButtonAction:(UIButton *)sender {
    WORMHOLE *wormhole = self.data;
    if (self.delWormhole) {
        self.delWormhole(wormhole);
    }
}

@end
