//
//  WormholeVideoListCell.h
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/2.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WormholeVideoListCell : UITableViewCell

@property (nonatomic, strong) NSNumber *indexNumber;
@property (nonatomic, copy) NSString *wormholeId;

@property (nonatomic, copy) void(^playVideo)(void);
@property (nonatomic, copy) void(^moreMenuViewHidden)(VIDEO *video);

@end
