//
//  MyWormholeItem.h
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/1.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyWormholeItem : UICollectionViewCell

@property (nonatomic, copy) void(^delWormhole)(WORMHOLE *wormhole);

@end
