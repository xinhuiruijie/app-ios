//
//  WormholeSortingView.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/2.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "WormholeSortingView.h"

@interface WormholeSortingView ()

@property (nonatomic, strong) UILabel *label;

@end

@implementation WormholeSortingView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Combined_Shape"]];
        imageView.frame = CGRectMake(self.width - 20, 0, 18, 18);
        imageView.center = CGPointMake(imageView.center.x, self.center.y);
        [self addSubview:imageView];
        
        self.label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.width - 18, self.height)];
        self.label.text = @"最新";
        self.label.font = [UIFont systemFontOfSize:12];
        self.label.textAlignment = NSTextAlignmentRight;
        self.label.textColor = [AppTheme normalTextColor];
        [self addSubview:self.label];
        
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake(0, 0, self.width, self.height);
        [button addTarget:self action:@selector(sortingType) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:button];
    }
    return self;
}

- (void)sortingType {
    if (self.wormholeSorting) {
        self.wormholeSorting(self.label);
    }
}

@end
