//
//  WormholeCreateContentCell.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/1.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "WormholeCreateContentCell.h"

@interface WormholeCreateContentCell () <UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *promptBackLabel;
@property (weak, nonatomic) IBOutlet UILabel *textNumberLabel;

@end

@implementation WormholeCreateContentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

#pragma mark - UITextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    if (self.delegate && [self.delegate respondsToSelector:@selector(textViewShouldBeginEditing:)]) {
        return [self.delegate textViewShouldBeginEditing:textView];
    }
    return YES;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    self.promptBackLabel.hidden = YES;
    if (self.delegate && [self.delegate respondsToSelector:@selector(textViewDidBeginEditing:)]) {
        [self.delegate textViewDidBeginEditing:textView];
    }
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    if (self.delegate && [self.delegate respondsToSelector:@selector(textViewShouldEndEditing:)]) {
        [self.delegate textViewShouldEndEditing:textView];
    }
    return YES;
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    NSString *getStr = [self getSubString:textView.text];
    if(getStr && getStr.length > 0) {
        textView.text = getStr;
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(textViewDidEndEditing:)]) {
        [self.delegate textViewDidEndEditing:textView];
    }
    if (textView.text.length == 0) {
        self.promptBackLabel.hidden = NO;
    }
}

- (void)textViewDidChange:(UITextView *)textView {
    [self textViewChanged:textView replacementText:@""]; // 防止输入字数饱和后联想输入可继续输入一次造成实际字数超长
    if (self.delegate && [self.delegate respondsToSelector:@selector(textViewDidChange:)]) {
        [self.delegate textViewDidChange:textView];
    }
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    [self textViewChanged:textView replacementText:text];
    if (self.delegate && [self.delegate respondsToSelector:@selector(textView:shouldChangeTextInRange:replacementText:)]) {
        return [self.delegate textView:textView shouldChangeTextInRange:range replacementText:text];
    }
    
    if ([text isEqualToString:@"\n"]) {
        //换行结束输入
        [textView endEditing:YES];
        return NO;
    }
    
    if ([self isInputRuleNotBlank:text] || [text isEqualToString:@""]) {//当输入符合规则和退格键时允许改变输入框
        return YES;
    } else {
        NSLog(@"超出字数限制");
        return NO;
    }
}

#pragma mark - CN/EN Language Judgment

- (void)textViewChanged:(UITextView *)textView replacementText:(NSString *)text {
    NSString *toBeString = [NSString stringWithFormat:@"%@%@", textView.text, text];
    toBeString = [toBeString stringByReplacingOccurrencesOfString:@"\n" withString:@""];
    if (![self isInputRuleAndBlank:toBeString]) {
        return;
    }
    
    NSString *lang = [[textView textInputMode] primaryLanguage]; // 获取当前键盘输入模式
    //简体中文输入,第三方输入法（搜狗）所有模式下都会显示“zh-Hans”
    if([lang isEqualToString:@"zh-Hans"]) {
        UITextRange *selectedRange = [textView markedTextRange];
        //获取高亮部分
        UITextPosition *position = [textView positionFromPosition:selectedRange.start offset:0];
        //没有高亮选择的字，则对已输入的文字进行字数统计和限制
        if(!position) {
            NSString *getStr = [self getSubString:toBeString];
            if(getStr && getStr.length > 0) {
                textView.text = getStr;
            }
        }
    } else{
        NSString *getStr = [self getSubString:toBeString];
        if(getStr && getStr.length > 0) {
            textView.text = getStr;
        }
    }
}

/**
 * 字母、数字、中文、中英文部分标点符号正则判断
 */
- (BOOL)isInputRuleNotBlank:(NSString *)str {
    NSString *pattern = @"^[a-zA-Z\u4E00-\u9FA5\\d\\s？！，。,.?!_@#/]*$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
    BOOL isMatch = [pred evaluateWithObject:str];
    
    if (!isMatch) {
        NSString *other = @"➋➌➍➎➏➐➑➒";
        unsigned long len=str.length;
        for(int i=0;i<len;i++) {
            unichar a=[str characterAtIndex:i];
            if(!((isalpha(a))
                 ||(isalnum(a))
                 ||((a=='_') || (a == '-'))
                 ||((a >= 0x4e00 && a <= 0x9fa6))
                 ||([other rangeOfString:str].location != NSNotFound)
                 ))
                return NO;
        }
        return YES;
    }
    return isMatch;
}

/**
 * 字母、数字、中文、中英文部分标点符号正则判断（在系统输入法中文输入时会出现拼音之间有空格，需要忽略，当按return键时会自动用字母替换，按空格输入响应汉字）
 */
- (BOOL)isInputRuleAndBlank:(NSString *)str {
    NSString *pattern = @"^[a-zA-Z\u4E00-\u9FA5\\d\\s？！，。,.?!_@#/]*$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
    BOOL isMatch = [pred evaluateWithObject:str];
    return isMatch;
}

/**
 *  获得 CharacterCount长度的字符
 */
-(NSString *)getSubString:(NSString*)string {
    if (string.length > 20) {
        self.textNumberLabel.text = @"20/20";
        return [string substringToIndex:20];
    } else {
        self.textNumberLabel.text = [NSString stringWithFormat:@"%ld",(long)(string.length)];
    }
    return nil;
}

@end
