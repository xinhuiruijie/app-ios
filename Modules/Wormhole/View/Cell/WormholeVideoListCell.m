//
//  WormholeVideoListCell.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/2.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "WormholeVideoListCell.h"

@interface WormholeVideoListCell ()

@property (weak, nonatomic) IBOutlet UILabel *number;
@property (weak, nonatomic) IBOutlet UILabel *videoTitle;
@property (weak, nonatomic) IBOutlet UILabel *videoOwnerName;
@property (weak, nonatomic) IBOutlet UILabel *videoTime;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineHLC;

@property (nonatomic, strong) VIDEO *video;

@end

@implementation WormholeVideoListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.lineHLC.constant = [AppTheme onePixel];
}

- (void)setIndexNumber:(NSNumber *)indexNumber {
    self.number.text = [NSString stringWithFormat:@"%@", indexNumber];
}

- (void)dataDidChange {
    self.video = self.data;
    
    self.videoTitle.text = self.video.title;
    self.videoOwnerName.text = self.video.owner.name;
    self.videoTime.text = self.video.video_time;
}

#pragma mark - Button Action

- (IBAction)playVideoAction:(id)sender {
    if (self.playVideo) {
        self.playVideo();
    }
}

- (IBAction)moreMenuButtonAction:(id)sender {
    if (self.moreMenuViewHidden) {
        self.moreMenuViewHidden(self.video);
    }
}

@end
