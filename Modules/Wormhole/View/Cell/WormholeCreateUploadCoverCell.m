//
//  WormholeCreateUploadCoverCell.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/1.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "WormholeCreateUploadCoverCell.h"

@implementation WormholeCreateUploadCoverCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (IBAction)UploadCoverAction:(UIButton *)sender {
    if (self.uploadCover) {
        self.uploadCover(sender);
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
