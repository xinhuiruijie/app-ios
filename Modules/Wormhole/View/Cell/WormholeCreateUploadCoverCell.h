//
//  WormholeCreateUploadCoverCell.h
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/1.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WormholeCreateUploadCoverCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *uploadCoverImage;

@property (nonatomic, copy) void(^uploadCover)(UIButton *uploadCoverButton);

@end
