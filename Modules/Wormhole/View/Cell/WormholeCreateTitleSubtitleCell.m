//
//  WormholeCreateTitleSubtitleCell.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/1.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "WormholeCreateTitleSubtitleCell.h"

@interface WormholeCreateTitleSubtitleCell () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineHLC;

@end

@implementation WormholeCreateTitleSubtitleCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.lineHLC.constant = [AppTheme onePixel];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField {
    if (self.delegate && [self.delegate respondsToSelector:@selector(textFieldShouldBeginEditing:)]) {
        return [self.delegate textFieldShouldBeginEditing:textField];
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    if (self.delegate && [self.delegate respondsToSelector:@selector(textFieldDidBeginEditing:)]) {
        [self.delegate textFieldDidBeginEditing:textField];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField {
    if (self.delegate && [self.delegate respondsToSelector:@selector(textFieldDidEndEditing:)]) {
        [self.delegate textFieldDidEndEditing:textField];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
//    NSString *searchText = textField.text;
//    if ([textField isEqual:self.titleTextField] && searchText.length + string.length > 15) {
//        return NO;
//    } else if ([textField isEqual:self.SubtitleTextField] && searchText.textLength + string.textLength > 8) {
//        return NO;
//    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(textField:shouldChangeCharactersInRange:replacementString:)]) {
        return [self.delegate textField:textField shouldChangeCharactersInRange:range replacementString:string];
    }
//    return YES;
    
    if ([self isInputRuleNotBlank:string] || [string isEqualToString:@""]) {//当输入符合规则和退格键时允许改变输入框
        return YES;
    } else {
        NSLog(@"超出字数限制");
        return NO;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (self.delegate && [self.delegate respondsToSelector:@selector(textFieldShouldReturn:)]) {
        return [self.delegate textFieldShouldReturn:textField];
    }
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {
    if (self.delegate && [self.delegate respondsToSelector:@selector(textFieldShouldClear:)]) {
        return [self.delegate textFieldShouldClear:textField];
    }
    return YES;
}

- (void)textFieldTextDidChange:(UITextField *)textField {
    if (self.delegate && [self.delegate respondsToSelector:@selector(textFieldTextDidChange:)]) {
        [self.delegate textFieldTextDidChange:textField];
    }
}

#pragma mark - CN/EN Language Judgment

- (IBAction)textFieldChanged:(UITextField *)textField {
    NSString *toBeString = textField.text;
    if (![self isInputRuleAndBlank:toBeString]) {
        return;
    }
    
    NSInteger CharacterCount = 8;
    if ([textField isEqual:self.titleTextField]) {
        CharacterCount = 15;
    } else if ([textField isEqual:self.SubtitleTextField]) {
        CharacterCount = 8;
    }
    
    NSString *lang = [[textField textInputMode] primaryLanguage]; // 获取当前键盘输入模式
    //简体中文输入,第三方输入法（搜狗）所有模式下都会显示“zh-Hans”
    if([lang isEqualToString:@"zh-Hans"]) {
        UITextRange *selectedRange = [textField markedTextRange];
        //获取高亮部分
        UITextPosition *position = [textField positionFromPosition:selectedRange.start offset:0];
        //没有高亮选择的字，则对已输入的文字进行字数统计和限制
        if(!position) {
            NSString *getStr = [self getSubString:toBeString textFieldChanged:textField CharacterCount:CharacterCount];
            if(getStr && getStr.length > 0) {
                textField.text = getStr;
            }
        }
    } else{
        NSString *getStr = [self getSubString:toBeString textFieldChanged:textField CharacterCount:CharacterCount];
        if(getStr && getStr.length > 0) {
            textField.text= getStr;
        }
    }
}

/**
 * 字母、数字、中文正则判断（不包括空格）
 */
- (BOOL)isInputRuleNotBlank:(NSString *)str {
    NSString *pattern = @"^[a-zA-Z\u4E00-\u9FA5\\d]*$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
    BOOL isMatch = [pred evaluateWithObject:str];
    
    if (!isMatch) {
        NSString *other = @"➋➌➍➎➏➐➑➒";
        unsigned long len=str.length;
        for(int i=0;i<len;i++) {
            unichar a=[str characterAtIndex:i];
            if(!((isalpha(a))
                 ||(isalnum(a))
                 ||((a=='_') || (a == '-'))
                 ||((a >= 0x4e00 && a <= 0x9fa6))
                 ||([other rangeOfString:str].location != NSNotFound)
                 ))
                return NO;
        }
        return YES;
    }
    return isMatch;
}

/**
 * 字母、数字、中文正则判断（包括空格）（在系统输入法中文输入时会出现拼音之间有空格，需要忽略，当按return键时会自动用字母替换，按空格输入响应汉字）
 */
- (BOOL)isInputRuleAndBlank:(NSString *)str {
    NSString *pattern = @"^[a-zA-Z\u4E00-\u9FA5\\d\\s]*$";
    NSPredicate *pred = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pattern];
    BOOL isMatch = [pred evaluateWithObject:str];
    return isMatch;
}

/**
 *  获得 CharacterCount长度的字符
 */
-(NSString *)getSubString:(NSString *)string textFieldChanged:(UITextField *)textField CharacterCount:(NSInteger)CharacterCount {
    NSInteger stringLength = 8;
    if ([textField isEqual:self.titleTextField]) {
        stringLength = string.length;
    } else if ([textField isEqual:self.SubtitleTextField]) {
        stringLength = string.textLength;
    }
    
    if (stringLength > CharacterCount) {
        if ([textField isEqual:self.titleTextField]) {
            return [string substringToIndex:CharacterCount];
        } else if ([textField isEqual:self.SubtitleTextField]) {
            return [self textLengthText:string];
        }
    }
    return string;
}

- (NSString *)textLengthText:(NSString *)text {
    NSString *finalString;
    for (NSUInteger i = 0; i < text.length; i++) {
        finalString = [NSString stringWithFormat:@"%@%@", finalString?:@"", [text substringWithRange:NSMakeRange(i,1)]];
        if (finalString.textLength >= 8) {
            return finalString;
        }
    }
    return finalString;
}

@end
