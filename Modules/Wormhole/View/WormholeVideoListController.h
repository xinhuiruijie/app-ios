//
//  WormholeVideoListController.h
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/2.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WormholeVideoListController : UIViewController

@property (nonatomic, copy) NSString *wormholeID;

@property (nonatomic, copy) void(^popBack)(void);

@end
