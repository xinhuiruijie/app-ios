//
//  UserWormholeListController.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/1.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "UserWormholeListController.h"
#import "WormholeVideoListController.h"
#import "WormholeCreateController.h"
#import "UICollectionViewLeftAlignedLayout.h"
#import "UserController.h"

#import "WormholeSortingView.h"
#import "MyWormholeItem.h"
#import "SearchWormholeResultItem.h"

#import "UserWormholeListModel.h"
#import "WormholeListModel.h"
#import "DeleteWormholeModel.h"

@interface UserWormholeListController () <UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UICollectionView *wormholeList;
@property (weak, nonatomic) IBOutlet UIView *delWormholeView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *delWormholeTLC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *delWormholeLLC;

@property (nonatomic, strong) UserWormholeListModel *userWormholeListModel;
@property (nonatomic, strong) WormholeListModel *wormholeListModel;
@property (nonatomic, strong) WORMHOLE *wormhole;
@property (nonatomic, copy) NSString *navigationTitle;

@end

@implementation UserWormholeListController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Wormhole"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self customize];
    [self setupList];
    if (self.wormholeUser == WORMHOLE_USER_ALL) {
        [self allWormholeModelInit];
    } else {
        [self userWormholeModelInit];
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController.navigationBar lt_setBackgroundColor:[AppTheme wormholeNavigationColor]];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    self.delWormholeView.hidden = YES;
    [self.navigationController.navigationBar lt_reset];
    
}

- (void)customize {
    self.navigationItem.title = self.navigationTitle;
    
    @weakify(self)
    self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    switch (self.wormholeUser) {
        case WORMHOLE_USER_MY: {
            self.navigationItem.rightBarButtonItem = [AppTheme normalItemWithContent:[UIImage imageNamed:@"icon_w_add"] handler:^(id sender) {
                @strongify(self);
                // 创建虫洞
                WormholeCreateController *wormholeCreate = [WormholeCreateController spawn];
                wormholeCreate.wormholeCreateBack = ^{
                    [self.wormholeList.header beginRefreshing];
                };
                [self.navigationController pushViewController:wormholeCreate animated:YES];
            }];
        }
            break;
        case WORMHOLE_USER_TA:

            break;
        case WORMHOLE_USER_ALL: {
            WormholeSortingView *wormholeSorting = [[WormholeSortingView alloc] initWithFrame:CGRectMake(0, 0, 55, 18)];
            UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:wormholeSorting];
            self.navigationItem.rightBarButtonItem = rightItem;
            wormholeSorting.wormholeSorting = ^(UILabel *label) {
                switch (self.wormholeListModel.wormholeSort) {
                    case WORMHOLE_SORT_LATEST: {
                        self.wormholeListModel.wormholeSort = WORMHOLE_SORT_HOT;
                        label.text = @"最热";
                    }
                        break;
                    case WORMHOLE_SORT_HOT: {
                        self.wormholeListModel.wormholeSort = WORMHOLE_SORT_LATEST;
                        label.text = @"最新";
                    }
                        break;
                        
                    default:
                        break;
                }
                [self.wormholeList.header beginRefreshing];
                [self.wormholeListModel refresh];
            };
        }
            break;

        default:
            break;
    }
}

- (void)modelRefresh {
    [self.wormholeList setContentOffset:CGPointZero];
    if (self.wormholeUser == WORMHOLE_USER_ALL) {
        self.wormholeListModel.loaded = YES;
        [self.wormholeListModel refresh];
    } else {
        self.userWormholeListModel.loaded = YES;
        [self.userWormholeListModel refresh];
    }
    
}

#pragma mark - Model Init

- (void)userWormholeModelInit {
    @weakify(self);
    self.userWormholeListModel = [[UserWormholeListModel alloc] init];
    self.userWormholeListModel.userId = self.userId;
    if (self.wormholeUser == WORMHOLE_USER_TA) {
        self.userWormholeListModel.status = WORMHOLE_STATUS_PASSED;
    }
    self.userWormholeListModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self);
        
        [self.wormholeList.header endRefreshing];
        
        if (error == nil) {
            if (self.wormholeUser == WORMHOLE_USER_TA) {
                [self setupRefreshFooterView];
            }
            
            if (self.userWormholeListModel.more) {
                [self.wormholeList.footer endRefreshing];
            } else {
                [self.wormholeList.footer setTitle:MJRefreshFooterStateNoMoreDataText forState:MJRefreshFooterStateNoMoreData];
                [self.wormholeList.footer noticeNoMoreData];
            }
        } else {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
        [self.wormholeList reloadData];
    };
    
    [self setupRefreshHeaderView];
    [self.wormholeList.header beginRefreshing];
}

- (void)allWormholeModelInit {
    @weakify(self);
    self.wormholeListModel = [[WormholeListModel alloc] init];
    self.wormholeListModel.wormholeSort = WORMHOLE_SORT_LATEST;
    self.wormholeListModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self);
        
        [self.wormholeList.header endRefreshing];
        
        if (error == nil) {
            [self setupRefreshFooterView];
            if (self.wormholeListModel.more) {
                [self.wormholeList.footer endRefreshing];
            } else {
                [self.wormholeList.footer setTitle:MJRefreshFooterStateNoMoreDataText forState:MJRefreshFooterStateNoMoreData];
                [self.wormholeList.footer noticeNoMoreData];
            }
        } else {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
        [self.wormholeList reloadData];
    };
    
    [self setupRefreshHeaderView];
    [self.wormholeList.header beginRefreshing];
}

- (void)setupList {
    self.wormholeList.backgroundColor = [AppTheme backgroundColor];
    self.wormholeList.alwaysBounceVertical = YES;
    self.wormholeList.contentInset = UIEdgeInsetsMake(0, 0, 20, 0);
    
    UICollectionViewLeftAlignedLayout *layout = [UICollectionViewLeftAlignedLayout new];
    self.wormholeList.collectionViewLayout = layout;
    
    [self registerNibs];
}

- (void)registerNibs {
    [self.wormholeList registerNib:[EmptyCollectionCell nib] forCellWithReuseIdentifier:@"EmptyCollectionCell"];
    [self.wormholeList registerNib:[MyWormholeItem nib] forCellWithReuseIdentifier:@"MyWormholeItem"];
    [self.wormholeList registerNib:[SearchWormholeResultItem nib] forCellWithReuseIdentifier:@"SearchWormholeResultItem"];
}

#pragma mark - Setter

- (void)setWormholeUser:(WORMHOLE_USER)wormholeUser {
    _wormholeUser = wormholeUser;
    switch (wormholeUser) {
        case WORMHOLE_USER_MY:
            self.navigationTitle = @"My虫洞";
            break;
        case WORMHOLE_USER_TA:
            self.navigationTitle = @"TA的虫洞";
            break;
        case WORMHOLE_USER_ALL:
            self.navigationTitle = @"全部类型";
            break;
            
        default:
            break;
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if ([self isPredicateListEmpty]) {
        return 1;
    } else if (self.wormholeUser == WORMHOLE_USER_ALL) {
        return self.wormholeListModel.items.count;
    } else {
        return self.userWormholeListModel.items.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self isPredicateListEmpty]) {
        EmptyCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"EmptyCollectionCell" forIndexPath:indexPath];
        return cell;
    } else {
        switch (self.wormholeUser) {
            case WORMHOLE_USER_MY: {
                MyWormholeItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MyWormholeItem" forIndexPath:indexPath];
                cell.data = self.userWormholeListModel.items[indexPath.item];
                
                @weakify(cell)
                cell.delWormhole = ^(WORMHOLE *wormhole) {
                    @strongify(cell)
                    self.delWormholeView.hidden = NO;
                    self.wormhole = wormhole;
                    
                    CGRect cellForCollectionRect = [collectionView convertRect:cell.frame toView:collectionView];
                    CGRect cellForViewRect = [collectionView convertRect:cellForCollectionRect toView:self.view];
                    self.delWormholeTLC.constant = cellForViewRect.origin.y + cellForViewRect.size.height - 107;
                    self.delWormholeLLC.constant = cellForViewRect.origin.x + cellForViewRect.size.width - 135;
                };
                return cell;
            }
                break;
            case WORMHOLE_USER_TA: {
                SearchWormholeResultItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SearchWormholeResultItem" forIndexPath:indexPath];
                cell.data = self.userWormholeListModel.items[indexPath.item];
                cell.authorInfoAction = ^(USER *author) {
                    UserController *userController = [UserController spawn];
                    userController.userID = author.id;
                    [self.navigationController pushViewController:userController animated:YES];
                };
                return cell;
            }
                break;
            case WORMHOLE_USER_ALL: {
                SearchWormholeResultItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SearchWormholeResultItem" forIndexPath:indexPath];
                cell.data = self.wormholeListModel.items[indexPath.item];
                cell.authorInfoAction = ^(USER *author) {
                    UserController *userController = [UserController spawn];
                    userController.userID = author.id;
                    [self.navigationController pushViewController:userController animated:YES];
                };
                return cell;
            }
                break;
                
            default:
                break;
        }
    }
    
    return [[UICollectionViewCell alloc] init];
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell * cell = [collectionView cellForItemAtIndexPath:indexPath];
    WormholeVideoListController *wormholeVideoListController = [WormholeVideoListController spawn];
    WORMHOLE *wormhole = cell.data;
    wormholeVideoListController.wormholeID = wormhole.id;
    wormholeVideoListController.popBack = ^{
        if (self.wormholeUser == WORMHOLE_USER_ALL) {
            [self.wormholeListModel refresh];
        } else {
            [self.userWormholeListModel refresh];
        }
    };
    [self.navigationController pushViewController:wormholeVideoListController animated:YES];
}

//- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
//    if ( self.isDownScroll) {
//        if (![self isPredicateListEmpty]) {
//            if (indexPath.item < 3) {
//                NSInteger indexDelay = indexPath.row;
//                cell.alpha = 0;
//                [cell performSelector:@selector(startSingleViewAnimation) withObject:self afterDelay:indexDelay * 0.2];
//            } else {
//                [cell startSingleViewAnimation];
//            }
//        }
//    }
//}
//
//- (void)collectionView:(UICollectionView *)collectionView willDisplaySupplementaryView:(UICollectionReusableView *)view forElementKind:(NSString *)elementKind atIndexPath:(NSIndexPath *)indexPath {
//    if ( self.isDownScroll) {
//        if (![self isPredicateListEmpty]) {
//            [view startSingleViewAnimation];
//        }
//    }
//}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if ([self isPredicateListEmpty]) {
        return CGSizeMake(collectionView.width, collectionView.height);
    } else {
        switch (self.wormholeUser) {
            case WORMHOLE_USER_MY: {
                CGFloat cellWidth = ceil((kScreenWidth - 50) / 2);
                CGFloat cellHeight = ceil(cellWidth * 93 / 165);
                return CGSizeMake(cellWidth, cellHeight + 70);
            }
                break;
            case WORMHOLE_USER_TA: {
                CGFloat cellHeight = ceil(kScreenWidth * 211 / 375);
                return CGSizeMake(collectionView.width, cellHeight + 67);
            }
                break;
            case WORMHOLE_USER_ALL: {
                CGFloat cellHeight = ceil(kScreenWidth * 211 / 375);
                return CGSizeMake(collectionView.width, cellHeight + 67);
            }
                break;
                
            default:
                break;
        }
    }
    return CGSizeZero;
}

//- (UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath {
//    if ([kind isEqualToString:UICollectionElementKindSectionHeader]) {
//        PredicateListHeaderCell * cell = [collectionView dequeueReusableSupplementaryViewOfKind:kind withReuseIdentifier:@"PredicateListHeaderCell" forIndexPath:indexPath];
//        cell.data = self.searchResultModel.total;
//        return cell;
//    } else if ([kind isEqualToString:UICollectionElementKindSectionFooter]) {
//        return nil;
//    }
//
//    return nil;
//}


//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section {
//    return CGSizeMake(collectionView.width, 53.f);
//}

//- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
//    return CGSizeZero;
//}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    if (self.wormholeUser == WORMHOLE_USER_MY) {
        return UIEdgeInsetsMake(15, 15, 15, 15);
    }
    return UIEdgeInsetsZero;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    if (self.wormholeUser == WORMHOLE_USER_MY) {
        return 15;
    }
    return CGFLOAT_MIN;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    if (self.wormholeUser == WORMHOLE_USER_MY) {
        return 15;
    }
    return CGFLOAT_MIN;
}

//- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    CGFloat currentPostionY = scrollView.contentOffset.y;
//
//    if (currentPostionY - _lastPosition > 0) {
//        // 向下
//        _lastPosition = currentPostionY;
//        self.isDownScroll = YES;
//    } else if (_lastPosition - currentPostionY > 0) {
//        // 向上
//        _lastPosition = currentPostionY;
//        self.isDownScroll = NO;
//    }
//
//    if (scrollView.contentOffset.y < 0) {
//        self.isDownScroll = NO;
//    }
//
//    if (scrollView.contentOffset.y == 0) {
//        self.isDownScroll = NO;
//    }
//}

#pragma mark - Buton Action

- (IBAction)delViewHidAction:(UIButton *)sender {
    self.delWormholeView.hidden = YES;
}

- (IBAction)delWormholeAction:(UIButton *)sender {
    NSString *message = [NSString stringWithFormat:@"确认删除虫洞 %@？", self.wormhole.title];
    MPAlertView *alertView = [MPAlertView alertWithTitle:@"删除" message:message cancelButton:@"取消" confirmButton:@"确定"];
    alertView.confirmAction = ^{
        self.delWormholeView.hidden = YES;
        [self delWormholWithId:self.wormhole.id];
    };
    [alertView show];
}

#pragma mark -

- (BOOL)isPredicateListEmpty {
    if (self.wormholeUser == WORMHOLE_USER_ALL) {
        if (self.wormholeListModel.items.count == 0) {
            return YES;
        }
    } else {
        if (self.userWormholeListModel.items.count == 0) {
            return YES;
        }
    }
    
    return NO;
}

#pragma mark - Header && Footer

- (void)setupRefreshHeaderView {
    @weakify(self)
    [self.wormholeList addHeaderPullLoader:^{
        @strongify(self)
        if (self.wormholeUser == WORMHOLE_USER_ALL) {
            [self.wormholeListModel refresh];
        } else {
            [self.userWormholeListModel refresh];
        }
    }];
}

- (void)setupRefreshFooterView {
    if (self.wormholeList.footer == nil) {
        if (self.wormholeUser == WORMHOLE_USER_ALL) {
            if (!self.wormholeListModel.isEmpty) {
                @weakify(self)
                [self.wormholeList addFooterPullLoader:^{
                    @strongify(self)
                    [self.wormholeListModel loadMore];
                }];
            }
        } else {
            if (!self.userWormholeListModel.isEmpty) {
                @weakify(self)
                [self.wormholeList addFooterPullLoader:^{
                    @strongify(self)
                    [self.userWormholeListModel loadMore];
                }];
            }
        }
        
    } else {
        if (self.wormholeUser == WORMHOLE_USER_ALL) {
            if (self.wormholeListModel.isEmpty) {
                [self.wormholeList removeFooter];
            }
        } else {
            if (self.userWormholeListModel.isEmpty) {
                [self.wormholeList removeFooter];
            }
        }
        
    }
}

#pragma mark - Delete Wormhole

- (void)delWormholWithId:(NSString *)wormholeId {
    [self presentLoadingTips:nil];
    [DeleteWormholeModel wormholeId:wormholeId then:^(STIHTTPResponseError *error) {
        if (error == nil) {
            [self.userWormholeListModel refresh];
        } else {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
        [self dismissTips];
    }];
}

@end
