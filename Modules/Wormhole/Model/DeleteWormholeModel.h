//
//  DeleteWormholeModel.h
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/1.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "STIStreamModel.h"

@interface DeleteWormholeModel : STIStreamModel

+ (void)wormholeId:(NSString *)wormholeId
              then:(void (^)(STIHTTPResponseError *))then;

@end
