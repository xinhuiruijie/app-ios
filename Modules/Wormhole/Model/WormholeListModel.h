//
//  WormholeListModel.h
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/1.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "STIStreamModel.h"

@interface WormholeListModel : STIStreamModel

@property (nonatomic, assign) WORMHOLE_SORT wormholeSort;

@end
