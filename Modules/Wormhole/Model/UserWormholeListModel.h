//
//  UserWormholeListModel.h
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/7.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "STIStreamModel.h"

@interface UserWormholeListModel : STIStreamModel

@property (nonatomic, copy) NSString *userId;
@property (nonatomic, assign) WORMHOLE_STATUS status;

@end
