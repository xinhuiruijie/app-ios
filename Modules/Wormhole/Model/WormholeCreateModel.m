//
//  WormholeCreateModel.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/1.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "WormholeCreateModel.h"

@implementation WormholeCreateModel

+ (void)wormholeTitle:(NSString *)title  wormholeSubtitle:(NSString *)subtitle wormholeContent:(NSString *)content wormholePhoto:(NSString *)photo then:(void (^)(STIHTTPResponseError *))then {
    V1_API_WORMHOLE_CREATE_API *api = [[V1_API_WORMHOLE_CREATE_API alloc] init];
    api.req.title = title;
    api.req.subtitle = subtitle;
    api.req.content = content;
    api.req.photo = photo;
    api.whenUpdated = ^(V1_API_WORMHOLE_CREATE_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if ( error ) {
            PERFORM_BLOCK_SAFELY(then, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                PERFORM_BLOCK_SAFELY(then, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(then, errors);
            }
        }
    };
    [api send];
}

@end
