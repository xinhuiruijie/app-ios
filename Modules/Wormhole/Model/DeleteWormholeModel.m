//
//  DeleteWormholeModel.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/1.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "DeleteWormholeModel.h"

@implementation DeleteWormholeModel

+ (void)wormholeId:(NSString *)wormholeId then:(void (^)(STIHTTPResponseError *))then {
    V1_API_WORMHOLE_DELETE_API *api = [[V1_API_WORMHOLE_DELETE_API alloc] init];
    api.req.wormhole_id = wormholeId;
    api.whenUpdated = ^(V1_API_WORMHOLE_DELETE_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if ( error ) {
            PERFORM_BLOCK_SAFELY(then, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                PERFORM_BLOCK_SAFELY(then, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(then, errors);
            }
        }
    };
    [api send];
}

@end
