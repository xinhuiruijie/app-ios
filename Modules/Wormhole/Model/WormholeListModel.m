//
//  WormholeListModel.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/1.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "WormholeListModel.h"

@implementation WormholeListModel

- (void)refresh {
    [self fetchForFirstTime:YES];
}

- (void)loadMore {
    [self fetchForFirstTime:NO];
}

- (void)fetchForFirstTime:(BOOL)isFirstTime {
    if (isFirstTime) {
        self.currentPage = 1;
    } else {
        self.currentPage += 1;
    }
    
    V1_API_WORMHOLE_LIST_API *api = [[V1_API_WORMHOLE_LIST_API alloc] init];
    
    api.req.page = @(self.currentPage);
    api.req.per_page = @(10);
    api.req.sort = self.wormholeSort;
    
    api.whenUpdated = ^(V1_API_WORMHOLE_LIST_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if (error) {
            PERFORM_BLOCK_SAFELY(self.whenUpdated, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                self.loaded = YES;
                if (isFirstTime) {
                    [self.items removeAllObjects];
                }
                
                self.more = response.paged.more.boolValue;
                [self.items addObjectsFromArray:response.wormholes];
                PERFORM_BLOCK_SAFELY(self.whenUpdated, nil);
            } else {
                STIHTTPResponseError *errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(self.whenUpdated, errors);
            }
        }
    };
    
    [api send];
}

@end
