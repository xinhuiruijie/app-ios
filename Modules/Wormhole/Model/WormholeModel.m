//
//  WormholeModel.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/22.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "WormholeModel.h"

@implementation WormholeModel

- (void)refresh {
    V1_API_WORMHOLE_GET_API *api = [[V1_API_WORMHOLE_GET_API alloc] init];
    
    api.req.wormhole_id = self.wormholeID;
    
    api.whenUpdated = ^(V1_API_WORMHOLE_GET_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if (error) {
            PERFORM_BLOCK_SAFELY(self.whenUpdated, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                self.loaded = YES;
                [self.items removeAllObjects];
                
                [self.items addObject:response.wormhole];
                PERFORM_BLOCK_SAFELY(self.whenUpdated, nil);
            } else {
                STIHTTPResponseError *errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(self.whenUpdated, errors);
            }
        }
    };
    
    [api send];
}

@end
