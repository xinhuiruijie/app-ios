//
//  AddWormholeVideoModel.h
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/3.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "STIStreamModel.h"

@interface AddWormholeVideoModel : STIStreamModel

@property (nonatomic, copy) NSString *videoId;
@property (nonatomic, copy) NSString *wormholeId;

@end
