//
//  WormholeModel.h
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/22.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "STIStreamModel.h"

@interface WormholeModel : STIStreamModel

@property (nonatomic, copy) NSString *wormholeID;

@end
