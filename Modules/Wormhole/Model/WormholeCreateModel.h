//
//  WormholeCreateModel.h
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/1.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "STIStreamModel.h"

@interface WormholeCreateModel : STIStreamModel

+ (void)wormholeTitle:(NSString *)title  wormholeSubtitle:(NSString *)subtitle wormholeContent:(NSString *)content wormholePhoto:(NSString *)photo then:(void (^)(STIHTTPResponseError *))then;

@end
