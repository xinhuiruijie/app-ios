//
//  NewTopicVideoCell.m
//  MotherPlanet
//
//  Created by liuyadi on 2018/6/6.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "NewTopicVideoCell.h"

@interface NewTopicVideoCell ()

@property (weak, nonatomic) IBOutlet UILabel *topicVideoTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *topicVideoSubtitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *topicVideoPhoto;
@property (weak, nonatomic) IBOutlet UILabel *topicVideoDurationLabel;
@property (weak, nonatomic) IBOutlet UIView *topicVideoDurationBackView;

@end

@implementation NewTopicVideoCell

+ (CGFloat)heightForTopicVideo:(TOPIC_VIDEO *)topiceVideo {
    CGFloat videoHeight = ceil(kScreenWidth * 211 / 375);
    CGFloat constantHeight = videoHeight + 41;
    
    CGFloat maxWidth = kScreenWidth - 30;
    CGFloat titleHeight = [topiceVideo.title heightWithWidth:maxWidth attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:16 weight:UIFontWeightMedium]}];
    CGFloat subtitleHeight = [topiceVideo.subtitle heightWithWidth:maxWidth font:12];
    
    return ceil(constantHeight + titleHeight + subtitleHeight);
}

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.topicVideoDurationBackView.layer.cornerRadius = 2;
    self.topicVideoDurationBackView.layer.masksToBounds = YES;
}

- (void)dataDidChange {
    TOPIC_VIDEO *topicVideo = self.data;
    
    self.topicVideoTitleLabel.text = topicVideo.title;
    self.topicVideoSubtitleLabel.text = topicVideo.subtitle;
    [self.topicVideoPhoto setImageWithPhoto:topicVideo.video.photo];
    self.topicVideoDurationLabel.text = topicVideo.video.video_time;
}

- (IBAction)topicVideoPlayButtonAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(playVideoButtonAciton:)]) {
        [self.delegate playVideoButtonAciton:self];
    }
}

@end
