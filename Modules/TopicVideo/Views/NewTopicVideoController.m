//
//  NewTopicVideoController.m
//  MotherPlanet
//
//  Created by liuyadi on 2018/6/6.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "NewTopicVideoController.h"
#import "VideoInfoController.h"
#import "TopicListController.h"
#import "TopicCustomTransition.h"
#import "NewTopicVideoPlayManager.h"

#import "VideoStarAlertView.h"
#import "DarkShareView.h"
#import "FullScreenShareView.h"

#import "NewTopicVideoHeader.h"
#import "NewTopicVideoCell.h"

#import "TopicVideoListModel.h"
#import "TopicInfoModel.h"
#import "TopicModel.h"
#import "VideoModel.h"
#import "UserPointModel.h"

@interface NewTopicVideoController () <UITableViewDelegate, UITableViewDataSource, NewTopicVideoCellDelegate, NewTopicVideoPlayManagerDelegate>

@property (nonatomic, strong) UIButton *rightBarItem;
@property (nonatomic, strong) UITableView *tableView;

@property (nonatomic, strong) NewTopicVideoPlayManager *playManager;

@property (nonatomic, strong) VideoStarAlertView *actionAlert;
@property (nonatomic, strong) FullScreenShareView *largeShareView;

@property (nonatomic, strong) TopicVideoListModel *model;
@property (nonatomic, strong) TopicInfoModel *topicModel;

@property (nonatomic, strong) NewTopicVideoCell *playingCell;

@property (nonatomic, assign) CGRect originFrame;

@property (nonatomic, assign) BOOL statusBarHidden;
@property (nonatomic, assign) BOOL playing;
/// 进来后记录电台播放状态，退出时还原
@property (nonatomic, assign) BOOL audioPlaying;

@end

@implementation NewTopicVideoController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.audioPlaying = [AudioPlayManager sharedInstance].status == AudioPlayerStatusRestartToPlay;
    if (self.audioPlaying) {
        [[AudioPlayManager sharedInstance] pause];
    }
    if ([AudioPlayManager sharedInstance].status == AudioPlayerStatusBufferEmpty ||
        [AudioPlayManager sharedInstance].status == AudioPlayerStatusReadyToPlay) {
        [[AudioPlayManager sharedInstance] stop];// 如果是在缓冲中，就停止播放
    }
    
    @weakify(self)
    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
    self.navigationItem.leftBarButtonItem = [AppTheme whiteBackItemWithHandler:^(id sender) {
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
    self.navigationItem.rightBarButtonItem = [self rightBarButtonItem];
    
    self.view.backgroundColor = [UIColor whiteColor];
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.sectionFooterHeight = CGFLOAT_MIN;
    self.tableView.sectionHeaderHeight = CGFLOAT_MIN;
    self.tableView.estimatedRowHeight = 0;
    [self.view addSubview:self.tableView];
    
    if (iOSVersionGreaterThanOrEqualTo(@"10.0")) {
        CGFloat topInset = [SDiOSVersion deviceSize] == Screen5Dot8inch ? -88 : -64;
        self.tableView.contentInset = UIEdgeInsetsMake(topInset, 0, 0, 0);
    }
    if (iOSVersionGreaterThanOrEqualTo(@"11.0")) {
        self.tableView.estimatedRowHeight = 0;
        self.tableView.estimatedSectionHeaderHeight = 0;
        self.tableView.estimatedSectionFooterHeight = 0;
    }
    
    CGFloat headerHeight = ceil(kScreenWidth * 260 / 375);
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.width, headerHeight)];
    headerView.backgroundColor = [UIColor clearColor];
    self.tableView.tableHeaderView = headerView;
    
    self.topicHeader = [NewTopicVideoHeader loadFromNib];
    self.topicHeader.frame = CGRectMake(0, 0, self.view.width, headerHeight);
    self.originFrame = self.topicHeader.frame;
    [self.view addSubview:self.topicHeader];
    
    [self.tableView registerNib:[NewTopicVideoCell nib] forCellReuseIdentifier:@"NewTopicVideoCell"];
    
    [self setupModel];
    
    self.allowOritation = YES;

    self.playManager = [NewTopicVideoPlayManager loadFromNib];
    self.playManager.delegate = self;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppDidEnterBackGround:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController enabledMLBlackTransition:NO];
    
    [self scrollViewDidScroll:self.tableView];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    
    self.tableView.delegate = self;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self reloadCellVideoState];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    if (self.playManager.isFullScreen) {
        if (self.playManager.superview == self.view) {
            return;
        }
        [self.navigationController setNavigationBarHidden:YES animated:NO];
        [self.playManager removeFromSuperview];
        [self.view addSubview:self.playManager];
        self.playManager.frame = self.view.frame;
    } else {
        [self.navigationController setNavigationBarHidden:NO animated:NO];
        [self.playManager removeFromSuperview];
        [self.playingCell addSubview:self.playManager];
        self.playManager.frame = self.playingCell.playerView.frame;
    }
}

- (void)reloadCellVideoState {
    if (self.playingCell) {
        if (self.playing) {
            [self.playManager resumeVideo];
        }
    } else {
        if ([SettingModel sharedInstance].autoPlay == NO) {
            return;
        }
        
        CGFloat navigationHeight = [SDiOSVersion deviceSize] == Screen5Dot8inch ? 88 : 64;
        for (NewTopicVideoCell *visiableCell in [self.tableView visibleCells]) {
            CGRect frame = [self.view convertRect:visiableCell.playerView.frame fromView:visiableCell];
            if (frame.origin.y >= navigationHeight) {
                [self playVideoButtonAciton:visiableCell];
                break;
            }
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    [self.navigationController.navigationBar lt_reset];
    
    self.tableView.delegate = nil;
    
    self.playing = self.playManager.isPlaying;
    [self.playManager pauseVideo];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    
    [self.navigationController enabledMLBlackTransition:YES];
}

- (void)dealloc {
    [self.playManager stopVideo];
    [self.playManager removeFromSuperview];
    self.playManager = nil;
    [self.tableView removeFromSuperview];
    
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    [[AVAudioSession sharedInstance] setActive:NO error:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    // 如果进来时电台在播放，退出时还原
    if (self.audioPlaying) {
        [[AudioPlayManager sharedInstance] play];
    }
}

#pragma mark - Custom Views

- (UIBarButtonItem *)rightBarButtonItem {
    self.rightBarItem = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightBarItem.frame = CGRectMake(0, 0, 22, 22);
    self.rightBarItem.layer.borderColor = self.topic.is_collect ? [AppTheme selectedColor].CGColor : [UIColor whiteColor].CGColor;
    [self.rightBarItem setImage:[UIImage imageNamed:@"icon_collect_nor"] forState:UIControlStateNormal];
    [self.rightBarItem setImage:[UIImage imageNamed:@"icon_collect_sel"] forState:UIControlStateSelected];
    self.rightBarItem.selected = self.topic.is_collect;
    [self.rightBarItem addTarget:self action:@selector(collectTopic:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc] initWithCustomView:self.rightBarItem];
    return rightItem;
}

#pragma mark - Model Init

- (void)setupModel {
    @weakify(self)
    self.model = [[TopicVideoListModel alloc] init];
    self.model.topic_id = self.topic.id;
    self.model.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        [self.tableView.header endRefreshing];
        [self.tableView reloadData];
        
        [self reloadCellVideoState];
        
        if (error == nil) {
            [self setupRefreshFooterView];
            if (self.model.more) {
                [self.tableView.footer endRefreshing];
            } else {
                [self.tableView.footer noticeNoMoreData];
            }
        } else {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
    };
    
    self.topicModel = [[TopicInfoModel alloc] init];
    self.topicModel.topic_id = self.topic.id;
    self.topicModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        self.topic = self.topicModel.item;
        self.rightBarItem.selected = self.topic.is_collect;
        self.topicHeader.data = self.topic;
        if (error) {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
    };
    
    [self.topicModel refresh];
    [self.model refresh];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.model.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NewTopicVideoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NewTopicVideoCell" forIndexPath:indexPath];
    cell.delegate = self;
    cell.data = self.model.items[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell isEqual:self.playingCell]) {
        [self.playManager pauseVideo];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    VideoInfoController *videoInfo = [VideoInfoController spawn];
    TOPIC_VIDEO *topicVideo = self.model.items[indexPath.row];
    VIDEO *video = topicVideo.video;
    videoInfo.video = video;
    
    videoInfo.stateManager = self.playManager.stateManager;
//    [self presentNavigationController:videoInfo];
    videoInfo.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:videoInfo animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    TOPIC_VIDEO *topicVideo = self.model.items[indexPath.row];
    return [NewTopicVideoCell heightForTopicVideo:topicVideo];
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (decelerate == YES)
        return;
    
    if ([SettingModel sharedInstance].autoPlay == NO) {
        return;
    }

    CGFloat navigationHeight = [SDiOSVersion deviceSize] == Screen5Dot8inch ? 88 : 64;
    for (NewTopicVideoCell *visiableCell in [self.tableView visibleCells]) {
        CGRect frame = [self.view convertRect:visiableCell.playerView.frame fromView:visiableCell];
        if (frame.origin.y >= navigationHeight) {
            [self playVideoButtonAciton:visiableCell];
            break;
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if ([SettingModel sharedInstance].autoPlay == NO) {
        return;
    }

    CGFloat navigationHeight = [SDiOSVersion deviceSize] == Screen5Dot8inch ? 88 : 64;
    for (NewTopicVideoCell *visiableCell in [self.tableView visibleCells]) {
        CGRect frame = [self.view convertRect:visiableCell.playerView.frame fromView:visiableCell];
        if (frame.origin.y >= navigationHeight) {
            [self playVideoButtonAciton:visiableCell];
            break;
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offsetY = scrollView.contentOffset.y;
    
    @weakify(self)
    CGFloat navigationHeight = [SDiOSVersion deviceSize] == Screen5Dot8inch ? 88 : 64;
    CGFloat changePoint = 50;
    if (offsetY > changePoint) {
        CGFloat alpha = MIN(1, 1 - ((changePoint + navigationHeight - offsetY) / navigationHeight));
        [self.navigationController.navigationBar lt_setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:alpha]];
        [self.rightBarItem setImage:[UIImage imageNamed:@"icon_collect_bar_nor"] forState:UIControlStateNormal];
        self.navigationItem.title = self.topic.title;
        self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
            @strongify(self)
            [self.navigationController popViewControllerAnimated:YES];
        }];
        [self setNeedsStatusBarAppearanceUpdate];
    } else {
        [self.navigationController.navigationBar lt_setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0]];
        [self.rightBarItem setImage:[UIImage imageNamed:@"icon_collect_nor"] forState:UIControlStateNormal];
        self.navigationItem.title = nil;
        self.navigationItem.leftBarButtonItem = [AppTheme whiteBackItemWithHandler:^(id sender) {
            @strongify(self)
            [self.navigationController popViewControllerAnimated:YES];
        }];
        [self setNeedsStatusBarAppearanceUpdate];
    }
    
    if (offsetY > 0) { // 上滑
        CGRect frame = self.originFrame;
        frame.origin.y = self.originFrame.origin.y - offsetY;
        self.topicHeader.frame = frame;
    } else {
        CGRect frame = self.originFrame;
        frame.size.height = self.originFrame.size.height - offsetY;
        frame.origin.x = self.originFrame.origin.x - (frame.size.width - self.originFrame.size.width) * 0.5;
        self.topicHeader.frame = frame;
    }
}

#pragma mark - Navigation Button Action

- (void)collectTopic:(UIButton *)sender {
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }
    
    [sender disableSelf];
    [self presentLoadingTips:nil];
    if (self.topic.is_collect) {
        [TopicModel unfavourite:self.topic then:^(STIHTTPResponseError *error) {
            [self dismissTips];
            if (error == nil) {
                self.topic.is_collect = NO;
                sender.selected = self.topic.is_collect;
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
            [sender enableSelf];
        }];
    } else {
        [TopicModel favourite:self.topic then:^(STIHTTPResponseError *error) {
            [self dismissTips];
            if (error == nil) {
                self.topic.is_collect = YES;
                sender.selected = self.topic.is_collect;
                [UserPointModel userPointName:MODULE_NAME_CLICK_COLLECT_FINISH];
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
            [sender enableSelf];
        }];
    }
}

#pragma mark - NewTopicVideoCellDelegate

- (void)playVideoButtonAciton:(NewTopicVideoCell *)cell {
    if ([cell isEqual:self.playingCell]) {
        return;
    }

    [self.playManager pauseVideo];
    [self.playManager removeFromSuperview];
    self.playingCell = cell;
    TOPIC_VIDEO *topicVideo = cell.data;
    
    self.playManager.frame = cell.playerView.frame;
    self.playManager.video = topicVideo.video;
    [cell addSubview:self.playManager];
}

#pragma mark - NewTopicVideoPlayManagerDelegate

- (void)refreshVideoManager {
    [self.model refresh];
}

- (void)statusBarHide:(BOOL)hide {
    self.statusBarHidden = hide;
    [UIView animateWithDuration:0.3 animations:^{
        [self setNeedsStatusBarAppearanceUpdate];
    }];
}

- (void)fullScreenCollectVideo:(VIDEO *)video sender:(UIButton *)sender {
    if (![UserModel online]) {
        [self.playManager needChangeScreenWithFullView];
        [[Authorization sharedInstance] showAuth];
        return;
    }
    
    [sender disableSelf];
    if (video.is_collect) {
        [VideoModel unfavourite:video then:^(STIHTTPResponseError *error) {
            if (error == nil) {
                video.is_collect = NO;
                self.playManager.video = video;
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
            [sender enableSelf];
        }];
    } else {
        [VideoModel favourite:video then:^(STIHTTPResponseError *error) {
            if (error == nil) {
                video.is_collect = YES;
                self.playManager.video = video;
                [UserPointModel userPointName:MODULE_NAME_CLICK_COLLECT_FINISH];
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
            [sender enableSelf];
        }];
    }
}

- (void)fullScreenStarVideo:(VIDEO *)video sender:(UIButton *)sender {
    if (![UserModel online]) {
        [self.playManager needChangeScreenWithFullView];
        [[Authorization sharedInstance] showAuth];
        return;
    }
    
    if (video.is_star) {
        return;
    }
    
    [AppAnalytics clickEvent:@"click_video_score"];
    [self.view endEditing:YES];
    
    [sender disableSelf];
    VideoStarAlertView *actionAlert = [VideoStarAlertView loadFromNib];
    [actionAlert darkTheme];
    @weakify(actionAlert)
    @weakify(self)
    actionAlert.confirmAction = ^(NSInteger starValue) {
        @strongify(actionAlert)
        @strongify(self)
        if (starValue == 0) {
            [[UIApplication sharedApplication].keyWindow presentMessageTips:@"请给短片评分吧~"];
        } else {
            [VideoModel star:video point:starValue then:^(STIHTTPResponseError *error) {
                if (error == nil) {
                    video.is_star = YES;
                    self.playManager.video = video;

                    UIImageView *successIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_grade_acc"]];
                    [self presentMessageTips:@"评分成功" customView:successIcon];
                } else {
                    [[UIApplication sharedApplication].keyWindow presentMessageTips:error.message];
                }
                [sender enableSelf];
            }];
            [actionAlert hide];
        }
    };
    actionAlert.cancelAction = ^{
        [sender enableSelf];
    };
    self.actionAlert = actionAlert;
    [actionAlert show];
}

- (void)shareVideo:(VIDEO *)video isFullScreen:(BOOL)isFullScreen {
    [AppAnalytics clickEvent:@"click_video_share"];
    [self.view endEditing:YES];
    
    FullScreenShareView *largeShareView = [FullScreenShareView loadFromNib];
    self.largeShareView = largeShareView;
    
    @weakify(self)
    if (video.photo && video.photo.thumb) {
        [self presentLoadingTips:nil];
        [self imageWithUrl:video.photo.thumb completion:^(UIImage *image) {
            @strongify(self)
            dispatch_async(dispatch_get_main_queue(), ^{
                [self dismissTips];
                video.shareImage = image;
                self.largeShareView.data = video;
                [self.largeShareView show];
            });
        }];
    } else {
        self.largeShareView.data = video;
        [self.largeShareView show];
    }
}

#pragma mark - Notifaction


- (void)onAppDidEnterBackGround:(UIApplication*)app {
    if (self.playing) {
        [self.playManager pauseVideo];
    }
}

- (void)onAppWillResignActive:(UIApplication *)app {
    self.playing = self.playManager.isPlaying;
    if (self.playing) {
        [self.playManager pauseVideo];
    }
    if (self.playManager.isFullScreen) {
        self.isLandscape = YES;
    }
}

- (void)onAppWillEnterForeground:(UIApplication*)app {
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    if (self.playing) {
        [self.playManager resumeVideo];
    }
}

- (void)onAppDidBecomeActive:(UIApplication*)app {
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    self.isLandscape = NO;
    if (self.playing) {
        [self.playManager resumeVideo];
    }
}

#pragma mark - 支持转屏

- (BOOL)shouldAutorotate {
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (BOOL)prefersStatusBarHidden {
    return self.statusBarHidden;
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    if (self.tableView.contentOffset.y > 50) {
        return UIStatusBarStyleDefault;
    }
    return UIStatusBarStyleLightContent;
}

#pragma mark - 转屏事件回调

- (void)rotateScreen {
    [self.actionAlert hide];
    [self.largeShareView hide];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self rotateScreen];
    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        [self.playManager reloadFullViewWithIsFull:YES];
    } else {
        [self.playManager reloadFullViewWithIsFull:NO];
    }
}

#pragma mark - Header

- (void)setupRefreshFooterView {
    if (self.tableView.footer == nil) {
        if (!self.model.isEmpty) {
            @weakify(self)
            [self.tableView addFooterPullLoader:^{
                @strongify(self)
                [self.model loadMore];
            }];
        }
    } else {
        if (self.model.isEmpty) {
            [self.tableView removeFooter];
        }
    }
}

#pragma mark - 导航控制器的代理

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC {
    BOOL animated = ([toVC isKindOfClass:[self class]] && [fromVC isKindOfClass:[TopicListController class]]);
    if (animated) {
        return [[TopicCustomTransition alloc] initWithTransitionType:operation == UINavigationControllerOperationPush? push :pop];
    }
    return nil;
}

@end
