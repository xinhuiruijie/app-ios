//
//  NewTopicVideoCell.h
//  MotherPlanet
//
//  Created by liuyadi on 2018/6/6.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NewTopicVideoCell;

@protocol NewTopicVideoCellDelegate <NSObject>

- (void)playVideoButtonAciton:(NewTopicVideoCell *)cell;

@end

@interface NewTopicVideoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *playerView;
@property (nonatomic, weak) id<NewTopicVideoCellDelegate> delegate;

+ (CGFloat)heightForTopicVideo:(TOPIC_VIDEO *)topiceVideo;

@end
