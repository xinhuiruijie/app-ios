//
//  NewTopicVideoHeader.m
//  MotherPlanet
//
//  Created by liuyadi on 2018/6/6.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "NewTopicVideoHeader.h"

@interface NewTopicVideoHeader ()

@property (weak, nonatomic) IBOutlet UIImageView *coverPhoto;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *ownerLabel;
@property (weak, nonatomic) IBOutlet UIImageView *ownerAvatar;

@end

@implementation NewTopicVideoHeader

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.ownerAvatar.layer.cornerRadius = 30;
    self.ownerAvatar.layer.masksToBounds = YES;
}

- (void)dataDidChange {
    TOPIC *topic = self.data;
    
    [self.coverPhoto setImageWithPhoto:topic.photo];
    self.titleLabel.text = topic.title;
    self.subTitleLabel.text = topic.subtitle;
    self.ownerLabel.text = topic.owner.name;
    [self.ownerAvatar setImageWithPhoto:topic.owner.avatar placeholderImage:[AppTheme avatarDefaultImage]];
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    return nil;
}

@end
