//
//  NewTopicVideoController.h
//  MotherPlanet
//
//  Created by liuyadi on 2018/6/6.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "MPViewController.h"
@class NewTopicVideoHeader;

@interface NewTopicVideoController : MPViewController <UINavigationControllerDelegate>

@property (nonatomic, strong) TOPIC *topic;

@property (nonatomic, strong) NewTopicVideoHeader *topicHeader;

@property (nonatomic, assign) BOOL allowOritation;
@property (nonatomic, assign) BOOL isLandscape;

@end
