//
//  TopicVideoListModel.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/11/30.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "STIStreamModel.h"

@interface TopicVideoListModel : STIStreamModel

@property (nonatomic, strong) NSString * topic_id; // 话题id

@end
