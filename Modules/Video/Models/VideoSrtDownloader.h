//
//  VideoSrtDownloader.h
//  MotherPlanet
//
//  Created by liuyadi on 2018/4/4.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoSrtDownloader : NSObject

- (void)startDownload:(NSString *)url
      progressHandler:(void (^)(float progress))progressHandler
                 then:(void (^)(NSString * path, NSError * e))then;

@end
