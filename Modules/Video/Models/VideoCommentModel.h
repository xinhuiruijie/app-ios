//
//  VideoCommentModel.h
//  MotherPlanet
//
//  Created by liuyadi on 2018/3/1.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "STIWriteModel.h"

@interface VideoCommentModel : STIWriteModel

+ (void)reportComment:(NSString *)id reason:(REPORT_TYPE)reason then:(void (^)(STIHTTPResponseError *))then;
+ (void)setCommentUserToBlackList:(USER *)user then:(void (^)(STIHTTPResponseError *))then;

@end
