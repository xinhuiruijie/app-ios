//
//  VideoModel.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/6.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "STIWriteModel.h"

@interface VideoModel : STIWriteModel

+ (void)like:(VIDEO *)video then:(void (^)(STIHTTPResponseError *))then;
+ (void)unlike:(VIDEO *)video then:(void (^)(STIHTTPResponseError *))then;
+ (void)favourite:(VIDEO *)video then:(void (^)(STIHTTPResponseError *))then;
+ (void)unfavourite:(VIDEO *)video then:(void (^)(STIHTTPResponseError *))then;
+ (void)share:(VIDEO *)video type:(int)type then:(void (^)(STIHTTPResponseError *))then;
+ (void)star:(VIDEO *)video point:(NSInteger)point then:(void (^)(STIHTTPResponseError *))then;
+ (void)comment:(VIDEO *)video content:(NSString *)content then:(void (^)(STIHTTPResponseError *))then;
+ (void)replyComment:(COMMENT *)comment content:(NSString *)content then:(void (^)(STIHTTPResponseError *))then;
+ (void)replyReply:(REPLY *)reply content:(NSString *)content then:(void (^)(STIHTTPResponseError *))then;
+ (void)removeVideoWithVideo_ids:(NSArray *)video_ids then:(void (^)(STIHTTPResponseError *))then;
+ (void)report:(VIDEO *)video reason:(REPORT_TYPE)reason then:(void (^)(STIHTTPResponseError *))then;

@end
