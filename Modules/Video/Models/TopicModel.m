//
//  TopicModel.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/10.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "TopicModel.h"

@implementation TopicModel

+ (void)favourite:(TOPIC *)topic then:(void (^)(STIHTTPResponseError *))then {
    V1_API_TOPIC_FAVOURITE_API *api = [[V1_API_TOPIC_FAVOURITE_API alloc] init];
    api.req.topic_id = topic.id;
    api.whenUpdated = ^(V1_API_TOPIC_FAVOURITE_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if ( error ) {
            PERFORM_BLOCK_SAFELY(then, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                topic.is_collect = YES;
                PERFORM_BLOCK_SAFELY(then, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(then, errors);
            }
        }
    };
    [api send];
}

+ (void)unfavourite:(TOPIC *)topic then:(void (^)(STIHTTPResponseError *))then {
    V1_API_TOPIC_UNFAVOURITE_API *api = [[V1_API_TOPIC_UNFAVOURITE_API alloc] init];
    api.req.topic_id = topic.id;
    api.whenUpdated = ^(V1_API_TOPIC_UNFAVOURITE_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if ( error ) {
            PERFORM_BLOCK_SAFELY(then, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                topic.is_collect = NO;
                PERFORM_BLOCK_SAFELY(then, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(then, errors);
            }
        }
    };
    [api send];
}

+ (void)removeTopicWithTopic_ids:(NSArray *)topic_ids then:(void (^)(STIHTTPResponseError *))then {
    V1_API_TOPIC_FAVOURITE_DELETE_API *api = [[V1_API_TOPIC_FAVOURITE_DELETE_API alloc] init];
    api.req.topic_ids = [topic_ids JSONStringRepresentation];
    api.whenUpdated = ^(V1_API_TOPIC_FAVOURITE_DELETE_RESPONSE *resp, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if (error) {
            PERFORM_BLOCK_SAFELY(then, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                PERFORM_BLOCK_SAFELY(then, nil);
            } else {
                STIHTTPResponseError *errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(then, errors);
            }
        }
    };
    [api send];
}

@end
