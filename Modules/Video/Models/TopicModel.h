//
//  TopicModel.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/10.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "STIWriteModel.h"

@interface TopicModel : STIWriteModel

+ (void)favourite:(TOPIC *)topic then:(void (^)(STIHTTPResponseError *))then;
+ (void)unfavourite:(TOPIC *)topic then:(void (^)(STIHTTPResponseError *))then;
+ (void)removeTopicWithTopic_ids:(NSArray *)topic_ids then:(void (^)(STIHTTPResponseError *))then;

@end
