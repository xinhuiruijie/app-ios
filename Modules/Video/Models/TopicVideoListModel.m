//
//  TopicVideoListModel.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/11/30.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "TopicVideoListModel.h"

@implementation TopicVideoListModel

- (void)loadCache {
    NSArray *items = [self loadCacheWithKey:self.cacheKey objectClass:[VIDEO class]];
    
    [self.items removeAllObjects];
    if (items.count) {
        [self.items addObjectsFromArray:items];
    }
}

- (void)saveCache {
    [self saveCache:self.items key:self.cacheKey];
}

- (NSString *)cacheKey {
    return [NSString stringWithFormat:@"%@-%@",[[self class] description], self.topic_id];
}

- (void)refresh {
    [self fetchForFirstTime:YES];
}

- (void)loadMore {
    [self fetchForFirstTime:NO];
}

- (void)fetchForFirstTime:(BOOL)isFirstTime {
    if ( isFirstTime ) {
        self.currentPage = 1;
    } else {
        self.currentPage += 1;
    }
    
    V1_API_VIDEO_TOPIC_LIST_API * api = [[V1_API_VIDEO_TOPIC_LIST_API alloc] init];
    api.req.page = @(self.currentPage);
    api.req.per_page = @(10);
    api.req.topic_id = self.topic_id;
    
    api.whenUpdated = ^(V1_API_VIDEO_TOPIC_LIST_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        self.loaded = YES;
        if ( error ) {
            PERFORM_BLOCK_SAFELY(self.whenUpdated, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                if (isFirstTime) {
                    [self.items removeAllObjects];
                }
                [self.items addObjectsFromArray:response.topic_videos];
                [self saveCache];
                self.more = response.paged.more.boolValue;
                PERFORM_BLOCK_SAFELY(self.whenUpdated, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(self.whenUpdated, errors);
            }
        }
    };
    
    [api send];
}

@end
