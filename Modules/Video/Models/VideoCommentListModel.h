//
//  VideoCommentListModel.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/6.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "STIStreamModel.h"

@interface VideoCommentListModel : STIStreamModel
@property (nonatomic, assign) NSInteger total;
@property (nonatomic, strong) NSString * video_id; // 短片id

/// 发送评论
+ (void)sendComment:(NSString *)video_id
            content:(NSString *)content
          completed:(void (^)(COMMENT *comment, NSInteger total, STIHTTPResponseError *err))completed;

/// 回复评论
+ (void)replyComment:(NSString *)comment_id
             content:(NSString *)content
                type:(COMMENT_TYPE)type
           completed:(void (^)(COMMENT *comment, STIHTTPResponseError *err))completed;

@end
