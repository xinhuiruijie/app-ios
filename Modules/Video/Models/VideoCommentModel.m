//
//  VideoCommentModel.m
//  MotherPlanet
//
//  Created by liuyadi on 2018/3/1.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "VideoCommentModel.h"

@implementation VideoCommentModel

+ (void)reportComment:(NSString *)id reason:(REPORT_TYPE)reason then:(void (^)(STIHTTPResponseError *))then {
    V1_API_COMMENT_REPORT_API *api = [[V1_API_COMMENT_REPORT_API alloc] init];
    api.req.id = id;
    api.req.type = reason;
    api.whenUpdated = ^(V1_API_COMMENT_REPORT_RESPONSE *resp, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if (error) {
            PERFORM_BLOCK_SAFELY(then, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                PERFORM_BLOCK_SAFELY(then, nil);
            } else {
                STIHTTPResponseError *errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(then, errors);
            }
        }
    };
    [api send];
}

+ (void)setCommentUserToBlackList:(USER *)user then:(void (^)(STIHTTPResponseError *))then {
    V1_API_USER_BLACK_API *api = [[V1_API_USER_BLACK_API alloc] init];
    api.req.user_id = user.id;
    api.whenUpdated = ^(V1_API_USER_BLACK_RESPONSE *resp, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if (error) {
            PERFORM_BLOCK_SAFELY(then, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                PERFORM_BLOCK_SAFELY(then, nil);
            } else {
                STIHTTPResponseError *errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(then, errors);
            }
        }
    };
    [api send];
}

@end
