//
//  VideoModel.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/6.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "VideoModel.h"

@implementation VideoModel

+ (void)like:(VIDEO *)video then:(void (^)(STIHTTPResponseError *))then {
    V1_API_VIDEO_LIKE_API *api = [[V1_API_VIDEO_LIKE_API alloc] init];
    api.req.video_id = video.id;
    api.whenUpdated = ^(V1_API_VIDEO_LIKE_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if ( error ) {
            PERFORM_BLOCK_SAFELY(then, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                video.is_like = YES;
                PERFORM_BLOCK_SAFELY(then, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(then, errors);
            }
        }
    };
    [api send];
}

+ (void)unlike:(VIDEO *)video then:(void (^)(STIHTTPResponseError *))then {
    V1_API_VIDEO_UNLIKE_API *api = [[V1_API_VIDEO_UNLIKE_API alloc] init];
    api.req.video_id = video.id;
    api.whenUpdated = ^(V1_API_VIDEO_UNLIKE_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if ( error ) {
            PERFORM_BLOCK_SAFELY(then, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                video.is_like = NO;
                PERFORM_BLOCK_SAFELY(then, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(then, errors);
            }
        }
    };
    [api send];
}

+ (void)favourite:(VIDEO *)video then:(void (^)(STIHTTPResponseError *))then {
    V1_API_VIDEO_FAVOURITE_API *api = [[V1_API_VIDEO_FAVOURITE_API alloc] init];
    api.req.video_id = video.id;
    api.whenUpdated = ^(V1_API_VIDEO_FAVOURITE_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if ( error ) {
            PERFORM_BLOCK_SAFELY(then, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                video.is_collect = YES;
                PERFORM_BLOCK_SAFELY(then, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(then, errors);
            }
        }
    };
    [api send];
}

+ (void)unfavourite:(VIDEO *)video then:(void (^)(STIHTTPResponseError *))then {
    V1_API_VIDEO_UNFAVOURITE_API *api = [[V1_API_VIDEO_UNFAVOURITE_API alloc] init];
    api.req.video_id = video.id;
    api.whenUpdated = ^(V1_API_VIDEO_UNFAVOURITE_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if ( error ) {
            PERFORM_BLOCK_SAFELY(then, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                video.is_collect = NO;
                PERFORM_BLOCK_SAFELY(then, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(then, errors);
            }
        }
    };
    [api send];
}

+ (void)share:(VIDEO *)video type:(int)type then:(void (^)(STIHTTPResponseError *))then {
    V1_API_VIDEO_SHARE_API *api = [[V1_API_VIDEO_SHARE_API alloc] init];
    api.req.video_id = video.id;
    api.req.app = @(type);
    api.whenUpdated = ^(V1_API_VIDEO_SHARE_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if ( error ) {
            PERFORM_BLOCK_SAFELY(then, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                PERFORM_BLOCK_SAFELY(then, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(then, errors);
            }
        }
    };
    [api send];
}

+ (void)star:(VIDEO *)video point:(NSInteger)point then:(void (^)(STIHTTPResponseError *))then {
    V1_API_VIDEO_STAR_API *api = [[V1_API_VIDEO_STAR_API alloc] init];
    api.req.video_id = video.id;
    api.req.video_star = @(point);
    api.whenUpdated = ^(V1_API_VIDEO_STAR_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if ( error ) {
            PERFORM_BLOCK_SAFELY(then, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                PERFORM_BLOCK_SAFELY(then, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(then, errors);
            }
        }
    };
    [api send];
}

+ (void)comment:(VIDEO *)video content:(NSString *)content then:(void (^)(STIHTTPResponseError *))then {
    V1_API_COMMENT_VIDEO_SEND_API *api = [[V1_API_COMMENT_VIDEO_SEND_API alloc] init];
    api.req.video_id = video.id;
    api.req.content = content;
    api.whenUpdated = ^(V1_API_COMMENT_VIDEO_SEND_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if ( error ) {
            PERFORM_BLOCK_SAFELY(then, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                PERFORM_BLOCK_SAFELY(then, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(then, errors);
            }
        }
    };
    [api send];
}

+ (void)replyComment:(COMMENT *)comment content:(NSString *)content then:(void (^)(STIHTTPResponseError *))then {
    V1_API_COMMENT_VIDEO_REPLY_API *api = [[V1_API_COMMENT_VIDEO_REPLY_API alloc] init];
    api.req.id = comment.id;
    api.req.type = COMMENT_TYPE_COMMENT;
    api.req.content = content;
    api.whenUpdated = ^(V1_API_COMMENT_VIDEO_REPLY_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if ( error ) {
            PERFORM_BLOCK_SAFELY(then, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                PERFORM_BLOCK_SAFELY(then, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(then, errors);
            }
        }
    };
    [api send];
}

+ (void)replyReply:(REPLY *)reply content:(NSString *)content then:(void (^)(STIHTTPResponseError *))then {
    V1_API_COMMENT_VIDEO_REPLY_API *api = [[V1_API_COMMENT_VIDEO_REPLY_API alloc] init];
    api.req.id = reply.id;
    api.req.type = COMMENT_TYPE_REPLY;
    api.req.content = content;
    api.whenUpdated = ^(V1_API_COMMENT_VIDEO_REPLY_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if ( error ) {
            PERFORM_BLOCK_SAFELY(then, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                PERFORM_BLOCK_SAFELY(then, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(then, errors);
            }
        }
    };
    [api send];
}

+ (void)removeVideoWithVideo_ids:(NSArray *)video_ids then:(void (^)(STIHTTPResponseError *))then {
    V1_API_VIDEO_FAVOURITE_DELETE_API *api = [[V1_API_VIDEO_FAVOURITE_DELETE_API alloc] init];
    api.req.video_ids = [video_ids JSONStringRepresentation];
    api.whenUpdated = ^(V1_API_VIDEO_FAVOURITE_DELETE_RESPONSE *resp, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if (error) {
            PERFORM_BLOCK_SAFELY(then, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                PERFORM_BLOCK_SAFELY(then, nil);
            } else {
                STIHTTPResponseError *errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(then, errors);
            }
        }
    };
    [api send];
}

+ (void)report:(VIDEO *)video reason:(REPORT_TYPE)reason then:(void (^)(STIHTTPResponseError *))then {
    V1_API_VIDEO_REPORT_API *api = [[V1_API_VIDEO_REPORT_API alloc] init];
    api.req.video_id = video.id;
    api.req.type = reason;
    api.whenUpdated = ^(V1_API_VIDEO_REPORT_RESPONSE *resp, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if (error) {
            PERFORM_BLOCK_SAFELY(then, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                PERFORM_BLOCK_SAFELY(then, nil);
            } else {
                STIHTTPResponseError *errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(then, errors);
            }
        }
    };
    [api send];
}

@end
