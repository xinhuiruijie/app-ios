//
//  WatchLaterModel.h
//  MotherPlanet
//
//  Created by liuyadi on 2018/4/13.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "STIStreamModel.h"

@interface WatchLaterModel : STIStreamModel

@property (nonatomic, assign) BOOL hasAdded;
@property (nonatomic, strong) NSMutableArray *watchLaterList;

@property (nonatomic, copy) void(^addRepeat)(void);
@property (nonatomic, copy) void(^addSucceed)(void);

- (void)loadWatchLaterListCache;
- (void)addVideoToLater:(VIDEO *)video;
- (void)removeVideo:(VIDEO *)video;
- (void)removeAllVideos;

@end
