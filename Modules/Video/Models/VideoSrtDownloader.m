//
//  VideoSrtDownloader.m
//  MotherPlanet
//
//  Created by liuyadi on 2018/4/4.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "VideoSrtDownloader.h"

@interface VideoSrtDownloader ()

@end

@implementation VideoSrtDownloader

- (void)startDownload:(NSString *)url progressHandler:(void (^)(float progress))progressHandler then:(void (^)(NSString *path, NSError *e))then {
    NSString *fileName = [url lastPathComponent];
    NSString *path = [[AppConfig videoSrtCachePath] stringByAppendingPathComponent:fileName];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *e;
    if ([fileManager fileExistsAtPath:path]) {
        if (then) {
            then(path, e);
        }
        return;
    }
    
    if (![fileManager fileExistsAtPath:[AppConfig videoSrtCachePath]]) {
        [fileManager createDirectoryAtPath:[AppConfig videoSrtCachePath] withIntermediateDirectories:YES attributes:nil error:&e];
    }
    
    //创建传话管理者
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    NSURLSessionDownloadTask *task = [manager downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
    } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        return [NSURL fileURLWithPath:path];
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        if (then) {
            then(path, error);
        }
    }];
    
    [task resume];
}

@end
