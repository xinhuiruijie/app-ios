//
//  TopicInfoModel.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/11/30.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "STIOnceModel.h"

@interface TopicInfoModel : STIOnceModel

@property (nonatomic, strong) NSString * topic_id; // 话题id

@end
