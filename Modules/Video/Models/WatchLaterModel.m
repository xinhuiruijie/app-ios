//
//  WatchLaterModel.m
//  MotherPlanet
//
//  Created by liuyadi on 2018/4/13.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "WatchLaterModel.h"

@implementation WatchLaterModel

- (id)init {
    if (self = [super init]) {
        self.watchLaterList = [NSMutableArray array];
        [self loadWatchLaterListCache];
        [self loadHasAddedCache];
    }
    return self;
}

#pragma mark - History Keywords Cache

- (void)loadWatchLaterListCache {
    NSArray *items = [self loadCacheWithKey:[self watchLaterListCacheKey] objectClass:[VIDEO class]];
    
    [self.watchLaterList removeAllObjects];
    if ( items.count ) {
        [self.watchLaterList addObjectsFromArray:items];
    }
}

- (void)loadHasAddedCache {
    NSNumber *hasAdded = [self loadCacheWithKey:[self hasAddedCacheKey] objectClass:[NSNumber class]];
    self.hasAdded = [hasAdded boolValue];
}

- (void)saveWatchLaterListCache {
    [self saveCache:self.watchLaterList key:[self watchLaterListCacheKey]];
}

- (void)saveHasAddedCache {
    [self saveCache:@(YES) key:[self hasAddedCacheKey]];
}

- (void)clearWatchLaterListCache {
    [self clearCacheWithKey:[self watchLaterListCacheKey]];
}

- (NSString *)watchLaterListCacheKey {
    return [NSString stringWithFormat:@"%@-watchLaterListCacheKey-%@", [[self class] description], [UserModel sharedInstance].user.id];
}

- (NSString *)hasAddedCacheKey {
    return [NSString stringWithFormat:@"%@-hasAdded-%@", [[self class] description], [UserModel sharedInstance].user.id];
}

#pragma mark - History Keywords

- (void)addVideoToLater:(VIDEO *)video {
    if (video == nil)
        return;
    
    if (self.watchLaterList) {
        for (int i = 0; i < self.watchLaterList.count; i++) {
            VIDEO *cacheVideo = self.watchLaterList[i];
            if ([cacheVideo.id isEqualToString:video.id]) {
                [[self topViewController].view presentMessageTips:@"已添加至稍后观看"];
                return;
            }
        }
        [self.watchLaterList insertObject:video atIndex:0];
    }
    
    if (self.watchLaterList.count > 20) {
        NSMutableArray *watchList = [NSMutableArray arrayWithArray:[self.watchLaterList subarrayWithRange:NSMakeRange(0, 20)]];
        self.watchLaterList = watchList;
    }
    
    [self saveWatchLaterListCache];
    [self saveHasAddedCache];
    
    if (self.hasAdded) {
        [[self topViewController].view presentMessageTips:@"添加成功"];
    } else {
        [[self topViewController].view presentMessageTips:@"添加成功,可在 [我的]->[稍后观看]中查看"];
    }
}

- (void)removeVideo:(VIDEO *)video {
    if (video == nil)
        return;
    
    if (self.watchLaterList) {
        for (int i = 0; i < self.watchLaterList.count; i++) {
            VIDEO *cacheVideo = self.watchLaterList[i];
            if ([cacheVideo.id isEqualToString:video.id]) {
                [self.watchLaterList removeObject:cacheVideo];
                break;
            }
        }
    }
    
    if (self.watchLaterList.count > 20) {
        NSMutableArray *watchList = [NSMutableArray arrayWithArray:[self.watchLaterList subarrayWithRange:NSMakeRange(0, 20)]];
        self.watchLaterList = watchList;
    }
    
    [self saveWatchLaterListCache];
}

- (void)removeAllVideos {
    if (self.watchLaterList) {
        [self.watchLaterList removeAllObjects];
    }
    
    [self saveWatchLaterListCache];
}

@end
