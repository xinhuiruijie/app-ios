//
//  VideoInfoModel.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/6.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "STIOnceModel.h"

@interface VideoInfoModel : STIOnceModel

@property (nonatomic, strong) NSString * video_id; // 短片id

@end
