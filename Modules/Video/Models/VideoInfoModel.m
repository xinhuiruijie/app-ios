//
//  VideoInfoModel.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/6.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "VideoInfoModel.h"

@implementation VideoInfoModel

- (void)refresh {
    V1_API_VIDEO_GET_API *api = [[V1_API_VIDEO_GET_API alloc] init];
    api.req.video_id = self.video_id;
    
    AFNetworkReachabilityManager * manager = [AFNetworkReachabilityManager sharedManager];
    if (manager.networkReachabilityStatus == AFNetworkReachabilityStatusReachableViaWiFi) {
        api.req.network = NETWORK_STATUS_WIFI;
    } else {
        api.req.network = NETWORK_STATUS_GPRS;
    }
    
    api.whenUpdated = ^(V1_API_VIDEO_GET_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        self.loaded = YES;
        if ( error ) {
            PERFORM_BLOCK_SAFELY(self.whenUpdated, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                self.item = response.video;
                PERFORM_BLOCK_SAFELY(self.whenUpdated, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(self.whenUpdated, errors);
            }
        }
    };
    
    [api send];
}

@end
