//
//  VideoCommentListModel.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/6.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "VideoCommentListModel.h"

@implementation VideoCommentListModel

- (void)refresh {
    [self fetchForFirstTime:YES];
}

- (void)loadMore {
    [self fetchForFirstTime:NO];
}

- (void)fetchForFirstTime:(BOOL)isFirstTime {
    if ( isFirstTime ) {
        self.currentPage = 1;
    } else {
        self.currentPage += 1;
    }
    
    V1_API_COMMENT_VIDEO_LIST_API * api = [[V1_API_COMMENT_VIDEO_LIST_API alloc] init];
    api.req.video_id = self.video_id;
    api.req.page = @(self.currentPage);
    api.req.per_page = @(10);
    
    api.whenUpdated = ^(V1_API_COMMENT_VIDEO_LIST_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        self.loaded = YES;
        if ( error ) {
            PERFORM_BLOCK_SAFELY(self.whenUpdated, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                if (isFirstTime) {
                    [self.items removeAllObjects];
                }
                [self.items addObjectsFromArray:response.comments];
                self.more = response.paged.more.boolValue;
                self.total = response.paged.total.integerValue;
                PERFORM_BLOCK_SAFELY(self.whenUpdated, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(self.whenUpdated, errors);
            }
        }
    };
    
    [api send];
}

// 发评论
+ (void)sendComment:(NSString *)video_id content:(NSString *)content completed:(void (^)(COMMENT *comment, NSInteger total, STIHTTPResponseError *err))completed {
    if (!completed) {
        return;
    }
    
    V1_API_COMMENT_VIDEO_SEND_API *api = [[V1_API_COMMENT_VIDEO_SEND_API alloc] init];
    api.req.video_id = video_id;
    api.req.content = content;
    
    api.whenUpdated = ^(V1_API_COMMENT_VIDEO_SEND_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if ( error ) {
            PERFORM_BLOCK_SAFELY(completed, nil, 0, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                PERFORM_BLOCK_SAFELY(completed, response.comment, response.comment_total.integerValue, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(completed, nil, 0, errors);
            }
        }
    };
    
    [api send];
}

// 回复评论
+ (void)replyComment:(NSString *)comment_id content:(NSString *)content type:(COMMENT_TYPE)type completed:(void (^)(COMMENT *comment, STIHTTPResponseError *err))completed {
    if (!completed) {
        return;
    }
    
    V1_API_COMMENT_VIDEO_REPLY_API *api = [[V1_API_COMMENT_VIDEO_REPLY_API alloc] init];
    api.req.id = comment_id;
    api.req.content = content;
//    api.req.type = type;
    
    api.whenUpdated = ^(V1_API_COMMENT_VIDEO_REPLY_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if ( error ) {
            PERFORM_BLOCK_SAFELY(completed, nil, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                PERFORM_BLOCK_SAFELY(completed, response.comment, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(completed, nil, errors);
            }
        }
    };
    
    [api send];
}

@end
