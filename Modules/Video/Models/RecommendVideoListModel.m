//
//  RecommendVideoListModel.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/6.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "RecommendVideoListModel.h"

@implementation RecommendVideoListModel

- (void)refresh {
    V1_API_VIDEO_RECOMMEND_LIST_API * api = [[V1_API_VIDEO_RECOMMEND_LIST_API alloc] init];
    api.req.category_id = self.category_id;
    api.req.video_id = self.video_id;
    api.whenUpdated = ^(V1_API_VIDEO_RECOMMEND_LIST_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        self.loaded = YES;
        if ( error ) {
            PERFORM_BLOCK_SAFELY(self.whenUpdated, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                [self.items removeAllObjects];
                [self.items addObjectsFromArray:response.videos];
                PERFORM_BLOCK_SAFELY(self.whenUpdated, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(self.whenUpdated, errors);
            }
        }
    };
    
    [api send];
}

@end
