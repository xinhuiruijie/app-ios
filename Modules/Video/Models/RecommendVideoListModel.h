//
//  RecommendVideoListModel.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/6.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "STIStreamModel.h"

@interface RecommendVideoListModel : STIStreamModel

@property (nonatomic, strong) NSString *category_id;    // 短片分类id
@property (nonatomic, strong) NSString *video_id; // 当前短片id

@end
