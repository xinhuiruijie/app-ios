//
//  VideoState.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/1.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "VideoState.h"

@implementation VideoState

- (instancetype)init
{
    self = [super init];
    if (self) {
        _isExpanded = NO;
    }
    return self;
}

@end


#pragma mark -

@interface VideoStateManager ()
@property (nonatomic, strong) NSMutableDictionary * map;
@property (nonatomic, strong) id lastIdentifier;
@end

@implementation VideoStateManager

- (instancetype)init
{
    self = [super init];
    if (self) {
        _map = [NSMutableDictionary dictionary];
    }
    return self;
}

- (VideoState *)stateForIdentifier:(id)identifier
{
    if ( identifier == nil )
        return nil;
    
    VideoState * state = self.map[identifier];
    
    if ( state == nil )
    {
        state = [VideoState new];
        self.map[identifier] = state;
    }
    
    return state;
}

- (VideoState *)setExpanded:(BOOL)expanded forIdentifier:(id)identifier {
    if ( identifier == nil )
        return nil;
    
    VideoState * state = [self stateForIdentifier:identifier];
    state.isExpanded = expanded;
    return state;
}

- (VideoState *)setWatchTime:(NSString *)watchTime forIdentifier:(id)identifier {
    if ( identifier == nil )
        return nil;
    
    VideoState * state = [self stateForIdentifier:identifier];
    state.watchTime = watchTime;
    return state;
}

- (VideoState *)setDuration:(NSString *)duration forIdentifier:(id)identifier {
    if ( identifier == nil )
        return nil;
    
    VideoState * state = [self stateForIdentifier:identifier];
    state.duration = duration;
    return state;
}

- (void)clear {
    [self.map removeAllObjects];
}

@end
