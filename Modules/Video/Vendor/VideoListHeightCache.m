//
//  VideoListHeightCache.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/1.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "VideoListHeightCache.h"

@interface VideoListHeightCache ()
@property (nonatomic, strong) NSMutableDictionary * cache;
@end

@implementation VideoListHeightCache

@def_singleton(VideoListHeightCache)

- (instancetype)init
{
    self = [super init];
    if (self) {
        _cache = [NSMutableDictionary dictionary];
    }
    return self;
}

- (BOOL)hasCacheForData:(VIDEO *)data
{
    return self.cache[data.id];
}

- (void)clearCacheForData:(VIDEO *)data
{
    [self.cache removeObjectForKey:data.id];
}

- (void)cacheHeight:(id)height forData:(VIDEO *)data
{
    self.cache[data.id] = height;
}

- (id)heightForData:(VIDEO *)data
{
    return self.cache[data.id];
}

@end
