//
//  VIDEO+Extension.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/1.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "video-api.h"

@interface VIDEO (Extension)

@property (nonatomic, strong) NSString *watchTime;
@property (nonatomic, strong) NSString *duration;

@end
