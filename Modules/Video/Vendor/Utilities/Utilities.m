#import <sys/xattr.h>
#import "UIKit/UIKit.h"
#import <MediaPlayer/MediaPlayer.h>
#import <CommonCrypto/CommonDigest.h>
#import "Utilities.h"

@implementation Utilities

+ (NSString *)timeToHumanString:(unsigned long)ms
{
    unsigned long h, m, s;

    h = ms / 3600;
    m = (ms - h * 3600) / 60;
    s = ms - h * 3600 - m * 60;

	NSString * time = [NSString stringWithFormat:@"%02ld:%02ld:%02ld", h, m ,s];
	
	if ( h > 0 )
	{
		time = [NSString stringWithFormat:@"%02ld:%02ld:%02ld", h, m ,s];
	}
	else
	{
		time = [NSString stringWithFormat:@"%02ld:%02ld", m ,s];
	}

    return time;
}

@end
