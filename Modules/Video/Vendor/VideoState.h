//
//  VideoState.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/1.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoState : NSObject

@property (nonatomic, assign) BOOL isExpanded; // 全文/收起
@property (nonatomic, strong) NSString *watchTime;
@property (nonatomic, strong) NSString *duration;

@end

@interface VideoStateManager : NSObject

/**
 *  获得某一行的状态
 *
 *  @param identifier 唯一区分数据的标识
 *
 *  @return VideoState
 */
- (VideoState *)stateForIdentifier:(id)identifier;

- (VideoState *)setExpanded:(BOOL)expanded forIdentifier:(id)identifier;
- (VideoState *)setWatchTime:(NSString *)watchTime forIdentifier:(id)identifier;
- (VideoState *)setDuration:(NSString *)duration forIdentifier:(id)identifier;

- (void)clear;

@end
