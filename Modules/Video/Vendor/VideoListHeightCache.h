//
//  VideoListHeightCache.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/1.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VideoListHeightCache : NSObject

- (BOOL)hasCacheForData:(VIDEO *)data;
- (void)clearCacheForData:(VIDEO *)data;
- (id)heightForData:(VIDEO *)data;
- (void)cacheHeight:(id)height forData:(VIDEO *)data;

@singleton(VideoListHeightCache)

@end
