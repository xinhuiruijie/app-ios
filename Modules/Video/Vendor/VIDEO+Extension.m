//
//  VIDEO+Extension.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/1.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "VIDEO+Extension.h"

static const char kVideoWatchTimeKey;
static const char kVideoDurationKey;

@implementation VIDEO (Extension)

@dynamic watchTime;
@dynamic duration;

- (NSString *)watchTime {
    return objc_getAssociatedObject(self, &kVideoWatchTimeKey);
}

- (void)setWatchTime:(NSString *)watchTime {
    objc_setAssociatedObject(self, &kVideoWatchTimeKey, watchTime, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

- (NSString *)duration {
    return objc_getAssociatedObject(self, &kVideoDurationKey);
}

- (void)setDuration:(NSString *)duration {
    objc_setAssociatedObject(self, &kVideoDurationKey, duration, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
}

@end
