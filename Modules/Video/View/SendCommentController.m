//
//  SendCommentController.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/8.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "SendCommentController.h"
#import "ZLPlaceholderTextView.h"
#import "VideoCommentListModel.h"

@interface SendCommentController () <ZLPlaceholderTextViewDelegate>
@property (weak, nonatomic) IBOutlet ZLPlaceholderTextView *textView;
@property (weak, nonatomic) IBOutlet UIButton *sendBtn;
@property (weak, nonatomic) IBOutlet UILabel *commentTextCountLable;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewHeightLC;

@end

@implementation SendCommentController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Video"];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.view.backgroundColor = [AppTheme backgroundColor];
    self.textView.placeholder = @"说点啥吧...";
    self.textView.delegate = self;
    self.textView.font = [UIFont systemFontOfSize:14];
    self.textView.textView.textColor = [UIColor colorWithRGBValue:0x343338];
    self.textView.maxLenght = 120;
    
    self.commentTextCountLable.text = @"0/120";
    
    [self.sendBtn setTitleColor:[AppTheme normalTextColor] forState:UIControlStateNormal];
    [self.sendBtn setTitleColor:[AppTheme subTextColor] forState:UIControlStateDisabled];
    self.sendBtn.enabled = self.textView.text.length != 0;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardChangeFrame:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardChangeFrame:) name:UIKeyboardWillHideNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.textView.textView becomeFirstResponder];
}

- (void)setAdditional:(id)additional {
    _additional = additional;
    if ([additional isKindOfClass:[COMMENT class]]) {
        COMMENT *comment = (COMMENT *)additional;
        NSString *name = comment.owner && comment.owner.name ? comment.owner.name : [AppTheme defaultPlaceholder];
        self.textView.placeholder = [NSString stringWithFormat:@"回复 %@:", name];
    } else {
        REPLY *reply = (REPLY *)additional;
        NSString *name = reply.owner && reply.owner.name ? reply.owner.name : [AppTheme defaultPlaceholder];
        self.textView.placeholder = [NSString stringWithFormat:@"回复 %@:", name];
    }
}

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
}

- (IBAction)closeBtnAction:(UIButton *)sender {
    if ([self.delegate respondsToSelector:@selector(onClose)]) {
        [self.delegate onClose];
    }
}

- (IBAction)sendBtnAction:(UIButton *)sender {
    [self.view endEditing:YES];
    
    if (self.textView.text.length > 120) {
        [self presentFailureTips:@"发表内容超长"];
        return;
    }
    
    if (!self.additional) { // 评论
        [self presentLoadingTips:nil];
        @weakify(self)
        [VideoCommentListModel sendComment:self.video_id content:self.textView.text completed:^(COMMENT *comment, NSInteger total, STIHTTPResponseError *err) {
            @strongify(self)
            [self dismissTips];
            if (err) {
                [self presentMessage:err.message withTips:@"数据获取失败"];
            } else {
                if ([self.delegate respondsToSelector:@selector(onSendCommentSuccess)]) {
                    [self.delegate onSendCommentSuccess];
                }
            }
        }];
    } else { // 回复
        NSString *replyId = nil;
        COMMENT_TYPE type = COMMENT_TYPE_COMMENT;
        if ([self.additional isKindOfClass:[COMMENT class]]) {
            COMMENT *com = (COMMENT *)self.additional;
            replyId = com.id;
            type = COMMENT_TYPE_COMMENT;
        } else {
            REPLY *reply = (REPLY *)self.additional;
            replyId = reply.id;
            type = COMMENT_TYPE_REPLY;
        }
        
        [self presentLoadingTips:nil];
        @weakify(self)
        [VideoCommentListModel replyComment:replyId content:self.textView.text type:type completed:^(COMMENT *comment, STIHTTPResponseError *err) {
            @strongify(self)
            [self dismissTips];
            if (err) {
                [self presentMessage:err.message withTips:@"数据获取失败"];
            } else {
                if ([self.delegate respondsToSelector:@selector(onSendCommentSuccess)]) {
                    [self.delegate onSendCommentSuccess];
                }
            }
        }];
    }
}

- (void)keyboardChangeFrame:(NSNotification *)noti {
    NSValue *aValue = noti.userInfo[UIKeyboardFrameEndUserInfoKey];
    CGFloat keyboardHeight = aValue.CGRectValue.size.height;
    
    CGFloat videoViewHeight = 211.0 / 375 * kScreenWidth;
    CGFloat titleViewHeight = 60;
    
    if ([noti.name isEqualToString:UIKeyboardWillShowNotification]) {
        CGFloat textViewHeight = kScreenHeight - videoViewHeight - titleViewHeight - keyboardHeight - 15;
        self.textViewHeightLC.constant = textViewHeight;
        
    } else if ([noti.name isEqualToString:UIKeyboardWillHideNotification]) {
        CGFloat textViewHeight = kScreenHeight - videoViewHeight - titleViewHeight - keyboardHeight - 15;
        self.textViewHeightLC.constant = textViewHeight > 160 ? textViewHeight : 160;
    }
}

#pragma mark - ZLPlaceholderTextViewDelegate

- (void)textViewDidChange:(UITextView *)textView {
    self.commentTextCountLable.text = [NSString stringWithFormat:@"%ld/120", textView.text.length];
    self.sendBtn.enabled = textView.text.length != 0;
}

@end
