//
//  VideoCommentListController.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/7.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VideoCommentListDelegate <NSObject>

@optional

- (void)closeSelf;

@end

@interface VideoCommentListController : UIViewController

@property (nonatomic, weak) id<VideoCommentListDelegate> delegate;
@property (nonatomic, strong) NSString *video_id;

- (void)parentControllerRotate;

@end
