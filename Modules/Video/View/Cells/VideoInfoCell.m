//
//  VideoInfoCell.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/6.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "VideoInfoCell.h"
#import "VideoAnimatedButton.h"
#import "VIDEO+Extension.h"
#import "VideoTagView.h"
#import "PPCounter.h"

@interface VideoInfoCell ()

@property (weak, nonatomic) IBOutlet UIView *authorView;
@property (weak, nonatomic) IBOutlet UIImageView *authorAvatar;
@property (weak, nonatomic) IBOutlet UILabel *authorName;
@property (weak, nonatomic) IBOutlet UILabel *authorDesc;
@property (weak, nonatomic) IBOutlet UIView *authorFollowView;
@property (weak, nonatomic) IBOutlet UIButton *followButton;

@property (weak, nonatomic) IBOutlet UIView *bottomView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *starButton;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *likeCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *collectCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *shareCountLabel;

@property (weak, nonatomic) IBOutlet UIView *likeView;
@property (weak, nonatomic) IBOutlet UIView *collectView;
@property (weak, nonatomic) IBOutlet UIView *commentView;
@property (weak, nonatomic) IBOutlet UIView *shareView;

@property (nonatomic, strong) VIDEO *video;
@property (nonatomic, assign) BOOL isAnimating;

@property (nonatomic, strong) VideoAnimatedButton *likeAnimatedView;
@property (nonatomic, strong) VideoAnimatedButton *collectAnimatedView;
@property (nonatomic, strong) VideoAnimatedButton *commentAnimatedView;
@property (nonatomic, strong) VideoAnimatedButton *shareAnimatedView;

@property (weak, nonatomic) IBOutlet UILabel *addwormholeLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *wormholeWidth;
@property (weak, nonatomic) IBOutlet UIImageView *wormholePlusImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *wormholePlusWidth;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *wormholeLeading;

@property (nonatomic, strong) NSMutableDictionary *timersByAnimateLabel;
@end

@interface DispatchTimerWapper : NSObject
@property (nonatomic, strong) dispatch_source_t timer;
@end

@implementation DispatchTimerWapper
@end

@implementation VideoInfoCell

+ (CGFloat)heightForVideoInfoCell:(VIDEO *)video {
    CGFloat constantHeight = 151; // author height 65 + title top 15 + title bottom 10 + subtitle bottom 15 + action height 30 + action bottom 15 + line height 1
    CGFloat titleWidth = kScreenWidth - 90;
    CGFloat titleHeight = MAX(22, [AppTheme calculateHeigthWithAttributeContent:[AppTheme titleAttributedString:video.title] width:titleWidth]);
    
    CGFloat subtitleWidth = kScreenWidth - 20;
    CGFloat subtitleHeight = MAX(20, [AppTheme calculateHeigthWithAttributeContent:[AppTheme detailAttributedString:video.subtitle] width:subtitleWidth]);
    
    return constantHeight + titleHeight + subtitleHeight;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
//    [self.contentView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        obj.backgroundColor = [UIColor randomColor];
//    }];
    
    VideoAnimatedButton *likeAnimatedView = [[VideoAnimatedButton alloc] initWithFrame:CGRectMake(0, 0, 50, 24)];
    likeAnimatedView.selectedImage = [UIImage imageNamed:@"icon_video_like_sel"];
    likeAnimatedView.normalImage = [UIImage imageNamed:@"icon_video_like_nor"];
    likeAnimatedView.hideCountLabel = YES;
    self.likeAnimatedView = likeAnimatedView;
    [self.likeView addSubview:likeAnimatedView];
    
    VideoAnimatedButton *collectAnimatedView = [[VideoAnimatedButton alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
    collectAnimatedView.selectedImage = [UIImage imageNamed:@"icon_collect_sel"];
    collectAnimatedView.normalImage = [UIImage imageNamed:@"icon_video_collect_nor"];
    collectAnimatedView.hideCountLabel = YES;
    self.collectAnimatedView = collectAnimatedView;
    [self.collectView addSubview:collectAnimatedView];
    
    VideoAnimatedButton *commentAnimatedView = [[VideoAnimatedButton alloc] initWithFrame:CGRectMake(0, 0, 50, 24)];
    commentAnimatedView.hideCountLabel = YES;
    self.commentAnimatedView = commentAnimatedView;
    [self.commentView addSubview:commentAnimatedView];
    
    VideoAnimatedButton *shareAnimatedView = [[VideoAnimatedButton alloc] initWithFrame:CGRectMake(0, 0, 54, 24)];
    shareAnimatedView.hideCountLabel = YES;
    self.shareAnimatedView = shareAnimatedView;
    [self.shareView addSubview:shareAnimatedView];
    
    if (kScreenWidth < 375) {
        // 【+加入虫洞】小分辨率时去掉 +
        self.addwormholeLabel.text = @"加虫洞";
        self.wormholeWidth.constant = 40;
        self.wormholePlusImage.hidden = YES;
        self.wormholePlusWidth.constant = 0;
        self.wormholeLeading.constant = 0;
    }
}

- (void)startSingleViewAnimation {
    if (self.bottomView.alpha < 1) {
        return;
    }
    self.authorView.alpha = self.titleLabel.alpha = self.starButton.alpha = self.descLabel.alpha = self.bottomView.alpha = 0;
    [self.authorView startSingleViewAnimationDelay:0.0];
    [self.titleLabel startSingleViewAnimationDelay:0.3];
    [self.starButton startSingleViewAnimationDelay:0.3];
    [self.descLabel startSingleViewAnimationDelay:0.6];
    [self.bottomView startSingleViewAnimationDelay:0.9];
    
    [self resetAnimation];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.75 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self startAnimationIfNeeded];
    });
}

- (void)dataDidChange {
    VIDEO *video = self.data;
    
    USER *author = video.owner;
    if (author.avatar) {
        [self.authorAvatar setImageWithPhoto:author.avatar placeholderImage:[AppTheme avatarDefaultImage]];
    } else {
        self.authorAvatar.image = [AppTheme avatarDefaultImage];
    }
    self.authorName.text = author.name;
    self.authorDesc.text = author.introduce;
    
    if (author == nil || [author.id isEqualToString:[UserModel sharedInstance].user.id]) {
        self.authorFollowView.hidden = YES;
    } else {
        self.authorFollowView.hidden = NO;
        self.followButton.selected = author.is_follow && author.is_author;
    }
    
    self.collectCountLabel.text = [NSString stringWithFormat:@"%@", video.collect_count_array.number ? [video.collect_count_array.number videoCountNumber] : @(0)];
//    self.collectAnimatedView.count = [NSString stringWithFormat:@"%@", video.collect_count ? [video.collect_count setupCountNumber] : @(0)];
    self.collectAnimatedView.isSelected = video.is_collect;
    
    self.likeCountLabel.text = [NSString stringWithFormat:@"%@", video.like_count_array.number ? [video.like_count_array.number videoCountNumber] : @(0)];
//    self.likeAnimatedView.count = [NSString stringWithFormat:@"%@", video.like_count ? [video.like_count setupCountNumber] : @(0)];
    self.likeAnimatedView.isSelected = video.is_like;
    
    self.titleLabel.attributedText = [AppTheme titleAttributedString:video.title];
    self.descLabel.attributedText = [AppTheme detailAttributedString:video.subtitle];
    [self.starButton setTitle:[NSString stringWithFormat:@"%.1f", video.star?:0.0] forState:UIControlStateNormal];
    self.commentCountLabel.text = [NSString stringWithFormat:@"%@", video.comment_count_array.number ? [video.comment_count_array.number videoCountNumber] : @(0)];
    self.shareCountLabel.text = [NSString stringWithFormat:@"%@", video.share_count_array.number ? [video.share_count_array.number videoCountNumber] : @(0)];
    
    [self startAnimationIfNeeded];
}

#pragma mark - Animation

- (void)resetAnimation {
    VIDEO *video = self.data;
    video.like_count_array.hasRolled = NO;
    video.comment_count_array.hasRolled = NO;
    video.collect_count_array.hasRolled = NO;
    video.share_count_array.hasRolled = NO;
    
    [self stopAnimationIfShowing];
    
    if (video.like_count_array.roll.longValue == 1 && video.like_count_array.hasRolled == NO) {
        self.likeCountLabel.text = @"0";
    }
    if (video.comment_count_array.roll.longValue == 1 && video.comment_count_array.hasRolled == NO) {
        self.commentCountLabel.text = @"0";
    }
    if (video.collect_count_array.roll.longValue == 1 && video.collect_count_array.hasRolled == NO) {
        self.collectCountLabel.text = @"0";
    }
    if (video.share_count_array.roll.longValue == 1 && video.share_count_array.hasRolled == NO) {
        self.shareCountLabel.text = @"0";
    }
}

- (void)startAnimationIfNeeded {
    VIDEO *video = self.data;
    if (video.like_count_array.roll.longValue == 1 && video.like_count_array.hasRolled == NO) {
        [self startAnimationInLabel:self.likeCountLabel
                           interval:video.like_count_array.time.floatValue
                               list:video.like_count_array.list
                              atEnd:video.like_count_array.number];
        video.like_count_array.hasRolled = YES;
    }
    if (video.comment_count_array.roll.longValue == 1 && video.comment_count_array.hasRolled == NO) {
        [self startAnimationInLabel:self.commentCountLabel
                           interval:video.comment_count_array.time.floatValue
                               list:video.comment_count_array.list
                              atEnd:video.comment_count_array.number];
        video.comment_count_array.hasRolled = YES;
    }
    if (video.collect_count_array.roll.longValue == 1 && video.collect_count_array.hasRolled == NO) {
        [self startAnimationInLabel:self.collectCountLabel
                           interval:video.collect_count_array.time.floatValue
                               list:video.collect_count_array.list
                              atEnd:video.collect_count_array.number];
        video.collect_count_array.hasRolled = YES;
    }
    if (video.share_count_array.roll.longValue == 1 && video.share_count_array.hasRolled == NO) {
        [self startAnimationInLabel:self.shareCountLabel
                           interval:video.share_count_array.time.floatValue
                               list:video.share_count_array.list
                              atEnd:video.share_count_array.number];
        video.share_count_array.hasRolled = YES;
    }
}

- (void)startAnimationInLabel:(UILabel *)label interval:(CFTimeInterval)interval list:(NSArray *)list atEnd:(NSNumber *)end {
    NSMutableArray *newList = @[].mutableCopy;
    for (id obj in list) {
        if ([obj isKindOfClass:[NSNumber class]]) {
            [newList addObject:[(NSNumber *)obj videoCountNumber]];
        } else if ([obj isKindOfClass:[NSString class]]) {
            [newList addObject:obj];
        }
    }
    [newList addObject:[end videoCountNumber]];
    [self startAnimationInLabel:label duration:interval list:newList];
    
//    if (list.count > (index + 1)) {
//        CGFloat from = [(NSNumber *)list[index] floatValue];
//        CGFloat to = [(NSNumber *)list[index+1] floatValue];
//        CGFloat duration = interval/1000.0;
//        __weak typeof(self) wself = self;
//        [label pp_fromNumber:from toNumber:to duration:duration animationOptions:PPCounterAnimationOptionCurveLinear format:^NSString *(CGFloat currentNumber) {
//            return [@(from) videoCountNumber];
//        } completion:^(CGFloat endNumber) {
//            [wself startAnimationInLabel:label interval:interval list:list atIndex:index+1];
//        }];
//    } else {
//        NSNumber *end = list.lastObject;
//        label.text = [end videoCountNumber];
//    }
}

- (void)startAnimationInLabel:(UILabel *)label duration:(CFTimeInterval)msec list:(NSArray<NSString *> *)list {
    if (list.count < 1) return;
    
    if (!self.timersByAnimateLabel) {
        self.timersByAnimateLabel = @{}.mutableCopy;
    }
    
    __block NSInteger index = 0;
    label.text = list[index];
    dispatch_source_t timer = /*dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue())*/[self createDispatchTimerForLabel:label];
    dispatch_source_set_timer(timer, DISPATCH_TIME_NOW, msec * NSEC_PER_MSEC, 0);
    dispatch_source_set_event_handler(timer, ^{
        index++;
        if (index < list.count) {
            label.text = list[index];
        } else {
//            dispatch_source_cancel(timer);
            [self closeDispatchTimerForLabel:label];
        }
    });
    dispatch_resume(timer);
}

- (dispatch_source_t)dispatchTimerForLabel:(UILabel *)label {
    DispatchTimerWapper *wrapper = [self.timersByAnimateLabel objectForKey:@(label.hash)];
    if (wrapper) {
        return wrapper.timer;
    } else {
        return nil;
    }
}

- (dispatch_source_t)createDispatchTimerForLabel:(UILabel *)label {
    if (!label) {
        return nil;
    }
    DispatchTimerWapper *wrapper = [DispatchTimerWapper new];
    wrapper.timer = dispatch_source_create(DISPATCH_SOURCE_TYPE_TIMER, 0, 0, dispatch_get_main_queue());
    [self.timersByAnimateLabel setObject:wrapper forKey:@(label.hash)];
    return wrapper.timer;
}

- (void)removeDispatchTimerForLabel:(UILabel *)label {
    if (!label) {
        return;
    }
    if ([self dispatchTimerForLabel:label]) {
        [self.timersByAnimateLabel removeObjectForKey:@(label.hash)];
    }
}

- (void)closeDispatchTimerForLabel:(UILabel *)label {
    dispatch_source_t timer = [self dispatchTimerForLabel:label];
    if (timer) {
        dispatch_source_cancel(timer);
        [self removeDispatchTimerForLabel:label];
    }
}

- (void)stopAnimationIfShowing {
    for (DispatchTimerWapper *wrapper in [self.timersByAnimateLabel allValues]) {
        dispatch_source_cancel(wrapper.timer);
    }
    [self.timersByAnimateLabel removeAllObjects];
}

#pragma mark - Action

- (IBAction)likeVideoAction:(UIButton *)sender {
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }
    
    VIDEO *video = self.data;
    
    if (video.is_like) {
//        long value = [video.like_count longValue];
//        video.like_count = [NSNumber numberWithLong:value - 1];
//        self.likeAnimatedView.count = [NSString stringWithFormat:@"%@", video.like_count ? [video.like_count videoCountNumber] : @(0)];
//        self.likeAnimatedView.isSelected = NO;
        long value = [video.like_count_array.number longValue];
        video.like_count_array.number = [NSNumber numberWithLong:value + 1];
        self.likeAnimatedView.count = [NSString stringWithFormat:@"%@", video.like_count_array.number ? [video.like_count_array.number videoCountNumber] : @(1)];
        [self.likeAnimatedView startAnimation];
        self.likeAnimatedView.isSelected = YES;
    } else {
        long value = [video.like_count_array.number longValue];
        video.like_count_array.number = [NSNumber numberWithLong:value + 1];
        self.likeAnimatedView.count = [NSString stringWithFormat:@"%@", video.like_count_array.number ? [video.like_count_array.number videoCountNumber] : @(1)];
        [self.likeAnimatedView startAnimation];
        self.likeAnimatedView.isSelected = YES;
    }
    
    [self dataDidChange];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(likeVideo:sender:)]) {
        [self.delegate likeVideo:video sender:sender];
    }
}

- (IBAction)collectVideoAction:(UIButton *)sender {
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }
    VIDEO *video = self.data;
    
    if (video.is_collect) {
        long value = [video.collect_count_array.number longValue];
        if (value > 0) {
            video.collect_count_array.number = @(value - 1);
            [self.collectAnimatedView startMinusAnimation];
        }
        self.collectAnimatedView.isSelected = NO;
    } else {
        long value = [video.collect_count_array.number longValue];
        video.collect_count_array.number = @(value + 1);
        [self.collectAnimatedView startAnimation];
        self.collectAnimatedView.isSelected = YES;
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(collectVideo:sender:)]) {
        [self.delegate collectVideo:video sender:sender];
    }
    
    video.is_collect = !video.is_collect;
    [self dataDidChange];
}

- (IBAction)starVideoAction:(UIButton *)sender {
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }
    VIDEO *video = self.data;

    if (self.delegate && [self.delegate respondsToSelector:@selector(starVideo:sender:)]) {
        [self.delegate starVideo:video sender:sender];
    }
}

- (IBAction)commentVideoAction:(id)sender {
    VIDEO *video = self.data;
    if (self.delegate && [self.delegate respondsToSelector:@selector(commentVideo:sender:)]) {
        [self.delegate commentVideo:video sender:sender];
    }
}

- (void)startCommentAnimation {
    [self.commentAnimatedView startAnimation];
}

- (void)startShareAnimation {
    [self.shareAnimatedView startAnimation];
}

- (IBAction)authorInfoAction:(id)sender {
    VIDEO *video = self.data;
    if (video.owner == nil) {
        return;
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(gotoAuthorInfo:)]) {
        [self.delegate gotoAuthorInfo:video];
    }
}

- (IBAction)followAuthorAction:(UIButton *)sender {
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }
    VIDEO *video = self.data;
    if (self.delegate && [self.delegate respondsToSelector:@selector(followAuthor:sender:)]) {
        [self.delegate followAuthor:video sender:sender];
    }
}

- (IBAction)shareVideoAction:(id)sender {
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }
    VIDEO *video = self.data;
    if (self.delegate && [self.delegate respondsToSelector:@selector(shareVideo:sender:)]) {
        [self.delegate shareVideo:video sender:sender];
    }
}

- (IBAction)addVideoToWormholeAction:(id)sender {
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }
    VIDEO *video = self.data;
    if (self.delegate && [self.delegate respondsToSelector:@selector(addVideoToWormhole:sender:)]) {
        [self.delegate addVideoToWormhole:video sender:sender];
    }
}

@end
