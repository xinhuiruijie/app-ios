//
//  AddWormoleVideoCell.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/3.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "AddWormoleVideoCell.h"

@interface AddWormoleVideoCell ()

@property (weak, nonatomic) IBOutlet UIImageView *wormholePhoto;
@property (weak, nonatomic) IBOutlet UILabel *wormholeTitle;
@property (weak, nonatomic) IBOutlet UILabel *wormholeVideosNumber;

@end

@implementation AddWormoleVideoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)dataDidChange {
    WORMHOLE *wormhole = self.data;
    
    self.wormholePhoto.image = [AppTheme placeholderImage];
    if (wormhole.photo) {
        [self.wormholePhoto setImageWithPhoto:wormhole.photo];
    } else {
        self.wormholePhoto.image = [AppTheme placeholderImage];
    }
    self.wormholeTitle.text = wormhole.title;
    self.wormholeVideosNumber.text = [NSString stringWithFormat:@"%lu条短片", (unsigned long)wormhole.videos.count];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
