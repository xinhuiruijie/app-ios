//
//  VideoStarAlertView.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/6.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "VideoStarAlertView.h"

@interface VideoStarAlertView ()

@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIView *alertView;
@property (weak, nonatomic) IBOutlet UIView *starView;

@property (weak, nonatomic) IBOutlet UIView *alertBackView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel;
@property (weak, nonatomic) IBOutlet UIView *horizontalLine;
@property (weak, nonatomic) IBOutlet UIView *verticalLine;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;


@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalLineCenterX;

@property (nonatomic, assign) NSInteger starValue;

@end

@implementation VideoStarAlertView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.alertView.layer.cornerRadius = 12;
    self.alertView.layer.masksToBounds = YES;
    
    [self normalTheme];
    
    CGFloat originX = 0;
    for (int i = 0; i < 5; i++) {
        originX = i * (30 + 4);
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(originX, 0, 30, 30)];
        btn.tag = i;
        [btn setImage:[UIImage imageNamed:@"icon_star_big_nor"] forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:@"icon_star_big_sel"] forState:UIControlStateSelected];
        btn.adjustsImageWhenHighlighted = NO;
        [btn addTarget:self action:@selector(starAction:) forControlEvents:UIControlEventTouchUpInside];
        [self.starView addSubview:btn];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.frame = self.superview.bounds;
}

#pragma mark - Action

- (void)starAction:(UIButton *)sender {
    self.starValue = (sender.tag + 1) * 2;
    for (int i = 0; i < 5; i++) {
        UIButton *btn = (UIButton *)self.starView.subviews[i];
        if (i <= sender.tag) {
            btn.selected = YES;
        } else {
            btn.selected = NO;
        }
    }
}

- (IBAction)cancelAction:(id)sender {
    if (self.cancelAction) {
        self.cancelAction();
    }
//    [self cancelActionAnimated];
    [self hide];
}

- (IBAction)confirmAction:(id)sender {
    if (self.confirmAction) {
        self.confirmAction(self.starValue);
    }
//    [self confirmActionAnimated];
}

#pragma mark - theme

- (void)normalTheme {
    self.alertBackView.backgroundColor = [UIColor whiteColor];
    self.alertBackView.alpha = 1.0;
    self.titleLabel.textColor = [UIColor colorWithRGBValue:0x343338];
    self.subTitleLabel.textColor = [UIColor colorWithRGBValue:0xA9AEB5];
    self.horizontalLine.backgroundColor = self.verticalLine.backgroundColor = [UIColor blackColor];
    self.horizontalLine.alpha = self.verticalLine.alpha = 0.06;
    [self.confirmButton setTitleColor:[UIColor colorWithRGBValue:0x343338] forState:UIControlStateNormal];
}

- (void)darkTheme {
    self.alertBackView.backgroundColor = [UIColor colorWithRGBValue:0x272B31];
    self.alertBackView.alpha = 0.8;
    self.titleLabel.textColor = [UIColor whiteColor];
    self.subTitleLabel.textColor = [UIColor colorWithRGBValue:0xA9AEB5];
    self.horizontalLine.backgroundColor = self.verticalLine.backgroundColor = [UIColor whiteColor];
    self.horizontalLine.alpha = self.verticalLine.alpha = 0.1;
    [self.confirmButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}

#pragma mark -

- (void)show {
    self.alpha = 0.0;
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 1.0;
    }];
//    [self showActionAnimated];
}

- (void)hide {
    [UIView animateWithDuration:0.25 animations:^{
        self.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)showActionAnimated {
    CGFloat originY = kScreenHeight / 2 + self.alertView.height;
    CATransform3D translate = CATransform3DMakeTranslation(0, -originY, 0); //平移
    CATransform3D rotate = CATransform3DRotate(translate, 20 * M_PI / 180, 0, 0, 1);
    
    self.alertView.layer.anchorPoint = CGPointMake(0.5, 0.5);
    self.alertView.layer.transform = rotate;
    
    self.backView.alpha = 0;
    [UIView animateWithDuration:0.6 delay:0 usingSpringWithDamping:0.6 initialSpringVelocity:0.8 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        self.backView.alpha = 0.5;
        self.alertView.layer.transform = CATransform3DIdentity;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)cancelActionAnimated {
    CGFloat originY = kScreenHeight / 2 + self.alertView.height;
    CATransform3D translate = CATransform3DMakeTranslation(0, originY, 0); //平移
    CATransform3D rotate = CATransform3DRotate(translate, 20 * M_PI / 180, 0, 0, 1);
    
    self.alertView.layer.transform = CATransform3DIdentity;
    self.alertView.layer.anchorPoint = CGPointMake(0.5, 0.5);
    
    self.backView.alpha = 0.5;
    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:1.0 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        self.backView.alpha = 0;
        self.alertView.layer.transform = rotate;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)confirmActionAnimated {
    CATransform3D scale = CATransform3DMakeScale(0.1, 0.1, 1);
    
    self.alertView.layer.transform = CATransform3DIdentity;
    self.alertView.layer.anchorPoint = CGPointMake(0.5, 0.5);
    
    self.backView.alpha = 0.5;
    [UIView animateWithDuration:0.3 animations:^{
        self.backView.alpha = 0;
        self.alertView.layer.transform = scale;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

@end
