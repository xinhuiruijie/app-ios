//
//  VideoRecommendItemCell.m
//  MotherPlanet
//
//  Created by liuyadi on 2018/4/9.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "VideoRecommendItemCell.h"

@interface VideoRecommendItemCell ()

@property (weak, nonatomic) IBOutlet UIView *cornerView;
@property (weak, nonatomic) IBOutlet UIImageView *videoBackImage;
@property (weak, nonatomic) IBOutlet UILabel *videoPlayCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *videoTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *authorNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *videoTimeLabel;

@end

@implementation VideoRecommendItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.cornerView.layer.cornerRadius = 2;
    self.cornerView.layer.masksToBounds = YES;
}

- (void)dataDidChange {
    VIDEO *video = self.data;
    
    [self.videoBackImage setImageWithPhoto:video.photo];
    float videoPlayCount = [video.play_count floatValue];
    if (videoPlayCount >= 10000) {
        self.videoPlayCountLabel.text = [NSString stringWithFormat:@"%0.1fW", videoPlayCount / 10000];
    } else {
        self.videoPlayCountLabel.text = [NSString stringWithFormat:@"%@", video.play_count?:@(0)];
    }
    self.videoTitleLabel.text = video.title;
    self.authorNameLabel.text = video.owner.name;
    self.videoTimeLabel.text = video.video_time;
}

- (IBAction)watchLaterAction:(id)sender {
    VIDEO *video = self.data;
    if (self.delegate && [self.delegate respondsToSelector:@selector(addVideoToLater:)]) {
        [self.delegate addVideoToLater:video];
    }
}

@end
