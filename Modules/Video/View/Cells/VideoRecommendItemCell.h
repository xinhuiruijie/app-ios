//
//  VideoRecommendItemCell.h
//  MotherPlanet
//
//  Created by liuyadi on 2018/4/9.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VideoRecommendItemCellDelegate <NSObject>

@optional
- (void)addVideoToLater:(VIDEO *)video;

@end

@interface VideoRecommendItemCell : UICollectionViewCell

@property (nonatomic, weak) id<VideoRecommendItemCellDelegate> delegate;

@end
