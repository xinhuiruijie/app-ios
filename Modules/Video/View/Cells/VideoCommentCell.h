//
//  VideoCommentCell.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/7.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoCommentCell : UITableViewCell

@property (nonatomic, strong) COMMENT *comment;
@property (nonatomic, strong) REPLY *reply;
@property (nonatomic, copy) void (^onReply)(REPLY *reply);

+ (CGFloat)rowHeightWithComment:(COMMENT *)comment;
+ (CGFloat)rowHeightWithReplys:(NSArray<REPLY *> *)replys;

- (void)hideBottomLine:(BOOL)hide;

@end
