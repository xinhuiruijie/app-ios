//
//  VideoCommentCell.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/7.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "VideoCommentCell.h"

@interface VideoCommentCell() <UITableViewDataSource, UITableViewDelegate>
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *contentLabel;
@property (weak, nonatomic) IBOutlet UIView *bottomMarginLine;

@property (weak, nonatomic) IBOutlet UITableView *replyList;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *replyListHLC;
@end

@implementation VideoCommentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.iconImageView.layer.cornerRadius = 20;
    self.iconImageView.layer.masksToBounds = YES;
    self.iconImageView.contentMode = UIViewContentModeScaleAspectFill;
    
    self.replyList.scrollEnabled = NO;
    [self.replyList registerNib:NSStringFromClass([VideoCommentCell class])];
    
    self.bottomMarginLine.hidden = YES;
}

+ (CGFloat)rowHeightWithComment:(COMMENT *)comment {
    
    CGFloat subviewHeight = 70.0;
    CGFloat h = [comment.content heightWithWidth:kScreenWidth - 70 font:12];
    CGFloat commentHeight = subviewHeight + h;
    
    commentHeight += [self rowHeightWithReplys:comment.replies];
    
    return commentHeight;
}

+ (CGFloat)rowHeightWithReplys:(NSArray<REPLY *> *)replys; {
    CGFloat height = 0;
    for (REPLY *reply in replys) {
        CGFloat subviewHeight = 70.0;
        NSString *content = [NSString stringWithFormat:@"回复 %@：%@", reply.reference.owner.name?:@"--", reply.content];
        CGFloat h = [content heightWithWidth:kScreenWidth - 140 font:12];
        
        height += (subviewHeight + h);
    }
    
    return height;
}

- (void)startSingleViewAnimationDelay:(CGFloat)delay {
    [self.contentView startSingleViewAnimationDelay:0.3 + delay];
}

- (void)setComment:(COMMENT *)comment {
    _comment = comment;
    
    CGFloat replyListHeight = [[self class] rowHeightWithReplys:comment.replies];
    self.replyListHLC.constant = replyListHeight;
    self.replyList.hidden = comment.replies.count <= 0;
    
    [self.iconImageView setImageWithPhoto:comment.owner.avatar placeholderImage:[AppTheme avatarDefaultImage]];
    self.nameLabel.text = comment.owner.name ?:@"--";
    self.timeLabel.text = [NSDate formatter:@"yyyy-MM-dd HH:mm" dateStr:comment.created_at];
    self.contentLabel.text = comment.content;
    
    if (comment.replies.count > 0 && !self.replyList.dataSource) {
        self.replyList.dataSource = self;
        self.replyList.delegate = self;
    }
    if (comment.replies.count > 0) {
        [self.replyList reloadData];
    }
}

- (void)setReply:(REPLY *)reply {
    _reply = reply;
    
    self.replyListHLC.constant = 0;
    self.replyList.hidden = YES;
    
    [self.iconImageView setImageWithPhoto:reply.owner.avatar placeholderImage:[AppTheme avatarDefaultImage]];
    self.nameLabel.text = reply.owner.name ?:@"--";
    self.timeLabel.text = [NSDate formatter:@"yyyy-MM-dd HH:mm" dateStr:reply.created_at];
    self.contentLabel.text = [NSString stringWithFormat:@"回复 %@：%@", reply.reference.owner.name?:@"--", reply.content];
}

- (void)hideBottomLine:(BOOL)hide {
    self.bottomMarginLine.hidden = hide;
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.comment.replies.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VideoCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([VideoCommentCell class]) forIndexPath:indexPath];
    REPLY *reply = self.comment.replies[indexPath.row];
    cell.reply = reply;
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    REPLY *reply = self.comment.replies[indexPath.row];
    return [VideoCommentCell rowHeightWithReplys:@[reply]];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (self.onReply) {
        REPLY *reply = self.comment.replies[indexPath.row];
        self.onReply(reply);
    }
}

@end
