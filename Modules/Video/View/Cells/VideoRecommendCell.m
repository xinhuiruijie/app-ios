//
//  VideoRecommendCell.m
//  MotherPlanet
//
//  Created by liuyadi on 2018/4/9.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "VideoRecommendCell.h"
#import "VideoRecommendItemCell.h"

@interface VideoRecommendCell () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout, VideoRecommendItemCellDelegate>

@property (nonatomic, strong) NSArray *datasources;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation VideoRecommendCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self.collectionView registerNib:[VideoRecommendItemCell nib] forCellWithReuseIdentifier:@"VideoRecommendItemCell"];
    
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    layout.minimumLineSpacing = 15;
    layout.minimumInteritemSpacing = 15;
    layout.itemSize = CGSizeMake(212, 146);
    
    self.collectionView.contentInset = UIEdgeInsetsMake(0, 10, 0, 10);
}

- (void)startSingleViewAnimationDelay:(CGFloat)delay {
    self.titleLabel.alpha = self.collectionView.alpha = 0;
    [self.titleLabel startSingleViewAnimationDelay:0.0 + delay];
    [self.collectionView startSingleViewAnimationDelay:0.3 + delay];
}

- (void)dataDidChange {
    NSArray *datasources = self.data;
    self.datasources = datasources;
    [self.collectionView reloadData];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.datasources.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    VideoRecommendItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"VideoRecommendItemCell" forIndexPath:indexPath];
    VIDEO *video = self.datasources[indexPath.row];
    cell.data = video;
    cell.delegate = self;
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    VideoRecommendItemCell *cell = (VideoRecommendItemCell *)[collectionView cellForItemAtIndexPath:indexPath];
    VIDEO *video = cell.data;
    if (self.delegate && [self.delegate respondsToSelector:@selector(switchVideoData:)]) {
        [self.delegate switchVideoData:video];
    }
    [AppAnalytics clickEvent:@"click_video_recommendVideo"];
}

#pragma mark - UICollectionViewDelegateFlowLayout

#pragma mark - VideoRecommendItemCellDelegate

- (void)addVideoToLater:(VIDEO *)video {
    if (self.delegate && [self.delegate respondsToSelector:@selector(addVideoToLater:)]) {
        [self.delegate addVideoToLater:video];
    }
}

- (void)resetContentOffset {
    [self.collectionView setContentOffset:CGPointMake(-10, 0)];
}

@end
