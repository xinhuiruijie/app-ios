//
//  VideoAnimatedButton.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/11/30.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoAnimatedButton : UIView

@property (nonatomic, strong) UIImage *normalImage;
@property (nonatomic, strong) UIImage *selectedImage;
@property (nonatomic, strong) NSString *count;

@property (nonatomic, assign) BOOL hideCountLabel;
@property (nonatomic, assign) BOOL isSelected;  // 请先设置count值再设置是否选中

- (void)startAnimation;
- (void)startMinusAnimation;    // 减一动效

@end
