//
//  VideoStarAlertView.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/6.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoStarAlertView : UIView

@property (nonatomic, copy) void(^cancelAction)(void);
@property (nonatomic, copy) void(^confirmAction)(NSInteger starValue);

- (void)show;
- (void)hide;
- (void)confirmActionAnimated;

- (void)normalTheme;
- (void)darkTheme;

@end
