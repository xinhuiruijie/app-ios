//
//  AddWormholeVideoView.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/3.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "AddWormholeVideoView.h"

#import "AddNewWormoleCell.h"
#import "AddWormoleVideoCell.h"

@interface AddWormholeVideoView () <UIGestureRecognizerDelegate, UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, strong) UIButton *darkButton;
@property (nonatomic, strong) UIView *view;
@property (nonatomic, strong) UITableView *table;

@end

@implementation AddWormholeVideoView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        [self customize];
        [self modelInit];
    }
    return self;
}

#pragma mark - TableView Delegate

- (void)customize {
    self.darkButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.darkButton.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
    self.darkButton.backgroundColor = [UIColor blackColor];
    self.darkButton.alpha = 0.5;
    [self.darkButton addTarget:self action:@selector(hiddenAddViewAction) forControlEvents:UIControlEventTouchUpInside];
    [self addSubview:self.darkButton];
    
    CGFloat darkViewHeight = ceil(kScreenWidth * 297 / 375);
    CGFloat viewHeight = ceil(kScreenWidth * 40 / 375);
    self.view = [[UIView alloc] initWithFrame:CGRectMake(0, darkViewHeight, kScreenWidth, viewHeight)];
    self.view.backgroundColor = [AppTheme wormholeNavigationColor];
    [self addSubview:self.view];
    
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, 100, viewHeight)];
    label.text = @"添加到虫洞";
    label.textColor = [AppTheme normalTextColor];
    label.font = [UIFont systemFontOfSize:15];
    [self.view addSubview:label];
    
    self.table = [[UITableView alloc] initWithFrame:CGRectMake(0, darkViewHeight + viewHeight, kScreenWidth, kScreenHeight - darkViewHeight - viewHeight) style:UITableViewStylePlain];
    self.table.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.table.delegate = self;
    self.table.dataSource = self;
    [self addSubview:self.table];
    
    [self.table registerNib:[AddNewWormoleCell nib] forCellReuseIdentifier:@"AddNewWormoleCell"];
    [self.table registerNib:[AddWormoleVideoCell nib] forCellReuseIdentifier:@"AddWormoleVideoCell"];
}

#pragma mark - TableView Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1 + self.wormholeListModel.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        AddNewWormoleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AddNewWormoleCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    } else {
        AddWormoleVideoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AddWormoleVideoCell" forIndexPath:indexPath];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.data = self.wormholeListModel.items[indexPath.row - 1];
        return cell;
    }
    return [UITableViewCell new];
}

#pragma mark - TableView DataSource

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    if (indexPath.row == 0) {
        if (self.wormholeCreate) {
            self.wormholeCreate();
        }
    } else {
        WORMHOLE *wormhole = cell.data;
        if (self.addWormholeVideo) {
            self.addWormholeVideo(wormhole.id);
        }
        [self hide];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 62;
}

#pragma mark - Model

- (void)modelInit {
    @weakify(self);
    self.wormholeListModel = [[UserWormholeListModel alloc] init];
    self.wormholeListModel.userId = [UserModel sharedInstance].user.id;
    self.wormholeListModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self);
        
        [self dismissTips];
        
        if (error == nil) {
            [self setupRefreshFooterView];
            if (self.wormholeListModel.more) {
                [self.table.footer endRefreshing];
            } else {
                [self.table.footer setTitle:MJRefreshFooterStateNoMoreDataText forState:MJRefreshFooterStateNoMoreData];
                [self.table.footer noticeNoMoreData];
            }
        } else {
            [[UIApplication sharedApplication].keyWindow presentMessage:error.message withTips:@"数据获取失败"];
        }
        [self.table reloadData];
    };
}

#pragma mark - Footer

- (void)setupRefreshFooterView {
    if (self.table.footer == nil) {
        if (!self.wormholeListModel.isEmpty) {
            @weakify(self)
            [self.table addFooterPullLoader:^{
                @strongify(self)
                [self.wormholeListModel loadMore];
            }];
        }
    } else {
        if (self.wormholeListModel.isEmpty) {
            [self.table removeFooter];
        }
    }
}

#pragma mark - Action

- (void)hiddenAddViewAction {
    [self hide];
}

- (void)show {
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [self.wormholeListModel refresh];
    [self showActionAnimated];
}

- (void)hide {
    [self hideActionAnimated];
}

- (void)showActionAnimated {
    CATransform3D translate = CATransform3DMakeTranslation(0, kScreenHeight, 0); //平移
    self.view.layer.transform = translate;
    self.table.layer.transform = translate;
    self.darkButton.alpha = 0;
    
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:1.0 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        self.darkButton.alpha = 0.5;
        self.view.layer.transform = CATransform3DIdentity;
        self.table.layer.transform = CATransform3DIdentity;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)hideActionAnimated {
    CATransform3D translate = CATransform3DMakeTranslation(0, kScreenHeight, 0); //平移
    self.view.layer.transform = CATransform3DIdentity;
    self.table.layer.transform = CATransform3DIdentity;
    self.darkButton.alpha = 0.5;
    
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:1.0 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        self.darkButton.alpha = 0;
        self.view.layer.transform = translate;
        self.table.layer.transform = translate;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        [[UIApplication sharedApplication].keyWindow dismissTips];
    }];
}

@end
