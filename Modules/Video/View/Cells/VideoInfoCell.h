//
//  VideoInfoCell.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/6.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VideoInfoCellDelegate <NSObject>

@optional
- (void)followAuthor:(VIDEO *)video sender:(UIButton *)sender;
- (void)likeVideo:(VIDEO *)video sender:(UIButton *)sender;
- (void)commentVideo:(VIDEO *)video sender:(UIButton *)sender;
- (void)collectVideo:(VIDEO *)video sender:(UIButton *)sender;
- (void)starVideo:(VIDEO *)video sender:(UIButton *)sender;
- (void)shareVideo:(VIDEO *)video sender:(UIButton *)sender;
- (void)addVideoToWormhole:(VIDEO *)video sender:(UIButton *)sender;
- (void)gotoAuthorInfo:(VIDEO *)video;
@end

@interface VideoInfoCell : UITableViewCell

@property (nonatomic, weak) id<VideoInfoCellDelegate> delegate;
+ (CGFloat)heightForVideoInfoCell:(VIDEO *)video;
- (void)startCommentAnimation;
- (void)startShareAnimation;
@end
