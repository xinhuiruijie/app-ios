//
//  VideoRecommendCell.h
//  MotherPlanet
//
//  Created by liuyadi on 2018/4/9.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VideoRecommendCellDelegate <NSObject>

@optional
- (void)switchVideoData:(VIDEO *)video;
- (void)addVideoToLater:(VIDEO *)video;

@end

@interface VideoRecommendCell : UITableViewCell

@property (nonatomic, weak) id<VideoRecommendCellDelegate> delegate;

- (void)resetContentOffset;

@end
