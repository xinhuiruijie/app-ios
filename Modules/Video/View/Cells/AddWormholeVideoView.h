//
//  AddWormholeVideoView.h
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/3.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "UserWormholeListModel.h"

@interface AddWormholeVideoView : BaseShareView

@property (nonatomic, copy) NSString *videoId;

@property (nonatomic, strong) UserWormholeListModel *wormholeListModel;

@property (nonatomic, copy) void(^wormholeCreate)(void);
@property (nonatomic, copy) void(^addWormholeVideo)(NSString *wormholeId);

@end
