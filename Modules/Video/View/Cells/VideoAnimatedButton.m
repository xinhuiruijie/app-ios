//
//  VideoAnimatedButton.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/11/30.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "VideoAnimatedButton.h"

@interface VideoAnimatedButton ()
@property (nonatomic, strong) UIImageView *isCollectIcon;
@property (nonatomic, strong) UILabel *collectCount;
@property (nonatomic, strong) UILabel *addCountLabel;
@property (nonatomic, strong) UILabel *minusCountLabel;
@end

@implementation VideoAnimatedButton

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.addCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 22, 22)];
        self.addCountLabel.text = @"+1";
        self.addCountLabel.textAlignment = NSTextAlignmentCenter;
        self.addCountLabel.textColor = [AppTheme selectedColor];
        self.addCountLabel.font = [UIFont systemFontOfSize:10 weight:UIFontWeightMedium];
        self.addCountLabel.alpha = 0;
        
        self.minusCountLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 22, 22)];
        self.minusCountLabel.text = @"-1";
        self.minusCountLabel.textAlignment = NSTextAlignmentCenter;
        self.minusCountLabel.textColor = [AppTheme subTextColor];
        self.minusCountLabel.font = [UIFont systemFontOfSize:10 weight:UIFontWeightMedium];
        self.minusCountLabel.alpha = 0;
        
        self.isCollectIcon = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
        self.isCollectIcon.contentMode = UIViewContentModeScaleAspectFit;
        
        self.collectCount = [[UILabel alloc] initWithFrame:CGRectMake(17, 0, frame.size.width - 32, 13)];
        self.collectCount.font = [UIFont fontWithName:@"Gotham-Book" size:11];
        self.collectCount.adjustsFontSizeToFitWidth = YES;
        self.collectCount.minimumScaleFactor = 0.5;
        
        [self addSubview:self.addCountLabel];
        [self addSubview:self.isCollectIcon];
        [self addSubview:self.collectCount];
        [self addSubview:self.minusCountLabel];
        
        self.hideCountLabel = NO;
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    if (self.width != 70) {
        self.width = CGRectGetMaxX(self.collectCount.frame);
    }
}

- (void)startAnimation {
    [UIView animateWithDuration:0.12 animations:^{
        self.isCollectIcon.transform = CGAffineTransformMakeScale(0.0, 0.0);
    } completion:^(BOOL finished) {
        self.isCollectIcon.image = self.selectedImage;
        [UIView animateWithDuration:0.2 animations:^{
            self.isCollectIcon.transform = CGAffineTransformMakeScale(1.0, 1.0);
        } completion:^(BOOL finished) {
            [UIView animateWithDuration:0.12 animations:^{
                self.isCollectIcon.transform = CGAffineTransformMakeScale(1.2, 1.2);
            } completion:^(BOOL finished) {
                [UIView animateWithDuration:0.24 animations:^{
                    self.isCollectIcon.transform = CGAffineTransformMakeScale(0.94, 0.94);
                } completion:^(BOOL finished) {
                    [UIView animateWithDuration:0.28 animations:^{
                        self.isCollectIcon.transform = CGAffineTransformMakeScale(1.01, 1.01);
                    } completion:^(BOOL finished) {
                        [UIView animateWithDuration:0.12 animations:^{
                            self.isCollectIcon.transform = CGAffineTransformMakeScale(1.0, 1.0);
                        }];
                    }];
                }];
            }];
        }];
    }];
    
    NSLog(@"******** start add 1");
    [UIView animateWithDuration:0.48 delay:0.24 options:UIViewAnimationOptionLayoutSubviews animations:^{
        self.addCountLabel.transform = CGAffineTransformMakeTranslation(0, -15);
        NSLog(@"******** start add 2");
    } completion:^(BOOL finished) {
        self.addCountLabel.transform = CGAffineTransformMakeTranslation(0, 0);
        NSLog(@"******** start add 3");
    }];
    [UIView animateWithDuration:0.40 delay:0.24 options:UIViewAnimationOptionLayoutSubviews animations:^{
        self.addCountLabel.alpha = 1;
        NSLog(@"******** start add 4");
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.08 animations:^{
            self.addCountLabel.alpha = 0;
            NSLog(@"******** start add 5");
        }];
    }];
}

- (void)startMinusAnimation {
    NSLog(@"******** start minus 1");
    [UIView animateWithDuration:0.48 delay:0.24 options:UIViewAnimationOptionLayoutSubviews animations:^{
        self.minusCountLabel.transform = CGAffineTransformMakeTranslation(0, -15);
        NSLog(@"******** start minus 2");
    } completion:^(BOOL finished) {
        self.minusCountLabel.transform = CGAffineTransformMakeTranslation(0, 0);
        NSLog(@"******** start minus 3");
    }];
    [UIView animateWithDuration:0.40 delay:0.24 options:UIViewAnimationOptionLayoutSubviews animations:^{
        self.minusCountLabel.alpha = 1;
        NSLog(@"******** start minus 4");
    } completion:^(BOOL finished) {
        [UIView animateWithDuration:0.08 animations:^{
            self.minusCountLabel.alpha = 0;
            NSLog(@"******** start minus 5");
        }];
    }];
}

- (void)setHideCountLabel:(BOOL)hideCountLabel {
    _hideCountLabel = hideCountLabel;
    self.collectCount.hidden = hideCountLabel;
}

- (void)setIsSelected:(BOOL)isSelected {
    _isSelected = isSelected;
    self.collectCount.text = self.count;
    if (self.width != 70) {
        self.collectCount.width = [AppTheme calculateLikeAnimateLabelWidthWithContent:self.count];
    }
    if (isSelected) {
        self.isCollectIcon.image = self.selectedImage;
        self.collectCount.textColor = [AppTheme selectedColor];
    } else {
        self.isCollectIcon.image = self.normalImage;
        self.collectCount.textColor = [AppTheme normalColor];
    }
}

@end
