//
//  TransitioningDelegate.h
//  MotherPlanet
//
//  Created by GeekZooStudio on 2017/12/18.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TransitioningDelegate : NSObject <UIViewControllerTransitioningDelegate>
@property (nonatomic, assign) CGRect finalFrame;
@end
