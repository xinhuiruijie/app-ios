//
//  MPBackGuideView.h
//  MotherPlanet
//
//  Created by YaoAngel on 2018/12/25.
//  Copyright © 2018 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class MPBackGuideView;

@protocol MPBackGuideViewDelegate <NSObject>
@optional

- (void)guideViewDismiss:(MPBackGuideView *)guideView;

@end

@interface MPBackGuideView : UIView

@property (nonatomic, weak) id <MPBackGuideViewDelegate> delegate;

+ (instancetype)showBackGuide;

- (void)dismiss;

@end

NS_ASSUME_NONNULL_END
