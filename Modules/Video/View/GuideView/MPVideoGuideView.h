//
//  MPVideoGuideView.h
//  MotherPlanet
//
//  Created by YaoAngel on 2018/12/24.
//  Copyright © 2018 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class MPVideoGuideView;

@protocol MPVideoGuideViewDelegate <NSObject>

@optional

- (void)dismissOnGuideFinish:(MPVideoGuideView *)guideView;

@end

@interface MPVideoGuideView : UIView

@property (nonatomic, weak) id<MPVideoGuideViewDelegate> delegate;

+ (instancetype)showWithImageOriginY:(CGFloat)y;

@end

NS_ASSUME_NONNULL_END
