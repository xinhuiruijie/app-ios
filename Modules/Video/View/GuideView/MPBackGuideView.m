//
//  MPBackGuideView.m
//  MotherPlanet
//
//  Created by YaoAngel on 2018/12/25.
//  Copyright © 2018 Geek Zoo Studio. All rights reserved.
//

#import "MPBackGuideView.h"

@interface MPBackGuideView ()

@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) CAShapeLayer *shapeLayer;
@property (nonatomic, strong) UIImageView *backImage;
@property (nonatomic, strong) UIButton *actionButton;

@end

@implementation MPBackGuideView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:[UIScreen mainScreen].bounds];
    if (self) {
        _containerView = [[UIView alloc] initWithFrame:self.bounds];
        _containerView.backgroundColor = [UIColor clearColor];
        [self addSubview:_containerView];
        
        _shapeLayer = [CAShapeLayer layer];
        _shapeLayer.fillRule = kCAFillRuleEvenOdd;
        _shapeLayer.fillColor = [UIColor blackColor].CGColor;
        _shapeLayer.opacity = 0.8;
        [_containerView.layer addSublayer:_shapeLayer];
        
        _backImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"guide_rightslide"]];
        [self addSubview:_backImage];
        
        _actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_actionButton setBackgroundImage:[UIImage imageNamed:@"guide_button"] forState:UIControlStateNormal];
        [_actionButton.titleLabel setFont:[UIFont systemFontOfSize:16]];
        [_actionButton setTitle:@"我知道了" forState:UIControlStateNormal];
        [_actionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_actionButton addTarget:self action:@selector(actionButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_actionButton];
    }
    return self;
}

- (void)actionButtonClick:(UIButton *)btn {
    [self dismiss];
}

- (void)dismiss {
    if ([self.delegate respondsToSelector:@selector(guideViewDismiss:)]) {
        [self.delegate guideViewDismiss:self];
    }
    [self removeFromSuperview];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGFloat width = CGRectGetWidth(self.bounds);
    CGFloat height = CGRectGetHeight(self.bounds);
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0,0,self.window.bounds.size.width, self.window.bounds.size.height)cornerRadius:0];
    _shapeLayer.path = path.CGPath;
    _actionButton.frame = CGRectMake(width/2 - 60, height - 150, 120, 42);
    _backImage.frame = CGRectMake(width/2-120, height/2-100, 240, 150);
}

+ (instancetype)showBackGuide {
    MPBackGuideView *guideView = [[MPBackGuideView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [[UIApplication sharedApplication].keyWindow addSubview:guideView];
    return guideView;
}

@end
