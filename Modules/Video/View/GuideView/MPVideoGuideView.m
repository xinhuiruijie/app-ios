//
//  MPVideoGuideView.m
//  MotherPlanet
//
//  Created by YaoAngel on 2018/12/24.
//  Copyright © 2018 Geek Zoo Studio. All rights reserved.
//

#import "MPVideoGuideView.h"

typedef enum : NSUInteger {
    VideoGuideStateLike = 1,
    VideoGuideStateComment,
    VideoGuideStateCollect,
    VideoGuideStateShare,
    VideoGuideStateFinish
} VideoGuideState;

@interface MPVideoGuideView ()

@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) CAShapeLayer *shapeLayer;
@property (nonatomic, strong) UIImageView *likeImage;
@property (nonatomic, strong) UIImageView *commentImage;
@property (nonatomic, strong) UIImageView *collectImage;
@property (nonatomic, strong) UIImageView *shareImage;
@property (nonatomic, strong) UIButton *actionButton;
@property VideoGuideState state;
@property CGFloat originY;

@end

@implementation MPVideoGuideView

- (instancetype)initWithFrame:(CGRect)frame originY:(CGFloat)originY {
    self = [super initWithFrame:frame];
    if (self) {
        _containerView = [[UIView alloc] initWithFrame:self.bounds];
        _containerView.backgroundColor = [UIColor clearColor];
        [self addSubview:_containerView];
        
        _shapeLayer = [CAShapeLayer layer];
        _shapeLayer.fillRule = kCAFillRuleEvenOdd;
        _shapeLayer.fillColor = [UIColor blackColor].CGColor;
        _shapeLayer.opacity = 0.8;
        [_containerView.layer addSublayer:_shapeLayer];
        
        _likeImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"guide_like"]];
        _commentImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"guide_comment"]];
        _collectImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"guide_collection"]];
        _shareImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"guide_share"]];
        [self addSubview:_likeImage];
        [self addSubview:_commentImage];
        [self addSubview:_collectImage];
        [self addSubview:_shareImage];
        
        _actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_actionButton setBackgroundImage:[UIImage imageNamed:@"guide_button"] forState:UIControlStateNormal];
        [_actionButton.titleLabel setFont:[UIFont systemFontOfSize:16]];
        [_actionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [_actionButton addTarget:self action:@selector(actionButtonClick:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:_actionButton];
        
        _originY = originY;
        
        [self setup];
    }
    return self;
}

- (void)setup {
    [self displayGuideState:VideoGuideStateLike];
}

- (void)jumpNextState {
    VideoGuideState newState = self.state + 1;
    [self displayGuideState:newState];
}

- (void)displayGuideState:(VideoGuideState)state {
    _state = state;
    if (state == VideoGuideStateFinish) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(dismissOnGuideFinish:)]) {
            [self.delegate dismissOnGuideFinish:self];
        }
        [self removeFromSuperview];
    } else {
        _likeImage.hidden = (state != VideoGuideStateLike);
        _commentImage.hidden = (state != VideoGuideStateComment);
        _collectImage.hidden = (state != VideoGuideStateCollect);
        _shareImage.hidden = (state != VideoGuideStateShare);
        if (state == VideoGuideStateShare) {
            [_actionButton setTitle:@"我知道了" forState:UIControlStateNormal];
        } else {
            [_actionButton setTitle:@"下一步" forState:UIControlStateNormal];
        }
    }
}

- (void)actionButtonClick:(UIButton *)btn {
    
    [self jumpNextState];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGFloat width = CGRectGetWidth(self.bounds);
    CGFloat height = CGRectGetHeight(self.bounds);
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0,0,self.window.bounds.size.width, self.window.bounds.size.height)cornerRadius:0];
    _shapeLayer.path = path.CGPath;
    
    CGFloat imageHeight = 126;
    CGFloat imageWidth = 270;
    CGFloat y = (_originY > 0.0) ? _originY : height*0.5;
    _likeImage.frame = CGRectMake(8, y, imageWidth, imageHeight);
    _commentImage.frame = CGRectMake(60, y, imageWidth, imageHeight);
    _collectImage.frame = CGRectMake(46, y, imageWidth, imageHeight);
    _shareImage.frame = CGRectMake(48, y, imageWidth, imageHeight);
    _actionButton.frame = CGRectMake(width/2 - 60, height - 150, 120, 42);
}

+ (instancetype)showWithImageOriginY:(CGFloat)y {
    MPVideoGuideView *guideView = [[MPVideoGuideView alloc] initWithFrame:[UIScreen mainScreen].bounds originY:y];
    [[UIApplication sharedApplication].keyWindow addSubview:guideView];
    return guideView;
}

@end
