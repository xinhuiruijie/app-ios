//  QualityListView.h
//  leciyuan
//
//  Created by Chenyun on 16/1/8.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface QualityListView : UIView

@property (nonatomic, copy) void (^whenCanceled)(void);
@property (nonatomic, copy) void (^whenSelected)(NSNumber *index);

- (void)reloadData;

@end
