//  PlayerErrorMaskView.h
//  leciyuan
//
//  Created by Chenyun on 16/1/25.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, PlayerErrorMaskViewStatus) {
	PlayerErrorMaskViewStatusStoped = 0,  // 播放停止
	PlayerErrorMaskViewStatusPlaying = 1,  // 播放中
	PlayerErrorMaskViewStatusPaused = 2,  // 播放暂停
	PlayerErrorMaskViewStatusInterrupted = 3,  // 播放断开
	PlayerErrorMaskViewStatusLoadStalled = 6,  // 缓冲中
	PlayerErrorMaskViewStatusLoadPlayable = 7,  // 缓冲到可以播放
	PlayerErrorMaskViewStatusPlaybackStateUnknow = 8,  // 播放器初始状态
	PlayerErrorMaskViewStatusPlaybackWillPlay = 9,  // 将要播放
    PlayerErrorMaskViewStatusVideoError = 10,  // URL错误
};

@interface PlayerErrorMaskView : UIView

@property (nonatomic, assign) PlayerErrorMaskViewStatus state;

@end
