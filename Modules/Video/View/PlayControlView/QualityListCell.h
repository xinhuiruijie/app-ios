//  QualityListCell.h
//  leciyuan
//
//  Created by Chenyun on 16/1/11.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface VideoQualityInfo : NSObject
@property (nonatomic, strong) id quality;
@property (nonatomic, assign) BOOL isSelected;
@end

@interface QualityListCell : UICollectionViewCell

@end
