//
//  SmallPanelMoreViewCell.h
//  MotherPlanet
//
//  Created by liuyadi on 2018/4/13.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

typedef NS_ENUM(NSInteger, PANEL_MORE_STYLE) {
    PANEL_MORE_STYLE_SHARE = 0,
    PANEL_MORE_STYLE_LATER,
    PANEL_MORE_STYLE_REPORT,
};

#import <UIKit/UIKit.h>

@interface SmallPanelMoreViewCell : UITableViewCell

@end
