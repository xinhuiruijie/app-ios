//  BrightnessTipView.m
//  leciyuan
//
//  Created by Chenyun on 16/1/14.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import "BrightnessTipView.h"

static const NSInteger kBrightnessTipViewWidth = 156; // 7 + 1
static const NSInteger kBrightnessTipViewTickWidth = 8;   // 7 + 1
static const NSInteger kBrightnessTipViewTicksCount = 16; // 7 + 1

static UIInterfaceOrientation kGetDeviceOrientation() {
    return [UIApplication sharedApplication].statusBarOrientation;
}

@interface BrightnessTipView ()

@property (weak, nonatomic) IBOutlet UIView * progressView;
@property (weak, nonatomic) IBOutlet UIImageView * brightnessView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint * progressViewWC;
@property (strong, nonatomic) UIToolbar * backgroudView;
@property (strong, nonatomic) UIToolbar * backgroudView2;
@property (assign, nonatomic) BOOL shown;

@end

@implementation BrightnessTipView

+ (instancetype)sharedView
{
    static BrightnessTipView * view = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        view = [BrightnessTipView loadFromNib];
        view.frame = CGRectMake(0, 0, kBrightnessTipViewWidth, kBrightnessTipViewWidth);
    });
    return view;
}

- (void)relayout
{
    CGSize screenSize = [UIScreen mainScreen].bounds.size;
    CGRect frame = CGRectMake(0, 0, kBrightnessTipViewWidth, kBrightnessTipViewWidth);
    CGFloat x = (screenSize.width - kBrightnessTipViewWidth) / 2;
    CGFloat y = (screenSize.height - kBrightnessTipViewWidth) / 2;

    UIInterfaceOrientation statusBarOrientation = kGetDeviceOrientation();
    switch (statusBarOrientation) {
        case UIInterfaceOrientationPortrait:
        case UIInterfaceOrientationPortraitUpsideDown: {
            frame.origin.x = x;
            frame.origin.y = y - 5; // this 5 is a secret...
            break;
        }
        case UIInterfaceOrientationUnknown:
        case UIInterfaceOrientationLandscapeLeft:
        case UIInterfaceOrientationLandscapeRight: {
            frame.origin.x = x;
            frame.origin.y = y;
            break;
        }
    }
    
    self.frame = frame;
}

- (void)awakeFromNib
{
	[super awakeFromNib];
    
    self.clipsToBounds = YES;
    self.layer.cornerRadius = 10;
    self.backgroudView = [[UIToolbar alloc] initWithFrame:self.bounds];

    [self.layer insertSublayer:self.backgroudView.layer atIndex:1];
    
    self.brightnessView.image = [[UIImage imageNamed:@"brightness"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.brightnessView.tintColor = [UIColor blackColor];

    self.progressView.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"brightness-tick"]];
    
    self.alpha = 0;
    self.hidden = YES;
}

- (void)show
{
    self.shown = YES;
    
    if ( !self.superview )
    {
        [self relayout];
        [[UIApplication sharedApplication].keyWindow addSubview:self];
    }
    
    self.hidden = NO;

    [NSObject cancelPreviousPerformRequestsWithTarget:self selector:@selector(hide) object:nil];
    
    [UIView animateWithDuration:0
                          delay:0
                        options:UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.alpha = 1;
                     } completion:^(BOOL finished) {
                         [self performSelector:@selector(hide) withObject:nil afterDelay:3];
                     }];
}

- (void)hide
{
    if ( self.hidden )
        return;
    
    self.shown = NO;
    
    [UIView animateWithDuration:1
                          delay:1
                        options:UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionCurveEaseOut
                     animations:^{
                         self.alpha = 0;
                     } completion:^(BOOL finished) {
                         if ( !self.shown ) {
                             self.hidden = YES;
                             [self removeFromSuperview];
                         }
                     }];
}

- (void)setProgress:(CGFloat)progress
{
    _progress = progress;
    
    NSInteger index = (NSInteger)(kBrightnessTipViewTicksCount * progress + 0.5);
    
    self.progressViewWC.constant = kBrightnessTipViewTickWidth * index;
}

@end
