//  PlayerNetworkMaskView.h
//  leciyuan
//
//  Created by Chenyun on 16/1/28.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayerNetworkMaskView : UIView

@property (nonatomic, copy) void (^whenStopPlay)(void);
@property (nonatomic, copy) void (^whenRefresh)(void);
@property (nonatomic, copy) void (^whenBackClicked)(void);

@property (nonatomic, assign) AFNetworkReachabilityStatus state;

@property (weak, nonatomic) IBOutlet UIButton *maskButton;

@end
