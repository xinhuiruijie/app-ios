//  PlayerErrorMaskView.m
//  leciyuan
//
//  Created by Chenyun on 16/1/25.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import "PlayerErrorMaskView.h"
#import "NetworkReachabilityManager.h"

@interface PlayerErrorMaskView ()
@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIImageView *loadingImage;
@property (weak, nonatomic) IBOutlet UILabel *loadingImageLabel;
@property (weak, nonatomic) IBOutlet UILabel *playbackStatusLabel;
@end

@implementation PlayerErrorMaskView

- (void)awakeFromNib {
	[super awakeFromNib];

	// 设置imageview
    self.loadingImage.animationImages = @[[UIImage imageNamed:@"icon_cut1"], [UIImage imageNamed:@"icon_cut2"], [UIImage imageNamed:@"icon_cut3"]];
    self.loadingImage.animationDuration = 1.0;
    self.loadingImage.animationRepeatCount = 0;

    [self.loadingImage startAnimating];
    self.playbackStatusLabel.hidden = YES;
}

- (void)layoutSubviews {
	[super layoutSubviews];
}

#pragma mark -

- (void)setState:(PlayerErrorMaskViewStatus)state {
	_state = state;

	// 根据网络状态判断显示还是隐藏
	switch (state) {
		case PlayerErrorMaskViewStatusStoped: {
			// 播放停止
			[self playbackStoped];
		}
			break;
		case PlayerErrorMaskViewStatusPlaying: {
			// 播放中
			[self playbackPlaying];
		}
			break;
		case PlayerErrorMaskViewStatusPaused: {
			// 播放暂停
			[self playbackPaused];
		}
			break;
		case PlayerErrorMaskViewStatusInterrupted: {
			// 播放断开
			[self playbackInterrupted];
		}
			break;
		case PlayerErrorMaskViewStatusLoadStalled: {
			// 缓冲中
			[self playbackLoadStalled];
		}
			break;
		case PlayerErrorMaskViewStatusLoadPlayable: {
			// 缓冲到可以播放
            [self playbackLoadPlayable];
		}
			break;
		case PlayerErrorMaskViewStatusPlaybackStateUnknow: {
			// 播放器初始状态
			[self playbackStateUnknow];
		}
			break;
		case PlayerErrorMaskViewStatusPlaybackWillPlay: {
			// 将要播放
			[self playbackWillPlay];
		}
			break;
        case PlayerErrorMaskViewStatusVideoError: {
            // 播放断开
            [self playbackInterrupted];
        }
            break;
		default:
			break;
	}
}

#pragma mark -

- (void)networkViewhidden {
	self.loadingImage.hidden = YES;
	self.loadingImageLabel.hidden = YES;
	self.playbackStatusLabel.hidden = YES;

	self.bgView.alpha = 1.0;
}

#pragma mark -

- (void)playbackStoped {
	[self networkViewhidden];

	self.hidden = YES;
    [self stopLoading];

    self.playbackStatusLabel.text = @"停止播放";
	self.playbackStatusLabel.hidden = NO;
}

- (void)playbackPlaying {
	[self networkViewhidden];

	self.hidden = YES;
    [self stopLoading];

    self.playbackStatusLabel.text = @"正在播放";
	self.playbackStatusLabel.hidden = NO;
}

- (void)playbackPaused {
	[self networkViewhidden];

	self.hidden = YES;

    [self stopLoading];

    self.playbackStatusLabel.text = @"暂停播放";
	self.playbackStatusLabel.hidden = NO;
}

- (void)playbackInterrupted {
	[self networkViewhidden];
    
    self.playbackStatusLabel.text = @"播放失败";
	self.playbackStatusLabel.hidden = NO;

 	self.hidden = NO;
}

- (void)playbackLoadStalled {
    // 缓冲中
	[self networkViewhidden];

	self.hidden = NO;

    [self startLoading];

	self.bgView.alpha = 0.3;
}

- (void)playbackLoadPlayable {
    [self networkViewhidden];
    
    self.hidden = YES;
    [self stopLoading];
    
    self.playbackStatusLabel.text = @"正在播放";
    self.playbackStatusLabel.hidden = NO;
}

- (void)playbackStateUnknow {
	[self networkViewhidden];
	
	self.hidden = NO;
	
	[self startLoading];
}

- (void)playbackWillPlay {
	[self networkViewhidden];

	self.hidden = NO;

	[self startLoading];
}

- (void)startLoading {
    if (self.loadingImage.hidden) {
        self.loadingImageLabel.hidden = NO;
        self.loadingImage.hidden = NO;
        [self.loadingImage startAnimating];
    }
}

- (void)stopLoading {
    if (self.loadingImage.isAnimating) {
        [self.loadingImage stopAnimating];
    }
}

@end
