//  NetworkReachabilityManager.m
//  leciyuan
//
//  Created by Chenyun on 16/1/25.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import "NetworkReachabilityManager.h"

@interface NetworkReachabilityManager ()

@property (nonatomic, assign) AFNetworkReachabilityStatus oldStatus;

@end

@implementation NetworkReachabilityManager

@def_singleton( NetworkReachabilityManager )

- (instancetype)init
{
	self = [super init];
	
	if (self) {
		[self addNetworkingReachabilityObserver];
	}

	return self;
}

- (void)addNetworkingReachabilityObserver
{
	[[NSNotificationCenter defaultCenter] addObserver:self
											 selector:@selector(onNetworkingReachabilityDidChange:)
												 name:AFNetworkingReachabilityDidChangeNotification
											   object:nil];
}

- (void)removeNetworkingReachabilityObserver
{
	[[NSNotificationCenter defaultCenter] removeObserver:self
													name:AFNetworkingReachabilityDidChangeNotification
												  object:nil];
}

- (void)onNetworkingReachabilityDidChange:(NSNotification *)noti
{
	NSDictionary * info = noti.userInfo;

	if ( [info objectForKey:@"AFNetworkingReachabilityNotificationStatusItem"] )
	{
		AFNetworkReachabilityStatus status = ((NSNumber *)[info objectForKey:@"AFNetworkingReachabilityNotificationStatusItem"]).integerValue;

		switch ( status )
		{
			case AFNetworkReachabilityStatusUnknown:
			case AFNetworkReachabilityStatusNotReachable:
			{
				// 无网络状态
				if ( self.whenNotReachability )
				{
					self.whenNotReachability();
				}
			}
				break;
			case AFNetworkReachabilityStatusReachableViaWWAN:
			{
				// 处于运营商网络 2G/3G/4G网络
				if ( self.whenReachableViaWWAN )
				{
					self.whenReachableViaWWAN();
				}
			}
				break;
			case AFNetworkReachabilityStatusReachableViaWiFi:
			{
				// 处于wifi网络
				if ( self.whenWIFI )
				{
					self.whenWIFI();
				}
			}
				break;
			default:
				break;
		}
	}
}

- (void)dealloc
{
	[self removeNetworkingReachabilityObserver];
}

@end
