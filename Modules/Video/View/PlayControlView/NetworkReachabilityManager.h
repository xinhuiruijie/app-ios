//  NetworkReachabilityManager.h
//  leciyuan
//
//  Created by Chenyun on 16/1/25.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NetworkReachabilityManager : NSObject

@singleton( NetworkReachabilityManager )

///**
// *  从wifi切换到无网
// */
//@property (nonatomic, copy) void (^whenWIFIToNotReachability)( void );
//
///**
// *  从wifi切换到运营商环境
// */
//@property (nonatomic, copy) void (^whenWIFIToReachableViaWWAN)( void );
//
///**
// *  从无网切换到wifi
// */
//@property (nonatomic, copy) void (^whenNotReachabilityToWIFI)( void );
//
///**
// *  从无网切换到运营商
// */
//@property (nonatomic, copy) void (^whenNotReachabilityToReachableViaWWAN)( void );
//
///**
// *  从运营商切换到wifi
// */
//@property (nonatomic, copy) void (^whenReachableViaWWANToWIFI)( void );
//
///**
// *  从运营商切换到无网
// */
//@property (nonatomic, copy) void (^whenReachableViaWWANToNotReachability)( void );

/**
 *  切换到无网
 */
@property (nonatomic, copy) void (^whenNotReachability)( void );

/**
 *  切换到运营商
 */
@property (nonatomic, copy) void (^whenReachableViaWWAN)( void );

/**
 *  切换到WIFI
 */
@property (nonatomic, copy) void (^whenWIFI)( void );

@end
