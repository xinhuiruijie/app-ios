//
//  VideoPlayManager.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/10.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoState.h"

typedef NS_ENUM(NSInteger, PlayerMaskViewStatus) {
    PlayerMaskViewStatusNone = 0, // 默认状态
    PlayerMaskViewStatusStoped = 1, // 播放停止
    PlayerMaskViewStatusPlayer = 2, // 播放中
    PlayerMaskViewStatusPaused = 3, // 播放暂停
    PlayerMaskViewStatusLoadStalled = 4, // 缓冲中
    PlayerMaskViewStatusLoadPlayable = 5, // 缓冲到可以播放
    PlayerMaskViewStatusNetworkInterrupt = 6, // 网络状态更新，播放断开（主动）
    
    PlayerMaskViewStatusWait = 7,       // 等待播放
    PlayerMaskViewStatusEndWait = 8,       // 等待结束，用户主动调用播放
};

@protocol VideoPlayManagerDelegate <NSObject>

@optional
- (void)refreshVideoManager;
- (void)clickBackButton;
- (void)statusBarHide:(BOOL)hide;
- (void)fullScreenCollectVideo:(VIDEO *)video sender:(UIButton *)sender;
- (void)fullScreenStarVideo:(VIDEO *)video sender:(UIButton *)sender;
- (void)shareVideo:(VIDEO *)video isFullScreen:(BOOL)isFullScreen;
- (void)addVideoToLater:(VIDEO *)video;
- (void)reportVideo:(VIDEO *)video;
- (void)showSmallPanelMoreMenu;
- (void)startPlayVideo;

@end

@interface VideoPlayManager : UIView

@property (nonatomic, weak) id<VideoPlayManagerDelegate> delegate;
@property (nonatomic, assign) PlayerMaskViewStatus state;
@property (nonatomic, strong) VideoStateManager *stateManager;
@property (nonatomic, assign) BOOL isPlaying;
@property (nonatomic, assign) BOOL isWaiting;

@property (nonatomic, strong) VIDEO *video;

@property (nonatomic, assign) BOOL isFullScreen;
@property (nonatomic, assign) BOOL isHide;

@property (nonatomic, copy) BOOL (^typeOfSmallPanelMoreMenuHid)(void);

- (void)needChangeScreenWithFullView;
- (void)reloadFullViewWithIsFull:(BOOL)isFull;

- (void)resumeVideo;
- (void)pauseVideo;
- (void)stopVideo;

- (void)resumeLoad; // 开始加载(仅控制Player)
- (void)stopLoad;   // 停止加载，并不销毁(仅控制Player)

@end
