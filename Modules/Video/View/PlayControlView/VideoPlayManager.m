//
//  VideoPlayManager.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/10.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "VideoPlayManager.h"
#import "VIDEO+Extension.h"
#import "ScheduleTipView.h"
#import "BrightnessTipView.h"
#import "Utilities.h"
#import "PlayerLoadingView.h"
#import "PlayerNetworkMaskView.h"
#import "QualityListView.h"
#import "VideoSrtDownloader.h"
#import "UserPointModel.h"
#import <TXLiteAVSDK_UGC/TXVodPlayer.h>
#import <MediaPlayer/MediaPlayer.h>

@interface VideoPlayManager () <TXLivePlayListener>

@property (weak, nonatomic) IBOutlet UIImageView *videoAvatar;

@property (weak, nonatomic) IBOutlet UIView *videoSmallPanelView;
@property (weak, nonatomic) IBOutlet UIView *smallBackView;
@property (weak, nonatomic) IBOutlet UIImageView *smallVideoAvatar;
@property (weak, nonatomic) IBOutlet UIButton *smallPlayButton;
@property (weak, nonatomic) IBOutlet UILabel *smallPlayTime;
@property (weak, nonatomic) IBOutlet UIButton *smallRotateButton;
@property (weak, nonatomic) IBOutlet UILabel *smallDataUseLabel;
@property (weak, nonatomic) IBOutlet UISlider *smallBackBufferProgress;
@property (weak, nonatomic) IBOutlet UISlider *smallBackPlaySlider;
@property (weak, nonatomic) IBOutlet UILabel *smallStartTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *smallEndTimeLabel;
@property (weak, nonatomic) IBOutlet UISlider *smallBufferProgress;
@property (weak, nonatomic) IBOutlet UISlider *smallPlayProgress;
@property (weak, nonatomic) IBOutlet UIButton *smallBottomPlayButton;
@property (weak, nonatomic) IBOutlet UIView *smallTopActionView;
@property (weak, nonatomic) IBOutlet UIView *smallBottomActionView;
@property (weak, nonatomic) IBOutlet UIView *smallBottomSliderView;
@property (weak, nonatomic) IBOutlet UIView *smallBackBottomActionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *smallTopActionViewHC;

@property (weak, nonatomic) IBOutlet UIView *videoLargePanelView;
@property (weak, nonatomic) IBOutlet UIView *largeBackView;
@property (weak, nonatomic) IBOutlet UIImageView *largeVideoAvatar;
@property (weak, nonatomic) IBOutlet UIButton *largePlayButton;
@property (weak, nonatomic) IBOutlet UIView *largeTopActionView;
@property (weak, nonatomic) IBOutlet UIView *largeBottomActionView;

@property (weak, nonatomic) IBOutlet UIButton *largeRateButton;
@property (weak, nonatomic) IBOutlet UILabel *largeStartTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *largeEndTimeLabel;
@property (weak, nonatomic) IBOutlet UISlider *largeBufferProgress;
@property (weak, nonatomic) IBOutlet UISlider *largePlayProgress;
@property (weak, nonatomic) IBOutlet UILabel *largeTitleLabel;
@property (weak, nonatomic) IBOutlet UIButton *largeQualityButton;
@property (weak, nonatomic) IBOutlet UIButton *largeCollectButton;
@property (weak, nonatomic) IBOutlet UIButton *largeStarButton;
@property (weak, nonatomic) IBOutlet UIButton *largeShareButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *largeBottomActionHC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *largeQualityButtonWC;

@property (weak, nonatomic) IBOutlet UILabel *translateLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *translateLabelBC;

@property (weak, nonatomic) IBOutlet UIButton *transparentButton;

@property (nonatomic, strong) MPVolumeView *mpVolumeView;
@property (nonatomic, strong) UISlider *mpVolumeSlider;

@property (nonatomic, strong) TXVodPlayer *txVodPlayer;
@property (nonatomic, assign) BOOL startSeek;
@property (nonatomic, assign) long long trackingTouchTS;
@property (nonatomic, assign) float sliderValue;
@property (nonatomic, strong) NSString *videoUrl;
@property (nonatomic, strong) NSTimer * hideViewTimer;
@property (nonatomic, assign) CGFloat currentTimer;
@property (nonatomic, assign) CGFloat currentRate;

@property (nonatomic, assign) CGPoint beginPosition;
@property (nonatomic, assign) BOOL isHengMoved;
@property (nonatomic, assign) BOOL isShuMoved;
@property (nonatomic, assign) BOOL controlPanelEnabled;
@property (nonatomic, assign) BOOL isSlide;

@property (nonatomic, strong) NSMutableArray *supportQualities;

@property (nonatomic, strong) ScheduleTipView * scheduleTipView;
@property (nonatomic, strong) BrightnessTipView * brightnessTipView;
@property (nonatomic, strong) QualityListView * listView;
@property (nonatomic, strong) PlayerNetworkMaskView * playerNetworkMaskView;
@property (nonatomic, strong) PlayerLoadingView *playerLoadingView;

// 字幕
@property (nonatomic, strong) VideoSrtDownloader *downloader;
@property (nonatomic, strong) NSMutableArray *beginTimeArray;
@property (nonatomic, strong) NSMutableArray *endTimeArray;
@property (nonatomic, strong) NSMutableArray *subtitlesArray;

@end

@implementation VideoPlayManager

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self setupSubviewStyle];
    [self setupLoadingView];
    
    TXVodPlayer *txVodPlayer = [[TXVodPlayer alloc] init];
    [txVodPlayer setupVideoWidget:self insertIndex:0];
    txVodPlayer.delegate = self;
    [txVodPlayer setRenderMode:RENDER_MODE_FILL_SCREEN];
    self.txVodPlayer = txVodPlayer;
    
    [self setupNetworkMaskView];
    [self setupQualityList];
//    [self setupHideViewTimer];
    
    self.supportQualities = [NSMutableArray array];
    
    // 双击手势
    UITapGestureRecognizer *smallDoubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap)];
    smallDoubleTapGesture.numberOfTapsRequired = 2;
    smallDoubleTapGesture.numberOfTouchesRequired = 1;
    [self.videoSmallPanelView addGestureRecognizer:smallDoubleTapGesture];
    
    UITapGestureRecognizer *largeDoubleTapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleDoubleTap)];
    largeDoubleTapGesture.numberOfTapsRequired = 2;
    largeDoubleTapGesture.numberOfTouchesRequired = 1;
    [self.videoLargePanelView addGestureRecognizer:largeDoubleTapGesture];
    
    self.downloader = [[VideoSrtDownloader alloc] init];
    self.controlPanelEnabled = YES;
    
    self.currentRate = 1;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    if ([SDiOSVersion deviceSize] == Screen5Dot8inch) {
        self.smallTopActionViewHC.constant = 44;
        self.largeBottomActionHC.constant = 66;
    } else {
        self.smallTopActionViewHC.constant = 64;
        self.largeBottomActionHC.constant = 45;
    }
    
    if (@available(iOS 11.0, *)) {
        self.translateLabelBC.constant = self.isFullScreen ? (self.safeAreaInsets.bottom + 6) : 6;
    } else {
        self.translateLabelBC.constant = 6;
    }
    self.playerLoadingView.frame = CGRectMake(0, 0, self.width, self.height);
    self.scheduleTipView.frame = CGRectMake(self.width / 2 - 66, self.height / 2 - 30, 117, 66);
    if ([SDiOSVersion deviceSize] == Screen5Dot8inch && !self.isFullScreen) {
        self.scheduleTipView.center = CGPointMake(self.center.x, self.center.y - 44);
    } else {
        self.scheduleTipView.center = self.center;
    }
}

- (void)dealloc {
    // 停止播放
    [self.hideViewTimer invalidate];
    self.hideViewTimer = nil;
    
    [self.txVodPlayer stopPlay];
    self.txVodPlayer.delegate = nil;
    [self.txVodPlayer removeVideoWidget]; // 记得销毁view控件
    self.txVodPlayer = nil;
    
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
}

#pragma mark - Subview

- (void)setupSubviewStyle {
    [self.smallBackPlaySlider setThumbImage:[UIImage imageNamed:@"video_transparent_line"] forState:UIControlStateNormal];
    [self.smallBufferProgress setThumbImage:[UIImage imageNamed:@"icon_playpoint_line"] forState:UIControlStateNormal];
    [self.smallPlayProgress setThumbImage:[UIImage imageNamed:@"icon_video_small_point"] forState:UIControlStateNormal];
    [self.smallBackBufferProgress setThumbImage:[UIImage imageNamed:@"icon_playpoint_line"] forState:UIControlStateNormal];
    [self.smallPlayProgress addTarget:self action:@selector(onSeek:) forControlEvents:(UIControlEventValueChanged)];
    [self.smallPlayProgress addTarget:self action:@selector(onSeekBegin:) forControlEvents:(UIControlEventTouchDown)];
    [self.smallPlayProgress addTarget:self action:@selector(onDrag:) forControlEvents:UIControlEventTouchDragInside];
    
    [self.largePlayProgress setThumbImage:[UIImage imageNamed:@"icon_video_playpoint"] forState:UIControlStateNormal];
    [self.largeBufferProgress setThumbImage:[UIImage imageNamed:@"icon_playpoint_line"] forState:UIControlStateNormal];
    [self.largePlayProgress addTarget:self action:@selector(onSeek:) forControlEvents:(UIControlEventValueChanged)];
    [self.largePlayProgress addTarget:self action:@selector(onSeekBegin:) forControlEvents:(UIControlEventTouchDown)];
    [self.largePlayProgress addTarget:self action:@selector(onDrag:) forControlEvents:UIControlEventTouchDragInside];
    
    // 快进与后退时候显示的提示view
    self.scheduleTipView = [ScheduleTipView loadFromNib];
    self.scheduleTipView.hidden = YES;
    [self addSubview:self.scheduleTipView];
    
    // 亮度
    self.brightnessTipView = [BrightnessTipView sharedView];
}

- (void)setupLoadingView {
    self.playerLoadingView = [PlayerLoadingView loadFromNib];
    self.playerLoadingView.frame = CGRectMake(0, 0, self.width, self.height);
    self.playerLoadingView.hidden = YES;
    [self insertSubview:self.playerLoadingView atIndex:1];
}

- (void)setupNetworkMaskView {
    self.playerNetworkMaskView = [PlayerNetworkMaskView loadFromNib];
    self.playerNetworkMaskView.frame = CGRectMake(0, 0, self.width, self.height);
    
    @weakify(self);
    self.playerNetworkMaskView.whenStopPlay = ^(void) {
        @strongify(self)
        // 先判断当前是否已经在播放中,还没有进行播放，那么就不处理
        [self pauseVideo];
        self.state = PlayerMaskViewStatusNetworkInterrupt;
        self.isHide = YES;
        
        self.smallVideoAvatar.hidden = self.largeVideoAvatar.hidden = YES;
    };
    self.playerNetworkMaskView.whenRefresh = ^(void) {
        @strongify(self);
        if (self.delegate && [self.delegate respondsToSelector:@selector(refreshVideoManager)]) {
            [self.delegate refreshVideoManager];
        }
        [self syncNetworkStatus];
        self.state = PlayerMaskViewStatusLoadStalled;
    };
    self.playerNetworkMaskView.whenBackClicked = ^(void) {
        @strongify(self);
        if (self.isFullScreen) {
            [self largeBackButtonAction:nil];
        } else {
            [self smallBackButtonAction:nil];
        }
    };
    
    // 初始化网络状态
    [self syncNetworkStatus];
    
//    [self addSubview:self.playerNetworkMaskView];
    [self insertSubview:self.playerNetworkMaskView atIndex:2];
}

#pragma mark - Small Panel Action

- (IBAction)smallBottomPlayButtonAction:(UIButton *)sender {
    if (self.smallBottomPlayButton.selected) {
        NSMutableArray *animationImages = [NSMutableArray array];
        for (NSInteger i = 39; i < 53; i++) {
            UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"Pause_000%ld.png", i]];
            [animationImages addObject:image];
        }
        self.smallBottomPlayButton.imageView.animationImages = animationImages;
    } else {
        NSMutableArray *animationImages = [NSMutableArray array];
        for (NSInteger i = 8; i < 22; i++) {
            UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"Play_000%ld.png", i]];
            [animationImages addObject:image];
        }
        self.smallBottomPlayButton.imageView.animationImages = animationImages;
    }
    self.smallBottomPlayButton.imageView.animationDuration = 0.5;
    self.smallBottomPlayButton.imageView.animationRepeatCount = 1;
    [self.smallBottomPlayButton.imageView startAnimating];
    if (sender) {
        [self smallPlayButtonAction:self.smallPlayButton];
    }
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        self.smallBottomPlayButton.imageView.animationImages = nil;
    });
}

- (IBAction)smallPlayButtonAction:(UIButton *)sender {
    if (self.state == PlayerMaskViewStatusWait) {
        self.state = PlayerMaskViewStatusEndWait;
    }
    self.smallDataUseLabel.hidden = YES;
    self.txVodPlayer.isAutoPlay = YES;
    sender.selected = !sender.selected;
    if (sender.selected) {
        self.state = PlayerMaskViewStatusPaused;
    }
    
    self.smallBottomPlayButton.selected = self.smallPlayButton.selected = self.largePlayButton.selected = sender.selected;

    if (self.state == PlayerMaskViewStatusStoped) {
        [self.txVodPlayer startPlay:self.videoUrl];
        [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    } else if (self.state == PlayerMaskViewStatusPaused) {
        [self.txVodPlayer resume];
        [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    } else if (self.state == PlayerMaskViewStatusPlayer) {
        [self.txVodPlayer pause];
        [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    }
    self.isHide = NO;
}

- (IBAction)smallRotateButtonAction:(id)sender {
    self.isFullScreen = YES;
    [self changeScreen];
    self.videoSmallPanelView.hidden = YES;
    self.videoLargePanelView.hidden = NO;
}

- (IBAction)smallBackButtonAction:(id)sender {
    [self.hideViewTimer invalidate];
    self.hideViewTimer = nil;
    
    [self.txVodPlayer stopPlay];
    self.txVodPlayer.delegate = nil;
    [self.txVodPlayer removeVideoWidget]; // 记得销毁view控件
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(clickBackButton)]) {
        [self.delegate clickBackButton];
    }
}

- (IBAction)smallMoreMenuButtonAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(showSmallPanelMoreMenu)]) {
        [self.delegate showSmallPanelMoreMenu];
    }
}

#pragma mark - Large Panel Action

- (IBAction)largePlayButtonAction:(UIButton *)sender {
    if (self.state == PlayerMaskViewStatusWait) {
        self.state = PlayerMaskViewStatusEndWait;
    }
    self.txVodPlayer.isAutoPlay = YES;
    sender.selected = !sender.selected;
    if (sender.selected) {
        self.state = PlayerMaskViewStatusPaused;
    }
    
    self.smallBottomPlayButton.selected = self.smallPlayButton.selected = self.largePlayButton.selected = sender.selected;

    if (self.state == PlayerMaskViewStatusStoped) {
        [self.txVodPlayer startPlay:self.videoUrl];
        [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    } else if (self.state == PlayerMaskViewStatusPaused) {
        [self.txVodPlayer resume];
        [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    } else if (self.state == PlayerMaskViewStatusPlayer) {
        [self.txVodPlayer pause];
        [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    }
}

- (IBAction)largeRateButtonAction:(UIButton *)sender {
    if (self.currentRate == 1) {
        self.currentRate = 1.5;
        [self.largeRateButton setImage:[UIImage imageNamed:@"icon_play_1xx"] forState:UIControlStateNormal];
    } else if (self.currentRate == 1.5) {
        self.currentRate = 2;
        [self.largeRateButton setImage:[UIImage imageNamed:@"icon_play_2x"] forState:UIControlStateNormal];
    } else if (self.currentRate == 2) {
        self.currentRate = 1;
        [self.largeRateButton setImage:[UIImage imageNamed:@"icon_play_1x"] forState:UIControlStateNormal];
    }
    [self.txVodPlayer setRate:self.currentRate];
}

- (IBAction)largeRotateButtonAction:(id)sender {
    self.isFullScreen = NO;
    [self changeScreen];
    self.videoSmallPanelView.hidden = NO;
    self.videoLargePanelView.hidden = YES;
}

- (IBAction)largeBackButtonAction:(id)sender {
    self.isFullScreen = NO;
    [self changeScreen];
    self.videoSmallPanelView.hidden = NO;
    self.videoLargePanelView.hidden = YES;
}

- (IBAction)largeShareButtonAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(shareVideo:isFullScreen:)]) {
        [self.delegate shareVideo:self.video isFullScreen:YES];
    }
}

- (IBAction)largeStarButtonAction:(id)sender {
    if (self.video.is_star) {
        return;
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(fullScreenStarVideo:sender:)]) {
        [self.delegate fullScreenStarVideo:self.video sender:sender];
    }
}

- (IBAction)largeCollectButtonAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(fullScreenCollectVideo:sender:)]) {
        [self.delegate fullScreenCollectVideo:self.video sender:sender];
    }
}

- (IBAction)largeQualityButtonAction:(id)sender {
    [self qualityList];
}

#pragma mark - Video Action

- (BOOL)checkPlayUrl:(NSString*)playUrl {
    if ([playUrl hasPrefix:@"https:"] || [playUrl hasPrefix:@"http:"]) {
        if ([playUrl rangeOfString:@".flv"].length > 0 || [playUrl rangeOfString:@".m3u8"].length > 0 || [playUrl rangeOfString:@".mp4"].length > 0) {
            return YES;
        } else {
            return NO;
        }
    } else {
        return NO;
    }
}

- (void)resumeVideo {
    // 没有在播放中，恢复播放
    if (self.txVodPlayer.isPlaying) {
        return;
    }
    [self smallPlayButtonAction:self.smallPlayButton];
}

- (void)pauseVideo {
    // 在播放中，暂停播放
    if (!self.txVodPlayer.isPlaying) {
        return;
    }
    [self smallPlayButtonAction:self.smallPlayButton];
}

- (void)stopVideo {
    [self.txVodPlayer stopPlay];
    self.txVodPlayer.delegate = nil;
    [self.txVodPlayer removeVideoWidget]; // 记得销毁view控件
    self.txVodPlayer = nil;
}

- (void)resumeLoad {
    if (self.videoUrl == nil) {
        return;
    }
    [self.txVodPlayer startPlay:self.videoUrl];
}

- (void)stopLoad {
    [self.txVodPlayer stopPlay];
}

- (void)setupQualityList {
    @weakify(self);
    // 清晰度列表
    self.listView = [QualityListView loadFromNib];
    self.listView.whenCanceled = ^{
        @strongify(self);
        [self qualityList];
    };
    self.listView.whenSelected = ^(NSNumber *index) {
        @strongify(self);
        [self qualityList];
        // 切换短片的清晰度
        if ([index isEqualToNumber:@(VIDEO_QUALITY_NORMAL)]) {
            [self.largeQualityButton setTitle:@"标清(480P)" forState:UIControlStateNormal];
            self.videoUrl = self.video.video_url;
            [[self topViewController].view presentMessageTips:@"已为您切换到标清"];
        } else if ([index isEqualToNumber:@(VIDEO_QUALITY_SD)]) {
            [self.largeQualityButton setTitle:@"高清(720P)" forState:UIControlStateNormal];
            self.videoUrl = self.video.video_url_sd;
            [[self topViewController].view presentMessageTips:@"已为您切换到高清"];
        } else if ([index isEqualToNumber:@(VIDEO_QUALITY_HD)]) {
            [self.largeQualityButton setTitle:@"超清(1080P)" forState:UIControlStateNormal];
            self.videoUrl = self.video.video_url_hd;
            [[self topViewController].view presentMessageTips:@"已为您切换到超清"];
        }
        self.largeQualityButtonWC.constant = 88;
        
        // 切换清晰度，停止播放上一短片，开始播放该短片，默认让播放按钮选中，获取到上个短片的播放进度，切换清晰度时seek到上次观看位置
        self.video.watchTime = [NSString stringWithFormat:@"%.2f", self.smallPlayProgress.value];
        [self.stateManager setWatchTime:self.video.watchTime forIdentifier:self.video.id];
        if (self.isPlaying) {
            [self.txVodPlayer stopPlay];
            [self.txVodPlayer startPlay:self.videoUrl];
        } else {
            self.txVodPlayer.isAutoPlay = NO;
            [self.txVodPlayer stopPlay];
            [self.txVodPlayer startPlay:self.videoUrl];
        }
    };
    
    self.listView.hidden = YES;
    [self addSubview:self.listView];
}

- (void)qualityList {
    self.isHide = YES;
    if (self.listView.hidden) {
        self.listView.frame = CGRectMake(self.width, 0, self.width, self.height);
        
        self.listView.hidden = !self.listView.hidden;
        [UIView animateWithDuration:0.3 animations:^{
            self.listView.frame = CGRectMake(0, 0, self.width, self.height);
        }];
    } else {
        [UIView animateWithDuration:0.3 animations:^{
            self.listView.frame = CGRectMake(self.width, 0, self.width, self.height);
        } completion:^(BOOL finished) {
            self.listView.hidden = !self.listView.hidden;
        }];
    }
    
    self.listView.data = self.supportQualities;
}

#pragma mark - Video Delegate

- (void)changeScreen {
    int val = self.isFullScreen?UIInterfaceOrientationLandscapeRight:UIInterfaceOrientationPortrait;
    
    if ([[UIDevice currentDevice] respondsToSelector:@selector(setOrientation:)]) {
        NSNumber *num = [[NSNumber alloc] initWithInt:val];
        
        [[UIDevice currentDevice] performSelector:@selector(setOrientation:) withObject:(id)num];
        [UIViewController attemptRotationToDeviceOrientation];//这行代码是关键
    }
    
    SEL selector = NSSelectorFromString(@"setOrientation:");
    
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:[UIDevice instanceMethodSignatureForSelector:selector]];
    [invocation setSelector:selector];
    [invocation setTarget:[UIDevice currentDevice]];
    [invocation setArgument:&val atIndex:2];
    [invocation invoke];
}

- (void)onPlayEvent:(int)EvtID withParam:(NSDictionary*)param {
    NSDictionary* dict = param;
    
    dispatch_async(dispatch_get_main_queue(), ^{
//        NSLog(@"-----------  EvtID = %d  ----------------- self.videoUrl = %@", EvtID, self.videoUrl);
        if (EvtID == PLAY_EVT_PLAY_PROGRESS) {
            if (self.startSeek) {
                return;
            }
            if (self.state != PlayerMaskViewStatusPlayer) {
                self.state = PlayerMaskViewStatusPlayer;
                self.playerLoadingView.hidden = YES;
            }
            // 避免滑动进度条松开的瞬间可能出现滑动条瞬间跳到上一个位置
            long long curTs = [[NSDate date]timeIntervalSince1970]*1000;
            if (llabs(curTs - self.trackingTouchTS) < 500) {
                return;
            }
            self.trackingTouchTS = curTs;
            
            float progress = [dict[EVT_PLAY_PROGRESS] floatValue];
            float duration = [dict[EVT_PLAY_DURATION] floatValue];
            
            // 防止手势拖拽时受到影响造成时间回退
            if (!self.isHengMoved) {
                self.currentTimer = progress;
            }
            
            int intDuration = duration + 0.5;
            if (duration > 0 && self.smallPlayProgress.maximumValue != duration) {
                [self.smallPlayProgress setMaximumValue:duration];
                [self.smallBufferProgress setMaximumValue:duration];
                [self.smallBackPlaySlider setMaximumValue:duration];
                [self.smallBackBufferProgress setMaximumValue:duration];
                [self.largePlayProgress setMaximumValue:duration];
                [self.largeBufferProgress setMaximumValue:duration];
                self.smallEndTimeLabel.text = self.largeEndTimeLabel.text = [NSString stringWithFormat:@"%02d:%02d", (int)(intDuration / 60), (int)(intDuration % 60)];
            }
            
            [self.smallBackBufferProgress setValue:[dict[EVT_PLAYABLE_DURATION] floatValue]];
            [self.smallBufferProgress setValue:[dict[EVT_PLAY_DURATION] floatValue]];
            [self.largeBufferProgress setValue:[dict[EVT_PLAYABLE_DURATION] floatValue]];

            self.controlPanelEnabled = YES;
            
            // 加载字幕
            [self translateSubtitleText:progress];
            return ;
        } else if (EvtID == PLAY_EVT_PLAY_LOADING) {
            self.state = PlayerMaskViewStatusLoadStalled;
            self.playerLoadingView.hidden = NO;
            if (self.currentTimer == 0) {
                self.videoAvatar.hidden = self.smallVideoAvatar.hidden = self.largeVideoAvatar.hidden = NO;
            } else {
                if (self.state == PlayerMaskViewStatusWait) {
                    self.videoAvatar.hidden = self.smallVideoAvatar.hidden = self.largeVideoAvatar.hidden = NO;
                } else {
                    self.videoAvatar.hidden = self.smallVideoAvatar.hidden = self.largeVideoAvatar.hidden = YES;
                }
            }
        } else if (EvtID == PLAY_EVT_PLAY_BEGIN) {
            self.state = PlayerMaskViewStatusLoadPlayable;
            self.playerLoadingView.hidden = YES;
            if (self.state == PlayerMaskViewStatusWait) {
                self.videoAvatar.hidden = self.smallVideoAvatar.hidden = self.largeVideoAvatar.hidden = NO;
            } else {
                self.videoAvatar.hidden = self.smallVideoAvatar.hidden = self.largeVideoAvatar.hidden = YES;
            }
        } else if (EvtID == PLAY_EVT_CHANGE_RESOLUTION) {
            // 短片分辨率改变 在这里进行切换清晰度从上次观看进度开始的操作，在开始播放处进行会有延迟
            VideoState *state = [self.stateManager stateForIdentifier:self.video.id];
            if (state.watchTime) {
                CGFloat watchTime = state.watchTime.floatValue;
                self.sliderValue = watchTime;
                [self.smallPlayProgress setValue:self.sliderValue animated:YES];
                [self.smallBackPlaySlider setValue:self.sliderValue animated:YES];
                [self.largePlayProgress setValue:self.sliderValue animated:YES];
                [self.txVodPlayer seek:self.sliderValue];
            }
        } else if (EvtID == PLAY_EVT_PLAY_END) {
            self.smallBottomPlayButton.selected = self.smallPlayButton.selected = self.largePlayButton.selected = NO;
            self.currentTimer = 0;
            
            [self.hideViewTimer invalidate];
            self.hideViewTimer = nil;
            
            self.controlPanelEnabled = YES;
            self.state = PlayerMaskViewStatusStoped;
            self.playerLoadingView.hidden = YES;
            
            [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
        } else if (EvtID == PLAY_ERR_NET_DISCONNECT) {
            self.state = PlayerMaskViewStatusNetworkInterrupt;
            self.playerNetworkMaskView.state = AFNetworkReachabilityStatusNotReachable;

            [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
        } else if (EvtID == PLAY_ERR_FILE_NOT_FOUND) {
            [self.txVodPlayer stopPlay];
            
            self.smallBottomPlayButton.selected = self.smallPlayButton.selected = self.largePlayButton.selected = NO;
            [self.smallPlayProgress setValue:0];
            [self.smallBackPlaySlider setValue:0];
            [self.largePlayProgress setValue:0];
            self.smallStartTimeLabel.text = self.largeStartTimeLabel.text = @"00:00";

            [self.hideViewTimer invalidate];
            self.hideViewTimer = nil;
            
            self.controlPanelEnabled = NO;
            self.state = PlayerMaskViewStatusNetworkInterrupt;
            self.playerLoadingView.hidden = YES;
            
            NSString *errorMsg = (NSString*)[dict valueForKey:EVT_MSG];
            [[MPRootViewController sharedInstance].currentViewController presentMessageTips:errorMsg];
            
            [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
        }
    });
}

- (BOOL)onPlayerPixelBuffer:(CVPixelBufferRef)pixelBuffer {
    return NO;
}

- (void)onNetStatus:(NSDictionary *)param {
    NSDictionary* dict = param;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        int netspeed  = [(NSNumber*)[dict valueForKey:NET_STATUS_NET_SPEED] intValue];
        int vbitrate  = [(NSNumber*)[dict valueForKey:NET_STATUS_VIDEO_BITRATE] intValue];
        int abitrate  = [(NSNumber*)[dict valueForKey:NET_STATUS_AUDIO_BITRATE] intValue];
        int cachesize = [(NSNumber*)[dict valueForKey:NET_STATUS_CACHE_SIZE] intValue];
        int dropsize  = [(NSNumber*)[dict valueForKey:NET_STATUS_DROP_SIZE] intValue];
        int jitter    = [(NSNumber*)[dict valueForKey:NET_STATUS_NET_JITTER] intValue];
        int fps       = [(NSNumber*)[dict valueForKey:NET_STATUS_VIDEO_FPS] intValue];
        int width     = [(NSNumber*)[dict valueForKey:NET_STATUS_VIDEO_WIDTH] intValue];
        int height    = [(NSNumber*)[dict valueForKey:NET_STATUS_VIDEO_HEIGHT] intValue];
        float cpu_usage = [(NSNumber*)[dict valueForKey:NET_STATUS_CPU_USAGE] floatValue];
        float cpu_app_usage = [(NSNumber*)[dict valueForKey:NET_STATUS_CPU_USAGE_D] floatValue];
        NSString *serverIP = [dict valueForKey:NET_STATUS_SERVER_IP];
        int codecCacheSize = [(NSNumber*)[dict valueForKey:NET_STATUS_CODEC_CACHE] intValue];
        int nCodecDropCnt = [(NSNumber*)[dict valueForKey:NET_STATUS_CODEC_DROP_CNT] intValue];
        int nCahcedSize = [(NSNumber*)[dict valueForKey:NET_STATUS_CACHE_SIZE] intValue]/1000;
        
        NSString* log = [NSString stringWithFormat:@"CPU:%.1f%%|%.1f%%\tRES:%d*%d\tSPD:%dkb/s\nJITT:%d\tFPS:%d\tARA:%dkb/s\nQUE:%d|%d\tDRP:%d|%d\tVRA:%dkb/s\nSVR:%@\t\tCAH:%d kb",
                         cpu_app_usage*100,
                         cpu_usage*100,
                         width,
                         height,
                         netspeed,
                         jitter,
                         fps,
                         abitrate,
                         codecCacheSize,
                         cachesize,
                         nCodecDropCnt,
                         dropsize,
                         vbitrate,
                         serverIP,
                         nCahcedSize];
        //        [_statusView setText:log];
        //        AppDemoLogOnlyFile(@"Current status, VideoBitrate:%d, AudioBitrate:%d, FPS:%d, RES:%d*%d, netspeed:%d", vbitrate, abitrate, fps, width, height, netspeed);
    });
}

#pragma mark - Video Info Controller

- (void)needChangeScreenWithFullView {
    [self largeRotateButtonAction:nil];
}

- (void)reloadFullViewWithIsFull:(BOOL)isFull {
    self.isFullScreen = isFull;
    if (@available(iOS 11.0, *)) {
        self.translateLabelBC.constant = self.isFullScreen ? (self.safeAreaInsets.bottom + 6) : 6;
    } else {
        self.translateLabelBC.constant = 6;
    }
    self.translateLabel.font = isFull ? [UIFont systemFontOfSize:14] : [UIFont systemFontOfSize:11];
    [self hideViewUIWithisHide:self.isHide];
}

#pragma mark - Video Play Manager UI Status

- (void)hideViewUIWithisHide:(BOOL)hide {
    CGFloat alpa = hide ? 0 : 1;
    if (self.isFullScreen) {
        // 全屏
        self.videoSmallPanelView.hidden = YES;
        
        [UIView animateWithDuration:0.1 animations:^{
            self.videoLargePanelView.hidden = NO;

            self.videoLargePanelView.alpha = alpa;
            if (self.state == PlayerMaskViewStatusLoadStalled || self.state == PlayerMaskViewStatusNetworkInterrupt) {
                self.largePlayButton.alpha = 0;
                self.largeBackView.alpha = 0;
            } else {
                self.largePlayButton.alpha = alpa;
                self.largeBackView.alpha = 0.5;
            }
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(statusBarHide:)]) {
                [self.delegate statusBarHide:hide];
            }
        }];
    } else {
        // 半屏
        self.videoLargePanelView.hidden = YES;
        self.listView.hidden = YES;
        
        [UIView animateWithDuration:0.1 animations:^{
            self.videoSmallPanelView.hidden = NO;

            if (self.state == PlayerMaskViewStatusLoadStalled || self.state == PlayerMaskViewStatusNetworkInterrupt) {
                self.smallTopActionView.alpha = self.smallBottomActionView.alpha = alpa;
                self.smallPlayButton.alpha = self.smallPlayTime.alpha = 0;
                self.smallPlayProgress.alpha = alpa;
            } else {
                if (self.state == PlayerMaskViewStatusWait) {
                    self.smallPlayButton.alpha = self.smallPlayTime.alpha = alpa;
                } else {
                    self.smallPlayButton.alpha = self.smallPlayTime.alpha = 0;
                }
                self.smallTopActionView.alpha = self.smallBottomActionView.alpha = self.smallPlayProgress.alpha = alpa;
            }
            
            // 背景进度条和展示进度条 交替显示 半透明视图跟随展示进度条 同步展示
            if (self.smallBottomActionView.alpha) {
                self.smallBackView.alpha = 0.5;
                self.smallBackBottomActionView.alpha = 0;
            } else {
                self.smallBackView.alpha = 0;
                self.smallBackBottomActionView.alpha = 1;
            }
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(statusBarHide:)]) {
                [self.delegate statusBarHide:hide];
            }
        }];
    }
    
    // 隐藏UI时停止timer
    if (hide) {
        [self.hideViewTimer invalidate];
        self.hideViewTimer = nil;
    } else {
        [self.hideViewTimer invalidate];
        self.hideViewTimer = nil;
        self.hideViewTimer = [NSTimer scheduledTimerWithTimeInterval:5.0
                                                              target:self
                                                            selector:@selector(hiddenPlayerView)
                                                            userInfo:nil
                                                             repeats:YES];
    }
}

- (void)setupHideViewTimer {
    [self.hideViewTimer invalidate];
    self.hideViewTimer = nil;
    self.hideViewTimer = [NSTimer scheduledTimerWithTimeInterval:5.0
                                                          target:self
                                                        selector:@selector(hiddenPlayerView)
                                                        userInfo:nil
                                                         repeats:YES];
}

- (void)hiddenPlayerView {
    if (self.state == PlayerMaskViewStatusWait) {
        return;
    }
    
    //  根据 moremenu.hidden 状态判断，显示则不收起，收起重新计时准备隐藏
    if (self.typeOfSmallPanelMoreMenuHid) {
        if (self.typeOfSmallPanelMoreMenuHid()) {
            self.isHide = YES;
        } else {
            [self setupHideViewTimer];
        }
    }
}

#pragma mark - Button Action

- (void)initState {
    if (self.state == PlayerMaskViewStatusNetworkInterrupt) {
        return;
    }
    self.state = PlayerMaskViewStatusNone;
    self.smallBottomPlayButton.selected = self.smallPlayButton.selected = self.largePlayButton.selected = NO;
    [self.smallPlayProgress setValue:0];
    [self.smallBufferProgress setValue:0];
    [self.smallBackPlaySlider setValue:0];
    [self.smallBackBufferProgress setValue:0];
    [self.largePlayProgress setValue:0];
    [self.largeBufferProgress setValue:0];
    self.smallStartTimeLabel.text = self.largeStartTimeLabel.text = @"00:00";
    self.smallEndTimeLabel.text = self.largeEndTimeLabel.text = @"00:00";
}

- (IBAction)transparentButtonAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(startPlayVideo)]) {
        [self.delegate startPlayVideo];
    }
}

#pragma mark - setter

- (void)setAlpha:(CGFloat)alpha {
    [super setAlpha:alpha];
    
    self.transparentButton.alpha = alpha < 1 ? 1 : 0;
}

- (void)setIsHide:(BOOL)isHide {
    _isHide = isHide;
    [self hideViewUIWithisHide:isHide];
}

- (void)setVideo:(VIDEO *)video {
    [self.videoAvatar setImageWithPhoto:video.photo];
    [self.smallVideoAvatar setImageWithPhoto:video.photo];
    [self.largeVideoAvatar setImageWithPhoto:video.photo];
    
    self.largeTitleLabel.text = video.title;
    self.smallPlayTime.text = video.video_time;
    self.smallDataUseLabel.text = nil;
//    优化：设置按钮选中的话，会有高亮的状态
    if (video.is_collect) {
        [self.largeCollectButton setImage:[UIImage imageNamed:@"icon_collect_sel"] forState:UIControlStateNormal];
    } else {
        [self.largeCollectButton setImage:[UIImage imageNamed:@"icon_collect_nor"] forState:UIControlStateNormal];
    }
    if (video.is_star) {
        [self.largeStarButton setImage:[UIImage imageNamed:@"icon_video_grade_sel"] forState:UIControlStateNormal];
    } else {
        [self.largeStarButton setImage:[UIImage imageNamed:@"icon_video_grade"] forState:UIControlStateNormal];
    }
    
    if (video.isCache) {
        if (![_video.id isEqualToString:video.id]) {
            [self initState];
            [self.txVodPlayer stopPlay];
            
            AFNetworkReachabilityStatus status = [AFNetworkReachabilityManager sharedManager].networkReachabilityStatus;
            if (status == AFNetworkReachabilityStatusNotReachable) {
                self.playerNetworkMaskView.state = status;
                self.state = PlayerMaskViewStatusNetworkInterrupt;
                self.playerLoadingView.hidden = YES;
                self.isHide = YES;
            } else if ([SettingModel sharedInstance].autoPlay == NO) {
                if (_video == nil) {
                    self.state = PlayerMaskViewStatusWait;
                    self.playerLoadingView.hidden = YES;
                    self.isHide = NO;
                }
            } else {
                self.state = PlayerMaskViewStatusLoadStalled;
                self.playerLoadingView.hidden = NO;
                self.isHide = YES;
            }
        }
        return;
    }
    
    [self.supportQualities removeAllObjects];
    if (video.video_url && [self checkPlayUrl:video.video_url]) {
        [self.supportQualities addObject:@(VIDEO_QUALITY_NORMAL)];
    }
    if (video.video_url_sd && [self checkPlayUrl:video.video_url_sd]) {
        [self.supportQualities addObject:@(VIDEO_QUALITY_SD)];
    }
    if (video.video_url_hd && [self checkPlayUrl:video.video_url_hd]) {
        [self.supportQualities addObject:@(VIDEO_QUALITY_HD)];
    }
    
    if (![self checkPlayUrl:video.video_url]) {
        [[MPRootViewController sharedInstance].currentViewController presentMessageTips:@"播放地址不合法，点播目前仅支持flv,hls,mp4播放方式!"];
        self.playerLoadingView.hidden = YES;

        [self.txVodPlayer stopPlay];
        self.videoUrl = nil;
        return;
    }
    
    // 不要动这里的逻辑，除了@liuyadi...
    // 在setVideo前判断两个id是否相同，不相同的情况才去重新播放短片，相同的情况只是刷新数据就可以了
    if ([_video.id isEqualToString:video.id]) {
        _video = video;
    } else {
        [self.stateManager setWatchTime:@"0" forIdentifier:_video.id];
        
        if (self.playerNetworkMaskView.state == AFNetworkReachabilityStatusReachableViaWWAN) {
            self.smallDataUseLabel.text = [NSString stringWithFormat:@"正在使用非WiFi网络，播放将消耗%@流量", video.dataflow];
        }
        
        _video = video;
        
        [self seprateTranslateContent:video.srt];
        NSLog(@"\n\nvideo.srt:%@\n\n\\n",video.srt);

        // 当前播放的URL与当前传入的VIDEO对象不同，不同的短片不考虑URL相同的情况
        if (self.videoUrl == nil) {
            self.videoUrl = [self videoQualityUrl:video];

            if ([SettingModel sharedInstance].autoPlay == NO) {
                self.txVodPlayer.isAutoPlay = NO;
                self.smallBottomPlayButton.selected = self.smallPlayButton.selected = self.largePlayButton.selected = NO;
            } else {
                self.txVodPlayer.isAutoPlay = YES;
                self.smallBottomPlayButton.selected = self.smallPlayButton.selected = self.largePlayButton.selected = YES;
                
                if (self.playerNetworkMaskView.state == AFNetworkReachabilityStatusReachableViaWWAN) {
                    self.smallDataUseLabel.hidden = YES;
                }
            }
            [self.txVodPlayer startPlay:self.videoUrl];
            [AppAnalytics clickEvent:@"click_video_play"];
            [UserPointModel userPointName:MODULE_NAME_CLICK_PLAY_FINISH];
            
            VideoState *state = [self.stateManager stateForIdentifier:video.id];
            if (state.watchTime) {
                CGFloat watchTime = state.watchTime.floatValue;
                self.sliderValue = watchTime;
                [self.smallPlayProgress setValue:self.sliderValue animated:YES];
                [self.smallBackPlaySlider setValue:self.sliderValue animated:YES];
                [self.largePlayProgress setValue:self.sliderValue animated:YES];
                [self.txVodPlayer seek:self.sliderValue];
            }
            
            if (self.state == PlayerMaskViewStatusNetworkInterrupt) {
                [self.txVodPlayer stopPlay];
            } else {
                if (self.txVodPlayer.isAutoPlay) {
                    self.state = PlayerMaskViewStatusLoadStalled;
                } else {
                    self.state = PlayerMaskViewStatusWait;
                    self.isHide = NO;
                }
            }
            
        } else {
            [self initState];
            self.videoUrl = [self videoQualityUrl:video];

            [self.txVodPlayer stopPlay];
            [self.txVodPlayer startPlay:self.videoUrl];
            [AppAnalytics clickEvent:@"click_video_play"];
            [UserPointModel userPointName:MODULE_NAME_CLICK_PLAY_FINISH];

            self.isHide = NO;
            
            if (self.state == PlayerMaskViewStatusNetworkInterrupt) {
                [self.txVodPlayer stopPlay];
            } else {
                if (self.txVodPlayer.isAutoPlay) {
                    self.state = PlayerMaskViewStatusLoadStalled;
                    self.smallBottomPlayButton.selected = self.smallPlayButton.selected = self.largePlayButton.selected = YES;
                } else {
                    self.state = PlayerMaskViewStatusWait;
                    self.isHide = NO;
                    self.smallBottomPlayButton.selected = self.smallPlayButton.selected = self.largePlayButton.selected = NO;
                }
            }
        }
    }
}

- (void)setCurrentTimer:(CGFloat)currentTimer {
    _currentTimer = currentTimer;
    self.smallStartTimeLabel.text = self.largeStartTimeLabel.text = [Utilities timeToHumanString:currentTimer];
    self.smallPlayProgress.value = self.smallBackPlaySlider.value = self.largePlayProgress.value = currentTimer;
    self.scheduleTipView.scheduleProgress.progress = currentTimer / self.smallPlayProgress.maximumValue;
}

- (void)setState:(PlayerMaskViewStatus)state {
    if (_state == state) {
        return;
    }
    
    if (_state == PlayerMaskViewStatusWait) {
        if (state == PlayerMaskViewStatusEndWait) {
            self.videoAvatar.hidden = self.smallVideoAvatar.hidden = self.largeVideoAvatar.hidden = YES;
            _state = state;
        }
    } else {
        _state = state;
    }
    
    if (_state == PlayerMaskViewStatusLoadStalled) {
        self.isHide = YES;
    }
}

- (NSString *)videoQualityUrl:(VIDEO *)video {
    NSString *videoUrl;
    VIDEO_QUALITY defaultQuality = [SettingModel sharedInstance].defaultQuality;
    if (defaultQuality == VIDEO_QUALITY_HD) {
        videoUrl = video.video_url_hd;
        [self.largeQualityButton setTitle:@"超清" forState:UIControlStateNormal];
    } else if (defaultQuality == VIDEO_QUALITY_SD) {
        videoUrl = video.video_url_sd;
        [self.largeQualityButton setTitle:@"高清" forState:UIControlStateNormal];
    } else if (defaultQuality == VIDEO_QUALITY_NORMAL) {
        videoUrl = video.video_url;
        [self.largeQualityButton setTitle:@"标清" forState:UIControlStateNormal];
    }
    
    // 确认某一清晰度存在
    if (videoUrl == nil) {
        if (video.video_url_hd) {
            videoUrl = video.video_url_hd;
            [self.largeQualityButton setTitle:@"超清" forState:UIControlStateNormal];
        } else if (video.video_url_sd) {
            videoUrl = video.video_url_sd;
            [self.largeQualityButton setTitle:@"高清" forState:UIControlStateNormal];
        } else if (video.video_url) {
            videoUrl = video.video_url;
            [self.largeQualityButton setTitle:@"标清" forState:UIControlStateNormal];
        }
    }
    
    [self.listView reloadData];
    return videoUrl;
}

#pragma mark - getter

- (BOOL)isPlaying {
    return self.txVodPlayer.isPlaying;
}

- (BOOL)isWaiting {
    return self.state == PlayerMaskViewStatusWait;
}

- (MPVolumeView *)mpVolumeView {
    if (!_mpVolumeView) {
        _mpVolumeView = [[MPVolumeView alloc] init];
        for (UIView *view in [_mpVolumeView subviews]) {
            if ([view.class.description isEqualToString:@"MPVolumeSlider"]) {
                _mpVolumeSlider = (UISlider *)view;
                break;
            }
        }
        [_mpVolumeView setFrame:CGRectMake(-100, -100, 40, 40)];
        [_mpVolumeView setShowsVolumeSlider:YES];
        [_mpVolumeView sizeToFit];
    }
    return _mpVolumeView;
}

// 会有前一个页面传过来的情况
- (VideoStateManager *)stateManager {
    if (!_stateManager) {
        _stateManager = [[VideoStateManager alloc] init];
    }
    return _stateManager;
}

#pragma mark - gensture Action

- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (!self.controlPanelEnabled)
        return;
    
    if (self.isHengMoved || self.isShuMoved) {
        return;
    }
    
    UITouch * touch = [touches anyObject];
    self.beginPosition  = [touch locationInView:self];
    
    self.isSlide = NO;
}

- (void)touchesEnded:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (!self.controlPanelEnabled)
        return;
    
    if (self.isSlide) {
        // 如果当前是滑动音量和改变亮度，那么就不需要
        if (self.isHengMoved) {
            // 横向移动结束后 隐藏
            self.scheduleTipView.hidden = YES;
            [self.txVodPlayer seek:self.sliderValue];
        } else {
            [self.brightnessTipView hide];
        }
    } else {
        if (!self.playerNetworkMaskView.hidden) {
            CGPoint point = [[touches anyObject] locationInView:self.playerNetworkMaskView];
            point = [self.playerNetworkMaskView.maskButton.layer convertPoint:point fromLayer:self.playerNetworkMaskView.layer];
            if ([self.playerNetworkMaskView.maskButton.layer containsPoint:point]) {
                if (self.delegate && [self.delegate respondsToSelector:@selector(refreshVideoManager)]) {
                    [self.delegate refreshVideoManager];
                }
                [self syncNetworkStatus];
                self.state = PlayerMaskViewStatusLoadStalled;
            } else {
                if (!self.listView.hidden) {
                    self.listView.hidden = YES;
                } else {
                    self.isHide = !self.isHide;
                }
            }
        } else {
            if (!self.listView.hidden) {
                self.listView.hidden = YES;
            } else {
                self.isHide = !self.isHide;
            }
        }
    }
    
    self.isHengMoved = NO;
    self.isShuMoved = NO;
}

- (void)touchesMoved:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    if (!self.controlPanelEnabled)
        return;
    
    UITouch *touch = [touches anyObject];
    CGPoint currentPosition = [touch locationInView:self];
    CGPoint previousPosition = [touch previousLocationInView:self];
    
    CGFloat width = 0;
    CGFloat height = 0;
    
    if (self.isFullScreen) {
        width = [MPRootViewController sharedInstance].view.height;
        height = [MPRootViewController sharedInstance].view.width;
    } else {
        width = [MPRootViewController sharedInstance].view.width;
        height = [MPRootViewController sharedInstance].view.height;
    }
    
    if (self.isHengMoved && self.isShuMoved) {
        return;
    }
    
    // 横扫
    if (currentPosition.x - previousPosition.x >= 1 && fabs(currentPosition.x - previousPosition.x) > fabs(currentPosition.y - previousPosition.y)) {
        if (self.isShuMoved)
            return;
        
        self.isHengMoved = YES;
        
        // 快进
        if (self.currentTimer > self.smallPlayProgress.maximumValue - 1) {
            self.currentTimer = self.smallPlayProgress.maximumValue;
        } else {
            self.currentTimer += 1;
        }
        
        self.sliderValue = self.currentTimer;
        // 显示scheduleTipView
        self.scheduleTipView.hidden = NO;
        self.isHide = YES;
        
        [self.scheduleTipView reloadStatusWithTime:[NSString stringWithFormat:@"%@/%@", self.largeStartTimeLabel.text, self.largeEndTimeLabel.text] isRetreat:NO];
        
        self.isSlide = YES;
    } else if (previousPosition.x - currentPosition.x >= 1 && fabs(currentPosition.x - previousPosition.x) > fabs(currentPosition.y - previousPosition.y)) {
        if (self.isShuMoved)
            return;
        
        self.isHengMoved = YES;
        
        self.isSlide = YES;
        
        // 后退
        if (self.currentTimer < 1) {
            self.currentTimer = 0;
        } else {
            self.currentTimer -= 1;
        }
        
        self.sliderValue = self.currentTimer;
        self.scheduleTipView.hidden = NO;
        self.isHide = YES;
        [self.scheduleTipView reloadStatusWithTime:[NSString stringWithFormat:@"%@/%@", self.largeStartTimeLabel.text, self.largeEndTimeLabel.text] isRetreat:YES];
    }
    
    // 上下滑动
    if (fabs(currentPosition.x - previousPosition.x) < fabs(currentPosition.y - previousPosition.y)) {
        if (self.isHengMoved)
            return;
        
        width = self.isFullScreen ? MAX(width, height) : [MPRootViewController sharedInstance].view.width;
        
        self.isShuMoved = YES;
        if ( self.beginPosition.x > (width * 0.6) && currentPosition.x > (width * 0.6)) { // 调节音量
            [self adjustVolumeWithCurrentPoint:currentPosition previousPosition:previousPosition];
        } else if ( self.beginPosition.x < (width * 0.4) && currentPosition.x < (width * 0.4)) { // 调整屏幕亮度
            [self adjustBrightnessWithCurrentPoint:currentPosition previousPosition:previousPosition];
        }
        self.isSlide = YES;
    }
}

// 调节音量
- (void)adjustVolumeWithCurrentPoint:(CGPoint)currentPosition previousPosition:(CGPoint)previousPosition {
    CGFloat nowVolumeValue = self.mpVolumeSlider.value;
    float changeValue = 0;
    
//    if (currentPosition.y < previousPosition.y) {
//        // 向上滑动
//        changeValue = (nowVolumeValue + 0.01);
//    } else {
//        // 向下滑动
//        changeValue = (nowVolumeValue - 0.01);
//    }
    
    // 最大调节范围占屏幕70%，上下各预留15%
    CGFloat minY = self.height * 0.15;
    CGFloat maxY = self.height * 0.85;
    
    if (previousPosition.y >= minY && previousPosition.y <= maxY) {
        // 在滑动区间内的滑动才是有效滑动
        BOOL isUp = currentPosition.y < previousPosition.y;
        CGFloat distance = currentPosition.y - previousPosition.y;
        CGFloat maxDistance = fabs(maxY - minY);
        
        distance = (distance < 0.0) ? ceil(-distance) : ceil(distance);
        
        CGFloat scale = distance / maxDistance;
        scale = (isUp) ? scale : -scale;
        
        CGFloat newVolumn = nowVolumeValue + scale;
        if (newVolumn > 1.0) {
            newVolumn = 1.0;
        }
        
        [self.mpVolumeView setHidden:YES];
        [self.mpVolumeSlider setValue:newVolumn animated:YES];
        [self.mpVolumeSlider sendActionsForControlEvents:UIControlEventAllEvents];
    }
}

// 调节亮度
- (void)adjustBrightnessWithCurrentPoint:(CGPoint)currentPosition previousPosition:(CGPoint)previousPosition {
    CGFloat brightness = [UIScreen mainScreen].brightness;
    
    self.brightnessTipView.progress = brightness;
    
    [self.brightnessTipView show];
    
//    if (currentPosition.y < previousPosition.y) {
//        // 向上滑动
//        brightness += 0.01;
//        self.brightnessTipView.progress = brightness;
//    } else {
//        // 向下滑动
//        brightness -= 0.01;
//        self.brightnessTipView.progress = brightness;
//    }
    
    // 最大调节范围占屏幕70%，上下各预留15%
    CGFloat minY = self.height * 0.15;
    CGFloat maxY = self.height * 0.85;
    
    if (previousPosition.y >= minY && previousPosition.y <= maxY) {
        // 在滑动区间内的滑动才是有效滑动
        BOOL isUp = currentPosition.y < previousPosition.y;
        CGFloat distance = currentPosition.y - previousPosition.y;
        CGFloat maxDistance = fabs(maxY - minY);
        
        distance = (distance < 0.0) ? ceil(-distance) : ceil(distance);
        
        CGFloat scale = distance / maxDistance;
        scale = (isUp) ? scale : -scale;
        CGFloat newBrightness = brightness + scale;
        
        if (newBrightness > 1.0) {
            newBrightness = 1.0;
        }
        
        //调整系统屏幕亮度
        self.brightnessTipView.progress = newBrightness;
        [[UIScreen mainScreen] setBrightness:newBrightness];
    }
}

- (void)handleDoubleTap {
    if (!self.isFullScreen) {
        self.isHide = NO;
        [self smallBottomPlayButtonAction:self.smallBottomPlayButton];
    } else {
        [self largePlayButtonAction:self.largePlayButton];
    }
}

- (void)syncNetworkStatus {
    self.playerNetworkMaskView.state = [AFNetworkReachabilityManager sharedManager].networkReachabilityStatus;
}

#pragma mark - UISlider

- (void)onSeek:(UISlider *)slider {
    [self.txVodPlayer seek:self.sliderValue];
    self.trackingTouchTS = [[NSDate date]timeIntervalSince1970] * 1000;
    self.startSeek = NO;
}

- (void)onSeekBegin:(UISlider *)slider {
    self.startSeek = YES;
}

- (void)onDrag:(UISlider *)slider {
    float progress = slider.value;
    int intProgress = progress + 0.5;
    self.smallStartTimeLabel.text = self.largeStartTimeLabel.text = [NSString stringWithFormat:@"%02d:%02d",(int)(intProgress / 60), (int)(intProgress % 60)];
    self.sliderValue = slider.value;
}

#pragma mark - Translate Actions

- (void)seprateTranslateContent:(NSString *)urlPath {
    self.translateLabel.text = nil;
    if ([urlPath hasPrefix:@"https:"] || [urlPath hasPrefix:@"http:"]) {
        @weakify(self)
        [self.downloader startDownload:urlPath progressHandler:^(float progress) {
        } then:^(NSString *filePath, NSError *error) {
            @strongify(self)
            if (error == nil && filePath) {
                NSError *e;
                NSString *content = [NSString stringWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:&e];
                if (e) {
                    self.beginTimeArray = [NSMutableArray array];
                    self.endTimeArray = [NSMutableArray array];
                    self.subtitlesArray = [NSMutableArray array];
                    return;
                }

                //按行分割存放到数组中
                NSArray *singleArray = [content componentsSeparatedByString:@"\n"];
                NSMutableArray *subtitlesArray = [NSMutableArray array];
                NSMutableArray *beginTimeArray = [NSMutableArray array];
                NSMutableArray *endTimeArray = [NSMutableArray array];

                NSString *subStr = @"";
                for (int i = 0; i < singleArray.count; i++) {
                    if ((i % 5) == 0) {

                    } else if ((i % 5) == 1) {
                        //时间
                        NSString *timeStr = [singleArray objectAtIndex:i];
                        NSRange range2 = [timeStr rangeOfString:@" --> "];
                        if (range2.location != NSNotFound) {
                            NSString *beginstr = [timeStr substringToIndex:range2.location];
                            NSString *endstr = [timeStr substringFromIndex:range2.location+range2.length];

                            NSArray *arr = [beginstr componentsSeparatedByString:@":"];
                            NSArray *arr1 = [arr[2] componentsSeparatedByString:@","];

                            //将开始时间数组中的时间换化成秒为单位的
                            float teim = [arr[0] floatValue] * 60 * 60 + [arr[1] floatValue] * 60 + [arr1[0] floatValue] + [arr1[1] floatValue] / 1000;
                            //将float类型转化成NSNumber类型才能存入数组
                            NSNumber *beginnum = [NSNumber numberWithFloat:teim];
                            [beginTimeArray addObject:beginnum];

                            NSArray *array = [endstr componentsSeparatedByString:@":"];
                            NSArray *arr2 = [array[2] componentsSeparatedByString:@","];
                            //将结束时间数组中的时间换化成秒为单位的
                            float fl = [array[0] floatValue] * 60 * 60 + [array[1] floatValue] * 60 + [arr2[0] floatValue] + [arr2[1] floatValue] / 1000;
                            NSNumber *endnum = [NSNumber numberWithFloat:fl];
                            [endTimeArray addObject:endnum];
                        }
                    } else {
                        if ((i % 5) == 2) {
                            //中文字幕
                            subStr = [NSString stringWithFormat:@"%@", [singleArray objectAtIndex:i]];
                            subStr = [subStr stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                            subStr = [subStr stringByReplacingOccurrencesOfString:@"\r" withString:@""];
                        } else if ((i % 5) == 3) {
                            //英文原文
                            NSString *title = [singleArray objectAtIndex:i];
                            title = [title stringByReplacingOccurrencesOfString:@"\n" withString:@""];
                            title = [title stringByReplacingOccurrencesOfString:@"\r" withString:@""];
                            if (title && title.length) {
                                subStr = [subStr stringByAppendingFormat:@"\n%@", title];
                            }
                            [subtitlesArray addObject:subStr];
                            subStr = @"";
                        }
                    }
                }

                self.beginTimeArray = beginTimeArray;
                self.endTimeArray = endTimeArray;
                self.subtitlesArray = subtitlesArray;
            }
        }];
    } else {
        // 切换推荐短片没有对应字幕清空数据防止复用上一视频字幕
        [self.beginTimeArray removeAllObjects];
        [self.endTimeArray removeAllObjects];
        [self.subtitlesArray removeAllObjects];
    }
}

- (void)translateSubtitleText:(CGFloat)second {
    if (self.subtitlesArray == nil || self.subtitlesArray.count == 0) {
        return;
    }
    if (second < [self.beginTimeArray.firstObject floatValue] || second > [self.endTimeArray.lastObject floatValue]) {
        self.translateLabel.text = nil;
        return;
    }

    for (int i = 0; i < self.beginTimeArray.count ; i++) {
        CGFloat beginarr =  [self.beginTimeArray[i] floatValue];
        CGFloat endarr = [self.endTimeArray[i] floatValue];

        if (second > beginarr && second < endarr) {
            //同步字幕
            self.translateLabel.text = self.subtitlesArray[i];
            break;
        } else {
            self.translateLabel.text = nil;
        }
    }
}

@end
