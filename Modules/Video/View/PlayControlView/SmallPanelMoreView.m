//
//  SmallPanelMoreView.m
//  MotherPlanet
//
//  Created by liuyadi on 2018/4/13.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "SmallPanelMoreView.h"
#import "SmallPanelMoreViewCell.h"

@interface SmallPanelMoreView () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewTC;

@end

@implementation SmallPanelMoreView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.tableView.rowHeight = 42;
    [self.tableView registerNib:[SmallPanelMoreViewCell nib] forCellReuseIdentifier:@"SmallPanelMoreViewCell"];
    [self.tableView reloadData];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    if ([SDiOSVersion deviceSize] == Screen5Dot8inch) {
        self.tableViewTC.constant = 55;
    } else {
        self.tableViewTC.constant = 31;
    }
}

#pragma mark - UITableViewDelegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SmallPanelMoreViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SmallPanelMoreViewCell" forIndexPath:indexPath];
    cell.data = @(indexPath.row);
    return cell;
}

#pragma mark - UITableViewDataSource

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(shareVideo)]) {
            [self.delegate shareVideo];
        }
    } else if (indexPath.row == 1) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(addToLater)]) {
            [self.delegate addToLater];
        }
    } else if (indexPath.row == 2) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(reportVideo)]) {
            [self.delegate reportVideo];
        }
    }
    self.hidden = YES;
    [self removeFromSuperview];
}

- (IBAction)hideButtonAction:(id)sender {
    self.hidden = YES;
    [self removeFromSuperview];
}

@end
