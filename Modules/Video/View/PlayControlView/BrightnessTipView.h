//  BrightnessTipView.h
//  leciyuan
//
//  Created by Chenyun on 16/1/14.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BrightnessTipView : UIView

@property (nonatomic, assign) CGFloat progress;

+ (instancetype)sharedView;

- (void)show;
- (void)hide;

@end
