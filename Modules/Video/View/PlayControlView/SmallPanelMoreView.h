//
//  SmallPanelMoreView.h
//  MotherPlanet
//
//  Created by liuyadi on 2018/4/13.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SmallPanelMoreViewDelegate <NSObject>

- (void)shareVideo;
- (void)addToLater;
- (void)reportVideo;

@end

@interface SmallPanelMoreView : UIView

@property (nonatomic, weak) id<SmallPanelMoreViewDelegate> delegate;

@end
