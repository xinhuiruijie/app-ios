//  ScheduleTipView.h
//  leciyuan
//
//  Created by Chenyun on 16/1/14.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScheduleTipView : UIView
@property (weak, nonatomic) IBOutlet UIProgressView *scheduleProgress;
- (void)reloadStatusWithTime:(NSString *)timeStr isRetreat:(BOOL)isRetreat;
@end
