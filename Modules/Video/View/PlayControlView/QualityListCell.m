//  QualityListCell.m
//  leciyuan
//
//  Created by Chenyun on 16/1/11.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import "QualityListCell.h"

@implementation VideoQualityInfo

@end

@interface QualityListCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *selectIcon;

@end

@implementation QualityListCell

- (void)awakeFromNib
{
	[super awakeFromNib];
}

- (void)dataDidChange
{
	if ( [self.data isKindOfClass:[VideoQualityInfo class]] )
	{
		VideoQualityInfo * info = self.data;
        
        if ([info.quality isKindOfClass:[NSNumber class]]) {
            if ([info.quality isEqualToNumber:@(VIDEO_QUALITY_NORMAL)]) {
                self.titleLabel.text = @"标清(480P)";
            } else if ([info.quality isEqualToNumber:@(VIDEO_QUALITY_SD)]) {
                self.titleLabel.text = @"高清(720P)";
            } else if ([info.quality isEqualToNumber:@(VIDEO_QUALITY_HD)]) {
                self.titleLabel.text = @"超清(1080P)";
            }
            if (info.isSelected) {
                self.titleLabel.textColor = [AppTheme qualitySelectedColor];
                self.selectIcon.hidden = NO;
            } else {
                self.titleLabel.textColor = [AppTheme qualityNormalColor];
                self.selectIcon.hidden = YES;
            }
        } else if ([info.quality isKindOfClass:[NSString class]]) {
            self.titleLabel.text = info.quality;
        }
		
    } else if ([self.data isKindOfClass:[NSString class]]) {
        self.titleLabel.text = self.data;
    }
        
}

@end
