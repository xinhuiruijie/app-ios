//
//  SmallPanelMoreViewCell.m
//  MotherPlanet
//
//  Created by liuyadi on 2018/4/13.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "SmallPanelMoreViewCell.h"

@interface SmallPanelMoreViewCell ()

@property (weak, nonatomic) IBOutlet UIImageView *typeIcon;
@property (weak, nonatomic) IBOutlet UILabel *typeLabel;

@end

@implementation SmallPanelMoreViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)dataDidChange {
    NSNumber *type = self.data;
    
    if ([type isEqualToNumber:@(PANEL_MORE_STYLE_SHARE)]) {
        self.typeIcon.image = [UIImage imageNamed:@"icon_more_share"];
        self.typeLabel.text = @"分享短片";
    } else if ([type isEqualToNumber:@(PANEL_MORE_STYLE_LATER)]) {
        self.typeIcon.image = [UIImage imageNamed:@"icon_more_nextplay"];
        self.typeLabel.text = @"稍后观看";
    } else if ([type isEqualToNumber:@(PANEL_MORE_STYLE_REPORT)]) {
        self.typeIcon.image = [UIImage imageNamed:@"icon_more_report"];
        self.typeLabel.text = @"内容举报";
    }
}

@end
