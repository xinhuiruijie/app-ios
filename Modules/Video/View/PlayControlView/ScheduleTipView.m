//  ScheduleTipView.m
//  leciyuan
//
//  Created by Chenyun on 16/1/14.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import "ScheduleTipView.h"

@interface ScheduleTipView ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@end

@implementation ScheduleTipView

- (void)awakeFromNib
{
	[super awakeFromNib];
}

- (void)reloadStatusWithTime:(NSString *)timeStr isRetreat:(BOOL)isRetreat
{
	if ( isRetreat )
	{
		// 后退
		self.imageView.image = [UIImage imageNamed:@"icon_play_fr"];
	}
	else
	{
		// 快进
		self.imageView.image = [UIImage imageNamed:@"icon_play_ff"];
	}

	self.timeLabel.text = timeStr;
}

@end
