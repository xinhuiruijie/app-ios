//
//  PlayerLoadingView.m
//  MotherPlanet
//
//  Created by liuyadi on 2018/4/10.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "PlayerLoadingView.h"

@interface PlayerLoadingView ()

@property (weak, nonatomic) IBOutlet UIView *bgView;
@property (weak, nonatomic) IBOutlet UIImageView *loadingImage;
@property (weak, nonatomic) IBOutlet UILabel *loadingImageLabel;

@end

@implementation PlayerLoadingView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    // 设置imageview
    self.loadingImage.animationImages = @[[UIImage imageNamed:@"icon_cut1"], [UIImage imageNamed:@"icon_cut2"], [UIImage imageNamed:@"icon_cut3"]];
    self.loadingImage.animationDuration = 1.0;
    self.loadingImage.animationRepeatCount = 0;
    
    [self.loadingImage startAnimating];
}

- (void)layoutSubviews {
    [super layoutSubviews];
}

- (void)setHidden:(BOOL)hidden {
    [super setHidden:hidden];
    
    if (hidden) {
        [self.loadingImage stopAnimating];
    } else {
        [self.loadingImage startAnimating];
    }
}

@end
