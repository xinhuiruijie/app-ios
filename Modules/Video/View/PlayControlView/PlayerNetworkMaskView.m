//  PlayerNetworkMaskView.m
//  leciyuan
//
//  Created by Chenyun on 16/1/28.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import "PlayerNetworkMaskView.h"
#import "NetworkReachabilityManager.h"

@interface PlayerNetworkMaskView ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIButton *subButton;

@end

@implementation PlayerNetworkMaskView

- (void)awakeFromNib {
	[super awakeFromNib];
    
    self.maskButton.layer.cornerRadius = 15;
    self.maskButton.layer.masksToBounds = YES;
}

- (void)setState:(AFNetworkReachabilityStatus)state {
	_state = state;
	
	if (state == AFNetworkReachabilityStatusUnknown || state == AFNetworkReachabilityStatusNotReachable) {
		[self notReachable];
    } else {
        self.hidden = YES;
    }
}

- (void)notReachable {
	self.titleLabel.text = @"网络状态不给力，请检查网络设置";
	[self.maskButton setTitle:@"重新加载" forState:UIControlStateNormal];
	
	self.hidden = NO;
	
	if (self.whenStopPlay) {
		self.whenStopPlay();
	}
}

- (IBAction)reloadDataAction:(id)sender {
    self.hidden = YES;
    
    if (self.whenRefresh) {
        self.whenRefresh();
    }
}

- (IBAction)backButtonAction:(id)sender {
    if (self.whenBackClicked) {
        self.whenBackClicked();
    }
}

@end
