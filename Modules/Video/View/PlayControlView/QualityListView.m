//  QualityListView.m
//  leciyuan
//
//  Created by Chenyun on 16/1/8.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import "QualityListView.h"
#import "QualityListCell.h"

@interface QualityListView () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (nonatomic, strong) UICollectionView * list;
@property (nonatomic, strong) UIView *backView;

@property (nonatomic, strong) NSMutableArray * datas;

@property (nonatomic, assign) NSInteger currentSelectedIndex;

@property (nonatomic, assign) BOOL isUnDefault;

@end

@implementation QualityListView

- (void)awakeFromNib
{
	[super awakeFromNib];

	self.backgroundColor = [UIColor clearColor];
    
    self.backView = [[UIView alloc] init];
    self.backView.backgroundColor = [UIColor colorWithRGBValue:0x272B31];
    self.backView.alpha = 0.8;
    [self addSubview:self.backView];

	// 默认选中的为1
	self.currentSelectedIndex = 1;
}

- (void)reloadViewWithDatas:(NSArray *)datas
{
	[self.datas removeAllObjects];

	if ( !datas.count )
	{
		return;
	}
	else
	{
		self.datas = [NSMutableArray arrayWithArray:datas];

		if ( !self.list )
		{
			UICollectionViewFlowLayout * collectLayout = [[UICollectionViewFlowLayout alloc] init];
			collectLayout.minimumInteritemSpacing = 0.0f;
			collectLayout.minimumLineSpacing = 0.0f;

			self.list = [[UICollectionView alloc] initWithFrame:CGRectZero collectionViewLayout:collectLayout];

			// 注册
			[self.list registerNib:[UINib nibWithNibName:@"QualityListCell" bundle:nil] forCellWithReuseIdentifier:@"QualityListCell"];

			self.list.backgroundColor = [UIColor clearColor];
			self.list.delegate = self;
			self.list.dataSource = self;

			// 是否显示水平方向的滚动条
			self.list.showsHorizontalScrollIndicator = NO;

			// 是否显示垂直方向的滚动条
			self.list.showsVerticalScrollIndicator = NO;

			[self addSubview:self.list];
		}
	}
}

#pragma mark -
- (void)dataDidChange
{
	if ( [self.data isKindOfClass:[NSArray class]] || [self.data isKindOfClass:[NSMutableArray class]] )
	{
		NSArray * datas = self.data;

		if ( !self.isUnDefault )
		{
			for ( int i = 0 ; i<datas.count; i++ )
			{
				id index = datas[i];
                if ([index isKindOfClass:[NSNumber class]]) {
                    VIDEO_QUALITY defaultQuality = [SettingModel sharedInstance].defaultQuality;
                    if ( [index isEqualToNumber:@(defaultQuality)] )
                    {
                        self.isUnDefault = YES;
                        self.currentSelectedIndex = i;
                    }
                }
			}
		}

		[self reloadViewWithDatas:datas];
		[self.list reloadData];
	}
}

- (void)reloadData {
    self.isUnDefault = NO;
}

#pragma mark - delegate

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.datas.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	static NSString * cellStr = @"QualityListCell";

	UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellStr forIndexPath:indexPath];

	VideoQualityInfo * info = [[VideoQualityInfo alloc] init];

	info.quality = self.datas[indexPath.row];

	if ( indexPath.row == self.currentSelectedIndex )
	{
		info.isSelected = YES;
	}
	else
	{
		info.isSelected = NO;
	}

	cell.data = info;
	return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.currentSelectedIndex == indexPath.row) {
        return;
    }
    
	// 设置当前选中的下标
	self.currentSelectedIndex = indexPath.row;

	// 刷新页面
	[self.list reloadData];

	// 发送消息
    if ( self.whenSelected ) {
        self.whenSelected(self.datas[indexPath.row]);
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
	return CGSizeMake(collectionView.width, 60);
}

- (void)layoutSubviews
{
	[super layoutSubviews];

	self.list.frame = CGRectMake(self.width - 170, (self.height - 180) / 2, 170, 180);
    self.backView.frame = CGRectMake(self.width - 170, 0, 170, self.height);
}

- (IBAction)hideViewAction:(id)sender {
    if (self.whenCanceled) {
        self.whenCanceled();
    }
}

@end
