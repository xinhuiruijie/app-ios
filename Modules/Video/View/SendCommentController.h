//
//  SendCommentController.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/8.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SendCommentControllerDelegate <NSObject>
- (void)onClose;
- (void)onSendCommentSuccess;
@end

@interface SendCommentController : UIViewController

@property (nonatomic, weak) id<SendCommentControllerDelegate> delegate;
@property (nonatomic, strong) NSString *video_id;

@property (nonatomic, strong) id additional;

@end
