//
//  PresentAnimator.m
//  MotherPlanet
//
//  Created by GeekZooStudio on 2017/12/18.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "PresentAnimator.h"

@implementation PresentAnimator

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    return 0.25;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    
    UIView *containView = [transitionContext containerView];
    UIView *toView = [transitionContext viewForKey:UITransitionContextToViewKey];
    
    CGRect finalFrame = toView.frame;
    finalFrame.origin.y = finalFrame.size.height;
    toView.frame = finalFrame;
    
//    toView.transform = CGAffineTransformMakeTranslation(0, finalFrame.size.height);
    
    [containView addSubview:toView];
    
    [UIView animateWithDuration:0.25 animations:^{
//        toView.transform = CGAffineTransformMakeTranslation(0, 200);
        toView.frame = self.finalFrame;
    } completion:^(BOOL finished) {
        [transitionContext completeTransition:YES];
    }];
}

@end
