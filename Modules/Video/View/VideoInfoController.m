//
//  VideoInfoController.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/6.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "VideoInfoController.h"
#import "AuthorInfoController.h"
#import "WormholeCreateController.h"

#import "VideoModel.h"
#import "VideoInfoModel.h"
#import "OfflineWatchListModel.h"
#import "VideoCommentModel.h"
#import "VideoCommentListModel.h"
#import "RecommendVideoListModel.h"
#import "AuthorModel.h"
#import "WatchLaterModel.h"
#import "AddWormholeVideoModel.h"
#import "UserPointModel.h"

#import "VIDEO+Extension.h"
#import "VideoInfoCell.h"
#import "VideoRecommendCell.h"
#import "VideoCommentHeader.h"
#import "VideoCommentCell.h"
#import "VideoInfoEndCell.h"
#import "VideoAnimatedButton.h"
#import "VideoStarAlertView.h"
#import "DarkShareView.h"
#import "FullScreenShareView.h"
#import "ZLPlaceholderTextView.h"
#import "VideoPlayManager.h"
#import "AddWormholeVideoView.h"
#import "SmallPanelMoreView.h"

#import "MPVideoGuideView.h"

NSString * const kVideoInfoRefreshNotification = @"kVideoInfoRefreshNotification";

@interface VideoInfoController () <UITableViewDelegate, UITableViewDataSource, VideoInfoCellDelegate, VideoPlayManagerDelegate, VideoRecommendCellDelegate, ZLPlaceholderTextViewDelegate, SmallPanelMoreViewDelegate, MPVideoGuideViewDelegate>

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *videoPlayTC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *navigationViewHC;

@property (weak, nonatomic) IBOutlet UIView *navigationView;
@property (weak, nonatomic) IBOutlet UIView *backImageBgView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *videoView;
@property (weak, nonatomic) IBOutlet UIImageView *videoBackImage;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomViewHC;
@property (weak, nonatomic) IBOutlet ZLPlaceholderTextView *commentInputView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *inputViewBC;

@property (nonatomic, strong) VideoInfoModel *videoInfoModel;
@property (nonatomic, strong) RecommendVideoListModel *recommendListModel;
@property (nonatomic, strong) OfflineWatchListModel *offlineWatchModel;
@property (nonatomic, strong) VideoCommentListModel *videoCommentListModel;

@property (nonatomic, strong) VideoAnimatedButton *likeAnimatedView;
@property (nonatomic, strong) VideoStarAlertView *actionAlert;
@property (nonatomic, strong) DarkShareView *smallShareView;
@property (nonatomic, strong) FullScreenShareView *largeShareView;
@property (nonatomic, strong) SmallPanelMoreView *smallPanelMoreView;

@property (nonatomic, strong) AddWormholeVideoView *addWormholeVideoView;

@property (nonatomic, strong) AlertView *reportCommentAlertView;
@property (nonatomic, strong) AlertView *reportCommentTypeAlertView;
@property (nonatomic, strong) MPAlertView *blackUserAlertView;
@property (nonatomic, strong) AlertView *reportTypeAlertView;

@property (nonatomic, strong) VideoPlayManager *playManager;

@property (nonatomic, assign) BOOL statusBarHidden;
@property (nonatomic, assign) BOOL hideAnimation;
@property (nonatomic, assign) int lastPosition;
@property (nonatomic, assign) CGFloat tableViewHeight;
@property (nonatomic, assign) BOOL playing;
@property (nonatomic, strong) id additional;
/// 进来后记录电台播放状态，退出时还原
@property (nonatomic, assign) BOOL audioPlaying;

@property (nonatomic, strong) NSDate *lastAnimationDate;

@end

@implementation VideoInfoController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Video"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // 从稍后观看列表中移除
    WatchLaterModel *model = [[WatchLaterModel alloc] init];
    [model removeVideo:self.video];
    
    self.audioPlaying = [AudioPlayManager sharedInstance].status == AudioPlayerStatusRestartToPlay;
    if (self.audioPlaying) {
        [[AudioPlayManager sharedInstance] pause];
    }
    if ([AudioPlayManager sharedInstance].status == AudioPlayerStatusBufferEmpty ||
        [AudioPlayManager sharedInstance].status == AudioPlayerStatusReadyToPlay) {
        [[AudioPlayManager sharedInstance] stop];// 如果是在缓冲中，就停止播放
    }
    
    if ([SDiOSVersion deviceSize] == Screen5Dot8inch) {
        self.videoPlayTC.constant = 44;
        self.navigationViewHC.constant = 44;
    } else {
        self.videoPlayTC.constant = 0;
        self.navigationViewHC.constant = 44;
    }
    
    self.commentInputView.layer.cornerRadius = 17;
    self.commentInputView.layer.masksToBounds = YES;
    self.commentInputView.backgroundColor = [UIColor colorWithRGBValue:0xE9ECF0];
    self.commentInputView.placeholder = @"写下并发表你的看法";
    self.commentInputView.delegate = self;
    self.commentInputView.font = [UIFont systemFontOfSize:14];
    self.commentInputView.maxLenght = 120;
    self.commentInputView.textView.returnKeyType = UIReturnKeySend;
    
    self.hideAnimation = YES;
    self.playManager = [VideoPlayManager loadFromNib];
    self.playManager.stateManager = self.stateManager;
    self.playManager.frame = CGRectMake(0, 0, self.view.width, ceil(self.view.width * 211 / 375));
    if (self.video && self.video.video_url) {
        self.video.isCache = YES;
        self.playManager.video = self.video;
        [self.videoBackImage setImageWithPhoto:self.video.photo];
    }
    self.playManager.delegate = self;
    [self.view addSubview:self.playManager];
    @weakify(self)
    self.playManager.typeOfSmallPanelMoreMenuHid = ^BOOL{
        @strongify(self)
        return self.smallPanelMoreView.isHidden;
    };
    self.playManager.disableMLBlackTransition = YES;
    
    [self setupModel];
    
    self.tableView.estimatedRowHeight = 0;
    [self.tableView registerNib:[VideoInfoCell nib] forCellReuseIdentifier:@"VideoInfoCell"];
    [self.tableView registerNib:[VideoRecommendCell nib] forCellReuseIdentifier:@"VideoRecommendCell"];
    [self.tableView registerNib:[VideoCommentCell nib] forCellReuseIdentifier:@"VideoCommentCell"];
    [self.tableView registerNib:[VideoInfoEndCell nib] forCellReuseIdentifier:@"VideoInfoEndCell"];
    [self.tableView reloadData];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppDidEnterBackGround:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(statusBarOrientationChanged:) name:UIApplicationDidChangeStatusBarOrientationNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardChangeFrame:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardChangeFrame:) name:UIKeyboardWillHideNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(shareFinished:) name:kMPSharedFinishNotification object:nil];
    
    self.allowOritation = YES;
    
    // 添加下拉刷新返回
//    [self setupRefreshHeaderView];
    // 添加右划返回
    UISwipeGestureRecognizer *panBackGesture = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(setPanRightBackGesture:)];
    // 轻扫方向:默认是右边
//    panBackGesture.direction = UISwipeGestureRecognizerDirectionUp;
    [self.tableView addGestureRecognizer:panBackGesture];
    
    [self.view bringSubviewToFront:self.navigationView];
    
    // 页面进入加载一次
    [self.videoInfoModel refresh];
    if (self.recommendListModel.category_id) {
        [self.recommendListModel refresh];
    }
    [self.videoCommentListModel refresh];
}

- (void)setPanRightBackGesture:(UISwipeGestureRecognizer *)panBackGesture {
    NSLog(@"滑动～～～～～～～～～～");
    [self handleSwipeDown];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];

//    [self.videoInfoModel refresh];
//    if (self.recommendListModel.category_id) {
//        [self.recommendListModel refresh];
//    }
//    [self.videoCommentListModel refresh];

    if (self.playing) {
        [self.playManager resumeVideo];
    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self showFeatureGuideViewIfNeeded];    // 显示引导页
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    if (self.tableViewHeight == 0) {
        self.tableViewHeight = self.tableView.height;
    }
    
    if (self.playManager.isFullScreen) {
        self.playManager.frame = self.view.frame;
    } else {
        self.playManager.frame = self.videoView.frame;
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
//    [self.navigationController setNavigationBarHidden:NO animated:animated];
    self.playing = self.playManager.isPlaying;
    [self.playManager pauseVideo];
//    self.isDownScroll = NO;
}

- (void)dealloc {
    [self.playManager stopVideo];
    [self.playManager removeFromSuperview];
    self.playManager = nil;
    
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    [[AVAudioSession sharedInstance] setActive:NO error:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    // 如果进来时电台在播放，退出时还原
    if (self.audioPlaying) {
        [[AudioPlayManager sharedInstance] play];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    if (self.playManager.alpha < 0.5) {
        return UIStatusBarStyleDefault;
    }
    return UIStatusBarStyleLightContent;
}

- (void)setupModel {
    @weakify(self)
    self.videoInfoModel = [[VideoInfoModel alloc] init];
    self.videoInfoModel.video_id = self.video.id;
    self.videoInfoModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        if (error == nil) {
            VIDEO *modelItem = self.videoInfoModel.item;
            if ([self.video.id isEqualToString:modelItem.id]) {
                self.hideAnimation = YES;
            }
            if (self.video.isCache) {
                self.hideAnimation = NO;
            }

            self.video = self.videoInfoModel.item;
            if (![UserModel online]) {
                // 没有登录的时候，保存观看记录到本地
                VIDEO_HISTORY *history = [[VIDEO_HISTORY alloc] init];
                history.video_id = self.video.id;
                history.watch_at = [NSString stringWithFormat:@"%ld", (long)[[NSDate date] timeIntervalSince1970]];
                [self.offlineWatchModel addWatchVideo:history];
            }
            
            if (!self.recommendListModel.category_id) {
                self.recommendListModel.category_id = self.video.category.id;
                self.recommendListModel.video_id = self.video.id;
                [self.recommendListModel refresh];
            }
            self.video.isCache = NO;
            self.playManager.video = self.video;
            [self.videoBackImage setImageWithPhoto:self.video.photo];
            [self notificationDataRefresh];
        } else {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
//        [self expandVideoCell];
        [self reloadData];
    };
    
    self.recommendListModel = [[RecommendVideoListModel alloc] init];
    self.recommendListModel.video_id = self.video.id;
    if (self.video.category && self.video.category.id) {
        self.recommendListModel.category_id = self.video.category.id;
    }
    self.recommendListModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        if (error) {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
        [self reloadData];
    };
    
    self.offlineWatchModel = [[OfflineWatchListModel alloc] init];
    
    self.videoCommentListModel = [[VideoCommentListModel alloc] init];
    self.videoCommentListModel.video_id = self.video.id;
    self.videoCommentListModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        // 视频详情加载顺序错误：先加载 没有更多了，后加载视频详情
        [self reloadData];
        if (error == nil) {
            if (self.videoCommentListModel.more) {
                [self.tableView.footer endRefreshing];
            } else {
                [self.tableView.footer noticeNoMoreData];
            }
        } else {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
    };
}

- (void)reloadData {
    if (self.videoInfoModel.loaded && self.recommendListModel.loaded && self.videoCommentListModel.loaded) {
        [self.tableView reloadData];
    }
}

#pragma mark - dataSource

- (BOOL)isRecommendVideoListHasItems {
    return self.recommendListModel.loaded && !self.recommendListModel.isEmpty;
}

- (BOOL)isVideoCommentListHasItems {
    return self.videoCommentListModel.loaded && !self.videoCommentListModel.isEmpty;
}

- (BOOL)isShowEndCell {
    return self.recommendListModel.loaded && self.recommendListModel.isEmpty && self.videoCommentListModel.loaded && self.videoCommentListModel.isEmpty;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    } else if (section == 1) {
        if ([self isRecommendVideoListHasItems]) {
            return 1;
        } else {
            return 0;
        }
    } else if (section == 2) {
        return self.videoCommentListModel.items.count;
    } else if (section == 3) {
        if ([self isShowEndCell]) {
            return 1;
        } else {
            return 0;
        }
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        VideoInfoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VideoInfoCell" forIndexPath:indexPath];
        cell.delegate = self;
        cell.data = self.video;
        return cell;
    } else if (indexPath.section == 1) {
        VideoRecommendCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VideoRecommendCell" forIndexPath:indexPath];
        cell.delegate = self;
        cell.data = self.recommendListModel.items;
        return cell;
    } else if (indexPath.section == 2) {
        VideoCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VideoCommentCell" forIndexPath:indexPath];
        COMMENT *comment = self.videoCommentListModel.items[indexPath.row];
        cell.comment = comment;
        
        @weakify(self)
        cell.onReply = ^(REPLY *reply) {
            @strongify(self)
            // 回复回复
            [self commentSelectAction:reply];
        };
        return cell;
    } else if (indexPath.section == 3) {
        VideoInfoEndCell *endCell = [tableView dequeueReusableCellWithIdentifier:@"VideoInfoEndCell" forIndexPath:indexPath];
        return endCell;
    }
    
    return nil;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return [VideoInfoCell heightForVideoInfoCell:self.video];
    } else if (indexPath.section == 1) {
        return 220;
    } else if (indexPath.section == 2) {
        COMMENT *comment = self.videoCommentListModel.items[indexPath.row];
        return [VideoCommentCell rowHeightWithComment:comment];
    } else if (indexPath.section == 3) {
        return 43;
    }
    
    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 2 && [self isVideoCommentListHasItems]) {
        return 55;
    }
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 2 && [self isVideoCommentListHasItems]) {
        VideoCommentHeader *header = [VideoCommentHeader loadFromNib];
        return header;
    }
    return nil;
}

- (nullable UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return nil;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.video.isCache) {
        cell.alpha = 0;
    }
    if (self.hideAnimation) {
        return;
    }

    if (indexPath.section == 0) {
        [cell startSingleViewAnimation];
    }
    if (indexPath.section == 1 && [tableView numberOfRowsInSection:indexPath.section]) {
        [cell startSingleViewAnimationDelay:1.2];
    }
    if (indexPath.section == 2) {
        if ([tableView numberOfRowsInSection:1]) {
            [cell startSingleViewAnimationDelay:1.8];
        } else {
            [cell startSingleViewAnimationDelay:1.5];
        }
    }
    if (indexPath.section == 3) {
        [cell startSingleViewAnimationDelay:1.2];
    }
    
    [self setupRefreshFooterView];
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    if (self.video.isCache) {
        view.alpha = 0;
    }
    if (self.hideAnimation) {
        return;
    }

    if (section == 2) {
        view.alpha = 0;
        if ([tableView numberOfRowsInSection:1]) {
            [view startSingleViewAnimationDelay:1.5];
        } else {
            [view startSingleViewAnimationDelay:1.2];
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 2) {
        [self.view endEditing:YES];
        VideoCommentCell *cell = (VideoCommentCell *)[tableView cellForRowAtIndexPath:indexPath];
        [self commentSelectAction:cell.comment];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    self.hideAnimation = YES;
    
    if (self.tableView.contentSize.height <= self.tableViewHeight) {
        return;
    }
    if (!self.playManager.isWaiting) {
        return;
    }
    
    CGFloat fakeHeight = [SDiOSVersion deviceSize] == Screen5Dot8inch ? 44 : 64;
    CGFloat alphaViewHeight = ceil(self.view.width * 211 / 375);
    CGFloat transformY = alphaViewHeight - fakeHeight;
    CGFloat offset = scrollView.contentOffset.y;
    
    if (offset < 0) {
        offset = 0;
    }
    if (transformY - offset < 0) {
        offset = transformY;
    }
    
    CGFloat videoPlayTC = [SDiOSVersion deviceSize] == Screen5Dot8inch ? 44 : 0;
    self.videoPlayTC.constant = self.playManager.y = videoPlayTC - offset;
    
    CGFloat changePoint = 0;
    if (offset > changePoint) {
        CGFloat alpha = (alphaViewHeight - offset - fakeHeight) / alphaViewHeight;
        self.playManager.alpha = alpha;
        self.navigationView.alpha = 1 - alpha;
        [self setNeedsStatusBarAppearanceUpdate];
    } else {
        self.playManager.alpha = 1;
        self.navigationView.alpha = 0;
        [self setNeedsStatusBarAppearanceUpdate];
    }
    
    // 解决scrollView来回跳动的问题
    int currentPostion = scrollView.contentOffset.y;
    if (currentPostion - _lastPosition > 10 && currentPostion > 0) {
        _lastPosition = currentPostion;
        if (self.videoCommentListModel.more) {
            scrollView.bounces = YES;
        } else {
            scrollView.bounces = NO;
        }
    } else if ((_lastPosition - currentPostion > 10) && (currentPostion <= scrollView.contentSize.height - scrollView.bounds.size.height - 10)) {
        _lastPosition = currentPostion;
        scrollView.bounces = YES;
    }
}

- (void)refreshData {
    self.videoInfoModel.video_id = self.video.id;
    self.recommendListModel.category_id = self.video.category.id;
    self.recommendListModel.video_id = self.video.id;
    self.videoCommentListModel.video_id = self.video.id;

    // 数据传给manager
    self.video.isCache = YES;
    self.playManager.video = self.video;

    [self.videoInfoModel refresh];
    [self.recommendListModel refresh];
    [self.videoCommentListModel refresh];
}

#pragma mark - VideoInfoCellDelegate

- (void)commentVideo:(VIDEO *)video sender:(UIButton *)sender {
    [AppAnalytics clickEvent:@"click_video_comment"];
    
    NSInteger rowCount = [self.tableView numberOfRowsInSection:2];
    if (rowCount > 0) {
        for (UITableViewCell *cell in self.tableView.visibleCells) {
            NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
            if (indexPath.section == 2) {
                return;
            }
        }
        
        if (self.playManager.isWaiting) {
            CGFloat videoInfoHeight = [VideoInfoCell heightForVideoInfoCell:self.video];
            CGFloat height = videoInfoHeight + 220;
            CGFloat offset = height;
            if (self.tableView.contentSize.height < (self.tableViewHeight + height)) {
                CGFloat rowHeights = rowCount * 80;
                
                if (rowHeights > videoInfoHeight) {
                    offset = videoInfoHeight;
                } else {
                    CGFloat ratio = 1;
                    if ([SDiOSVersion deviceSize] == Screen5Dot8inch) {
                        ratio = 1.94;
                    } else if ([SDiOSVersion deviceSize] == Screen4inch) {
                        ratio = 1.54;
                    } else if ([SDiOSVersion deviceSize] == Screen4Dot7inch) {
                        ratio = 1.38;
                    } else if ([SDiOSVersion deviceSize] == Screen5Dot5inch) {
                        ratio = 1.54;
                    }
                    
                    CGFloat calcuteOffset = videoInfoHeight / ratio;
                    offset = calcuteOffset;
                }
            }
            [self.tableView setContentOffset:CGPointMake(0, offset) animated:YES];
        } else {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:2];
            [self.tableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
        }
    } else {
        [self.commentInputView.textView becomeFirstResponder];
    }
}

- (void)likeVideo:(VIDEO *)video sender:(UIButton *)sender {
    [AppAnalytics clickEvent:@"click_video_thumbUp"];
    [self.view endEditing:YES];
    
//    [self presentLoadingTips:nil];
    if (video.is_like) {
//        [VideoModel unlike:video then:^(STIHTTPResponseError *error) {
//            [self dismissTips];
//            if (error == nil) {
//                video.is_like = NO;
//                self.video = video;
//                self.playManager.video = video;
//
//                [self notificationDataRefresh];
//            } else {
//                [self presentMessage:error.message withTips:@"数据获取失败"];
//            }
//            [self refreshVideoCell];
//        }];
        self.lastAnimationDate = [NSDate date];
        [VideoModel like:video then:^(STIHTTPResponseError *error) {
//            [self dismissTips];
            if (error == nil) {
                video.is_like = YES;
                self.video = video;
                self.playManager.video = video;
                
                [self notificationDataRefresh];
                [UserPointModel userPointName:MODULE_NAME_CLICK_THUMBUP_FINISH];
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
            
            // 本地刷新内容
//            [self delayRefreshCell];
        }];
    } else {
        self.lastAnimationDate = [NSDate date];
        [VideoModel like:video then:^(STIHTTPResponseError *error) {
//            [self dismissTips];
            if (error == nil) {
                video.is_like = YES;
                self.video = video;
                self.playManager.video = video;
                
                [self notificationDataRefresh];
                [UserPointModel userPointName:MODULE_NAME_CLICK_THUMBUP_FINISH];
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
            
            // 本地刷新内容
//            [self delayRefreshCell];
        }];
    }
}

- (void)collectVideo:(VIDEO *)video sender:(UIButton *)sender {
    [AppAnalytics clickEvent:@"click_video_collect"];
    [self.view endEditing:YES];

//    [self presentLoadingTips:nil];
    if (video.is_collect) {
        self.lastAnimationDate = [NSDate date];
        [VideoModel unfavourite:video then:^(STIHTTPResponseError *error) {
//            [self dismissTips];
            if (error == nil) {
//                video.is_collect = NO;
                self.video = video;
                self.playManager.video = video;
                
                [self notificationDataRefresh];
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
            
            // 本地刷新内容
//            [self delayRefreshCell];
        }];
    } else {
        self.lastAnimationDate = [NSDate date];
        [VideoModel favourite:video then:^(STIHTTPResponseError *error) {
//            [self dismissTips];
            if (error == nil) {
//                video.is_collect = YES;
                self.video = video;
                self.playManager.video = video;

                [self notificationDataRefresh];
                [UserPointModel userPointName:MODULE_NAME_CLICK_COLLECT_FINISH];
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
            
            // 本地刷新内容
//            [self delayRefreshCell];
        }];
    }
}

- (void)delayRefreshCell {
    NSTimeInterval time = [[NSDate date] timeIntervalSinceDate:self.lastAnimationDate];
    if (time > 1.1) {
        [self refreshVideoCell];
    } else {
        [self performSelector:@selector(refreshVideoCell) withObject:nil afterDelay:(1.1 - time)];
    }
    self.lastAnimationDate = nil;
}

- (void)starVideo:(VIDEO *)video sender:(UIButton *)sender {
    [self starVideo:video isFullScreen:NO sender:sender];
}

- (void)starVideo:(VIDEO *)video isFullScreen:(BOOL)fullScreen sender:(UIButton *)sender {
    [AppAnalytics clickEvent:@"click_video_score"];
    [self.view endEditing:YES];

    [sender disableSelf];
    VideoStarAlertView *actionAlert = [VideoStarAlertView loadFromNib];
    if (fullScreen) {
        [actionAlert darkTheme];
    }
    @weakify(actionAlert)
    @weakify(self)
    actionAlert.confirmAction = ^(NSInteger starValue) {
        @strongify(actionAlert)
        @strongify(self)
        if (starValue == 0) {
            [[UIApplication sharedApplication].keyWindow presentMessageTips:@"请给短片评分吧~"];
        } else {
            if (video.is_star) {
                [self presentMessageTips:@"您已经给这个视频评过分了"];
            } else {
                [VideoModel star:self.video point:starValue then:^(STIHTTPResponseError *error) {
                    if (error == nil) {
                        video.is_star = YES;
                        [self refreshVideoCell];
                        [self.videoInfoModel refresh];
                        
                        UIImageView *successIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_grade_acc"]];
                        [self presentMessageTips:@"评分成功" customView:successIcon];
                    } else {
                        [[UIApplication sharedApplication].keyWindow presentMessageTips:error.message];
                    }
                }];
            }
            [sender enableSelf];
            [actionAlert hide];
        }
    };
    actionAlert.cancelAction = ^{
        [sender enableSelf];
    };
    self.actionAlert = actionAlert;
    [actionAlert show];
}

- (void)gotoAuthorInfo:(VIDEO *)video {
    AuthorInfoController *authorInfo = [AuthorInfoController spawn];
    authorInfo.author = video.owner;
    authorInfo.followAuthor = ^(USER *author) {
        video.owner = author;
        [self refreshVideoCell];
    };
    [self presentNavigationController:authorInfo];
}

- (void)followAuthor:(VIDEO *)video sender:(UIButton *)sender {
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }
    
    [sender followAuthorButtonAnimated];
    
    [sender disableSelf];
    [self presentLoadingTips:nil];
    if (video.owner.is_follow) {
        [AuthorModel unfollow:video.owner then:^(STIHTTPResponseError *error) {
            if (error == nil) {
                video.owner.is_follow = NO;
                [self refreshVideoCell];
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
            [self dismissTips];
            [sender enableSelf];
            [self notificationDataRefresh];
        }];
    } else {
        if (!video.owner.is_author) {
            [self dismissTips];
            [[UIApplication sharedApplication].keyWindow presentMessageTips:@"作者不存在！"];
            return;
        }
        
        [AuthorModel follow:video.owner then:^(STIHTTPResponseError *error) {
            if (error == nil) {
                video.owner.is_follow = YES;
                [self refreshVideoCell];
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
            [self dismissTips];
            [sender enableSelf];
            [self notificationDataRefresh];
        }];
    }
}

- (void)shareVideo:(VIDEO *)video sender:(UIButton *)sender {
    [self shareVideo:video isFullScreen:NO];
}

- (void)addVideoToWormhole:(VIDEO *)video sender:(UIButton *)sender {
    // TODO: 加入虫洞
    [self addWormholeVideoCustom];
    
    @weakify(self)
    self.addWormholeVideoView.wormholeCreate = ^{
        @strongify(self);
        // 创建虫洞
        WormholeCreateController *wormholeCreate = [WormholeCreateController spawn];
        [self.addWormholeVideoView hide];
        [self.navigationController pushViewController:wormholeCreate animated:YES];
        wormholeCreate.wormholeCreateBack = ^{
            [self addWormholeVideoCustom];
            [self.addWormholeVideoView.wormholeListModel refresh];
        };
    };
    self.addWormholeVideoView.addWormholeVideo = ^(NSString *wormholeId) {
        @strongify(self);
        [self addToWormholeWithVideoId:video.id wormholeId:wormholeId];
    };
}

- (void)addWormholeVideoCustom {
    if (!self.addWormholeVideoView) {
        self.addWormholeVideoView = [[AddWormholeVideoView alloc] initWithFrame:kScreenRect];
        [self.view addSubview:self.addWormholeVideoView];
    }
    [self.addWormholeVideoView show];
}

- (void)expandVideoCell {
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
    [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
    [self.tableView setContentOffset:CGPointZero animated:YES];
}

- (void)refreshVideoCell {
    self.hideAnimation = YES;
    [self expandVideoCell];
}

#pragma mark - VideoPlayManagerDelegate

- (void)refreshVideoManager {
    [self.videoInfoModel refresh];
    [self.recommendListModel refresh];
    [self.videoCommentListModel refresh];
}

- (void)clickBackButton {
    self.allowOritation = NO;
    [self.view endEditing:YES];
    [self closeController];
}

- (void)statusBarHide:(BOOL)hide {
    self.statusBarHidden = hide;
    [UIView animateWithDuration:0.3 animations:^{
        [self setNeedsStatusBarAppearanceUpdate];
    }];
}

- (void)fullScreenCollectVideo:(VIDEO *)video sender:(UIButton *)sender {
    if (![UserModel online]) {
        [self.playManager needChangeScreenWithFullView];
        [[Authorization sharedInstance] showAuth];
        return;
    }

    [sender disableSelf];
    if (video.is_collect) {
        [VideoModel unfavourite:video then:^(STIHTTPResponseError *error) {
            if (error == nil) {
                video.is_collect = NO;
                self.video = video;
                self.playManager.video = video;
                [self refreshVideoCell];
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
            [self.videoInfoModel refresh];
            [sender enableSelf];
        }];
    } else {
        [VideoModel favourite:video then:^(STIHTTPResponseError *error) {
            if (error == nil) {
                video.is_collect = YES;
                self.video = video;
                self.playManager.video = video;
                [self refreshVideoCell];
                [UserPointModel userPointName:MODULE_NAME_CLICK_COLLECT_FINISH];
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
            [self.videoInfoModel refresh];
            [sender enableSelf];
        }];
    }
}

- (void)fullScreenStarVideo:(VIDEO *)video sender:(UIButton *)sender {
    if (![UserModel online]) {
        [self.playManager needChangeScreenWithFullView];
        [[Authorization sharedInstance] showAuth];
        return;
    }
    
    if (video.is_star) {
        return;
    }
    
    [self starVideo:video isFullScreen:YES sender:sender];
}

- (void)shareVideo:(VIDEO *)video isFullScreen:(BOOL)isFullScreen {
    [AppAnalytics clickEvent:@"click_video_share"];
    [self.view endEditing:YES];

    if (isFullScreen) {
        FullScreenShareView *largeShareView = [FullScreenShareView loadFromNib];
        self.largeShareView = largeShareView;
    } else {
        DarkShareView *smallShareView = [DarkShareView loadFromNib];
        self.smallShareView = smallShareView;
    }
    
    @weakify(self)
    if (video.photo && video.photo.thumb) {
        [self presentLoadingTips:nil];
        [self imageWithUrl:video.photo.thumb_square completion:^(UIImage *image) {
            @strongify(self)
            dispatch_async(dispatch_get_main_queue(), ^{
                [self dismissTips];
                video.shareImage = image;
                if (isFullScreen) {
                    self.largeShareView.data = video;
                    [self.largeShareView show];
                } else {
                    self.smallShareView.data = video;
                    [self.smallShareView show];
                }
            });
        }];
    } else {
        if (isFullScreen) {
            self.largeShareView.data = video;
            [self.largeShareView show];
        } else {
            self.smallShareView.data = video;
            [self.smallShareView show];
        }
    }
}

- (void)addVideoToLater:(VIDEO *)video {
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }

    WatchLaterModel *watchLaterModel = [[WatchLaterModel alloc] init];
    [watchLaterModel addVideoToLater:video];
}

- (void)reportVideo:(VIDEO *)video {
    [self.view endEditing:YES];
    
    GKZActionSheet *actionSheet = [GKZActionSheet loadFromNib];
    self.reportTypeAlertView = [[AlertView alloc] initWithContent:actionSheet type:AlertViewTypeMonospaced];
    actionSheet.data = @[@"垃圾广告营销", @"恶意攻击谩骂", @"淫秽色情信息", @"违法有害信息", @"其它不良信息"];
    
    actionSheet.whenHide = ^(BOOL hide) {
        [self.reportTypeAlertView hide];
    };
    actionSheet.whenRegistered = ^(id data, NSInteger index) {
        [self.reportTypeAlertView hide];
        REPORT_TYPE type = index + 1;
        [VideoModel report:self.video reason:type then:^(STIHTTPResponseError *error) {
            if (error == nil) {
                [self presentMessageTips:@"您的举报我们会在24小时内进行处理"];
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
        }];
    };
    
    [self.reportTypeAlertView showSharedView];
}

- (void)notificationDataRefresh {
    // 同步最新的数据给前一页面
    if (self.reloadVideo) {
        self.reloadVideo(self.video);
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:kVideoInfoRefreshNotification object:nil userInfo:@{@"data": self.video}];
}

- (void)startPlayVideo {
    [self playVideoButtonAction:nil];
}

- (void)showSmallPanelMoreMenu {
    [self navigationMoreButtonAction:nil];
}

#pragma mark - SmallPanelMoreViewDelegate

- (void)hideSmallPanelMoreMenu {
    self.smallPanelMoreView.hidden = YES;
    [self.smallPanelMoreView removeFromSuperview];
}

- (void)shareVideo {
    [self shareVideo:self.video isFullScreen:NO];
}

- (void)addToLater {
    [self addVideoToLater:self.video];
}

- (void)reportVideo {
    [self reportVideo:self.video];
}

#pragma mark - VideoRecommendCellDelegate

- (void)switchVideoData:(VIDEO *)video {
    [self.view endEditing:YES];
    self.video = video;
    [self refreshData];
    self.hideAnimation = NO;
    
    NSInteger rowCount = [self.tableView numberOfRowsInSection:1];
    if (rowCount == 1) {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
        VideoRecommendCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        [cell resetContentOffset];
    }
    
    [self.tableView reloadData];
    [self.tableView setContentOffset:CGPointZero animated:YES];
}

#pragma mark - ZLPlaceholderTextViewDelegate

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return NO;
    }
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {
        [self sendCommentButtonAction:nil];
        return NO;
    }
    return YES;
}

#pragma mark - Button Action

- (IBAction)navigationBackButtonAction:(id)sender {
    [self clickBackButton];
}

- (IBAction)navigationMoreButtonAction:(id)sender {
    self.smallPanelMoreView.hidden = NO;
    [self.view addSubview:self.smallPanelMoreView];
}

- (IBAction)playVideoButtonAction:(id)sender {
    [self initSubviewState];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.playManager resumeVideo];
    });
}

- (void)initSubviewState {
    CGFloat videoPlayTC = [SDiOSVersion deviceSize] == Screen5Dot8inch ? 44 : 0;
    self.videoPlayTC.constant = self.playManager.y = videoPlayTC;
    self.playManager.alpha = 1;
    self.navigationView.alpha = 0;
    
    [self.tableView setContentOffset:CGPointZero animated:YES];
}

- (IBAction)sendCommentButtonAction:(id)sender {
    [AppAnalytics clickEvent:@"click_video_comment"];
    
    if (self.commentInputView.text.trim.length <= 0) {
        [self presentFailureTips:@"内容不能为空"];
        return;
    }
    if (self.commentInputView.text.length <= 0) {
        [self presentFailureTips:@"内容不能为空"];
        return;
    }
    if (self.commentInputView.text.length > 120) {
        [self presentFailureTips:@"发表内容超长"];
        return;
    }
    
    self.hideAnimation = YES;
    [self.view endEditing:YES];
    NSString *content = self.commentInputView.text;
    self.commentInputView.text = nil;
    if (!self.additional) { // 评论
        [self presentLoadingTips:nil];
        @weakify(self)
        [VideoCommentListModel sendComment:self.video.id content:content completed:^(COMMENT *comment, NSInteger total, STIHTTPResponseError *error) {
            @strongify(self)
            [self dismissTips];
            if (error) {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            } else {
                [UserPointModel userPointName:MODULE_NAME_CLICK_SCORE_FINISH];
                [self.view presentMessageTips:@"评论发表成功"];
//                [self.videoInfoModel refresh];
//                [self.videoCommentListModel refresh];
                
                // 改为延迟刷新，保证动画
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    //[self.videoInfoModel refresh];
                    [self.videoCommentListModel refresh];
                });
                self.additional = nil;
                
                
                long value = self.video.comment_count_array.number.longValue + 1;
                self.video.comment_count_array.number = @(value);
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
                [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                VideoInfoCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
                [cell startCommentAnimation];
            }
        }];
    } else { // 回复
        NSString *replyId = nil;
        COMMENT_TYPE type = COMMENT_TYPE_COMMENT;
        if ([self.additional isKindOfClass:[COMMENT class]]) {
            COMMENT *com = (COMMENT *)self.additional;
            replyId = com.id;
            type = COMMENT_TYPE_COMMENT;
        } else {
            REPLY *reply = (REPLY *)self.additional;
            replyId = reply.id;
            type = COMMENT_TYPE_REPLY;
        }
        
        [self presentLoadingTips:nil];
        @weakify(self)
        [VideoCommentListModel replyComment:replyId content:content type:type completed:^(COMMENT *comment, STIHTTPResponseError *error) {
            @strongify(self)
            [self dismissTips];
            if (error) {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            } else {
                [self.view presentMessageTips:@"评论发表成功"];
                [self.videoInfoModel refresh];
                [self.videoCommentListModel refresh];
                self.additional = nil;
            }
        }];
    }
}

- (void)setAdditional:(id)additional {
    _additional = additional;
    if ([additional isKindOfClass:[COMMENT class]]) {
        COMMENT *comment = (COMMENT *)additional;
        NSString *name = comment.owner && comment.owner.name ? comment.owner.name : [AppTheme defaultPlaceholder];
        self.commentInputView.placeholder = [NSString stringWithFormat:@"回复 %@:", name];
    } else if ([additional isKindOfClass:[REPLY class]]) {
        REPLY *reply = (REPLY *)additional;
        NSString *name = reply.owner && reply.owner.name ? reply.owner.name : [AppTheme defaultPlaceholder];
        self.commentInputView.placeholder = [NSString stringWithFormat:@"回复 %@:", name];
    } else {
        self.commentInputView.placeholder = @"写下并发表你的看法";
    }
}

- (void)commentSelectAction:(id)commentData {
    GKZActionSheet *actionSheet = [GKZActionSheet loadFromNib];
    AlertView * alertView = [[AlertView alloc] initWithContent:actionSheet type:AlertViewTypeMonospaced];
    self.reportCommentTypeAlertView = alertView;
    
    USER *owner;
    NSString *commentId;
    if ([commentData isKindOfClass:[COMMENT class]]) {
        COMMENT *comment = (COMMENT *)commentData;
        owner = comment.owner;
        commentId = comment.id;
    } else {
        REPLY *reply = (REPLY *)commentData;
        owner = reply.owner;
        commentId = reply.id;
    }
    
    if ([owner.id isEqualToString:[UserModel sharedInstance].user.id]) {
        actionSheet.data = @[@"回复"];
    } else {
        actionSheet.data = @[@"回复", @"举报", @"拉黑"];
    }
    [alertView showSharedView];
    
    actionSheet.whenHide = ^(BOOL hide) {
        [alertView hide];
    };
    actionSheet.whenRegistered = ^(id data, NSInteger index) {
        [alertView hide];
        if (index == 0) {
            self.additional = commentData;
            [self.commentInputView.textView becomeFirstResponder];
        } else if (index == 1) {
            GKZActionSheet *actionSheet = [GKZActionSheet loadFromNib];
            self.reportCommentAlertView = [[AlertView alloc] initWithContent:actionSheet type:AlertViewTypeMonospaced];
            actionSheet.data = @[@"垃圾广告营销", @"恶意攻击谩骂", @"淫秽色情信息", @"违法有害信息", @"其它不良信息"];
            actionSheet.whenHide = ^(BOOL hide) {
                [self.reportCommentAlertView hide];
            };
            actionSheet.whenRegistered = ^(id data, NSInteger index) {
                [self.reportCommentAlertView hide];
                REPORT_TYPE type = index + 1;
                [VideoCommentModel reportComment:commentId reason:type then:^(STIHTTPResponseError *error) {
                    if (error == nil) {
                        [self presentMessageTips:@"您的举报我们会在24小时内进行处理"];
                    } else {
                        [self presentMessage:error.message withTips:@"数据获取失败"];
                    }
                }];
            };
            [self.reportCommentAlertView showSharedView];
        } else if (index == 2) {
            MPAlertView *blackUserAlertView = [MPAlertView alertWithTitle:@"确认要将该用户加入黑名单吗？" message:@"您将不会在评论区看到该用户的相关信息" cancelButton:@"取消" confirmButton:@"确定"];
            self.blackUserAlertView = blackUserAlertView;
            blackUserAlertView.confirmAction = ^{
                [VideoCommentModel setCommentUserToBlackList:owner then:^(STIHTTPResponseError *error) {
                    if (error == nil) {
                        [self presentMessageTips:@"加入黑名单成功"];
                        [self.videoCommentListModel refresh];
                    } else {
                        [self presentMessage:error.message withTips:@"数据获取失败"];
                    }
                }];
            };
            [blackUserAlertView show];
        }
    };
}

#pragma mark - 支持转屏

- (BOOL)shouldAutorotate {
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (BOOL)prefersStatusBarHidden {
    return self.statusBarHidden;
}

#pragma mark - 转屏事件回调

- (void)rotateScreen {
    [self initSubviewState];
    
    [self.actionAlert hide];
    [self.largeShareView hide];
    [self.smallShareView hide];
    [self.addWormholeVideoView hide];
    [self.reportTypeAlertView hide];
    [self.reportCommentAlertView hide];
    [self.reportCommentTypeAlertView hide];
    [self.blackUserAlertView hide];
    [self hideSmallPanelMoreMenu];
    [self.view endEditing:YES];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self rotateScreen];
    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        [self.playManager reloadFullViewWithIsFull:YES];
    } else {
        [self.playManager reloadFullViewWithIsFull:NO];
    }
}

#pragma mark - Notification

- (void)onAppDidEnterBackGround:(UIApplication*)app {
    if (self.playing) {
        [self.playManager pauseVideo];
    }
}

- (void)onAppWillResignActive:(UIApplication *)app {
    self.playing = self.playManager.isPlaying;
    if (self.playing) {
        [self.playManager pauseVideo];
    }
    if (self.playManager.isFullScreen) {
        self.isLandscape = YES;
    }
}

- (void)onAppWillEnterForeground:(UIApplication*)app {
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    if (self.playing) {
        [self.playManager resumeVideo];
    }
}

- (void)onAppDidBecomeActive:(UIApplication*)app {
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    self.isLandscape = NO;
    if (self.playing) {
        [self.playManager resumeVideo];
    }
}

- (void)statusBarOrientationChanged:(NSNotification *)note {
    
}

- (void)keyboardChangeFrame:(NSNotification *)notification {
    self.hideAnimation = YES;
    
    CGRect keyboardBounds = [notification.userInfo[UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGFloat duration = [notification.userInfo[UIKeyboardAnimationDurationUserInfoKey] doubleValue];

    CGFloat bottomInset = 0;
    if (@available(iOS 11.0, *)) {
        bottomInset = self.view.safeAreaInsets.bottom;
    }
    if ([notification.name isEqualToString:UIKeyboardWillShowNotification]) {
        [UIView animateWithDuration:duration animations:^{
            self.inputViewBC.constant = keyboardBounds.size.height - bottomInset;
        }];
    } else if ([notification.name isEqualToString:UIKeyboardWillHideNotification]) {
        [UIView animateWithDuration:duration animations:^{
            self.inputViewBC.constant = 0;
        }];
    }
}

#pragma mark - Header

- (void)setupRefreshHeaderView {
    @weakify(self)
    [self.tableView addVideoHeaderPullLoader:^{
        @strongify(self)
        [self handleSwipeDown];
    }];
}

- (void)setupRefreshFooterView {
    if (self.tableView.footer == nil) {
        if (!self.videoCommentListModel.isEmpty) {
            @weakify(self);
            [self.tableView addFooterPullLoader:^{
                @strongify(self);
                [self.videoCommentListModel loadMore];
            }];
        }
    } else {
        if (self.videoCommentListModel.isEmpty) {
            [self.tableView removeFooter];
        }
    }
}

#pragma mark - gesture

- (void)handleSwipeDown {
    if (!self.playManager.isFullScreen) {
        [self clickBackButton];
    }
}

#pragma mark - Model Action

- (void)addToWormholeWithVideoId:(NSString *)videoId wormholeId:(NSString *)wormholeId {
    AddWormholeVideoModel *addWormholeVideo = [[AddWormholeVideoModel alloc] init];
    addWormholeVideo.videoId = videoId;
    addWormholeVideo.wormholeId = wormholeId;
    [addWormholeVideo refresh];
    addWormholeVideo.whenUpdated = ^(STIHTTPResponseError *error) {
        if (error) {
            [self presentMessage:error.message withTips:error.message];
        } else {
            [self presentMessageTips:@"添加成功"];
        }
    };
    
    if (addWormholeVideo.items.count > 0) {
        self.data = addWormholeVideo.items.firstObject;
    }
}

#pragma mark - lazy load

- (SmallPanelMoreView *)smallPanelMoreView {
    if (!_smallPanelMoreView) {
        _smallPanelMoreView = [SmallPanelMoreView loadFromNib];
        _smallPanelMoreView.frame = CGRectMake(0, 0, self.view.width, self.view.height);
        _smallPanelMoreView.delegate = self;
    }
    return _smallPanelMoreView;
}

#pragma mark - Guide

// 显示新手引导图
- (void)showFeatureGuideViewIfNeeded {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    BOOL showGuide = [ud boolForKey:@"showVideoGuide"];
    if (!showGuide) {
        [self.playManager stopLoad];
        [self showFeatureGuideView];
    }
}

- (void)showFeatureGuideView {
    CGFloat cellHeight = [VideoInfoCell heightForVideoInfoCell:self.video];
    CGFloat y = CGRectGetMinY(self.tableView.frame) + cellHeight - 10 - 126;
    MPVideoGuideView *guideView = [MPVideoGuideView showWithImageOriginY:y];
    guideView.delegate = self;
}

- (void)dismissOnGuideFinish:(MPVideoGuideView *)guideView {
    NSLog(@"dismiss the guide view");
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setBool:YES forKey:@"showVideoGuide"];
    [ud synchronize];
    [self.playManager resumeLoad];
}

#pragma mark - Share

// 分享完成，包括成功和失败
- (void)shareFinished:(NSNotification *)note {
    NSLog(@"shared finish");
    if (self.video) {
        long value = self.video.share_count_array.number.longValue + 1;
        self.video.share_count_array.number = @(value);
        
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [self.tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
        VideoInfoCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
        [cell startShareAnimation];
    }
}

@end
