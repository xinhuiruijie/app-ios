//
//  VideoInfoController.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/6.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "MPViewController.h"
#import "VideoState.h"

FOUNDATION_EXPORT NSString * const kVideoInfoRefreshNotification;

@interface VideoInfoController : MPViewController

@property (nonatomic, strong) VIDEO *video;
@property (nonatomic, strong) VideoStateManager *stateManager;

@property (nonatomic, assign) BOOL allowOritation;
@property (nonatomic, assign) BOOL isLandscape;

@property (nonatomic, copy) void(^reloadVideo)(VIDEO *video);

@end
