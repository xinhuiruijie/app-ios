//
//  TransitioningDelegate.m
//  MotherPlanet
//
//  Created by GeekZooStudio on 2017/12/18.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "TransitioningDelegate.h"
#import "PresentAnimator.h"

@implementation TransitioningDelegate

#pragma mark - UIViewControllerTransitioningDelegate

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    
    return nil;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented presentingController:(UIViewController *)presenting sourceController:(UIViewController *)source {
    
    PresentAnimator *p = [[PresentAnimator alloc] init];
    p.finalFrame = self.finalFrame;
    return p;
}

@end
