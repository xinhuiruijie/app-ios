//
//  VideoCommentListController.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/7.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "VideoCommentListController.h"
#import "SendCommentController.h"
#import "VideoCommentListModel.h"
#import "VideoCommentCell.h"
#import "VideoReplyCommentCell.h"
#import "VideoCommentModel.h"

@interface VideoCommentListController () <UITableViewDelegate, UITableViewDataSource, SendCommentControllerDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *commentTotalLabel;
@property (weak, nonatomic) IBOutlet UIImageView *userIconImgView;

@property (nonatomic, strong) AlertView *reportAlertView;
@property (nonatomic, strong) AlertView *reportTypeAlertView;
@property (nonatomic, strong) MPAlertView *blackUserAlertView;

@property (nonatomic, strong) VideoCommentListModel *commentListModel;
@property (nonatomic, strong) SendCommentController *sendCommentController;

@end

@implementation VideoCommentListController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Video"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.userIconImgView.layer.cornerRadius = 20;
    self.userIconImgView.layer.masksToBounds = YES;
    [self.userIconImgView setImageWithPhoto:[UserModel sharedInstance].user.avatar placeholderImage:[AppTheme avatarDefaultImage]];
    
    [self.tableView registerNib:NSStringFromClass([VideoCommentCell class])];
    
    [self initData];
}

- (void)initData {
    self.commentListModel = [[VideoCommentListModel alloc] init];
    self.commentListModel.video_id = self.video_id;
    @weakify(self)
    self.commentListModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        [self.tableView reloadData];
        self.commentTotalLabel.text = [NSString stringWithFormat:@"全部评论(%ld)", self.commentListModel.total];
    };
    
    [self.commentListModel refresh];
}

- (IBAction)sendCommendAction:(id)sender {
    [UIView animateWithDuration:0.25 animations:^{
        self.sendCommentController.view.frame = self.view.bounds;
    }];
}

- (IBAction)closeAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(closeSelf)]) {
        [self.delegate closeSelf];
    }
}

- (SendCommentController *)sendCommentController {
    if (!_sendCommentController) {
        _sendCommentController = [SendCommentController spawn];
        [self addChildViewController:_sendCommentController];
        _sendCommentController.delegate = self;
        _sendCommentController.video_id = self.video_id;
        _sendCommentController.view.frame = CGRectMake(0, self.view.height, self.view.width, self.view.height);
        [self.view addSubview:_sendCommentController.view];
    }
    return _sendCommentController;
}

#pragma mark - SendCommentControllerDelegate

- (void)onClose {
    [UIView animateWithDuration:0.25 animations:^{
        self.sendCommentController.view.frame = CGRectMake(0, self.view.height, self.view.width, self.view.height);
    } completion:^(BOOL finished) {
        [self.sendCommentController.view removeFromSuperview];
        [self.sendCommentController removeFromParentViewController];
        self.sendCommentController = nil;
    }];
}

- (void)onSendCommentSuccess {
    [self.view presentMessageTips:@"评论发表成功"];
    [self onClose];
    [self initData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.commentListModel.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    VideoCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([VideoCommentCell class]) forIndexPath:indexPath];
    COMMENT *comment = self.commentListModel.items[indexPath.row];
    cell.comment = comment;
    [cell hideBottomLine:indexPath.row != self.commentListModel.items.count - 1];
    
    @weakify(self)
    cell.onReply = ^(REPLY *reply) {
      @strongify(self)
        // 回复回复
        [self commentSelectAction:reply];
    };
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    COMMENT *comment = self.commentListModel.items[indexPath.row];
    return [VideoCommentCell rowHeightWithComment:comment];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    [self commentSelectAction:self.commentListModel.items[indexPath.row]];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (self.commentListModel.loaded && self.commentListModel.items.count == 0) {
        return tableView.height;
    }
    return 0.0001;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (self.commentListModel.loaded && self.commentListModel.items.count == 0) {
        return [EmptyTableCell loadFromNib];
    }
    return nil;
}

#pragma mark - Action

- (void)parentControllerRotate {
    [self.reportAlertView hide];
    [self.reportTypeAlertView hide];
    [self.blackUserAlertView hide];
}

- (void)commentSelectAction:(id)commentData {
    USER *owner;
    if ([commentData isKindOfClass:[COMMENT class]]) {
        COMMENT *comment = (COMMENT *)commentData;
        owner = comment.owner;
    } else {
        REPLY *reply = (REPLY *)commentData;
        owner = reply.owner;
    }
    
    GKZActionSheet *actionSheet = [GKZActionSheet loadFromNib];
    AlertView * alertView = [[AlertView alloc] initWithContent:actionSheet type:AlertViewTypeMonospaced];
    self.reportAlertView = alertView;
    if ([owner.id isEqualToString:[UserModel sharedInstance].user.id]) {
        actionSheet.data = @[@"回复"];
    } else {
        actionSheet.data = @[@"回复", @"举报", @"拉黑"];
    }
    [alertView showSharedView];
    
    actionSheet.whenHide = ^(BOOL hide) {
        [alertView hide];
    };
    actionSheet.whenRegistered = ^(id data, NSInteger index) {
        [alertView hide];
        if (index == 0) {
            self.sendCommentController.additional = commentData;
            [self sendCommendAction:nil];
        } else if (index == 1) {
            GKZActionSheet *actionSheet = [GKZActionSheet loadFromNib];
            self.reportTypeAlertView = [[AlertView alloc] initWithContent:actionSheet type:AlertViewTypeMonospaced];
            actionSheet.data = @[@"垃圾广告营销", @"恶意攻击谩骂", @"淫秽色情信息", @"违法有害信息", @"其它不良信息"];
            
            actionSheet.whenHide = ^(BOOL hide) {
                [self.reportTypeAlertView hide];
            };
            actionSheet.whenRegistered = ^(id data, NSInteger index) {
                [self.reportTypeAlertView hide];
                REPORT_TYPE type = index + 1;
                NSString *commentId;
                if ([commentData isKindOfClass:[COMMENT class]]) {
                    COMMENT *comment = (COMMENT *)commentData;
                    commentId = comment.id;
                } else {
                    REPLY *reply = (REPLY *)commentData;
                    commentId = reply.id;
                }
                [VideoCommentModel reportComment:commentId reason:type then:^(STIHTTPResponseError *error) {
                    if (error == nil) {
                        [self presentMessageTips:@"您的举报我们会在24小时内进行处理"];
                    } else {
                        [self presentMessage:error.message withTips:@"数据获取失败"];
                    }
                }];
            };
            
            [self.reportTypeAlertView showSharedView];
        } else if (index == 2) {
            MPAlertView *blackUserAlertView = [MPAlertView alertWithTitle:@"确认要将该用户加入黑名单吗？" message:@"您将不会在评论区看到该用户的相关信息" cancelButton:@"取消" confirmButton:@"确定"];
            self.blackUserAlertView = blackUserAlertView;
            blackUserAlertView.confirmAction = ^{
                [VideoCommentModel setCommentUserToBlackList:owner then:^(STIHTTPResponseError *error) {
                    if (error == nil) {
                        [self presentMessageTips:@"加入黑名单成功"];
                        [self.commentListModel refresh];
                    } else {
                        [self presentMessage:error.message withTips:@"数据获取失败"];
                    }
                }];
            };
            [blackUserAlertView show];
        }
    };
}

@end
