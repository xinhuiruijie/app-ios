//
//  HomeVideoListModel.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "HomeVideoListModel.h"

@implementation HomeVideoListModel

- (void)loadCache {
    NSArray *items = [self loadCacheWithKey:self.cacheKey objectClass:[VIDEO class]];
    
    [self.items removeAllObjects];
    if (items.count) {
        [self.items addObjectsFromArray:items];
    }
}

- (void)saveCache {
    [self saveCache:self.items key:self.cacheKey];
}

- (void)refresh {
    [self fetchForFirstTime:YES];
}

- (void)loadMore {
    [self fetchForFirstTime:NO];
}

- (void)fetchForFirstTime:(BOOL)isFirstTime {
    if ( isFirstTime ) {
        self.currentPage = 1;
    } else {
        self.currentPage += 1;
    }
    
    V1_API_HOME_VIDEO_LIST_API * api = [[V1_API_HOME_VIDEO_LIST_API alloc] init];
    api.req.page = @(self.currentPage);
    api.req.per_page = @(20);
    
    api.whenUpdated = ^(V1_API_HOME_VIDEO_LIST_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        self.loaded = YES;
        if ( error ) {
            PERFORM_BLOCK_SAFELY(self.whenUpdated, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                
                // 后台会自己做更新数量的判断。此处为了避免后台没有返回的情况
                NSInteger updateCount = response.videos.count;
                for (int i = 0; i < response.videos.count; i++) {
                    for (int j = 0; j < self.items.count; j++) {
                        VIDEO *latestVideo = response.videos[i];
                        VIDEO *cacheVideo = self.items[j];
                        if ([latestVideo.id isEqualToString:cacheVideo.id]) {
                            updateCount--;
                            continue;
                        }
                    }
                }
                self.updateVideoCount = [NSString stringWithFormat:@"更新了%ld条短片", (long)updateCount];
                
                if (isFirstTime) {
                    [self.items removeAllObjects];
                }
                [self.items addObjectsFromArray:response.videos];
                [self saveCache];
                self.more = response.paged.more.boolValue;
                
                [self saveLastRequestTime];
                
                PERFORM_BLOCK_SAFELY(self.whenUpdated, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(self.whenUpdated, errors);
            }
        }
    };
    
    [api send];
}

- (void)saveLastRequestTime {
    NSString *lastRequestTime = [NSString stringWithFormat:@"%ld", (long)[[NSDate date] timeIntervalSince1970]];
    [self saveCache:lastRequestTime key:@"lastRequestTime"];
}
- (NSString *)lastRequestTime {
    return [self loadCacheWithKey:@"lastRequestTime" objectClass:[NSString class]];
}
// 首页刷新特定提示文案
- (void)updateVideoCountCompleted:(void(^)(STIHTTPResponseError *error))completed {
    V1_API_HOME_COPYWRITING_REFRESHEN_API *api = [[V1_API_HOME_COPYWRITING_REFRESHEN_API alloc] init];
    api.req.last_request = [self lastRequestTime];
    
    api.whenUpdated = ^(V1_API_HOME_COPYWRITING_REFRESHEN_RESPONSE *resp, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if ( error ) {
            PERFORM_BLOCK_SAFELY(completed, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                self.copywriting = resp.copywriting;
                PERFORM_BLOCK_SAFELY(completed, nil);
            } else {
                STIHTTPResponseError * err = [[STIHTTPResponseError alloc] init];
                err.code = X_MPlanet_ErrorCode(allHeaders);
                err.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(completed, err);
            }
        }
    };
    [api send];
}

@end
