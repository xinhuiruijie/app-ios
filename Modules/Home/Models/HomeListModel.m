//
//  HomeListModel.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "HomeListModel.h"

@implementation HomeListModel

- (instancetype)init {
    self = [super init];
    if (self) {
        self.cards = [NSMutableArray array];
        self.topics = [NSMutableArray array];
    }
    return self;
}

- (void)loadCache {
    NSArray *cardsItems = [self loadCacheWithKey:self.cardCacheKey objectClass:[CARD class]];
    NSArray *topicItems = [self loadCacheWithKey:self.topicCacheKey objectClass:[TOPIC class]];
    
    [self.cards removeAllObjects];
    [self.topics removeAllObjects];
    
    if (cardsItems.count) {
        [self.cards addObjectsFromArray:cardsItems];
    }
    if (topicItems.count) {
        [self.topics addObjectsFromArray:topicItems];
    }
}

- (void)saveCache {
    [self saveCache:self.cards key:self.cardCacheKey];
    [self saveCache:self.topics key:self.topicCacheKey];
}

- (NSString *)cardCacheKey {
    return [NSString stringWithFormat:@"%@-cardCacheKey",[[self class] description]];
}

- (NSString *)topicCacheKey {
    return [NSString stringWithFormat:@"%@-topicCacheKey",[[self class] description]];
}

- (void)refresh {
    V1_API_HOME_TOPIC_LIST_API *api = [[V1_API_HOME_TOPIC_LIST_API alloc] init];
    
    api.whenUpdated = ^(V1_API_HOME_TOPIC_LIST_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        self.loaded = YES;
        if ( error ) {
            PERFORM_BLOCK_SAFELY(self.whenUpdated, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                [self.topics removeAllObjects];
                [self.cards removeAllObjects];
                
                [self.topics addObjectsFromArray:response.topics];
//                [self.cards addObjectsFromArray:response.cards];
                [self saveCache];
                PERFORM_BLOCK_SAFELY(self.whenUpdated, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(self.whenUpdated, errors);
            }
        }
    };
    
    [api send];
}

@end
