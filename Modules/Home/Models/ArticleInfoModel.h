//
//  ArticleInfoModel.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "STIOnceModel.h"

@interface ArticleInfoModel : STIOnceModel

@property (nonatomic, strong) NSString * article_id; // 文章ID

@end
