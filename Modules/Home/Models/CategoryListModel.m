//
//  CategoryListModel.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "CategoryListModel.h"

@implementation CategoryListModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        [self loadCache];
    }
    return self;
}

- (void)loadCache {
    NSArray *items = [self loadCacheWithKey:self.cacheKey objectClass:[CATEGORY class]];
    
    [self.items removeAllObjects];
    if (items.count) {
        [self.items addObjectsFromArray:items];
    }
}

- (void)saveCache {
    [self saveCache:self.items key:self.cacheKey];
}

#pragma mark -

- (void)refresh {
    V1_API_CATEGORY_LIST_API * api = [[V1_API_CATEGORY_LIST_API alloc] init];
    
    api.whenUpdated = ^(V1_API_CATEGORY_LIST_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        self.loaded = YES;
        if ( error ) {
            PERFORM_BLOCK_SAFELY(self.whenUpdated, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                // 同步本地的分类排序
                [self computeWithResponse:response.category];
                
                [self saveCache];
                
                PERFORM_BLOCK_SAFELY(self.whenUpdated, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(self.whenUpdated, errors);
            }
        }
    };
    
    [api send];
}

//同步本地的分类排序
- (void)computeWithResponse:(NSArray *)categories {
    if ( self.items && self.items.count != 0 ) {
        NSMutableArray *tempArray = [NSMutableArray array];
        for ( CATEGORY *cacheModel in self.items ) {
            for ( CATEGORY *responseModel in categories ) {
                if ( [cacheModel.id isEqualToString:responseModel.id] ) {
                    [tempArray addObject:responseModel];
                    break;
                }
            }
        }
        if ( tempArray.count != categories.count ) {
            for ( CATEGORY *responseModel in categories ) {
                BOOL isNot = YES;
                for ( CATEGORY *cacheModel in tempArray ) {
                    if ( [responseModel.id isEqualToString:cacheModel.id] ) {
                        isNot = NO;
                        break;
                    }
                }
                
                if( isNot ) {
                    [tempArray addObject:responseModel];
                }
            }
        }
        
        self.items = [NSMutableArray arrayWithArray:tempArray];
    } else {
        self.items = [NSMutableArray arrayWithArray:categories];
    }
}

@end
