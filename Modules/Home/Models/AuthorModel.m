//
//  AuthorModel.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "AuthorModel.h"

NSString * const kAuthorFollowNotification = @"kAuthorFollowNotification";
NSString * const kAuthorUnfollowNotification = @"kAuthorUnfollowNotification";

@implementation AuthorModel

+ (void)follow:(USER *)user then:(void (^)(STIHTTPResponseError *))then {
    V1_API_AUTHOR_FOLLOW_API *api = [[V1_API_AUTHOR_FOLLOW_API alloc] init];
    api.req.author_id = user.id;
    api.whenUpdated = ^(V1_API_AUTHOR_FOLLOW_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if ( error ) {
            PERFORM_BLOCK_SAFELY(then, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                user.is_follow = YES;
                [[NSNotificationCenter defaultCenter] postNotificationName:kAuthorFollowNotification object:nil userInfo:@{@"data": user}];
                PERFORM_BLOCK_SAFELY(then, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(then, errors);
            }
        }
    };
    [api send];
}

+ (void)unfollow:(USER *)user then:(void (^)(STIHTTPResponseError *))then {
    V1_API_AUTHOR_UNFOLLOW_API *api = [[V1_API_AUTHOR_UNFOLLOW_API alloc] init];
    api.req.author_id = user.id;
    api.whenUpdated = ^(V1_API_AUTHOR_UNFOLLOW_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if ( error ) {
            PERFORM_BLOCK_SAFELY(then, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                user.is_follow = NO;
                [[NSNotificationCenter defaultCenter] postNotificationName:kAuthorUnfollowNotification object:nil userInfo:@{@"data": user}];
                PERFORM_BLOCK_SAFELY(then, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(then, errors);
            }
        }
    };
    [api send];
}

+ (void)reportAuthor:(NSString *)authorID withType:(REPORT_TYPE)type completed:(void (^)(STIHTTPResponseError *))completed {
    V1_API_AUTHOR_REPORT_API *api = [[V1_API_AUTHOR_REPORT_API alloc] init];
    api.req.author_id = authorID;
    api.req.type = type;
    
    api.whenUpdated = ^(V1_API_AUTHOR_REPORT_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if ( error ) {
            PERFORM_BLOCK_SAFELY(completed, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                PERFORM_BLOCK_SAFELY(completed, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(completed, errors);
            }
        }
    };
    
    [api send];
}

@end
