//
//  KeywordsModel.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "STIStreamModel.h"

@interface KeywordsModel : STIStreamModel

@property (nonatomic, strong) NSMutableArray *historyKeywords;

#pragma mark - History Keywords

- (void)loadHistoryKeywordsCache;
- (void)addKeyword:(NSString *)keyword;
- (void)removeKeywords;

@end
