//
//  ArticleModel.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "ArticleModel.h"

@implementation ArticleModel

+ (void)like:(ARTICLE *)article then:(void (^)(STIHTTPResponseError *))then {
    V1_API_ARTICLE_LIKE_API *api = [[V1_API_ARTICLE_LIKE_API alloc] init];
    api.req.article_id = article.id;
    api.whenUpdated = ^(V1_API_ARTICLE_LIKE_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if ( error ) {
            PERFORM_BLOCK_SAFELY(then, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                article.is_like = YES;
                PERFORM_BLOCK_SAFELY(then, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(then, errors);
            }
        }
    };
    [api send];
}

+ (void)unlike:(ARTICLE *)article then:(void (^)(STIHTTPResponseError *))then {
    V1_API_ARTICLE_UNLIKE_API *api = [[V1_API_ARTICLE_UNLIKE_API alloc] init];
    api.req.article_id = article.id;
    api.whenUpdated = ^(V1_API_ARTICLE_UNLIKE_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if ( error ) {
            PERFORM_BLOCK_SAFELY(then, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                article.is_like = NO;
                PERFORM_BLOCK_SAFELY(then, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(then, errors);
            }
        }
    };
    [api send];
}

+ (void)share:(ARTICLE *)article then:(void (^)(STIHTTPResponseError *))then {
    V1_API_ARTICLE_SHARE_API *api = [[V1_API_ARTICLE_SHARE_API alloc] init];
    api.req.article_id = article.id;
    api.whenUpdated = ^(V1_API_ARTICLE_SHARE_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if ( error ) {
            PERFORM_BLOCK_SAFELY(then, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                PERFORM_BLOCK_SAFELY(then, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(then, errors);
            }
        }
    };
    [api send];
}

@end
