//
//  ArticleModel.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "STIWriteModel.h"

@interface ArticleModel : STIWriteModel

+ (void)like:(ARTICLE *)article then:(void (^)(STIHTTPResponseError *))then;
+ (void)unlike:(ARTICLE *)article then:(void (^)(STIHTTPResponseError *))then;
+ (void)share:(ARTICLE *)article then:(void (^)(STIHTTPResponseError *))then;

@end
