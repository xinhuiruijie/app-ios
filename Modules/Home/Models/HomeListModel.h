//
//  HomeListModel.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "STIOnceModel.h"

@interface HomeListModel : STIOnceModel

@property (nonatomic, strong) NSMutableArray * cards; // 卡片
@property (nonatomic, strong) NSMutableArray * topics; // 短片话题

@end
