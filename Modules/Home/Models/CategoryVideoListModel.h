//
//  CategoryVideoListModel.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "STIStreamModel.h"

@interface CategoryVideoListModel : STIStreamModel
@property (nonatomic, strong) NSMutableArray<VIDEO *> *hotVideos; // 精选头条（前三条）
@property (nonatomic, strong) NSMutableArray<VIDEO *> *videos; // 其他短片（不包含精选头条）

@property (nonatomic, strong) NSString *category_id;

@end
