//
//  HomeVideoListModel.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "STIStreamModel.h"

@interface HomeVideoListModel : STIStreamModel

@property (nonatomic, strong) NSString *updateVideoCount; // 更新了几条短片
@property (nonatomic, strong) NSString *copywriting; // 首页刷新特定提示文案
- (void)updateVideoCountCompleted:(void(^)(STIHTTPResponseError *error))completed;

@end
