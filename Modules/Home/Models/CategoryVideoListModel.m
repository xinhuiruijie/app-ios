//
//  CategoryVideoListModel.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "CategoryVideoListModel.h"

@implementation CategoryVideoListModel

- (instancetype)init
{
    self = [super init];
    if (self) {
        _hotVideos = [NSMutableArray array];
        _videos = [NSMutableArray array];
        
//        [self loadCache];
    }
    return self;
}

- (void)loadCache {
    NSArray *hotVideos = [self loadCacheWithKey:[self hotVideosCacheKey] objectClass:[VIDEO class]];
    NSArray *videos = [self loadCacheWithKey:[self videosCacheKey] objectClass:[VIDEO class]];
    
    [_hotVideos removeAllObjects];
    [_videos removeAllObjects];
    
    if (hotVideos.count) {
        [_hotVideos addObjectsFromArray:hotVideos];
    }
    if (videos.count) {
        [_videos addObjectsFromArray:videos];
    }
}

- (void)saveCache {
    [self saveCache:_hotVideos key:[self hotVideosCacheKey]];
    [self saveCache:_videos key:[self videosCacheKey]];
}

- (NSString *)cacheKey {
    return [NSString stringWithFormat:@"%@-%@", [[self class] description], self.category_id];
}

- (NSString *)hotVideosCacheKey {
    return [NSString stringWithFormat:@"%@-hotVideos", self.cacheKey];
}

- (NSString *)videosCacheKey {
    return [NSString stringWithFormat:@"%@-videos", self.cacheKey];
}

- (BOOL)isEmpty {
    return (!_hotVideos || 0 == _hotVideos.count) && (!_videos || 0 == _videos.count);
}

#pragma mark -

- (void)refresh {
    [self fetchForFirstTime:YES];
}

- (void)loadMore {
    [self fetchForFirstTime:NO];
}

- (void)fetchForFirstTime:(BOOL)isFirstTime {
    if ( isFirstTime ) {
        self.currentPage = 1;
    } else {
        self.currentPage += 1;
    }
    
    V1_API_VIDEO_CATEGORY_LIST_API * api = [[V1_API_VIDEO_CATEGORY_LIST_API alloc] init];
    api.req.category_id = self.category_id;
    api.req.page = @(self.currentPage);
    api.req.per_page = @(10);
    
    api.whenUpdated = ^(V1_API_VIDEO_CATEGORY_LIST_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        self.loaded = YES;
        if ( error ) {
            PERFORM_BLOCK_SAFELY(self.whenUpdated, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                if (isFirstTime) {
                    [_hotVideos removeAllObjects];
                    [_videos removeAllObjects];
                }
                [_hotVideos addObjectsFromArray:response.hot_videos];
                [_videos addObjectsFromArray:response.videos];
                
                [self saveCache];
                self.more = response.paged.more.boolValue;
                
                PERFORM_BLOCK_SAFELY(self.whenUpdated, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(self.whenUpdated, errors);
            }
        }
    };
    
    [api send];
}

@end
