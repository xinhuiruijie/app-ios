//
//  BannerListModel.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "BannerListModel.h"

@implementation BannerListModel

- (void)loadCache {
    NSArray *items = [self loadCacheWithKey:self.cacheKey objectClass:[BANNER class]];
    
    [self.items removeAllObjects];
    if (items.count) {
        [self.items addObjectsFromArray:items];
    }
}

- (void)saveCache {
    [self saveCache:self.items key:self.cacheKey];
}

- (NSString *)cacheKey {
    return [NSString stringWithFormat:@"%@-%@",[[self class] description], @(self.banner_type)];
}

- (void)refresh {
    V1_API_BANNER_LIST_API * api = [[V1_API_BANNER_LIST_API alloc] init];
    
    api.req.banner_type = self.banner_type;
    
    api.whenUpdated = ^(V1_API_BANNER_LIST_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        self.loaded = YES;
        if ( error ) {
            PERFORM_BLOCK_SAFELY(self.whenUpdated, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                [self.items removeAllObjects];
                [self.items addObjectsFromArray:response.banners];
                [self saveCache];
                PERFORM_BLOCK_SAFELY(self.whenUpdated, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(self.whenUpdated, errors);
            }
        }
    };
    
    [api send];
}

@end
