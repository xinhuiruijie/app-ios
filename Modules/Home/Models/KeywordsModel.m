//
//  KeywordsModel.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "KeywordsModel.h"

@implementation KeywordsModel

- (id)init {
    if (self = [super init]) {
        self.historyKeywords = [NSMutableArray array];
    }
    return self;
}

#pragma mark - History Keywords Cache

- (void)loadHistoryKeywordsCache {
    NSArray *items = [self loadCacheWithKey:[self historyKeywordsCacheKey] objectClass:[NSString class]];
    
    [self.historyKeywords removeAllObjects];
    if ( items.count ) {
        [self.historyKeywords addObjectsFromArray:items];
    }
}

- (void)saveHistoryKeywordsCache {
    [self saveCache:self.historyKeywords key:[self historyKeywordsCacheKey]];
}

- (void)clearHistoryKeywordsCache {
    [self clearCacheWithKey:[self historyKeywordsCacheKey]];
}

- (NSString *)historyKeywordsCacheKey {
    return [NSString stringWithFormat:@"%@-historyKeywords",[[self class] description]];
}

#pragma mark - History Keywords

- (void)addKeyword:(NSString *)keyword {
    if (keyword == nil)
        return;
    
    if (self.historyKeywords) {
        if ([self.historyKeywords containsObject:keyword]) {
            [self.historyKeywords removeObject:keyword];
        }
        [self.historyKeywords insertObject:keyword atIndex:0];
    }
    
    if (self.historyKeywords.count > 10) {
        NSMutableArray *keywords = [NSMutableArray arrayWithArray:[self.historyKeywords subarrayWithRange:NSMakeRange(0, 10)]];
        self.historyKeywords = keywords;
    }
    
    [self saveHistoryKeywordsCache];
}

- (void)removeKeywords {
    if (self.historyKeywords) {
        [self.historyKeywords removeAllObjects];
    }
    
    [self saveHistoryKeywordsCache];
}

@end
