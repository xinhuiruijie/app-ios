//
//  ArticleInfoModel.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "ArticleInfoModel.h"

@implementation ArticleInfoModel

- (NSString *)cacheKey {
    return [NSString stringWithFormat:@"%@-%@", [[self class] description], self.article_id];
}

- (void)refresh {
    V1_API_ARTICLE_GET_API *api = [[V1_API_ARTICLE_GET_API alloc] init];
    api.req.article_id = self.article_id;
    
    api.whenUpdated = ^(V1_API_ARTICLE_GET_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        self.loaded = YES;
        if ( error ) {
            PERFORM_BLOCK_SAFELY(self.whenUpdated, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                self.item = response.article;
                [self saveCache];
                PERFORM_BLOCK_SAFELY(self.whenUpdated, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(self.whenUpdated, errors);
            }
        }
    };
    
    [api send];
}

@end
