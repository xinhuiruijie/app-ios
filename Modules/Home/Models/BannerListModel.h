//
//  BannerListModel.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "STIStreamModel.h"

@interface BannerListModel : STIStreamModel

@property (nonatomic, assign) BANNER_TYPE banner_type; // banner类型（首页banner和热议banner）

@end
