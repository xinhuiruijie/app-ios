//
//  AuthorInfoModel.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "STIOnceModel.h"

@interface AuthorInfoModel : STIOnceModel

@property (nonatomic, strong) NSString *author_id;  // 作者id

@end
