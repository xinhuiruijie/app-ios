//
//  AuthorModel.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "STIWriteModel.h"

FOUNDATION_EXPORT NSString * const kAuthorFollowNotification;
FOUNDATION_EXPORT NSString * const kAuthorUnfollowNotification;

@interface AuthorModel : STIWriteModel

+ (void)follow:(USER *)user then:(void (^)(STIHTTPResponseError *))then;
+ (void)unfollow:(USER *)user then:(void (^)(STIHTTPResponseError *))then;
+ (void)reportAuthor:(NSString *)authorID withType:(REPORT_TYPE)type completed:(void (^)(STIHTTPResponseError *))completed;

@end
