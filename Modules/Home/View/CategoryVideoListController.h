//
//  CategoryVideoListController.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "ListViewController.h"
#import "HomePageItemViewControllerProtocol.h"

@interface CategoryVideoListController : ListViewController <HomePageItemViewControllerProtocol>
@property (nonatomic, strong) CATEGORY *category;
@end
