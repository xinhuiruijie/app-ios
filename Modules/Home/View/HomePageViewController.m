//
//  HomePageViewController.m
//  MotherPlanet
//
//  Created by GeekZooStudio on 2018/4/10.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "HomePageViewController.h"
#import "HomeViewController.h"
#import "ArticleListController.h"
#import "CategoryVideoListController.h"
#import "CategoryTaxisListController.h"
#import "AudioViewController.h"
#import "SearchViewController.h"
#import "SearchTabController.h"

#import "ZLBlurView.h"
#import "AudioMiniView.h"
#import "DateAnimatedView.h"

#import "AudioListModel.h"
#import "CategoryListModel.h"

#import <UMCommon/UMCommon.h>
#import <UMAnalytics/MobClick.h>

@interface HomePageViewController ()
@property (nonatomic, strong) CategoryListModel *categoryModel;
@property (nonatomic, strong) AudioListModel *audioModel;

@property (nonatomic, strong) DateAnimatedView *dateAnimatedView;
@property (nonatomic, strong) PlayingLineAnimatedView *playingLineAnimated;

@property (nonatomic, strong) HomeViewController *home;
@property (nonatomic, strong) ArticleListController *article;

@property (nonatomic, strong) UIView *topBarView;
@property (nonatomic, strong) AudioMiniView *audioMiniView;
@property (nonatomic, assign) CGFloat audioMiniViewHeight;
@property (nonatomic, strong) UIButton *searchItem;
@end

@implementation HomePageViewController

- (instancetype)init {
    if (self = [super init]) {
        self.menuViewStyle = WMMenuViewStyleFlood;
        self.progressViewBottomSpace = 10;
        self.progressHeight = 24;
        self.progressViewCornerRadius = 2;
        self.progressColor = [AppTheme normalTextColor];
        self.titleFontName = @"PingFangSC-Medium";
    }
    return self;
}

- (void)reloadData {
    [super reloadData];
    [self.view insertSubview:_audioMiniView belowSubview:self.topBarView];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver: self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
//    self.navigationItem.leftBarButtonItem = [self dateAnimatedViewItem];
//    self.navigationItem.rightBarButtonItem = [self fmRightItem];
//    self.navigationItem.titleView = [self fakerTitleView];
    
    [self modelInit];
    [self customize];
    
    // Splash结束后调用开始动画
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subViewStartAnimation) name:APP_SPLASH_HIDE object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopLaunchAnimating) name:APP_LAUNCH_ANIMATION_HIDE object:nil];
    // 监听电台播放状态改变通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(audioPlayerStatusChangedNotification:) name:AudioPlayManagerStatusChangedNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onTabBarDoubleClick:) name:UITabBarItemDoubleClickNotification object:nil];
    
    [self.dateAnimatedView startAnimation];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    if (self.audioMiniView) {
        [self.audioMiniView checkPlayStatus];
        [self.audioMiniView resetDelegate];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
//    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    [self.view addSubview:self.topBarView];
}

- (void)customize {
    self.menuView.backgroundColor = [UIColor clearColor];
    
    ZLBlurView *blurView = [[ZLBlurView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 44)];
    [self.menuView insertSubview:blurView atIndex:0];
    
    UIButton *categotyBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    categotyBtn.frame = CGRectMake(0, 0, 44, 44);
    [categotyBtn setImage:[UIImage imageNamed:@"icon_home_cla"] forState:UIControlStateNormal];
    [categotyBtn addTarget:self action:@selector(gotoCategoryList) forControlEvents:UIControlEventTouchUpInside];
    self.menuView.leftView = categotyBtn;
    
    [self setupTopView];
}

- (void)onTabBarDoubleClick:(NSNotification *)noti {
    // tabbar双击时刷新
    if ([noti.object isKindOfClass:[MPRootViewController class]]) {
        if ([self.navigationController isEqual:((MPRootViewController *)noti.object).selectedViewController]) {
            if ([self.currentViewController isEqual:self.home]) {
                if (![self.home getListView].header.isRefreshing) {
                    [[self.home getListView].header beginRefreshing];
                }
            }
        }
    }
}

- (void)setupTopView {
    if (!self.topBarView) {
        self.topBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, TopBarHeight)];
        self.topBarView.backgroundColor = [UIColor clearColor];
        
        ZLBlurView *blurView = [[ZLBlurView alloc] initWithFrame:self.topBarView.bounds];
        [self.topBarView addSubview:blurView];
        
        // title view
        UIView *titleView = [self fakerTitleView];
        titleView.center = CGPointMake(self.topBarView.width / 2.0, StatusBarHeight + NavigationBarHeight / 2.0);
        [self.topBarView addSubview:titleView];
        
        // left item
        UIView *leftView = [self dateAnimatedViewItem].customView;
        leftView.frame = CGRectMake(10, StatusBarHeight + (NavigationBarHeight - 20) / 2.0, 60, 20);
        [self.topBarView addSubview:leftView];
        
        // right item
//        UIView *rightView = [self fmRightItem].customView;
        UIView *rightView = [self searchNormalItem].customView;
        rightView.frame = CGRectMake(kScreenWidth - 10 - 44, StatusBarHeight, 44, NavigationBarHeight);
        
        // 爱音斯坦 播放状态动画
//        self.playingLineAnimated = [[PlayingLineAnimatedView alloc] initWithFrame:CGRectMake(0, 0, 24, 24) lineWidth:2 lineColor:[AppTheme normalTextColor]];
//        self.playingLineAnimated.center = CGPointMake(rightView.width / 2.0, rightView.height / 2.0);
//        [rightView addSubview:self.playingLineAnimated];
//        self.playingLineAnimated.userInteractionEnabled = NO;
        
        [self.topBarView addSubview:rightView];
    }
}

- (void)modelInit {
    self.categoryModel = [[CategoryListModel alloc] init];
    @weakify(self)
    self.categoryModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        if (error) {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
        [self reloadData];
    };
    [self.categoryModel loadCache];
    [self reloadData];
    [self.categoryModel refresh];
    
    // 电台
    self.audioModel = [[AudioListModel alloc] init];
//    self.audioModel.whenUpdated = ^(STIHTTPResponseError *error) {
//        @strongify(self)
//    };
    [self.audioModel refresh];
}

#pragma mark - notification

// 监听电台播放状态改变通知
- (void)audioPlayerStatusChangedNotification:(NSNotification *)noti {
    if ([noti.object isEqual:[AudioPlayManager sharedInstance]]) {
        AudioPlayerStatus status = [AudioPlayManager sharedInstance].status;
        
        if (status == AudioPlayerStatusRestartToPlay) {
            [self.playingLineAnimated addAnimation];
        } else {
            [self.playingLineAnimated removeAnimation];
        }
    }
}

- (void)subViewStartAnimation {
    [self.dateAnimatedView startAnimation];
}

- (void)stopLaunchAnimating {
    [self.dateAnimatedView startAnimation];
}

#pragma mark - UIBarButtonItems

- (UIBarButtonItem *)dateAnimatedViewItem {
    DateAnimatedView *dateView = [[DateAnimatedView alloc] initWithFrame:CGRectMake(0, 0, 60, 20)];
    self.dateAnimatedView = dateView;
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:dateView];
    return leftItem;
}

- (UIBarButtonItem *)fmRightItem {
//    UIButton *fmButton = [AppTheme navigationItemCustomView:[UIImage imageNamed:@"icon_fm_nor_home"]];
    UIButton *fmButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [fmButton addTarget:self action:@selector(fmButtonAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *fmItem = [[UIBarButtonItem alloc] initWithCustomView:fmButton];
    return fmItem;
}

- (UIBarButtonItem *)searchNormalItem {
    UIButton *searchButton = [AppTheme navigationItemCustomView:[UIImage imageNamed:@"a1_icon_search"]];
    self.searchItem = searchButton;
    [searchButton addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:searchButton];
    return searchItem;
}

- (void)searchAction {
    SearchTabController *searchView = [SearchTabController spawn];
    searchView.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:searchView animated:NO];
}

- (UIView *)fakerTitleView {
    UIImageView *imgView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_motherplanet"]];
    return imgView;
}

#pragma mark - action

- (void)fmButtonAction {
    if (!self.audioMiniView) {
        self.audioMiniView = [AudioMiniView loadFromNibWithFrame:CGRectMake(0, 0, kScreenWidth, 60)];
        self.audioMiniView.hidden = YES;
        [self.view insertSubview:self.audioMiniView belowSubview:self.topBarView];
        @weakify(self)
        self.audioMiniView.onBack = ^{
            @strongify(self)
            [self fmButtonAction];
        };
    }
    
    if (self.audioMiniView.hidden) {
        [self showAudioMiniView];
        self.audioMiniView.datas = self.audioModel.items.copy;
    } else {
        [self hideAudioMiniView];
    }

    // 列表视图内容要同步位置
    UIViewController<HomePageItemViewControllerProtocol> *vc =  (UIViewController<HomePageItemViewControllerProtocol> *)self.currentViewController;
    UIScrollView *listView = [vc getListView];

    CGFloat offsetY = listView.contentOffset.y - (self.audioMiniViewHeight == 0 ? -60 : 60);
    CGFloat insetTop = self.topBarView.bottom + self.audioMiniViewHeight + 44 - StatusBarHeight;
    [UIView animateWithDuration:0.25 animations:^{
        listView.contentOffset = CGPointMake(0, offsetY);
    } completion:^(BOOL finished) {
        listView.contentInsets_top = insetTop;
    }];
}

- (void)showAudioMiniView {
    self.audioMiniViewHeight = 60;
    self.audioMiniView.hidden = NO;
    
    [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.audioMiniView.y = self.topBarView.bottom;
    } completion:nil];
    
    [UIView animateWithDuration:0.25 delay:0.03 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self forceLayoutSubviews];
    } completion:nil];
}

- (void)hideAudioMiniView {
    self.audioMiniViewHeight = 0;
    
    [UIView animateWithDuration:0.25 delay:0.03 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.audioMiniView.y = 0;
    } completion:^(BOOL finished) {
        self.audioMiniView.hidden = YES;
    }];
    
    [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        [self forceLayoutSubviews];
    } completion:nil];
}

- (void)gotoCategoryList {
    CategoryTaxisListController *categoryTaxisList = [CategoryTaxisListController spawn];
    categoryTaxisList.hidesBottomBarWhenPushed = YES;
    categoryTaxisList.dataSource = self.categoryModel.items.copy;
    @weakify(self)
    categoryTaxisList.didTaxis = ^(NSArray *newDataSource) {
        @strongify(self)
        if (newDataSource) {
            // 先记录交换之前选中的分类, 前两个固定不可移动，所以忽略
            CATEGORY *last = nil;
            if (self.selectIndex >= 2) {
                last = [self.categoryModel.items objectAtIndex:self.selectIndex - 2];
            }
            
            // 保存交换之后的数据顺序，刷新列表
            self.categoryModel.items = [NSMutableArray arrayWithArray:newDataSource];
            [self.categoryModel saveCache];
            
            [self reloadData];
            
            // 重新排序后回来刷新列表，之前选择的是哪个分类，还要选中哪个分类
            if (last) {
                self.selectIndex = (int)[self.categoryModel.items indexOfObject:last] + 2;
            }
        }
    };
    
    categoryTaxisList.didSelected = ^(NSInteger index) {
        @strongify(self)
        self.selectIndex = (int)index + 2;
    };
    
    [self.navigationController pushViewController:categoryTaxisList animated:YES];
}

#pragma mark - WMPageControllerDataSource

- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    return 1 + self.categoryModel.items.count;
}

- (NSString *)pageController:(WMPageController *)pageController titleAtIndex:(NSInteger)index {
    NSString *title = @"";
    switch (index) {
        case 0:
            title = @"All";
            break;
//        case 1:
//            title = @"日报";
//            break;
        default:
        {
            CATEGORY *category = self.categoryModel.items[index - 1];
            title = category.title;
        }
            break;
    }
    return title;
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    UIViewController<HomePageItemViewControllerProtocol> *viewController = nil;
    switch (index) {
        case 0:
        {
            if (!self.home) {
                self.home = [HomeViewController spawn];
            }
            viewController = self.home;
        }
            break;
//        case 1:
//        {
//            if (!self.article) {
//                self.article = [ArticleListController spawn];
//            }
//            viewController = self.article;
//        }
//            break;
        default:
        {
            CATEGORY *category = self.categoryModel.items[index - 1];
            if ([category.title isEqualToString:@"日报"]) {
                // 日报
                if (!self.article) {
                    self.article = [ArticleListController spawn];
                }
                viewController = self.article;
            } else {
                CategoryVideoListController *pageItemVC = [CategoryVideoListController spawn];
                pageItemVC.category = category;
                viewController = pageItemVC;
            }
            
//            CategoryVideoListController *pageItemVC = [CategoryVideoListController spawn];
//            pageItemVC.category = self.categoryModel.items[index - 2];
//            viewController = pageItemVC;
        }
            break;
    }
    
    CGFloat insetTop = self.topBarView.bottom + self.audioMiniViewHeight + 44 - StatusBarHeight;
    if ([viewController respondsToSelector:@selector(setListViewInsetTop:)]) {
        [viewController setListViewInsetTop:insetTop];
    }
    return viewController;
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView {
    return CGRectMake(0, self.topBarView.bottom + self.audioMiniViewHeight, self.view.width, 44);
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView {
    return CGRectMake(0, 0, self.view.width, self.view.height);
}

- (void)pageController:(WMPageController *)pageController willEnterViewController:(__kindof UIViewController *)viewController withInfo:(NSDictionary *)info {
    
    UIViewController<HomePageItemViewControllerProtocol> *vc = (UIViewController<HomePageItemViewControllerProtocol> *)viewController;
    UIScrollView *listView = [vc getListView];
    
//    设置contentInsets
    CGFloat insetTop = self.topBarView.bottom + self.audioMiniViewHeight + 44 - StatusBarHeight;
    listView.contentInsets_top = insetTop;
    
    // 每次滑动切换vc时都会调用，控制时机设置contentOffset
    if (insetTop != -listView.contentOffset.y) {
        CGFloat offsetY = listView.contentOffset.y - (self.audioMiniViewHeight == 0 ? -60 : 60);
        listView.contentOffset = CGPointMake(0, offsetY);
    }
}

#pragma mark - WMMenuViewDataSource

- (WMMenuItem *)menuView:(WMMenuView *)menu initialMenuItem:(WMMenuItem *)initialMenuItem atIndex:(NSInteger)index {
    menu.fontName = @"PingFangSC-Semibold";
    initialMenuItem.normalColor = [AppTheme normalTextColor];
    initialMenuItem.selectedColor = [UIColor whiteColor];
    return initialMenuItem;
}

#pragma mark - WMMenuViewDelegate

- (CGFloat)menuView:(WMMenuView *)menu widthForItemAtIndex:(NSInteger)index {
    CGFloat width = 0;
    switch (index) {
        case 0:
            width = [@"All" widthWithheight:24 font:16] + 8 * 2;
            break;
//        case 1:
//            width = [@"日报" widthWithheight:24 font:16] + 8 * 2;
//            break;
        default:
        {
            CATEGORY *category = self.categoryModel.items[index - 1];
            width = [category.title widthWithheight:24 font:16] + 8 * 2;
        }
            break;
    }
    return width;
}

- (CGFloat)menuView:(WMMenuView *)menu itemMarginAtIndex:(NSInteger)index {
    return 10;
}

- (void)menuView:(WMMenuView *)menu didSelesctedIndex:(NSInteger)index currentIndex:(NSInteger)currentIndex {
    [super menuView:menu didSelesctedIndex:index currentIndex:currentIndex];
    NSString *title = [self.dataSource pageController:self titleAtIndex:index];
    [MobClick event:@"click_star_category" attributes:@{@"category":title}];
}

#pragma mark -

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
}

- (BOOL)shouldAutorotate
{
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

@end
