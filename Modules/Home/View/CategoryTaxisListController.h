//
//  CategoryTaxisListController.h
//  MotherPlanet
//
//  Created by GeekZooStudio on 2018/4/10.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CategoryTaxisListController : UIViewController
@property (nonatomic, strong) NSArray *dataSource;
@property (nonatomic, copy) void(^didTaxis)(NSArray *newDataSource);
@property (nonatomic, copy) void(^didSelected)(NSInteger index);

@end
