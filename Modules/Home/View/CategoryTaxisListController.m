//
//  CategoryTaxisListController.m
//  MotherPlanet
//
//  Created by GeekZooStudio on 2018/4/10.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "CategoryTaxisListController.h"
#import "ZLMoveTaxisTableView.h"
#import "CategoryTaxisListCell.h"

@interface CategoryTaxisListController () <ZLMoveTaxisTableViewDataSource, ZLMoveTaxisTableViewDelegate>
@property (nonatomic, strong) ZLMoveTaxisTableView *tableView;
@end

@implementation CategoryTaxisListController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.navigationItem.title = @"分类排序";
    @weakify(self)
    self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [self customize];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tableView.frame = self.view.bounds;
}

- (void)customize {
    self.tableView = [[ZLMoveTaxisTableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    [self.view addSubview:self.tableView];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.rowHeight = 64;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.marginScrollDistance = 64;
    
    [self.tableView registerNib:NSStringFromClass([CategoryTaxisListCell class])];
}

#pragma mark - ZLMoveTaxisTableViewDataSource

// 返回数据源
- (NSArray *)dataSourceInTableView:(ZLMoveTaxisTableView *)tableView {
    return self.dataSource;
}

// 交换后的数据源，需要保存新的数据源
- (void)tableView:(ZLMoveTaxisTableView *)tableView newDataSource:(NSArray *)newDataSource {
    self.dataSource = newDataSource.copy;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CategoryTaxisListCell *cell = [tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CategoryTaxisListCell class]) forIndexPath:indexPath];
    cell.data = self.dataSource[indexPath.row];
    return cell;
}

#pragma mark - ZLMoveTaxisTableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    return; // 甲方需要，不可点击切换
    if (self.didSelected) {
        self.didSelected(indexPath.row);
    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)tableView:(ZLMoveTaxisTableView *)tableview endMoveCellAtIndexPath:(NSIndexPath *)indexPath {
    if (self.didTaxis) {
        self.didTaxis(self.dataSource);
    }
}

@end
