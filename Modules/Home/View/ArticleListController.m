//
//  ArticleListController.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "ArticleListController.h"
#import "ArticleInfoController.h"
#import "ArticleListModel.h"
#import "ArticleListCell.h"

@interface ArticleListController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) ArticleListModel *model;

@property (nonatomic, assign) CGFloat insetTop;
@end

@implementation ArticleListController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Home"];
}

- (UIScrollView *)list {
    return self.tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"日报精选";
    @weakify(self)
    self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    self.tableView.backgroundColor = [AppTheme listBackgroundColor];
    [self.tableView registerNib:[ArticleListCell nib] forCellReuseIdentifier:@"ArticleListCell"];
    self.tableView.estimatedRowHeight = 340;
    
    self.model = [[ArticleListModel alloc] init];
    self.model.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        [self.tableView.header endRefreshing];
        [self.tableView reloadData];
        
        if (error == nil) {
            [self setupRefreshFooterView];
            if (self.model.more) {
                [self.tableView.footer endRefreshing];
            } else {
                [self.tableView.footer noticeNoMoreData];
            }
        } else {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
    };
    
    [self getListView].contentInsets_top = self.insetTop;
}

#pragma mark - HomePageItemViewControllerProtocol

- (UIScrollView *)getListView {
    return self.tableView;
}

- (void)setListViewInsetTop:(CGFloat)insetTop {
    self.insetTop = insetTop;
    [self getListView].contentInsets_top = self.insetTop;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (self.model.loaded) {
        [self.model refresh];
    } else {
        [self.list.header beginRefreshing];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return self.model.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    ArticleListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ArticleListCell" forIndexPath:indexPath];
    cell.data = self.model.items[indexPath.row];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    [AppAnalytics clickEvent:@"click_star_article"];
    ArticleInfoController *articleInfo = [ArticleInfoController spawn];
    articleInfo.article = self.model.items[indexPath.row];
    [self presentNavigationController:articleInfo];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (self.model.loaded && self.model.isEmpty) {
        return tableView.height;
    }
    return 0.000000f;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (self.model.loaded && self.model.isEmpty) {
        EmptyTableCell *cell = [EmptyTableCell loadFromNib];
        return cell;
    }
    return nil;
}

@end
