//
//  AuthorInfoController.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AuthorInfoController : UIViewController

@property (nonatomic, strong) USER *author;
@property (nonatomic, assign) BOOL isFromMessage;
@property (nonatomic, copy) void(^followAuthor)(USER *author);

@end
