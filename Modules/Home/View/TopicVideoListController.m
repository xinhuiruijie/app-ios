//
//  TopicVideoListController.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/11/28.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "TopicVideoListController.h"
#import "VideoInfoController.h"
#import "TopicListController.h"
#import "AuthorInfoController.h"

#import "TopicVideoCell.h"
#import "NewTopicVideoPlayManager.h"
#import "TopicVideoHeader.h"
#import "FullScreenShareView.h"
#import "VideoStarAlertView.h"

#import "VideoModel.h"
#import "TopicVideoListModel.h"
#import "TopicInfoModel.h"
#import "TopicModel.h"
#import "UserPointModel.h"

#import "VIDEO+Extension.h"
//#import "UITableView+VideoPlay.h"
#import "TopicCustomTransition.h"

#import "MPBackGuideView.h"

@interface TopicVideoListController () <UINavigationControllerDelegate, UITableViewDelegate, UITableViewDataSource, TopicVideoCellDelegate, NewTopicVideoPlayManagerDelegate, MPBackGuideViewDelegate>

@property (nonatomic, strong) UIButton *rightBarItem;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) FullScreenShareView *largeShareView;
@property (nonatomic, strong) VideoStarAlertView *actionAlert;
@property (nonatomic, strong) TopicVideoCell *playingCell;
@property (nonatomic, strong) NewTopicVideoPlayManager *playManager;

@property (nonatomic, strong) TopicVideoListModel *model;
@property (nonatomic, strong) TopicInfoModel *topicModel;

@property (nonatomic, assign) CGRect originFrame;
@property (nonatomic, assign) CGFloat offsetY_last;
@property (nonatomic, assign) BOOL isFirstLoad;
@property (nonatomic, assign) BOOL playing;
@property (nonatomic, assign) BOOL statusBarHidden;
/// 进来后记录电台播放状态，退出时还原
@property (nonatomic, assign) BOOL audioPlaying;

@end

@implementation TopicVideoListController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Video"];
}

//- (UIScrollView *)list {
//    return self.tableView;
//}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    @weakify(self)
    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
    self.navigationItem.leftBarButtonItem = [AppTheme whiteBackItemWithHandler:^(id sender) {
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
    self.navigationItem.rightBarButtonItem = [self rightBarButtonItem];
    
    self.audioPlaying = [AudioPlayManager sharedInstance].status == AudioPlayerStatusRestartToPlay;
    if (self.audioPlaying) {
        [[AudioPlayManager sharedInstance] pause];
    }
    if ([AudioPlayManager sharedInstance].status == AudioPlayerStatusBufferEmpty ||
        [AudioPlayManager sharedInstance].status == AudioPlayerStatusReadyToPlay) {
        [[AudioPlayManager sharedInstance] stop];// 如果是在缓冲中，就停止播放
    }

    self.isFirstLoad = YES;
    self.allowOritation = YES;
    
    self.playManager = [NewTopicVideoPlayManager loadFromNib];
    self.playManager.delegate = self;
    
    [self tableViewCustom];
    [self headerViewsCustom];
    [self topicModelInit];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadVideos:) name:kVideoInfoRefreshNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppDidEnterBackGround:) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppWillEnterForeground:) name:UIApplicationWillEnterForegroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppWillResignActive:) name:UIApplicationWillResignActiveNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAppDidBecomeActive:) name:UIApplicationDidBecomeActiveNotification object:nil];
    
    [self.model refresh];
    [self.topicModel refresh];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self scrollViewDidScroll:self.tableView];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
//    [self.navigationController enabledMLBlackTransition:NO];
    
    // 用来防止选中 cell push 到下个控制器时, tableView 再次调用 scrollViewDidScroll 方法, 造成 playingCell 被置空.
    self.tableView.delegate = self;
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    [self reloadCellVideoState];
    if (self.isFirstLoad) {
        [self.header startAnimation];
        self.isFirstLoad = NO;
    }
    
    [self showFeatureGuideViewIfNeeded];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    
    [self.navigationController enabledMLBlackTransition:YES];

    // 用来防止选中 cell push 到下个控制器时, tableView 再次调用 scrollViewDidScroll 方法, 造成 playingCell 被置空.
    self.tableView.delegate = nil;
    
    self.playing = self.playManager.isPlaying;
    [self.playManager pauseVideo];
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.navigationController.navigationBar lt_reset];
}

- (void)reloadCellVideoState {
    if (self.playingCell) {
        if (self.playing) {
            [self.playManager resumeVideo];
        }
    } else {
        if ([SettingModel sharedInstance].autoPlay == NO) {
            return;
        }
        
        CGFloat navigationHeight = [SDiOSVersion deviceSize] == Screen5Dot8inch ? 88 : 64;
        for (TopicVideoCell *visiableCell in [self.tableView visibleCells]) {
            CGRect frame = [self.view convertRect:visiableCell.videoView.frame fromView:visiableCell];
            if (frame.origin.y >= navigationHeight) {
                [self playVideoButtonAciton:visiableCell];
                break;
            }
        }
    }
}

- (void)viewDidLayoutSubviews {
    [super viewDidLayoutSubviews];
    
    if (self.playManager.isFullScreen) {
        if (self.playManager.superview == self.view) {
            return;
        }
        [self.navigationController setNavigationBarHidden:YES animated:NO];
        [self.playManager removeFromSuperview];
        [self.view addSubview:self.playManager];
        self.playManager.frame = self.view.frame;
    } else {
        [self.navigationController setNavigationBarHidden:NO animated:NO];
        [self.playManager removeFromSuperview];
        [self.playingCell addSubview:self.playManager];
        CGFloat mangerHeight = ceilf(kScreenWidth * 211 / 375);
        self.playManager.frame = CGRectMake(0, self.playingCell.height - mangerHeight, kScreenWidth, mangerHeight);
//        self.playManager.frame = self.playingCell.videoView.frame;
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    if (self.tableView.contentOffset.y > 50) {
        return UIStatusBarStyleDefault;
    }
    return UIStatusBarStyleLightContent;
}

- (void)dealloc {
    [self.playManager stopVideo];
    [self.playManager removeFromSuperview];
    self.playManager = nil;
    [self.tableView removeFromSuperview];
    
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
    [[AVAudioSession sharedInstance] setActive:NO error:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    // 如果进来时电台在播放，退出时还原
    if (self.audioPlaying) {
        [[AudioPlayManager sharedInstance] play];
    }
}

#pragma mark - Custom Views

- (void)tableViewCustom {
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.sectionFooterHeight = CGFLOAT_MIN;
    self.tableView.sectionHeaderHeight = CGFLOAT_MIN;
    self.tableView.estimatedRowHeight = 0;
    [self.view addSubview:self.tableView];
    
    if (iOSVersionGreaterThanOrEqualTo(@"10.0")) {
        CGFloat topInset = [SDiOSVersion deviceSize] == Screen5Dot8inch ? -88 : -64;
        self.tableView.contentInset = UIEdgeInsetsMake(topInset, 0, 0, 0);
    }
    if (iOSVersionGreaterThanOrEqualTo(@"11.0")) {
        self.tableView.estimatedRowHeight = 0;
        self.tableView.estimatedSectionHeaderHeight = 0;
        self.tableView.estimatedSectionFooterHeight = 0;
    }
    
    [self.tableView registerNib:[TopicVideoCell nib] forCellReuseIdentifier:@"TopicVideoCell"];
}

- (void)headerViewsCustom {
    CGFloat headerHeight;
    if ([SDiOSVersion deviceSize] == Screen5Dot8inch) {
        headerHeight = ceil(kScreenWidth * 420 / 750) + 20;
    } else {
        headerHeight = ceil(kScreenWidth * 420 / 750) + 20;
    }
    
    TopicVideoHeader *header = [TopicVideoHeader loadFromNib];
    header.frame = CGRectMake(0, 0, kScreenWidth, 30 + headerHeight);
    header.data = self.topic;
    self.header = header;
    self.tableView.tableHeaderView = header;
    @weakify(self)
    self.header.gotoAuthorInfo = ^(USER *author) {
        @strongify(self)
        AuthorInfoController *authorInfo = [AuthorInfoController spawn];
        authorInfo.author = author;
        [self presentNavigationController:authorInfo];
    };
}

- (UIBarButtonItem *)rightBarButtonItem {
    self.rightBarItem = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightBarItem.frame = CGRectMake(0, 0, 22, 22);
    self.rightBarItem.layer.borderColor = self.topic.is_collect ? [AppTheme selectedColor].CGColor : [UIColor whiteColor].CGColor;
    [self.rightBarItem setImage:[UIImage imageNamed:@"icon_collect_nor"] forState:UIControlStateNormal];
    [self.rightBarItem setImage:[UIImage imageNamed:@"icon_collect_sel"] forState:UIControlStateSelected];
    self.rightBarItem.selected = self.topic.is_collect;
    [self.rightBarItem addTarget:self action:@selector(collectTopic:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *rightItem = [[UIBarButtonItem alloc]initWithCustomView:self.rightBarItem];
    return rightItem;
}

#pragma mark - Model Init

- (void)topicModelInit {
    @weakify(self)
    self.model = [[TopicVideoListModel alloc] init];
    self.model.topic_id = self.topic.id;
    self.model.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        [self.tableView.header endRefreshing];
        [self.tableView reloadData];
        
        [self reloadCellVideoState];
        
        if (error == nil) {
            [self setupRefreshFooterView];
            if (self.model.more) {
                [self.tableView.footer endRefreshing];
            } else {
                [self.tableView.footer noticeNoMoreData];
            }
        } else {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
    };
    
    self.topicModel = [[TopicInfoModel alloc] init];
    self.topicModel.topic_id = self.topic.id;
    self.topicModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        self.topic = self.topicModel.item;
        self.header.data = self.topic;
        if (error) {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
    };
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.model.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TopicVideoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TopicVideoCell" forIndexPath:indexPath];
    cell.delegate = self;
    cell.data = self.model.items[indexPath.row];
    return cell;
}

- (void)tableView:(UITableView *)tableView didEndDisplayingCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([cell isEqual:self.playingCell]) {
        [self.playManager pauseVideo];
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    VideoInfoController *videoInfo = [VideoInfoController spawn];
    TOPIC_VIDEO *topicVideo = self.model.items[indexPath.row];
    VIDEO *video = topicVideo.video;
    videoInfo.video = video;
    videoInfo.stateManager = self.playManager.stateManager;
//    [self presentNavigationController:videoInfo];
    videoInfo.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:videoInfo animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.model.items.count < 1) {
        return 0;
    }
    TOPIC_VIDEO *topicVideo = self.model.items[indexPath.row];
    return [TopicVideoCell heightForTopicVideoItemRowWithSubtitle:topicVideo];
}

#pragma mark - UIScrollViewDelegate

/**
 * Called on finger up if the user dragged. decelerate is true if it will continue moving afterwards
 * 松手时已经静止, 只会调用scrollViewDidEndDragging
 */
- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (decelerate == YES)
        return;
    
    // 设置为不自动播放时，不自动播放短片
    if ([SettingModel sharedInstance].autoPlay == NO) {
        return;
    }
    
    CGFloat navigationHeight = [SDiOSVersion deviceSize] == Screen5Dot8inch ? 88 : 64;
    for (TopicVideoCell *visiableCell in [self.tableView visibleCells]) {
        CGRect frame = [self.view convertRect:visiableCell.videoView.frame fromView:visiableCell];
        if (frame.origin.y >= navigationHeight) {
            [self playVideoButtonAciton:visiableCell];
            break;
        }
    }
}

/**
 * Called on tableView is static after finger up if the user dragged and tableView is scrolling.
 * 松手时还在运动, 先调用scrollViewDidEndDragging, 再调用scrollViewDidEndDecelerating
 */
- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
//     scrollView已经完全静止
//    [self.tableView handleScrollStop];
    
    // 设置为不自动播放时，不自动播放短片
    if ([SettingModel sharedInstance].autoPlay == NO) {
        return;
    }
    
    CGFloat navigationHeight = [SDiOSVersion deviceSize] == Screen5Dot8inch ? 88 : 64;
    for (TopicVideoCell *visiableCell in [self.tableView visibleCells]) {
        CGRect frame = [self.view convertRect:visiableCell.videoView.frame fromView:visiableCell];
        if (frame.origin.y >= navigationHeight) {
            [self playVideoButtonAciton:visiableCell];
            break;
        }
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    self.offsetY_last = scrollView.contentOffset.y;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offsetY = scrollView.contentOffset.y;
    
    @weakify(self)
    CGFloat navigationHeight = [SDiOSVersion deviceSize] == Screen5Dot8inch ? 88 : 64;
    CGFloat changePoint = 50;
    if (offsetY > changePoint) {
        CGFloat alpha = MIN(1, 1 - ((changePoint + navigationHeight - offsetY) / navigationHeight));
        [self.navigationController.navigationBar lt_setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:alpha]];
        [self.rightBarItem setImage:[UIImage imageNamed:@"icon_collect_bar_nor"] forState:UIControlStateNormal];
        self.navigationItem.title = self.topic.title;
        self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
            @strongify(self)
            [self.navigationController popViewControllerAnimated:YES];
        }];
        [self setNeedsStatusBarAppearanceUpdate];
    } else {
        [self.navigationController.navigationBar lt_setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0]];
        [self.rightBarItem setImage:[UIImage imageNamed:@"icon_collect_nor"] forState:UIControlStateNormal];
        self.navigationItem.title = nil;
        self.navigationItem.leftBarButtonItem = [AppTheme whiteBackItemWithHandler:^(id sender) {
            @strongify(self)
            [self.navigationController popViewControllerAnimated:YES];
        }];
        [self setNeedsStatusBarAppearanceUpdate];
    }
    
    // 处理滚动方向
//    [self handleScrollDerectionWithOffset:scrollView.contentOffset.y];
//    [self.tableView.playingCell cellVideoStopPlay];
    // 处理循环利用
//    [self.tableView handleQuickScroll];
}

//- (void)handleScrollDerectionWithOffset:(CGFloat)offsetY{
//    self.tableView.currentDerection = (offsetY-self.offsetY_last>0) ? JPVideoPlayerDemoScrollDerectionUp : JPVideoPlayerDemoScrollDerectionDown;
//    self.offsetY_last = offsetY;
//}

#pragma mark - TopicVideoCellDelegate

- (void)playVideoButtonAciton:(TopicVideoCell *)cell {
    TOPIC_VIDEO *topicVideo = cell.data;
    // cell 复用，会造成快速滑动播放视频为同 cell 上一次视频的问题，因此根据 data 来进行判断（或注释掉，但会造成播放按钮为暂停但实际播放的情况）
    //if ([cell isEqual:self.playingCell]) {
    if (self.playManager.video == topicVideo.video) {
        return;
    }
    
    [self.playManager pauseVideo];
    [self.playManager removeFromSuperview];
    self.playingCell = cell;
    
    self.playManager.frame = cell.videoView.frame;
    self.playManager.video = topicVideo.video;
    [cell addSubview:self.playManager];
}

#pragma mark - NewTopicVideoPlayManagerDelegate

- (void)refreshVideoManager {
    [self.model refresh];
}

- (void)statusBarHide:(BOOL)hide {
    self.statusBarHidden = hide;
    [UIView animateWithDuration:0.3 animations:^{
        [self setNeedsStatusBarAppearanceUpdate];
    }];
}

- (void)fullScreenCollectVideo:(VIDEO *)video sender:(UIButton *)sender {
    if (![UserModel online]) {
        [self.playManager pauseVideo];
        [self.playManager needChangeScreenWithFullView];
        [[Authorization sharedInstance] showAuth];
        return;
    }
    
    [sender disableSelf];
    if (video.is_collect) {
        [VideoModel unfavourite:video then:^(STIHTTPResponseError *error) {
            if (error == nil) {
                video.is_collect = NO;
                self.playManager.video = video;
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
            [sender enableSelf];
        }];
    } else {
        [VideoModel favourite:video then:^(STIHTTPResponseError *error) {
            if (error == nil) {
                video.is_collect = YES;
                self.playManager.video = video;
                [UserPointModel userPointName:MODULE_NAME_CLICK_COLLECT_FINISH];
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
            [sender enableSelf];
        }];
    }
}

- (void)fullScreenStarVideo:(VIDEO *)video sender:(UIButton *)sender {
    if (![UserModel online]) {
        [self.playManager pauseVideo];
        [self.playManager needChangeScreenWithFullView];
        [[Authorization sharedInstance] showAuth];
        return;
    }
    
    if (video.is_star) {
        return;
    }
    
    [AppAnalytics clickEvent:@"click_video_score"];
    [self.view endEditing:YES];
    
    [sender disableSelf];
    VideoStarAlertView *actionAlert = [VideoStarAlertView loadFromNib];
    [actionAlert darkTheme];
    @weakify(actionAlert)
    @weakify(self)
    actionAlert.confirmAction = ^(NSInteger starValue) {
        @strongify(actionAlert)
        @strongify(self)
        if (starValue == 0) {
            [[UIApplication sharedApplication].keyWindow presentMessageTips:@"请给短片评分吧~"];
        } else {
            [VideoModel star:video point:starValue then:^(STIHTTPResponseError *error) {
                if (error == nil) {
                    video.is_star = YES;
                    self.playManager.video = video;
                    
                    UIImageView *successIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_grade_acc"]];
                    [self presentMessageTips:@"评分成功" customView:successIcon];
                } else {
                    [[UIApplication sharedApplication].keyWindow presentMessageTips:error.message];
                }
                [sender enableSelf];
            }];
            [actionAlert hide];
        }
    };
    actionAlert.cancelAction = ^{
        [sender enableSelf];
    };
    self.actionAlert = actionAlert;
    [actionAlert show];
}

- (void)shareVideo:(VIDEO *)video isFullScreen:(BOOL)isFullScreen {
    [AppAnalytics clickEvent:@"click_video_share"];
    [self.view endEditing:YES];
    
    FullScreenShareView *largeShareView = [FullScreenShareView loadFromNib];
    self.largeShareView = largeShareView;
    
    @weakify(self)
    if (video.photo && video.photo.thumb) {
        [self presentLoadingTips:nil];
        [self imageWithUrl:video.photo.thumb completion:^(UIImage *image) {
            @strongify(self)
            dispatch_async(dispatch_get_main_queue(), ^{
                [self dismissTips];
                video.shareImage = image;
                self.largeShareView.data = video;
                [self.largeShareView show];
            });
        }];
    } else {
        self.largeShareView.data = video;
        [self.largeShareView show];
    }
}

#pragma mark - Collect Topic Action

- (void)collectTopic:(UIButton *)sender {
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }
    
    [sender disableSelf];
    [self presentLoadingTips:nil];
    if (self.topic.is_collect) {
        [TopicModel unfavourite:self.topic then:^(STIHTTPResponseError *error) {
            [self dismissTips];
            if (error == nil) {
                self.topic.is_collect = NO;
                sender.selected = self.topic.is_collect;
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
            [sender enableSelf];
        }];
    } else {
        [TopicModel favourite:self.topic then:^(STIHTTPResponseError *error) {
            [self dismissTips];
            if (error == nil) {
                self.topic.is_collect = YES;
                sender.selected = self.topic.is_collect;
                [UserPointModel userPointName:MODULE_NAME_CLICK_COLLECT_FINISH];
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
            [sender enableSelf];
        }];
    }
}

#pragma mark - Notifaction

- (void)reloadVideos:(NSNotification *)notification {
    if ([[self topViewController] isKindOfClass:[self class]]) {
        return;
    }
    VIDEO *video = [notification.userInfo objectForKey:@"data"];
    for (int i = 0; i < self.model.items.count; i++) {
        TOPIC_VIDEO *videoItem = self.model.items[i];
        if (videoItem.video.id && [videoItem.video.id isEqualToString:video.id]) {
            videoItem.video.is_collect = video.is_collect;
            videoItem.video.is_like = video.is_like;
            videoItem.video.is_star = video.is_star;
            self.playManager.video = video;
            break;  // 一个列表不会出现相同的短片数据
        }
    }
//    [self.tableView reloadData];
}

- (void)onAppDidEnterBackGround:(UIApplication*)app {
    if (self.playing) {
        [self.playManager pauseVideo];
    }
}

- (void)onAppWillResignActive:(UIApplication *)app {
    self.playing = self.playManager.isPlaying;
    if (self.playing) {
        [self.playManager pauseVideo];
    }
    if (self.playManager.isFullScreen) {
        self.isLandscape = YES;
    }
}

- (void)onAppWillEnterForeground:(UIApplication*)app {
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];
    if (self.playing) {
        [self.playManager resumeVideo];
    }
}

- (void)onAppDidBecomeActive:(UIApplication*)app {
    [[AVAudioSession sharedInstance] setActive:YES error:nil];
    [[AVAudioSession sharedInstance] setCategory:AVAudioSessionCategoryPlayback error:nil];

    self.isLandscape = NO;
    if (self.playing) {
        [self.playManager resumeVideo];
    }
}

#pragma mark - Header

- (void)setupRefreshFooterView {
    if (self.tableView.footer == nil) {
        if (!self.model.isEmpty) {
            @weakify(self)
            [self.tableView addFooterPullLoader:^{
                @strongify(self)
                [self.model loadMore];
            }];
        }
    } else {
        if (self.model.isEmpty) {
            [self.tableView removeFooter];
        }
    }
}
#pragma mark - 转屏事件回调

- (void)rotateScreen {
    [self.actionAlert hide];
    [self.largeShareView hide];
}

- (void)willRotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation duration:(NSTimeInterval)duration {
    [self rotateScreen];
    if (toInterfaceOrientation == UIInterfaceOrientationLandscapeLeft || toInterfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        [self.playManager reloadFullViewWithIsFull:YES];
    } else {
        [self.playManager reloadFullViewWithIsFull:NO];
    }
}

#pragma mark - 导航控制器的代理

- (id<UIViewControllerAnimatedTransitioning>)navigationController:(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC {
    // 注释代码控制了返回页面时动画。
    BOOL animated = ([toVC isKindOfClass:[self class]] && [fromVC isKindOfClass:[TopicListController class]]);
    // || ([toVC isKindOfClass:[TopicListController class]] && [fromVC isKindOfClass:[self class]]);
    if (animated) {
        return [[TopicCustomTransition alloc] initWithTransitionType:operation == UINavigationControllerOperationPush? push :pop];
    }
    return nil;
}

#pragma mark - 是否支持自动转屏

- (BOOL)shouldAutorotate {
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskAllButUpsideDown;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

- (BOOL)prefersStatusBarHidden {
    return self.statusBarHidden;
}

#pragma mark - Guide

// 显示新手引导图
- (void)showFeatureGuideViewIfNeeded {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    BOOL showGuide = [ud boolForKey:@"showTopicVideoGuide"];
    if (!showGuide) {
        [self showFeatureGuideView];
        [self.playManager stopLoad];
    }
}

- (void)showFeatureGuideView {
    MPBackGuideView *guideView = [MPBackGuideView showBackGuide];
    guideView.delegate = self;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (guideView) {
            [guideView dismiss];
        }
    });
}

- (void)guideViewDismiss:(MPBackGuideView *)guideView {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    [ud setBool:YES forKey:@"showTopicVideoGuide"];
    [ud synchronize];
    [self.playManager resumeLoad];
}

@end
