//
//  HomePageItemViewControllerProtocol.h
//  MotherPlanet
//
//  Created by GeekZooStudio on 2018/4/18.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol HomePageItemViewControllerProtocol <NSObject>
- (void)setListViewInsetTop:(CGFloat)insetTop;
- (UIScrollView *)getListView;
@end
