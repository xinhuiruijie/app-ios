//
//  TopicListController.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "TopicListController.h"
#import "TopicVideoListController.h"
#import "AuthorInfoController.h"
#import "TopicListModel.h"
#import "TopicListCell.h"
#import "MPBackGuideView.h"

@interface TopicListController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) TopicListModel *model;

@property (nonatomic, assign) int lastPosition;
@property (nonatomic, assign) BOOL isDownScroll;
@property (nonatomic, assign) BOOL showAnimation;

@end

@implementation TopicListController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Home"];
}

- (UIScrollView *)list {
    return self.tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"全部话题";
    @weakify(self)
    self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    self.isDownScroll = YES;
    self.showAnimation = YES;
    
    self.tableView.backgroundColor = [AppTheme listBackgroundColor];
    self.tableView.estimatedRowHeight = 0;
    [self.tableView registerNib:[TopicListCell nib] forCellReuseIdentifier:@"TopicListCell"];
    [self.tableView registerNib:[EmptyTableCell nib] forCellReuseIdentifier:@"EmptyTableCell"];

    self.model = [[TopicListModel alloc] init];
    self.model.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        [self.tableView.header endRefreshing];
        [self.tableView reloadData];
        
        if (error == nil) {
            [self setupRefreshFooterView];
            if (self.model.more) {
                [self.tableView.footer endRefreshing];
            } else {
                [self.tableView.footer noticeNoMoreData];
            }
        } else {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
    };
    [self.model refresh];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
//    if (self.model.loaded) {
//        [self.model refresh];
//    } else {
//        [self.list.header beginRefreshing];
//    }
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    self.showAnimation = YES;
//    [self showFeatureGuideViewIfNeeded];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    self.showAnimation = NO;
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat currentPostionY = scrollView.contentOffset.y;
    
    if (currentPostionY - _lastPosition > 0) {
        // 向下
        _lastPosition = currentPostionY;
        self.isDownScroll = YES;
    } else if (_lastPosition - currentPostionY > 0) {
        // 向上
        _lastPosition = currentPostionY;
        self.isDownScroll = NO;
    }
    
    if (scrollView.contentOffset.y < 0) {
        self.isDownScroll = NO;
    }
    
    if (scrollView.contentOffset.y == 0) {
        self.isDownScroll = YES;
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.model.loaded && self.model.isEmpty) {
        return 1;
    }
    return self.model.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.model.loaded && self.model.isEmpty) {
        EmptyTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EmptyTableCell" forIndexPath:indexPath];
        return cell;
    }
    TopicListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TopicListCell" forIndexPath:indexPath];
    cell.data = self.model.items[indexPath.row];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.model.loaded && self.model.isEmpty) {
        return;
    }
    TopicVideoListController *topicVideo = [TopicVideoListController spawn];
    topicVideo.topic = self.model.items[indexPath.row];
    
    TopicListCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    self.currentCell = cell;
    self.navigationController.delegate = topicVideo;
    [self.navigationController pushViewController:topicVideo animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.model.loaded && self.model.isEmpty) {
        return tableView.height;
    }
    return ceil(kScreenWidth * 311 / 750);
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.isDownScroll) {
        if (CGRectGetMinY(cell.frame) < self.view.height) {
            if (self.showAnimation) {
                cell.alpha = 0;
                NSInteger indexDelay = indexPath.row;
                [cell performSelector:@selector(startSingleViewAnimation) withObject:self afterDelay:indexDelay * 0.2];
            }
        } else {
            if (self.showAnimation) {
                [cell startSingleViewAnimation];
            }
        }
    }
}

#pragma mark - Guide

// 显示新手引导图
- (void)showFeatureGuideViewIfNeeded {
    NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
    BOOL showGuide = [ud boolForKey:@"showTopicListGuide"];
    if (!showGuide) {
        [self showFeatureGuideView];
    }
}

- (void)showFeatureGuideView {
    MPBackGuideView *guideView = [MPBackGuideView showBackGuide];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (guideView) {
            [guideView dismiss];
        }
        NSUserDefaults *ud = [NSUserDefaults standardUserDefaults];
        [ud setBool:YES forKey:@"showTopicListGuide"];
        [ud synchronize];
    });
}

@end
