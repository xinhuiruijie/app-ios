//
//  CategoryListController.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "CategoryListController.h"
#import "CategoryListCell.h"
#import "CategoryListModel.h"
#import "CategoryVideoListController.h"
#import "ULBCollectionViewFlowLayout.h"

@interface CategoryListController () <UICollectionViewDelegate, UICollectionViewDataSource, ULBCollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (nonatomic, strong) CategoryListModel *model;

@end

@implementation CategoryListController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Home"];
}

- (UIScrollView *)list {
    return self.collectionView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"全部分类";
    @weakify(self)
    self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    ULBCollectionViewFlowLayout *layout = (ULBCollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    layout.minimumLineSpacing = CGFLOAT_MIN;
    layout.minimumInteritemSpacing = CGFLOAT_MIN;
    layout.sectionInset = UIEdgeInsetsMake(0, 15, 0, 15);
    
    self.collectionView.backgroundColor = [AppTheme listBackgroundColor];
    [self.collectionView registerNib:[CategoryListCell nib] forCellWithReuseIdentifier:@"CategoryListCell"];
    [self.collectionView registerNib:[EmptyCollectionCell nib] forCellWithReuseIdentifier:@"EmptyCollectionCell"];

    self.model = [[CategoryListModel alloc] init];
    self.model.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        [self.collectionView.header endRefreshing];
        [self.collectionView reloadData];
        
        if (error == nil) {
            [self setupRefreshFooterView];
            if (self.model.more) {
                [self.collectionView.footer endRefreshing];
            } else {
                [self.collectionView.footer noticeNoMoreData];
            }
        } else {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
    };
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if (self.model.loaded) {
        [self.model refresh];
    } else {
        [self.list.header beginRefreshing];
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (self.model.loaded && self.model.isEmpty) {
        return 1;
    }
    return self.model.items.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.model.loaded && self.model.isEmpty) {
        EmptyCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"EmptyCollectionCell" forIndexPath:indexPath];
        return cell;
    }
    CategoryListCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"CategoryListCell" forIndexPath:indexPath];
    cell.data = self.model.items[indexPath.row];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.model.loaded && self.model.isEmpty) {
        return;
    }
    CategoryVideoListController *videoList = [CategoryVideoListController spawn];
    CATEGORY *category = self.model.items[indexPath.row];
    videoList.category = category;
    [self.navigationController pushViewController:videoList animated:YES];
}

#pragma mark - ULBCollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.model.loaded && self.model.isEmpty) {
        return collectionView.size;
    }
    CGFloat itemWidth = ceil((kScreenWidth - 45) / 2);
    CGFloat itemHeight = ceil(itemWidth * 340 / 330);
    return CGSizeMake(itemWidth, itemHeight);
}

- (UIColor *)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout colorForSectionAtIndex:(NSInteger)section {
    return [UIColor whiteColor];
}

@end
