//
//  TopicVideoListController.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/11/28.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
@class TopicVideoHeader;
@class NewTopicVideoHeader;

@interface TopicVideoListController : UIViewController <UINavigationControllerDelegate>

@property (nonatomic, strong) TopicVideoHeader *header;
@property (nonatomic, strong) NewTopicVideoHeader *topicHeader;
@property (nonatomic, strong) TOPIC *topic;

@property (nonatomic, assign) BOOL allowOritation;
@property (nonatomic, assign) BOOL isLandscape;

@end
