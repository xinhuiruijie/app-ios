//
//  HotVideoListCell.h
//  MotherPlanet
//
//  Created by GeekZooStudio on 2018/4/12.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface HotVideoListCell : UITableViewCell

+ (CGFloat)heightForRowWith:(VIDEO *)video;

- (void)hideBottomLine:(BOOL)hide;
@end
