//
//  BannerListCell.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BannerListCell : UITableViewCell

@property (nonatomic, assign) BANNER_TYPE bannerType; // banner类型（首页banner和热议banner）

- (void)start;
- (void)stop;
@end
