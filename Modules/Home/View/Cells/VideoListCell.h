//
//  VideoListCell.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol VideoListCellDelegate <NSObject>

@optional
- (void)gotoAuthorInfo:(USER *)author;
- (void)followAuthor:(VIDEO *)video sender:(UIButton *)sender;

@end

@interface VideoListCell : UITableViewCell

+ (CGFloat)heightForVideoListCell:(VIDEO *)video;

@property (nonatomic, weak) id<VideoListCellDelegate> delegate;

@end
