//
//  HomeTopicCell.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "HomeTopicCell.h"
#import "HomeTopicItemCell.h"

@interface HomeTopicCell () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic, strong) NSArray *topics;

@property (nonatomic, assign) int lastPosition;
@property (nonatomic, assign) BOOL isRightScroll;

@end

@implementation HomeTopicCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

    self.lastPosition = -15;    // 默认有15的间距
    
    [self.collectionView registerNib:[HomeTopicItemCell nib] forCellWithReuseIdentifier:@"HomeTopicItemCell"];
    
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    CGFloat itemHeight = ceil(kScreenWidth * 258 / 750);
    CGFloat itemWidth = ceil(kScreenWidth * 460 / 750);
    layout.itemSize = CGSizeMake(itemWidth, itemHeight);
    layout.minimumLineSpacing = 10;
//    layout.minimumInteritemSpacing = 10;
    self.collectionView.contentInset = UIEdgeInsetsMake(0, 15, 0, 15);
}

- (void)dataDidChange {
    NSArray *topics = self.data;
    self.topics = topics;
    
    [self.collectionView reloadData];
}

- (void)startSingleViewAnimation {
    self.alpha = 1;
    for (UICollectionViewCell *cell in self.collectionView.visibleCells) {
        NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
        NSInteger indexDelay = indexPath.item;
        [cell startSingleViewAnimationDelay:indexDelay * 0.2];
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.topics.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    HomeTopicItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeTopicItemCell" forIndexPath:indexPath];
    cell.data = self.topics[indexPath.item];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.isRightScroll) {
        if (CGRectGetMinX(cell.frame) > collectionView.width) {
            cell.alpha = 0;
            [cell startSingleViewAnimation];
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat currentPostionX = scrollView.contentOffset.x;

    if (currentPostionX - _lastPosition > 0) {
        // 向右
        _lastPosition = currentPostionX;
        self.isRightScroll = YES;
    } else if (_lastPosition - currentPostionX > 0) {
        // 向左
        _lastPosition = currentPostionX;
        self.isRightScroll = NO;
    }

    if (scrollView.contentOffset.x < -15) {
        self.isRightScroll = NO;
    }

    if (scrollView.contentOffset.x == -15) {
        self.isRightScroll = YES;
    }
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    TOPIC *topic = self.topics[indexPath.item];
    if (self.delegate && [self.delegate respondsToSelector:@selector(gotoTopicInfo:)]) {
        [self.delegate gotoTopicInfo:topic];
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (IBAction)showAllTopicAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(gotoTopicList)]) {
        [self.delegate gotoTopicList];
    }
}

@end
