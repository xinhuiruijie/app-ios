//
//  HomeTopicCell.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HomeTopicCellDelegate <NSObject>

@optional
- (void)gotoTopicInfo:(TOPIC *)topic;
- (void)gotoTopicList;

@end

@interface HomeTopicCell : UITableViewCell

@property (nonatomic, weak) id<HomeTopicCellDelegate> delegate;

@end
