//
//  HomeVideoHeader.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, VIDEO_HEADER_TYPE) {
    VIDEO_HEADER_STAR,
    VIDEO_HEADER_PLANET,
};

@interface HomeVideoHeader : UIView

@property (nonatomic, assign) VIDEO_HEADER_TYPE type;

@end
