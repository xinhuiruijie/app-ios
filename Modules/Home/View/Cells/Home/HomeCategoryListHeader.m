//
//  HomeCategoryListHeader.m
//  MotherPlanet
//
//  Created by GeekZooStudio on 2018/4/12.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "HomeCategoryListHeader.h"

@interface HomeCategoryListHeader()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@end

@implementation HomeCategoryListHeader

- (void)setTitle:(NSString *)title {
    _title = title;
    self.titleLabel.text = title;
}

@end
