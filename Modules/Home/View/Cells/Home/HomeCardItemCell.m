//
//  HomeCardItemCell.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "HomeCardItemCell.h"

@interface HomeCardItemCell ()

@property (weak, nonatomic) IBOutlet UIImageView *cardPhoto;
@property (weak, nonatomic) IBOutlet UILabel *cardTitleLabel;

@end

@implementation HomeCardItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)dataDidChange {
    CARD *card = self.data;
    self.cardPhoto.image = [AppTheme placeholderImage];
    if (card.photo) {
        [self.cardPhoto setImageWithPhoto:card.photo];
    } else {
        self.cardPhoto.image = [AppTheme placeholderImage];
    }
    self.cardTitleLabel.text = card.title?:@"";
}

@end
