//
//  HomeTopicItemCell.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "HomeTopicItemCell.h"

@interface HomeTopicItemCell ()

@property (weak, nonatomic) IBOutlet UIImageView *topicPhoto;
@property (weak, nonatomic) IBOutlet UILabel *topicTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *topicSubtitleLabel;

@end

@implementation HomeTopicItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.topicPhoto.layer.cornerRadius = 3;
}

- (void)dataDidChange {
    TOPIC *topic = self.data;
    self.topicPhoto.image = [AppTheme placeholderImage];
    if (topic.cover_photo) {
        [self.topicPhoto setImageWithPhoto:topic.cover_photo];
    } else {
        self.topicPhoto.image = [AppTheme placeholderImage];
    }
    self.topicTitleLabel.text = topic.title.length ? [NSString stringWithFormat:@"#%@", topic.title] : @"";
    self.topicSubtitleLabel.text = topic.subtitle?:@"";
}

@end
