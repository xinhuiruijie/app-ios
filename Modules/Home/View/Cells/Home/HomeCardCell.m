//
//  HomeCardCell.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "HomeCardCell.h"
#import "HomeCardItemCell.h"

@interface HomeCardCell () <UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@property (nonatomic, strong) NSArray *cards;

@property (nonatomic, assign) int lastPosition;
@property (nonatomic, assign) BOOL isRightScroll;

@end

@implementation HomeCardCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code

    self.lastPosition = -15;    // 默认有15的间距
    
    [self.collectionView registerNib:[HomeCardItemCell nib] forCellWithReuseIdentifier:@"HomeCardItemCell"];
    
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    CGFloat itemHeight = ceil(kScreenWidth * 110 / 750);
    CGFloat itemWidth = ceil(kScreenWidth * 146 / 750);
    layout.itemSize = CGSizeMake(itemWidth, itemHeight);
    layout.minimumLineSpacing = 15;
    layout.minimumInteritemSpacing = 15;
    self.collectionView.contentInset = UIEdgeInsetsMake(0, 15, 0, 15);
}

- (void)dataDidChange {
    NSArray *cards = self.data;
    self.cards = cards;
    
    [self.collectionView reloadData];
}

- (void)startSingleViewAnimation {
    self.alpha = 1;
    for (UICollectionViewCell *cell in self.collectionView.visibleCells) {
        NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
        NSInteger indexDelay = indexPath.item;
        [cell startSingleViewAnimationDelay:indexDelay * 0.2];
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.cards.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    HomeCardItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"HomeCardItemCell" forIndexPath:indexPath];
    cell.data = self.cards[indexPath.item];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView willDisplayCell:(UICollectionViewCell *)cell forItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.isRightScroll) {
        if (CGRectGetMinX(cell.frame) > collectionView.width) {
            cell.alpha = 0;
            [cell startSingleViewAnimation];
        }
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat currentPostionX = scrollView.contentOffset.x;

    if (currentPostionX - _lastPosition > 0) {
        // 向右
        _lastPosition = currentPostionX;
        self.isRightScroll = YES;
    } else if (_lastPosition - currentPostionX > 0) {
        // 向左
        _lastPosition = currentPostionX;
        self.isRightScroll = NO;
    }

    if (scrollView.contentOffset.x < -15) {
        self.isRightScroll = NO;
    }

    if (scrollView.contentOffset.x == -15) {
        self.isRightScroll = YES;
    }
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    CARD *card = self.cards[indexPath.item];
    if (self.delegate && [self.delegate respondsToSelector:@selector(gotoCardInfo:)]) {
        [self.delegate gotoCardInfo:card];
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

@end
