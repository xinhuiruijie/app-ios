//
//  HomeCardCell.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol HomeCardCellDelegate <NSObject>

@optional
- (void)gotoCardInfo:(CARD *)card;

@end

@interface HomeCardCell : UITableViewCell

@property (nonatomic, weak) id<HomeCardCellDelegate> delegate;

@end
