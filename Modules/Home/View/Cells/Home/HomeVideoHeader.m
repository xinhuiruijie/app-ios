//
//  HomeVideoHeader.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "HomeVideoHeader.h"

@interface HomeVideoHeader ()
@property (weak, nonatomic) IBOutlet UIView *TopHotView;

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation HomeVideoHeader

- (void)setType:(VIDEO_HEADER_TYPE)type {
    if (type == VIDEO_HEADER_STAR) {
        self.titleLabel.text = @"TOP精选短片";
        
        self.TopHotView.hidden = NO;
    } else if (type == VIDEO_HEADER_PLANET) {
        self.titleLabel.text = @"短片推荐";
        
        self.TopHotView.hidden = YES;
    }
}

@end
