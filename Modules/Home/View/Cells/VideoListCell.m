//
//  VideoListCell.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "VideoListCell.h"
#import "VideoTagView.h"

@interface VideoListCell ()
@property (weak, nonatomic) IBOutlet UIButton *authorAvatar;
@property (weak, nonatomic) IBOutlet UILabel *authorName;
@property (weak, nonatomic) IBOutlet UILabel *authorDesc;
//@property (weak, nonatomic) IBOutlet UIImageView *authorFollowIcon;
//@property (weak, nonatomic) IBOutlet UILabel *authorFollowLabel;
@property (weak, nonatomic) IBOutlet UIImageView *videoPhoto;
@property (weak, nonatomic) IBOutlet UILabel *videoTitle;
@property (weak, nonatomic) IBOutlet UILabel *videoStar;
//@property (weak, nonatomic) IBOutlet UIView *videoStarView;
//@property (weak, nonatomic) IBOutlet UILabel *videoSubtitle;
@property (weak, nonatomic) IBOutlet UILabel *playCountLabel;
//@property (weak, nonatomic) IBOutlet UILabel *likeCountLabel;
//@property (weak, nonatomic) IBOutlet UILabel *collectCountLabel;
//@property (weak, nonatomic) IBOutlet UILabel *shareCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *videoTimeLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *videoTimeLabelLeftLC;
@property (weak, nonatomic) IBOutlet UIView *tagView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tagViewWidthLC;
@property (weak, nonatomic) IBOutlet UIView *authorFollowView;
@property (weak, nonatomic) IBOutlet UIButton *followButton;

@end

@implementation VideoListCell

+ (CGFloat)heightForVideoListCell:(VIDEO *)video {
//    CGFloat constHeight = 155;
//    CGFloat imageHeight = (kScreenWidth - 30) * 330 / 690;
//
//    CGFloat contentHeight = [AppTheme calculateHeigthWithAttributeContent:[AppTheme detailAttributedString:video.subtitle] width:kScreenWidth - 30];
//    CGFloat contentBoundHeight = 36;
//    CGFloat labelHeight = MIN(contentHeight, contentBoundHeight);
//
//    return ceil(constHeight + imageHeight + labelHeight);
    
    CGFloat constHeight = 72;
    CGFloat imageHeight = kScreenWidth * 422 / 750;
    
    return ceil(constHeight + imageHeight);
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.authorAvatar.imageView.contentMode = UIViewContentModeScaleAspectFill;
    
//    [self.contentView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        obj.backgroundColor = [UIColor randomColor];
//    }];
}

- (void)dataDidChange {
    VIDEO *video = self.data;
    
    if (video.owner.avatar) {
        [self.authorAvatar setImageWithPhoto:video.owner.avatar placeholderImage:[AppTheme avatarDefaultImage]];
    } else {
        [self.authorAvatar setImage:[AppTheme avatarDefaultImage] forState:UIControlStateNormal];
    }
    self.authorName.text = video.owner.name?:@"";
    self.authorDesc.text = video.owner.introduce?:@"";
    if (video.owner == nil || [video.owner.id isEqualToString:[UserModel sharedInstance].user.id]) {
        self.authorFollowView.hidden = YES;
    } else {
        self.authorFollowView.hidden = NO;
        self.followButton.selected = video.owner.is_follow && video.owner.is_author;
    }
    
    self.videoTitle.text = video.title?:@"";
    [self.tagView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj removeFromSuperview];
    }];
//    需求变更：列表只展示分类，暂不删除以前已完成的逻辑
//    CGFloat tagOriginX = 0;
//    CGFloat margin = 10;
//    CGFloat tagWidth = kScreenWidth - 110;
//    CGFloat maxWidth = floor((tagWidth - (margin * video.tags.count)) / (video.tags.count + 1));
//    for (int i = 0; i < video.tags.count + 1; i++) {
//        NSString *title = @"#";
//        if (i == 0) {
//            if (video.category) {
//                CATEGORY *category = video.category;
//                title = [title stringByAppendingString:category.title];
//            } else {
//                continue;
//            }
//        } else {
//            TAG *tag = video.tags[i - 1];
//            title = [title stringByAppendingString:tag.name];
//        }
//
//        CGFloat width = [AppTheme calculateWidthWithContent:title height:18] + 12;
//        width = MIN(maxWidth, width);
//
//        VideoTagView *tagSubview = [[VideoTagView alloc] initWithFrame:CGRectMake(tagOriginX, 0, width, 18)];
//        if (i == 0) {
//            tagSubview.type = VIDEO_CATEGORY;
//        } else {
//            tagSubview.type = VIDEO_TAG;
//        }
//        tagSubview.title = title;
//        tagOriginX += width;
//        tagOriginX += margin;
//
//        [self.tagView addSubview:tagSubview];
//    }
    
    CGFloat tagOriginX = 0;
    CGFloat tagWidth = kScreenWidth - 110;
    CGFloat tagViewWidth = 0;
    if (video.category) {
        CATEGORY *category = video.category;
        NSString *title = [NSString stringWithFormat:@"#%@", category.title ? : [AppTheme defaultPlaceholder]];
        
        CGFloat width = [AppTheme calculateWidthWithContent:title height:18] + 12;
        width = MIN(tagWidth, width);
        
        VideoTagView *tagSubview = [[VideoTagView alloc] initWithFrame:CGRectMake(tagOriginX, 0, width, 18)];
        tagSubview.type = VIDEO_CATEGORY;
        tagSubview.title = title;
        tagSubview.layer.cornerRadius = 2;
        [self.tagView addSubview:tagSubview];
        
        tagViewWidth = tagOriginX + width;
    }
    self.tagViewWidthLC.constant = tagViewWidth;
    self.videoTimeLabelLeftLC.constant = tagViewWidth == 0 ? 0 : 10 ;
    
//    self.videoSubtitle.attributedText = [AppTheme detailAttributedString:video.subtitle];
//    self.videoSubtitle.lineBreakMode = NSLineBreakByTruncatingTail;
    
    self.videoPhoto.image = [AppTheme placeholderImage];
    if (video.photo) {
        [self.videoPhoto setImageWithPhoto:video.photo];
    } else {
        self.videoPhoto.image = [AppTheme placeholderImage];
    }
    
    float videoPlayCount = [video.play_count floatValue];
    if (videoPlayCount >= 10000) {
        self.playCountLabel.text = [NSString stringWithFormat:@"%0.1fW", videoPlayCount / 10000];
    } else {
        self.playCountLabel.text = [NSString stringWithFormat:@"%@", video.play_count?:@(0)];
    }
//    self.likeCountLabel.text = [NSString stringWithFormat:@"%@", video.like_count ? [video.like_count setupCountNumber] : @(0)];
//    self.collectCountLabel.text = [NSString stringWithFormat:@"%@", video.collect_count ? [video.collect_count setupCountNumber] : @(0)];
//    self.shareCountLabel.text = [NSString stringWithFormat:@"%@", video.share_count ? [video.share_count setupCountNumber] : @(0)];
    self.videoStar.text = [NSString stringWithFormat:@"%.1f", video.star?:0.0];
    self.videoTimeLabel.text = video.video_time?:@"00'00''";
    
//    [self.videoStarView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        [obj removeFromSuperview];
//    }];
//
//    NSInteger star = roundf(video.star);
//    CGFloat originX = 0;
//    for (int i = 1; i < 10; i+=2) {
//        originX = (i/2) * (10 + 3);
//        NSString *starIcon;
//        if (i < star) {
//            starIcon = @"icon_star_sel";
//        } else if (i > star) {
//            starIcon = @"icon_star_nor";
//        } else {
//            starIcon = @"icon_star_half";
//        }
//        UIImageView *starImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:starIcon]];
//        starImage.frame = CGRectMake(originX, 0, 10, 10);
//        [self.videoStarView addSubview:starImage];
//    }
}

- (IBAction)authorAvatarAction:(id)sender {
    VIDEO *video = self.data;
    if (video.owner) {
        if (self.delegate && [self.delegate respondsToSelector:@selector(gotoAuthorInfo:)]) {
            [self.delegate gotoAuthorInfo:video.owner];
        }
    }
}

- (IBAction)followAuthor:(UIButton *)sender {
    VIDEO *video = self.data;
    if (self.delegate && [self.delegate respondsToSelector:@selector(followAuthor:sender:)]) {
        [self.delegate followAuthor:video sender:sender];
    }
}

@end
