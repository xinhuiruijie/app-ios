//
//  ArticleListCell.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "ArticleListCell.h"

@interface ArticleListCell ()
@property (weak, nonatomic) IBOutlet UIImageView *articlePhoto;
@property (weak, nonatomic) IBOutlet UILabel *articleTitle;
@property (weak, nonatomic) IBOutlet UILabel *articleSubtitle;
@property (weak, nonatomic) IBOutlet UILabel *viewCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *likeCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *shareCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end

@implementation ArticleListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.shareCountLabel.text = @"";
}

- (void)dataDidChange {
    ARTICLE *article = self.data;
    
    [self.articlePhoto setImageWithPhoto:article.photo];
    self.articleTitle.text = article.title?:@"";
    self.articleSubtitle.attributedText = [AppTheme detailAttributedString:article.subtitle];
    self.articleSubtitle.lineBreakMode = NSLineBreakByTruncatingTail;
    
    self.usernameLabel.text = article.resource ? [article.resource formatNameWithBy] : @"";
    self.dateLabel.text = [[article.created_at stringFromTimestamp] stringByAppendingString:@"发布"];
    
    self.viewCountLabel.text = [NSString stringWithFormat:@"%@", article.view_count ? [article.view_count setupCountNumber] : @(0)];
    self.likeCountLabel.text = [NSString stringWithFormat:@"%@", article.like_count ? [article.like_count setupCountNumber] : @(0)];
//    self.shareCountLabel.text = [NSString stringWithFormat:@"%@", article.share_count ? [article.share_count setupCountNumber] : @(0)];
}

@end
