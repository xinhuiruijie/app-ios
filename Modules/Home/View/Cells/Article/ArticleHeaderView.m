//
//  ArticleHeaderView.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "ArticleHeaderView.h"

@interface ArticleHeaderView ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *viewCountLabel;

@end

@implementation ArticleHeaderView

+ (CGFloat)heightForArticleHeader:(ARTICLE *)article {
    CGFloat constHeight = 46;
    CGFloat titleHeight = [AppTheme calculateHeigthWithAttributeContent:[AppTheme titleAttributedString:article.title] width:(kScreenWidth - 30)];
    return constHeight + titleHeight;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    ARTICLE *article = self.data;
    CGFloat constHeight = 46;
    CGFloat titleHeight = [AppTheme calculateHeigthWithAttributeContent:[AppTheme titleAttributedString:article.title] width:(kScreenWidth - 30)] ;
    self.size = CGSizeMake(kScreenWidth, constHeight + titleHeight);
}

- (void)dataDidChange {
    ARTICLE *article = self.data;
    self.titleLabel.attributedText = [AppTheme titleAttributedString:article.title];
    self.subtitleLabel.text = [NSString stringWithFormat:@"%@ %@", article.resource, [article.created_at stringFromTimestamp]];
//    self.viewCountLabel.text = [NSString stringWithFormat:@"%@", article.view_count ? [article.view_count setupCountNumber] : @(0)];
    self.viewCountLabel.text = [NSString stringWithFormat:@"%@", article.view_count?:@"0"];
}

@end
