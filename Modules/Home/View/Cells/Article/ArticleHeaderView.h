//
//  ArticleHeaderView.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ArticleHeaderView : UIView

+ (CGFloat)heightForArticleHeader:(ARTICLE *)article;

@end
