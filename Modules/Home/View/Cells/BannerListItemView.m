//
//  BannerListItemView.m
//  MotherPlanet
//
//  Created by GeekZooStudio on 2018/4/11.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "BannerListItemView.h"

@interface BannerListItemView()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imgView;

@property (weak, nonatomic) IBOutlet UIView *hotView;
@end

@implementation BannerListItemView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.hotView.layer.cornerRadius = 2;
}

- (void)dataDidChange {
    if (![self.data isKindOfClass:[BANNER class]]) return;
    
    BANNER *banner = self.data;
    
    self.titleLabel.text = banner.title;
    self.subtitleLabel.text = banner.subtitle;
    [self.imgView setImageWithPhoto:banner.photo];
}

@end
