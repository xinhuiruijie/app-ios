//
//  CategoryListCell.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "CategoryListCell.h"

@interface CategoryListCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *categoryPhoto;

@end

@implementation CategoryListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.categoryPhoto.layer.cornerRadius = 2;
    self.categoryPhoto.layer.masksToBounds = YES;
}

- (void)dataDidChange {
    CATEGORY *category = self.data;
    self.titleLabel.text = category.title?:@"";
    [self.categoryPhoto setImageWithPhoto:category.photo placeholderImage:[AppTheme placeholderImage]];
}

@end
