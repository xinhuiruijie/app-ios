//
//  AuthorInfoCoverView.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "AuthorInfoCoverView.h"

@interface AuthorInfoCoverView ()

@property (weak, nonatomic) IBOutlet UIImageView *userCoverPhoto;
@property (weak, nonatomic) IBOutlet UIView *avaterBackView;
@property (weak, nonatomic) IBOutlet UIImageView *avaterPhoto;
@property (weak, nonatomic) IBOutlet UILabel *avaterName;
@property (weak, nonatomic) IBOutlet UILabel *avaterIntroduce;
@property (weak, nonatomic) IBOutlet UIButton *followButton;

@end

@implementation AuthorInfoCoverView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.avaterBackView.layer.cornerRadius = 41;
    self.avaterBackView.clipsToBounds = YES;
    self.avaterPhoto.layer.cornerRadius = 40;
    self.avaterPhoto.clipsToBounds = YES;
}

- (void)dataDidChange {
    USER *user = self.data;
    [self.userCoverPhoto setImageWithPhoto:user.cover_photo placeholderImage:[UIImage imageNamed:@"bg_lab"]];
    
    [self.avaterPhoto setImageWithPhoto:user.avatar placeholderImage:[AppTheme avatarDefaultImage]];
    
    self.avaterName.text = user.name?:@"";
    self.avaterIntroduce.text = user.introduce?:@"";
    
    if (user == nil || [user.id isEqualToString:[UserModel sharedInstance].user.id]) {
        self.followButton.hidden = YES;
    } else {
        self.followButton.hidden = NO;
        if (user.is_follow && user.is_author) {
            [self.followButton setImage:[UIImage imageNamed:@"icon_following_author"] forState:UIControlStateNormal];
        } else {
            [self.followButton setImage:[UIImage imageNamed:@"icon_follow_author"] forState:UIControlStateNormal];
        }
    }
}

- (IBAction)followAvaterAction:(id)sender {
    if (self.followAuthor) {
        self.followAuthor(sender);
    }
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    UIView *result = [super hitTest:point withEvent:event];
    if ([result isKindOfClass:[UIButton class]]) {
        return result;
    } else if (result.superview == self) {
        return nil;
    } else {
        return result;
    }
}

@end
