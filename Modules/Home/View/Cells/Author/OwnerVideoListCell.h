//
//  OwnerVideoListCell.h
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/9.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OwnerVideoListCell : UITableViewCell

@property (nonatomic, assign) BOOL firstCell;

@end
