//
//  AuthorVideoListCell.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "AuthorVideoListCell.h"

@interface AuthorVideoListCell ()

@property (weak, nonatomic) IBOutlet UILabel *videoCreateTime;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *videoPhoto;
@property (weak, nonatomic) IBOutlet UILabel *videoTime;
@property (weak, nonatomic) IBOutlet UILabel *videoPlayCount;
@property (weak, nonatomic) IBOutlet UILabel *videoLikeCount;
@property (weak, nonatomic) IBOutlet UILabel *videoCollectCount;
@property (weak, nonatomic) IBOutlet UILabel *videoShareCount;
@property (weak, nonatomic) IBOutlet UILabel *videoTitle;
@property (weak, nonatomic) IBOutlet UILabel *videoDesc;

@end

@implementation AuthorVideoListCell

+ (CGFloat)heightForAuthorVideoCell:(VIDEO *)video {
    CGFloat constHeight = 103;
    CGFloat imageHeight = (kScreenWidth - 30) * 330 / 690;
    
    CGFloat contentHeight = [AppTheme calculateHeigthWithAttributeContent:[AppTheme detailAttributedString:video.subtitle] width:kScreenWidth - 30];
    CGFloat contentBoundHeight = 36;
    CGFloat labelHeight = MIN(contentHeight, contentBoundHeight);
    
    return ceil(constHeight + imageHeight + labelHeight);
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
//    [self.contentView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
//        obj.backgroundColor = [UIColor randomColor];
//    }];
}

- (void)dataDidChange {
    VIDEO *video = self.data;
    
    self.videoCreateTime.text = [video.created_at stringFromTimestamp];
    self.videoTitle.text = video.title;
    self.videoDesc.attributedText = [AppTheme detailAttributedString:video.subtitle];
    self.videoPhoto.image = [AppTheme placeholderImage];
    if (video.photo) {
        [self.videoPhoto setImageWithPhoto:video.photo];
    } else {
        self.videoPhoto.image = [AppTheme placeholderImage];
    }
    float playCount = [video.play_count floatValue];
    if (playCount >= 10000) {
        self.videoPlayCount.text = [NSString stringWithFormat:@"%0.1fW", playCount / 10000];
    } else {
        self.videoPlayCount.text = [NSString stringWithFormat:@"%@", video.play_count?:@(0)];
    }
    self.videoLikeCount.text = [NSString stringWithFormat:@"%@", video.like_count ? [video.like_count setupCountNumber] : @(0)];
    self.videoCollectCount.text = [NSString stringWithFormat:@"%@", video.collect_count ? [video.collect_count setupCountNumber] : @(0)];
    self.videoShareCount.text = [NSString stringWithFormat:@"%@", video.share_count ? [video.share_count setupCountNumber] : @(0)];
    self.videoTime.text = video.video_time;
}

@end
