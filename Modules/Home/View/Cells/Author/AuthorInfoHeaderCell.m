//
//  AuthorInfoHeaderCell.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

typedef NS_ENUM(NSUInteger, COUNT_TYPE) {
    COUNT_TYPE_FANS = 1, // 关注
    COUNT_TYPE_VIDEO = 2, // 短片数
    COUNT_TYPE_STAR = 3, // 口碑
};

#import "AuthorInfoHeaderCell.h"
#import "MSNumberScrollAnimatedView.h"

@interface AuthorInfoHeaderCell ()

@property (weak, nonatomic) IBOutlet MSNumberScrollAnimatedView *fansCountNumberAnimated;
@property (weak, nonatomic) IBOutlet MSNumberScrollAnimatedView *videoCountNumberAnimated;
@property (weak, nonatomic) IBOutlet MSNumberScrollAnimatedView *starScoreNumberAnimated;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fansCountNAWLC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *videoCountNAWLC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *starScoreNAWLC;

@end

@implementation AuthorInfoHeaderCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    [self customAnimatedView:self.fansCountNumberAnimated isIntager:YES];
    [self customAnimatedView:self.videoCountNumberAnimated isIntager:YES];
    [self customAnimatedView:self.starScoreNumberAnimated isIntager:NO];
}

- (void)dataDidChange {
    USER *author = self.data;
    
    [self animationActionWithCount:[author.fans_count doubleValue] countType:COUNT_TYPE_FANS];
    [self animationActionWithCount:[author.video_count doubleValue] countType:COUNT_TYPE_VIDEO];
    [self animationActionWithCount:[author.star_score doubleValue] countType:COUNT_TYPE_STAR];
}

#pragma mark - Custom Number Animated View

- (void)customAnimatedView:(MSNumberScrollAnimatedView *)view isIntager:(BOOL)isIntager {
    view.font = [UIFont fontWithName:@"Gotham-Medium" size:20];
    view.textColor = [AppTheme normalTextColor];
    view.minLength = 1;
    view.isIntager = isIntager;
    view.number = 0;
    [view startAnimation];
    [view stopAnimation];
}

#pragma mark - Animation Action

- (void)animationActionWithCount:(double)count countType:(COUNT_TYPE)countType {
    NSString *countString;
    NSLayoutConstraint *constraint;
    MSNumberScrollAnimatedView *animatedView;
    
    switch (countType) {
        case COUNT_TYPE_FANS: {
            countString = [NSString stringWithFormat:@"%@", [NSNumber numberWithDouble:count]];
            constraint = self.fansCountNAWLC;
            animatedView = self.fansCountNumberAnimated;
        }
            break;
            
        case COUNT_TYPE_VIDEO: {
            countString = [NSString stringWithFormat:@"%@", [NSNumber numberWithDouble:count]];
            constraint = self.videoCountNAWLC;
            animatedView = self.videoCountNumberAnimated;
        }
            break;
            
        case COUNT_TYPE_STAR: {
            countString = [NSString stringWithFormat:@"%0.1f", count];
            constraint = self.starScoreNAWLC;
            animatedView = self.starScoreNumberAnimated;
        }
            break;
            
        default:
            break;
    }
    
    constraint.constant = countString.length * 13;
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        animatedView.number = [NSNumber numberWithDouble:count];
        [animatedView startAnimation];
        if (count == 0.0) {
            [animatedView stopAnimation];
        }
    });
}

@end
