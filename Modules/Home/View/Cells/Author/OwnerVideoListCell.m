//
//  OwnerVideoListCell.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/9.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "OwnerVideoListCell.h"

@interface OwnerVideoListCell ()

@property (weak, nonatomic) IBOutlet UILabel *newsVideo;
@property (weak, nonatomic) IBOutlet UIImageView *videoPhoto;
@property (weak, nonatomic) IBOutlet UILabel *createdAt;
@property (weak, nonatomic) IBOutlet UILabel *playCount;
@property (weak, nonatomic) IBOutlet UILabel *category;
@property (weak, nonatomic) IBOutlet UILabel *videoTime;
@property (weak, nonatomic) IBOutlet UILabel *videoTitle;
@property (weak, nonatomic) IBOutlet UILabel *star;

@end

@implementation OwnerVideoListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.category.layer.cornerRadius = 2;
    self.category.clipsToBounds = YES;
}

- (void)setFirstCell:(BOOL)firstCell {
    self.newsVideo.hidden = firstCell;
}

- (void)dataDidChange {
    VIDEO *video = self.data;
    
    [self.videoPhoto setImageWithPhoto:video.photo placeholderImage:[AppTheme placeholderImage]];
    self.createdAt.text = [video.created_at stringFromTimestamp];
    float videoPlayCount = [video.play_count floatValue];
    if (videoPlayCount >= 10000) {
        self.playCount.text = [NSString stringWithFormat:@"%0.1fW", videoPlayCount / 10000];
    } else {
        self.playCount.text = [NSString stringWithFormat:@"%@", video.play_count?:@(0)];
    }
    self.category.text = video.category.title ? [NSString stringWithFormat:@" #%@ ", video.category.title] : @"";
    self.videoTime.text = video.video_time?:@"";
    self.videoTitle.text = video.title?:@"";
    self.star.text = [NSString stringWithFormat:@"%0.1f", video.star?:0.0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
