//
//  TopicVideoCell.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/11/30.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "TopicVideoCell.h"
#import "VIDEO+Extension.h"

@interface TopicVideoCell () <UIGestureRecognizerDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *videoAvatar;
@property (weak, nonatomic) IBOutlet UILabel *videoTitle;
@property (weak, nonatomic) IBOutlet UILabel *videoDesc;

@property (weak, nonatomic) IBOutlet UIView *playerView;
@property (weak, nonatomic) IBOutlet UIView *videoPlayView;
@property (weak, nonatomic) IBOutlet UIButton *transparentButton;

@property (weak, nonatomic) IBOutlet UIView *noPlayView;
@property (weak, nonatomic) IBOutlet UIButton *playButton;
@property (weak, nonatomic) IBOutlet UIView *videoTimeBackView;
@property (weak, nonatomic) IBOutlet UILabel *videoTime;

@end

@implementation TopicVideoCell

+ (CGFloat)heightForTopicVideoItemRow {
    CGFloat imageHeight = ceil(kScreenWidth * 211 / 375);
    return (imageHeight + 63);
}

+ (CGFloat)heightForTopicVideoItemRowWithSubtitle:(TOPIC_VIDEO *)topicVideo {
    CGFloat videoHeight = ceil(kScreenWidth * 211 / 375);
    CGFloat constantHeight = videoHeight + 41;
    
    CGFloat maxWidth = kScreenWidth - 30;
    CGFloat titleHeight = [topicVideo.title heightWithWidth:maxWidth attributes:@{NSFontAttributeName: [UIFont systemFontOfSize:16 weight:UIFontWeightMedium]}];
    CGFloat subtitleHeight = [topicVideo.subtitle heightWithWidth:maxWidth font:12];
    
    return ceil(constantHeight + titleHeight + subtitleHeight);
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.videoView.layer.cornerRadius = 2;
    self.videoView.layer.masksToBounds = YES;
    
    self.videoTimeBackView.layer.cornerRadius = 2;
    self.videoTimeBackView.layer.masksToBounds = YES;
}

- (void)dataDidChange {
    TOPIC_VIDEO *topicVideo = self.data;
    
    [self.videoAvatar setImageWithPhoto:topicVideo.video.photo placeholderImage:[AppTheme placeholderImage]];
    self.videoTitle.text = topicVideo.title?:@"";
    self.videoDesc.text = topicVideo.subtitle?:@"";
    self.videoTime.text = topicVideo.video.video_time ? [NSString stringWithFormat:@" %@ ", topicVideo.video.video_time] : @"";
}

- (void)dealloc {
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
}

#pragma mark - gensture Action

- (IBAction)transparentButtonAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(playVideoButtonAciton:)]) {
        [self.delegate playVideoButtonAciton:self];
    }
}

#pragma mark - play

- (void)initState {
    self.playButton.selected = NO;
    
    self.noPlayView.alpha = 1;
}

@end
