//
//  TopicVideoStarAlertView.h
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/6/7.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopicVideoStarAlertView : UIView

@property (nonatomic, copy) void(^cancelAction)(void);
@property (nonatomic, copy) void(^confirmAction)(NSInteger starValue);

- (void)show;
- (void)hide;
- (void)confirmActionAnimated;

- (void)normalTheme;
- (void)darkTheme;

@end
