//
//  TopicListCell.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "TopicListCell.h"

@interface TopicListCell ()

@property (weak, nonatomic) IBOutlet UIView *dataView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subTitleLabel;

@end

@implementation TopicListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.dataView.layer.cornerRadius = 2;
    self.dataView.layer.masksToBounds = YES;
}

- (void)dataDidChange {
    TOPIC *topic = self.data;
    self.titleLabel.text = topic.title?:@"";
    self.subTitleLabel.text = topic.subtitle?:@"";
    [self.coverPhoto setImageWithPhoto:topic.photo placeholderImage:[AppTheme placeholderImage]];
}

- (void)startSingleViewAnimation {
    CATransform3D rotation = CATransform3DMakeTranslation(0 ,20 ,20);//3D旋转
    rotation.m34 = 1.0/ -600;

    self.dataView.layer.shadowOffset = CGSizeMake(10, 10);
    self.dataView.layer.transform = rotation;
    self.dataView.alpha = 0;
    self.titleLabel.layer.shadowOffset = CGSizeMake(10, 10);
    self.titleLabel.layer.transform = rotation;
    self.titleLabel.alpha = 0;
    self.subTitleLabel.layer.shadowOffset = CGSizeMake(10, 10);
    self.subTitleLabel.layer.transform = rotation;
    self.subTitleLabel.alpha = 0;
    self.coverPhoto.alpha = 0;

    [UIView beginAnimations:@"rotation" context:NULL];
    //旋转时间
    [UIView setAnimationDuration:0.6];
    self.dataView.layer.shadowOffset = CGSizeMake(0, 0);
    self.dataView.layer.transform = CATransform3DIdentity;
    self.dataView.alpha = 1;
    self.titleLabel.layer.shadowOffset = CGSizeMake(0, 0);
    self.titleLabel.layer.transform = CATransform3DIdentity;
    self.titleLabel.alpha = 1;
    self.coverPhoto.alpha = 1;
    self.alpha = 1;

    [UIView setAnimationDelay:0.1];
    [UIView setAnimationDuration:0.8];
    self.subTitleLabel.layer.shadowOffset = CGSizeMake(0, 0);
    self.subTitleLabel.layer.transform = CATransform3DIdentity;
    self.subTitleLabel.alpha = 1;

    [UIView commitAnimations];
}

@end
