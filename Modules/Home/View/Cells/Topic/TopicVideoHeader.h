//
//  TopicVideoHeader.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/11/30.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TopicVideoHeader : UIView

@property (weak, nonatomic) IBOutlet UIImageView *coverPhoto;

@property (nonatomic, copy) void(^gotoAuthorInfo)(USER *author);

- (void)startAnimation;

@end
