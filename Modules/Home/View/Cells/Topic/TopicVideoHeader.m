//
//  TopicVideoHeader.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/11/30.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "TopicVideoHeader.h"

@interface TopicVideoHeader ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UILabel *authorsLabel;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImage;

@property (nonatomic, assign) CGFloat headerHeight;

@end

@implementation TopicVideoHeader

- (void)awakeFromNib {
    [super awakeFromNib];
    
//    self.collectButton.layer.borderWidth = [AppTheme onePixel];
    
    self.avatarImage.layer.cornerRadius = 30;
    self.avatarImage.clipsToBounds = YES;
    
    if ([SDiOSVersion deviceSize] == Screen5Dot8inch) {
        self.headerHeight = ceil(kScreenWidth * 420 / 750) + 20;
    } else {
        self.headerHeight = ceil(kScreenWidth * 420 / 750);
    }
    // 首页 push 进入后先隐藏后动画展示文字
    self.titleLabel.alpha = 0;
    self.descLabel.alpha = 0;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.frame = CGRectMake(0, 0, kScreenWidth, 30 + self.headerHeight);
}

- (void)dataDidChange {
    TOPIC *topic = self.data;
    
    [self.coverPhoto setImageWithPhoto:topic.photo placeholder:[AppTheme defaultPlaceholder]];
    self.titleLabel.text = topic.title?:@"";
    self.descLabel.text = topic.subtitle?:@"";
    self.authorsLabel.text = [NSString stringWithFormat:@"by:%@",topic.owner.name?:@""];
    [self.avatarImage setImageWithPhoto:topic.owner.avatar placeholderImage:[AppTheme placeholderImage]];
}

- (void)startAnimation {
    CATransform3D rotation = CATransform3DMakeTranslation(0 ,20 ,20);//3D旋转
    rotation.m34 = 1.0/ -600;
    
    self.titleLabel.layer.shadowOffset = CGSizeMake(10, 10);
    self.titleLabel.layer.transform = rotation;
    self.titleLabel.alpha = 0;
    self.descLabel.layer.shadowOffset = CGSizeMake(10, 10);
    self.descLabel.layer.transform = rotation;
    self.descLabel.alpha = 0;
    
    [UIView beginAnimations:@"rotation" context:NULL];
    //旋转时间
    [UIView setAnimationDuration:0.6];
    self.titleLabel.layer.shadowOffset = CGSizeMake(0, 0);
    self.titleLabel.layer.transform = CATransform3DIdentity;
    self.titleLabel.alpha = 1;
    
    [UIView setAnimationDelay:0.1];
    [UIView setAnimationDuration:0.8];
    self.descLabel.layer.shadowOffset = CGSizeMake(0, 0);
    self.descLabel.layer.transform = CATransform3DIdentity;
    self.descLabel.alpha = 1;
    
    [UIView setAnimationDelay:0.1];
    [UIView setAnimationDuration:0.8];
    
    [UIView commitAnimations];
    // 首页 push 进入后先隐藏后动画展示文字，动画结束后保持显示状态固定
    self.titleLabel.alpha = 1;
    self.descLabel.alpha = 1;
}

- (IBAction)gotoAuthorInfo:(UIButton *)sender {
    TOPIC *topic = self.data;
    if (self.gotoAuthorInfo) {
        self.gotoAuthorInfo(topic.owner);
    }
}

@end
