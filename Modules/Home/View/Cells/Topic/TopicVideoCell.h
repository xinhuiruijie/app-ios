//
//  TopicVideoCell.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/11/30.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VideoState.h"
@class TopicVideoCell;

/**
 * The style of cell cannot stop in screen center.
 * 播放滑动不可及cell的类型
 */
typedef NS_OPTIONS(NSInteger, JPPlayUnreachCellStyle) {
    JPPlayUnreachCellStyleNone = 1 << 0,  // normal 播放滑动可及cell
    JPPlayUnreachCellStyleUp = 1 << 1,    // top 顶部不可及
    JPPlayUnreachCellStyleDown = 1<< 2    // bottom 底部不可及
};

@protocol TopicVideoCellDelegate <NSObject>

@optional

- (void)playVideoButtonAciton:(TopicVideoCell *)cell;

@end

@interface TopicVideoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *videoView;

/** videoPath */
@property (nonatomic, strong) NSString *videoUrl;

/** cell类型 */
@property(nonatomic, assign) JPPlayUnreachCellStyle cellStyle;

@property (nonatomic, weak) id<TopicVideoCellDelegate> delegate;

+ (CGFloat)heightForTopicVideoItemRow;
+ (CGFloat)heightForTopicVideoItemRowWithSubtitle:(TOPIC_VIDEO *)topicVideo;
- (void)cellVideoStartPlay;
- (void)cellVideoStopPlay;

@end
