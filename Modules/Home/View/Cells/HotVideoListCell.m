//
//  HotVideoListCell.m
//  MotherPlanet
//
//  Created by GeekZooStudio on 2018/4/12.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "HotVideoListCell.h"

@interface HotVideoListCell()
@property (weak, nonatomic) IBOutlet UIImageView *imgView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgViewWidthLC;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *userLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *starLabel;
@property (weak, nonatomic) IBOutlet UIView *lineView;
@property (weak, nonatomic) IBOutlet UILabel *viewCountLabel;

@end

@implementation HotVideoListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

+ (CGFloat)heightForRowWith:(VIDEO *)video {
    CGFloat constHeight = 15;
    
    CGFloat imgWidth = 150 / 375.0 * kScreenWidth;
    CGFloat imgHeight = imgWidth / 150 * 85;
    
    return constHeight + imgHeight;
}

- (void)dataDidChange {
    if (![self.data isKindOfClass:[VIDEO class]]) return;
    
    VIDEO *video = self.data;
    [self.imgView setImageWithPhoto:video.photo];
    self.imgViewWidthLC.constant = 150 / 375.0 * kScreenWidth;
    self.titleLabel.text = video.title?:@"";
    self.userLabel.text = video.owner.name ? [video.owner.name formatNameWithBy] : @"";
    self.timeLabel.text = video.video_time?:@"";
    self.starLabel.text = [NSString stringWithFormat:@"%.1f", video.star?:0.0];
    self.viewCountLabel.text = [NSString stringWithFormat:@"%@", video.play_count?:@(0)];
}

- (void)hideBottomLine:(BOOL)hide {
    self.lineView.hidden = hide;
}

@end
