//
//  CategoryTaxisListCell.m
//  MotherPlanet
//
//  Created by GeekZooStudio on 2018/4/10.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "CategoryTaxisListCell.h"

@interface CategoryTaxisListCell()
@property (weak, nonatomic) IBOutlet UIImageView *iconImgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation CategoryTaxisListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.iconImgView.layer.cornerRadius = 16;
    self.iconImgView.layer.masksToBounds = YES;
}

- (void)dataDidChange {
    if (![self.data isKindOfClass:[CATEGORY class]]) {
        return;
    }
    
    CATEGORY *category = self.data;
    
    [self.iconImgView setImageWithPhoto:category.photo placeholderImage:[AppTheme placeholderImage]];
    
    self.titleLabel.text = category.title?:@"";
}

@end
