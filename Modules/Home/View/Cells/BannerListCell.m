//
//  BannerListCell.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "BannerListCell.h"
#import "XLCycleScrollView.h"
#import "BannerListItemView.h"
#import "PlanetBannerListItemView.h"

@interface BannerListCell () <XLCycleScrollViewDatasource, XLCycleScrollViewDelegate>

@property (weak, nonatomic) IBOutlet XLCycleScrollView *bannerView;
@property (nonatomic, strong) NSTimer *timer;

@end

@implementation BannerListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.bannerView.delegate = self;
    self.bannerView.datasource = self;
//    self.bannerView.layer.cornerRadius = 2;
//    self.bannerView.layer.masksToBounds = YES;
    self.bannerView.indicatorAlign = IndictarAlignCenter;
    self.bannerView.indicatorBottomMargin = 20;
    
    self.bannerView.pageControl.currentBackgroundColor = [AppTheme selectedColor];
    self.bannerView.pageControl.unSelectedBackgroundColor = [AppTheme normalColor];
    self.bannerView.pageControl.currentBorderColor = [AppTheme selectedColor];
    self.bannerView.pageControl.pageBorderColor = [AppTheme normalColor];
    self.bannerView.pageControl.indicatorSize = CGSizeMake(6, 6);
    self.bannerView.pageControl.indicatorCornerRadius = 3;
    self.bannerView.pageControl.spacing = 8.f;
    
    @weakify(self)
    self.bannerView.whenSelectedIndex = ^(NSInteger index) {
        @strongify(self)
//        BANNER *banner = self.data[self.bannerView.currentPage];
//        if (self.delegate && [self.delegate respondsToSelector:@selector(bannerWillDisplayWithData:)]) {
//            [self.delegate bannerWillDisplayWithData:banner];
//        }
    };
}

#pragma mark - XLCycleScrollViewDatasource

- (NSInteger)numberOfPages {
    if (self.data && [self.data isKindOfClass:[NSArray class]]) {
        NSArray *datas = self.data;
        return datas.count;
    }
    return 0;
}

- (UIView *)pageAtIndex:(NSInteger)index {
    UIView *aView = [[UIView alloc] initWithFrame:self.bounds];
    if (self.data && [self.data isKindOfClass:[NSArray class]]) {
        NSArray *datas = self.data;
        if (datas.count) {
            if (index < datas.count) {
                BANNER *banner = datas[index];
                
//                UIImageView *imageView = [[UIImageView alloc] initWithFrame:self.bannerView.bounds];
//                imageView.contentMode = UIViewContentModeScaleAspectFill;
//                imageView.clipsToBounds = YES;
//                [imageView setImageWithPhoto:banner.photo];
//                [aView addSubview:imageView];
                if (self.bannerType == BANNER_TYPE_HOME) {
                    BannerListItemView *view = [BannerListItemView loadFromNibWithFrame:self.bounds];
                    view.data = banner;
                    [aView addSubview:view];
                } else if (self.bannerType == BANNER_TYPE_PLANET) {
                    PlanetBannerListItemView *view = [PlanetBannerListItemView loadFromNibWithFrame:self.bounds];
                    view.data = banner;
                    [aView addSubview:view];
                }
            }
        }
    }
    return aView;
}

#pragma mark - XLCycleScrollViewDelegate

- (void)gestureRecognizerStateChanged:(UIGestureRecognizerState)state {
    if (state == UIGestureRecognizerStateBegan) {
        [self stop];
    } else if (state == UIGestureRecognizerStateEnded ||
               state == UIGestureRecognizerStateCancelled ||
               state == UIGestureRecognizerStateFailed) {
        [self start];
    }
}

- (void)didClickPage:(XLCycleScrollView *)csView atIndex:(NSInteger)index {
    if (self.data && [self.data isKindOfClass:[NSArray class]]) {
        NSArray *datas = self.data;
        if (datas.count > 0) {
            BANNER *banner = datas[index];
            if (self.bannerType == BANNER_TYPE_HOME) {
                [AppAnalytics clickEvent:@"click_star_banner"];
            } else if (self.bannerType == BANNER_TYPE_PLANET) {
                [AppAnalytics clickEvent:@"click_planet_banner"];
            }
            if (banner.link && banner.link.length) {
                if (![DeepLink handleURLString:banner.link withCompletion:nil]) {
                    [[UIApplication sharedApplication].keyWindow presentFailureTips:@"无法打开的链接"];
                }
            }
        }
    }
}

- (void)dataDidChange {
    if (self.data) {
        NSArray *datas = self.data;
        if (datas.count <= 1) {
            [self stop];
        } else {
            [self start];
        }
        [self.bannerView reloadData];
        
        self.bannerView.pageControl.currentBackgroundColor = [AppTheme selectedColor];
        self.bannerView.pageControl.unSelectedBackgroundColor = [AppTheme normalColor];
        self.bannerView.pageControl.currentBorderColor = [AppTheme selectedColor];
        self.bannerView.pageControl.pageBorderColor = [AppTheme normalColor];
        self.bannerView.pageControl.indicatorSize = CGSizeMake(6, 6);
        self.bannerView.pageControl.indicatorCornerRadius = 3;
        self.bannerView.pageControl.spacing = 8.f;
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
//    self.bannerView.frame = CGRectMake(15, 10, self.frame.size.width - 30, self.frame.size.height - 10);
    self.bannerView.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
}

- (void)start {
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
    self.timer = [NSTimer scheduledTimerWithTimeInterval:5 target:self selector:@selector(updateCenter) userInfo:nil repeats:YES];
}

- (void)updateCenter {
    [self.bannerView selectNextView];
}

- (void)stop {
    [self.timer invalidate];
}

- (void)dealloc {
    [self stop];
}

@end
