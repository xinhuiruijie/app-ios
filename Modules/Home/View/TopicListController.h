//
//  TopicListController.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "ListViewController.h"
@class TopicListCell;

@interface TopicListController : ListViewController

@property (nonatomic, strong) TopicListCell *currentCell;

@end
