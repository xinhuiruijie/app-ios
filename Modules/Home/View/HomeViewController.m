//
//  HomeViewController.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "HomeViewController.h"
#import "VideoInfoController.h"
#import "TopicListController.h"
#import "TopicVideoListController.h"
#import "AuthorInfoController.h"
#import "SearchViewController.h"

#import "HomeVideoListModel.h"
#import "BannerListModel.h"
#import "HomeListModel.h"
#import "AuthorModel.h"

#import "BannerListCell.h"
#import "HomeTopicCell.h"
#import "VideoListCell.h"
#import "HomeVideoHeader.h"
#import "DateAnimatedView.h"
#import "FakeSearchTitleView.h"
#import "ZLInsetLabel.h"

@interface HomeViewController () <UITableViewDataSource, UITableViewDelegate, HomeTopicCellDelegate, VideoListCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIImageView *placeHolderImage;

@property (nonatomic, strong) BannerListModel *bannerModel;
@property (nonatomic, strong) HomeListModel *homeListModel;
@property (nonatomic, strong) HomeVideoListModel *videoListModel;

@property (nonatomic, strong) NSMutableArray *cellIdentifies;

@property (nonatomic, strong) DateAnimatedView *dateAnimatedView;

@property (nonatomic, assign) BOOL isLaunchAnimating;
@property (nonatomic, assign) BOOL showImageAnimation;
@property (nonatomic, assign) BOOL specialCellAnimation;
@property (nonatomic, assign) BOOL specialHeaderAnimation;
@property (nonatomic, assign) BOOL scrolled;
@property (nonatomic, strong) NSDate *lastDate;

@property (nonatomic, assign) int lastPosition;
@property (nonatomic, assign) BOOL isDownScroll;

@property (nonatomic, assign) CGFloat insetTop;

@property (nonatomic, strong) ZLInsetLabel *copywritingLabel;
@property (nonatomic, strong) dispatch_group_t group_t;
@end

@implementation HomeViewController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Home"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self modelInit];
    [self cellAnimationStateInit];
    
    self.cellIdentifies = [NSMutableArray array];
    self.tableView.estimatedRowHeight = 0;
    self.tableView.backgroundColor = [AppTheme listBackgroundColor];
    [self.tableView registerNib:[BannerListCell nib] forCellReuseIdentifier:@"BannerListCell"];
    [self.tableView registerNib:[HomeTopicCell nib] forCellReuseIdentifier:@"HomeTopicCell"];
    [self.tableView registerNib:[VideoListCell nib] forCellReuseIdentifier:@"VideoListCell"];
    
    // Splash结束后调用开始动画
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(subViewStartAnimation) name:APP_SPLASH_HIDE object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopLaunchAnimating) name:APP_LAUNCH_ANIMATION_HIDE object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadVideosAuthor:) name:kAuthorFollowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadVideosAuthor:) name:kAuthorUnfollowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadVideos:) name:kVideoInfoRefreshNotification object:nil];
    
    [self.videoListModel refresh];
    
    [self getListView].contentInsets_top = self.insetTop;
}

#pragma mark - HomePageItemViewControllerProtocol

- (UIScrollView *)getListView {
    return self.tableView;
}

- (void)setListViewInsetTop:(CGFloat)insetTop {
    self.insetTop = insetTop;
    [self getListView].contentInsets_top = self.insetTop;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.bannerModel refresh];
    [self.homeListModel refresh];
    // 将要显示的时候开始banner轮播
    if (self.cellIdentifies.count > 0 && [self.cellIdentifies containsObject:@"BannerListCell"]) {
        BannerListCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:[self.cellIdentifies indexOfObject:@"BannerListCell"]]];
        if ([cell isKindOfClass:[BannerListCell class]]) {
            [cell start];
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    // 第二次进入页面时没有刷新动效，除非手动滑动
    self.isDownScroll = NO;
    self.scrolled = YES;
    // 将要消失的时候停止banner轮播
    if (self.cellIdentifies.count > 0 && [self.cellIdentifies containsObject:@"BannerListCell"]) {
        BannerListCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:[self.cellIdentifies indexOfObject:@"BannerListCell"]]];
        if ([cell isKindOfClass:[BannerListCell class]]) {
            [cell stop];
        }
    }
}

- (void)cellAnimationStateInit {
    self.isLaunchAnimating = YES;
    self.showImageAnimation = YES;
    self.specialCellAnimation = YES;
    self.specialHeaderAnimation = YES;
    self.scrolled = NO;
    self.isDownScroll = YES;
}

- (void)subViewStartAnimation {
    self.isDownScroll = YES;
    if (self.lastDate) {
        NSDate *nowDate = [NSDate date];
        NSTimeInterval time = [nowDate timeIntervalSinceDate:self.lastDate];
        if (time < 1.2) {
            for (UITableViewCell *cell in [self.tableView visibleCells]) {
                [NSObject cancelPreviousPerformRequestsWithTarget:cell selector:@selector(startSingleViewAnimation) object:self];
            }
        }
    }
    self.specialCellAnimation = YES;
    self.specialHeaderAnimation = YES;
    [self.tableView reloadData];
    [self.dateAnimatedView startAnimation];
    
    // 最后一次动画时间
    self.lastDate = [NSDate date];
}

- (void)stopLaunchAnimating {
    if (self.isLaunchAnimating) {
        self.isLaunchAnimating = NO;
        [self placeHolderAnimation];
        [self subViewStartAnimation];
    }
}

- (void)placeHolderAnimation {
    if (self.showImageAnimation && self.bannerModel.loaded && self.homeListModel.loaded && self.videoListModel.loaded) {
        self.placeHolderImage.alpha = 1;
        self.tableView.alpha = 0;
        self.showImageAnimation = NO;
        [UIView animateWithDuration:0.6 animations:^{
            self.placeHolderImage.alpha = 0;
            self.tableView.alpha = 1;
        } completion:nil];
    }
}

- (void)modelInit {
    @weakify(self)
    self.bannerModel = [[BannerListModel alloc] init];
    self.bannerModel.banner_type = BANNER_TYPE_HOME;
    self.bannerModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        if (self.group_t) {
            dispatch_group_leave(self.group_t);
        } else {
            [self.tableView.header endRefreshing];
        }
        self.isDownScroll = NO;
        
        [self reloadList];
        [self placeHolderAnimation];
        if (error) {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
    };
    
    self.homeListModel = [[HomeListModel alloc] init];
    self.homeListModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        if (self.group_t) {
            dispatch_group_leave(self.group_t);
        } else {
            [self.tableView.header endRefreshing];
        }
        self.isDownScroll = NO;
        
        [self reloadList];
        [self placeHolderAnimation];
        if (error) {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
    };
    
    self.videoListModel = [[HomeVideoListModel alloc] init];
    self.videoListModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        if (self.group_t) {
            dispatch_group_leave(self.group_t);
        } else {
            [self.tableView.header endRefreshing];
        }
        self.isDownScroll = NO;
        
        [self reloadList];
        [self placeHolderAnimation];
        if (error == nil) {
            [self setupRefreshFooterView];
            if (self.videoListModel.more) {
                [self.tableView.footer endRefreshing];
            } else {
                [self.tableView.footer noticeNoMoreData];
            }
        } else {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
    };
    
    [self setupRefreshHeaderView];
}

// 刷新短片个数特定文案
- (void)updateVideoCount {
    @weakify(self)
    [self.videoListModel updateVideoCountCompleted:^(STIHTTPResponseError *error) {
        @strongify(self)
        
        self.group_t = dispatch_group_create();
        dispatch_group_enter(self.group_t);
        dispatch_group_enter(self.group_t);
        dispatch_group_enter(self.group_t);
        
        [self.bannerModel refresh];
        [self.homeListModel refresh];
        [self.videoListModel refresh];
        
        dispatch_group_notify(self.group_t, dispatch_get_main_queue(), ^{
            self.group_t = nil;
            [self.tableView.header endRefreshing];
            // header上去之后再显示文字
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(MJRefreshSlowAnimationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self showUpdateVideoCountCopywriting];
            });
        });
    }];
}

- (void)showUpdateVideoCountCopywriting {
    if (!self.copywritingLabel) {
        self.copywritingLabel = [[ZLInsetLabel alloc] init];
        self.copywritingLabel.textAlignment = NSTextAlignmentCenter;
        self.copywritingLabel.backgroundColor = [AppTheme textSelectedColor];
        self.copywritingLabel.textColor = [AppTheme normalTextColor];
        self.copywritingLabel.font = [UIFont systemFontOfSize:12];
    }
    
    if (self.copywritingLabel.superview) {
        return;
    }
    
    [self.view addSubview:self.copywritingLabel];
    self.copywritingLabel.text = self.videoListModel.copywriting ? : self.videoListModel.updateVideoCount;
    [self.copywritingLabel sizeToFit];
    self.copywritingLabel.frame = CGRectMake(0, 0, self.copywritingLabel.width + 24, 27);
    self.copywritingLabel.center = CGPointMake(self.view.width * 0.5, [self getListView].contentInsets_top - self.copywritingLabel.height * 0.5);
    self.copywritingLabel.alpha = 0.5;
    
    self.copywritingLabel.layer.cornerRadius = self.copywritingLabel.height * 0.5;
    self.copywritingLabel.layer.masksToBounds = YES;
    
    [UIView animateWithDuration:0.25 animations:^{
        self.copywritingLabel.alpha = 1.0;
        self.copywritingLabel.transform = CGAffineTransformMakeTranslation(0, self.copywritingLabel.height + StatusBarHeight + 10);
        
    } completion:^(BOOL finished) {
        // 延迟0.5秒 隐藏
        [UIView animateWithDuration:0.25 delay:0.5 options:UIViewAnimationOptionCurveLinear animations:^{
            self.copywritingLabel.alpha = 0.0;
        } completion:^(BOOL finished) {
            self.copywritingLabel.transform = CGAffineTransformIdentity;
            [self.copywritingLabel removeFromSuperview];
        }];
    }];
}

- (void)reloadList {
    [self.cellIdentifies removeAllObjects];
    
    if (!self.bannerModel.isEmpty) {
        [self.cellIdentifies addObject:@"BannerListCell"];
    }
    if (self.homeListModel.topics && self.homeListModel.topics.count) {
        [self.cellIdentifies addObject:@"HomeTopicCell"];
    }
    if (!self.videoListModel.isEmpty) {
        [self.cellIdentifies addObject:@"VideoListCell"];
    }
    
    [self.tableView reloadData];
}

#pragma mark - UIBarButtonItems

- (UIBarButtonItem *)dateAnimatedViewItem {
    DateAnimatedView *dateView = [[DateAnimatedView alloc] initWithFrame:CGRectMake(0, 0, 60, 20)];
    self.dateAnimatedView = dateView;
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:dateView];
    return leftItem;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.cellIdentifies.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *identifier = self.cellIdentifies[section];
    if ([identifier isEqualToString:@"VideoListCell"]) {
        return self.videoListModel.items.count;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = self.cellIdentifies[indexPath.section];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    if ([identifier isEqualToString:@"BannerListCell"]) {
        cell.data = self.bannerModel.items;
        ((BannerListCell *)cell).bannerType = BANNER_TYPE_HOME;
    } else if ([identifier isEqualToString:@"HomeTopicCell"]) {
        cell.data = self.homeListModel.topics;
        ((HomeTopicCell *)cell).delegate = self;
    } else if ([identifier isEqualToString:@"VideoListCell"]) {
        VIDEO *video = self.videoListModel.items[indexPath.row];
        ((VideoListCell *)cell).delegate = self;
        cell.data = video;
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = self.cellIdentifies[indexPath.section];
    
    if ([identifier isEqualToString:@"BannerListCell"]) {
        return ceil(tableView.width * 560 / 750);
    } else if ([identifier isEqualToString:@"HomeTopicCell"]) {
        return ceil(tableView.width * 372 / 750);
    } else if ([identifier isEqualToString:@"VideoListCell"]) {
        VIDEO *video = self.videoListModel.items[indexPath.row];
        return [VideoListCell heightForVideoListCell:video];
    }
    
    return CGFLOAT_MIN;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = self.cellIdentifies[indexPath.section];
    
    if ([identifier isEqualToString:@"VideoListCell"]) {
        [AppAnalytics clickEvent:@"click_star_video"];
        VIDEO *video = self.videoListModel.items[indexPath.row];
        VideoInfoController *videoInfo = [VideoInfoController spawn];
        videoInfo.video = video;
        
        videoInfo.reloadVideo = ^(VIDEO *video) {
            for (int i = 0; i < self.videoListModel.items.count; i++) {
                VIDEO *perVideo = self.videoListModel.items[i];
                if ([perVideo.id isEqualToString:video.id]) {
                    [self.videoListModel.items replaceObjectAtIndex:i withObject:video];
                    break;
                }
            }
            [self.tableView reloadData];
        };
//        [self presentNavigationController:videoInfo];
        videoInfo.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:videoInfo animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    NSString *identifier = self.cellIdentifies[section];

    if ([identifier isEqualToString:@"VideoListCell"]) {
        return 58;
    }
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *identifier = self.cellIdentifies[section];
    
    if ([identifier isEqualToString:@"VideoListCell"]) {
        HomeVideoHeader *header = [HomeVideoHeader loadFromNib];
        header.type = VIDEO_HEADER_STAR;
        return header;
    }
    return [[UIView alloc] init];
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    // 前几个特殊的cell
    if (self.specialCellAnimation) {
        if (indexPath.row == 0) {
            cell.alpha = 0;
            NSInteger indexDelay = indexPath.section;
            [cell performSelector:@selector(startSingleViewAnimation) withObject:self afterDelay:indexDelay * 0.2];
            
            // 最后一个section时，不再展示动效
            if (indexPath.section == ([tableView numberOfSections] - 1)) {
                self.specialCellAnimation = NO;
            }
        }
    } else {
        // 处理一般的cell,即没有卡片砰砰砰效果的动效
        // 特殊情况处理：只有短片列表信息的时候，页面展示多余1个cell，需要特殊处理。判断一下当前有没有滑动过，滑动按照滑动处理来，没有滑动判断cell是否在页面上，如果默认在页面上，就要延迟执行一个动画
        if (self.isDownScroll) {
            if (indexPath.row > 0) {
                cell.alpha = 0;
                [cell startSingleViewAnimation];
            }
        } else if (!self.scrolled) {
            // 这个条件当页面滑动过，就应该永远也不执行
            // 不能在specialCellAnimation条件里写的原因是：不一定是第几个cell最后展示，
            NSInteger indexDelay = indexPath.section + indexPath.row;
            cell.alpha = 0;
            [cell performSelector:@selector(startSingleViewAnimation) withObject:self afterDelay:indexDelay * 0.2];
        }
    }
}

- (void)tableView:(UITableView *)tableView willDisplayHeaderView:(UIView *)view forSection:(NSInteger)section {
    if (self.specialHeaderAnimation) {
        view.alpha = 0;
        NSInteger sectionDelay = section;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(sectionDelay * 0.2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            self.specialHeaderAnimation = NO;
            [view startSingleViewAnimation];
        });
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat currentPostionY = scrollView.contentOffset.y;
    self.scrolled = YES;
    
    if (currentPostionY - _lastPosition > 0) {
        // 向下
        _lastPosition = currentPostionY;
        self.isDownScroll = YES;
    } else if (_lastPosition - currentPostionY > 0) {
        // 向上
        _lastPosition = currentPostionY;
        self.isDownScroll = NO;
    }
    
    if (scrollView.contentOffset.y < 0) {
        self.isDownScroll = NO;
    }
    
    if (scrollView.contentOffset.y == 0) {
        self.isDownScroll = NO;
    }
}

#pragma mark - HomeTopicCellDelegate

- (void)gotoTopicInfo:(TOPIC *)topic {
    [AppAnalytics clickEvent:@"click_star_topic"];
    TopicVideoListController *topicVideoList = [TopicVideoListController spawn];
    topicVideoList.topic = topic;
    topicVideoList.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:topicVideoList animated:YES];
}

- (void)gotoTopicList {
    TopicListController *topicList = [TopicListController spawn];
    topicList.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:topicList animated:YES];
}

#pragma mark - VideoListCellDelegate

- (void)gotoAuthorInfo:(USER *)author {
    AuthorInfoController *authorInfo = [AuthorInfoController spawn];
    authorInfo.author = author;
    authorInfo.followAuthor = ^(USER *author) {
        [self reloadAuthorInfo:author];
    };
    [self presentNavigationController:authorInfo];
}

- (void)followAuthor:(VIDEO *)video sender:(UIButton *)sender {
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }
    self.isDownScroll = NO;
    
    [sender followAuthorButtonAnimated];

    [self presentLoadingTips:nil];
    [sender disableSelf];
    if (video.owner.is_follow) {
        [AuthorModel unfollow:video.owner then:^(STIHTTPResponseError *error) {
            if (error == nil) {
                video.owner.is_follow = NO;
                [self reloadAuthorInfo:video.owner];
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
            [self dismissTips];
            [sender enableSelf];
        }];
    } else {
        if (!video.owner.is_author) {
            [self dismissTips];
            [[UIApplication sharedApplication].keyWindow presentMessageTips:@"作者不存在！"];
            [sender enableSelf];
            return;
        }
        
        [AuthorModel follow:video.owner then:^(STIHTTPResponseError *error) {
            if (error == nil) {
                video.owner.is_follow = YES;
                [self reloadAuthorInfo:video.owner];
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
            [self dismissTips];
            [sender enableSelf];
        }];
    }
}

#pragma mark - Notifaction

- (void)reloadVideos:(NSNotification *)notification {
    if ([[self topViewController] isKindOfClass:[self class]]) {
        return;
    }
    VIDEO *video = [notification.userInfo objectForKey:@"data"];
    for (int i = 0; i < self.videoListModel.items.count; i++) {
        VIDEO *videoItem = self.videoListModel.items[i];
        if (videoItem.id && [videoItem.id isEqualToString:video.id]) {
            [self.videoListModel.items replaceObjectAtIndex:i withObject:video];
            break;  // 一个列表不会出现相同的短片数据
        }
    }
    [self.tableView reloadData];
}

- (void)reloadVideosAuthor:(NSNotification *)notification {
    if ([[self topViewController] isKindOfClass:[self class]]) {
        return;
    }
    USER *author = [notification.userInfo objectForKey:@"data"];
    [self reloadAuthorInfo:author];
}

- (void)reloadAuthorInfo:(USER *)replaceAuthor {
    for (int i = 0; i < self.videoListModel.items.count; i++) {
        VIDEO *videoItem = self.videoListModel.items[i];
        if (videoItem.owner && [videoItem.owner.id isEqualToString:replaceAuthor.id]) {
            videoItem.owner = replaceAuthor;
        }
    }
    [self.tableView reloadData];
}

#pragma mark - Header

- (void)setupRefreshHeaderView {
    @weakify(self)
    [self.tableView addHeaderPullLoader:^{
        @strongify(self)
        [self updateVideoCount];
    }];
}

- (void)setupRefreshFooterView {
    if (self.tableView.footer == nil) {
        if (!self.videoListModel.isEmpty) {
            @weakify(self)
            [self.tableView addFooterPullLoader:^{
                @strongify(self)
                [self.videoListModel loadMore];
            }];
        }
    } else {
        if (self.videoListModel.isEmpty) {
            [self.tableView removeFooter];
        }
    }
}

- (BOOL)shouldAutorotate {
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations {
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation {
    return UIInterfaceOrientationPortrait;
}

@end
