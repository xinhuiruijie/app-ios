//
//  ArticleInfoController.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "ArticleInfoController.h"
#import "ArticleInfoModel.h"
#import "ArticleHeaderView.h"
#import "VideoAnimatedButton.h"
#import "ArticleModel.h"
#import "DarkShareView.h"

@interface ArticleInfoController () <UIWebViewDelegate>

@property (nonatomic, strong) ArticleInfoModel *model;
@property (nonatomic, strong) ArticleHeaderView *headerView;
@property (nonatomic, strong) VideoAnimatedButton *likeAnimatedView;
@property (nonatomic, strong) DarkShareView *shareAlertView;
@property (weak, nonatomic) IBOutlet UILabel *shareCountLabel;

@property (weak, nonatomic) IBOutlet UIView *likeView;
@property (weak, nonatomic) IBOutlet UIView *shareView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UIView *bottomActionView;

@property (nonatomic, strong) UIView *browserView;
@property (nonatomic, assign) BOOL showAnimation;

@end

@implementation ArticleInfoController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Home"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.showAnimation = YES;
    
    self.navigationItem.title = @"文章详情";
    @weakify(self)
    self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
        @strongify(self)
        [self closeController];
    }];
    self.navigationItem.rightBarButtonItem = [AppTheme shareItemWithHandler:^(id sender) {
        @strongify(self)
        [self shareArticleAction:nil];
    }];
    
    self.headerView = [ArticleHeaderView loadFromNib];
    self.headerView.data = self.article;
    self.headerView.frame = CGRectMake(0, 0, self.view.width, [ArticleHeaderView heightForArticleHeader:self.article]);
    [self.webView.scrollView addSubview:self.headerView];

    VideoAnimatedButton *likeAnimatedView = [[VideoAnimatedButton alloc] initWithFrame:CGRectMake(0, 0, 32, 24)];
    likeAnimatedView.center = CGPointMake(self.likeView.width / 2, self.likeView.height / 2);
    likeAnimatedView.selectedImage = [UIImage imageNamed:@"icon_video_like_sel"];
    likeAnimatedView.normalImage = [UIImage imageNamed:@"icon_daily_like_nor"];
    self.likeAnimatedView = likeAnimatedView;
    [self.likeView insertSubview:likeAnimatedView atIndex:0];
    [self resetWebview];

    [self enumerateWebViewSubViews:self.webView];
    
    self.model = [[ArticleInfoModel alloc] init];
    self.model.article_id = self.article.id;
    self.model.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        if (error == nil) {
            self.article = self.model.item;
            [self resetWebview];
        } else {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
    };
}

// 寻找webView的显示控件
- (void)enumerateWebViewSubViews:(UIView *)view {
    for (UIView *obj in view.subviews) {
        if ([NSStringFromClass([obj class]) isEqualToString:@"UIWebBrowserView"]) {
            self.browserView = obj;
            break;
        }
        if (obj.subviews.count > 0) {
            [self enumerateWebViewSubViews:obj];
        }
    }
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.model refresh];
    
    if (self.showAnimation) {
        self.browserView.alpha = 0;
        self.bottomActionView.alpha = 0;
        [self.browserView startSingleViewAnimationDelay:0.3];
        [self.bottomActionView startSingleViewAnimationDelay:0.4];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.showAnimation = NO;
}

- (void)resetWebview {
    CGFloat headerHeight = [ArticleHeaderView heightForArticleHeader:self.article];
    self.webView.backgroundColor = [AppTheme backgroundColor];
    self.webView.scrollView.contentInset = UIEdgeInsetsMake(headerHeight, 0, -headerHeight, 0);
    
    self.headerView.frame = CGRectMake(0, -headerHeight, self.view.width, headerHeight);
    self.headerView.data = self.article;
    self.likeAnimatedView.count = [NSString stringWithFormat:@"%@", self.article.like_count ? [self.article.like_count setupCountNumber] : @(0)];
    self.likeAnimatedView.isSelected = self.article.is_like;
    self.likeAnimatedView.center = CGPointMake(self.likeView.width / 2, self.likeView.height / 2);
    self.shareCountLabel.text = [NSString stringWithFormat:@"%@", self.article.share_count ? [self.article.share_count setupCountNumber] : @(0)];
    
    if (self.article.content.length) {
        [self loadResources:self.article.content];
    }
}

- (void)loadResources:(NSString *)bodyContent {
    //Create a string with the contents of PostDetail.html
    NSString * filepath = [[NSBundle mainBundle] pathForResource:@"PostDetail" ofType:@"html"];
    NSData * htmlData = [NSData dataWithContentsOfFile:filepath];
    NSString * htmlString = [[NSString alloc] initWithData:htmlData encoding:NSUTF8StringEncoding];
    
    //Add PostDetail.js to the html file
    NSString *css = [[NSBundle mainBundle] pathForResource:@"PostDetail" ofType:@"css"];
    NSString *cssString = [[NSString alloc] initWithData:[NSData dataWithContentsOfFile:css] encoding:NSUTF8StringEncoding];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"<!--PostDetail.css-->" withString:cssString];
    
    //Add PostDetail.js to the html file
    NSString *source = [[NSBundle mainBundle] pathForResource:@"PostDetail" ofType:@"js"];
    NSString *jsString = [[NSString alloc] initWithData:[NSData dataWithContentsOfFile:source] encoding:NSUTF8StringEncoding];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"<!--PostDetail.js-->" withString:jsString];
    
//    bodyContent = [self getPostContent:bodyContent];
    if (bodyContent && bodyContent.length) {
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"<!--bodyPart-->" withString:bodyContent];
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"<pre>" withString:@""];
        htmlString = [htmlString stringByReplacingOccurrencesOfString:@"</pre>" withString:@""];
    }
    
    NSString * shouldDownloadImage = @"false";
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"___shouldDownloadImage___" withString:shouldDownloadImage];
    
    NSURL * baseURL = [NSURL fileURLWithPath:filepath];
    [self.webView loadHTMLString:htmlString baseURL:baseURL];
}

// 省流量模式下替换本地默认img
- (NSString *)getPostContent:(NSString *)bodyContent {
    NSString *tempContent = bodyContent;
    if (bodyContent.length > 0) {
        tempContent = [tempContent replaceTargetPattern:@"<img.*?>" withReplacement:@"<img src=\"default_article.png\">"];
    }
    return tempContent;
}

#pragma mark -

- (void)downloadPhoto:(NSString *)url completion:(void(^)(UIImage * image))then
{
    if ( url && url.length )
    {
        NSString *key = [[SDWebImageManager sharedManager] cacheKeyForURL:[NSURL URLWithString:url]];
        UIImage * cachedImage = [[SDImageCache sharedImageCache] imageFromDiskCacheForKey:key];
        if (!cachedImage) {
            [[SDWebImageDownloader sharedDownloader] downloadImageWithURL:[NSURL URLWithString:url] options:0 progress:^(NSInteger receivedSize, NSInteger expectedSize) {
                
            } completed:^(UIImage *image, NSData *data, NSError *error, BOOL finished) {
                if ( image )
                {
                    [[SDImageCache sharedImageCache] storeImage:image forKey:url];
                    then(image);
                } else {
                    then(nil);
                }
            }];
        } else {
            then(cachedImage);
        }
    }
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    CGFloat headerHeight = [ArticleHeaderView heightForArticleHeader:self.article];
    self.webView.scrollView.contentInset = UIEdgeInsetsMake(headerHeight, 0, 0, 0);
}

#pragma mark - ButtonAction

- (IBAction)likeArticleAction:(UIButton *)sender {
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }
    
    if (self.article.is_like) {
        long value = [self.article.like_count longValue];
        self.article.like_count = [NSNumber numberWithLong:value - 1];
        
        self.likeAnimatedView.count = [NSString stringWithFormat:@"%@", self.article.like_count ? [self.article.like_count setupCountNumber] : @(0)];
        self.likeAnimatedView.isSelected = NO;
    } else {
        long value = [self.article.like_count longValue];
        self.article.like_count = [NSNumber numberWithLong:value + 1];
        
        self.likeAnimatedView.count = [NSString stringWithFormat:@"%@", self.article.like_count ? [self.article.like_count setupCountNumber] : @(1)];
        [self.likeAnimatedView startAnimation];
        self.likeAnimatedView.isSelected = YES;
    }
    
    self.likeAnimatedView.center = CGPointMake(self.likeView.width / 2, self.likeView.height / 2);
    
    [self presentLoadingTips:nil];
    if (self.article.is_like) {
        [ArticleModel unlike:self.article then:^(STIHTTPResponseError *error) {
            [self dismissTips];
            if (error == nil) {
                self.article.is_like = NO;
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
        }];
    } else {
        [ArticleModel like:self.article then:^(STIHTTPResponseError *error) {
            [self dismissTips];
            if (error == nil) {
                self.article.is_like = YES;
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
        }];
    }
}

- (IBAction)shareArticleAction:(UIButton *)sender {
    DarkShareView *shareAlert = [DarkShareView loadFromNib];
    self.shareAlertView = shareAlert;
    @weakify(self)
    if (self.article.photo && self.article.photo.thumb) {
        [self presentLoadingTips:nil];
        [self imageWithUrl:self.article.photo.thumb completion:^(UIImage *image) {
            @strongify(self)
            dispatch_async(dispatch_get_main_queue(), ^{
                [self dismissTips];
                self.article.shareImage = image;
                shareAlert.data = self.article;
                [shareAlert show];
            });
        }];
    } else {
        shareAlert.data = self.article;
        [shareAlert show];
    }
}

@end
