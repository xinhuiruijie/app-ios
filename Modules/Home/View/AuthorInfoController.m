//
//  AuthorInfoController.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "AuthorInfoController.h"
#import "VideoInfoController.h"
#import "UserWormholeListController.h"

#import "AuthorModel.h"
#import "AuthorInfoModel.h"
#import "AuthorVideoListModel.h"

#import "AuthorInfoHeaderCell.h"
#import "AuthorVideoListCell.h"
#import "AuthorInfoCoverView.h"
#import "OwnerVideoListCell.h"

@interface AuthorInfoController () <UITableViewDelegate, UITableViewDataSource>
@property (nonatomic, strong) AuthorInfoCoverView *authorHeader;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) UIImageView *refreshImage;
@property (nonatomic, strong) UIView *reportView;
@property (nonatomic, strong) AlertView *reportTypeAlertView;

@property (nonatomic, strong) AuthorInfoModel *infoModel;
@property (nonatomic, strong) AuthorVideoListModel *videoListModel;

@property (nonatomic, assign) CGRect originFrame;

// 页面动效显示机制：
// 1. 第一次进入时，前几个Cell有动效。上滑无动效，下滑有动效
// 2. 从其他页面返回时，页面无动效。上滑无动效，下滑有动效
// showAnimation字段在viewDidLoad初始化为有动效，在viewWillDisappear设置为无动效，在viewDidAppear设置为恢复动效
// 所有的相关动效都应为这样的流程
@property (nonatomic, assign) int lastPosition;
@property (nonatomic, assign) BOOL isDownScroll;
@property (nonatomic, assign) BOOL isFirstLoad;
@property (nonatomic, assign) BOOL specialCellAnimation;

@end

@implementation AuthorInfoController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Home"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    @weakify(self)
    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
    self.navigationItem.leftBarButtonItem = [AppTheme whiteBackItemWithHandler:^(id sender) {
        @strongify(self)
        [self closeController];
    }];
    self.navigationItem.rightBarButtonItem = [AppTheme defaultItemWithContent:[UIImage imageNamed:@"icon_author_more"] handler:^(id sender) {
        @strongify(self)
        self.reportView.hidden = NO;
    }];
    
    if (!self.author.is_author) {
        self.author = nil;
    } else {
        [self setupModel];
    }
    self.authorHeader.data = self.author;
    [self tableViewCustom];
    [self authorInfoCoverViewCustom];
    [self reportAuthorViewCustom];
    if (!self.author.is_author) {
        [self.view presentMessageTips:@"作者不存在！"];
    }
    
    // 页面无刷新，动画隐藏
    self.refreshImage.hidden = YES;
    
    self.isDownScroll = YES;
    self.isFirstLoad = YES;
    self.specialCellAnimation = YES;
    
    [self.videoListModel refresh];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadVideos:) name:kVideoInfoRefreshNotification object:nil];
}

#pragma mark - View Custom

- (void)reportAuthorViewCustom {
    self.reportView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight)];
    self.reportView.backgroundColor = [UIColor clearColor];
//    [[UIApplication sharedApplication].keyWindow addSubview:self.reportView];
    [self.navigationController.view addSubview:self.reportView];
    self.reportView.hidden = YES;
    
    UIButton *hidButton = [UIButton buttonWithType:UIButtonTypeCustom];
    hidButton.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
    [hidButton addTarget:self action:@selector(reportViewHidAction) forControlEvents:UIControlEventTouchUpInside];
    [self.reportView addSubview:hidButton];
    
    CGFloat topInset = [SDiOSVersion deviceSize] == Screen5Dot8inch ? 53 : 29;
    UIView *reportAuthorView = [[UIView alloc] initWithFrame:CGRectMake(kScreenWidth - 148, topInset, 118, 51)];
    [self.reportView addSubview:reportAuthorView];
    
    UIImageView *operateImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_w_operate1"]];
    operateImage.frame = reportAuthorView.bounds;
    [reportAuthorView addSubview:operateImage];
    
    UIButton *reportAuthorButton = [UIButton buttonWithType:UIButtonTypeCustom];
    reportAuthorButton.frame = reportAuthorView.bounds;
    [reportAuthorButton addTarget:self action:@selector(reportAuthorAction) forControlEvents:UIControlEventTouchUpInside];
    [reportAuthorView addSubview:reportAuthorButton];
}

- (void)tableViewCustom {
    CGFloat topInset = [SDiOSVersion deviceSize] == Screen5Dot8inch ? -88 : -64;
//    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight) style:UITableViewStyleGrouped];
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.sectionFooterHeight = CGFLOAT_MIN;
    self.tableView.sectionHeaderHeight = CGFLOAT_MIN;
    self.tableView.estimatedRowHeight = 0;
    
    if (iOSVersionGreaterThanOrEqualTo(@"11.0")) {
        self.tableView.contentInset = UIEdgeInsetsMake(topInset, 0, 0, 0);
    }
    
    CGFloat headerHeight = ceil(kScreenWidth * 260 / 375);
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.width, headerHeight)];
    headerView.backgroundColor = [UIColor clearColor];
    self.tableView.tableHeaderView = headerView;
    
    [self.view addSubview:self.tableView];
    
    [self.tableView registerNib:[AuthorInfoHeaderCell nib] forCellReuseIdentifier:@"AuthorInfoHeaderCell"];
    [self.tableView registerNib:[AuthorVideoListCell nib] forCellReuseIdentifier:@"AuthorVideoListCell"];
    [self.tableView registerNib:[OwnerVideoListCell nib] forCellReuseIdentifier:@"OwnerVideoListCell"];
}

- (void)authorInfoCoverViewCustom {
    CGFloat headerHeight = ceil(kScreenWidth * 260 / 375);
    AuthorInfoCoverView *header = [AuthorInfoCoverView loadFromNib];
    header.frame = CGRectMake(0, 0, self.view.width, headerHeight);
    self.originFrame = header.frame;
    [self.view addSubview:header];
    self.authorHeader = header;
    @weakify(self)
    self.authorHeader.followAuthor = ^(UIButton *sender) {
        @strongify(self);
        [self followAuthor:sender];
    };
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self scrollViewDidScroll:self.tableView];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
    
    if (self.videoListModel.loaded) {
        [self.infoModel refresh];
    } else {
        [self refreshAnimation];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    self.isDownScroll = NO;
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
    [self.navigationController.navigationBar lt_reset];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (UIStatusBarStyle)preferredStatusBarStyle{
    if (self.tableView.contentOffset.y > 50) {
        return UIStatusBarStyleDefault;
    }
    return UIStatusBarStyleLightContent;
}

#pragma mark - Models

- (void)setupModel {
    @weakify(self)
    self.infoModel = [[AuthorInfoModel alloc] init];
    self.infoModel.author_id = self.author.id;
    self.infoModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        self.author = self.infoModel.item;
        self.authorHeader.data = self.author;
        [self.tableView.header endRefreshing];
        [self.tableView reloadData];
        
        if (error) {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
    };
    
    self.videoListModel = [[AuthorVideoListModel alloc] init];
    self.videoListModel.author_id = self.author.id;
    self.videoListModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        [self finishLoadAnimation];
        [self.tableView.header endRefreshing];
        [self.tableView reloadData];
        if (error == nil) {
            [self setupRefreshFooterView];
            if (self.videoListModel.more) {
                [self.tableView.footer endRefreshing];
            } else {
                [self.tableView.footer noticeNoMoreData];
            }
        } else {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
    };
    
}

#pragma mark - Action

- (void)reportAuthorAction {
    [self reportViewHidAction];
    GKZActionSheet *actionSheet = [GKZActionSheet loadFromNib];
    
    self.reportTypeAlertView = [[AlertView alloc] initWithContent:actionSheet type:AlertViewTypeMonospaced];
    actionSheet.data = @[@"垃圾广告营销", @"恶意攻击谩骂", @"淫秽色情信息", @"违法有害信息", @"其它不良信息"];
    
    @weakify(self)
    actionSheet.whenHide = ^(BOOL hide) {
        @strongify(self)
        [self.reportTypeAlertView hide];
    };
    
    actionSheet.whenRegistered = ^(id data, NSInteger index) {
        @strongify(self)
        [self.reportTypeAlertView hide];
        REPORT_TYPE type = index + 1;
        [AuthorModel reportAuthor:self.author.id withType:type completed:^(STIHTTPResponseError *error) {
            if (error == nil) {
                [self presentMessageTips:@"您的举报我们会在24小时内进行处理"];
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
        }];
    };
    
    [self.reportTypeAlertView showSharedView];
}

- (void)reportViewHidAction {
    self.reportView.hidden = YES;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offsetY = scrollView.contentOffset.y;
    
    @weakify(self)
    CGFloat navigationHeight = [SDiOSVersion deviceSize] == Screen5Dot8inch ? 88 : 64;
    CGFloat changePoint = 50;
    if (offsetY > changePoint) {
        CGFloat alpha = MIN(1, 1 - ((changePoint + navigationHeight - offsetY) / navigationHeight));
        [self.navigationController.navigationBar lt_setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:alpha]];
        self.navigationItem.rightBarButtonItem = [AppTheme defaultItemWithContent:[UIImage imageNamed:@"icon_btn_author_more"] handler:^(id sender) {
            @strongify(self)
            self.reportView.hidden = NO;
        }];
        self.navigationItem.title = self.author.name;
        self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
            @strongify(self)
            [self closeController];
        }];
        [self setNeedsStatusBarAppearanceUpdate];
    } else {
        [self.navigationController.navigationBar lt_setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0]];
        self.navigationItem.rightBarButtonItem = [AppTheme defaultItemWithContent:[UIImage imageNamed:@"icon_author_more"] handler:^(id sender) {
            @strongify(self)
            self.reportView.hidden = NO;
        }];
        self.navigationItem.title = nil;
        self.navigationItem.leftBarButtonItem = [AppTheme whiteBackItemWithHandler:^(id sender) {
            @strongify(self)
            [self closeController];
        }];
        [self setNeedsStatusBarAppearanceUpdate];
    }
    
    // 头部图片拉伸设置
//    CGFloat imageRatio = 0.37;
    if (offsetY > 0) { // 上滑
        CGRect frame = self.originFrame;
        frame.origin.y = self.originFrame.origin.y - offsetY;
        self.authorHeader.frame = frame;
    } else { // 下拉
        CGRect frame = self.originFrame;
        frame.size.height = self.originFrame.size.height - offsetY;
//        frame.size.width = frame.size.height / imageRatio;        // 注释下拉放大，等图片全部展示出来后再放大
        frame.origin.x = self.originFrame.origin.x - (frame.size.width - self.originFrame.size.width) * 0.5;

        // 作者头像下拉不放大，超出范围直接显示白底(不要删掉)
//        if (frame.size.height > frame.size.width) {
//            frame.size.height = frame.size.width;
//            frame.origin.y = -offsetY - ceil(kScreenWidth * 470 / 750);
//        }
        self.authorHeader.frame = frame;
    }
    
    if (offsetY < -44) {
        [self refreshAnimation];
    }
    
    CGFloat currentPostionY = scrollView.contentOffset.y;
    
    if (currentPostionY - _lastPosition > 0) {
        // 向下
        _lastPosition = currentPostionY;
        self.isDownScroll = YES;
    } else if (_lastPosition - currentPostionY > 0) {
        // 向上
        _lastPosition = currentPostionY;
        self.isDownScroll = NO;
    } else {
        self.isDownScroll = NO;
    }
    
    if (scrollView.contentOffset.y < 0) {
        self.isDownScroll = NO;
    }
    
    if (scrollView.contentOffset.y == 0) {
        if (self.isFirstLoad) {
            self.isDownScroll = YES;
        } else {
            self.isDownScroll = NO;
        }
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }
    return self.videoListModel.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        AuthorInfoHeaderCell *headerCell = [tableView dequeueReusableCellWithIdentifier:@"AuthorInfoHeaderCell" forIndexPath:indexPath];
//        headerCell.delegate = self;
        headerCell.data = self.infoModel.item;
        return headerCell;
    } else {
//        AuthorVideoListCell *videoCell = [tableView dequeueReusableCellWithIdentifier:@"AuthorVideoListCell" forIndexPath:indexPath];
//        videoCell.data = self.videoListModel.items[indexPath.row];
//        return videoCell;
        OwnerVideoListCell *videoCell = [tableView dequeueReusableCellWithIdentifier:@"OwnerVideoListCell" forIndexPath:indexPath];
        videoCell.firstCell = (indexPath.row != 0);
        videoCell.data = self.videoListModel.items[indexPath.row];
        return videoCell;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 85;
    } else {
//        VIDEO *video = self.videoListModel.items[indexPath.row];
//        return [AuthorVideoListCell heightForAuthorVideoCell:video];
        CGFloat cellHeight = ceil(kScreenWidth * 211 / 375);
        if (indexPath.row == 0) {
            return cellHeight + 58;
        } else {
            return cellHeight + 42;
        }
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 1) {
        VideoInfoController *videoInfo = [VideoInfoController spawn];
        videoInfo.video = self.videoListModel.items[indexPath.row];
        
        videoInfo.reloadVideo = ^(VIDEO *video) {
            for (int i = 0; i < self.videoListModel.items.count; i++) {
                VIDEO *perVideo = self.videoListModel.items[i];
                if ([perVideo.id isEqualToString:video.id]) {
                    [self.videoListModel.items replaceObjectAtIndex:i withObject:video];
                    break;
                }
            }
            [self.tableView reloadData];
        };
//        [self presentNavigationController:videoInfo];
        videoInfo.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:videoInfo animated:YES];
    }
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.isDownScroll && indexPath.section == 1) {
        self.isFirstLoad = NO;
        if (indexPath.row < 2) {
            if (self.specialCellAnimation) {
                cell.alpha = 0;
                NSInteger indexDelay = indexPath.row;
                [cell performSelector:@selector(startSingleViewAnimation) withObject:self afterDelay:indexDelay * 0.2];
                if ((self.videoListModel.items.count == 1 && indexPath.row == 0) || (self.videoListModel.items.count > 2 && indexPath.row == 1)) {
                    self.specialCellAnimation = NO;
                }
            }
        } else {
            cell.alpha = 0;
            [cell startSingleViewAnimation];
        }
    }
}

#pragma mark - AuthorInfoHeaderCellDelegate

- (void)followAuthor:(UIButton *)sender {
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }
    
    [sender followAuthorButtonAnimated];
    
    self.isDownScroll = NO;
    [sender disableSelf];
    [self presentLoadingTips:nil];
    if (self.author.is_follow) {
        [AuthorModel unfollow:self.author then:^(STIHTTPResponseError *error) {
            [self dismissTips];
            if (error == nil) {
                self.author.is_follow = NO;
                [self.tableView reloadData];
                [self.infoModel refresh];
                if (self.followAuthor) {
                    self.followAuthor(self.author);
                }
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
            [sender enableSelf];
        }];
    } else {
        if (!self.author.is_author) {
            [self.view presentMessageTips:@"作者不存在！"];
            return;
        }
        
        [AuthorModel follow:self.author then:^(STIHTTPResponseError *error) {
            [self dismissTips];
            if (error == nil) {
                self.author.is_follow = YES;
                [self.tableView reloadData];
                [self.infoModel refresh];
                if (self.followAuthor) {
                    self.followAuthor(self.author);
                }
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
            [sender enableSelf];
        }];
    }
}

#pragma mark - Notifaction

- (void)reloadVideos:(NSNotification *)notification {
    if ([[self topViewController] isKindOfClass:[self class]]) {
        return;
    }
    VIDEO *video = [notification.userInfo objectForKey:@"data"];
    for (int i = 0; i < self.videoListModel.items.count; i++) {
        VIDEO *videoItem = self.videoListModel.items[i];
        if (videoItem.id && [videoItem.id isEqualToString:video.id]) {
            [self.videoListModel.items replaceObjectAtIndex:i withObject:video];
            break;  // 一个列表不会出现相同的短片数据
        }
    }
    [self.tableView reloadData];
}

#pragma mark - Animation

- (void)refreshAnimation {
    if (self.refreshImage.isAnimating) {
        return;
    }
    self.refreshImage.alpha = 1;
    [self.refreshImage startAnimating];
    
    // 网速快的情况，会一直刷新。强制执行完一遍动画后再刷新
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self.infoModel refresh];
        [self.videoListModel refresh];
    });
}

- (void)finishLoadAnimation {
    [self.refreshImage stopAnimating];
    self.refreshImage.image = [UIImage imageNamed:@"icon_refresh_ok"];
    CGFloat transformY = [SDiOSVersion deviceSize] == Screen5Dot8inch ? -88 : -60;
    [UIView animateWithDuration:0.5 animations:^{
        self.refreshImage.layer.transform = CATransform3DMakeTranslation(0, transformY, 0);
    } completion:^(BOOL finished) {
        self.refreshImage.alpha = 0;
        self.refreshImage.layer.transform = CATransform3DIdentity;
    }];
}

- (UIImageView *)refreshImage {
    if (!_refreshImage) {
        CGFloat orginY = [SDiOSVersion deviceSize] == Screen5Dot8inch ? 50 : 30;
        _refreshImage = [[UIImageView alloc] initWithFrame:CGRectMake((self.view.width - 23 - 15), orginY, 23, 23)];
        NSMutableArray *animationImages = [NSMutableArray array];
        for (NSInteger i = 1; i < 35; i++) {
            UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"icon_refresh_%ld", i]];
            [animationImages addObject:image];
        }
        _refreshImage.animationImages = animationImages;
        _refreshImage.animationDuration = 1;
        _refreshImage.animationRepeatCount = 0;
        [self.view addSubview:_refreshImage];
    }
    return _refreshImage;
}

#pragma mark - Header

- (void)setupRefreshFooterView {
    if (self.tableView.footer == nil) {
        if (!self.videoListModel.isEmpty) {
            @weakify(self)
            [self.tableView addFooterPullLoader:^{
                @strongify(self)
                [self.videoListModel loadMore];
            }];
        }
    } else {
        if (self.videoListModel.isEmpty) {
            [self.tableView removeFooter];
        }
    }
}

@end
