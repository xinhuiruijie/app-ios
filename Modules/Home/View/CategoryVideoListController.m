//
//  CategoryVideoListController.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "CategoryVideoListController.h"
#import "VideoInfoController.h"
#import "AuthorInfoController.h"
#import "CategoryVideoListModel.h"
#import "AuthorModel.h"
#import "VideoListCell.h"
#import "HotVideoListCell.h"
#import "HomeCategoryListHeader.h"

@interface CategoryVideoListController () <UITableViewDelegate, UITableViewDataSource, VideoListCellDelegate>
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) CategoryVideoListModel *model;

@property (nonatomic, assign) CGFloat insetTop;
@end

@implementation CategoryVideoListController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Home"];
}

- (UIScrollView *)list {
    return self.tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = self.category.title;
    @weakify(self)
    self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    self.tableView.estimatedRowHeight = 0;
    self.tableView.backgroundColor = [AppTheme listBackgroundColor];
    [self.tableView registerNib:[VideoListCell nib] forCellReuseIdentifier:@"VideoListCell"];
    [self.tableView registerNib:[HotVideoListCell nib] forCellReuseIdentifier:@"HotVideoListCell"];

    self.model = [[CategoryVideoListModel alloc] init];
    self.model.category_id = self.category.id;
    self.model.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        [self.tableView.header endRefreshing];
        [self.tableView reloadData];
        if (error == nil) {
            [self setupRefreshFooterView];
            if (self.model.more) {
                [self.tableView.footer endRefreshing];
            } else {
                [self.tableView.footer noticeNoMoreData];
            }
            
//            if (!self.model.isEmpty) {
//                VIDEO *video = self.model.items.firstObject;
//                self.navigationItem.title = video.category.title;
//            }
        } else {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
    };
    
    [self.list.header beginRefreshing];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadVideosAuthor:) name:kAuthorFollowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadVideosAuthor:) name:kAuthorUnfollowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadVideos:) name:kVideoInfoRefreshNotification object:nil];
    
    [self getListView].contentInsets_top = self.insetTop;
}

#pragma mark - HomePageItemViewControllerProtocol

- (UIScrollView *)getListView {
    return self.list;
}

- (void)setListViewInsetTop:(CGFloat)insetTop {
    self.insetTop = insetTop;
    [self getListView].contentInsets_top = self.insetTop;
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (self.model.hotVideos.count && self.model.videos.count) {
        return 2;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        if (self.model.hotVideos.count > 3) {
            return 3;
        }
        return self.model.hotVideos.count;
    }
    return self.model.videos.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        VIDEO *video = self.model.hotVideos[indexPath.row];
        if (indexPath.row == 0) {
            VideoListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VideoListCell" forIndexPath:indexPath];
            cell.data = video;
            cell.delegate = self;
            return cell;
        } else {
            HotVideoListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HotVideoListCell" forIndexPath:indexPath];
            cell.data = video;
            [cell hideBottomLine:indexPath.row != self.model.hotVideos.count - 1];
            return cell;
        }
    } else {
        VIDEO *video = self.model.videos[indexPath.row];
        VideoListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VideoListCell" forIndexPath:indexPath];
        cell.data = video;
        cell.delegate = self;
        return cell;
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    VIDEO *video = nil;
    if (indexPath.section == 0) {
        video = self.model.hotVideos[indexPath.row];
    } else {
        video = self.model.videos[indexPath.row];
    }
    
    VideoInfoController *videoInfo = [VideoInfoController spawn];
    videoInfo.video = video;
    
    videoInfo.reloadVideo = ^(VIDEO *video) {
        if (indexPath.section == 0) {
            for (int i = 0; i < self.model.hotVideos.count; i++) {
                VIDEO *perVideo = self.model.hotVideos[i];
                if ([perVideo.id isEqualToString:video.id]) {
                    [self.model.hotVideos replaceObjectAtIndex:i withObject:video];
                    break;
                }
            }
        } else {
            for (int i = 0; i < self.model.videos.count; i++) {
                VIDEO *perVideo = self.model.videos[i];
                if ([perVideo.id isEqualToString:video.id]) {
                    [self.model.videos replaceObjectAtIndex:i withObject:video];
                    break;
                }
            }
        }
        [self.tableView reloadData];
    };
//    [self presentNavigationController:videoInfo];
    videoInfo.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:videoInfo animated:YES];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        VIDEO *video = self.model.hotVideos[indexPath.row];
        if (indexPath.row == 0) {
            return [VideoListCell heightForVideoListCell:video];
        } else {
            return [HotVideoListCell heightForRowWith:video];
        }
    } else {
        VIDEO *video = self.model.videos[indexPath.row];
        return [VideoListCell heightForVideoListCell:video];
    }
}

// header in section
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (self.model.isEmpty) {
        return 0.000001f;
    }
    return 58;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (self.model.isEmpty) {
        return [UIView new];
    }
    
    HomeCategoryListHeader *header = [HomeCategoryListHeader loadFromNib];
    if (section == 0) {
        header.title = @"精选头条";
    } else {
        header.title = @"全部短片";
    }
    return header;
}

// footer in section
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (self.model.loaded && self.model.isEmpty) {
        return tableView.height;
    }
    return 0.000001f;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    if (self.model.loaded && self.model.isEmpty) {
        EmptyTableCell *cell = [EmptyTableCell loadFromNib];
        return cell;
    }
    return [UIView new];
}

#pragma mark - VideoListCellDelegate

- (void)gotoAuthorInfo:(USER *)author {
    AuthorInfoController *authorInfo = [AuthorInfoController spawn];
    authorInfo.author = author;
    authorInfo.followAuthor = ^(USER *author) {
        [self reloadAuthorInfo:author];
    };
    [self presentNavigationController:authorInfo];
}

- (void)followAuthor:(VIDEO *)video sender:(UIButton *)sender {
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }
    
    [sender followAuthorButtonAnimated];
    
    [self presentLoadingTips:nil];
    [sender disableSelf];
    if (video.owner.is_follow) {
        [AuthorModel unfollow:video.owner then:^(STIHTTPResponseError *error) {
            if (error == nil) {
                video.owner.is_follow = NO;
                [self reloadAuthorInfo:video.owner];
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
            [self dismissTips];
            [sender enableSelf];
        }];
    } else {
        if (!video.owner.is_author) {
            [self dismissTips];
            [[UIApplication sharedApplication].keyWindow presentMessageTips:@"作者不存在！"];
            return;
        }
        
        [AuthorModel follow:video.owner then:^(STIHTTPResponseError *error) {
            if (error == nil) {
                video.owner.is_follow = YES;
                [self reloadAuthorInfo:video.owner];
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
            [self dismissTips];
            [sender enableSelf];
        }];
    }
}

#pragma mark - Notifaction

- (void)reloadVideos:(NSNotification *)notification {
    if ([[self topViewController] isKindOfClass:[self class]]) {
        return;
    }
    VIDEO *video = [notification.userInfo objectForKey:@"data"];
    
    for (int i = 0; i < self.model.hotVideos.count; i++) {
        VIDEO *videoItem = self.model.hotVideos[i];
        if (videoItem.id && [videoItem.id isEqualToString:video.id]) {
            [self.model.hotVideos replaceObjectAtIndex:i withObject:video];
            break;  // 一个列表不会出现相同的短片数据
        }
    }
    
    for (int i = 0; i < self.model.videos.count; i++) {
        VIDEO *videoItem = self.model.videos[i];
        if (videoItem.id && [videoItem.id isEqualToString:video.id]) {
            [self.model.videos replaceObjectAtIndex:i withObject:video];
            break;  // 一个列表不会出现相同的短片数据
        }
    }
    
    [self.tableView reloadData];
}

- (void)reloadVideosAuthor:(NSNotification *)notification {
    if ([[self topViewController] isKindOfClass:[self class]]) {
        return;
    }
    USER *author = [notification.userInfo objectForKey:@"data"];
    [self reloadAuthorInfo:author];
}

- (void)reloadAuthorInfo:(USER *)replaceAuthor {
    for (int i = 0; i < self.model.hotVideos.count; i++) {
        VIDEO *videoItem = self.model.hotVideos[i];
        if (videoItem.owner && [videoItem.owner.id isEqualToString:replaceAuthor.id]) {
            videoItem.owner = replaceAuthor;
        }
    }
    
    for (int i = 0; i < self.model.videos.count; i++) {
        VIDEO *videoItem = self.model.videos[i];
        if (videoItem.owner && [videoItem.owner.id isEqualToString:replaceAuthor.id]) {
            videoItem.owner = replaceAuthor;
        }
    }
    
    [self.tableView reloadData];
}

@end
