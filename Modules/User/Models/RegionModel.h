//
//  RegionModel.h
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "STIOnceModel.h"

@interface RegionModel : STIOnceModel

@singleton(RegionModel)

@end
