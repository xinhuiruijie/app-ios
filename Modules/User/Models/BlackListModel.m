//
//  BlackListModel.m
//  MotherPlanet
//
//  Created by 陈熙 on 2018/3/1.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "BlackListModel.h"

@implementation BlackListModel

- (void)fetchForFirstTime:(BOOL)isFirstTime {
    if (isFirstTime) {
        self.currentPage = 1;
    } else {
        self.currentPage += 1;
    }
    V1_API_USER_BLACKLIST_API *api = [[V1_API_USER_BLACKLIST_API alloc] init];
    api.req.page = @(self.currentPage);
    api.req.per_page = @(10);
    api.whenUpdated = ^(V1_API_USER_BLACKLIST_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if (error) {
            PERFORM_BLOCK_SAFELY(self.whenUpdated, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                if (isFirstTime) {
                    [self.items removeAllObjects];
                }
                [self.items addObjectsFromArray:response.users];
                self.loaded = YES;
                self.more = response.paged.more.boolValue;
                PERFORM_BLOCK_SAFELY(self.whenUpdated, nil);
            } else {
                STIHTTPResponseError *errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(self.whenUpdated, errors);
            }
        }
    };
    [api send];
}

- (void)refresh {
    [self fetchForFirstTime:YES];
}

- (void)loadMore {
    [self fetchForFirstTime:NO];
}

@end
