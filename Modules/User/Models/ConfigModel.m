//
//  ConfigModel.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/17.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "ConfigModel.h"

@implementation ConfigModel

@def_singleton(ConfigModel);

- (instancetype)init {
    if (self = [super init]) {
        [self loadCache];
    }
    return self;
}

- (void)loadCache {
    self.item = [self loadCacheWithKey:self.cacheKey objectClass:[OAUTH_CONFIG class]];
}

- (void)saveCache {
    [self saveCache:self.item key:self.cacheKey];
}

- (NSString *)cacheKey {
    return [[self class] description];
}

- (void)refresh {
    V1_API_CONFIG_GET_API *api = [[V1_API_CONFIG_GET_API alloc] init];
    api.whenUpdated = ^(V1_API_CONFIG_GET_RESPONSE *resp, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if (error) {
            PERFORM_BLOCK_SAFELY(self.whenUpdated, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                self.loaded = YES;
                self.item = resp.oauth;
                self.share_url = resp.share_base_url;
                self.register_url = resp.register_url;
                self.feedback_url = resp.feedback_url;
                self.about_url = resp.about_url;
                self.copyright_url = resp.copyright_url;
                self.app_guide_url = resp.app_guide_url;
                self.integral_mall_url = resp.integral_mall_url;
                self.score_guide_url = resp.score_guide_url;
                self.mall_disclaimer_url = resp.mall_disclaimer_url;
                self.apply_author_url = resp.apply_author_url;
                self.app_introduce_url = resp.app_introduce_url;
                [self saveCache];
                PERFORM_BLOCK_SAFELY(self.whenUpdated, nil);
            } else {
                STIHTTPResponseError *errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(self.whenUpdated, errors);
            }
        }
    };
    [api send];
}

/**
 * 获取协议
 */
- (void)getPotocolUrlWithCompletion:(void (^)(NSString * register_url, STIHTTPResponseError *e))completion {
    V1_API_REGISTER_URL_API *api = [[V1_API_REGISTER_URL_API alloc] init];
    api.whenUpdated = ^(V1_API_REGISTER_URL_RESPONSE *resp, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if (error) {
            PERFORM_BLOCK_SAFELY(completion, nil , error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                PERFORM_BLOCK_SAFELY(completion, resp.register_url ,nil);
            } else {
                STIHTTPResponseError *errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(completion, nil, errors);
            }
        }
    };
    [api send];
}

@end
