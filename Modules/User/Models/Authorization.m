//
//  Authorization.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/10/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "Authorization.h"
#import "LoginViewController.h"

NSString *const AuthorizationWasFailedNotification    = @"AuthorizationWasFailedNotification";
NSString *const AuthorizationWasSucceedNotification   = @"AuthorizationWasSucceedNotification";
NSString *const AuthorizationWasInvalidNotification   = @"AuthorizationWasInvalidNotification";
NSString *const AuthorizationWasCancelledNotification = @"AuthorizationWasCancelledNotification";

@interface Authorization(Private)
+ (void)showAuthIn:(UIViewController *)vc completion:(void (^)(void))completion;
+ (void)hideAuthIn:(UIViewController *)vc completion:(void (^)(void))completion;
@end

@implementation Authorization

@def_singleton(Authorization);

- (void)showAuth {
    @weakify(self);
    [self showAuthWithCompletion:^{
        @strongify(self);
        self.isPresentLogin = YES;
    }];
}

- (void)hideAuth {
    @weakify(self);
    [self hideAuthWithCompletion:^{
        @strongify(self);
        self.isPresentLogin = NO;
    }];
}

- (void)showAuthWithCompletion:(void (^)(void))completion {
//    [[self class] showAuthIn:self.delegate completion:completion];
    [[self class] showAuthIn:[self topViewController] completion:completion];
}

- (void)hideAuthWithCompletion:(void (^)(void))completion {
//    [[self class] hideAuthIn:self.delegate completion:completion];
    [[self class] hideAuthIn:[self topViewController] completion:completion];
}

+ (void)showAuthIn:(UIViewController *)vc completion:(void (^)(void))completion {
    UINavigationController * auth = [[UINavigationController alloc] initWithRootViewController:[LoginViewController spawn]];
    [vc presentViewController:auth animated:YES completion:completion];
}

+ (void)hideAuthIn:(UIViewController *)vc completion:(void (^)(void))completion {
    [vc dismissViewControllerAnimated:YES completion:completion];
}

#pragma mark -

- (void)authorizationWasSucceed {
    if (self.delegate &&
        [self.delegate respondsToSelector:@selector(authorizationWasSucceed)]) {
        [self.delegate authorizationWasSucceed];
    }
    
    [self fk_postNotification:AuthorizationWasSucceedNotification];
}

- (void)authorizationWasFailed {
    if (self.delegate &&
        [self.delegate respondsToSelector:@selector(authorizationWasFailed)]) {
        [self.delegate authorizationWasFailed];
    }
    
    [self fk_postNotification:AuthorizationWasFailedNotification];
}

- (void)authorizationWasInvalid:(NSString *)errorDesc {
    if (self.delegate &&
        [self.delegate respondsToSelector:@selector(authorizationWasInvalid:)]) {
        [self.delegate authorizationWasInvalid:errorDesc];
    }
    
    [self fk_postNotification:AuthorizationWasInvalidNotification];
}

- (void)authorizationWasCancelled {
    if (self.delegate &&
        [self.delegate respondsToSelector:@selector(authorizationWasCancelled)]) {
        [self.delegate authorizationWasCancelled];
    }
    
    [self fk_postNotification:AuthorizationWasCancelledNotification];
}

@end

