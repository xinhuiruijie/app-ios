//
//  RegionModel.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "RegionModel.h"

@implementation RegionModel

@def_singleton(RegionModel)

- (instancetype)init {
    if (self = [super init]) {
        [self loadCache];
    }
    return self;
}

- (void)loadCache {
    self.item = [self loadCacheWithKey:self.cacheKey objectClass:[NSArray class]];
}

- (void)saveCache {
    [self saveCache:self.item key:self.cacheKey];
}

- (NSString *)cacheKey {
    return [[self class] description];
}

- (void)refresh {
    V1_API_REGION_LIST_API *api = [[V1_API_REGION_LIST_API alloc] init];
    api.whenUpdated = ^(V1_API_REGION_LIST_RESPONSE *resp, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if (error) {
            PERFORM_BLOCK_SAFELY(self.whenUpdated, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                self.loaded = YES;
                self.item = resp.regions;
                [self saveCache];
                PERFORM_BLOCK_SAFELY(self.whenUpdated, nil);
            } else {
                STIHTTPResponseError *errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(self.whenUpdated, errors);
            }
        }
    };
    [api send];
}

@end
