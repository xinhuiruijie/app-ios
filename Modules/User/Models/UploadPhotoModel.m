//
//  UploadPhotoModel.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/14.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "UploadPhotoModel.h"
#import "TraceEncryption.h"

@implementation UploadPhotoModel

/**
 * 上传图片
 */
+ (void)uploadWithFile:(UIImage *)file
completion:(void (^)(NSString *url, STIHTTPResponseError *e))completion {
    
    AppSesseionManager *sharedManager = [[AppSesseionManager alloc] initWithBaseURL:[NSURL URLWithString:kAppServerAPIURL]];
    sharedManager.requestSerializer = [AFHTTPRequestSerializer serializer];
    [sharedManager.requestSerializer setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    sharedManager.requestSerializer.timeoutInterval = 60;
    sharedManager.setup = ^(id vars,...) {
    };
    if ([UserModel sharedInstance].token && [UserModel sharedInstance].token.length) {
        [sharedManager.requestSerializer setValue:[UserModel sharedInstance].token forHTTPHeaderField:@"Authorization"];
    } else {
        [sharedManager.requestSerializer setValue:nil forHTTPHeaderField:@"Authorization"];
    }
    
    NSDictionary *param = @{@"file" : @"file"};
    
    [sharedManager POST:kAppUploadImageApiURL parameters:param constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        NSString *name = @"file";
        NSString *fileName = [NSString stringWithFormat:@"file"];
        NSData * data = UIImageJPEGRepresentation(file, 0.8);
        [formData appendPartWithFileData:data name:name fileName:fileName mimeType:@"image/jpeg"];
    } progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        id responseJson = nil;
        if ( [responseObject isKindOfClass:[NSDictionary class]] && [responseObject objectAtPath:@"data"]) {
            NSString * json = [TraceEncryption decryptBase64StringToString:[responseObject objectAtPath:@"data"]?:@"" stringKey:TRACE_ENCRYPTION_KEY sign:YES];
            
            responseJson = [json dictionaryWithJsonString];
        } else {
            responseJson = responseObject;
        }
        // 请求成功，解析数据
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        NSDictionary *allHeaders = response.allHeaderFields;
        responseJson = [sharedManager processedDataWithResponseObject:responseJson task:task allHeaders:allHeaders];
        V1_API_PHOTO_UPLOAD_RESPONSE *resp = [V1_API_PHOTO_UPLOAD_RESPONSE ac_objectWithAny:responseJson];
        completion(resp.url, nil);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSHTTPURLResponse *response = (NSHTTPURLResponse *)task.response;
        NSDictionary *allHeaders = response.allHeaderFields;
        [sharedManager processedDataWithResponseObject:nil task:task allHeaders:allHeaders];
        
        STIHTTPResponseError * e = [STIHTTPResponseError new];
        e.message = error.localizedDescription;
        e.code = error.code;
        completion(nil, e);
    }];
}

@end
