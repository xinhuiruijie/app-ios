//
//  Authorization.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/10/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark -

FOUNDATION_EXTERN NSString *const AuthorizationWasFailedNotification;
FOUNDATION_EXTERN NSString *const AuthorizationWasSucceedNotification;
FOUNDATION_EXTERN NSString *const AuthorizationWasInvalidNotification;
FOUNDATION_EXTERN NSString *const AuthorizationWasCancelledNotification;

#pragma mark -

@protocol AuthorizationDelegate <NSObject>
- (void)authorizationWasFailed;
- (void)authorizationWasSucceed;
- (void)authorizationWasInvalid:(NSString *)errorDesc;
- (void)authorizationWasCancelled;
@end

#pragma mark -

@interface Authorization : NSObject

@singleton(Authorization);

@property (nonatomic, weak) UIViewController<AuthorizationDelegate> * delegate;
@property (nonatomic, assign) BOOL isPresentLogin;

- (void)showAuth;
- (void)hideAuth;
- (void)showAuthWithCompletion:(void (^)(void))completion;
- (void)hideAuthWithCompletion:(void (^)(void))completion;
+ (void)showAuthIn:(UIViewController *)vc completion:(void (^)(void))completion;
+ (void)hideAuthIn:(UIViewController *)vc completion:(void (^)(void))completion;

// 授权成功
- (void)authorizationWasSucceed;
// 授权失败
- (void)authorizationWasFailed;
// 授权过期
- (void)authorizationWasInvalid:(NSString *)errorDesc;
// 用户取消授权
- (void)authorizationWasCancelled;

@end
