//
//  UserModel.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/10/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "UserModel.h"
#import "Authorization.h"

NSNotificationName kMPUserInfoDidChangedNotification = @"kMPUserInfoDidChangedNotification";

@implementation UserModel

@def_singleton(UserModel)

- (id)init {
    if (self = [super init]) {
        [self loadCache];
    }
    return self;
}

- (void)loadCache {
    USER *user = [self loadCacheWithKey:[self cacheKey] objectClass:[USER class]];
    NSString *token = [self loadCacheWithKey:[self tokenCacheKey] objectClass:nil];
    
    if (user && [user isKindOfClass:[USER class]]) {
        self.user = user;
    }
    if (token && [token isKindOfClass:[NSString class]]) {
        self.token = token;
    }
}

- (void)saveCache {
    [self saveCache:self.user key:[self cacheKey]];
    [self saveCache:self.token key:[self tokenCacheKey]];
}

- (void)clearCache {
    self.user = nil;
    self.token = nil;
    
    [self clearCacheWithKey:[self cacheKey]];
    [self clearCacheWithKey:[self tokenCacheKey]];
}

- (NSString *)cacheKey {
    return NSStringFromClass([self class]);
}

- (NSString *)tokenCacheKey {
    return [NSString stringWithFormat:@"%@.%@", [self cacheKey], @"token"];
}

- (void)setUser:(USER *)user {
    USER *oldUser = _user;
    _user = user;
    if (oldUser) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:kMPUserInfoDidChangedNotification object:user];
        });
    }
}

#pragma mark -

+ (BOOL)online {
    return [[UserModel sharedInstance] online];
}

- (BOOL)online {
    if (self.token && self.token.length) {
        return YES;
    }
    return NO;
}

- (void)signout {
    [self clearCache];
    [[Authorization sharedInstance] authorizationWasCancelled];
}

- (void)kickout:(NSString *)errorDesc {
    [self clearCache];
    [[Authorization sharedInstance] authorizationWasInvalid:errorDesc];
}

/**
 * 登录
 */
- (void)loginWithMobile:(NSString *)mobile
               password:(NSString *)password
             completion:(void (^)(STIHTTPResponseError *e))completion {
    V1_API_AUTH_SIGNIN_API *api = [[V1_API_AUTH_SIGNIN_API alloc] init];
    api.req.mobile = mobile;
    api.req.password = password;
    api.whenUpdated = ^(V1_API_AUTH_SIGNIN_RESPONSE *resp, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if (error) {
            PERFORM_BLOCK_SAFELY(completion, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                if (resp.token.length) {
                    self.token = resp.token;
                    self.user = resp.user;
                    [self saveCache];
                }
                PERFORM_BLOCK_SAFELY(completion, nil);
            } else {
                STIHTTPResponseError *errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(completion, errors);
            }
        }
    };
    [api send];
}

/**
 * 获取手机验证码
 */
- (void)sendCodeWithMobile:(NSString *)mobile
                      type:(CODE_TYPE)type
                completion:(void (^)(STIHTTPResponseError *e))completion {
    V1_API_AUTH_MOBILE_SEND_API *api = [[V1_API_AUTH_MOBILE_SEND_API alloc] init];
    api.req.mobile = mobile;
    api.req.type = type;
    api.whenUpdated = ^(V1_API_AUTH_MOBILE_SEND_RESPONSE *resp, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if (error) {
            PERFORM_BLOCK_SAFELY(completion, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                PERFORM_BLOCK_SAFELY(completion, nil);
            } else {
                STIHTTPResponseError *errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(completion, errors);
            }
        }
    };
    [api send];
}

/**
 * 获得手机验证码(验证验证码)
 */
- (void)verifyCodeWithMoblie:(NSString *)mobile
                        code:(NSString *)code
                        type:(CODE_TYPE)type
                  completion:(void (^)(STIHTTPResponseError *e))completion {
    V1_API_AUTH_MOBILE_CODE_VERIFY_API *api = [[V1_API_AUTH_MOBILE_CODE_VERIFY_API alloc] init];
    api.req.mobile = mobile;
    api.req.code = code;
    api.req.type = type;
    api.whenUpdated = ^(V1_API_AUTH_MOBILE_CODE_VERIFY_RESPONSE *resp, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if (error) {
            PERFORM_BLOCK_SAFELY(completion, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                PERFORM_BLOCK_SAFELY(completion, nil);
            } else {
                STIHTTPResponseError *errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(completion, errors);
            }
        }
    };
    [api send];
}

/**
 * 注册
 */
- (void)signupWithName:(NSString *)name
                mobile:(NSString *)mobile
                gender:(USER_GENDER)gender
                avatar:(NSString *)avater
              password:(NSString *)password
            completion:(void (^)(STIHTTPResponseError *e))completion {
    V1_API_AUTH_MOBILE_SIGNUP_API *api = [[V1_API_AUTH_MOBILE_SIGNUP_API alloc] init];
    api.req.name = name;
    api.req.mobile = mobile;
    api.req.gender = gender;
    api.req.avatar = avater;
    api.req.password = password;
    api.whenUpdated = ^(V1_API_AUTH_MOBILE_SIGNUP_RESPONSE *resp, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if (error) {
            PERFORM_BLOCK_SAFELY(completion, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                if (resp.token.length) {
                    self.token = resp.token;
                    self.user = resp.user;
                    [self saveCache];
                }
                PERFORM_BLOCK_SAFELY(completion, nil);
            } else {
                STIHTTPResponseError *errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(completion, errors);
            }
        }
    };
    [api send];
}

/**
 * 手机重置密码(忘记密码)
 */
- (void)resetPasswordWithMobile:(NSString *)mobile
                           code:(NSString *)code
                       password:(NSString *)password
                     completion:(void (^)(STIHTTPResponseError *e))completion {
    V1_API_AUTH_MOBILE_RESET_API *api = [[V1_API_AUTH_MOBILE_RESET_API alloc] init];
    api.req.mobile = mobile;
    api.req.code = code;
    api.req.password = password;
    api.whenUpdated = ^(V1_API_AUTH_MOBILE_RESET_RESPONSE *resp, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if (error) {
            PERFORM_BLOCK_SAFELY(completion, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                PERFORM_BLOCK_SAFELY(completion, nil);
            } else {
                STIHTTPResponseError *errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(completion, errors);
            }
        }
    };
    [api send];
}

/**
 * 第三方登录
 */
- (void)authSocialWithVendor:(SOCIAL_VENDOR)vendor
                access_token:(NSString *)access_token
                     open_id:(NSString *)open_id
                  completion:(void (^)(STIHTTPResponseError *e))completion {
    V1_API_AUTH_SOCIAL_API *api = [[V1_API_AUTH_SOCIAL_API alloc] init];
    api.req.vendor = vendor;
    api.req.access_token = access_token;
    api.req.open_id = open_id;
    api.whenUpdated = ^(V1_API_AUTH_SOCIAL_RESPONSE *resp, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if (error) {
            PERFORM_BLOCK_SAFELY(completion, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                if (resp.token.length) {
                    self.token = resp.token;
                    self.user = resp.user;
                    [self saveCache];
                }
                PERFORM_BLOCK_SAFELY(completion, nil);
            } else {
                STIHTTPResponseError *errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(completion, errors);
            }
        }
    };
    [api send];
}

/**
 * 第三方登录绑定手机号
 */
- (void)mobileBindWithMoblie:(NSString *)mobile
                        code:(NSString *)code
                    password:(NSString *)password
                  completion:(void (^)(STIHTTPResponseError *e))completion {
    V1_API_AUTH_MOBILE_BIND_API *api = [[V1_API_AUTH_MOBILE_BIND_API alloc] init];
    api.req.mobile = mobile;
    api.req.code = code;
    api.req.password = password;
    api.whenUpdated = ^(V1_API_AUTH_MOBILE_BIND_RESPONSE *resp, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if (error) {
            PERFORM_BLOCK_SAFELY(completion, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                if (resp.token.length) {
                    self.token = resp.token;
                    self.user = resp.user;
                    [self saveCache];
                }
                PERFORM_BLOCK_SAFELY(completion, nil);
            } else {
                STIHTTPResponseError *errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(completion, errors);
            }
        }
    };
    [api send];
}

/**
 * 绑定第三方账号
 */
- (void)socialBindWithVendor:(SOCIAL_VENDOR)vendor
                access_token:(NSString *)access_token
                     open_id:(NSString *)open_id
                  completion:(void (^)(STIHTTPResponseError *e))completion {
    V1_API_AUTH_SOCIAL_BIND_API *api = [[V1_API_AUTH_SOCIAL_BIND_API alloc] init];
    api.req.vendor = vendor;
    api.req.access_token = access_token;
    api.req.open_id = open_id;
    api.whenUpdated = ^(V1_API_AUTH_SOCIAL_BIND_RESPONSE *resp, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if (error) {
            PERFORM_BLOCK_SAFELY(completion, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                PERFORM_BLOCK_SAFELY(completion, nil);
            } else {
                STIHTTPResponseError *errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(completion, errors);
            }
        }
    };
    [api send];
}

/**
 * 解绑第三方账号
 */
- (void)socialUnbindWithVendor:(SOCIAL_VENDOR)vendor
                    completion:(void (^)(STIHTTPResponseError *e))completion {
    V1_API_AUTH_SOCIAL_UNBIND_API *api = [[V1_API_AUTH_SOCIAL_UNBIND_API alloc] init];
    api.req.vendor = vendor;
    api.whenUpdated = ^(V1_API_AUTH_SOCIAL_UNBIND_RESPONSE *resp, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if (error) {
            PERFORM_BLOCK_SAFELY(completion, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                PERFORM_BLOCK_SAFELY(completion, nil);
            } else {
                STIHTTPResponseError *errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(completion, errors);
            }
        }
    };
    [api send];
}

/**
 * 获取用户资料
 */
- (void)getUserProfileWithUser_id:(NSString *)user_id
                       completion:(void (^)(STIHTTPResponseError *e))completion {
    V1_API_USER_PROFILE_GET_API *api = [[V1_API_USER_PROFILE_GET_API alloc] init];
    if (user_id) {
        api.req.user_id = user_id;
    }
    api.whenUpdated = ^(V1_API_USER_PROFILE_GET_RESPONSE *resp, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if (error) {
            PERFORM_BLOCK_SAFELY(completion, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                self.user = resp.user;
                [self saveCache];
                PERFORM_BLOCK_SAFELY(completion, nil);
            } else {
                STIHTTPResponseError *errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(completion, errors);
            }
        }
    };
    [api send];
}

/**
 * 修改用户资料
 */
- (void)updateUserProfileWithGender:(USER_GENDER)gender
                               name:(NSString *)name
                              email:(NSString *)email
                           birthday:(NSString *)birthday
                          introduce:(NSString *)introduce
                             avatar:(NSString *)avatar
                        cover_photo:(NSString *)cover_photo
                      constellation:(NSString *)constellation
                           location:(NSString *)location
                         completion:(void (^)(STIHTTPResponseError *e))completion; {
    V1_API_USER_PROFILE_UPDATE_API *api = [[V1_API_USER_PROFILE_UPDATE_API alloc] init];
    api.req.gender = gender;
    api.req.name = name;
    api.req.email = email;
    api.req.birthday = birthday;
    api.req.introduce = introduce;
    api.req.avatar = avatar;
    api.req.cover_photo = cover_photo;
    api.req.constellation = constellation;
    api.req.location = location;
    api.whenUpdated = ^(V1_API_USER_PROFILE_UPDATE_RESPONSE *resp, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if (error) {
            PERFORM_BLOCK_SAFELY(completion, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                self.user = resp.user;
                [self saveCache];
                PERFORM_BLOCK_SAFELY(completion, nil);
            } else {
                STIHTTPResponseError *errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(completion, errors);
            }
        }
    };
    [api send];
}

/**
 * 修改用户密码
 */
- (void)updateUserPasswordWithOld_password:(NSString *)old_password
                                  password:(NSString *)password
                                completion:(void (^)(STIHTTPResponseError *e))completion {
    V1_API_USER_PASSWORD_UPDATE_API *api = [[V1_API_USER_PASSWORD_UPDATE_API alloc] init];
    api.req.old_password = old_password;
    api.req.password = password;
    api.whenUpdated = ^(V1_API_USER_PASSWORD_UPDATE_RESPONSE *resp, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if (error) {
            PERFORM_BLOCK_SAFELY(completion, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                PERFORM_BLOCK_SAFELY(completion, nil);
            } else {
                STIHTTPResponseError *errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(completion, errors);
            }
        }
    };
    [api send];
}

@end
