//
//  UserModel.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/10/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "STIOnceModel.h"

extern NSNotificationName kMPUserInfoDidChangedNotification;

@interface UserModel : STIOnceModel 

@singleton(UserModel)

@property (nonatomic, strong) USER *user;
@property (nonatomic, strong) NSString *token;

+ (BOOL)online;
- (BOOL)online;

- (void)signout;
- (void)kickout:(NSString *)errorDesc;

/**
 * 登录
 */
- (void)loginWithMobile:(NSString *)mobile
               password:(NSString *)password
             completion:(void (^)(STIHTTPResponseError *e))completion;

/**
 * 获取手机验证码
 */
- (void)sendCodeWithMobile:(NSString *)mobile
                      type:(CODE_TYPE)type
                completion:(void (^)(STIHTTPResponseError *e))completion;

/**
 * 获得手机验证码(验证验证码)
 */
- (void)verifyCodeWithMoblie:(NSString *)mobile
                        code:(NSString *)code
                        type:(CODE_TYPE)type
                  completion:(void (^)(STIHTTPResponseError *e))completion;

/**
 * 注册
 */
- (void)signupWithName:(NSString *)name
                mobile:(NSString *)mobile
                gender:(USER_GENDER)gender
                avatar:(NSString *)avater
              password:(NSString *)password
            completion:(void (^)(STIHTTPResponseError *e))completion;

/**
 * 手机重置密码(忘记密码)
 */
- (void)resetPasswordWithMobile:(NSString *)mobile
                           code:(NSString *)code
                       password:(NSString *)password
                     completion:(void (^)(STIHTTPResponseError *e))completion;

/**
 * 第三方登录
 */
- (void)authSocialWithVendor:(SOCIAL_VENDOR)vendor
                access_token:(NSString *)access_token
                     open_id:(NSString *)open_id
                  completion:(void (^)(STIHTTPResponseError *e))completion;

/**
 * 第三方登录绑定手机号
 */
- (void)mobileBindWithMoblie:(NSString *)mobile
                        code:(NSString *)code
                    password:(NSString *)password
                  completion:(void (^)(STIHTTPResponseError *e))completion;

/**
 * 绑定第三方账号
 */
- (void)socialBindWithVendor:(SOCIAL_VENDOR)vendor
                access_token:(NSString *)access_token
                     open_id:(NSString *)open_id
                  completion:(void (^)(STIHTTPResponseError *e))completion;

/**
 * 解绑第三方账号
 */
- (void)socialUnbindWithVendor:(SOCIAL_VENDOR)vendor
                    completion:(void (^)(STIHTTPResponseError *e))completion;

/**
 * 获取用户资料
 */
- (void)getUserProfileWithUser_id:(NSString *)user_id
                       completion:(void (^)(STIHTTPResponseError *e))completion;

/**
 * 修改用户资料
 */
- (void)updateUserProfileWithGender:(USER_GENDER)gender
                               name:(NSString *)name
                              email:(NSString *)email
                           birthday:(NSString *)birthday
                          introduce:(NSString *)introduce
                             avatar:(NSString *)avatar
                        cover_photo:(NSString *)cover_photo
                      constellation:(NSString *)constellation
                           location:(NSString *)location
                         completion:(void (^)(STIHTTPResponseError *e))completion;
/**
 * 修改用户密码
 */
- (void)updateUserPasswordWithOld_password:(NSString *)old_password
                                  password:(NSString *)password
                                completion:(void (^)(STIHTTPResponseError *e))completion;

@end
