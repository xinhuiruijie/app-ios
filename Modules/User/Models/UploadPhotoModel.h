//
//  UploadPhotoModel.h
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/14.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "STIOnceModel.h"

@interface UploadPhotoModel : STIOnceModel

/**
 * 上传图片
 */
+ (void)uploadWithFile:(UIImage *)file
            completion:(void (^)(NSString *url, STIHTTPResponseError *e))completion;

@end
