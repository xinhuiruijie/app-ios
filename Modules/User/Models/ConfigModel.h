//
//  ConfigModel.h
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/17.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "STIOnceModel.h"

@interface ConfigModel : STIOnceModel

@singleton(ConfigModel);

@property (nonatomic, strong) NSString *share_url; // 分享链接URL
@property (nonatomic, strong) NSString *register_url; // 用户注册协议URL
@property (nonatomic, strong) NSString *feedback_url; // 意见反馈URL
@property (nonatomic, strong) NSString *copyright_url; // 版权声明URL
@property (nonatomic, strong) NSString *about_url; // 关于我们URL
@property (nonatomic, strong) NSString *app_guide_url; // 软件说明URL
@property (nonatomic, strong) NSString *integral_mall_url; // 积分商城URL
@property (nonatomic, strong) NSString *score_guide_url; // 任务攻略URL
@property (nonatomic, strong) NSString *apply_author_url; // 我要投稿URL
@property (nonatomic, strong) NSString *mall_disclaimer_url; // 商城免责URL
@property (nonatomic, strong) NSString *app_introduce_url; // 应用简介URL

/**
 * 获取协议
 */
- (void)getPotocolUrlWithCompletion:(void (^)(NSString * register_url, STIHTTPResponseError *e))completion;

@end
