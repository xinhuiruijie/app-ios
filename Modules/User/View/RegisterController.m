//
//  RegisterController.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/13.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "RegisterController.h"
#import "FinishProfileInfoController.h"
#import "VerifyCodeButton.h"
#import "ProfileSettingController.h"

@interface RegisterController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *mobileTextField;
@property (weak, nonatomic) IBOutlet UITextField *codeTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (weak, nonatomic) IBOutlet VerifyCodeButton *verifyButton;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;

@end

@implementation RegisterController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Auth"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    @weakify(self)
    self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
        @strongify(self)
        if (self.type == CODE_TYPE_MODIFY_MOBILE) {
            [[UserModel sharedInstance] clearCache];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [self customize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - CustomMethod

- (void)customize {
    self.confirmButton.layer.cornerRadius = 22;
    self.confirmButton.layer.masksToBounds = YES;
    if (self.type == CODE_TYPE_REGISTER) {
        [self.confirmButton setTitle:@"下一步" forState:UIControlStateNormal];
        self.navigationItem.title = @"用户注册";
    }
    if (self.type == CODE_TYPE_FORGOT_PASSWORD) {
        [self.confirmButton setTitle:@"确定" forState:UIControlStateNormal];
        self.navigationItem.title = @"忘记密码";
        self.passwordTextField.placeholder = @"请输入新密码";
        self.confirmPasswordTextField.placeholder = @"请确认新密码";
    }
    if (self.type == CODE_TYPE_MODIFY_MOBILE) {
        [self.confirmButton setTitle:@"绑定" forState:UIControlStateNormal];
        self.navigationItem.title = @"绑定手机";
        @weakify(self)
        self.navigationItem.leftBarButtonItem = [AppTheme normalItemWithContent:@" " handler:^(id sender) {
            
        }];
        NSString *backTitle = (self.backType == MPRegisterBackTypeCancel) ? @"取消" : @"跳过";
        self.navigationItem.rightBarButtonItem = [AppTheme lightGrayItemWithContent:backTitle handler:^(id sender) {
            @strongify(self)
            [self jumpAction];
        }];
    }
    
    [self.mobileTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    [self.codeTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    self.passwordTextField.delegate = self;
    self.confirmPasswordTextField.delegate = self;
}

- (void)textFieldDidChange:(UITextField *)textField {
    if (textField == self.mobileTextField) {
        if (textField.text.length > 11) {
            textField.text = [textField.text substringToIndex:11];
        }
    }
    if (textField == self.codeTextField) {
        if (textField.text.length > 6) {
            textField.text = [textField.text substringToIndex:6];
        }
    }
}

//满足注册条件
- (BOOL)checkConditionOfRegister {
    if (self.mobileTextField.text.length == 0 || [self.mobileTextField.text isEqualToString:[AppTheme defaultPlaceholder]]) {
        [self.view presentMessageTips:@"请输入手机号码"];
        [self.mobileTextField becomeFirstResponder];
        return NO;
    }
    if (self.mobileTextField.text.length != 11) {
        [self.view presentMessageTips:@"请输入正确的手机号码"];
        [self.mobileTextField becomeFirstResponder];
        return NO;
    }
    if (self.codeTextField.text.length == 0 || [self.codeTextField.text isEqualToString:[AppTheme defaultPlaceholder]]) {
        [self.view presentMessageTips:@"请输入验证码"];
        [self.codeTextField becomeFirstResponder];
        return NO;
    }
    if (self.codeTextField.text.length != 6) {
        [self.view presentMessageTips:@"请输入6位验证码"];
        [self.codeTextField becomeFirstResponder];
        return NO;
    }
    if (self.passwordTextField.text.length == 0 || [self.passwordTextField.text isEqualToString:[AppTheme defaultPlaceholder]]) {
        [self.view presentMessageTips:@"请输入密码"];
        [self.passwordTextField becomeFirstResponder];
        return NO;
    }
    if (self.passwordTextField.text.length < 6 || self.passwordTextField.text.length > 19) {
        [self.view presentMessageTips:@"请输入6-19位的密码"];
        [self.passwordTextField becomeFirstResponder];
        return NO;
    }
    if (self.confirmPasswordTextField.text.length == 0 || [self.confirmPasswordTextField.text isEqualToString:[AppTheme defaultPlaceholder]]) {
        [self.view presentMessageTips:@"请输入确认密码"];
        [self.confirmPasswordTextField becomeFirstResponder];
        return NO;
    }
    if (self.confirmPasswordTextField.text.length < 6 || self.confirmPasswordTextField.text.length > 19) {
        [self.view presentMessageTips:@"请输入6-19位的密码"];
        [self.confirmPasswordTextField becomeFirstResponder];
        return NO;
    }
    if (![self.confirmPasswordTextField.text isEqualToString:self.passwordTextField.text]) {
        [self.view presentMessageTips:@"两次输入的密码不一致"];
        [self.confirmPasswordTextField becomeFirstResponder];
        return NO;
    }
    return YES;
}

- (void)backAction {
    if ([UserModel online]) {
        for (UIViewController *viewController in self.navigationController.viewControllers) {
            if ([viewController isKindOfClass:[ProfileSettingController class]]) {
                [self.navigationController popToViewController:viewController animated:NO];
            }
        }
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)jumpAction {
    if ([UserModel online]) {
        for (UIViewController *viewController in self.navigationController.viewControllers) {
            if ([viewController isKindOfClass:[ProfileSettingController class]]) {
                [self.navigationController popToViewController:viewController animated:NO];
                return;
            }
        }
        [[MPRootViewController sharedInstance].currentViewController.view presentSuccessTips:@"登录成功"];
        [[Authorization sharedInstance] authorizationWasSucceed];
        [self dismissViewControllerAnimated:YES completion:nil];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *spaceString = [[string componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]componentsJoinedByString:[AppTheme defaultPlaceholder]];
    if (![string isEqualToString:spaceString]) {
        return NO;
    }
    return YES;
}

#pragma mark - IBAction

//获取验证码
- (IBAction)getCodeAction:(id)sender {
    if (self.mobileTextField.text.length == 0 || [self.mobileTextField.text isEqualToString:[AppTheme defaultPlaceholder]]) {
        [self.view presentMessageTips:@"请输入手机号码"];
        [self.mobileTextField becomeFirstResponder];
        return;
    }
    if (self.mobileTextField.text.length != 11) {
        [self.view presentMessageTips:@"请输入正确的手机号码"];
        [self.mobileTextField becomeFirstResponder];
        return;
    }
    [self.view presentLoadingTips:nil];
    @weakify(self)
    [[UserModel sharedInstance] sendCodeWithMobile:self.mobileTextField.text type:self.type completion:^(STIHTTPResponseError *e) {
        @strongify(self)
        [self.view dismissTips];
        if (e == nil) {
            [self.codeTextField becomeFirstResponder];
            [self.view presentSuccessTips:@"发送成功"];
            [self.verifyButton start];
        } else {
            [self.view presentMessage:e.message withTips:@"发送失败"];
        }
    }];
}

- (IBAction)confirmAction:(id)sender {
    if (![self checkConditionOfRegister]) {
        return;
    }
    [self.view endEditing:YES];
    [self.view presentLoadingTips:nil];
    //注册
    if (self.type == CODE_TYPE_REGISTER) {
        @weakify(self)
        [[UserModel sharedInstance] verifyCodeWithMoblie:self.mobileTextField.text code:self.codeTextField.text type:CODE_TYPE_REGISTER completion:^(STIHTTPResponseError *e) {
            @strongify(self)
            [self.view dismissTips];
            if (e == nil) {
                FinishProfileInfoController *finishProfileInfo = [FinishProfileInfoController spawn];
                finishProfileInfo.pushType = PUSH_TYPE_REGISTER;
                finishProfileInfo.mobile = self.mobileTextField.text;
                finishProfileInfo.password = self.passwordTextField.text;
                [self.navigationController pushViewController:finishProfileInfo animated:YES];
            } else {
                [self.view presentMessage:e.message withTips:@"短信验证码错误"];
            }
        }];
    }
    //忘记密码
    if (self.type == CODE_TYPE_FORGOT_PASSWORD) {
        @weakify(self)
        [[UserModel sharedInstance] resetPasswordWithMobile:self.mobileTextField.text code:self.codeTextField.text password:self.passwordTextField.text completion:^(STIHTTPResponseError *e) {
            @strongify(self)
            [self.view dismissTips];
            if (e == nil) {
                [self.view presentSuccessTips:@"找回密码成功"];
                [self performSelector:@selector(backAction) withObject:nil afterDelay:0.7];
            } else {
                [self.view presentMessage:e.message withTips:@"修改密码失败"];
            }
        }];
    }
    //第三方登录绑定手机号
    if (self.type == CODE_TYPE_MODIFY_MOBILE) {
        @weakify(self)
        [[UserModel sharedInstance] mobileBindWithMoblie:self.mobileTextField.text code:self.codeTextField.text password:self.passwordTextField.text completion:^(STIHTTPResponseError *e) {
            @strongify(self)
            [self.view dismissTips];
            if (e == nil) {
                // 从设置页面进来的，就退回去
                for (UIViewController *viewController in self.navigationController.viewControllers) {
                    if ([viewController isKindOfClass:[ProfileSettingController class]]) {
                        [self.navigationController popToViewController:viewController animated:NO];
                        return;
                    }
                }
                [[Authorization sharedInstance] hideAuth];
            } else {
                [self.view presentMessage:e.message withTips:@"绑定失败"];
            }
        }];
    }
}

@end
