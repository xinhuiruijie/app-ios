//
//  RegisterController.h
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/13.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef enum : NSUInteger {
    MPRegisterBackTypeUnknown,
    MPRegisterBackTypeJump,
    MPRegisterBackTypeCancel,
} MPRegisterBackType;

@interface RegisterController : UITableViewController

@property (nonatomic, assign) CODE_TYPE type;
@property (nonatomic, assign) MPRegisterBackType backType;

@end
