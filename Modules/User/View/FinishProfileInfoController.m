//
//  FinishProfileInfoController.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/13.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "FinishProfileInfoController.h"
#import "LoginViewController.h"
#import "UserPointModel.h"

@interface FinishProfileInfoController () <UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UIButton *headImageButton;
@property (weak, nonatomic) IBOutlet UITextField *nicknameTextField;
@property (weak, nonatomic) IBOutlet UIButton *genderManButton;
@property (weak, nonatomic) IBOutlet UIButton *genderWomenButton;
@property (weak, nonatomic) IBOutlet UIButton *ensureButton;

@property (nonatomic, assign) BOOL gender;
@property (nonatomic, strong) NSString *avatar;

@end

@implementation FinishProfileInfoController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Auth"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"完善资料";
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    @weakify(self)
    self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
        @strongify(self)
        [self backAction];
    }];
    self.genderManButton.selected = YES;
    self.gender = YES;
    self.genderWomenButton.selected = NO;
    [self customize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - CustomMethod

- (void)backAction {
    MPAlertView *alertView = [MPAlertView alertWithTitle:@"确认离开" cancelButton:@"取消" confirmButton:@"确定"];
    @weakify(self)
    [alertView setConfirmAction:^{
        @strongify(self)
        if (self.pushType == PUSH_TYPE_LOGIN) {
            [[UserModel sharedInstance] signout];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [alertView show];
}

- (void)customize {
    self.headImageView.layer.cornerRadius = self.headImageView.bounds.size.width / 2.0;
    self.headImageView.layer.masksToBounds = YES;
    
    if ([SDiOSVersion deviceVersion] == iPhone5S) {
        self.ensureButton.layer.cornerRadius = 17;
    } else {
        self.ensureButton.layer.cornerRadius = 22;
    }
    self.ensureButton.layer.masksToBounds = YES;
    self.nicknameTextField.delegate = self;
}

//满足注册条件
- (BOOL)checkConditionOfRegister {
    if (self.nicknameTextField.text.length == 0 || [self.nicknameTextField.text isEqualToString:[AppTheme defaultPlaceholder]]) {
        [self presentMessageTips:@"请输入昵称"];
        [self.nicknameTextField becomeFirstResponder];
        return NO;
    }
    if (self.nicknameTextField.text.length > 16 ) {
        [self.view presentFailureTips:@"不能超过8个汉字或者16个字符"];
        [self.nicknameTextField becomeFirstResponder];
        return NO;
    }
    return YES;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *spaceString = [[string componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]componentsJoinedByString:[AppTheme defaultPlaceholder]];
    if (![string isEqualToString:spaceString]) {
        return NO;
    }
    return YES;
}

#pragma mark - IBAction

//选择头像
- (IBAction)selectHeadImageAction:(id)sender {
    [self.view endEditing:YES];
    GKZActionSheet *actionSheet = [GKZActionSheet loadFromNib];
    AlertView * alertView = [[AlertView alloc] initWithContent:actionSheet type:AlertViewTypeMonospaced];
    actionSheet.data = @[@"拍照", @"图库"];
    [alertView showSharedView];
    actionSheet.whenHide = ^(BOOL hide) {
        [alertView hide];
    };
    actionSheet.whenRegistered = ^(id data, NSInteger index) {
        [alertView hide];
        if (index == 0) {
            // 拍照
            if ( [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] ) {
                UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                imagePickerController.delegate = self;
                imagePickerController.navigationBar.tintColor = [UIColor blackColor];
                [imagePickerController.navigationBar setTitleTextAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:17], NSForegroundColorAttributeName : [UIColor blackColor]}];
                imagePickerController.allowsEditing = YES;
                imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:imagePickerController animated:YES completion:NULL];
            } else {
                [self.view presentFailureTips:@"请在设置中打开摄像头权限"];
            }
        } else if (index == 1) {
            // 选照片
            if ( [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary] ) {
                UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                imagePickerController.delegate = self;
                imagePickerController.navigationBar.tintColor = [UIColor blackColor];
                [imagePickerController.navigationBar setTitleTextAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:17], NSForegroundColorAttributeName : [UIColor blackColor]}];
                imagePickerController.allowsEditing = YES;
                imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                [self presentViewController:imagePickerController animated:YES completion:NULL];
            } else {
                [self.view presentFailureTips:@"请在设置中打开相册权限"];
            }
        }
    };
}

//注册
- (IBAction)ensureButton:(id)sender {
    if (![self checkConditionOfRegister]) {
        return;
    }
    [self.view endEditing:YES];
    [self.view presentLoadingTips:nil];
    USER_GENDER user_gender = self.gender == YES ? USER_GENDER_MALE : USER_GENDER_FEMALE;
    if (self.pushType == PUSH_TYPE_REGISTER) {
        @weakify(self)
        [[UserModel sharedInstance] signupWithName:self.nicknameTextField.text mobile:self.mobile gender:user_gender avatar:self.avatar password:self.password completion:^(STIHTTPResponseError *e) {
            @strongify(self)
            [self dismissTips];
            if (e == nil) {
                [[Authorization sharedInstance] authorizationWasSucceed];
                [UserPointModel userPointName:MODULE_NAME_CLICK_REGISTER_FINISH];
            } else {
                [self.view presentMessage:e.message withTips:@"注册失败"];
            }
        }];
    } else {
        @weakify(self)
        [[UserModel sharedInstance] updateUserProfileWithGender:user_gender name:self.nicknameTextField.text email:nil birthday:nil introduce:nil avatar:self.avatar cover_photo:nil constellation:nil location:nil completion:^(STIHTTPResponseError *e) {
            @strongify(self)
            [self.view dismissTips];
            if (e == nil) {
                [[Authorization sharedInstance] authorizationWasSucceed];
            } else {
                [self.view presentMessage:e.message withTips:@"登录失败"];
            }
        }];
    }
}

//选择性别（男）
- (IBAction)selectGenderMan:(UIButton *)sender {
    if (!sender.selected) {
        self.genderManButton.selected = !self.genderManButton.selected;
        self.genderWomenButton.selected = !self.genderWomenButton.selected;
        self.gender = YES;
    }
}

//选择性别（女）
- (IBAction)selectGenderWoman:(UIButton *)sender {
    if (!sender.selected) {
        self.genderWomenButton.selected = !self.genderWomenButton.selected;
        self.genderManButton.selected = !self.genderManButton.selected;
        self.gender = NO;
    }
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *image = info[UIImagePickerControllerEditedImage] ? : info[UIImagePickerControllerOriginalImage];
    @weakify(self);
    [picker dismissViewControllerAnimated:YES completion:^{
        @strongify(self);
        [self uploadPhotoWithImage:image];
    }];
}

- (void)uploadPhotoWithImage:(UIImage *)image {
    @weakify(self)
    [image fixedCameraImageToSize:CGSizeMake(kScreenWidth, kScreenWidth) then:^(UIImage * fixedImg) {
        [UploadPhotoModel uploadWithFile:fixedImg completion:^(NSString *url, STIHTTPResponseError *e) {
            @strongify(self)
            if (e) {
                [self.view presentMessage:e.message withTips:@"上传失败"];
            } else {
                [self.headImageView setImageWithString:url];
                self.avatar = url;
                [self.tableView reloadData];
            }
        }];
    }];
}

#pragma mark - UINavigationControllerDelegate

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if (navigationController.viewControllers.count > 1) {
        UIViewController *imageVC = navigationController.viewControllers[1];
        if ([imageVC isEqual:viewController]) {
            __weak typeof(imageVC) weakImageVC = imageVC;
            imageVC.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
                [weakImageVC.navigationController popViewControllerAnimated:YES];
            }];
        }
    }
}

@end
