//
//  LoginViewController.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/12.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "LoginViewController.h"
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import "LoginItem.h"
#import "RegisterController.h"
#import "PotocolUrlController.h"
#import "FinishProfileInfoController.h"
#import "UserPointModel.h"

@interface LoginViewController () <UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *playerView;
@property (weak, nonatomic) IBOutlet UITextField *mobileTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *potocolButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *contentSizeHLC;
@property (weak, nonatomic) IBOutlet UIView *loginTypeView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *loginTypeViewWLC;

@property (nonatomic, strong) NSURL *url;
@property (nonatomic, strong) AVPlayer *player;
@property (nonatomic, strong) AVPlayerItem *playerItem;
@property (nonatomic, strong) AVPlayerLayer *playerLayer;

@property (nonatomic, strong) NSMutableArray *loginItems;

@end

@implementation LoginViewController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Auth"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    self.loginItems = [NSMutableArray array];
    [self loadLoginItems];
    [self customize];
    [self setupVideoPlayer];
    @weakify(self)
    self.navigationItem.leftBarButtonItem = [AppTheme whiteBackItemWithHandler:^(id sender) {
        @strongify(self)
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)dealloc {
    self.player = nil;
    self.playerLayer = nil;
    self.playerItem = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - Customed

- (void)customize {
    //手机号
    NSString *moblieString = @"请输入手机号";
    NSMutableAttributedString *mobilePlaceholder = [[NSMutableAttributedString alloc] initWithString:moblieString];
    [mobilePlaceholder addAttribute:NSForegroundColorAttributeName value:[AppTheme LoginPlaceholderColor] range:NSMakeRange(0, moblieString.length)];
    self.mobileTextField.attributedPlaceholder = mobilePlaceholder;
    
    //密码
    NSString *passwordString = @"密码";
    NSMutableAttributedString *passwordPlaceholder = [[NSMutableAttributedString alloc] initWithString:passwordString];
    [passwordPlaceholder addAttribute:NSForegroundColorAttributeName value:[AppTheme LoginPlaceholderColor] range:NSMakeRange(0, passwordString.length)];
    self.passwordTextField.attributedPlaceholder = passwordPlaceholder;
    
    if ([SDiOSVersion deviceVersion] == iPhone5S) {
        self.loginButton.layer.cornerRadius = 17;
    } else {
        self.loginButton.layer.cornerRadius = 22;
    }
    self.loginButton.layer.masksToBounds = YES;
    
    //协议
    NSString *defaultString = @"登录代表您同意母星系的";
    NSMutableAttributedString *potocolString = [[NSMutableAttributedString alloc] initWithString:defaultString];
    [potocolString addAttribute:NSForegroundColorAttributeName value:[AppTheme LoginPlaceholderColor] range:NSMakeRange(0, defaultString.length)];
    NSString *appendingString = @"《用户协议》";
    NSMutableAttributedString *appendingPotocolString = [[NSMutableAttributedString alloc] initWithString:appendingString];
    [appendingPotocolString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithRGBValue:0xEFB400] range:NSMakeRange(0, appendingString.length)];
    [potocolString appendAttributedString:appendingPotocolString];
    [self.potocolButton setAttributedTitle:potocolString forState:UIControlStateNormal];
    
    if ([SDiOSVersion resolutionSize] == Screen3Dot5inch) {
        self.contentSizeHLC.constant = 568;
    } else {
        self.contentSizeHLC.constant = kScreenHeight;
    }
    
    //其他登录方式
    [self.mobileTextField addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    self.passwordTextField.delegate = self;
    
    CGFloat itemWidth = (kScreenWidth - 60 - 120) / 4;
    CGFloat itemHeight = 70;
    CGFloat margin = 0;
    for (NSInteger i = 0; i < self.loginItems.count; i++) {
        LoginItem *item = [LoginItem loadFromNib];
        item.frame = CGRectMake(margin, 0, itemWidth, itemHeight);
        item.loginType = [[self.loginItems objectAtIndex:i] integerValue];
        margin += itemWidth;
        margin += 20;
        @weakify(self)
        [item setSelectBlock:^{
            @strongify(self)
            [self selectLoginItemWithIndex:[[self.loginItems objectAtIndex:i] integerValue]];
        }];
        [self.loginTypeView addSubview:item];
    }
    self.loginTypeViewWLC.constant = margin - 20;
}

- (void)textFieldDidChange:(UITextField *)textField {
    if (textField == self.mobileTextField) {
        if (textField.text.length > 11) {
            textField.text = [textField.text substringToIndex:11];
        }
    }
}

- (void)selectLoginItemWithIndex:(NSInteger)index {
    switch (index) {
        case 0:
            [self QQLogin];
            break;
        case 1:
            [self wechatLogin];
            break;
        case 2:
            [self sinaLogin];
            break;
        case 3:
        {
            [AppAnalytics clickEvent:@"click_login_register"];
            RegisterController *registerController = [RegisterController spawn];
            registerController.type = CODE_TYPE_REGISTER;
            [self.navigationController pushViewController:registerController animated:YES];
        }
            break;
        default:
            break;
    }
}

//加载登录方式类型
- (void)loadLoginItems {
    if ([AppConfig isShowQQ]) {
        [self.loginItems addObject:@(LOGIN_TYPE_QQ)];
    }
    if ([AppConfig isShowWechat]) {
        [self.loginItems addObject:@(LOGIN_TYPE_WECHAT)];
    }
    if ([AppConfig isShowWeibo]) {
        [self.loginItems addObject:@(LOGIN_TYPE_WEIBO)];
    }
    [self.loginItems addObject:@(LOGIN_TYPE_MOBILE)];
}

//背景播放短片
- (void)setupVideoPlayer {
    self.url = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"loginBackgroundMovie" ofType:@"mp4"]];
    
    self.playerItem = [[AVPlayerItem alloc] initWithURL:self.url];
    self.player = [[AVPlayer alloc] initWithPlayerItem:self.playerItem];
    [self.player play];
    
    self.playerLayer = [AVPlayerLayer playerLayerWithPlayer:self.player];
    self.playerLayer.frame = self.view.bounds;
    self.playerLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.playerView.layer addSublayer:self.playerLayer];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playbackFinished:) name:AVPlayerItemDidPlayToEndTimeNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(continuePlay) name:UIApplicationDidBecomeActiveNotification object:nil];
}

//循环播放
- (void)playbackFinished:(NSNotification *)notification {
    [self.player seekToTime:kCMTimeZero];
    [self.player play];
}

//继续播放
- (void)continuePlay {
    [self.player play];
}

//满足登录条件
- (BOOL)checkConditionOfLogin {
    if (self.mobileTextField.text.length == 0 || [self.mobileTextField.text isEqualToString:[AppTheme defaultPlaceholder]]) {
        [self.view presentFailureTips:@"请输入手机号码"];
        [self.mobileTextField becomeFirstResponder];
        return NO;
    }
    if (self.mobileTextField.text.length != 11) {
        [self.view presentFailureTips:@"请输入正确的手机号码"];
        [self.mobileTextField becomeFirstResponder];
        return NO;
    }
    if (self.passwordTextField.text.length == 0 || [self.passwordTextField.text isEqualToString:[AppTheme defaultPlaceholder]]) {
        [self.view presentFailureTips:@"请输入密码"];
        [self.passwordTextField becomeFirstResponder];
        return NO;
    }
    if (self.passwordTextField.text.length < 6 || self.passwordTextField.text.length > 19) {
        [self.view presentFailureTips:@"请输入6-19位密码"];
        [self.passwordTextField becomeFirstResponder];
        return NO;
    }
    return YES;
}


//微信登录
- (void)wechatLogin {
    [AppAnalytics clickEvent:@"click_login_loginByWeChat"];
    WXChatShared *wechat = [WXChatShared sharedInstance];
    @weakify(self)
    wechat.whenGetUserInfoSucceed = ^(id data) {
        @strongify(self)
        ACCOUNT *account = (ACCOUNT *)data;
        account.vendor = SOCIAL_VENDOR_WEIXIN;
        [self socialLoginWithAccount:account];
    };
    wechat.whenGetUserInfoFailed = ^{
        [self.view presentFailureTips:@"取消登录"];
    };
    [wechat getuserInfo];
}

//QQ登录
- (void)QQLogin {
    [AppAnalytics clickEvent:@"click_login_loginByQQ"];
    TencentOpenShared *tencent = [TencentOpenShared sharedInstance];
    @weakify(self)
    tencent.whenGetUserInfoSucceed = ^(id data) {
        @strongify(self)
        ACCOUNT *account = (ACCOUNT *)data;
        account.vendor = SOCIAL_VENDOR_QQ;
        [self socialLoginWithAccount:account];
    };
    tencent.whenGetUserInfoFailed = ^{
        [self.view presentFailureTips:@"取消登录"];
    };
    [tencent getUserInfo];
}

//微博登录
- (void)sinaLogin {
    [AppAnalytics clickEvent:@"click_login_loginByWeibo"];
    SinaWeibo *sina = [SinaWeibo sharedInstance];
    @weakify(self)
    sina.whenGetUserInfoSucceed = ^(id data) {
        @strongify(self)
        ACCOUNT *account = (ACCOUNT *)data;
        account.vendor = SOCIAL_VENDOR_WEIBO;
        [self socialLoginWithAccount:account];
    };
    sina.whenGetUserInfoFailed = ^{
        [self.view presentFailureTips:@"取消登录"];
    };
    [sina getUserInfo];
}

//第三方登录
- (void)socialLoginWithAccount:(ACCOUNT *)account {
    @weakify(self)
    [self.view presentLoadingTips:@"登录中"];
    [[UserModel sharedInstance] authSocialWithVendor:account.vendor access_token:account.auth_token open_id:account.user_id completion:^(STIHTTPResponseError *e) {
        @strongify(self)
        [self dismissTips];
        if (e == nil) {
            BOOL is_bind_mobile = [UserModel sharedInstance].user.is_bind_mobile;
            if (is_bind_mobile == NO) {
                RegisterController *bindMobile = [RegisterController spawn];
                bindMobile.type = CODE_TYPE_MODIFY_MOBILE;
                [self.navigationController pushViewController:bindMobile animated:YES];
            } else {
                [[MPRootViewController sharedInstance].currentViewController.view presentSuccessTips:@"登录成功"];
                [[Authorization sharedInstance] authorizationWasSucceed];
            }
        } else {
            [[Authorization sharedInstance] authorizationWasFailed];
            [self.view presentMessage:e.message withTips:@"登录失败"];
        }
    }];
}

#pragma mark - IBAction

- (IBAction)closeKeyboardAction:(id)sender {
    [self.view endEditing:YES];
}

/**
 * 忘记密码
 */
- (IBAction)forgotPasswordAction:(id)sender {
    RegisterController *forgotPassword = [RegisterController spawn];
    forgotPassword.type = CODE_TYPE_FORGOT_PASSWORD;
    [self.navigationController pushViewController:forgotPassword animated:YES];
}

/**
 * 登录
 */
- (IBAction)loginAction:(id)sender {
    [self.view endEditing:YES];
    [self.view presentLoadingTips:@"登录中"];
    if ([self checkConditionOfLogin]) {
        [AppAnalytics clickEvent:@"click_login_login"];
        @weakify(self)
        [[UserModel sharedInstance] loginWithMobile:self.mobileTextField.text password:self.passwordTextField.text completion:^(STIHTTPResponseError *e) {
            @strongify(self)
            [self.view dismissTips];
            if (e == nil) {
                if ([UserModel sharedInstance].user.name && ([UserModel sharedInstance].user.gender == USER_GENDER_MALE || [UserModel sharedInstance].user.gender == USER_GENDER_FEMALE)) {
                    [[MPRootViewController sharedInstance].currentViewController.view presentSuccessTips:@"登录成功"];
                    [UserPointModel userPointName:MODULE_NAME_CLICK_LOGIN_FINISH];
                    [[Authorization sharedInstance] authorizationWasSucceed];
                    [self dismissViewControllerAnimated:YES completion:nil];
                } else {
                    FinishProfileInfoController *finishInfo = [FinishProfileInfoController spawn];
                    finishInfo.pushType = PUSH_TYPE_LOGIN;
                    [self.navigationController pushViewController:finishInfo animated:YES];
                }
            } else {
                [self.view presentMessage:e.message withTips:@"登录失败"];
            }
        }];
    }
}

//查看《用户协议》
- (IBAction)viewPotocolAction:(id)sender {
    PotocolUrlController *potocolUrl = [PotocolUrlController spawn];
    potocolUrl.potocolType = POTOCOL_TYPE_USER;
    [self.navigationController pushViewController:potocolUrl animated:YES];
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *spaceString = [[string componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]componentsJoinedByString:[AppTheme defaultPlaceholder]];
    if (![string isEqualToString:spaceString]) {
        return NO;
    }
    return YES;
}

@end
