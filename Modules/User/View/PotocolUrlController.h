//
//  PotocolUrlController.h
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/15.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, POTOCOL_TYPE) {
    POTOCOL_TYPE_ABOUT = 0, //About Motherplanet
    POTOCOL_TYPE_USER, //用户协议
    POTOCOL_TYPE_COPYRIGHT, //版权声明
    POTOCOL_TYPE_SOFTWARE, //软件说明
    POTOCOL_TYPE_FEEDBACK, //意见反馈
    POTOCOL_TYPE_AUTHOR, //我要投稿
    POTOCOL_TYPE_TASK_GUIDE, //任务攻略
};

@interface PotocolUrlController : UIViewController

@property (nonatomic, assign) POTOCOL_TYPE potocolType;

@end
