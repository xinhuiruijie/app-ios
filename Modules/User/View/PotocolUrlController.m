//
//  PotocolUrlController.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/15.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "PotocolUrlController.h"

@interface PotocolUrlController ()

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@property (nonatomic, strong) NSString *url;

@end

@implementation PotocolUrlController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Auth"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.webView.backgroundColor = [UIColor whiteColor];
    @weakify(self)
    self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [self loadData];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)setPotocolType:(POTOCOL_TYPE)potocolType {
    _potocolType = potocolType;
    switch (potocolType) {
        case POTOCOL_TYPE_ABOUT:
            self.navigationItem.title = @"About Motherplanet";
            self.url = [ConfigModel sharedInstance].about_url;
            break;
        case POTOCOL_TYPE_USER:
            self.navigationItem.title = @"注册协议";
            self.url = [ConfigModel sharedInstance].register_url;
            break;
        case POTOCOL_TYPE_COPYRIGHT:
            self.navigationItem.title = @"版权声明";
            self.url = [ConfigModel sharedInstance].copyright_url;
            break;
        case POTOCOL_TYPE_SOFTWARE:
            self.navigationItem.title = @"软件说明";
            self.url = [ConfigModel sharedInstance].app_guide_url;
            break;
        case POTOCOL_TYPE_FEEDBACK:
            self.navigationItem.title = @"意见反馈";
            self.url = [ConfigModel sharedInstance].feedback_url;
            break;
        case POTOCOL_TYPE_AUTHOR:
            self.navigationItem.title = @"我要投稿";
            self.url = [ConfigModel sharedInstance].apply_author_url;
            break;
        case POTOCOL_TYPE_TASK_GUIDE:
            self.navigationItem.title = @"M币攻略";
            self.url = [ConfigModel sharedInstance].score_guide_url;
        default:
            break;
    }
}

//加载数据
- (void)loadData {
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:self.url]]];
}

@end
