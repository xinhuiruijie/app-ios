//
//  LoginItem.m
//  MotherPlanet
//
//  Created by 陈熙 on 2018/1/22.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "LoginItem.h"

@interface LoginItem ()

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation LoginItem

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.size = CGSizeMake((kScreenWidth - 60 - 120) / 4, self.superview.size.height);
}

- (void)setLoginType:(LOGIN_TYPE)loginType {
    _loginType = loginType;
    switch (loginType) {
        case LOGIN_TYPE_QQ:
            self.iconImageView.image = [UIImage imageNamed:@"icon_qq_login"];
            self.titleLabel.text = @"QQ";
            break;
        case LOGIN_TYPE_WECHAT:
            self.iconImageView.image = [UIImage imageNamed:@"icon_wechat_login"];
            self.titleLabel.text = @"微信";
            break;
        case LOGIN_TYPE_WEIBO:
            self.iconImageView.image = [UIImage imageNamed:@"icon_sina_login"];
            self.titleLabel.text = @"微博";
            break;
        case LOGIN_TYPE_MOBILE:
            self.iconImageView.image = [UIImage imageNamed:@"icon_phone_login"];
            self.titleLabel.text = @"用户注册";
        default:
            break;
    }
}
- (IBAction)selectItem:(id)sender {
    if (self.selectBlock) {
        self.selectBlock();
    }
}

@end
