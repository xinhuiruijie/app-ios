//
//  LoginItem.h
//  MotherPlanet
//
//  Created by 陈熙 on 2018/1/22.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, LOGIN_TYPE) {
    LOGIN_TYPE_QQ = 0,
    LOGIN_TYPE_WECHAT,
    LOGIN_TYPE_WEIBO,
    LOGIN_TYPE_MOBILE,
};

@interface LoginItem : UIView

@property (nonatomic, copy) void (^selectBlock)(void);

@property (nonatomic, assign) LOGIN_TYPE loginType;

@end
