//
//  FinishProfileInfoController.h
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/13.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, PUSH_TYPE) {
    PUSH_TYPE_REGISTER,
    PUSH_TYPE_LOGIN,
};

@interface FinishProfileInfoController : UITableViewController

@property (nonatomic, strong) NSString *mobile;

@property (nonatomic, strong) NSString *password;

@property (nonatomic, assign) PUSH_TYPE pushType;

@end
