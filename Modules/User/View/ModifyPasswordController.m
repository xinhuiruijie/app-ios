//
//  ModifyPasswordController.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/13.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "ModifyPasswordController.h"
#import "RegisterController.h"

@interface ModifyPasswordController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITextField *oldPasswordTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTextField;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *oldPasswordLineHLC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *passwordLineHLC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *confirmPasswordLineHLC;

@end

@implementation ModifyPasswordController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Auth"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self customize];
    self.navigationItem.title = @"修改密码";
    self.confirmButton.layer.cornerRadius = 20;
    self.confirmButton.layer.masksToBounds = YES;
    @weakify(self)
    self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - CustomMethod

- (void)customize {
    self.oldPasswordLineHLC.constant = [AppTheme onePixel];
    self.passwordLineHLC.constant = [AppTheme onePixel];
    self.confirmPasswordLineHLC.constant = [AppTheme onePixel];
    
    self.oldPasswordTextField.delegate = self;
    self.passwordTextField.delegate = self;
    self.confirmPasswordTextField.delegate = self;
}

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

//满足修改条件
- (BOOL)checkConditionOfModify {
    if (self.oldPasswordTextField.text.length == 0 || [self.oldPasswordTextField.text isEqualToString:[AppTheme defaultPlaceholder]]) {
        [self.view presentMessageTips:@"请输入旧密码"];
        [self.oldPasswordTextField becomeFirstResponder];
        return NO;
    }
    if (self.oldPasswordTextField.text.length < 6 || self.oldPasswordTextField.text.length > 19) {
        [self.view presentFailureTips:@"请输入6-19位密码"];
        [self.oldPasswordTextField becomeFirstResponder];
        return NO;
    }
    if (self.passwordTextField.text.length == 0 || [self.passwordTextField.text isEqualToString:[AppTheme defaultPlaceholder]]) {
        [self.view presentMessageTips:@"请输入新密码"];
        [self.passwordTextField becomeFirstResponder];
        return NO;
    }
    if (self.passwordTextField.text.length < 6 || self.passwordTextField.text.length > 19) {
        [self.view presentFailureTips:@"请输入6-19位密码"];
        [self.passwordTextField becomeFirstResponder];
        return NO;
    }
    if (self.confirmPasswordTextField.text.length == 0 || [self.confirmPasswordTextField.text isEqualToString:[AppTheme defaultPlaceholder]]) {
        [self.view presentMessageTips:@"请再次输入新密码"];
        [self.confirmPasswordTextField becomeFirstResponder];
        return NO;
    }
    if (![self.passwordTextField.text isEqualToString:self.confirmPasswordTextField.text]) {
        [self.view presentMessageTips:@"两次输入密码不一致"];
        [self.confirmPasswordTextField becomeFirstResponder];
        return NO;
    }
    return YES;
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *spaceString = [[string componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]]componentsJoinedByString:[AppTheme defaultPlaceholder]];
    if (![string isEqualToString:spaceString]) {
        return NO;
    }
    return YES;
}

#pragma mark - IBAction

//忘记密码
- (IBAction)forgotPasswordAction:(id)sender {
    RegisterController *forgotPassword = [RegisterController spawn];
    forgotPassword.type = CODE_TYPE_FORGOT_PASSWORD;
    [self.navigationController pushViewController:forgotPassword animated:YES];
}

//修改密码
- (IBAction)confirmAction:(id)sender {
    if (![self checkConditionOfModify]) {
        return;
    }
    [self.view endEditing:YES];
    [self.view presentLoadingTips:nil];
    @weakify(self)
    [[UserModel sharedInstance] updateUserPasswordWithOld_password:self.oldPasswordTextField.text password:self.passwordTextField.text completion:^(STIHTTPResponseError *e) {
        @strongify(self)
        [self.view dismissTips];
        if (e == nil) {
            [self.view presentSuccessTips:@"修改密码成功"];
            [self performSelector:@selector(backAction) withObject:nil afterDelay:0.7];
        } else {
            [self.view presentMessage:e.message withTips:@"修改密码失败"];
        }
    }];
}

@end
