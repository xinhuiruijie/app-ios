//
//  RecentAuthorListController.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "RecentAuthorListController.h"
#import "AuthorInfoController.h"
#import "AuthorRecentListModel.h"
#import "AuthorModel.h"
#import "AuthorListCell.h"

@interface RecentAuthorListController () <UITableViewDelegate, UITableViewDataSource, AuthorListCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) AuthorRecentListModel *model;

@end

@implementation RecentAuthorListController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Planet"];
}

- (UIScrollView *)list {
    return self.tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"最新加入星球";
    @weakify(self)
    self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    self.tableView.rowHeight = 66;
    self.tableView.backgroundColor = [AppTheme listBackgroundColor];
    [self.tableView registerNib:[AuthorListCell nib] forCellReuseIdentifier:@"AuthorListCell"];
    
    self.model = [[AuthorRecentListModel alloc] init];
    self.model.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        [self.tableView.header endRefreshing];
        [self.tableView reloadData];
        if (error == nil) {
            [self setupRefreshFooterView];
            if (self.model.more) {
                [self.tableView.footer endRefreshing];
            } else {
                [self.tableView.footer noticeNoMoreData];
            }
        } else {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
    };
    
    [self.list.header beginRefreshing];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.model.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    AuthorListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AuthorListCell" forIndexPath:indexPath];
    cell.data = self.model.items[indexPath.row];
    cell.delegate = self;
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    AuthorInfoController *authorInfo = [AuthorInfoController spawn];
    USER *author = self.model.items[indexPath.row];
    authorInfo.author = author;
    authorInfo.followAuthor = ^(USER *author) {
        for (int i = 0; i < self.model.items.count; i++) {
            USER *perAuthor = self.model.items[i];
            if ([perAuthor.id isEqualToString:author.id]) {
                [self.model.items replaceObjectAtIndex:i withObject:author];
                break;
            }
        }
        [self.tableView reloadData];
    };
    [self presentNavigationController:authorInfo];
}

#pragma mark - AuthorListCell

- (void)followAuthor:(USER *)author sender:(UIButton *)sender {
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }
    
    [sender followAuthorButtonAnimated];
    
    [sender disableSelf];
    [self presentLoadingTips:nil];
    if (author.is_follow) {
        [AuthorModel unfollow:author then:^(STIHTTPResponseError *error) {
            if (error == nil) {
                author.is_follow = NO;
                [self.tableView reloadData];
//                [self.model refresh];
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
            [self dismissTips];
            [sender enableSelf];
        }];
    } else {
        if (!author.is_author) {
            [self dismissTips];
            [[UIApplication sharedApplication].keyWindow presentMessageTips:@"作者不存在！"];
            return;
        }
        
        [AuthorModel follow:author then:^(STIHTTPResponseError *error) {
            if (error == nil) {
                author.is_follow = YES;
                [self.tableView reloadData];
//                [self.model refresh];
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
            [self dismissTips];
            [sender enableSelf];
        }];
    }
}

@end
