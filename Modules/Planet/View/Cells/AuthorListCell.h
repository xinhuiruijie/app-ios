//
//  AuthorListCell.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol AuthorListCellDelegate <NSObject>

@optional
- (void)followAuthor:(USER *)author sender:(UIButton *)sender;

@end

@interface AuthorListCell : UITableViewCell

@property (nonatomic, weak) id<AuthorListCellDelegate> delegate;

@end
