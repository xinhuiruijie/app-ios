//
//  AuthorListCell.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "AuthorListCell.h"

@interface AuthorListCell ()

@property (weak, nonatomic) IBOutlet UIImageView *authorAvatar;
@property (weak, nonatomic) IBOutlet UILabel *authorName;
@property (weak, nonatomic) IBOutlet UILabel *authorDesc;
@property (weak, nonatomic) IBOutlet UIImageView *isFollowIcon;
@property (weak, nonatomic) IBOutlet UILabel *isFollowLabel;
@property (weak, nonatomic) IBOutlet UIView *authorFollowView;
@property (weak, nonatomic) IBOutlet UIButton *followButton;

@end

@implementation AuthorListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)dataDidChange {
    USER *author = self.data;
    if (author.avatar) {
        [self.authorAvatar setImageWithPhoto:author.avatar placeholderImage:[AppTheme avatarDefaultImage]];
    } else {
        self.authorAvatar.image = [AppTheme avatarDefaultImage];
    }
    self.authorName.text = author.name;
    self.authorDesc.text = author.introduce;
    
    if (author == nil || [author.id isEqualToString:[UserModel sharedInstance].user.id]) {
        self.authorFollowView.hidden = YES;
    } else {
        self.authorFollowView.hidden = NO;
        self.followButton.selected = author.is_follow && author.is_author;
    }
}

- (IBAction)followAuthor:(UIButton *)sender {
    USER *author = self.data;
    if (self.delegate && [self.delegate respondsToSelector:@selector(followAuthor:sender:)]) {
        [self.delegate followAuthor:author sender:sender];
    }
}

@end
