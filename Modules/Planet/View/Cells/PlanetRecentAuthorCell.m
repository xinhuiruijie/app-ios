//
//  PlanetRecentAuthorCell.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "PlanetRecentAuthorCell.h"

@interface PlanetRecentAuthorCell ()

@property (weak, nonatomic) IBOutlet UIView *recentAuthorView;
@property (weak, nonatomic) IBOutlet UIImageView *animatedIcon;
@property (weak, nonatomic) IBOutlet UIImageView *arrowView;

@end

@implementation PlanetRecentAuthorCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    NSMutableArray *animateIcons = [NSMutableArray array];
    for (int i = 1; i < 76; i++) {
        NSString *imageName = [NSString stringWithFormat:@"icon_join_%d", i];
        UIImage *animateImage = [UIImage imageNamed:imageName];
        [animateIcons addObject:animateImage];
    }
    self.animatedIcon.animationImages = animateIcons;
    self.animatedIcon.animationRepeatCount = 0;
    self.animatedIcon.animationDuration = 3.0;
    
    self.showAuthorAnimation = YES;
}

- (void)dataDidChange {
    [self.animatedIcon stopAnimating];
    [self.animatedIcon startAnimating];
    NSArray *authors = self.data;
    
    [self.recentAuthorView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj removeFromSuperview];
    }];
    
    NSInteger authorCount = MIN(authors.count, 3);
    // 反序
    CGFloat originX = 138 - 30;
    for (NSInteger i = (authorCount - 1); i >= 0; i--) {
        USER *author = [authors objectAtIndex:i];
        
        UIView *authorView = [[UIView alloc] initWithFrame:CGRectMake(originX, 0, 36, 36)];
        [self.recentAuthorView addSubview:authorView];
        
        UIImageView *authorAvatar = [[UIImageView alloc] init];
        [authorAvatar setImageWithPhoto:author.avatar placeholderImage:[AppTheme avatarDefaultImage]];
        authorAvatar.frame = CGRectMake(0, 0, 36, 36);
        authorAvatar.contentMode = UIViewContentModeScaleAspectFill;
        authorAvatar.clipsToBounds = YES;
        authorAvatar.layer.cornerRadius = 18;
        authorAvatar.layer.masksToBounds = YES;
        [authorView addSubview:authorAvatar];
        
        UIImageView *authorImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_badge_author"]];
        authorImage.frame = CGRectMake(24, 24, 12, 12);
        authorImage.layer.cornerRadius = 6;
        authorImage.layer.masksToBounds = YES;
        [authorView addSubview:authorImage];
        
        originX -= (36 + 15);
    }
    
    // 需求：首次进入和刷新的时候出现
    if (authorCount > 0 && self.showAuthorAnimation && self.arrowView.alpha == 1.0) {
        [self showAnimation];
    }
}

- (void)showAnimation {
    if (self.recentAuthorView.subviews.count == 0) {
        return;
    }
    
    self.arrowView.alpha = 0.0;
    
    for (int i = 0; i < self.recentAuthorView.subviews.count; i++) {
        UIView *view = self.recentAuthorView.subviews[i];
        
        view.transform = CGAffineTransformMakeTranslation(kScreenWidth, 0);
        // 动画显示每一个头像
        [UIView animateWithDuration:0.5 delay:(self.recentAuthorView.subviews.count - i) * 0.1 usingSpringWithDamping:0.7 initialSpringVelocity:0.3 options:UIViewAnimationOptionCurveEaseIn animations:^{
            view.transform = CGAffineTransformIdentity;
        } completion:nil];
    }
    
    // 动画显示箭头
    [UIView animateWithDuration:0.2 delay:self.recentAuthorView.subviews.count * 0.1 + 0.25 options:UIViewAnimationOptionCurveEaseIn animations:^{
        self.arrowView.alpha = 1.0;
    } completion:^(BOOL finished) {
        self.showAuthorAnimation = NO;
    }];
}

@end
