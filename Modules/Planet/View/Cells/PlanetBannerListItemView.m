//
//  PlanetBannerListItemView.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/10.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "PlanetBannerListItemView.h"

@interface PlanetBannerListItemView ()

@property (weak, nonatomic) IBOutlet UILabel *bannerLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *subtitleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *bannerPhoto;

@end

@implementation PlanetBannerListItemView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.bannerLabel.layer.cornerRadius = 2;
    self.bannerLabel.clipsToBounds = YES;
    self.bannerPhoto.layer.cornerRadius = 2;
    self.bannerPhoto.clipsToBounds = YES;
}

- (void)dataDidChange {
    if (![self.data isKindOfClass:[BANNER class]]) return;
    
    BANNER *banner = self.data;
    
    self.titleLabel.text = banner.title?:@"";
    self.subtitleLabel.text = banner.subtitle?:@"";
    [self.bannerPhoto setImageWithPhoto:banner.photo placeholderImage:[AppTheme placeholderImage]];
}

@end
