//
//  PlanetViewController.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "PlanetViewController.h"
#import "VideoInfoController.h"
#import "AuthorInfoController.h"
#import "RecentAuthorListController.h"
#import "SearchViewController.h"
#import "SearchTabController.h"

#import "BannerListCell.h"
#import "VideoListCell.h"
#import "PlanetRecentAuthorCell.h"
#import "NoAttentionCell.h"
#import "PlanetVideoHeader.h"
#import "FakeSearchTitleView.h"
#import "AudioMiniView.h"
#import "ZLBlurView.h"

#import "BannerListModel.h"
#import "AuthorRecentListModel.h"
#import "PlanetVideoListModel.h"
#import "AuthorModel.h"
#import "AudioListModel.h"

@interface PlanetViewController () <UITableViewDataSource, UITableViewDelegate, VideoListCellDelegate>
@property (nonatomic, strong) UIView *topBarView;
@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) BannerListModel *bannerModel;
@property (nonatomic, strong) AuthorRecentListModel *recentModel;
@property (nonatomic, strong) PlanetVideoListModel *videoListModel;
@property (nonatomic, strong) AudioListModel *audioModel;

@property (nonatomic, strong) NSMutableArray *cellIdentifies;

@property (nonatomic, strong) PlayingLineAnimatedView *playingLineAnimated;
@property (nonatomic, strong) AudioMiniView *audioMiniView;
@property (nonatomic, strong) PlanetRecentAuthorCell *planetRecentAuthorCell;

//@property (nonatomic, strong) FakeSearchTitleView *fakerTitleView;
@property (nonatomic, strong) UIButton *searchItem;
//@property (nonatomic, assign) BOOL isFirstLoad;
@end

@implementation PlanetViewController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Planet"];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self modelInit];
    
    self.cellIdentifies = [NSMutableArray array];

    [self setupTopView];
    
    self.tableView.backgroundColor = [AppTheme listBackgroundColor];
    self.tableView.frame = CGRectMake(0, [UIApplication sharedApplication].statusBarFrame.size.height, kScreenWidth, kScreenHeight);
    CGFloat contentInsets_bottom = [SDiOSVersion deviceSize] == Screen5Dot8inch ? 40 : 20;
    self.tableView.contentInset = UIEdgeInsetsMake(self.topBarView.bottom - StatusBarHeight, 0, contentInsets_bottom, 0);
    self.tableView.estimatedRowHeight = 0;
    
    [self.tableView registerNib:[BannerListCell nib] forCellReuseIdentifier:@"BannerListCell"];
    [self.tableView registerNib:[PlanetRecentAuthorCell nib] forCellReuseIdentifier:@"PlanetRecentAuthorCell"];
    [self.tableView registerNib:[VideoListCell nib] forCellReuseIdentifier:@"VideoListCell"];
    [self.tableView registerNib:[NoAttentionCell nib] forCellReuseIdentifier:@"NoAttentionCell"];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadVideosAuthor:) name:kAuthorFollowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadVideosAuthor:) name:kAuthorUnfollowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadVideos:) name:kVideoInfoRefreshNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onTabBarDoubleClick:) name:UITabBarItemDoubleClickNotification object:nil];
    // 监听电台播放状态改变通知
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(audioPlayerStatusChangedNotification:) name:AudioPlayManagerStatusChangedNotification object:nil];
    
    [self.tableView.header beginRefreshing];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
    
    if (self.audioMiniView) {
        [self.audioMiniView checkPlayStatus];
        [self.audioMiniView resetDelegate];
    }
    if ([AudioPlayManager sharedInstance].status == AudioPlayerStatusRestartToPlay) {
        [self.playingLineAnimated addAnimation];
    }
    // 将要显示的时候开始banner轮播
    if (self.cellIdentifies.count > 0 && [self.cellIdentifies containsObject:@"BannerListCell"]) {
        BannerListCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:[self.cellIdentifies indexOfObject:@"BannerListCell"]]];
        if ([cell isKindOfClass:[BannerListCell class]]) {
            [cell start];
        }
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    // 将要消失的时候停止banner轮播
    if (self.cellIdentifies.count > 0 && [self.cellIdentifies containsObject:@"BannerListCell"]) {
        BannerListCell *cell = [self.tableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:[self.cellIdentifies indexOfObject:@"BannerListCell"]]];
        if ([cell isKindOfClass:[BannerListCell class]]) {
            [cell stop];
        }
    }
}

- (void)viewDidDisappear:(BOOL)animated {
    [super viewDidDisappear:animated];
}

- (void)onTabBarDoubleClick:(NSNotification *)notification {
    // tabbar双击时刷新
    if ([notification.object isKindOfClass:[MPRootViewController class]]) {
        MPRootViewController *rootController = (MPRootViewController *)notification.object;
        if ([self.navigationController isEqual:rootController.selectedViewController]) {
            [self.tableView.header beginRefreshing];
        }
    }
}

- (void)setupTopView {
    if (!self.topBarView) {
        self.topBarView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, TopBarHeight)];
        self.topBarView.backgroundColor = [UIColor clearColor];
        
        ZLBlurView *blurView = [[ZLBlurView alloc] initWithFrame:self.topBarView.bounds];
        [self.topBarView addSubview:blurView];
        
        // left item
        UIView *leftView = [self dateAnimatedViewItem].customView;
        leftView.frame = CGRectMake(15, StatusBarHeight + (NavigationBarHeight - 20) / 2.0, 60, 20);
        [self.topBarView addSubview:leftView];
        
        // right item
//        UIView *rightView = [self fmRightItem].customView;
        UIView *rightView = [self searchNormalItem].customView;
        rightView.frame = CGRectMake(kScreenWidth - 10 - 44, StatusBarHeight, 44, NavigationBarHeight);
        [self.topBarView addSubview:rightView];
        
        // 爱音斯坦 播放状态动画
//        self.playingLineAnimated = [[PlayingLineAnimatedView alloc] initWithFrame:CGRectMake(0, 0, 24, 24) lineWidth:2 lineColor:[AppTheme normalTextColor]];
//        self.playingLineAnimated.center = CGPointMake(rightView.width / 2.0, rightView.height / 2.0);
//        [rightView addSubview:self.playingLineAnimated];
//        self.playingLineAnimated.userInteractionEnabled = NO;
        
        [self.view addSubview:self.topBarView];
    }
}

- (void)modelInit {
    @weakify(self)
    self.bannerModel = [[BannerListModel alloc] init];
    self.bannerModel.banner_type = BANNER_TYPE_PLANET;
    self.bannerModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        [self.tableView.header endRefreshing];
        [self reloadList];
        
        if (error) {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
    };
    [self.bannerModel loadCache];
    
    self.recentModel = [[AuthorRecentListModel alloc] init];
    self.recentModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        [self.tableView.header endRefreshing];
        [self reloadList];
        self.planetRecentAuthorCell.showAuthorAnimation = YES;
        
        if (error) {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
    };
    [self.recentModel loadCache];
    
    self.videoListModel = [[PlanetVideoListModel alloc] init];
    self.videoListModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        [self.tableView.header endRefreshing];
        if (error == nil) {
            if (!self.videoListModel.isEmpty) {
                [self setupRefreshFooterView];
            }
            
            if (self.videoListModel.more) {
                [self.tableView.footer endRefreshing];
            } else {
                [self.tableView.footer noticeNoMoreData];
            }
            [self reloadList];
        } else {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
    };
    [self.videoListModel loadCache];
    
    [self reloadList];
    [self setupRefreshHeaderView];
    
    // 电台
    self.audioModel = [[AudioListModel alloc] init];
//    self.audioModel.whenUpdated = ^(STIHTTPResponseError *error) {
//        @strongify(self)
//    };
    [self.audioModel refresh];
}

- (void)reloadList {
    [self.cellIdentifies removeAllObjects];
    
    if (!self.bannerModel.isEmpty) {
        [self.cellIdentifies addObject:@"BannerListCell"];
    }
    if (!self.recentModel.isEmpty) {
        [self.cellIdentifies addObject:@"PlanetRecentAuthorCell"];
    }
    if (!self.videoListModel.isEmpty) {
        [self.cellIdentifies addObject:@"VideoListCell"];
    } else {
        [self.cellIdentifies addObject:@"NoAttentionCell"];
    }
    
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.cellIdentifies.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *identifier = self.cellIdentifies[section];
    if ([identifier isEqualToString:@"VideoListCell"]) {
        return self.videoListModel.items.count;
    }
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = self.cellIdentifies[indexPath.section];
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier forIndexPath:indexPath];
    
    if ([identifier isEqualToString:@"BannerListCell"]) {
        cell.data = self.bannerModel.items;
        ((BannerListCell *)cell).bannerType = BANNER_TYPE_PLANET;
    } else if ([identifier isEqualToString:@"PlanetRecentAuthorCell"]) {
        cell.data = self.recentModel.items;
        self.planetRecentAuthorCell = (PlanetRecentAuthorCell *)cell;
    } else if ([identifier isEqualToString:@"VideoListCell"]) {
        VIDEO *video = self.videoListModel.items[indexPath.row];
        cell.data = video;
        ((VideoListCell *)cell).delegate = self;
    } else if ([identifier isEqualToString:@"NoAttentionCell"]) {
        
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = self.cellIdentifies[indexPath.section];
    
    if ([identifier isEqualToString:@"BannerListCell"]) {
//        return ceil(tableView.width * 430 / 750);
        return ceil(tableView.width * 388 / 750) + 88;
    } else if ([identifier isEqualToString:@"PlanetRecentAuthorCell"]) {
        return ceil(tableView.width * 144 / 750);
    } else if ([identifier isEqualToString:@"VideoListCell"]) {
        VIDEO *video = self.videoListModel.items[indexPath.row];
        return [VideoListCell heightForVideoListCell:video];
    } else if ([identifier isEqualToString:@"NoAttentionCell"]) {
        return ceil(tableView.width * 240 / 750) + 98;
    }
    
    return CGFLOAT_MIN;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = self.cellIdentifies[indexPath.section];
    
    if ([identifier isEqualToString:@"PlanetRecentAuthorCell"] || [identifier isEqualToString:@"NoAttentionCell"]) {
        [AppAnalytics clickEvent:@"click_planet_latestJoin"];
        RecentAuthorListController *authorList = [RecentAuthorListController spawn];
        authorList.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:authorList animated:YES];
    } else if ([identifier isEqualToString:@"VideoListCell"]) {
        [AppAnalytics clickEvent:@"click_planet_video"];
        VIDEO *video = self.videoListModel.items[indexPath.row];
        VideoInfoController *videoInfo = [VideoInfoController spawn];
        videoInfo.video = video;
        
        videoInfo.hidesBottomBarWhenPushed = YES;
        videoInfo.reloadVideo = ^(VIDEO *video) {
            for (int i = 0; i < self.videoListModel.items.count; i++) {
                VIDEO *perVideo = self.videoListModel.items[i];
                if ([perVideo.id isEqualToString:video.id]) {
                    [self.videoListModel.items replaceObjectAtIndex:i withObject:video];
                    break;
                }
            }
            [self.tableView reloadData];
        };
//        [self presentNavigationController:videoInfo];
        videoInfo.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:videoInfo animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    NSString *identifier = self.cellIdentifies[section];
    
    if ([identifier isEqualToString:@"VideoListCell"]) {
        return 58;
    }
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *identifier = self.cellIdentifies[section];
    
    if ([identifier isEqualToString:@"VideoListCell"]) {
        PlanetVideoHeader *header = [PlanetVideoHeader loadFromNib];
        return header;
    }
    return [[UIView alloc] init];
}

#pragma mark - VideoListCellDelegate

- (void)gotoAuthorInfo:(USER *)author {
    AuthorInfoController *authorInfo = [AuthorInfoController spawn];
    authorInfo.author = author;
    authorInfo.followAuthor = ^(USER *author) {
        [self reloadAuthorInfo:author];
    };
    [self presentNavigationController:authorInfo];
}

- (void)followAuthor:(VIDEO *)video sender:(UIButton *)sender {
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }
    
    [sender followAuthorButtonAnimated];
    
    [self presentLoadingTips:nil];
    [sender disableSelf];
    if (video.owner.is_follow) {
        [AuthorModel unfollow:video.owner then:^(STIHTTPResponseError *error) {
            if (error == nil) {
                video.owner.is_follow = NO;
                [self reloadAuthorInfo:video.owner];
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
            [self dismissTips];
            [sender enableSelf];
        }];
    } else {
        if (!video.owner.is_author) {
            [self dismissTips];
            [[UIApplication sharedApplication].keyWindow presentMessageTips:@"作者不存在！"];
            return;
        }
        
        [AuthorModel follow:video.owner then:^(STIHTTPResponseError *error) {
            if (error == nil) {
                video.owner.is_follow = YES;
                [self reloadAuthorInfo:video.owner];
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
            [self dismissTips];
            [sender enableSelf];
        }];
    }
}

#pragma mark - Notifaction

// 监听电台播放状态改变通知
- (void)audioPlayerStatusChangedNotification:(NSNotification *)noti {
    if ([noti.object isEqual:[AudioPlayManager sharedInstance]]) {
        AudioPlayerStatus status = [AudioPlayManager sharedInstance].status;
        
        if (status == AudioPlayerStatusRestartToPlay) {
            [self.playingLineAnimated addAnimation];
        } else {
            [self.playingLineAnimated removeAnimation];
        }
    }
}

- (void)reloadVideos:(NSNotification *)notification {
    if ([[self topViewController] isKindOfClass:[self class]]) {
        return;
    }
    VIDEO *video = [notification.userInfo objectForKey:@"data"];
    for (int i = 0; i < self.videoListModel.items.count; i++) {
        VIDEO *videoItem = self.videoListModel.items[i];
        if (videoItem.id && [videoItem.id isEqualToString:video.id]) {
            [self.videoListModel.items replaceObjectAtIndex:i withObject:video];
            break;  // 一个列表不会出现相同的短片数据
        }
    }
    [self.tableView reloadData];
}

- (void)reloadVideosAuthor:(NSNotification *)notification {
    if ([[self topViewController] isKindOfClass:[self class]]) {
        return;
    }
    USER *author = [notification.userInfo objectForKey:@"data"];
    [self reloadAuthorInfo:author];
}

- (void)reloadAuthorInfo:(USER *)replaceAuthor {
    for (int i = 0; i < self.videoListModel.items.count; i++) {
        VIDEO *videoItem = self.videoListModel.items[i];
        if (videoItem.owner && [videoItem.owner.id isEqualToString:replaceAuthor.id]) {
            videoItem.owner = replaceAuthor;     // 一个列表不会出现相同的短片数据，但会出现同一作者的多个短片
        }
    }
    [self.tableView reloadData];
}

#pragma mark - UIBarButtonItems

- (UIBarButtonItem *)dateAnimatedViewItem {
    UILabel *titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 60, 22)];
    titleLabel.text = @"星球";
    titleLabel.font = [UIFont systemFontOfSize:24 weight:UIFontWeightMedium];
    titleLabel.textColor = [AppTheme normalTextColor];
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithCustomView:titleLabel];
    return leftItem;
}

- (UIBarButtonItem *)fmRightItem {
//    UIButton *fmButton = [AppTheme navigationItemCustomView:[UIImage imageNamed:@"icon_fm_nor"]];
    UIButton *fmButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [fmButton addTarget:self action:@selector(fmButtonAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *fmItem = [[UIBarButtonItem alloc] initWithCustomView:fmButton];
    return fmItem;
}

- (UIBarButtonItem *)searchNormalItem {
    UIButton *searchButton = [AppTheme navigationItemCustomView:[UIImage imageNamed:@"a1_icon_search"]];
    self.searchItem = searchButton;
    [searchButton addTarget:self action:@selector(searchAction) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *searchItem = [[UIBarButtonItem alloc] initWithCustomView:searchButton];
    return searchItem;
}

//- (FakeSearchTitleView *)fakerTitleView {
//    if (!_fakerTitleView) {
//        _fakerTitleView = [[FakeSearchTitleView alloc] initWithFrame:CGRectMake(0, 0, self.view.width, 24)];
//        _fakerTitleView.searchType = FakeSearchPlanet;
//    }
//    return _fakerTitleView;
//}

- (void)searchAction {
    SearchTabController *searchView = [SearchTabController spawn];
    searchView.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:searchView animated:NO];
}

- (void)fmButtonAction {
    if (!self.audioMiniView) {
        self.audioMiniView = [AudioMiniView loadFromNibWithFrame:CGRectMake(0, 0, kScreenWidth, 60)];
        self.audioMiniView.hidden = YES;
        [self.view insertSubview:self.audioMiniView belowSubview:self.topBarView];
        @weakify(self)
        self.audioMiniView.onBack = ^{
            @strongify(self)
            [self fmButtonAction];
        };
    }
    
    if (self.audioMiniView.hidden) {
        [self showAudioMiniView];
        self.audioMiniView.datas = self.audioModel.items.copy;
    } else {
        [self hideAudioMiniView];
    }
    
    CGFloat offsetY = self.tableView.contentOffset.y - (self.audioMiniView.y == 0 ? -60 : 60);
    CGFloat insetTop = self.topBarView.bottom + (self.audioMiniView.y == 0 ? 0 : 60) - StatusBarHeight;
    [UIView animateWithDuration:0.25 animations:^{
        self.tableView.contentOffset = CGPointMake(0, offsetY);
    } completion:^(BOOL finished) {
        self.tableView.contentInsets_top = insetTop;
    }];
}

- (void)showAudioMiniView {
    self.audioMiniView.hidden = NO;
    
    [UIView animateWithDuration:0.25 delay:0.0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.audioMiniView.y = self.topBarView.bottom;
    } completion:nil];
}

- (void)hideAudioMiniView {
    [UIView animateWithDuration:0.25 delay:0.03 options:UIViewAnimationOptionCurveEaseInOut animations:^{
        self.audioMiniView.y = 0;
    } completion:^(BOOL finished) {
        self.audioMiniView.hidden = YES;
    }];
}

#pragma mark - Header

- (void)setupRefreshHeaderView {
    @weakify(self)
    [self.tableView addHeaderPullLoader:^{
        @strongify(self)
        [self.bannerModel refresh];
        [self.recentModel refresh];
        [self.videoListModel refresh];
    }];
}

- (void)setupRefreshFooterView {
    if (self.tableView.footer == nil) {
        if (!self.videoListModel.isEmpty) {
            @weakify(self)
            [self.tableView addFooterPullLoader:^{
                @strongify(self)
                [self.videoListModel loadMore];
            }];
        }
    } else {
        if (self.videoListModel.isEmpty) {
            [self.tableView removeFooter];
        }
    }
}

@end
