//
//  SettingModel.h
//  leciyuan
//
//  Created by Chenyun on 16/1/20.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import "STIOnceModel.h"

@interface SettingModel : STIOnceModel

@singleton(SettingModel)

@property (nonatomic, assign) BOOL autoPlay;
@property (nonatomic, assign) VIDEO_QUALITY defaultQuality;

@end
