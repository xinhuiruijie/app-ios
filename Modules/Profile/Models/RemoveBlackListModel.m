//
//  RemoveBlackListModel.m
//  MotherPlanet
//
//  Created by 陈熙 on 2018/3/1.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "RemoveBlackListModel.h"

@implementation RemoveBlackListModel

+ (void)removeUserFromBlackListWithUserId:(NSString *)user_id
                               completion:(void (^)(STIHTTPResponseError *))completion {
    V1_API_USER_BLACKLIST_DELETE_API *api = [[V1_API_USER_BLACKLIST_DELETE_API alloc] init];
    api.req.user_id = user_id;
    api.whenUpdated = ^(V1_API_USER_BLACKLIST_DELETE_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if (error) {
            PERFORM_BLOCK_SAFELY(completion, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                PERFORM_BLOCK_SAFELY(completion, nil);
            } else {
                STIHTTPResponseError *errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(completion, errors);
            }
        }
    };
    [api send];
}

@end
