//
//  MyCouponListModel.h
//  ParkviewGreen
//
//  Created by GeekZooStudio on 17/2/16.
//  Copyright © 2017年 GeekZooStudio. All rights reserved.
//

#import "STIStreamModel.h"

@interface MyCouponListModel : STIStreamModel
@property (nonatomic, assign) COUPON_TYPE couponFilter;
@end
