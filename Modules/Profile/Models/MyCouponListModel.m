//
//  MyCouponListModel.m
//  ParkviewGreen
//
//  Created by GeekZooStudio on 17/2/16.
//  Copyright © 2017年 GeekZooStudio. All rights reserved.
//

#import "MyCouponListModel.h"

@implementation MyCouponListModel

- (void)refresh {
    [self fetchForFirstTime:YES];
}

- (void)loadMore {
    [self fetchForFirstTime:NO];
}

- (void)fetchForFirstTime:(BOOL)isFirstTime {
    if ( isFirstTime ) {
        self.currentPage = 1;
    } else {
        self.currentPage += 1;
    }
    
    V1_API_COUPON_LIST_API * api = [[V1_API_COUPON_LIST_API alloc] init];
    api.manuallyCancel = YES;
    api.req.page = @(self.currentPage);
    api.req.per_page = @(10);
    api.req.type = self.couponFilter;
    
    api.whenUpdated = ^(V1_API_COUPON_LIST_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        self.loaded = YES;
        if ( error ) {
            PERFORM_BLOCK_SAFELY(self.whenUpdated, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                
                if (isFirstTime) {
                    [self.items removeAllObjects];
                }
                [self.items addObjectsFromArray:response.coupons];
                self.more = response.paged.more.boolValue;
                PERFORM_BLOCK_SAFELY(self.whenUpdated, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(self.whenUpdated, errors);
            }
        }
    };
    
    [api send];
}

@end
