//
//  OfflineWatchListModel.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/22.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "OfflineWatchListModel.h"

@implementation OfflineWatchListModel

- (id)init {
    if (self = [super init]) {
        self.offlineWatchList = [NSMutableArray array];
        self.recentWatchArray = [NSMutableArray array];
        self.earlyWatchArray = [NSMutableArray array];
    }
    return self;
}

#pragma mark - Offline Watch Video

- (void)loadOfflineListCache {
    NSArray *items = [self loadCacheWithKey:[self watchVideoListCacheKey] objectClass:[VIDEO_HISTORY class]];
    
    [self.offlineWatchList removeAllObjects];
    if ( items.count ) {
        [self.offlineWatchList addObjectsFromArray:items];
    }
    [self separateWatchList];
}

- (void)saveWatchVideoListCache {
    [self saveCache:self.offlineWatchList key:[self watchVideoListCacheKey]];
}

- (NSString *)watchVideoListCacheKey {
    return [NSString stringWithFormat:@"%@-watchVideoListCacheKey",[[self class] description]];
}

#pragma mark - History Keywords

- (void)addWatchVideo:(VIDEO_HISTORY *)video {
    if (video == nil)
        return;
    
    if (self.offlineWatchList) {
        for (VIDEO_HISTORY *history in self.offlineWatchList) {
            if ([history.video_id isEqualToString:video.video_id]) {
                // 历史记录中已存在
                [self.offlineWatchList removeObject:history];
                break;
            }
        }
        [self.offlineWatchList insertObject:video atIndex:0];
        if (self.offlineWatchList.count > 20) {
            [self.offlineWatchList subarrayWithRange:NSMakeRange(0, 20)];
        }
    }
    
    [self saveWatchVideoListCache];
}

- (void)removeAll {
    if (self.offlineWatchList) {
        [self.offlineWatchList removeAllObjects];
    }
    self.offlineWatchList = nil;

    [self saveWatchVideoListCache];
    [self clearCacheWithKey:[self watchVideoListCacheKey]];
}

- (void)separateWatchList {
    if (self.offlineWatchList == nil || self.offlineWatchList.count == 0) {
        return;
    }
    NSMutableArray *recentWatchArray = [NSMutableArray array];
    NSMutableArray *earlyWatchArray = [NSMutableArray array];
    
    NSDate *startTime = [NSDate dateWithTimeIntervalSinceNow:-60 * 60 * 24 * 3];
    NSDate *endTime = [NSDate date];
    
    for (VIDEO_HISTORY *videoHistory in self.offlineWatchList) {
        NSDate *watchAt = [NSDate dateWithTimeIntervalSince1970:videoHistory.watch_at.integerValue];
        if ([watchAt compare:startTime] == NSOrderedDescending && [watchAt compare:endTime] == NSOrderedAscending) {
            [recentWatchArray addObject:videoHistory];
        } else {
            [earlyWatchArray addObject:videoHistory];
        }
    }
    
    self.recentWatchArray = recentWatchArray;
    self.earlyWatchArray = earlyWatchArray;
}

@end
