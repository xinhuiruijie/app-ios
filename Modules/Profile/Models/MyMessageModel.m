//
//  MyMessageModel.m
//  MotherPlanet
//
//  Created by 陈熙 on 2018/1/2.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "MyMessageModel.h"

@implementation MyMessageModel

- (void)loadCache {
    NSArray *items = [self loadCacheWithKey:self.cacheKey objectClass:[MESSAGE class]];
    
    [self.items removeAllObjects];
    if (items.count) {
        [self.items addObjectsFromArray:items];
    }
}

- (void)saveCache {
    [self saveCache:self.items key:self.cacheKey];
}

- (NSString *)cacheKey {
    return [NSString stringWithFormat:@"%@-%@", [[self class] description], [UserModel sharedInstance].user.id];
}

- (void)refresh {
    [self fetchForFirstTime:YES];
}

- (void)loadMore {
    [self fetchForFirstTime:NO];
}

- (void)fetchForFirstTime:(BOOL)isFirstTime {
    if ( isFirstTime ) {
        self.currentPage = 1;
    } else {
        self.currentPage += 1;
    }
    
    V1_API_VIDEO_MESSAGE_LIST_API * api = [[V1_API_VIDEO_MESSAGE_LIST_API alloc] init];
    api.req.page = @(self.currentPage);
    api.req.per_page = @(10);
    
    api.whenUpdated = ^(V1_API_VIDEO_MESSAGE_LIST_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        self.loaded = YES;
        if ( error ) {
            PERFORM_BLOCK_SAFELY(self.whenUpdated, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                if (isFirstTime) {
                    [self.items removeAllObjects];
                }
                [self.items addObjectsFromArray:response.messages];
                [self saveCache];
                self.more = response.paged.more.boolValue;
                PERFORM_BLOCK_SAFELY(self.whenUpdated, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(self.whenUpdated, errors);
            }
        }
    };
    
    [api send];
}

@end
