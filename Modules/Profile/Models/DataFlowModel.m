//
//  DataFlowModel.m
//  MotherPlanet
//
//  Created by liuyadi on 2018/1/11.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "DataFlowModel.h"

@implementation DataFlowModel

- (void)refresh {
    V1_API_DATA_FLOW_QUERY_API *api = [[V1_API_DATA_FLOW_QUERY_API alloc] init];
    
    api.whenUpdated = ^(V1_API_DATA_FLOW_QUERY_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        self.loaded = YES;
        if ( error ) {
            PERFORM_BLOCK_SAFELY(self.whenUpdated, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
//                DATA_FLOW *dataFlow = [[DATA_FLOW alloc] init];
//                dataFlow.has_dataflow = response.has_dataflow;
//                dataFlow.total_flow = response.total_flow;
//                dataFlow.used_flow = response.used_flow;
//                dataFlow.surplus_flow = response.surplus_flow;
//                
//                self.dataFlow = dataFlow;
                PERFORM_BLOCK_SAFELY(self.whenUpdated, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(self.whenUpdated, errors);
            }
        }
    };
    
    [api send];
}

@end
