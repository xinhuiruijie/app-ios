//
//  SettingModel.m
//  leciyuan
//
//  Created by Chenyun on 16/1/20.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import "SettingModel.h"

@implementation SettingModel

@def_singleton(SettingModel)

- (id)init {
    if (self = [super init]) {
        [self loadCache];
    }
    return self;
}

- (void)loadCache {
    NSNumber *style = [self loadCacheWithKey:[self autoPlayCacheKey] objectClass:[NSNumber class]];
    NSNumber *quality = [self loadCacheWithKey:[self defaultQualityCacheKey] objectClass:[NSNumber class]];
    self.autoPlay = [style boolValue];
    self.defaultQuality = [quality integerValue];
}

- (NSString *)autoPlayCacheKey {
    return [NSString stringWithFormat:@"SettingModel-autoPlayCacheKey"];
}

- (NSString *)defaultQualityCacheKey {
    return [NSString stringWithFormat:@"SettingModel-defaultQualityCacheKey"];
}

- (void)setAutoPlay:(BOOL)autoPlay {
    _autoPlay = autoPlay;
    [self saveCache:@(autoPlay) key:[self autoPlayCacheKey]];
}

- (void)setDefaultQuality:(VIDEO_QUALITY)defaultQuality {
    _defaultQuality = defaultQuality;
    [self saveCache:@(defaultQuality) key:[self defaultQualityCacheKey]];
}

@end
