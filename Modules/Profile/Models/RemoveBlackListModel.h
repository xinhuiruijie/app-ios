//
//  RemoveBlackListModel.h
//  MotherPlanet
//
//  Created by 陈熙 on 2018/3/1.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "STIWriteModel.h"

@interface RemoveBlackListModel : STIWriteModel

+ (void)removeUserFromBlackListWithUserId:(NSString *)user_id
                               completion:(void (^)(STIHTTPResponseError *e))completion;

@end
