//
//  MyTimelineModel.h
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/26.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "STIStreamModel.h"

@interface MyTimelineModel : STIStreamModel

@property (nonatomic, copy) NSString *userID;

@end
