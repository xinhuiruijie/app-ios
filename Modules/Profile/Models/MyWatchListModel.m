//
//  MyWatchListModel.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/22.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "MyWatchListModel.h"

@implementation MyWatchListModel

- (instancetype)init {
    self = [super init];
    if (self) {
        self.recentWatchArray = [NSMutableArray array];
        self.earlyWatchArray = [NSMutableArray array];
    }
    return self;
}

- (void)loadCache {
    NSArray *items = [self loadCacheWithKey:self.cacheKey objectClass:[VIDEO class]];
    
    [self.items removeAllObjects];
    if (items.count) {
        [self.items addObjectsFromArray:items];
    }
    [self separateWatchList];
}

- (void)saveCache {
    [self saveCache:self.items key:self.cacheKey];
}

- (NSString *)cacheKey {
    return [NSString stringWithFormat:@"%@-%@", [[self class] description], [UserModel sharedInstance].user.id];
}

- (void)refresh {
    [self fetchForFirstTime:YES];
}

- (void)loadMore {
    [self fetchForFirstTime:NO];
}

- (void)fetchForFirstTime:(BOOL)isFirstTime {
    if ( isFirstTime ) {
        self.currentPage = 1;
    } else {
        self.currentPage += 1;
    }
    
    V1_API_VIDEO_WATCH_LIST_API * api = [[V1_API_VIDEO_WATCH_LIST_API alloc] init];
    api.req.page = @(self.currentPage);
    api.req.per_page = @(20);
    
    api.whenUpdated = ^(V1_API_VIDEO_WATCH_LIST_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        self.loaded = YES;
        if ( error ) {
            PERFORM_BLOCK_SAFELY(self.whenUpdated, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                if (isFirstTime) {
                    [self.items removeAllObjects];
                }
                [self.items addObjectsFromArray:response.videos];
                [self saveCache];
                [self separateWatchList];
                self.more = response.paged.more.boolValue;
                PERFORM_BLOCK_SAFELY(self.whenUpdated, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(self.whenUpdated, errors);
            }
        }
    };
    
    [api send];
}

- (void)separateWatchList {
    if (self.isEmpty) {
        return;
    }
    NSMutableArray *recentWatchArray = [NSMutableArray array];
    NSMutableArray *earlyWatchArray = [NSMutableArray array];
    
    NSDate *startTime = [NSDate dateWithTimeIntervalSinceNow:-60 * 60 * 24 * 3];
    NSDate *endTime = [NSDate date];
    
    for (VIDEO *video in self.items) {
        NSDate *watchAt = [NSDate dateWithTimeIntervalSince1970:video.watch_at.integerValue];
        if ([watchAt compare:startTime] == NSOrderedDescending && [watchAt compare:endTime] == NSOrderedAscending) {
            [recentWatchArray addObject:video];
        } else {
            [earlyWatchArray addObject:video];
        }
    }
    
    self.recentWatchArray = recentWatchArray;
    self.earlyWatchArray = earlyWatchArray;
}

@end
