//
//  MessageUnreadModel.m
//  MotherPlanet
//
//  Created by 陈熙 on 2018/1/22.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "MessageUnreadModel.h"

@implementation MessageUnreadModel

+ (void)SystemMessageUnreadWithCompletion:(void (^)(BOOL, STIHTTPResponseError *))completion {
    V1_API_SYSTEM_MESSAGE_UNREAD_API *api = [[V1_API_SYSTEM_MESSAGE_UNREAD_API alloc] init];
    api.whenUpdated = ^(V1_API_SYSTEM_MESSAGE_UNREAD_RESPONSE *resp, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if (error) {
            PERFORM_BLOCK_SAFELY(completion, NO, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                PERFORM_BLOCK_SAFELY(completion, resp.has_unread, nil);
            } else {
                STIHTTPResponseError *errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(completion, NO, errors);
            }
        }
    };
    [api send];
}

+ (void)TimelineMessageUnreadWithCompletion:(void (^)(BOOL, STIHTTPResponseError *))completion {
    V1_API_VIDEO_MESSAGE_UNREAD_API *api = [[V1_API_VIDEO_MESSAGE_UNREAD_API alloc] init];
    api.whenUpdated = ^(V1_API_VIDEO_MESSAGE_UNREAD_RESPONSE *resp, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if (error) {
            PERFORM_BLOCK_SAFELY(completion, NO, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                PERFORM_BLOCK_SAFELY(completion, resp.has_unread, nil);
            } else {
                STIHTTPResponseError *errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(completion, NO, errors);
            }
        }
    };
    [api send];
}

@end
