//
//  MessageUnreadModel.h
//  MotherPlanet
//
//  Created by 陈熙 on 2018/1/22.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "STIWriteModel.h"

@interface MessageUnreadModel : STIWriteModel

+ (void)SystemMessageUnreadWithCompletion:(void (^)(BOOL has_unread, STIHTTPResponseError *e))completion;

+ (void)TimelineMessageUnreadWithCompletion:(void (^)(BOOL has_unread, STIHTTPResponseError *e))completion;

@end
