//
//  MyWatchListModel.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/22.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "STIStreamModel.h"

@interface MyWatchListModel : STIStreamModel

@property (nonatomic, strong) NSMutableArray *recentWatchArray;
@property (nonatomic, strong) NSMutableArray *earlyWatchArray;

@end
