//
//  WatchVideoModel.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/22.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "STIWriteModel.h"

@interface WatchVideoModel : STIWriteModel

+ (void)deleteWatchList:(NSArray *)video_ids then:(void (^)(STIHTTPResponseError *))then;
+ (void)syncWatchList:(NSArray *)video_history then:(void (^)(STIHTTPResponseError *))then;

@end
