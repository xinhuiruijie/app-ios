//
//  WatchVideoModel.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/22.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "WatchVideoModel.h"

@implementation WatchVideoModel

+ (void)deleteWatchList:(NSArray *)video_ids then:(void (^)(STIHTTPResponseError *))then {
    V1_API_WATCH_LIST_DELETE_API *api = [[V1_API_WATCH_LIST_DELETE_API alloc] init];
    
    // 清空时不传参
    if (video_ids && video_ids.count) {
        api.req.video_ids = [video_ids JSONStringRepresentation];
    }
    
    api.whenUpdated = ^(V1_API_WATCH_LIST_DELETE_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if ( error ) {
            PERFORM_BLOCK_SAFELY(then, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                PERFORM_BLOCK_SAFELY(then, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(then, errors);
            }
        }
    };
    
    [api send];
}

+ (void)syncWatchList:(NSArray *)video_history then:(void (^)(STIHTTPResponseError *))then {
    V1_API_WATCH_LIST_SYNC_API *api = [[V1_API_WATCH_LIST_SYNC_API alloc] init];
    
    if (video_history && video_history.count) {
        api.req.video_history = [video_history JSONStringRepresentation];
    }
    
    api.whenUpdated = ^(V1_API_WATCH_LIST_SYNC_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        if ( error ) {
            PERFORM_BLOCK_SAFELY(then, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                PERFORM_BLOCK_SAFELY(then, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(then, errors);
            }
        }
    };
    
    [api send];
}

@end
