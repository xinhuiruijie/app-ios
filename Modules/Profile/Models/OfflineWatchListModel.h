//
//  OfflineWatchListModel.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/22.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "STIStreamModel.h"

@interface OfflineWatchListModel : STIStreamModel

@property (nonatomic, strong) NSMutableArray *offlineWatchList;
@property (nonatomic, strong) NSMutableArray *recentWatchArray;
@property (nonatomic, strong) NSMutableArray *earlyWatchArray;

#pragma mark - Offline Watch Video

- (void)loadOfflineListCache;
- (void)addWatchVideo:(VIDEO_HISTORY *)video;
- (void)removeAll;

@end
