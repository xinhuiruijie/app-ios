//
//  MyMessageController.h
//  MotherPlanet
//
//  Created by 陈熙 on 2018/1/2.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MyMessageControllerDelegate <NSObject>

- (void)noticeSuperRefreshTimeline;

@end

@interface MyMessageController : UIViewController

@property (nonatomic, weak) id <MyMessageControllerDelegate> delegate;

@end
