//
//  MessageRemindController.h
//  MotherPlanet
//
//  Created by 陈熙 on 2018/1/16.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "WMPageController.h"

@interface MessageRemindController : WMPageController

@property (nonatomic, strong) UIViewController *topController;

@end
