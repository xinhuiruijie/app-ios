//
//  AboutMotherPlanetController.m
//  MotherPlanet
//
//  Created by 陈熙 on 2018/1/1.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "AboutMotherPlanetController.h"
#import "PotocolUrlController.h"

@interface AboutMotherPlanetController ()

@end

@implementation AboutMotherPlanetController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Profile"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.backgroundColor = [UIColor whiteColor];
    [self setupNavigationBar];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - CustomMethod

- (void)setupNavigationBar {
    self.navigationItem.title = @"关于我们";
    @weakify(self)
    self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)pushPotocolControllerWithPotocolType:(POTOCOL_TYPE)potocolType {
    PotocolUrlController *potocolUrl = [PotocolUrlController spawn];
    potocolUrl.potocolType = potocolType;
    [self.navigationController pushViewController:potocolUrl animated:YES];
}

#pragma mark - UITableViewDeleagate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 1) {
        [self pushPotocolControllerWithPotocolType:indexPath.row];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        return 10;
    }
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

@end
