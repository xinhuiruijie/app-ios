//
//  CollectTopicListController.h
//  MotherPlanet
//
//  Created by 陈熙 on 2018/1/16.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *kchangeTopicNotification = @"kchangeTopicNotification";

@protocol CollectTopicListControllerDelegate <NSObject>

- (void)noticeSuperTopicState:(BOOL)state;

@end

@interface CollectTopicListController : UIViewController

@property (nonatomic, assign) BOOL isEditState;

@property (nonatomic, weak) id <CollectTopicListControllerDelegate> delegate;

- (void)reloadModel;

@end
