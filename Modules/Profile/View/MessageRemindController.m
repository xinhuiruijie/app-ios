//
//  MessageRemindController.m
//  MotherPlanet
//
//  Created by 陈熙 on 2018/1/16.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "MessageRemindController.h"
#import "MyMessageController.h"
#import "SystemMessageController.h"
#import "MessageUnreadModel.h"

@interface MessageRemindController () <MyMessageControllerDelegate, SystemMessageControllerDelegate>

@property (nonatomic, assign) BOOL hasUnreadSystem;
@property (nonatomic, assign) BOOL hasUnreadTimeline;

@end

@implementation MessageRemindController

- (instancetype)init {
    if (self = [super init]) {
        self.titleColorSelected = [UIColor colorWithRGBValue:0x343338];
        self.titleColorNormal = [UIColor colorWithRGBValue:0xA9AEB5];
        self.menuViewStyle = WMMenuViewStyleLine;
        self.titleSizeNormal = 14;
        self.titleSizeSelected = 14;
        self.progressWidth = 40;
        self.progressViewIsDamping = YES;
        self.pageAnimatable = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"消息通知";
    @weakify(self)
    self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [self.navigationController enabledMLBlackTransition:NO];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self messageBadgeHasUnread];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController enabledMLBlackTransition:YES];
}

- (void)messageBadgeHasUnread {
    @weakify(self)
    [MessageUnreadModel TimelineMessageUnreadWithCompletion:^(BOOL has_unread, STIHTTPResponseError *e) {
        @strongify(self)
        if (has_unread) {
            self.hasUnreadTimeline = YES;
        } else {
            self.hasUnreadTimeline = NO;
        }
        [self.menuView updateBadgeViewAtIndex:0];
    }];
    [MessageUnreadModel SystemMessageUnreadWithCompletion:^(BOOL has_unread, STIHTTPResponseError *e) {
        @strongify(self)
        if (has_unread) {
            self.hasUnreadSystem = YES;
        } else {
            self.hasUnreadSystem = NO;
        }
        [self.menuView updateBadgeViewAtIndex:1];
    }];
}

- (void)noticeSuperRefreshSystem {
    [self messageBadgeHasUnread];
}

- (void)noticeSuperRefreshTimeline {
    [self messageBadgeHasUnread];
}

- (NSArray<NSString *> *)titles {
    return @[@"我的提示", @"系统消息"];
}

- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    return self.titles.count;
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    if (index == 0) {
        //我的提示
        MyMessageController *myMessage = [MyMessageController spawn];
        myMessage.delegate = self;
        return myMessage;
    } else {
        //系统消息
        SystemMessageController *systemMessage = [SystemMessageController spawn];
        systemMessage.delegate = self;
        return systemMessage;
    }
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView {
    return CGRectMake(0, 0, self.view.frame.size.width , 44);
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView {
    CGFloat originY = CGRectGetMaxY([self pageController:pageController preferredFrameForMenuView:self.menuView]);
    return CGRectMake(0, originY, self.view.frame.size.width, self.view.frame.size.height - originY);
}

- (UIView *)menuView:(WMMenuView *)menu badgeViewAtIndex:(NSInteger)index {
    UIView *badge = [[UIView alloc] initWithFrame:CGRectMake(60, 10, 8, 8)];
    badge.layer.cornerRadius = 4;
    badge.layer.masksToBounds = YES;
    badge.backgroundColor = [UIColor redColor];
    if (index == 0) {
        if (self.hasUnreadTimeline) {
            return badge;
        } else {
            return nil;
        }
    } else {
        if (self.hasUnreadSystem) {
            return badge;
        } else {
            return nil;
        }
    }
}

- (void)pageController:(WMPageController *)pageController didEnterViewController:(__kindof UIViewController *)viewController withInfo:(NSDictionary *)info {
    [self messageBadgeHasUnread];
}

@end
