//
//  MyFollowListController.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/22.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "MyFollowListController.h"
#import "AuthorInfoController.h"
#import "MyFollowListModel.h"
#import "AuthorModel.h"
#import "AuthorListCell.h"

@interface MyFollowListController () <UITableViewDelegate, UITableViewDataSource, AuthorListCellDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) MyFollowListModel *model;

@end

@implementation MyFollowListController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Profile"];
}

- (UIScrollView *)list {
    return self.tableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"我的关注";
    @weakify(self)
    self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    self.tableView.backgroundColor = [AppTheme listBackgroundColor];
    [self.tableView registerNib:[AuthorListCell nib] forCellReuseIdentifier:@"AuthorListCell"];
    [self.tableView registerNib:[EmptyTableCell nib] forCellReuseIdentifier:@"EmptyTableCell"];

    self.model = [[MyFollowListModel alloc] init];
    self.model.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        [self.tableView.header endRefreshing];
        [self.tableView reloadData];
        if (error == nil) {
            [self setupRefreshFooterView];
            if (self.model.more) {
                [self.tableView.footer endRefreshing];
            } else {
                [self.tableView.footer noticeNoMoreData];
            }
        } else {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
    };
    
    [self.list.header beginRefreshing];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.model.loaded && self.model.isEmpty) {
        return 1;
    }
    return self.model.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.model.loaded && self.model.isEmpty) {
        EmptyTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EmptyTableCell" forIndexPath:indexPath];
        return cell;
    }
    AuthorListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AuthorListCell" forIndexPath:indexPath];
    cell.data = self.model.items[indexPath.row];
    cell.delegate = self;
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.model.loaded && self.model.isEmpty) {
        return tableView.height;
    }
    return 66;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.model.loaded && self.model.isEmpty) {
        return;
    }
    AuthorInfoController *authorInfo = [AuthorInfoController spawn];
    USER *author = self.model.items[indexPath.row];
    authorInfo.author = author;
    authorInfo.followAuthor = ^(USER *author) {
        for (int i = 0; i < self.model.items.count; i++) {
            USER *authorItem = self.model.items[i];
            if ([author.id isEqualToString:authorItem.id]) {
                if (!author.is_follow) {
                    [self.model.items removeObject:authorItem];
                }
                break;
            }
        }
        [self.tableView reloadData];
    };
    [self presentNavigationController:authorInfo];
}

#pragma mark - AuthorListCell

- (void)followAuthor:(USER *)author sender:(UIButton *)sender {
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }
    
    [sender followAuthorButtonAnimated];
    
    if (author.is_follow) {
        MPAlertView *alertView = [MPAlertView alertWithTitle:@"取消关注" message:@"确定取消关注?" cancelButton:@"取消" confirmButton:@"确定"];
        alertView.confirmAction = ^{
            @weakify(self)
            [AuthorModel unfollow:author then:^(STIHTTPResponseError *error) {
                @strongify(self)
                if (error == nil) {
                    author.is_follow = NO;
                    [self.model.items removeObject:author];
                    [self.tableView reloadData];
                } else {
                    [self presentMessage:error.message withTips:@"数据获取失败"];
                }
            }];
        };
        [alertView show];
    } else {
        if (!author.is_author) {
            [self dismissTips];
            [[UIApplication sharedApplication].keyWindow presentMessageTips:@"作者不存在！"];
            return;
        }
        
        [AuthorModel follow:author then:^(STIHTTPResponseError *error) {
            if (error == nil) {
                author.is_follow = YES;
                [self.tableView reloadData];
            } else {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
        }];
    }
}

@end
