//
//  SetResolutionController.h
//  MotherPlanet
//
//  Created by liuyadi on 2018/4/19.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SetResolutionControllerDelegate <NSObject>

- (void)finishSelectResolution:(NSString *)resolution;

@end

@interface SetResolutionController : UITableViewController

@property (nonatomic, weak) id <SetResolutionControllerDelegate>delegate;

@end
