//
//  CollectVideoListController.h
//  MotherPlanet
//
//  Created by 陈熙 on 2018/1/16.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

static NSString *kchangeVideoNotification = @"kchangeVideoNotification";

@protocol CollectVideoListControllerDelegate <NSObject>

- (void)noticeSuperVideoState:(BOOL)state;

@end

@interface CollectVideoListController : UIViewController

@property (nonatomic, assign) BOOL isEditState;

@property (nonatomic, weak) id <CollectVideoListControllerDelegate> delegate;

- (void)reloadModel;

@end
