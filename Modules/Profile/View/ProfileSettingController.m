//
//  ProfileSettingController.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/26.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "ProfileSettingController.h"
#import "ProfileSettingHeaderView.h"
#import "SettingSecurityMobileCell.h"
#import "SettingSecurityPasswordCell.h"
#import "SetResolutionController.h"
#import "SettingBindingCell.h"
#import "SettingOtherCell.h"
#import "SettingSignOutCell.h"
#import "ModifyPasswordController.h"
#import "AboutMotherPlanetController.h"
#import "PotocolUrlController.h"
#import "MyBlackListController.h"
#import "RegisterController.h"

@interface ProfileSettingController ()

@property (nonatomic, strong) NSMutableArray *bindingArray;
@property (nonatomic, strong) NSString *defaultResolution;
@property (nonatomic, strong) NSString *cacheString;

@end

@implementation ProfileSettingController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Profile"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.bindingArray = [NSMutableArray array];
    self.tableView.backgroundColor = [UIColor whiteColor];
    [self setupNavigation];
    [self registerNib];
    [self vaildCell];

    self.cacheString = [self getCacheSize];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dismissLoding) name:UIApplicationDidEnterBackgroundNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userInfoDidChanged:) name:kMPUserInfoDidChangedNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    VIDEO_QUALITY defaultQuality = [SettingModel sharedInstance].defaultQuality;
    if (defaultQuality == VIDEO_QUALITY_NORMAL) {
        self.defaultResolution = @"标清";
    } else if (defaultQuality == VIDEO_QUALITY_SD) {
        self.defaultResolution = @"高清";
    } else if (defaultQuality == VIDEO_QUALITY_HD) {
        self.defaultResolution = @"超清";
    }
    [self.tableView reloadData];
}

#pragma mark - CustomMethod

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)dismissLoding {
    [self.view dismissTips];
}

- (void)setupNavigation {
    self.navigationItem.title = @"设置";
    @weakify(self)
    self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)registerNib {
    [self.tableView registerNib:[SettingSecurityMobileCell nib] forCellReuseIdentifier:@"SettingSecurityMobileCell"];
    [self.tableView registerNib:[SettingSecurityPasswordCell nib] forCellReuseIdentifier:@"SettingSecurityPasswordCell"];
    [self.tableView registerNib:[SettingBindingCell nib] forCellReuseIdentifier:@"SettingBindingCell"];
    [self.tableView registerNib:[SettingOtherCell nib] forCellReuseIdentifier:@"SettingOtherCell"];
    [self.tableView registerNib:[SettingSignOutCell nib] forCellReuseIdentifier:@"SettingSignOutCell"];
}

- (void)vaildCell {
    [self.bindingArray removeAllObjects];
    if ([AppConfig isShowWechat]) {
        [self.bindingArray addObject:@"wechat"];
    }
    if ([AppConfig isShowWeibo]) {
        [self.bindingArray addObject:@"weibo"];
    }
    if ([AppConfig isShowQQ]) {
        [self.bindingArray addObject:@"qq"];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    CGFloat sectionCount = 3;
    if (self.bindingArray.count) {
        sectionCount += 1;
    }
    return sectionCount;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.bindingArray.count) {
        if (section == 0) {
            return 2;
        } else if (section == 1) {
            return self.bindingArray.count;
        } else if (section == 2) {
            return 2;
        } else if (section == 3) {
            return 7;
        }
    } else {
        if (section == 0) {
            return 2;
        } else if (section == 1) {
            return 2;
        } else if (section == 2) {
            return 7;
        }
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.bindingArray.count) {
        if (indexPath.section == 0) {
            if (indexPath.row == 0) {
                SettingSecurityMobileCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingSecurityMobileCell" forIndexPath:indexPath];
                cell.user = self.user;
                return cell;
            } else {
                SettingSecurityPasswordCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingSecurityPasswordCell" forIndexPath:indexPath];
                return cell;
            }
        } else if (indexPath.section == 1) {
            SettingBindingCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingBindingCell" forIndexPath:indexPath];
            cell.user = self.user;
            cell.data = self.bindingArray[indexPath.row];
            return cell;
        } else if (indexPath.section == 2) {
            SettingOtherCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingOtherCell" forIndexPath:indexPath];
            if (indexPath.row == 1) {
                cell.value = self.defaultResolution;
            }
            cell.settingType = SETTING_TYPE_AUTOPLAY + indexPath.row;
            return cell;
        } else if (indexPath.section == 3) {
            SettingOtherCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingOtherCell" forIndexPath:indexPath];
            if (indexPath.row == 0) {
                cell.value = self.cacheString;
            }
            cell.settingType = SETTING_TYPE_CLEAR_CACHE + indexPath.row;
            return cell;
        }
    } else {
        if (indexPath.section == 0) {
            if (indexPath.row == 0) {
                SettingSecurityMobileCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingSecurityMobileCell" forIndexPath:indexPath];
                cell.user = self.user;
                return cell;
            } else {
                SettingSecurityPasswordCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingSecurityPasswordCell" forIndexPath:indexPath];
                return cell;
            }
        } else if (indexPath.section == 1) {
            SettingOtherCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingOtherCell" forIndexPath:indexPath];
            if (indexPath.row == 1) {
                cell.value = self.defaultResolution;
            }
            cell.settingType = SETTING_TYPE_AUTOPLAY + indexPath.row;
            return cell;
        } else if (indexPath.section == 2) {
            SettingOtherCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SettingOtherCell" forIndexPath:indexPath];
            if (indexPath.row == 0) {
                cell.value = self.cacheString;
            }
            cell.settingType = SETTING_TYPE_CLEAR_CACHE + indexPath.row;
            return cell;
        }
    }
    return nil;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.bindingArray.count) {
        if (indexPath.section == 0) {
            if (indexPath.row == 1) {
                ModifyPasswordController *modifyPassword = [ModifyPasswordController spawn];
                [self.navigationController pushViewController:modifyPassword animated:YES];
            } else {
                if (self.user.mobile.length < 1) {  // 未绑定手机号
                    RegisterController *bindMobile = [RegisterController spawn];
                    bindMobile.type = CODE_TYPE_MODIFY_MOBILE;
                    bindMobile.backType = MPRegisterBackTypeCancel;
                    [self.navigationController pushViewController:bindMobile animated:YES];
                }
            }
        } else if (indexPath.section == 1) {
            NSString *identify = self.bindingArray[indexPath.row];
            if ([identify isEqualToString:@"wechat"]) {
                //微信
                if (!self.user.is_bind_wechat) {
                    [self.view presentLoadingTips:nil];
                    [self bindingWechat];
                } else {
                    [self unBindWithVendor:identify];
                }
            } else if ([identify isEqualToString:@"weibo"]) {
                //微博
                if (!self.user.is_bind_weibo) {
                    [self.view presentLoadingTips:nil];
                    [self bindingWeibo];
                } else {
                    [self unBindWithVendor:identify];
                }
            } else {
                //qq
                if (!self.user.is_bind_QQ) {
                    [self.view presentLoadingTips:nil];
                    [self bindingQQ];
                } else {
                    [self unBindWithVendor:identify];
                }
            }
        } else if (indexPath.section == 2) {
            if (indexPath.row == 1) {
                [self pushSetResolution];
            }
        } else if (indexPath.section == 3) {
            [self selectRowAtLastSectionIndexPath:indexPath];
        }
    } else {
        if (indexPath.section == 0) {
            if (indexPath.row == 1) {
                ModifyPasswordController *modifyPassword = [ModifyPasswordController spawn];
                [self.navigationController pushViewController:modifyPassword animated:YES];
            }
        } else if (indexPath.section == 1) {
            if (indexPath.row == 1) {
                [self pushSetResolution];
            }
        } else if (indexPath.section == 2) {
            [self selectRowAtLastSectionIndexPath:indexPath];
        }
    }
}

- (void)selectRowAtLastSectionIndexPath:(NSIndexPath *)indexPath {
    SettingOtherCell *cell = [self.tableView cellForRowAtIndexPath:indexPath];
    switch (cell.settingType) {
        case SETTING_TYPE_CLEAR_CACHE: {
            [self clearCacheStart];
            break;
        }
        case SETTING_TYPE_IDEA: {
            [self pushFeedback];
            break;
        }
        case SETTING_TYPE_BLACKLIST: {
            [self pushBlackList];
            break;
        }
        case SETTING_TYPE_CONTRIBUTE: {
            [self pushSubmission];
            break;
        }
        case SETTING_TYPE_ABOUT: {
            [self pushAboutMotherPlanet];
            break;
        }
        case SETTING_TYPE_APPSTORE: {
            [self goAppStore];
            break;
        }
        case SETTING_TYPE_SIGNOUT: {
            [self signOut];
            break;
        }
        default:
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 44;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    ProfileSettingHeaderView *header = [ProfileSettingHeaderView loadFromNib];
    if (self.bindingArray.count) {
        if (section == 0) {
            header.headerType = SET_HEADER_TYPE_SECURITY;
        } else if (section == 1) {
            header.headerType = SET_HEADER_TYPE_BINDING;
        } else if (section == 2) {
            header.headerType = SET_HEADER_TYPE_VIDEO;
        } else {
            header.headerType = SET_HEADER_TYPE_OTHER;
        }
    } else {
        if (section == 0) {
            header.headerType = SET_HEADER_TYPE_SECURITY;
        } else if (section == 1) {
            header.headerType = SET_HEADER_TYPE_VIDEO;
        } else {
            header.headerType = SET_HEADER_TYPE_OTHER;
        }
    }
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return MAX(ceil(kScreenWidth * 72 / 750), 36);
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    if (self.bindingArray.count) {
        if (section == 2) {
            return CGFLOAT_MIN;
        }
        return 10;
    } else {
        if (section == 1) {
            return CGFLOAT_MIN;
        }
        return 10;
    }
}

//获取缓存大小
- (NSString *)getCacheSize {
    NSInteger byteSize = [[SDImageCache sharedImageCache] getSize];
    
    double size = 0.0;
    
    if (byteSize < 1024) {
        return [NSString stringWithFormat:@"%.1f B", size];
    } else if (byteSize < 1048576) {
        size = byteSize / 1024.0;
        return [NSString stringWithFormat:@"%.1f KB", size];
    } else if (byteSize < 1073741824) {
        size = byteSize / 1048576.0;
        return [NSString stringWithFormat:@"%.1f MB", size];
    } else {
        size = byteSize / 1073741824.0;
        return [NSString stringWithFormat:@"%.1f G", size];
    }
    
    return [NSString stringWithFormat:@"0.0 B"];
}

//清除缓存
- (void)clearCacheStart {
    if ([self.cacheString isEqualToString:@"0.0 B"]) {
        [self presentMessageTips:@"已经很干净了，记得下次来清理呦~"];
    } else {
        MPAlertView *alertView = [MPAlertView alertWithTitle:@"清除缓存" message:@"确定清除缓存" cancelButton:@"取消" confirmButton:@"确定"];
        @weakify(self)
        alertView.confirmAction = ^{
            @strongify(self)
            [self presentLoadingTips:@"清除中"];
            [[SDImageCache sharedImageCache] clearDisk];
            [self dismissTips];
            self.cacheString = @"0.0 B";
            UIImageView *successIcon = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"icon_grade_acc"]];
            [self presentMessageTips:@"已清除缓存" customView:successIcon];
            [self.tableView reloadData];
        };
        [alertView show];
    }
}

//解绑第三方账号
- (void)unBindWithVendor:(NSString *)vendor {
    NSString *message = [AppTheme defaultPlaceholder];
    SOCIAL_VENDOR socialVendor = SOCIAL_VENDOR_WEIXIN;
    if ([vendor isEqualToString:@"wechat"]) {
        message = @"确定解绑微信?";
        socialVendor = SOCIAL_VENDOR_WEIXIN;
    } else if ([vendor isEqualToString:@"weibo"]) {
        message = @"确定解绑微博?";
        socialVendor = SOCIAL_VENDOR_WEIBO;
    } else {
        message = @"确定解绑QQ?";
        socialVendor = SOCIAL_VENDOR_QQ;
    }
    
    MPAlertView *alertView = [MPAlertView alertWithTitle:@"解绑" message:message cancelButton:@"取消" confirmButton:@"确定"];
    @weakify(self)
    alertView.confirmAction = ^{
        [self.view presentLoadingTips:nil];
        [[UserModel sharedInstance] socialUnbindWithVendor:socialVendor completion:^(STIHTTPResponseError *e) {
            @strongify(self)
            [self.view dismissTips];
            if (e == nil) {
                [self reloadUserInfo];
                [self.tableView reloadData];
                [self.view presentSuccessTips:@"解绑成功"];
            } else {
                [self.view presentMessage:e.message withTips:@"解绑失败"];
            }
        }];
    };
    [alertView show];
}

- (void)bindingQQ {
    TencentOpenShared *tencent = [TencentOpenShared sharedInstance];
    @weakify(self)
    tencent.whenGetUserInfoSucceed = ^(id data) {
        @strongify(self)
        ACCOUNT *account = (ACCOUNT *)data;
        account.vendor = SOCIAL_VENDOR_QQ;
        [self socialBindingWithAccount:account];
    };
    tencent.whenGetUserInfoFailed = ^{
        [self.view presentFailureTips:@"取消绑定"];
    };
    [tencent getUserInfo];
}

- (void)bindingWeibo {
    SinaWeibo *weibo = [SinaWeibo sharedInstance];
    @weakify(self)
    weibo.whenGetUserInfoSucceed = ^(id data) {
        @strongify(self)
        ACCOUNT *account = (ACCOUNT *)data;
        account.vendor = SOCIAL_VENDOR_WEIBO;
        [self socialBindingWithAccount:account];
    };
    weibo.whenGetUserInfoFailed = ^{
        [self.view presentFailureTips:@"取消绑定"];
    };
    [weibo getUserInfo];
}

- (void)bindingWechat {
    WXChatShared *wechat = [WXChatShared sharedInstance];
    @weakify(self)
    wechat.whenGetUserInfoSucceed = ^(id data) {
        @strongify(self)
        ACCOUNT *account = (ACCOUNT *)data;
        account.vendor = SOCIAL_VENDOR_WEIXIN;
        [self socialBindingWithAccount:account];
    };
    wechat.whenGetUserInfoFailed = ^{
        [self.view presentFailureTips:@"取消绑定"];
    };
    [wechat getuserInfo];
}

//第三方账号绑定
- (void)socialBindingWithAccount:(ACCOUNT *)account {
    [self.view presentLoadingTips:@"授权中"];
    @weakify(self)
    [[UserModel sharedInstance] socialBindWithVendor:account.vendor access_token:account.auth_token open_id:account.user_id completion:^(STIHTTPResponseError *e) {
        @strongify(self)
        [self dismissTips];
        if (e == nil) {
            [self reloadUserInfo];
            [self.view presentSuccessTips:@"绑定成功"];
        } else {
            [self presentMessage:e.message withTips:@"数据获取失败"];
        }
    }];
}

//刷新用户信息
- (void)reloadUserInfo {
    @weakify(self)
    [[UserModel sharedInstance] getUserProfileWithUser_id:nil completion:^(STIHTTPResponseError *e) {
        @strongify(self)
        if (e == nil) {
            self.user = [UserModel sharedInstance].user;
            [self.tableView reloadData];
        }
    }];
}

- (void)pushSetResolution {
    SetResolutionController *setResolution = [SetResolutionController spawn];
    setResolution.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:setResolution animated:YES];
}

//意见反馈
- (void)pushFeedback {
    PotocolUrlController *potocolUrl = [PotocolUrlController spawn];
    potocolUrl.potocolType = POTOCOL_TYPE_FEEDBACK;
    [self.navigationController pushViewController:potocolUrl animated:YES];
}

//黑名单
- (void)pushBlackList {
    MyBlackListController *blackList = [MyBlackListController spawn];
    [self.navigationController pushViewController:blackList animated:YES];
}

//关于我们
- (void)pushAboutMotherPlanet {
    AboutMotherPlanetController *aboutMotherPlanet = [AboutMotherPlanetController spawn];
    [self.navigationController pushViewController:aboutMotherPlanet animated:YES];
}

//我要投稿
- (void)pushSubmission {
    PotocolUrlController *potocolUrl = [PotocolUrlController spawn];
    potocolUrl.potocolType = POTOCOL_TYPE_AUTHOR;
    [self.navigationController pushViewController:potocolUrl animated:YES];
}

//去App Store评分
- (void)goAppStore {
    NSString *urlStr = @"https://itunes.apple.com/app/id1336325864";
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:urlStr]];
}

- (void)signOut {
    MPAlertView *alertView = [MPAlertView alertWithTitle:@"退出登录" message:@"确定退出登录?" cancelButton:@"取消" confirmButton:@"确定"];
    alertView.confirmAction = ^{
        [[UserModel sharedInstance] signout];
        [[MPRootViewController sharedInstance].topViewController.view presentSuccessTips:@"退出登录成功"];
    };
    [alertView show];
}

#pragma mark - Notification

- (void)userInfoDidChanged:(NSNotification *)note {
    self.user = [UserModel sharedInstance].user;
    [self.tableView reloadData];
}

@end
