//
//  MyCollectionController.m
//  MotherPlanet
//
//  Created by 陈熙 on 2018/1/16.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "MyCollectionController.h"
#import "CollectVideoListController.h"
#import "CollectTopicListController.h"

@interface MyCollectionController () <CollectVideoListControllerDelegate, CollectTopicListControllerDelegate>

@property (nonatomic, strong) UIButton *editButton;
@property (nonatomic, strong) CollectVideoListController *videoList;
@property (nonatomic, strong) CollectTopicListController *topicList;

@end

@implementation MyCollectionController

- (instancetype)init {
    if (self = [super init]) {
        self.titleColorSelected = [UIColor colorWithRGBValue:0x343338];
        self.titleColorNormal = [UIColor colorWithRGBValue:0xA9AEB5];
        self.menuViewStyle = WMMenuViewStyleLine;
        self.titleSizeNormal = 14;
        self.titleSizeSelected = 14;
        self.progressWidth = 40;
        self.progressViewIsDamping = YES;
        self.pageAnimatable = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"我的收藏";
    @weakify(self)
    self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [self customize];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if (self.selectIndex == 0) {
        [self.videoList reloadModel];
    } else {
        [self.topicList reloadModel];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController enabledMLBlackTransition:YES];
}

#pragma mark - CustomMethod

- (void)customize {
    [self.navigationController enabledMLBlackTransition:NO];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, 40 , 40);
    self.editButton = button;
    [button setTitle:@"编辑" forState:UIControlStateNormal];
    [button setTitle:@"取消" forState:UIControlStateSelected];
    [button setTitleColor:[AppTheme verifyCodeButtonColor] forState:UIControlStateNormal];
    [button setTitleColor:[AppTheme verifyCodeButtonColor] forState:UIControlStateSelected];
    button.titleLabel.font = [UIFont systemFontOfSize:14];
    [button addTarget:self action:@selector(openEditState) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
}

- (void)openEditState {
    self.editButton.selected = !self.editButton.selected;
    if (self.selectIndex == 0) {
        self.videoList.isEditState = self.editButton.selected;
    } else {
        self.topicList.isEditState = self.editButton.selected;
    }
    [self pageViewCanScroll];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kchangeVideoNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kchangeTopicNotification object:nil];
}

- (void)pageViewCanScroll {
    if (self.editButton.selected == YES) {
        self.scrollEnable = NO;
        self.menuView.userInteractionEnabled = NO;
    } else {
        self.scrollEnable = YES;
        self.menuView.userInteractionEnabled = YES;
    }
}

#pragma mark - CollectVideoListControllerDelegate

- (void)noticeSuperVideoState:(BOOL)state {
    if (state == YES) {
        self.editButton.hidden = NO;
        self.editButton.selected = NO;
        self.videoList.isEditState = NO;
    } else {
        self.editButton.hidden = YES;
    }
    [self pageViewCanScroll];
}

#pragma mark - CollectTopicListControllerDelegate

- (void)noticeSuperTopicState:(BOOL)state {
    if (state == YES) {
        self.editButton.hidden = NO;
        self.editButton.selected = NO;
        self.topicList.isEditState = NO;
    } else {
        self.editButton.hidden = YES;
    }
    [self pageViewCanScroll];
}

#pragma mark -

- (NSArray<NSString *> *)titles {
    return @[@"短片", @"话题"];
}

- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    return self.titles.count;
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    if (index == 0) {
        //短片
        self.videoList = [CollectVideoListController spawn];
        self.videoList.delegate = self;
        return self.videoList;
    } else {
        //话题
        self.topicList = [CollectTopicListController spawn];
        self.topicList.delegate = self;
        return self.topicList;
    }
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView {
    return CGRectMake(0, 0, self.view.frame.size.width , 44);
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView {
    CGFloat originY = CGRectGetMaxY([self pageController:pageController preferredFrameForMenuView:self.menuView]);
    return CGRectMake(0, originY, self.view.frame.size.width, self.view.frame.size.height - originY);
}

- (void)pageController:(WMPageController *)pageController didEnterViewController:(__kindof UIViewController *)viewController withInfo:(NSDictionary *)info {
    if (self.selectIndex == 0) {
        [[NSNotificationCenter defaultCenter] postNotificationName:kchangeVideoNotification object:nil];
    } else {
        [[NSNotificationCenter defaultCenter] postNotificationName:kchangeTopicNotification object:nil];
    }
}

@end
