//
//  CollectTopicListController.m
//  MotherPlanet
//
//  Created by 陈熙 on 2018/1/16.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "CollectTopicListController.h"
#import "MyCollectTopicCell.h"
#import "MyLikeTopicListModel.h"
#import "MyCollectHeaderView.h"
#import "TopicInfoModel.h"
#import "TopicModel.h"
#import "TopicVideoListController.h"

@interface CollectTopicListController () <UITableViewDataSource, UITableViewDelegate, MyCollectHeaderViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *deleteView;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

@property (nonatomic, strong) MyLikeTopicListModel *mylikeTopicListModel;
@property (nonatomic, strong) MyCollectHeaderView *headerView;
@property (nonatomic, strong) NSMutableArray *deleteTopicArray;

@end

@implementation CollectTopicListController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Profile"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.deleteTopicArray = [NSMutableArray array];
    [self setupSubview];
    [self modelInit];
    @weakify(self)
    [self fk_observeNotifcation:kchangeTopicNotification usingBlock:^(NSNotification *note) {
        @strongify(self)
        [self.mylikeTopicListModel  refresh];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

#pragma mark - CustomMethod

- (void)setupSubview {
    self.deleteView.hidden = YES;
    self.deleteButton.layer.cornerRadius = 15;
    self.deleteButton.layer.masksToBounds = YES;
    self.deleteButton.layer.borderWidth = [AppTheme onePixel];
    self.deleteButton.layer.borderColor = [UIColor colorWithRGBValue:0xFF1717].CGColor;
    
    self.tableView.tableFooterView = [UIView new];
    self.tableView.backgroundColor = [UIColor whiteColor];
    [self.tableView registerNib:[MyCollectTopicCell nib] forCellReuseIdentifier:@"MyCollectTopicCell"];
    [self.tableView registerNib:[EmptyTableCell nib] forCellReuseIdentifier:@"EmptyTableCell"];
    
    self.headerView = [MyCollectHeaderView loadFromNib];
    self.headerView.delegate = self;
}

- (void)modelInit {
    self.mylikeTopicListModel = [[MyLikeTopicListModel alloc] init];
    @weakify(self)
    self.mylikeTopicListModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        if (error == nil) {
            [self setupRefreshFooterView];
            if ([self.delegate respondsToSelector:@selector(noticeSuperTopicState:)]) {
                if (self.mylikeTopicListModel.items.count) {
                    [self.delegate noticeSuperTopicState:YES];
                } else {
                    [self.delegate noticeSuperTopicState:NO];
                }
            }
            
            if (self.mylikeTopicListModel.more) {
                [self.tableView.footer endRefreshing];
            } else {
                [self.tableView.footer noticeNoMoreData];
            }
        } else {
            [self.view presentMessage:error.message withTips:@"加载数据失败"];
        }
        [self.tableView reloadData];
    };
}

- (void)setupRefreshFooterView {
    if (self.tableView.footer == nil && !self.mylikeTopicListModel.isEmpty) {
        @weakify(self)
        [self.tableView addFooterPullLoader:^{
            @strongify(self)
            [self.mylikeTopicListModel loadMore];
        }];
    } else {
        if (self.mylikeTopicListModel.isEmpty) {
            [self.tableView removeFooter];
        }
    }
}

- (BOOL)deleteTopicArrayContainData:(NSString *)dataId {
    if ([self.deleteTopicArray containsObject:dataId]) {
        return YES;
    } else {
        return NO;
    }
}

- (void)setIsEditState:(BOOL)isEditState {
    _isEditState = isEditState;
    if (isEditState) {
        self.deleteView.hidden = NO;
    } else {
        self.deleteView.hidden = YES;
        self.headerView.isAllSelect = NO;
        if (self.deleteTopicArray.count > 0) {
            [self.deleteTopicArray removeAllObjects];
        }
    }
    [self.tableView reloadData];
}

//单选短片
- (void)selectTopicWithID:(NSString *)topicID isSelect:(BOOL)isSelect {
    if (isSelect) {
        [self.deleteTopicArray addObject:topicID];
        if (self.deleteTopicArray.count == self.mylikeTopicListModel.items.count) {
            self.headerView.isAllSelect = YES;
        }
    } else {
        if (self.deleteTopicArray.count > 0) {
            [self.deleteTopicArray removeObject:topicID];
            if (self.deleteTopicArray.count != self.mylikeTopicListModel.items.count) {
                self.headerView.isAllSelect = NO;
            }
        }
    }
    [self.tableView reloadData];
}

- (void)reloadModel {
    [self.mylikeTopicListModel refresh];
}

#pragma mark - MyCollectHeaderViewDelegate

- (void)selectAllItemIsCancel:(BOOL)isCancel {
    if (isCancel == NO) {
        for (TOPIC *topic in self.mylikeTopicListModel.items) {
            [self.deleteTopicArray addObject:topic.id];
        }
    } else {
        if (self.deleteTopicArray.count > 0) {
            [self.deleteTopicArray removeAllObjects];
        }
    }
    [self.tableView reloadData];
}

- (IBAction)deleteTopicAction:(id)sender {
    if (self.deleteTopicArray.count) {
        MPAlertView *alertView = [MPAlertView alertWithTitle:@"取消收藏" message:@"确定取消收藏?" cancelButton:@"取消" confirmButton:@"确定"];
        alertView.confirmAction = ^{
            @weakify(self)
            [TopicModel removeTopicWithTopic_ids:self.deleteTopicArray then:^(STIHTTPResponseError *error) {
                @strongify(self)
                if (error == nil) {
                    [self.view presentSuccessTips:@"删除成功"];
                    self.deleteView.hidden = YES;
                    if ([self.delegate respondsToSelector:@selector(noticeSuperTopicState:)]) {
                        if (self.mylikeTopicListModel.items.count) {
                            [self.delegate noticeSuperTopicState:YES];
                        } else {
                            [self.delegate noticeSuperTopicState:NO];
                        }
                    }
                    [self.mylikeTopicListModel refresh];
                } else {
                    [self.view presentMessage:error.message withTips:@"删除失败"];
                }
            }];
        };
        [alertView show];
    }
}

#pragma mark - UITableViewDataSource;

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.mylikeTopicListModel.isEmpty && self.mylikeTopicListModel.loaded) {
        return 1;
    }
    return self.mylikeTopicListModel.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.mylikeTopicListModel.isEmpty && self.mylikeTopicListModel.loaded) {
        EmptyTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EmptyTableCell" forIndexPath:indexPath];
        return cell;
    }
    MyCollectTopicCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyCollectTopicCell" forIndexPath:indexPath];
    cell.data = self.mylikeTopicListModel.items[indexPath.row];
    cell.isEditState = self.isEditState;
    TOPIC *topic = self.mylikeTopicListModel.items[indexPath.row];
    cell.isSelect = [self deleteTopicArrayContainData:topic.id];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.mylikeTopicListModel.isEmpty && self.mylikeTopicListModel.loaded) {
        return self.tableView.bounds.size.height;
    }
    return MAX(kScreenWidth * 180 / 750, 90);
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (self.isEditState) {
        if (self.mylikeTopicListModel.items.count > 0) {
            return 35;
        } else {
            return CGFLOAT_MIN;
        }
    } else {
        return CGFLOAT_MIN;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (self.isEditState) {
        if (self.mylikeTopicListModel.items.count > 0) {
            return self.headerView;
        } else {
            return nil;
        }
    } else {
        return nil;
    }
}

#pragma mark - UITableViewDelegate;

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.mylikeTopicListModel.items.count) {
        if (self.isEditState) {
            TOPIC *topic = self.mylikeTopicListModel.items[indexPath.row];
            MyCollectTopicCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.isSelect = !cell.isSelect;
            [self selectTopicWithID:topic.id isSelect:cell.isSelect];
        } else {
            TOPIC *topic = self.mylikeTopicListModel.items[indexPath.row];
            TopicVideoListController *topicInfo = [TopicVideoListController spawn];
            topicInfo.topic = topic;
            [self.navigationController pushViewController:topicInfo animated:YES];
        }
    }
}


@end
