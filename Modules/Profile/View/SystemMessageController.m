//
//  SystemMessageController.m
//  MotherPlanet
//
//  Created by 陈熙 on 2018/1/2.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "SystemMessageController.h"
#import "SystemMessageModel.h"
#import "SystemMessageCell.h"

@interface SystemMessageController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) SystemMessageModel *systemMessageModel;

@end

@implementation SystemMessageController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Profile"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self registerNib];
    [self setupModel];
    self.tableView.backgroundColor = [AppTheme listBackgroundColor];
    [self setupRefreshHeaderView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView.header beginRefreshing];
}

#pragma mark - CustomMethod

- (void)registerNib {
    self.tableView.backgroundColor = [AppTheme listBackgroundColor];
    [self.tableView registerNib:[SystemMessageCell nib] forCellReuseIdentifier:@"SystemMessageCell"];
    [self.tableView registerNib:[EmptyTableCell nib] forCellReuseIdentifier:@"EmptyTableCell"];
}

- (void)setupModel {
    self.systemMessageModel = [[SystemMessageModel alloc] init];
    @weakify(self)
    self.systemMessageModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        [self.tableView.header endRefreshing];
        if (error == nil) {
            [self setupRefreshFooterView];
            if (self.systemMessageModel.more) {
                [self.tableView.footer endRefreshing];
            } else {
                [self.tableView.footer noticeNoMoreData];
            }
            if ([self.delegate respondsToSelector:@selector(noticeSuperRefreshSystem)]) {
                [self.delegate noticeSuperRefreshSystem];
            }
        } else {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
        [self.tableView reloadData];
    };
}

- (void)setupRefreshHeaderView {
    @weakify(self)
    [self.tableView addHeaderPullLoader:^{
        @strongify(self)
        [self.systemMessageModel refresh];
    }];
}

- (void)setupRefreshFooterView {
    if ( self.tableView.footer == nil && !self.systemMessageModel.isEmpty) {
        @weakify(self)
        [self.tableView addFooterPullLoader:^{
            @strongify(self)
            [self.systemMessageModel loadMore];
        }];
    } else {
        if (self.systemMessageModel.isEmpty) {
            [self.tableView removeFooter];
        }
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.systemMessageModel.isEmpty && self.systemMessageModel.loaded) {
        return 1;
    }
    return self.systemMessageModel.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.systemMessageModel.isEmpty && self.systemMessageModel.loaded) {
        EmptyTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EmptyTableCell" forIndexPath:indexPath];
        return cell;
    }
    SystemMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SystemMessageCell" forIndexPath:indexPath];
    cell.data = self.systemMessageModel.items[indexPath.row];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.systemMessageModel.items.count) {
        SYSTEM_MESSAGE *message = self.systemMessageModel.items[indexPath.row];
        NSString *link = message.link;
        if ([link isEqualToString:@"mplanet://messageList"]) {
            return;
        }
        if (link && link.length) {
            if (![DeepLink handleURLString:link withCompletion:nil]) {
                [[UIApplication sharedApplication].keyWindow presentFailureTips:@"无法打开的链接"];
            }
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.systemMessageModel.isEmpty && self.systemMessageModel.loaded) {
        return self.tableView.height;
    }
    return MAX(ceil(kScreenWidth * 192 / 750), 96);
}

@end
