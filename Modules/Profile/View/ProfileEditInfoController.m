//
//  ProfileEditInfoController.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "ProfileEditInfoController.h"
#import "ProfilePickerView.h"
#import "ChooseDatePickerView.h"
#import "RegionModel.h"

@interface ProfileEditInfoController () <ProfilePickerViewDelegate, ChooseDatePickerViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextViewDelegate, UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UIButton *genderManButton;
@property (weak, nonatomic) IBOutlet UIButton *genderWomanButton;
@property (weak, nonatomic) IBOutlet UIImageView *genderManImage;
@property (weak, nonatomic) IBOutlet UIImageView *genderWomanImage;
@property (weak, nonatomic) IBOutlet UITextField *nicknameTextField;
@property (weak, nonatomic) IBOutlet UILabel *birthdayLabel;
@property (weak, nonatomic) IBOutlet UILabel *constellationLabel;
@property (weak, nonatomic) IBOutlet UILabel *addressLabel;
@property (weak, nonatomic) IBOutlet UITextView *introduceTextView;
@property (weak, nonatomic) IBOutlet UILabel *introducePlaceholderLaebl;
@property (weak, nonatomic) IBOutlet UILabel *introduceNumberLabel;
@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIImageView *cameraImageView;
@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *backViewWLC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *backViewHLC;

@property (nonatomic, strong) UIPickerView *pickerView;
@property (nonatomic, strong) NSMutableArray *provinceArray;
@property (nonatomic, assign) NSInteger provinceIndex;
@property (nonatomic, strong) NSDate *selectDate;
@property (nonatomic, strong) RegionModel *regionModel;
@property (nonatomic, strong) NSString *avatar;
@property (nonatomic, assign) USER_GENDER gender;

@end

@implementation ProfileEditInfoController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Profile"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.provinceArray = [NSMutableArray array];
    self.provinceIndex = 0;
    [self setupNavigationBar];
    [self customize];
    [self loadData];
    [self modelInit];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textDidChange) name:UITextViewTextDidChangeNotification object:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.regionModel refresh];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:UITextViewTextDidChangeNotification];
}

#pragma mark - CustomMethod

- (void)customize {
    self.backViewHLC.constant = kScreenWidth * 192 / 750;
    self.backViewWLC.constant = kScreenWidth * 192 / 750;
    self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;
    self.backView.layer.cornerRadius = kScreenWidth * 192 / 750 / 2;
    self.backView.layer.masksToBounds = YES;
}

- (void)loadData {
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }
    [self.headImageView setImageWithPhoto:self.user.avatar placeholderImage:[AppTheme avatarDefaultImage]];
    if (self.user.gender == USER_GENDER_MALE) {
        self.genderManButton.selected = YES;
        self.genderWomanButton.selected = NO;
        self.genderManImage.image = [UIImage imageNamed:@"icon_man_sel"];
        self.gender = USER_GENDER_MALE;
    } else {
        self.genderManButton.selected = NO;
        self.genderWomanButton.selected = YES;
        self.genderWomanImage.image = [UIImage imageNamed:@"icon_woman_sel"];
        self.gender = USER_GENDER_FEMALE;
    }
    self.nicknameTextField.text = self.user.name ?: [AppTheme defaultPlaceholder];
    self.nicknameTextField.delegate = self;
    self.birthdayLabel.text = self.user.birthday ?: @"请选择";
    self.constellationLabel.text = self.user.constellation ?: @"请选择";
    self.addressLabel.text = self.user.location ?: @"请选择";
    self.introduceTextView.delegate = self;
    if (self.user.introduce && self.user.introduce.length) {
        self.introducePlaceholderLaebl.hidden = YES;
        self.introduceTextView.text = self.user.introduce;
    } else {
        self.introducePlaceholderLaebl.hidden = NO;
        self.introducePlaceholderLaebl.text = @"请填写简介，最多输入30个字";
    }
    self.introduceNumberLabel.text = [NSString stringWithFormat:@"%@/30", [NSNumber numberWithInteger:self.user.introduce.length]];
}

- (void)modelInit {
    self.regionModel = [RegionModel sharedInstance];
    @weakify(self)
    self.regionModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        if (error == nil) {
            self.provinceArray = self.regionModel.item;
            if (self.user.location && self.user.location.length) {
                self.provinceIndex = [self.provinceArray indexOfObject:self.user.location];
            }
        }
    };
}

//导航条
- (void)setupNavigationBar {
    self.navigationItem.title = @"个人资料";
    @weakify(self)
    self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
    self.navigationItem.rightBarButtonItem = [AppTheme normalItemWithContent:@"保存" handler:^(id sender) {
        @strongify(self)
        [self finishEdit];
    }];
}

- (void)textDidChange {
    if (self.introduceTextView.text.length > 0) {
        self.introducePlaceholderLaebl.hidden = YES;
    } else {
        self.introducePlaceholderLaebl.hidden = NO;
    }
    self.introduceNumberLabel.text = [NSString stringWithFormat:@"%@/30", [NSNumber numberWithInteger:self.introduceTextView.text.length]];
}

//保存
- (void)finishEdit {
    if (self.nicknameTextField.text.length == 0) {
        [self.view presentFailureTips:@"昵称不能为空"];
        [self.nicknameTextField becomeFirstResponder];
        return;
    }
    [self.view endEditing:YES];
    [self.view presentLoadingTips:nil];
    NSString *birthdayString = self.birthdayLabel.text;
    NSString *constellationString = self.constellationLabel.text;
    NSString *addressString = self.addressLabel.text;
    if ([self.birthdayLabel.text isEqualToString:@"请选择"]) {
        birthdayString = nil;
    }
    if ([self.constellationLabel.text isEqualToString:@"请选择"]) {
        constellationString = nil;
    }
    if ([self.addressLabel.text isEqualToString:@"请选择"]) {
        addressString = nil;
    }
    @weakify(self)
    [[UserModel sharedInstance] updateUserProfileWithGender:self.gender name:self.nicknameTextField.text email:nil birthday:birthdayString introduce:self.introduceTextView.text avatar:self.avatar cover_photo:nil constellation:constellationString location:addressString completion:^(STIHTTPResponseError *e) {
        @strongify(self)
        [self.view dismissTips];
        if (e == nil) {
            [self.view presentSuccessTips:@"修改成功"];
            [self performSelector:@selector(backAction) withObject:nil afterDelay:0.7];
        } else {
            [self.view presentMessage:e.message withTips:@"修改失败"];
        }
    }];
}

- (void)backAction {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)setupChooseDatePickerView {
    ChooseDatePickerView *chooseDataPicker = [ChooseDatePickerView loadFromNib];
    chooseDataPicker.delegate = self;
    if (self.user.birthday) {
        if (!self.selectDate) {
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd"];
            NSDate *date = [dateFormatter dateFromString:self.user.birthday];
            self.selectDate = date;
        }
    }
    chooseDataPicker.data = self.selectDate;
    [chooseDataPicker show];
}

- (void)setupPickViewWithdata:(NSMutableArray *)dataArray selectIndex:(NSInteger)selectIndex {
    ProfilePickerView *profilePickerView = [ProfilePickerView loadFromNib];
    profilePickerView.selectIndex = selectIndex;
    profilePickerView.data = dataArray;
    profilePickerView.delegate = self;
    [profilePickerView show];
}

#pragma mark - UITextViewDelegate

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if (textView.text.length + text.length > 30) {
        self.introduceNumberLabel.text = [NSString stringWithFormat:@"30/30"];
        return NO;
    } else {
        self.introduceNumberLabel.text = [NSString stringWithFormat:@"%@/30", [NSNumber numberWithInteger:textView.text.length + text.length]];
        return YES;
    }
}

#pragma mark - UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField.text.textLength + string.textLength > 16) {
        return NO;
    } else {
        return YES;
    }
}

#pragma mark - DatePickerViewDelegate

- (void)finishSelectDate:(NSDate *)date {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd"];
    self.birthdayLabel.text = [formatter stringFromDate:date];
    self.selectDate = date;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:date];
    NSInteger month = [components month];
    NSInteger day = [components day];
    NSString *astroString = @"摩羯座水瓶座双鱼座白羊座金牛座双子座巨蟹座狮子座处女座天秤座天蝎座射手座魔羯座";
    NSString *astroFormat = @"102123444543";
    NSString *result = [NSString stringWithFormat:@"%@",[astroString substringWithRange:NSMakeRange(month * 3 - (day < [[astroFormat substringWithRange:NSMakeRange((month - 1), 1)] intValue] - (-19)) * 3, 3)]];
    self.constellationLabel.text = result;
}

#pragma mark - ProfilePickerViewDelegate

- (void)finishPickProvinceForRow:(NSInteger)row {
    self.addressLabel.text = self.provinceArray[row];
    self.provinceIndex = row;
}

#pragma mark - UITableViewDeleagte

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.introduceTextView endEditing:YES];
    [self.nicknameTextField endEditing:YES];
    switch (indexPath.row) {
        case EDIT_TYPE_BIRTHDAY:
            [self setupChooseDatePickerView];
            break;
        case EDIT_TYPE_ADDRESS:
            [self setupPickViewWithdata:self.provinceArray selectIndex:self.provinceIndex];
            break;
        default:
            break;
    }
}

#pragma mark - IBAction

- (IBAction)modifyHeadImageView:(id)sender {
    [AppAnalytics clickEvent:@"click_profile_avatarChange"];
    [self.view endEditing:YES];
    GKZActionSheet *actionSheet = [GKZActionSheet loadFromNib];
    AlertView * alertView = [[AlertView alloc] initWithContent:actionSheet type:AlertViewTypeMonospaced];
    actionSheet.data = @[@"拍照", @"图库"];
    [alertView showSharedView];
    actionSheet.whenHide = ^(BOOL hide) {
        [alertView hide];
    };
    actionSheet.whenRegistered = ^(id data, NSInteger index) {
        [alertView hide];
        if (index == 0) {
            // 拍照
            if ( [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] ) {
                UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                imagePickerController.delegate = self;
                imagePickerController.navigationBar.tintColor = [UIColor blackColor];
                [imagePickerController.navigationBar setTitleTextAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:17], NSForegroundColorAttributeName : [UIColor blackColor]}];
                imagePickerController.allowsEditing = YES;
                imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:imagePickerController animated:YES completion:NULL];
            } else {
                [self presentFailureTips:@"请在设置中打开摄像头权限"];
            }
        } else if (index == 1) {
            // 选照片
            if ( [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary] ) {
                UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                imagePickerController.delegate = self;
                imagePickerController.navigationBar.tintColor = [UIColor blackColor];
                [imagePickerController.navigationBar setTitleTextAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:17], NSForegroundColorAttributeName : [UIColor blackColor]}];
                imagePickerController.allowsEditing = YES;
                imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                [self presentViewController:imagePickerController animated:YES completion:NULL];
            } else {
                [self presentFailureTips:@"请在设置中打开相册权限"];
            }
        }
    };
}
- (IBAction)selectGenderManAction:(UIButton *)sender {
    if (!sender.selected) {
        self.genderManButton.selected = !self.genderManButton.selected;
        self.genderWomanButton.selected = !self.genderWomanButton.selected;
        self.genderManImage.image = [UIImage imageNamed:@"icon_man_sel"];
        self.genderWomanImage.image = [UIImage imageNamed:@"icon_woman_nor"];
        self.gender = USER_GENDER_MALE;
    }
}

- (IBAction)selectGenderWomanAction:(UIButton *)sender {
    if (!sender.selected) {
        self.genderWomanButton.selected = !self.genderWomanButton.selected;
        self.genderManButton.selected = !self.genderManButton.selected;
        self.genderWomanImage.image = [UIImage imageNamed:@"icon_woman_sel"];
        self.genderManImage.image = [UIImage imageNamed:@"icon_man_nor"];
        self.gender = USER_GENDER_FEMALE;
    }
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    image = [image fixOrientation];
    CGRect crop = [[info valueForKey:@"UIImagePickerControllerCropRect"] CGRectValue];
    image = [self ordinaryCrop:image toRect:crop];
    @weakify(self);
    [picker dismissViewControllerAnimated:YES completion:^{
        @strongify(self);
        [self uploadPhotoWithImage:image];
    }];
}

-(UIImage *)ordinaryCrop:(UIImage *)imageToCrop toRect:(CGRect)cropRect {
    CGImageRef imageRef = CGImageCreateWithImageInRect([imageToCrop CGImage], cropRect);
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return cropped;
}

- (void)uploadPhotoWithImage:(UIImage *)image {
    @weakify(self)
//    [image fixedCameraImageToSize:CGSizeMake(kScreenWidth, kScreenWidth) then:^(UIImage * fixedImg) {
        [UploadPhotoModel uploadWithFile:image completion:^(NSString *url, STIHTTPResponseError *e) {
            @strongify(self)
            if (e) {
                [self presentMessage:e.message withTips:@"上传失败"];
            } else {
                [self.headImageView setImageWithString:url];
                self.avatar = url;
                [self.tableView reloadData];
            }
        }];
//    }];
}

#pragma mark - UINavigationControllerDelegate

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if (navigationController.viewControllers.count > 1) {
        UIViewController *imageVC = navigationController.viewControllers[1];
        if ([imageVC isEqual:viewController]) {
            __weak typeof(imageVC) weakImageVC = imageVC;
            imageVC.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
                [weakImageVC.navigationController popViewControllerAnimated:YES];
            }];
        }
    }
}

@end
