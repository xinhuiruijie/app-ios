//
//  MyCollectDeleteView.h
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/25.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MyCollectDeleteViewDelegate <NSObject>

- (void)deleteCollectItems;

@end

@interface MyCollectDeleteView : UIView

@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

@property (nonatomic, weak) id <MyCollectDeleteViewDelegate>delegate;

@end
