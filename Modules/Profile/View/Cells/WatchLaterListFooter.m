//
//  WatchLaterListFooter.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/31.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "WatchLaterListFooter.h"

@interface WatchLaterListFooter ()

@property (nonatomic, strong) UIButton *deleteButton;

@end

@implementation WatchLaterListFooter

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        UIView *topLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, [AppTheme onePixel])];
        topLine.backgroundColor = [UIColor colorWithRed:233/255.0 green:236/255.0 blue:240/255.0 alpha:1];
        [self addSubview:topLine];
        
        UIView *centerLine = [[UIView alloc] initWithFrame:CGRectMake(kScreenWidth / 2, 0, [AppTheme onePixel], frame.size.height)];
        centerLine.backgroundColor = [UIColor colorWithRed:233/255.0 green:236/255.0 blue:240/255.0 alpha:1];
        [self addSubview:centerLine];
        
        self.deleteButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.deleteButton.frame = CGRectMake(0, 0, kScreenWidth / 2, frame.size.height);
        [self.deleteButton setTitle:@"删除" forState:UIControlStateNormal];
        [self.deleteButton setTitleColor:[AppTheme fractionLabelTextColor] forState:UIControlStateNormal];
        [self.deleteButton setTitleColor:[UIColor lightGrayColor] forState:UIControlStateSelected];
        self.deleteButton.selected = YES;
        [self.deleteButton addTarget:self action:@selector(deleteAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.deleteButton];
        
        UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
        cancelButton.frame = CGRectMake(kScreenWidth / 2, 0, kScreenWidth / 2, frame.size.height);
        [cancelButton setTitle:@"取消" forState:UIControlStateNormal];
        [cancelButton setTitleColor:[AppTheme subTextColor] forState:UIControlStateNormal];
        [cancelButton addTarget:self action:@selector(cancelAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:cancelButton];
    }
    return self;
}

- (void)setIsHighlighted:(BOOL)isHighlighted {
    self.deleteButton.selected = isHighlighted;
}

- (void)deleteAction:(UIButton *)sender {
    if (sender.selected) {
        return;
    }
    if (self.deleteAction) {
        self.deleteAction();
    }
}

- (void)cancelAction:(UIButton *)sender {
    if (self.cancelAction) {
        self.cancelAction();
    }
}

@end
