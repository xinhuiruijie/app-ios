//
//  SetResolutionCell.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/17.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "SetResolutionCell.h"

@interface SetResolutionCell ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *checkIcon;

@end

@implementation SetResolutionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setQuality:(VIDEO_QUALITY)quality {
    _quality = quality;
    if (quality == VIDEO_QUALITY_NORMAL) {
        self.titleLabel.text = @"标清";
    } else if (quality == VIDEO_QUALITY_SD) {
        self.titleLabel.text = @"高清";
    } else if (quality == VIDEO_QUALITY_HD) {
        self.titleLabel.text = @"超清";
    }
    
    // 在setQuality中解决选中无效的问题，此问题在GeekMall中遇到过
    if (self.selected) {
        self.checkIcon.hidden = NO;
        self.titleLabel.textColor = [AppTheme textSelectedColor];
    } else {
        self.checkIcon.hidden = YES;
        self.titleLabel.textColor = [AppTheme normalTextColor];
    }
}

@end
