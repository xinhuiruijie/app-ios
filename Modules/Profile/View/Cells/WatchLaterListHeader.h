//
//  WatchLaterListHeader.h
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/30.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WatchLaterListHeader : UIView

@property (nonatomic, assign) BOOL isSelectedAll;
@property (nonatomic, copy) void(^selectedAll)(BOOL selectAll);

@end
