//
//  MyCollectionCell.h
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/23.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, COLLECT_TYPE) {
    COLLECT_TYPE_VIDEO = 1, //视频
    COLLECT_TYPE_TOPIC, //专题
};

@protocol MyCollectionCellDelegate <NSObject>

- (void)deleteItemsWithArray:(NSArray *)array type:(COLLECT_TYPE)type;

- (void)noticeSuperVideoState:(BOOL)state;

- (void)noticeSuperTopicState:(BOOL)state;

@end

@interface MyCollectionCell : UICollectionViewCell

@property (nonatomic, assign) BOOL isEditState;

@property (nonatomic, weak) id <MyCollectionCellDelegate>delegate;

@end
