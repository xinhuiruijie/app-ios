//
//  WatchLaterListFooter.h
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/31.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WatchLaterListFooter : UIView

@property (nonatomic, assign) BOOL isHighlighted;

@property (nonatomic, copy) void(^deleteAction)(void);
@property (nonatomic, copy) void(^cancelAction)(void);

@end
