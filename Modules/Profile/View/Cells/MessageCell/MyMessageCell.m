//
//  MyMessageCell.m
//  MotherPlanet
//
//  Created by 陈熙 on 2018/1/2.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "MyMessageCell.h"

@interface MyMessageCell ()

@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *nickNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *messageTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentLabel;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentTLC;

@end

@implementation MyMessageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.headImageView.layer.cornerRadius = self.headImageView.bounds.size.width / 2;
    self.headImageView.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

+ (CGFloat)heightForRowWithMessage:(MESSAGE *)message {
    CGFloat rowHeight = 16;
    CGFloat headHeight = 40;
    CGFloat commentHeight = 0;
    if (message.content && message.content.length) {
        commentHeight = [AppTheme calculateHeigthWithContent:message.content width:kScreenWidth - 60 - 13];
        commentHeight += 6;
    }
    commentHeight += 10;
    CGFloat lineHeight = 1;
    lineHeight += 10;
    CGFloat iconHeight = 70;
    iconHeight += 20;
    rowHeight = rowHeight + headHeight + commentHeight + lineHeight + iconHeight;
    return rowHeight;
}

- (void)dataDidChange {
    if (!self.data || ![self.data isKindOfClass:[MESSAGE class]]) {
        return;
    }
    
    MESSAGE *message = self.data;
    [self.headImageView setImageWithPhoto:message.owner.avatar placeholderImage:[AppTheme avatarDefaultImage]];
    self.nickNameLabel.text = message.owner.name ?: [AppTheme defaultPlaceholder];
    self.messageTypeLabel.text = [self getMessageType:message.target_type];
    //时间戳转换
    if (message.push_at && message.push_at.length) {
        self.timeLabel.text = [message.push_at conversionDateWithString];
    }
    if (message.target_type == TARGET_TYPE_LIKE) {
        self.commentTLC.constant = 0;
        self.commentLabel.hidden = YES;
        self.commentLabel.text = [AppTheme defaultPlaceholder];
    } else {
        if (message.content && message.content.length) {
            self.commentTLC.constant = 6;
            self.commentLabel.hidden = NO;
            self.commentLabel.text = message.content;
        } else {
            self.commentTLC.constant = 0;
            self.commentLabel.hidden = YES;
            self.commentLabel.text = [AppTheme defaultPlaceholder];
        }
    }
    [self.iconImageView setImageWithPhoto:message.video.photo placeholderImage:[AppTheme avatarDefaultImage]];
    self.titleLabel.text = message.video.title?:@"";
    self.descLabel.text = message.video.subtitle?:@"";
}

- (NSString *)getMessageType:(TARGET_TYPE)type {
    NSString *messageType = [AppTheme defaultPlaceholder];
    switch (type) {
        case TARGET_TYPE_LIKE:
            messageType = @"赞了你的短片";
            break;
        case TARGET_TYPE_REPLY:
            messageType = @"回复了你的评论";
            break;
        case TARGET_TYPE_COMMENT:
            messageType = @"评论了你的短片";
            break;
        default:
            break;
    }
    return messageType;
}

@end
