//
//  MyMessageCell.h
//  MotherPlanet
//
//  Created by 陈熙 on 2018/1/2.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyMessageCell : UITableViewCell

+ (CGFloat)heightForRowWithMessage:(MESSAGE *)message;

@end
