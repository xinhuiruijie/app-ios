//
//  SystemMessageCell.m
//  MotherPlanet
//
//  Created by 陈熙 on 2018/1/2.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "SystemMessageCell.h"

@interface SystemMessageCell ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@end

@implementation SystemMessageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)dataDidChange {
    if (!self.data || ![self.data isKindOfClass:[SYSTEM_MESSAGE class]]) {
        return;
    }
    SYSTEM_MESSAGE *message = self.data;
    self.titleLabel.text = @"系统消息";
    self.descLabel.text = message.content ?: [AppTheme defaultPlaceholder];
    if (message.push_at && message.push_at.length) {
        self.timeLabel.text = [message.push_at conversionDateWithString];
    }
}

@end
