//
//  MyWatchListSectionHeader.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/22.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "MyWatchListSectionHeader.h"

@interface MyWatchListSectionHeader ()

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@end

@implementation MyWatchListSectionHeader

- (void)dataDidChange {
    if (!self.data) {
        return;
    }
    NSString *time = self.data;
    self.timeLabel.text = time;
}

@end
