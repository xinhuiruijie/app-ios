//
//  MyCollectHeaderView.h
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/25.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol MyCollectHeaderViewDelegate <NSObject>

- (void)selectAllItemIsCancel:(BOOL)isCancel;

@end

@interface MyCollectHeaderView : UIView

@property (nonatomic, assign) BOOL isAllSelect;

@property (nonatomic, weak) id <MyCollectHeaderViewDelegate> delegate;

@end
