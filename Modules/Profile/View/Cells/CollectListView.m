//
//  CollectListView.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/23.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "CollectListView.h"

@implementation CollectListView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.bottomList.scrollEnabled = YES;
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    if (self = [super initWithCoder:coder]) {
        self.bottomList.scrollEnabled = YES;
    }
    return self;
}

- (void)reloadData {
    [self.bottomList reloadData];
    [self.topList reloadData];
}

#pragma mark -

- (CGPoint)topListOrigin {
    return CGPointMake(0, 0);
}

- (CGSize)topListSize {
    return CGSizeMake(kScreenWidth, 44);
}

- (CGFloat)topListCellWidth {
    NSInteger count = [self.dataSource itemNumber] > 4 ? 4 : [self.dataSource itemNumber];
    return [self topListSize].width / count;
}

- (BOOL)showListMargeLine {
    return YES;
}

- (BOOL)showIndexLine {
    return YES;
}

- (UIColor *)indexLineColor {
    return [AppTheme normalTextColor];
}

- (CGFloat)topListIndexLineWidth {
    return 40;
}

- (UIColor *)listMargeLineColor {
    return [UIColor colorWithRGBValue:0xE9ECF0];
}

@end
