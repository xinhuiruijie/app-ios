//
//  MyWatchListCell.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/22.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "MyWatchListCell.h"

@interface MyWatchListCell ()
@property (weak, nonatomic) IBOutlet UIImageView *videoPhoto;
@property (weak, nonatomic) IBOutlet UILabel *videoTitle;
@property (weak, nonatomic) IBOutlet UILabel *videoSubtitle;
@property (weak, nonatomic) IBOutlet UIView *videoStarView;
@property (weak, nonatomic) IBOutlet UILabel *videoStar;
@property (weak, nonatomic) IBOutlet UILabel *videoDuration;

@end

@implementation MyWatchListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)dataDidChange {
    if ([self.data isKindOfClass:[VIDEO class]]) {
        VIDEO *video = self.data;
        
        self.videoTitle.text = video.title;
        self.videoSubtitle.attributedText = [AppTheme detailAttributedString:video.subtitle];
        self.videoSubtitle.lineBreakMode = NSLineBreakByTruncatingTail;
        [self.videoPhoto setImageWithPhoto:video.photo];
        self.videoStar.text = [NSString stringWithFormat:@"%.1f", video.star?:0.0];
        self.videoDuration.text = video.video_time;
        [self.videoStarView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            [obj removeFromSuperview];
        }];
        
        NSInteger star = roundf(video.star);
        CGFloat originX = 0;
        for (int i = 1; i < 10; i+=2) {
            originX = (i/2) * (10 + 3);
            NSString *starIcon;
            if (i < star) {
                starIcon = @"icon_star_sel";
            } else if (i > star) {
                starIcon = @"icon_star_nor";
            } else {
                starIcon = @"icon_star_half";
            }
            UIImageView *starImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:starIcon]];
            starImage.frame = CGRectMake(originX, 0, 10, 10);
            [self.videoStarView addSubview:starImage];
        }
    }
}

@end
