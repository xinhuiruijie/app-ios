//
//  MyCollectTopicCell.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/24.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "MyCollectTopicCell.h"

@interface MyCollectTopicCell ()

@property (weak, nonatomic) IBOutlet UIButton *selectButton;
@property (weak, nonatomic) IBOutlet UIImageView *topicImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topicImageViewLLC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *selectButtonWLC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *selectButtonLLC;

@property (nonatomic, strong) NSString *topicID;

@end

@implementation MyCollectTopicCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setIsSelect:(BOOL)isSelect {
    _isSelect = isSelect;
    self.selectButton.selected = isSelect;
}

- (void)setIsEditState:(BOOL)isEditState {
    _isEditState = isEditState;
    if (isEditState) {
        self.selectButtonLLC.constant = 15;
        self.topicImageViewLLC.constant = 12;
        self.selectButtonWLC.constant = 15;
    } else {
        self.selectButtonLLC.constant = 0;
        self.topicImageViewLLC.constant = 10;
        self.selectButtonWLC.constant = 0;
    }
}

#pragma mark - CustomMethod

- (void)dataDidChange {
    if (![self.data isKindOfClass:[TOPIC class]]) {
        return;
    }
    TOPIC *topic = self.data;
    [self.topicImageView setImageWithPhoto:topic.cover_photo];
    self.titleLabel.text = topic.title ?: [AppTheme defaultPlaceholder];
    self.descLabel.text = topic.subtitle ?: [AppTheme defaultPlaceholder];
    self.topicID = topic.id;
}

@end
