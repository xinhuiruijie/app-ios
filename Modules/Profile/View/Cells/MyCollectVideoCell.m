//
//  MyCollectVideoCell.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/24.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "MyCollectVideoCell.h"

@interface MyCollectVideoCell ()

@property (weak, nonatomic) IBOutlet UIButton *selectButton;
@property (weak, nonatomic) IBOutlet UIImageView *videoImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;
@property (weak, nonatomic) IBOutlet UIView *levelView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *selectButtonLLC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *videoImageViewLLC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *selectButtonWLC;

@property (nonatomic, strong) NSString *videoID;

@end

@implementation MyCollectVideoCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setIsSelect:(BOOL)isSelect {
    _isSelect = isSelect;
    self.selectButton.selected = isSelect;
}

- (void)setIsEditState:(BOOL)isEditState {
    _isEditState = isEditState;
    if (isEditState) {
        self.selectButtonLLC.constant = 15;
        self.videoImageViewLLC.constant = 12;
        self.selectButtonWLC.constant = 15;
    } else {
        self.selectButtonLLC.constant = 0;
        self.videoImageViewLLC.constant = 10;
        self.selectButtonWLC.constant = 0;
    }
}

#pragma mark - CustomMethod

- (void)dataDidChange {
    if (![self.data isKindOfClass:[VIDEO class]]) {
        return;
    }
    VIDEO *video = self.data;
    [self.videoImageView setImageWithPhoto:video.photo];
    self.titleLabel.text = video.title ?: [AppTheme defaultPlaceholder];
    self.descLabel.text = video.subtitle ?: [AppTheme defaultPlaceholder];
    self.timeLabel.text = video.video_time ?: [AppTheme defaultPlaceholder];
    self.levelLabel.text = [NSString stringWithFormat:@"%.1f", video.star?:0.0];
    self.videoID = video.id;
    
    [self.levelView.subviews enumerateObjectsUsingBlock:^(__kindof UIView * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
        [obj removeFromSuperview];
    }];
    
    NSInteger star = roundf(video.star);
    CGFloat originX = 0;
    for (int i = 1; i < 10; i+=2) {
        originX = (i/2) * (10 + 3);
        NSString *starIcon;
        if (i < star) {
            starIcon = @"icon_star_sel";
        } else if (i > star) {
            starIcon = @"icon_star_nor";
        } else {
            starIcon = @"icon_star_half";
        }
        UIImageView *starImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:starIcon]];
        starImage.frame = CGRectMake(originX, 0, 10, 10);
        [self.levelView addSubview:starImage];
    }
}

@end
