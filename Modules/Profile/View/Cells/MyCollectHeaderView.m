//
//  MyCollectHeaderView.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/25.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "MyCollectHeaderView.h"

@interface MyCollectHeaderView ()
@property (weak, nonatomic) IBOutlet UIButton *selectButton;
@end

@implementation MyCollectHeaderView

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setIsAllSelect:(BOOL)isAllSelect {
    _isAllSelect = isAllSelect;
    self.selectButton.selected = isAllSelect;
}

#pragma mark - IBAction

- (IBAction)allSelectAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    self.isAllSelect = sender.selected;
    if ([self.delegate respondsToSelector:@selector(selectAllItemIsCancel:)]) {
        [self.delegate selectAllItemIsCancel:!sender.selected];
    }
}


@end
