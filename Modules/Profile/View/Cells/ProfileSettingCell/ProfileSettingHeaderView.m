//
//  ProfileSettingHeaderView.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/26.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "ProfileSettingHeaderView.h"

@interface ProfileSettingHeaderView ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation ProfileSettingHeaderView

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setHeaderType:(SET_HEADER_TYPE)headerType {
    _headerType = headerType;
    switch (headerType) {
        case SET_HEADER_TYPE_SECURITY:
            self.titleLabel.text = @"安全中心";
            break;
        case SET_HEADER_TYPE_BINDING:
            self.titleLabel.text = @"绑定账号";
            break;
        case SET_HEADER_TYPE_VIDEO:
            self.titleLabel.text = @"播映设置";
            break;
        case SET_HEADER_TYPE_OTHER:
            self.titleLabel.text = @"其他设置";
            break;
        default:
            break;
    }
}

@end
