//
//  SettingSecurityMobileCell.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/31.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "SettingSecurityMobileCell.h"

@interface SettingSecurityMobileCell ()

@property (weak, nonatomic) IBOutlet UILabel *mobileLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *mobileRight;
@property (weak, nonatomic) IBOutlet UIImageView *arrowImage;

@end

@implementation SettingSecurityMobileCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)setUser:(USER *)user {
    _user = user;
    
    NSString *mobile = nil;
    if (user.mobile.length > 7) {
        mobile = [user.mobile stringByReplacingCharactersInRange:NSMakeRange(3, 4) withString:@"****"];
    }
    if (mobile.length < 1) {
        _mobileRight.constant = 40;
        _arrowImage.hidden = NO;
        self.mobileLabel.text = @"未绑定";
        self.mobileLabel.textColor = [UIColor colorWithRGBValue:0xEFB400];
    } else {
        _mobileRight.constant = 15;
        _arrowImage.hidden = YES;
        self.mobileLabel.text = mobile;
        self.mobileLabel.textColor = [UIColor colorWithRGBValue:0xA9AEB5];
    }
}

@end
