//
//  SettingBindingCell.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/31.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "SettingBindingCell.h"

@interface SettingBindingCell ()

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *tipLabel;

@end

@implementation SettingBindingCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)dataDidChange {
    if (!self.data) {
        return;
    }
    NSString *typeString = self.data;
    if ([typeString isEqualToString:@"wechat"]) {
        self.iconImageView.image = [UIImage imageNamed:@"icon_set_wechat"];
        self.titleLabel.text = @"微信";
        if (self.user.is_bind_wechat) {
            self.tipLabel.text = self.user.wechat_nickname;
            self.tipLabel.textColor = [UIColor colorWithRGBValue:0xA9AEB5];
        } else {
            self.tipLabel.text = @"未绑定";
            self.tipLabel.textColor = [UIColor colorWithRGBValue:0xEFB400];
        }
    } else if ([typeString isEqualToString:@"weibo"]) {
        self.iconImageView.image = [UIImage imageNamed:@"icon_set_sina"];
        self.titleLabel.text = @"微博";
        if (self.user.is_bind_weibo) {
            self.tipLabel.text = self.user.weibo_nickname;
            self.tipLabel.textColor = [UIColor colorWithRGBValue:0xA9AEB5];
        } else {
            self.tipLabel.text = @"未绑定";
            self.tipLabel.textColor = [UIColor colorWithRGBValue:0xEFB400];
        }
    } else {
        self.iconImageView.image = [UIImage imageNamed:@"icon_set_qq"];
        self.titleLabel.text = @"QQ";
        if (self.user.is_bind_QQ) {
            self.tipLabel.text = self.user.QQ_nickname;
            self.tipLabel.textColor = [UIColor colorWithRGBValue:0xA9AEB5];
        } else {
            self.tipLabel.text = @"未绑定";
            self.tipLabel.textColor = [UIColor colorWithRGBValue:0xEFB400];
        }
    }
}

@end
