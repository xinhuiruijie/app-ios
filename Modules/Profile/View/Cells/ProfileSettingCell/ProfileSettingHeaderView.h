//
//  ProfileSettingHeaderView.h
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/26.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, SET_HEADER_TYPE) {
    SET_HEADER_TYPE_SECURITY,
    SET_HEADER_TYPE_BINDING,
    SET_HEADER_TYPE_VIDEO,
    SET_HEADER_TYPE_OTHER,
};

@interface ProfileSettingHeaderView : UIView

@property (nonatomic, assign) SET_HEADER_TYPE headerType;

@end
