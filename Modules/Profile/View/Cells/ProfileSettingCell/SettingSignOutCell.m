//
//  SettingSignOutCell.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/31.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "SettingSignOutCell.h"

@interface SettingSignOutCell ()

@property (weak, nonatomic) IBOutlet UIButton *signOutButton;

@end

@implementation SettingSignOutCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.signOutButton.layer.cornerRadius = 20;
    self.signOutButton.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)signOutAction:(id)sender {
    if (self.signOutBlock) {
        self.signOutBlock();
    }
}

@end
