//
//  SettingOtherCell.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/31.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "SettingOtherCell.h"

@interface SettingOtherCell ()

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *tipLabel;
@property (weak, nonatomic) IBOutlet UISwitch *autoplaySwicth;
@property (weak, nonatomic) IBOutlet UIImageView *arrowIcon;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tipLabelRC;

@end

@implementation SettingOtherCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.autoplaySwicth.transform = CGAffineTransformMakeScale(0.8, 0.8); //缩放 
}

- (void)setSettingType:(SETTING_TYPE)settingType {
    _settingType = settingType;
    switch (settingType) {
        case SETTING_TYPE_AUTOPLAY: {
            self.autoplaySwicth.on = [SettingModel sharedInstance].autoPlay;
            self.iconImageView.image = [UIImage imageNamed:@"icon_set_play"];
            self.titleLabel.text = @"短片自动播放";
            self.autoplaySwicth.hidden = NO;
            self.tipLabel.hidden = YES;
            self.arrowIcon.hidden = YES;
            break;
        }
        case SETTING_TYPE_RESOLUTION: {
            self.iconImageView.image = [UIImage imageNamed:@"icon_set_hd"];
            self.titleLabel.text = @"默认清晰度";
            self.tipLabel.text = self.value;
            self.autoplaySwicth.hidden = YES;
            self.tipLabel.hidden = NO;
            self.arrowIcon.hidden = NO;
            self.tipLabelRC.constant = 30;
            break;
        }
        case SETTING_TYPE_CLEAR_CACHE: {
            self.iconImageView.image = [UIImage imageNamed:@"icon_set_cache"];
            self.titleLabel.text = @"清除缓存";
            self.tipLabel.text = self.value;
            self.autoplaySwicth.hidden = YES;
            self.tipLabel.hidden = NO;
            self.arrowIcon.hidden = YES;
            self.tipLabelRC.constant = 10;
            break;
        }
        case SETTING_TYPE_IDEA: {
            self.iconImageView.image = [UIImage imageNamed:@"icon_set_idea"];
            self.titleLabel.text = @"意见反馈";
            self.autoplaySwicth.hidden = YES;
            self.tipLabel.hidden = YES;
            self.arrowIcon.hidden = NO;
            break;
        }
        case SETTING_TYPE_BLACKLIST: {
            self.iconImageView.image = [UIImage imageNamed:@"icon_set_stop"];
            self.titleLabel.text = @"黑名单";
            self.autoplaySwicth.hidden = YES;
            self.tipLabel.hidden = YES;
            self.arrowIcon.hidden = YES;
            break;
        }
        case SETTING_TYPE_CONTRIBUTE: {
            self.iconImageView.image = [UIImage imageNamed:@"icon_set_contribute"];
            self.titleLabel.text = @"我要投稿";
            self.autoplaySwicth.hidden = YES;
            self.tipLabel.hidden = YES;
            self.arrowIcon.hidden = YES;
            break;
        }
        case SETTING_TYPE_ABOUT: {
            self.iconImageView.image = [UIImage imageNamed:@"icon_set_about"];
            self.titleLabel.text = @"关于我们";
            self.autoplaySwicth.hidden = YES;
            self.tipLabel.hidden = YES;
            self.arrowIcon.hidden = YES;
            break;
        }
        case SETTING_TYPE_APPSTORE: {
            self.iconImageView.image = [UIImage imageNamed:@"icon_set_store"];
            self.titleLabel.text = @"去App Store评分";
            self.autoplaySwicth.hidden = YES;
            self.tipLabel.hidden = YES;
            self.arrowIcon.hidden = YES;
            break;
        }
        case SETTING_TYPE_SIGNOUT: {
            self.iconImageView.image = [UIImage imageNamed:@"icon_set_quit"];
            self.titleLabel.text = @"退出登录";
            self.autoplaySwicth.hidden = YES;
            self.tipLabel.hidden = YES;
            self.arrowIcon.hidden = YES;
            break;
        }
        default:
            break;
    }
}

- (IBAction)autoplaySwitcValueChanged:(id)sender {
    self.autoplaySwicth.on = !self.autoplaySwicth.isOn;
    [SettingModel sharedInstance].autoPlay = self.autoplaySwicth.isOn;
}

@end
