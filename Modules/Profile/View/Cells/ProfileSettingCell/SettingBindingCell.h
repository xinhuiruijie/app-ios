//
//  SettingBindingCell.h
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/31.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingBindingCell : UITableViewCell

@property (nonatomic, strong) USER *user;

@end
