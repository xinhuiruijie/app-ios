//
//  SettingOtherCell.h
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/31.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, SETTING_TYPE) {
    SETTING_TYPE_AUTOPLAY,
    SETTING_TYPE_RESOLUTION,
    SETTING_TYPE_CLEAR_CACHE,
    SETTING_TYPE_IDEA,
    SETTING_TYPE_BLACKLIST,
    SETTING_TYPE_CONTRIBUTE,
    SETTING_TYPE_ABOUT,
    SETTING_TYPE_APPSTORE,
    SETTING_TYPE_SIGNOUT,
};

@interface SettingOtherCell : UITableViewCell

@property (nonatomic, assign) SETTING_TYPE settingType;

// Please set value before set settingType
@property (nonatomic, strong) NSString *value;

@end
