//
//  TimelineHeaderView.m
//  MotherPlanet
//
//  Created by 陈熙 on 2018/2/2.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "TimelineHeaderView.h"

@interface TimelineHeaderView ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation TimelineHeaderView

- (void)setIsUserMine:(BOOL)isUserMine {
    if (isUserMine) {
        self.titleLabel.text = @"我的瞬间";
    } else {
        self.titleLabel.text = @"TA的瞬间";
    }
}

@end
