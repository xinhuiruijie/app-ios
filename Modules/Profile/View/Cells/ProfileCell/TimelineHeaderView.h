//
//  TimelineHeaderView.h
//  MotherPlanet
//
//  Created by 陈熙 on 2018/2/2.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TimelineHeaderView : UIView

@property (nonatomic, assign) BOOL isUserMine;

@end
