//
//  ProfileCommunityCell.h
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, PROFILE_TYPE) {
    PROFILE_TYPE_WATCH_LATER = 0,
//    PROFILE_TYPE_PACKAGE,
    PROFILE_TYPE_INVITE,
};

@interface ProfileCommunityCell : UITableViewCell

@property (nonatomic, assign) PROFILE_TYPE profileType;

@end
