//
//  ProfileOperationCell.h
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, OPERATION_TYPE) {
    OPERATION_TYPE_FOLLOW = 0,
    OPERATION_TYPE_COLLECT = 1,
    OPERATION_TYPE_WATCH_VIDEO = 2,
    OPERATION_TYPE_MESSAGE = 3,
};

@interface ProfileOperationCell : UITableViewCell

@property (nonatomic, copy) void (^selectBlock)(OPERATION_TYPE operationType);

@end
