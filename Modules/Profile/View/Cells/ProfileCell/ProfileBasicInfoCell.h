//
//  ProfileBasicInfoCell.h
//  MotherPlanet
//
//  Created by liuyadi on 2018/6/1.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProfileBasicInfoViewDelegate <NSObject>

- (void)editProfileInfoAction;

@end

@interface ProfileBasicInfoCell : UITableViewCell

@property (nonatomic, weak) id <ProfileBasicInfoViewDelegate> delegate;

@end
