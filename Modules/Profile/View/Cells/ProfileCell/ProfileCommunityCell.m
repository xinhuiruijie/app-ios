//
//  ProfileCommunityCell.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "ProfileCommunityCell.h"

@interface ProfileCommunityCell ()

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineHLC;

@end

@implementation ProfileCommunityCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.lineHLC.constant = [AppTheme onePixel];
}

- (void)setProfileType:(PROFILE_TYPE)profileType {
    _profileType = profileType;
    switch (profileType) {
        case PROFILE_TYPE_WATCH_LATER:
            self.titleLabel.text = @"稍后观看";
            self.iconImageView.image = [UIImage imageNamed:@"icon_pro_viewlater"];
            break;
//        case PROFILE_TYPE_PACKAGE:
//            self.titleLabel.text = @"我的卡包";
//            self.iconImageView.image = [UIImage imageNamed:@"icon_pro_card"];
//            break;
        case PROFILE_TYPE_INVITE:
            self.titleLabel.text = @"邀请好友";
            self.iconImageView.image = [UIImage imageNamed:@"icon_pro_invite"];
            break;
        default:
            break;
    }
}

@end
