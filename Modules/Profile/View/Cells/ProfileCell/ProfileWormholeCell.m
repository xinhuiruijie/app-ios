//
//  ProfileWormholeCell.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/16.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "ProfileWormholeCell.h"

#import "SearchWormholeCollectionItem.h"
#import "ProfileCreateWormholeItem.h"

#import "UserWormholeListModel.h"

@interface ProfileWormholeCell () <UICollectionViewDelegate, UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *wormholeUsetTitle;
@property (weak, nonatomic) IBOutlet UICollectionView *collection;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *lineHLC;
@property (weak, nonatomic) IBOutlet UILabel *moreLabel;
@property (weak, nonatomic) IBOutlet UIImageView *moreImage;
@property (weak, nonatomic) IBOutlet UILabel *nullLabel;

@property (nonatomic, strong) UserWormholeListModel *userWormholeModel;
@property (nonatomic, assign) USER_GENDER userRender;

@end

@implementation ProfileWormholeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.lineHLC.constant = [AppTheme onePixel];
    
    [self.collection registerNib:[SearchWormholeCollectionItem nib] forCellWithReuseIdentifier:@"SearchWormholeCollectionItem"];
    [self.collection registerNib:[ProfileCreateWormholeItem nib] forCellWithReuseIdentifier:@"ProfileCreateWormholeItem"];
}

- (void)setUser:(USER *)user {
    self.userRender = user.gender;
}

- (void)dataDidChange {
    self.userWormholeModel = self.data;
    [self.collection reloadData];
    
    if (self.wormholeUser == WORMHOLE_USER_TYPE_MY) {
        self.wormholeUsetTitle.text = @"My虫洞";
        self.moreLabel.hidden = NO;
        self.moreImage.hidden = NO;
        self.nullLabel.hidden = YES;
    } else {
        if (self.userWormholeModel.items.count == 0) {
            self.moreLabel.hidden = YES;
            self.moreImage.hidden = YES;
            self.nullLabel.hidden = NO;
        } else {
            self.moreLabel.hidden = NO;
            self.moreImage.hidden = NO;
            self.nullLabel.hidden = YES;
        }
        switch (self.userRender) {
            case USER_GENDER_FEMALE:
                self.wormholeUsetTitle.text = @"Her虫洞";
                break;
                
            default:
                self.wormholeUsetTitle.text = @"His虫洞";
                break;
        }
    }
}

#pragma mark - Collection View Data Source

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (self.userWormholeModel.items.count == 0) {
        return 1;
    } else if (self.userWormholeModel.items.count >= 8) {
        return 8;
    } else {
        return self.userWormholeModel.items.count;
    }
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.userWormholeModel.items.count == 0) {
        ProfileCreateWormholeItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ProfileCreateWormholeItem" forIndexPath:indexPath];
        return cell;
    } else {
        SearchWormholeCollectionItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SearchWormholeCollectionItem" forIndexPath:indexPath];
        cell.data = self.userWormholeModel.items[indexPath.row];
        return cell;
    }
//    SearchWormholeCollectionItem *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"SearchWormholeCollectionItem" forIndexPath:indexPath];
//    cell.data = self.userWormholeModel.items[indexPath.row];
//    return cell;
}

#pragma mark - Collection View Delegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    UICollectionViewCell * cell = [collectionView cellForItemAtIndexPath:indexPath];
    WORMHOLE *wormhole = cell.data;
    if (self.wormholeAction) {
        self.wormholeAction(wormhole);
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(140, 109);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    return CGSizeZero;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section {
    return 10;
}

@end
