//
//  ProfileHeaderView.h
//  MotherPlanet
//
//  Created by liuyadi on 2018/6/1.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProfileHeaderViewDelegate <NSObject>

- (void)updateCoverPhotoAction;
- (void)editProfileInfoAction;

@end

@interface ProfileHeaderView : UIView

@property (nonatomic, weak) id<ProfileHeaderViewDelegate> delegate;

@end
