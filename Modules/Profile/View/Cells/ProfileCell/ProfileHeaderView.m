//
//  ProfileHeaderView.m
//  MotherPlanet
//
//  Created by liuyadi on 2018/6/1.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "ProfileHeaderView.h"

@interface ProfileHeaderView ()

@property (weak, nonatomic) IBOutlet UIView *avatarView;
@property (weak, nonatomic) IBOutlet UIImageView *avatarImage;
@property (weak, nonatomic) IBOutlet UIButton *updateCoverPhotoButton;
@property (weak, nonatomic) IBOutlet UIImageView *coverPhotoImage;

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *genderImageView;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UIImageView *locationImage;
@property (weak, nonatomic) IBOutlet UILabel *constellationLabel;
@property (weak, nonatomic) IBOutlet UIImageView *constellationImage;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *ageImage;
@property (weak, nonatomic) IBOutlet UILabel *introduceLabel;
@property (weak, nonatomic) IBOutlet UIButton *editInfoButton;

@end

@implementation ProfileHeaderView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.avatarView.layer.masksToBounds = YES;
    self.avatarImage.layer.masksToBounds = YES;
    self.avatarView.layer.cornerRadius = 38;
    self.avatarImage.layer.cornerRadius = 35;
    
    self.genderImageView.hidden = YES;
    self.locationImage.hidden = self.constellationImage.hidden = self.ageImage.hidden = YES;
}

- (void)dataDidChange {
    USER *user = self.data;
    
    [self.coverPhotoImage sd_cancelCurrentImageLoad];
    self.coverPhotoImage.image = [AppTheme placeholderImage];
    [self.coverPhotoImage setImageWithPhoto:user.cover_photo];
    [self.avatarImage setImageWithPhoto:user.avatar placeholderImage:[AppTheme avatarDefaultImage]];
    
    self.nameLabel.text = user.name ?: [AppTheme defaultPlaceholder];
    self.genderImageView.hidden = !user.gender;
    if (user.gender == USER_GENDER_MALE) {
        self.genderImageView.image = [UIImage imageNamed:@"icon_pro_sex_man"];
    } else {
        self.genderImageView.image = [UIImage imageNamed:@"icon_pro_sex_woman"];
    }
    
    self.locationImage.hidden = !user.location;
    self.locationLabel.text = user.location ?: [AppTheme defaultPlaceholder];
    
    self.constellationImage.hidden = !user.constellation;
    self.constellationLabel.text = user.constellation ?: [AppTheme defaultPlaceholder];
    self.constellationImage.image = [AppTheme imageOfConstellation:user.constellation];
    
    self.ageImage.hidden = !user.birthday;
    self.ageLabel.text = [self titleOfAgeLabel:user.birthday];
    
    self.introduceLabel.text = user.introduce ?: [AppTheme defaultPlaceholder];
}

- (IBAction)updateCoverPhotoAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(updateCoverPhotoAction)]) {
        [self.delegate updateCoverPhotoAction];
    }
}

- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event {
    UIView *result = [super hitTest:point withEvent:event];
    if ([result isKindOfClass:[UIButton class]]) {
        return result;
    } else if (result.superview == self) {
        return nil;
    } else {
        return result;
    }
}

- (IBAction)editInfoAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(editProfileInfoAction)]) {
        [self.delegate editProfileInfoAction];
    }
}

- (NSString *)titleOfAgeLabel:(NSString *)birthday {
    if (birthday && ![birthday isEqualToString:@""] && birthday.length >= 3) {
        NSString *stringRange2 = [birthday substringWithRange:NSMakeRange(2, 1)];
        return [NSString stringWithFormat:@"%@0后", stringRange2];
    } else {
        return [AppTheme defaultPlaceholder];
    }
}

- (BOOL)proPage:(NSString *)birthday compare:(NSString *)date {
    int result = [birthday compare:date options:NSCaseInsensitiveSearch | NSNumericSearch];
    return result == NSOrderedAscending;
}

@end
