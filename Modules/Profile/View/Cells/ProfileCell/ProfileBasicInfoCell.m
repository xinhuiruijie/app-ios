//
//  ProfileBasicInfoCell.m
//  MotherPlanet
//
//  Created by liuyadi on 2018/6/1.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "ProfileBasicInfoCell.h"

@interface ProfileBasicInfoCell ()

@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UIImageView *genderImageView;
@property (weak, nonatomic) IBOutlet UILabel *locationLabel;
@property (weak, nonatomic) IBOutlet UIImageView *locationImage;
@property (weak, nonatomic) IBOutlet UILabel *constellationLabel;
@property (weak, nonatomic) IBOutlet UIImageView *constellationImage;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *ageImage;
@property (weak, nonatomic) IBOutlet UILabel *introduceLabel;
@property (weak, nonatomic) IBOutlet UIButton *editInfoButton;

@end

@implementation ProfileBasicInfoCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.editInfoButton.layer.cornerRadius = 4;
    self.editInfoButton.layer.masksToBounds = YES;
    
    self.locationImage.hidden = self.constellationImage.hidden = self.ageImage.hidden = YES;
}

- (void)dataDidChange {
    if ([self.data isKindOfClass:[USER class]]) {
        USER *user = self.data;
        self.nameLabel.text = user.name ?: [AppTheme defaultPlaceholder];
        if (user.gender == USER_GENDER_MALE) {
            self.genderImageView.image = [UIImage imageNamed:@"icon_man_sel"];
        } else {
            self.genderImageView.image = [UIImage imageNamed:@"icon_woman_sel"];
        }
        
        self.locationImage.hidden = !user.location;
        self.locationLabel.text = user.location ?: [AppTheme defaultPlaceholder];
        
        self.constellationImage.hidden = !user.constellation;
        self.constellationLabel.text = user.constellation ?: [AppTheme defaultPlaceholder];
        self.constellationImage.image = [AppTheme imageOfConstellation:user.constellation];
        
        self.ageImage.hidden = !user.birthday;
        self.ageLabel.text = [self titleOfAgeLabel:user.birthday];
        
        self.introduceLabel.text = user.introduce ?: [AppTheme defaultPlaceholder];
    }
}

#pragma mark - IBAction

- (IBAction)editInfoAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(editProfileInfoAction)]) {
        [self.delegate editProfileInfoAction];
    }
}

- (NSString *)titleOfAgeLabel:(NSString *)birthday {
    if (birthday && ![birthday isEqualToString:@""] && birthday.length >= 3) {
        NSString *stringRange2 = [birthday substringWithRange:NSMakeRange(2, 1)];
        return [NSString stringWithFormat:@"%@0后", stringRange2];
    } else {
        return [AppTheme defaultPlaceholder];
    }
}

- (BOOL)proPage:(NSString *)birthday compare:(NSString *)date {
    int result = [birthday compare:date options:NSCaseInsensitiveSearch | NSNumericSearch];
    return result == NSOrderedAscending;
}

@end
