//
//  ProfileOperationCell.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

typedef NS_ENUM(NSUInteger, COUNT_TYPE) {
    COUNT_TYPE_FOLLOW = 1, // 关注
    COUNT_TYPE_COLLECT = 2, // 收藏
    COUNT_TYPE_WATCHVIDEO = 3, // 足迹
    COUNT_TYPE_UNREADMESSAGE = 4, // 消息
};

#import "ProfileOperationCell.h"
#import "MSNumberScrollAnimatedView.h"

@interface ProfileOperationCell ()

@property (weak, nonatomic) IBOutlet UILabel *followCount;
@property (weak, nonatomic) IBOutlet UILabel *collectCount;
@property (weak, nonatomic) IBOutlet UILabel *watchVideoCount;
@property (weak, nonatomic) IBOutlet UILabel *unreadMessageCount;

@property (weak, nonatomic) IBOutlet MSNumberScrollAnimatedView *followCountNumberAnimated;
@property (weak, nonatomic) IBOutlet MSNumberScrollAnimatedView *collectCountNumberAnimated;
@property (weak, nonatomic) IBOutlet MSNumberScrollAnimatedView *watchVideoCountNumberAnimated;
@property (weak, nonatomic) IBOutlet MSNumberScrollAnimatedView *unreadMessageCountNumberAnimated;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *followCountNAWLC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectCountNAWLC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *watchVideoCountNAWLC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *unreadMessageCountNAWLC;

@property (nonatomic, strong) NSNumber *oldFollowCount;
@property (nonatomic, strong) NSNumber *oldCollectCount;
@property (nonatomic, strong) NSNumber *oldWatchVideoCount;
@property (nonatomic, strong) NSNumber *oldUnreadMessageCount;

@end

@implementation ProfileOperationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.oldFollowCount = [NSNumber numberWithInteger:0];
    self.oldCollectCount = [NSNumber numberWithInteger:0];
    self.oldWatchVideoCount = [NSNumber numberWithInteger:0];
    self.oldUnreadMessageCount = [NSNumber numberWithInteger:0];
    
    [self customAnimatedView:self.followCountNumberAnimated];
    [self customAnimatedView:self.collectCountNumberAnimated];
    [self customAnimatedView:self.watchVideoCountNumberAnimated];
    [self customAnimatedView:self.unreadMessageCountNumberAnimated];
}

- (void)layoutSubviews {
    [super layoutSubviews];
}

- (void)dataDidChange {
    USER *user = self.data;
    
    self.oldFollowCount = [self animationWithCount:user.follow_count countType:COUNT_TYPE_FOLLOW];
    self.oldCollectCount = [self animationWithCount:user.collect_count countType:COUNT_TYPE_COLLECT];
    self.oldWatchVideoCount = [self animationWithCount:user.watch_video_count countType:COUNT_TYPE_WATCHVIDEO];
    self.oldUnreadMessageCount = [self animationWithCount:user.unread_message_count countType:COUNT_TYPE_UNREADMESSAGE];
}

- (IBAction)selectedAction:(UIButton *)sender {
    OPERATION_TYPE operationType = sender.tag - 10000;
    if (self.selectBlock) {
        self.selectBlock(operationType);
    }
}

#pragma mark - Custom Number Animated View

- (void)customAnimatedView:(MSNumberScrollAnimatedView *)view {
    view.font = [UIFont fontWithName:@"Gotham-Medium" size:20];
    view.textColor = [AppTheme normalTextColor];
    view.minLength = 1;
    view.isIntager = YES;
    view.number = 0;
    [view startAnimation];
    [view stopAnimation];
}

#pragma mark - Animation Action

- (NSNumber *)animationWithCount:(NSNumber *)count countType:(COUNT_TYPE)countType {
    NSNumber *oldCount;
    NSLayoutConstraint *constraint;
    MSNumberScrollAnimatedView *animatedView;
    
    switch (countType) {
        case COUNT_TYPE_FOLLOW: {
            oldCount = self.oldFollowCount;
            constraint = self.followCountNAWLC;
            animatedView = self.followCountNumberAnimated;
        }
            break;
            
        case COUNT_TYPE_COLLECT: {
            oldCount = self.oldCollectCount;
            constraint = self.collectCountNAWLC;
            animatedView = self.collectCountNumberAnimated;
        }
            break;
            
        case COUNT_TYPE_WATCHVIDEO: {
            oldCount = self.oldWatchVideoCount;
            constraint = self.watchVideoCountNAWLC;
            animatedView = self.watchVideoCountNumberAnimated;
        }
            break;
            
        case COUNT_TYPE_UNREADMESSAGE: {
            oldCount = self.oldUnreadMessageCount;
            constraint = self.unreadMessageCountNAWLC;
            animatedView = self.unreadMessageCountNumberAnimated;
        }
            break;
            
        default:
            break;
    }
    
    if (!count) {
        count = oldCount;
        animatedView.number = count;
    }
    
    if (count != oldCount) {
        NSString *followCountString = [NSString stringWithFormat:@"%@", count?:@"0"];
        constraint.constant = followCountString.length * 13;
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            animatedView.number = count;
            [animatedView startAnimation];
        });
    }
    return count;
}

@end
