//
//  ProfileTimelineCell.h
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/26.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ProfileTimelineCell : UITableViewCell

@property (nonatomic, assign) BOOL isFirstCell;
@property (nonatomic, assign) BOOL isLastCell;
@property (nonatomic, strong) USER *user;

+ (CGFloat)heightForRowWithTimeline:(TIMELINE *)timeline;

@end
