//
//  ProfileTimelineCell.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/26.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "ProfileTimelineCell.h"

@interface ProfileTimelineCell ()

@property (weak, nonatomic) IBOutlet UIView *topLineView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topLineWLC;
@property (weak, nonatomic) IBOutlet UIView *ovalView;
@property (weak, nonatomic) IBOutlet UIView *bottomLineView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomLineWLC;
//@property (weak, nonatomic) IBOutlet UIImageView *headImageView;
@property (weak, nonatomic) IBOutlet UILabel *nicknameLabel;
@property (weak, nonatomic) IBOutlet UILabel *timelineTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *commentLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentLabelTLC;
//@property (weak, nonatomic) IBOutlet NSLayoutConstraint *commentLabelBLC;
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UILabel *starLabel;
@property (weak, nonatomic) IBOutlet UILabel *myStarLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconImageHLC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *iconImageWLC;

@end

@implementation ProfileTimelineCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.topLineWLC.constant = [AppTheme onePixel];
    self.bottomLineWLC.constant = [AppTheme onePixel];
    
    self.ovalView.layer.borderWidth = 2;
//    self.ovalView.layer.borderColor = [[AppTheme wormholeNavigationColor] CGColor];
    self.ovalView.layer.borderColor = [[UIColor blackColor] CGColor];
    self.ovalView.layer.cornerRadius = self.ovalView.bounds.size.width / 2;
    self.ovalView.layer.masksToBounds = YES;
    
//    self.headImageView.layer.cornerRadius = self.headImageView.bounds.size.width / 2;
//    self.headImageView.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

+ (CGFloat)heightForRowWithTimeline:(TIMELINE *)timeline {
    CGFloat rowHeight = 10;
    CGFloat headHeight = 50;
    CGFloat commentHeight = 0;
    if (timeline.content && timeline.content.length) {
//        commentHeight = [AppTheme calculateHeigthWithContent:timeline.content width:kScreenWidth - 60 - 13];
//        commentHeight += 6;
        commentHeight += 22;
    }
    CGFloat lineHeight = 11;
    CGFloat iconHeight = 0;
    if (timeline.type == TIMELINE_TYPE_FOLLOW) {
        iconHeight = kScreenWidth * 100 / 750;
    } else {
        iconHeight = kScreenWidth * 140 / 750;
    }
    iconHeight += 10;
    rowHeight = rowHeight + headHeight + commentHeight + lineHeight + iconHeight;
    return rowHeight;
}

- (void)setIsFirstCell:(BOOL)isFirstCell {
    if (isFirstCell) {
        self.topLineView.hidden = YES;
    } else {
        self.topLineView.hidden = NO;
    }
}

- (void)setIsLastCell:(BOOL)isLastCell {
    if (isLastCell) {
        self.bottomLineView.hidden = YES;
    } else {
        self.bottomLineView.hidden = NO;
    }
}

- (void)dataDidChange {
    if (!self.data || ![self.data isKindOfClass:[TIMELINE class]]) {
        return;
    }
    
//    [self.headImageView setImageWithPhoto:self.user.avatar placeholderImage:[AppTheme avatarDefaultImage]];
    self.nicknameLabel.text = self.user.name;
    
    TIMELINE *timeline = self.data;
    NSString *timelineType = [AppTheme defaultPlaceholder];
    if (timeline.type) {
        timelineType = [self getTimelineType:timeline.type];
    }
    NSString *timelineReference = [AppTheme defaultPlaceholder];
    if (timeline.reference_type && timeline.type != TIMELINE_TYPE_STAR) {
        timelineReference = [self getTimelineReference:timeline.reference_type];
    }
//    self.timelineTypeLabel.text = [NSString stringWithFormat:@"%@%@", timelineType, timelineReference];
    self.timelineTypeLabel.text = timeline.show_string;
    //时间戳转换
    if (timeline.created_at && timeline.created_at.length) {
        NSTimeInterval time = [timeline.created_at doubleValue];
        NSDate *detaildate = [NSDate dateWithTimeIntervalSince1970:time];
        self.timeLabel.text = [detaildate timeAgo];
    }
    
    if (timeline.type == TIMELINE_TYPE_COMMENT) {
        if (timeline.content && timeline.content.length) {
            self.commentLabel.text = timeline.content;
            self.commentLabel.hidden = NO;
            self.commentLabelTLC.constant = 6;
        } else {
            self.commentLabel.text = [AppTheme defaultPlaceholder];
            self.commentLabel.hidden = YES;
            self.commentLabelTLC.constant = 0;
        }
    } else {
        self.commentLabel.text = [AppTheme defaultPlaceholder];
        self.commentLabel.hidden = YES;
        self.commentLabelTLC.constant = 0;
    }
    
    if (timeline.type == TIMELINE_TYPE_FOLLOW) {
        [self.iconImageView setImageWithPhoto:timeline.author.avatar placeholderImage:[AppTheme avatarDefaultImage]];
        self.titleLabel.text = timeline.author.name ?: [AppTheme defaultPlaceholder];
        self.descLabel.text = timeline.author.introduce ?: [AppTheme defaultPlaceholder];
        self.descLabel.numberOfLines = 1;
        self.iconImageHLC.constant = kScreenWidth * 100 / 750;
        self.iconImageWLC.constant = kScreenWidth * 100 / 750;
        self.iconImageView.layer.cornerRadius = kScreenWidth * 50 / 750;
        self.iconImageView.layer.masksToBounds = YES;
    } else {
        if (timeline.video) {
            [self.iconImageView setImageWithPhoto:timeline.video.photo];
            self.titleLabel.text = timeline.video.title ?: [AppTheme defaultPlaceholder];
            self.descLabel.text = timeline.video.subtitle ?: [AppTheme defaultPlaceholder];
        }
        if (timeline.article) {
            [self.iconImageView setImageWithPhoto:timeline.article.photo];
            self.titleLabel.text = timeline.article.title ?: [AppTheme defaultPlaceholder];
            self.descLabel.text = timeline.article.subtitle ?: [AppTheme defaultPlaceholder];
        }
        self.iconImageHLC.constant = kScreenWidth * 140 / 750;
        self.iconImageWLC.constant = kScreenWidth * 200 / 750;
        self.iconImageView.layer.cornerRadius = 0;
        self.iconImageView.layer.masksToBounds = YES;
    }
    
    if (timeline.type == TIMELINE_TYPE_STAR) {
        self.starLabel.text = [NSString stringWithFormat:@"影片总分:%.1f", timeline.video.star];
        self.myStarLabel.text = [NSString stringWithFormat:@"%.1f", timeline.video.my_star];
        
        self.descLabel.hidden = YES;
        self.starLabel.hidden = NO;
        self.myStarLabel.hidden = NO;
    } else {
        self.descLabel.hidden = NO;
        self.starLabel.hidden = YES;
        self.myStarLabel.hidden = YES;
    }
}

- (NSString *)getTimelineType:(TIMELINE_TYPE)type {
    NSString *timelineType = [AppTheme defaultPlaceholder];
    switch (type) {
        case TIMELINE_TYPE_LIKE:
            timelineType = @"赞了";
            break;
        case TIMELINE_TYPE_FOLLOW:
            timelineType = @"关注了";
            break;
        case TIMELINE_TYPE_COMMENT:
            timelineType = @"评论了";
            break;
        case TIMELINE_TYPE_SHARE:
            timelineType = @"分享了";
            break;
        case TIMELINE_TYPE_COLLECT:
            timelineType = @"收藏了";
            break;
        case TIMELINE_TYPE_STAR:
            timelineType = @"对这部短片评分";
            break;
        default:
            break;
    }
    return timelineType;
}

- (NSString *)getTimelineReference:(TIMELINE_REFERENCE_TYPE)reference {
    NSString *timelineReference = [AppTheme defaultPlaceholder];
    switch (reference) {
        case TIMELINE_REFERENCE_TYPE_ARTICLE:
            timelineReference = @"这篇文章";
            break;
        case TIMELINE_REFERENCE_TYPE_VIDEO:
            timelineReference = @"这个短片";
            break;
        case TIMELINE_REFERENCE_TYPE_AUTHOR:
            timelineReference = @"星球";
            break;
        default:
            break;
    }
    return timelineReference;
}

@end
