//
//  DataUsageCell.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "DataUsageCell.h"
#import "PPCounter.h"

@interface DataUsageCell ()
@property (weak, nonatomic) IBOutlet UILabel *totalFlowLabel;
@property (weak, nonatomic) IBOutlet UILabel *useFlowLabel;
@property (weak, nonatomic) IBOutlet UILabel *usePercentLabel;
@property (weak, nonatomic) IBOutlet UIView *useFlowFakeProgress;

@end

@implementation DataUsageCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.useFlowFakeProgress.x = 0;
}

- (void)dataDidChange {
//    DATA_FLOW *dataFlow = self.data;
//    
//    CGFloat usedFlow = dataFlow.used_flow.floatValue;
//    CGFloat totalFlow = dataFlow.total_flow.floatValue;
//    CGFloat progress = totalFlow == 0 ? 0 : (usedFlow / totalFlow);
//    CGFloat usedPercent = progress * 100;
//    
//    [self.useFlowLabel pp_fromNumber:0 toNumber:usedFlow duration:0.8 animationOptions:PPCounterAnimationOptionCurveLinear format:^NSString *(CGFloat number) {
//        return [NSString stringWithFormat:@"%.0fM", number];
//    } completion:nil];
//    [self.usePercentLabel pp_fromNumber:0 toNumber:usedPercent duration:0.8 animationOptions:PPCounterAnimationOptionCurveLinear format:^NSString *(CGFloat number) {
//        return [NSString stringWithFormat:@"%.0f%%", number];
//    } completion:nil];
//    self.totalFlowLabel.text = [NSString stringWithFormat:@"%@M", dataFlow.total_flow ? : @(0)];
//    
//    CGFloat totalFlowProgressWidth = kScreenWidth - 20;
//    CGFloat useFlowProgressWidth = totalFlow == 0 ? 0 : ceil(totalFlowProgressWidth * usedFlow / totalFlow);
//    self.useFlowFakeProgress.width = useFlowProgressWidth;
//    self.useFlowFakeProgress.x = 0;
//    
//    CATransform3D scale = CATransform3DMakeScale(1, 1, 1);
//    self.useFlowFakeProgress.layer.transform = CATransform3DMakeScale(0, 1, 1);
//    self.useFlowFakeProgress.layer.anchorPoint = CGPointMake(0, 0.5);
//    [UIView animateWithDuration:0.8 delay:0 options:UIViewAnimationOptionCurveLinear animations:^{
//        self.useFlowFakeProgress.layer.transform = scale;
//    } completion:nil];
}

@end
