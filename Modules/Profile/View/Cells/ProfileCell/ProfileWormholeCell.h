//
//  ProfileWormholeCell.h
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/16.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, WORMHOLE_USER_TYPE) {
    WORMHOLE_USER_TYPE_MY,
    WORMHOLE_USER_TYPE_TA,
};

@interface ProfileWormholeCell : UITableViewCell

@property (nonatomic, copy) void(^wormholeAction)(WORMHOLE *wormhole);

@property (nonatomic, copy) USER *user;

@property (nonatomic, assign) WORMHOLE_USER_TYPE wormholeUser;

@end
