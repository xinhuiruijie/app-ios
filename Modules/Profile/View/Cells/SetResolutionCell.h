//
//  SetResolutionCell.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/17.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SetResolutionCell : UITableViewCell

@property (nonatomic, assign) VIDEO_QUALITY quality;

@end
