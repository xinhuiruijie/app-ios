//
//  MyCollectionCell.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/23.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "MyCollectionCell.h"
#import "MyCollectVideoCell.h"
#import "MyCollectTopicCell.h"
#import "MyLikeVideoListModel.h"
#import "MyLikeTopicListModel.h"
#import "EnumListModel.h"
#import "MyCollectHeaderView.h"
#import "VideoInfoController.h"
#import "TopicVideoListController.h"

@interface MyCollectionCell () <UITableViewDataSource, UITableViewDelegate, MyCollectHeaderViewDelegate>

@property (nonatomic, weak) UITableView *tableView;
@property (nonatomic, strong) MyLikeVideoListModel *mylikeVideoListModel;
@property (nonatomic, strong) MyLikeTopicListModel *mylikeTopicListModel;
@property (nonatomic, strong) MyCollectHeaderView *headerView;
@property (nonatomic, strong) EnumListModel *model;
@property (nonatomic, strong) NSMutableArray *deleteVideoArray;
@property (nonatomic, strong) NSMutableArray *deleteTopicArray;

@end

@implementation MyCollectionCell

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self setupSubview];
        [self modelInit];
        self.deleteTopicArray = [NSMutableArray array];
        self.deleteVideoArray = [NSMutableArray array];
    }
    return self;
}

- (void)setupSubview {
    UITableView *tableView = [[UITableView alloc] initWithFrame:self.bounds];
    [self addSubview:tableView];
    self.tableView = tableView;
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.tableFooterView = [UIView new];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[MyCollectVideoCell nib] forCellReuseIdentifier:@"MyCollectVideoCell"];
    [self.tableView registerNib:[MyCollectTopicCell nib] forCellReuseIdentifier:@"MyCollectTopicCell"];
    [self.tableView registerNib:[EmptyTableCell nib] forCellReuseIdentifier:@"EmptyTableCell"];
    
    self.headerView = [MyCollectHeaderView loadFromNib];
    self.headerView.delegate = self;
}

- (void)modelInit {
    self.mylikeVideoListModel = [[MyLikeVideoListModel alloc] init];
    @weakify(self)
    self.mylikeVideoListModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        if (self.model.enumType.integerValue == COLLECT_TYPE_VIDEO) {
            if (error == nil) {
                [self setupRefreshFooterView];
                if ([self.delegate respondsToSelector:@selector(noticeSuperVideoState:)]) {
                    if (self.mylikeVideoListModel.items.count) {
                        [self.delegate noticeSuperVideoState:YES];
                    } else {
                        [self.delegate noticeSuperVideoState:NO];
                    }
                }
                
                if (self.mylikeVideoListModel.more) {
                    [self.tableView.footer endRefreshing];
                } else {
                    [self.tableView.footer noticeNoMoreData];
                }
            } else {
                [self presentMessage:error.message withTips:@"加载数据失败"];
            }
            [self.tableView reloadData];
        }
    };
    self.mylikeTopicListModel = [[MyLikeTopicListModel alloc] init];
    self.mylikeTopicListModel.whenUpdated = ^(STIHTTPResponseError *error) {
        if (self.model.enumType.integerValue == COLLECT_TYPE_TOPIC) {
            @strongify(self)
            if (error == nil) {
                [self setupRefreshFooterView];
                if ([self.delegate respondsToSelector:@selector(noticeSuperTopicState:)]) {
                    if (self.mylikeTopicListModel.items.count) {
                        [self.delegate noticeSuperTopicState:YES];
                    } else {
                        [self.delegate noticeSuperTopicState:NO];
                    }
                }
                if (self.mylikeTopicListModel.more) {
                    [self.tableView.footer endRefreshing];
                } else {
                    [self.tableView.footer noticeNoMoreData];
                }
            } else {
                [self presentMessage:error.message withTips:@"加载数据失败"];
            }
            [self.tableView reloadData];
        }
    };
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.tableView.frame = self.bounds;
}

- (void)setupRefreshFooterView {
    if (self.model.enumType.integerValue == COLLECT_TYPE_VIDEO) {
        if (self.tableView.footer == nil && !self.mylikeVideoListModel.isEmpty) {
            @weakify(self)
            [self.tableView addFooterPullLoader:^{
                @strongify(self)
                [self.mylikeVideoListModel loadMore];
            }];
        } else {
            if (self.mylikeVideoListModel.isEmpty) {
                [self.tableView removeFooter];
            }
        }
    } else {
        if (self.tableView.footer == nil && !self.mylikeTopicListModel.isEmpty) {
            @weakify(self)
            [self.tableView addFooterPullLoader:^{
                @strongify(self)
                [self.mylikeTopicListModel loadMore];
            }];
        } else {
            if (self.mylikeTopicListModel.isEmpty) {
                [self.tableView removeFooter];
            }
        }
    }
}

- (BOOL)deleteTopicArrayContainData:(NSString *)dataId {
    if ([self.deleteTopicArray containsObject:dataId]) {
        return YES;
    } else {
        return NO;
    }
}

- (BOOL)deleteVideoArrayContainData:(NSString *)dataId {
    if ([self.deleteVideoArray containsObject:dataId]) {
        return YES;
    } else {
        return NO;
    }
}

- (void)dataDidChange {
    if (!self.data || ![self.data isKindOfClass:[EnumListModel class]]) {
        return;
    }
    self.model = self.data;
    if (self.model.enumType.integerValue == COLLECT_TYPE_VIDEO) {
        [self.mylikeVideoListModel refresh];
        [self.mylikeVideoListModel loadCache];
        if (self.mylikeVideoListModel.isEmpty) {
            self.mylikeVideoListModel.loaded = NO;
        }
        [self.tableView removeFooter];
    } else {
        [self.mylikeTopicListModel refresh];
        [self.mylikeTopicListModel loadCache];
        if (self.mylikeTopicListModel.isEmpty) {
            self.mylikeTopicListModel.loaded = NO;
        }
        [self.tableView removeFooter];
    }
    [self.tableView reloadData];
}

- (void)setIsEditState:(BOOL)isEditState {
    _isEditState = isEditState;
    if (!isEditState) {
        self.headerView.isAllSelect = NO;
        if (self.model.enumType.integerValue == COLLECT_TYPE_VIDEO) {
            if (self.deleteVideoArray.count > 0) {
                [self.deleteVideoArray removeAllObjects];
            }
        } else {
            if (self.deleteTopicArray.count > 0) {
                [self.deleteTopicArray removeAllObjects];
            }
        }
    }
}

/**
 * 单选视频
 */
- (void)selectVideoWithID:(NSString *)videoID isSelect:(BOOL)isSelect {
    if (isSelect) {
        [self.deleteVideoArray addObject:videoID];
        if (self.deleteVideoArray.count == self.mylikeVideoListModel.items.count) {
            self.headerView.isAllSelect = YES;
        }
        if ([self.delegate respondsToSelector:@selector(deleteItemsWithArray:type:)]) {
            [self.delegate deleteItemsWithArray:self.deleteVideoArray type:COLLECT_TYPE_VIDEO];
        }
    } else {
        if (self.deleteVideoArray.count > 0) {
            [self.deleteVideoArray removeObject:videoID];
            if (self.deleteVideoArray.count != self.mylikeVideoListModel.items.count) {
                self.headerView.isAllSelect = NO;
            }
        }
        if ([self.delegate respondsToSelector:@selector(deleteItemsWithArray:type:)]) {
            [self.delegate deleteItemsWithArray:self.deleteVideoArray type:COLLECT_TYPE_VIDEO];
        }
    }
    [self.tableView reloadData];
}

/**
 * 单选专题
 */
- (void)selectTopicWithID:(NSString *)topicID isSelect:(BOOL)isSelect {
    if (isSelect) {
        [self.deleteTopicArray addObject:topicID];
        if (self.deleteTopicArray.count == self.mylikeTopicListModel.items.count) {
            self.headerView.isAllSelect = YES;
        }
        if ([self.delegate respondsToSelector:@selector(deleteItemsWithArray:type:)]) {
            [self.delegate deleteItemsWithArray:self.deleteTopicArray type:COLLECT_TYPE_TOPIC];
        }
    } else {
        if (self.deleteTopicArray.count > 0) {
            [self.deleteTopicArray removeObject:topicID];
            if (self.deleteTopicArray.count != self.mylikeTopicListModel.items.count) {
                self.headerView.isAllSelect = NO;
            }
        }
        if ([self.delegate respondsToSelector:@selector(deleteItemsWithArray:type:)]) {
            [self.delegate deleteItemsWithArray:self.deleteTopicArray type:COLLECT_TYPE_TOPIC];
        }
    }
}

#pragma mark - MyCollectHeaderViewDelegate

- (void)selectAllItemIsCancel:(BOOL)isCancel {
    if (isCancel == NO) {
        if (self.model.enumType.integerValue == COLLECT_TYPE_VIDEO) {
            for (VIDEO *video in self.mylikeVideoListModel.items) {
                [self.deleteVideoArray addObject:video.id];
            }
            if ([self.delegate respondsToSelector:@selector(deleteItemsWithArray:type:)]) {
                [self.delegate deleteItemsWithArray:self.deleteVideoArray type:COLLECT_TYPE_VIDEO];
            }
        } else {
            for (TOPIC *topic in self.mylikeTopicListModel.items) {
                [self.deleteTopicArray addObject:topic.id];
            }
            if ([self.delegate respondsToSelector:@selector(deleteItemsWithArray:type:)]) {
                [self.delegate deleteItemsWithArray:self.deleteTopicArray type:COLLECT_TYPE_TOPIC];
            }
        }
    } else {
        if (self.model.enumType.integerValue == COLLECT_TYPE_VIDEO) {
            if (self.deleteVideoArray.count > 0) {
                [self.deleteVideoArray removeAllObjects];
            }
            if ([self.delegate respondsToSelector:@selector(deleteItemsWithArray:type:)]) {
                [self.delegate deleteItemsWithArray:self.deleteTopicArray type:COLLECT_TYPE_TOPIC];
            }
        } else {
            if (self.deleteTopicArray.count > 0) {
                [self.deleteTopicArray removeAllObjects];
            }
            if ([self.delegate respondsToSelector:@selector(deleteItemsWithArray:type:)]) {
                [self.delegate deleteItemsWithArray:self.deleteTopicArray type:COLLECT_TYPE_TOPIC];
            }
        }
    }
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource;

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.model.enumType.integerValue == COLLECT_TYPE_VIDEO) {
        if (self.mylikeVideoListModel.items.count) {
            return self.mylikeVideoListModel.items.count;
        } else {
            return 1;
        }
    } else {
        if (self.mylikeTopicListModel.items.count) {
            return self.mylikeTopicListModel.items.count;
        } else {
            return 1;
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.model.enumType.integerValue == COLLECT_TYPE_VIDEO) {
        if (self.mylikeVideoListModel.items.count) {
            MyCollectVideoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyCollectVideoCell" forIndexPath:indexPath];
            cell.data = self.mylikeVideoListModel.items[indexPath.row];
            cell.isEditState = self.isEditState;
            VIDEO *video = self.mylikeVideoListModel.items[indexPath.row];
            cell.isSelect = [self deleteVideoArrayContainData:video.id];
            return cell;
        } else {
            EmptyTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EmptyTableCell" forIndexPath:indexPath];
            return cell;
        }
    } else {
        if (self.mylikeTopicListModel.items.count) {
            MyCollectTopicCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyCollectTopicCell" forIndexPath:indexPath];
            cell.data = self.mylikeTopicListModel.items[indexPath.row];
            cell.isEditState = self.isEditState;
            TOPIC *topic = self.mylikeTopicListModel.items[indexPath.row];
            cell.isSelect = [self deleteTopicArrayContainData:topic.id];
            return cell;
        } else {
            EmptyTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EmptyTableCell" forIndexPath:indexPath];
            return cell;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat height = kScreenWidth * 180 / 750 > 90 ? kScreenWidth * 180 / 750 : 90;
    if (self.model.enumType.integerValue == COLLECT_TYPE_VIDEO) {
        if (self.mylikeVideoListModel.items.count) {
            return height;
        } else {
            return self.tableView.bounds.size.height;
        }
    } else {
        if (self.mylikeTopicListModel.items.count) {
            return height;
        } else {
            return self.tableView.bounds.size.height;
        }
    }
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (self.isEditState) {
        if (self.model.enumType.integerValue == COLLECT_TYPE_VIDEO) {
            if (self.mylikeVideoListModel.items.count > 0) {
                return 35;
            } else {
                return CGFLOAT_MIN;
            }
        } else {
            if (self.mylikeTopicListModel.items.count > 0) {
                return 35;
            } else {
                return CGFLOAT_MIN;
            }
        }
    } else {
        return CGFLOAT_MIN;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (self.isEditState) {
        if (self.model.enumType.integerValue == COLLECT_TYPE_VIDEO) {
            if (self.mylikeVideoListModel.items.count > 0) {
                return self.headerView;
            } else {
                return nil;
            }
        } else {
            if (self.mylikeTopicListModel.items.count > 0) {
                return self.headerView;
            } else {
                return nil;
            }
        }
    } else {
        return nil;
    }
}

#pragma mark - UITableViewDelegate;

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.isEditState) {
        if (self.model.enumType.integerValue == COLLECT_TYPE_VIDEO) {
            VIDEO *video = self.mylikeVideoListModel.items[indexPath.row];
            MyCollectVideoCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.isSelect = !cell.isSelect;
            [self selectVideoWithID:video.id isSelect:cell.isSelect];
        } else {
            TOPIC *topic = self.mylikeTopicListModel.items[indexPath.row];
            MyCollectTopicCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.isSelect = !cell.isSelect;
            [self selectTopicWithID:topic.id isSelect:cell.isSelect];
        }
    } else {
        if (self.model.enumType.integerValue == COLLECT_TYPE_VIDEO) {
            if (self.mylikeVideoListModel.items.count) {
                VIDEO *video = self.mylikeVideoListModel.items[indexPath.row];
                VideoInfoController *videoInfo = [VideoInfoController spawn];
                videoInfo.video = video;
                [[self topViewController] presentNavigationController:videoInfo];
            }
        } else {
            if (self.mylikeTopicListModel.items.count) {
                TOPIC *topic = self.mylikeTopicListModel.items[indexPath.row];
                TopicVideoListController *topicInfo = [TopicVideoListController spawn];
                topicInfo.topic = topic;
                [[MPRootViewController sharedInstance].currentViewController.navigationController pushViewController:topicInfo animated:YES];
            }
        }
    }
}


@end
