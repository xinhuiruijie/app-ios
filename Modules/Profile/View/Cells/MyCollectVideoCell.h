//
//  MyCollectVideoCell.h
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/24.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MyCollectVideoCell : UITableViewCell

@property (nonatomic, assign) BOOL isEditState;
@property (nonatomic, assign) BOOL isSelect;

@end
