//
//  BlackListCell.m
//  MotherPlanet
//
//  Created by 陈熙 on 2018/3/1.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "BlackListCell.h"

@interface BlackListCell ()
@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *nicknameLabel;
@property (weak, nonatomic) IBOutlet UILabel *tipLabel;

@end

@implementation BlackListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.iconImageView.layer.cornerRadius = 18;
    self.iconImageView.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)dataDidChange {
    if (![self.data isKindOfClass:[USER class]]) {
        return;
    }
    USER *user = self.data;
    [self.iconImageView setImageWithPhoto:user.avatar placeholderImage:[AppTheme avatarDefaultImage]];
    self.nicknameLabel.text = user.name ?: [AppTheme defaultPlaceholder];
    self.tipLabel.text = user.introduce ?: [AppTheme defaultPlaceholder];
}

@end
