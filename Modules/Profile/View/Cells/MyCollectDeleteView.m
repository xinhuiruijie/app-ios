//
//  MyCollectDeleteView.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/25.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "MyCollectDeleteView.h"

@interface MyCollectDeleteView ()

@end

@implementation MyCollectDeleteView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.deleteButton.layer.cornerRadius = 15;
    self.deleteButton.layer.masksToBounds = YES;
    self.deleteButton.layer.borderWidth = [AppTheme onePixel];
    self.deleteButton.layer.borderColor = [UIColor colorWithRGBValue:0xFF1717].CGColor;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.frame = CGRectMake(0, 0, kScreenWidth, 56);
}

- (IBAction)deleteAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(deleteCollectItems)]) {
        [self.delegate deleteCollectItems];
    }
}

@end
