//
//  WatchLaterListCell.h
//  MotherPlanet
//
//  Created by liuyadi on 2018/4/25.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WatchLaterListCell : UITableViewCell

@property (nonatomic, assign) BOOL isEdit;
@property (nonatomic, assign) BOOL isSelected;

@property (nonatomic, copy) void(^isSelectedAction)(BOOL selected);

@end
