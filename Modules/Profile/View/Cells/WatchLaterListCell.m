//
//  WatchLaterListCell.m
//  MotherPlanet
//
//  Created by liuyadi on 2018/4/25.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "WatchLaterListCell.h"

@interface WatchLaterListCell ()
@property (weak, nonatomic) IBOutlet UIImageView *videoPhoto;
@property (weak, nonatomic) IBOutlet UILabel *videoTitleLabel;
@property (weak, nonatomic) IBOutlet UILabel *videoTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *videoPlaycountLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *selectedViewWLC;
@property (weak, nonatomic) IBOutlet UIButton *selectedButton;
@property (weak, nonatomic) IBOutlet UIImageView *selectedImage;

@end

@implementation WatchLaterListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)dataDidChange {
    VIDEO *video = self.data;
    
    self.videoTitleLabel.text = video.title?:@"";
    
    [self.videoPhoto setImageWithPhoto:video.photo placeholderImage:[AppTheme placeholderImage]];
    float videoPlayCount = [video.play_count floatValue];
    if (videoPlayCount >= 10000) {
        self.videoPlaycountLabel.text = [NSString stringWithFormat:@"%0.1fW", videoPlayCount / 10000];
    } else {
        self.videoPlaycountLabel.text = [NSString stringWithFormat:@"%@", video.play_count?:@(0)];
    }
    self.videoTimeLabel.text = video.video_time?:@"";
}

- (void)setIsEdit:(BOOL)isEdit {
    self.selectedButton.selected = NO;
    self.selectedImage.highlighted = NO;
    if (isEdit) {
        self.selectedViewWLC.constant = 43;
    } else {
        self.selectedViewWLC.constant = 15;
    }
}

- (void)setIsSelected:(BOOL)isSelected {
    if (isSelected) {
        self.selectedButton.selected = isSelected;
        self.selectedImage.highlighted = isSelected;
    } else {
        self.selectedButton.selected = isSelected;
        self.selectedImage.highlighted = isSelected;
    }
}

- (IBAction)selectedAction:(UIButton *)sender {
    sender.selected = !sender.isSelected;
    self.selectedImage.highlighted = sender.isSelected;
    if (self.isSelectedAction) {
        self.isSelectedAction(sender.selected);
    }
}

@end
