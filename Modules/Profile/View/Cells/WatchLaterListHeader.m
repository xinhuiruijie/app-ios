//
//  WatchLaterListHeader.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/5/30.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "WatchLaterListHeader.h"

@interface WatchLaterListHeader ()

@property (nonatomic, strong) UIButton *selectedButton;
@property (nonatomic, strong) UIImageView *selectedImage;

@end

@implementation WatchLaterListHeader

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor whiteColor];
        
        self.selectedButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.selectedButton.frame = CGRectMake(0, 0, 42, frame.size.height);
        [self.selectedButton addTarget:self action:@selector(selectAllAction:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.selectedButton];
        
        self.selectedImage = [[UIImageView alloc] initWithFrame:CGRectMake(15, 17, 15, 15)];
        self.selectedImage.image = [UIImage imageNamed:@"icon_box_nor"];
        [self addSubview:self.selectedImage];
        
        UILabel *selectedLabel = [[UILabel alloc] initWithFrame:CGRectMake(42, 15, 50, 14)];
        selectedLabel.text = @"全选";
        selectedLabel.textColor = [AppTheme normalTextColor];
        selectedLabel.font = [UIFont systemFontOfSize:14];
        [self addSubview:selectedLabel];
    }
    return self;
}

- (void)setIsSelectedAll:(BOOL)isSelectedAll {
    self.selectedButton.selected = isSelectedAll;
    if (isSelectedAll) {
        self.selectedImage.image = [UIImage imageNamed:@"icon_box_sel"];
    } else {
        self.selectedImage.image = [UIImage imageNamed:@"icon_box_nor"];
    }
}

- (void)selectAllAction:(UIButton *)sender {
    sender.selected = !sender.selected;
    if (sender.selected) {
        self.selectedImage.image = [UIImage imageNamed:@"icon_box_sel"];
    } else {
        self.selectedImage.image = [UIImage imageNamed:@"icon_box_nor"];
    }
    if (self.selectedAll) {
        self.selectedAll(sender.selected);
    }
}

@end
