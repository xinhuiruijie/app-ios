//
//  WatchLaterListController.m
//  MotherPlanet
//
//  Created by liuyadi on 2018/4/25.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "WatchLaterListController.h"
#import "VideoInfoController.h"
#import "WatchLaterModel.h"
#import "WatchLaterListCell.h"
#import "WatchLaterListHeader.h"
#import "WatchLaterListFooter.h"

@interface WatchLaterListController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet WatchLaterListFooter *footerView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *footerViewHLC;
@property (nonatomic, strong) UIButton *editButton;
@property (nonatomic, strong) WatchLaterListHeader *tableHeaderView;
@property (nonatomic, strong) WatchLaterListFooter *tableFooterView;

@property (nonatomic, strong) WatchLaterModel *model;

@property (nonatomic, strong) NSMutableArray *deleteVideosData;
@property (nonatomic, assign) BOOL isSelectAll;

@end

@implementation WatchLaterListController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Profile"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.navigationItem.title = @"稍后观看";
    @weakify(self)
    self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    self.model = [[WatchLaterModel alloc] init];
    self.deleteVideosData = [NSMutableArray array];
    
    if (self.model.watchLaterList.count > 0) {
        [self customize];
    }
    
    [self.tableView registerNib:[WatchLaterListCell nib] forCellReuseIdentifier:@"WatchLaterListCell"];
    [self.tableView registerNib:[EmptyTableCell nib] forCellReuseIdentifier:@"EmptyTableCell"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [self.tableView reloadData];
}

#pragma mark - CustomMethod

- (void)customize {
    [self.navigationController enabledMLBlackTransition:NO];
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.frame = CGRectMake(0, 0, 40, 40);
    self.editButton = button;
    [button setTitle:@"编辑" forState:UIControlStateNormal];
    [button setTitle:@"" forState:UIControlStateSelected];
    [button setTitleColor:[AppTheme verifyCodeButtonColor] forState:UIControlStateNormal];
    [button setTitleColor:[AppTheme verifyCodeButtonColor] forState:UIControlStateSelected];
    button.titleLabel.font = [UIFont systemFontOfSize:14];
    [button addTarget:self action:@selector(openEditState) forControlEvents:UIControlEventTouchUpInside];
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
}

- (void)openEditState {
    [self.deleteVideosData removeAllObjects];
    self.editButton.selected = !self.editButton.isSelected;
    self.tableHeaderView = nil;
    if (!self.tableFooterView) {
        [self.footerView addSubview:[self setFooterView]];
    }
    if (self.editButton.isSelected) {
        self.footerViewHLC.constant = 50;
    } else {
        self.footerViewHLC.constant = 0;
    }
    [self.tableView reloadData];
    
    if (self.model.watchLaterList.count == 0) {
        self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:[UIView new]];
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([self isWatchLaterListEmpty]) {
        return 1;
    } else {
        return self.model.watchLaterList.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self isWatchLaterListEmpty]) {
        EmptyTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EmptyTableCell" forIndexPath:indexPath];
        return cell;
    } else {
        WatchLaterListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"WatchLaterListCell" forIndexPath:indexPath];
        cell.data = self.model.watchLaterList[indexPath.row];
        cell.isEdit = self.editButton.isSelected;
        
        // 全选都被选中，否则根据数据源判断是否选中
        cell.isSelected = NO;
        if (self.isSelectAll) {
            cell.isSelected = self.isSelectAll;
        } else {
            for (VIDEO *video in self.deleteVideosData) {
                if (video == self.model.watchLaterList[indexPath.row]) {
                    cell.isSelected = YES;
                }
            }
        }
        
        @weakify(cell)
        cell.isSelectedAction = ^(BOOL selected) {
            @strongify(cell)
            VIDEO *video = cell.data;
            if (selected) {
                // 选择 添加对应数据源 删除 可点击
                [self.deleteVideosData addObject:video];
                if (self.deleteVideosData.count == self.model.watchLaterList.count) {
                    self.tableHeaderView.isSelectedAll = YES;
                }
                self.tableFooterView.isHighlighted = NO;
            } else {
                // 取消选择 删除对应数据源 为零 删除 不可点击
                for (NSInteger count = 0; count < self.deleteVideosData.count; count++) {
                    if (video == self.deleteVideosData[count]) {
                        [self.deleteVideosData removeObjectAtIndex:count];
                        self.tableHeaderView.isSelectedAll = NO;
                        break;
                    }
                }
                if (self.deleteVideosData.count == 0) {
                    self.tableFooterView.isHighlighted = YES;
                }
            }
            
        };
        return cell;
    }
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self isWatchLaterListEmpty]) {
        return tableView.height;
    } else {
        return ceil(tableView.width * 114 / 375);
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([self isWatchLaterListEmpty]) {
        return;
    }
    VIDEO *video = self.model.watchLaterList[indexPath.row];
    [self.model removeVideo:video];
    
    VideoInfoController *videoInfoController = [VideoInfoController spawn];
    videoInfoController.video = video;
//    [self presentNavigationController:videoInfoController];
    videoInfoController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:videoInfoController animated:YES];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (self.editButton.selected) {
        return [self setHeaderView];
    } else {
        return [UIView new];
    }
}

//- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
//    if (self.editButton.selected) {
//        return [self setFooterView];
//    } else {
//        return [UIView new];
//    }
//}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (self.editButton.selected) {
        return 34;
    } else {
        return CGFLOAT_MIN;
    }
}

//- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
//    if (self.editButton.selected) {
//        return 50;
//    } else {
//        return CGFLOAT_MIN;
//    }
//}

- (BOOL)isWatchLaterListEmpty {
    return self.model.watchLaterList.count <= 0;
}

#pragma mark - Header & Footer View

- (UIView *)setHeaderView {
    self.tableHeaderView = [[WatchLaterListHeader alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 34)];
    @weakify(self)
    self.tableHeaderView.selectedAll = ^(BOOL selectAll) {
        @strongify(self)
        
        if (selectAll) {
            // 全选添加
            [self.deleteVideosData removeAllObjects];
            for (VIDEO *video in self.model.watchLaterList) {
                [self.deleteVideosData addObject:video];
            }
        } else {
            // 全选删除
            [self.deleteVideosData removeAllObjects];
        }
        
        // 强行获取一波，有待优化
        for (NSInteger row = 0; row <= self.model.watchLaterList.count - 1; row++) {
            NSIndexPath *path=[NSIndexPath indexPathForRow:row inSection:0];
            WatchLaterListCell *cell = [self.tableView cellForRowAtIndexPath:path];
            cell.isSelected = selectAll;
        }
        self.isSelectAll = selectAll;
        
        self.tableFooterView.isHighlighted = !selectAll;
    };
    return self.tableHeaderView;
}

- (WatchLaterListFooter *)setFooterView {
    self.tableFooterView = [[WatchLaterListFooter alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, 50)];
    @weakify(self)
    self.tableFooterView.deleteAction = ^{
        @strongify(self)
        MPAlertView *alertView = [MPAlertView alertWithTitle:@"删除短片" message:@"确定删除所选短片?" cancelButton:@"取消" confirmButton:@"确定"];
        alertView.confirmAction = ^{
            [self deleteVideos];
        };
        [alertView show];
        
    };
    self.tableFooterView.cancelAction = ^{
        @strongify(self)
        [self openEditState];
    };
    return self.tableFooterView;
}

- (void)deleteVideos {
    // 删除数据总数 永远<= 总数据数量
    if (self.deleteVideosData.count == self.model.watchLaterList.count) {
        [self.model removeAllVideos];
    } else {
        if (self.deleteVideosData.count > 0) {
            for (VIDEO *video in self.deleteVideosData) {
                [self.model removeVideo:video];
            }
        }
    }
    [self.view presentMessageTips:@"删除成功"];
    [self openEditState];
}

@end
