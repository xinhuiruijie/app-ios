//
//  CollectVideoListController.m
//  MotherPlanet
//
//  Created by 陈熙 on 2018/1/16.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "CollectVideoListController.h"
#import "MyCollectVideoCell.h"
#import "MyLikeVideoListModel.h"
#import "MyCollectHeaderView.h"
#import "VideoInfoController.h"
#import "VideoModel.h"

@interface CollectVideoListController () <UITableViewDataSource, UITableViewDelegate, MyCollectHeaderViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *deleteView;
@property (weak, nonatomic) IBOutlet UIButton *deleteButton;

@property (nonatomic, strong) MyLikeVideoListModel *mylikeVideoListModel;
@property (nonatomic, strong) MyCollectHeaderView *headerView;
@property (nonatomic, strong) NSMutableArray *deleteVideoArray;
@end

@implementation CollectVideoListController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Profile"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.deleteVideoArray = [NSMutableArray array];
    [self setupSubview];
    [self modelInit];
    @weakify(self)
    [self fk_observeNotifcation:kchangeVideoNotification usingBlock:^(NSNotification *note) {
        @strongify(self)
        [self.mylikeVideoListModel refresh];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

#pragma mark - CustomMethod

- (void)setupSubview {
    self.deleteView.hidden = YES;
    self.deleteButton.layer.cornerRadius = 15;
    self.deleteButton.layer.masksToBounds = YES;
    self.deleteButton.layer.borderWidth = [AppTheme onePixel];
    self.deleteButton.layer.borderColor = [UIColor colorWithRGBValue:0xFF1717].CGColor;
    
    self.tableView.tableFooterView = [UIView new];
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.tableView registerNib:[MyCollectVideoCell nib] forCellReuseIdentifier:@"MyCollectVideoCell"];
    [self.tableView registerNib:[EmptyTableCell nib] forCellReuseIdentifier:@"EmptyTableCell"];
    
    self.headerView = [MyCollectHeaderView loadFromNib];
    self.headerView.delegate = self;
}

- (void)modelInit {
    self.mylikeVideoListModel = [[MyLikeVideoListModel alloc] init];
    @weakify(self)
    self.mylikeVideoListModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        if (error == nil) {
            [self setupRefreshFooterView];
            if ([self.delegate respondsToSelector:@selector(noticeSuperVideoState:)]) {
                if (self.mylikeVideoListModel.items.count) {
                    [self.delegate noticeSuperVideoState:YES];
                } else {
                    [self.delegate noticeSuperVideoState:NO];
                }
            }
            
            if (self.mylikeVideoListModel.more) {
                [self.tableView.footer endRefreshing];
            } else {
                [self.tableView.footer noticeNoMoreData];
            }
        } else {
            [self presentMessage:error.message withTips:@"加载数据失败"];
        }
        [self.tableView reloadData];
    };
}

- (void)setupRefreshFooterView {
    if (self.tableView.footer == nil && !self.mylikeVideoListModel.isEmpty) {
        @weakify(self)
        [self.tableView addFooterPullLoader:^{
            @strongify(self)
            [self.mylikeVideoListModel loadMore];
        }];
    } else {
        if (self.mylikeVideoListModel.isEmpty) {
            [self.tableView removeFooter];
        }
    }
}

- (BOOL)deleteVideoArrayContainData:(NSString *)dataId {
    if ([self.deleteVideoArray containsObject:dataId]) {
        return YES;
    } else {
        return NO;
    }
}

- (void)setIsEditState:(BOOL)isEditState {
    _isEditState = isEditState;
    if (isEditState) {
        self.deleteView.hidden = NO;
    } else {
        self.deleteView.hidden = YES;
        self.headerView.isAllSelect = NO;
        if (self.deleteVideoArray.count > 0) {
            [self.deleteVideoArray removeAllObjects];
        }
    }
    [self.tableView reloadData];
}

//单选短片
- (void)selectVideoWithID:(NSString *)videoID isSelect:(BOOL)isSelect {
    if (isSelect) {
        [self.deleteVideoArray addObject:videoID];
        if (self.deleteVideoArray.count == self.mylikeVideoListModel.items.count) {
            self.headerView.isAllSelect = YES;
        }
    } else {
        if (self.deleteVideoArray.count > 0) {
            [self.deleteVideoArray removeObject:videoID];
            if (self.deleteVideoArray.count != self.mylikeVideoListModel.items.count) {
                self.headerView.isAllSelect = NO;
            }
        }
    }
    [self.tableView reloadData];
}

- (void)reloadModel {
    [self.mylikeVideoListModel refresh];
}

#pragma mark - MyCollectHeaderViewDelegate

- (void)selectAllItemIsCancel:(BOOL)isCancel {
    if (isCancel == NO) {
        for (VIDEO *video in self.mylikeVideoListModel.items) {
            [self.deleteVideoArray addObject:video.id];
        }
    } else {
        if (self.deleteVideoArray.count > 0) {
            [self.deleteVideoArray removeAllObjects];
        }
    }
    [self.tableView reloadData];
}

- (IBAction)deleteVideoAction:(id)sender {
    if (self.deleteVideoArray.count) {
        MPAlertView *alertView = [MPAlertView alertWithTitle:@"取消收藏" message:@"确定取消收藏?" cancelButton:@"取消" confirmButton:@"确定"];
        alertView.confirmAction = ^{
            @weakify(self)
            [VideoModel removeVideoWithVideo_ids:self.deleteVideoArray then:^(STIHTTPResponseError *error) {
                @strongify(self)
                if (error == nil) {
                    [self.view presentSuccessTips:@"删除成功"];
                    self.deleteView.hidden = YES;
                    if ([self.delegate respondsToSelector:@selector(noticeSuperVideoState:)]) {
                        if (self.mylikeVideoListModel.items.count) {
                            [self.delegate noticeSuperVideoState:YES];
                        } else {
                            [self.delegate noticeSuperVideoState:NO];
                        }
                    }
                    [self.mylikeVideoListModel refresh];
                } else {
                    [self.view presentMessage:error.message withTips:@"删除失败"];
                }
            }];
        };
        [alertView show];
    }
}

#pragma mark - UITableViewDataSource;

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.mylikeVideoListModel.isEmpty && self.mylikeVideoListModel.loaded) {
        return 1;
    }
    return self.mylikeVideoListModel.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.mylikeVideoListModel.isEmpty && self.mylikeVideoListModel.loaded) {
        EmptyTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EmptyTableCell" forIndexPath:indexPath];
        return cell;
    }
    MyCollectVideoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyCollectVideoCell" forIndexPath:indexPath];
    cell.data = self.mylikeVideoListModel.items[indexPath.row];
    cell.isEditState = self.isEditState;
    VIDEO *video = self.mylikeVideoListModel.items[indexPath.row];
    cell.isSelect = [self deleteVideoArrayContainData:video.id];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.mylikeVideoListModel.isEmpty && self.mylikeVideoListModel.loaded) {
        return self.tableView.bounds.size.height;
    }
    return MAX(kScreenWidth * 180 / 750, 90);
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (self.isEditState) {
        if (self.mylikeVideoListModel.items.count > 0) {
            return 35;
        } else {
            return CGFLOAT_MIN;
        }
    } else {
        return CGFLOAT_MIN;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (self.isEditState) {
        if (self.mylikeVideoListModel.items.count > 0) {
            return self.headerView;
        } else {
            return nil;
        }
    } else {
        return nil;
    }
}

#pragma mark - UITableViewDelegate;

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.mylikeVideoListModel.items.count) {
        if (self.isEditState) {
            VIDEO *video = self.mylikeVideoListModel.items[indexPath.row];
            MyCollectVideoCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            cell.isSelect = !cell.isSelect;
            [self selectVideoWithID:video.id isSelect:cell.isSelect];
        } else {
            VIDEO *video = self.mylikeVideoListModel.items[indexPath.row];
            VideoInfoController *videoInfo = [VideoInfoController spawn];
            videoInfo.video = video;
            
//            [[self topViewController] presentNavigationController:videoInfo];
            videoInfo.hidesBottomBarWhenPushed = YES;
            [[self topViewController].navigationController pushViewController:videoInfo animated:YES];
        }
    }
}

@end
