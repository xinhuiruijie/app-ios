//
//  CouponListController.m
//  MotherPlanet
//
//  Created by 陈熙 on 2018/1/22.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "CouponListController.h"
#import "CouponUseController.h"
#import "CouponNotUseController.h"

@interface CouponListController ()

@end

@implementation CouponListController

- (instancetype)init {
    if (self = [super init]) {
        self.titleColorSelected = [UIColor colorWithRGBValue:0x343338];
        self.titleColorNormal = [UIColor colorWithRGBValue:0xA9AEB5];
        self.menuViewStyle = WMMenuViewStyleLine;
        self.titleSizeNormal = 14;
        self.titleSizeSelected = 14;
        self.progressWidth = 40;
        self.progressViewIsDamping = YES;
        self.pageAnimatable = YES;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"我的卡包";
    @weakify(self)
    self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
    [self.navigationController enabledMLBlackTransition:NO];
    [self customize];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController enabledMLBlackTransition:YES];
}

#pragma mark -

- (NSArray<NSString *> *)titles {
    return @[@"待使用", @"已使用"];
}

- (NSInteger)numbersOfChildControllersInPageController:(WMPageController *)pageController {
    return self.titles.count;
}

- (UIViewController *)pageController:(WMPageController *)pageController viewControllerAtIndex:(NSInteger)index {
    if (index == 0) {
        //待使用
        CouponNotUseController *couponNotUse = [CouponNotUseController spawn];
        return couponNotUse;
    } else {
        //已使用
        CouponUseController *couponUse = [CouponUseController spawn];
        return couponUse;
    }
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForMenuView:(WMMenuView *)menuView {
    return CGRectMake(0, 0, self.view.frame.size.width , 44);
}

- (CGRect)pageController:(WMPageController *)pageController preferredFrameForContentView:(WMScrollView *)contentView {
    CGFloat originY = CGRectGetMaxY([self pageController:pageController preferredFrameForMenuView:self.menuView]);
    return CGRectMake(0, originY, self.view.frame.size.width, self.view.frame.size.height - originY);
}

@end
