//
//  CouponView.h
//  ParkviewGreen
//
//  Created by GeekZooStudio on 17/2/15.
//  Copyright © 2017年 GeekZooStudio. All rights reserved.
//

#import "BaseListView.h"

@interface CouponView : BaseListView
- (void)reloadData;
@end
