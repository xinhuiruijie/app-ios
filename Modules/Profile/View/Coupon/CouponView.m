//
//  CouponView.m
//  ParkviewGreen
//
//  Created by GeekZooStudio on 17/2/15.
//  Copyright © 2017年 GeekZooStudio. All rights reserved.
//

#import "CouponView.h"

@implementation CouponView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.bottomList.scrollEnabled = YES;
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        self.bottomList.scrollEnabled = YES;
    }
    return self;
}

- (void)reloadData {
    [self.bottomList reloadData];
    [self.topList reloadData];
}

#pragma mark -

- (CGPoint)topListOrigin {
    return CGPointMake(0, 0);
}

- (CGSize)topListSize {
    return CGSizeMake(kScreenWidth, 44);
}

- (CGFloat)topListCellWidth {
    NSInteger count = [self.dataSource itemNumber] > 4 ? 4 : [self.dataSource itemNumber];
    return [self topListSize].width / count;
}

- (BOOL)showListMargeLine {
    return YES;
}

- (BOOL)showIndexLine {
    return YES;
}

- (UIColor *)indexLineColor {
    return [AppTheme normalTextColor];
}

- (CGFloat)topListIndexLineWidth {
    return 40;
}

- (UIColor *)listMargeLineColor {
    return [UIColor colorWithRGBValue:0xE9ECF0];
}

@end
