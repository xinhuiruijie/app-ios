//
//  CouponListCell.m
//  ParkviewGreen
//
//  Created by GeekZooStudio on 17/2/15.
//  Copyright © 2017年 GeekZooStudio. All rights reserved.
//

#import "CouponListCell.h"

@interface CouponListCell ()
@property (weak, nonatomic) IBOutlet UIImageView *iconImgView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;

@end

@implementation CouponListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [self customize];
}

- (void)customize {
    
}

- (void)dataDidChange {
    if (!self.data || ![self.data isKindOfClass:[COUPON class]]) {
        return;
    }
    // 券包
    COUPON *coupon = self.data;
    [self.iconImgView setImageWithPhoto:coupon.image];
    self.titleLabel.text = coupon.name?:[AppTheme defaultPlaceholder];
    self.timeLabel.text = [NSString stringWithFormat:@"%@-%@", [NSDate formatter:@"yyyy年MM月dd日" dateStr:coupon.start_date], [NSDate formatter:@"yyyy年MM月dd日" dateStr:coupon.end_date]];
}

@end
