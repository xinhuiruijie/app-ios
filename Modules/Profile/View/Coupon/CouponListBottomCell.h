//
//  CouponListBottomCell.h
//  ParkviewGreen
//
//  Created by GeekZooStudio on 17/2/15.
//  Copyright © 2017年 GeekZooStudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CouponListBottomCell : UICollectionViewCell

- (void)whenDidSelectedCoupon:(void (^)(COUPON *coupon))block;
@end
