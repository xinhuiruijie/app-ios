//
//  CouponListBottomCell.m
//  ParkviewGreen
//
//  Created by GeekZooStudio on 17/2/15.
//  Copyright © 2017年 GeekZooStudio. All rights reserved.
//

#import "CouponListBottomCell.h"
#import "CouponListCell.h"
#import "MyCouponListModel.h"
#import "EnumListModel.h"

@interface CouponListBottomCell () <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, weak) UITableView *tableView;
@property (nonatomic, strong) MyCouponListModel *myCouponListModel;
@property (nonatomic, copy) void (^whenSelectedCouponBlock)(COUPON *coupon);
@end

@implementation CouponListBottomCell

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupSubview];
        
        [self modelInit];
    }
    return self;
}

- (void)setupSubview {
    UITableView *tableView = [[UITableView alloc] initWithFrame:self.bounds];
    [self addSubview:tableView];
    self.tableView = tableView;
    self.tableView.backgroundColor = [AppTheme listBackgroundColor];
    self.tableView.tableFooterView = [UIView new];
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.tableView registerNib:NSStringFromClass([CouponListCell class])];
    self.tableView.rowHeight = 105;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    [self setupRefreshHeaderView];
}

- (void)modelInit {
    // 券包
    self.myCouponListModel = [[MyCouponListModel alloc] init];
    self.myCouponListModel.couponFilter = COUPON_TYPE_NOT_USED;
    @weakify(self)
    self.myCouponListModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        [self.tableView.header endRefreshing];
        if (error == nil) {
            [self setupRefreshFooterView];
            if (self.myCouponListModel.more) {
                [self.tableView.footer endRefreshing];
            } else {
                [self.tableView.footer noticeNoMoreData];
            }
        } else {
            [self presentMessage:error.message withTips:@"数据加载错误"];
        }
        
        [self.tableView reloadData];
    };
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.tableView.frame = self.bounds;
}

- (void)dataDidChange {
    if (!self.data || ![self.data isKindOfClass:[EnumListModel class]]) {
        return;
    }
    EnumListModel *model = self.data;
    
    self.myCouponListModel.couponFilter = model.enumType.integerValue;
    
    [self.tableView.header beginRefreshing];
    
    [self.model loadCache];
    if (self.model.isEmpty) {
        self.model.loaded = NO; // 由于cell重用导致self.couponListModel.loaded不会重置，所以手动重新设为no
        [self.tableView removeFooter];
    }
    [self.tableView reloadData];
}

- (void)whenDidSelectedCoupon:(void (^)(COUPON *coupon))block {
    self.whenSelectedCouponBlock = block;
}

#pragma mark - UITableViewDataSource;

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.model.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CouponListCell *cell = (CouponListCell *)[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CouponListCell class]) forIndexPath:indexPath];
    
    cell.data = self.model.items[indexPath.row];
    return cell;
}

#pragma mark - UITableViewDelegate;

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.whenSelectedCouponBlock) {
        self.whenSelectedCouponBlock(self.model.items[indexPath.row]);
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (self.model.isEmpty && self.model.loaded) {
        EmptyTableCell *empty = [EmptyTableCell loadFromNib];
        return empty;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (self.model.isEmpty && self.model.loaded) {
        return tableView.height;
    }
    return 0;
}

#pragma mark -

- (void)setupRefreshHeaderView {
    @weakify(self)
    [self.tableView addHeaderPullLoader:^{
        @strongify(self)
        [self.model refresh];
    }];
}

- (void)setupRefreshFooterView {
    if ( self.tableView.footer == nil && !self.model.isEmpty) {
        @weakify(self)
        [self.tableView addFooterPullLoader:^{
            @strongify(self)
            [self.model loadMore];
        }];
    } else {
        if (self.model.isEmpty) {
            [self.tableView removeFooter];
        }
    }
}

- (STIStreamModel *)model {
    return self.myCouponListModel;
}

@end
