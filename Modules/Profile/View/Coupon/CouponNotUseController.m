//
//  CouponNotUseController.m
//  MotherPlanet
//
//  Created by 陈熙 on 2018/1/22.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "CouponNotUseController.h"
#import "CouponListCell.h"
#import "MyCouponListModel.h"

@interface CouponNotUseController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) MyCouponListModel *myCouponListModel;

@end

@implementation CouponNotUseController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Profile"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.backgroundColor = [AppTheme listBackgroundColor];
    [self.tableView registerNib:[CouponListCell nib] forCellReuseIdentifier:@"CouponListCell"];
    self.tableView.tableFooterView = [UIView new];
    self.tableView.rowHeight = 105;
    [self setupModel];
    [self setupRefreshHeaderView];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView.header beginRefreshing];
}

#pragma mark - CustomMethod

- (void)setupModel {
    self.myCouponListModel = [[MyCouponListModel alloc] init];
    self.myCouponListModel.couponFilter = COUPON_TYPE_NOT_USED;
    @weakify(self)
    self.myCouponListModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        [self.tableView.header endRefreshing];
        if (error == nil) {
            [self setupRefreshFooterView];
            if (self.myCouponListModel.more) {
                [self.tableView.footer endRefreshing];
            } else {
                [self.tableView.footer noticeNoMoreData];
            }
        } else {
            [self presentMessage:error.message withTips:@"数据加载错误"];
        }
        
        [self.tableView reloadData];
    };
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.myCouponListModel.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    CouponListCell *cell = (CouponListCell *)[tableView dequeueReusableCellWithIdentifier:NSStringFromClass([CouponListCell class]) forIndexPath:indexPath];
    
    cell.data = self.myCouponListModel.items[indexPath.row];
    return cell;
}

#pragma mark - UITableViewDelegate

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (self.myCouponListModel.isEmpty && self.myCouponListModel.loaded) {
        EmptyTableCell *empty = [EmptyTableCell loadFromNib];
        return empty;
    }
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (self.myCouponListModel.isEmpty && self.myCouponListModel.loaded) {
        return tableView.height;
    }
    return 0;
}

- (void)setupRefreshHeaderView {
    @weakify(self)
    [self.tableView addHeaderPullLoader:^{
        @strongify(self)
        [self.myCouponListModel refresh];
    }];
}

- (void)setupRefreshFooterView {
    if ( self.tableView.footer == nil && !self.myCouponListModel.isEmpty) {
        @weakify(self)
        [self.tableView addFooterPullLoader:^{
            @strongify(self)
            [self.myCouponListModel loadMore];
        }];
    } else {
        if (self.myCouponListModel.isEmpty) {
            [self.tableView removeFooter];
        }
    }
}

@end
