//
//  CouponListViewController.m
//  ParkviewGreen
//
//  Created by GeekZooStudio on 17/2/15.
//  Copyright © 2017年 GeekZooStudio. All rights reserved.
//

#import "CouponListViewController.h"
#import "CouponView.h"
#import "CouponListBottomCell.h"
#import "EnumListModel.h"

@interface CouponListViewController () <BaseListViewDataSource, BaseListViewDelegate>
@property (nonatomic, weak) CouponView *couponView;
@property (nonatomic, strong) NSMutableArray *items;
@end

@implementation CouponListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor whiteColor];
    [self customize];
    
    [self setupSubview];
    
    // 刷新列表通知
//    @weakify(self)
//    [[NSNotificationCenter defaultCenter] fk_observeNotifcation:@"CouponListUpdateNotification" usingBlock:^(NSNotification *note) {
//        @strongify(self)
//        [self.couponView.bottomList reloadData];
//    }];
}

#pragma mark -

- (void)customize {
    self.navigationItem.title = @"我的卡包";
    @weakify(self)
    self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
       @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)setupSubview {
    CouponView *couponView = [[CouponView alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth, kScreenHeight - 64)];
    [self.view addSubview:couponView];
    self.couponView = couponView;
    self.couponView.dataSource = self;
    self.couponView.delegate = self;
    [self.couponView.topList registerNib:NSStringFromClass([BaseListViewTopCell class])];
    [self.couponView.bottomList registerClass:NSStringFromClass([CouponListBottomCell class])];
}

#pragma mark - BaseListViewDataSource

- (NSInteger)itemNumber {
    return self.items.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == self.couponView.topList) {
        UICollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([BaseListViewTopCell class]) forIndexPath:indexPath];
        cell.selected = indexPath.item == self.currentIndex;
        cell.data = self.items[indexPath.item];
        return cell;
    } else {
        CouponListBottomCell *cell = (CouponListBottomCell *)[collectionView dequeueReusableCellWithReuseIdentifier:NSStringFromClass([CouponListBottomCell class]) forIndexPath:indexPath];
        cell.data = self.items[indexPath.item];
        
//        @weakify(self)
//        [cell whenDidSelectedCoupon:^(COUPON *coupon) {
//            @strongify(self)
            // 跳转优惠券详情
//            CouponInfoViewController *couponInfo = [CouponInfoViewController spawn];
//            couponInfo.coupon = coupon;
//            [self.navigationController pushViewController:couponInfo animated:YES];
//        }];
        return cell;
    }
}

#pragma mark - BaseListViewDelegate

- (void)collectionView:(UICollectionView *)topList currentIndex:(NSInteger)currentIndex lastIndex:(NSInteger)lastIndex {
    if (currentIndex != self.currentIndex) {
        self.currentIndex = currentIndex;
        [self.couponView reloadData];
    }
}
//- (void)collectionView:(UICollectionView *)topList didSelectTopListItemAtIndexPath:(NSIndexPath *)indexPath {
//    self.currentIndex = indexPath.row;
//    [self.couponView reloadData];
//}

#pragma mark - 

- (NSMutableArray *)items {
    if (!_items) {
        _items = [NSMutableArray arrayWithCapacity:2];
        for (int i = 0; i < 2; i++) {
            EnumListModel *enumModel = [[EnumListModel alloc] init];
            switch (i) {
                case 0:
                    enumModel.displayName = @"待使用";
                    enumModel.enumType = @(COUPON_TYPE_NOT_USED);
                    break;
                case 1:
                    enumModel.displayName = @"已使用";
                    enumModel.enumType = @(COUPON_TYPE_USED);
                    break;
                default:
                    break;
            }
            [_items addObject:enumModel];
        }
    }
    return _items;
}

@end
