//
//  ProfileViewController.m
//  MotherPlanet
//
//  Created by 陈熙 on 2018/2/2.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "ProfileViewController.h"
#import "UserWormholeListController.h"
#import "ProfileEditInfoController.h"
#import "CouponListController.h"
#import "MyFollowListController.h"
#import "MyCollectionController.h"
#import "MyWatchListController.h"
#import "ProfileSettingController.h"
#import "MessageRemindController.h"
#import "WatchLaterListController.h"
#import "WormholeCreateController.h"
#import "WormholeVideoListController.h"

#import "ProfileHeaderView.h"
#import "ProfileWormholeCell.h"
#import "ProfileOperationCell.h"
#import "ProfileCommunityCell.h"
#import "ProfileTimelineCell.h"
#import "TimelineHeaderView.h"

#import "MyTimelineModel.h"
#import "UserWormholeListModel.h"

@interface ProfileViewController () <UITableViewDataSource, UITableViewDelegate, UIScrollViewDelegate, ProfileHeaderViewDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate>

@property (nonatomic, strong) UIImageView *loadingImage;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) ProfileHeaderView *headerView;

@property (nonatomic, strong) MyTimelineModel *timelineModel;
@property (nonatomic, strong) UserWormholeListModel *wormholeModel;

@property (nonatomic, strong) USER *user;

@property (nonatomic, strong) NSMutableArray *cellsArray;
@property (nonatomic, assign) CGRect originFrame;
@property (nonatomic, assign) BOOL isViewWillAppear;

@end

@implementation ProfileViewController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Profile"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.navigationController.navigationBar lt_setBackgroundColor:[UIColor clearColor]];

    self.cellsArray = [NSMutableArray array];
    
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.backgroundColor = [UIColor clearColor];
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    [self.view addSubview:self.tableView];
    
    CGFloat headerHeight = ceil(kScreenWidth * 270 / 375);
    self.headerView = [ProfileHeaderView loadFromNib];
    self.headerView.frame = CGRectMake(0, 0, self.view.width, headerHeight);
    self.originFrame = self.headerView.frame;
    self.headerView.delegate = self;
    self.headerView.data = self.user;
    [self.view addSubview:self.headerView];
    
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tableView.width, headerHeight)];
    headerView.backgroundColor = [UIColor clearColor];
    self.tableView.tableHeaderView = headerView;
    
    CGRect statusBarRect = [[UIApplication sharedApplication] statusBarFrame];
    CGFloat loadingImageY = statusBarRect.size.height + 10;
    self.loadingImage = [[UIImageView alloc] initWithFrame:CGRectMake(10, loadingImageY, 24, 24)];
    self.loadingImage.contentMode = UIViewContentModeScaleAspectFit;
    self.loadingImage.clipsToBounds = YES;
    [self.view addSubview:self.loadingImage];
    
    if (iOSVersionGreaterThanOrEqualTo(@"10.0")) {
        CGFloat topInset = [SDiOSVersion deviceSize] == Screen5Dot8inch ? -88 : -64;
        self.tableView.contentInset = UIEdgeInsetsMake(topInset, 0, 0, 0);
    }
    if (iOSVersionGreaterThanOrEqualTo(@"11.0")) {
        self.tableView.estimatedRowHeight = 0;
        self.tableView.estimatedSectionHeaderHeight = 0;
        self.tableView.estimatedSectionFooterHeight = 0;
    }
    
    [self modelInit];
    [self registerNib];
    [self validCell];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onTabBarDoubleClick:) name:UITabBarItemDoubleClickNotification object:nil];
    
    [self.tableView reloadData];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    self.isViewWillAppear = YES;
    
    self.tableView.delegate = self;
    [self scrollViewDidScroll:self.tableView];
    [self.navigationController.navigationBar setShadowImage:[UIImage new]];

    if ([UserModel online]) {
        [self startLoadingAnimation];
        @weakify(self)
        [[UserModel sharedInstance] getUserProfileWithUser_id:nil completion:^(STIHTTPResponseError *e) {
            @strongify(self)
            if (e == nil) {
                self.user = [UserModel sharedInstance].user;
                self.headerView.data = self.user;
                self.wormholeModel.userId = self.user.id;
                [self.wormholeModel refresh];
            } else {
                [self presentMessage:e.message withTips:@"数据获取失败"];
            }
        }];
    }
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    self.tableView.delegate = nil;
    [self.navigationController.navigationBar lt_reset];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)onTabBarDoubleClick:(NSNotification *)notification {
    // tabbar双击时刷新
    if ([notification.object isKindOfClass:[MPRootViewController class]]) {
        MPRootViewController *rootController = (MPRootViewController *)notification.object;
        if ([self.navigationController isEqual:rootController.selectedViewController]) {
            [self.tableView setContentOffset:CGPointZero animated:YES];
            [self startLoadingAnimation];
            CGFloat topInset = [SDiOSVersion deviceSize] == Screen5Dot8inch ? -88 : -64;
            [self.tableView setContentOffset:CGPointMake(0, topInset) animated:YES];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [self.tableView setContentOffset:CGPointMake(0, 0) animated:YES];
            });
        }
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    if (self.tableView.contentOffset.y > 50) {
        return UIStatusBarStyleDefault;
    }
    return UIStatusBarStyleLightContent;
}

#pragma mark - CustomMethod

- (void)registerNib {
    [self.tableView registerNib:[ProfileOperationCell nib] forCellReuseIdentifier:@"ProfileOperationCell"];
    [self.tableView registerNib:[ProfileWormholeCell nib] forCellReuseIdentifier:@"ProfileWormholeCell"];
    [self.tableView registerNib:[ProfileCommunityCell nib] forCellReuseIdentifier:@"ProfileCommunityCell"];
    [self.tableView registerNib:[ProfileTimelineCell nib] forCellReuseIdentifier:@"ProfileTimelineCell"];
}

- (void)validCell {
    [self.cellsArray removeAllObjects];
    [self.cellsArray addObject:@"ProfileOperationCell"];
    [self.cellsArray addObject:@"ProfileWormholeCell"];
    [self.cellsArray addObject:@"ProfileCommunityCell"];
    if (self.timelineModel.loaded && self.timelineModel.items.count) {
        [self.cellsArray addObject:@"ProfileTimelineCell"];
    }
}

#pragma mark - Model Init

- (void)modelInit {
    self.timelineModel = [[MyTimelineModel alloc] init];
    @weakify(self)
    self.timelineModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        if (error == nil) {
            [self validCell];
            [self setupRefreshFooterView];
            if (self.timelineModel.items.count) {
                if (self.timelineModel.more) {
                    [self.tableView.footer endRefreshing];
                } else {
                    [self.tableView.footer noticeNoMoreData];
                }
            }
        } else {
            [self.view presentMessage:error.message withTips:@"获取数据失败"];
        }
        [self.tableView reloadData];
        [self performSelector:@selector(stopLoadingAnimation) withObject:nil afterDelay:1];
    };
    
    self.wormholeModel = [[UserWormholeListModel alloc] init];
    self.wormholeModel.userId = [UserModel sharedInstance].user.id;
    self.wormholeModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self);
        if (error) {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
        [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:2] withRowAnimation:UITableViewRowAnimationFade];
    };
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat offsetY = scrollView.contentOffset.y;
    
    CGFloat navigationHeight = [SDiOSVersion deviceSize] == Screen5Dot8inch ? 88 : 64;
    CGFloat changePoint = 50;
    if (offsetY > changePoint) {
        CGFloat alpha = MIN(1, 1 - ((changePoint + navigationHeight - offsetY) / navigationHeight));
        [self.navigationController.navigationBar lt_setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:alpha]];
        self.navigationItem.title = self.user.name;
        UIImage *setImage = [UIImage imageNamed:@"icon_set"];
        if (alpha > 0.3) {
            setImage = [UIImage imageNamed:@"icon_set_black"];
        }
        self.navigationItem.rightBarButtonItem = [AppTheme normalItemWithContent:setImage handler:^(id sender) {
            [self pushSettingPage];
        }];
        [self setNeedsStatusBarAppearanceUpdate];
    } else {
        [self.navigationController.navigationBar lt_setBackgroundColor:[[UIColor whiteColor] colorWithAlphaComponent:0]];
        self.navigationItem.title = nil;
        self.navigationItem.rightBarButtonItem = [AppTheme normalItemWithContent:[UIImage imageNamed:@"icon_set"] handler:^(id sender) {
            [self pushSettingPage];
        }];
        [self setNeedsStatusBarAppearanceUpdate];
    }
    
    if (self.isViewWillAppear) {
        self.isViewWillAppear = NO;
        return;
    }
    
    if (offsetY > 0) { // 上滑
        CGRect frame = self.originFrame;
        frame.origin.y = self.originFrame.origin.y - offsetY;
        self.headerView.frame = frame;
    } else { // 下拉
        CGRect frame = self.originFrame;
        frame.size.height = self.originFrame.size.height - offsetY;
        frame.origin.x = self.originFrame.origin.x - (frame.size.width - self.originFrame.size.width) * 0.5;
        self.headerView.frame = frame;
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    CGFloat offsetY = scrollView.contentOffset.y;
    if (offsetY < -50) {
        [self startLoadingAnimation];
    }
}

#pragma mark - ProfileHeaderViewDelegate

- (void)editProfileInfoAction {
    [AppAnalytics clickEvent:@"click_profile_editProfile"];
    ProfileEditInfoController *profileEditInfo = [ProfileEditInfoController spawn];
    profileEditInfo.user = self.user;
    profileEditInfo.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:profileEditInfo animated:YES];
}

- (void)updateCoverPhotoAction {
    [AppAnalytics clickEvent:@"click_profile_cover"];
    [self.view endEditing:YES];
    GKZActionSheet *actionSheet = [GKZActionSheet loadFromNib];
    AlertView * alertView = [[AlertView alloc] initWithContent:actionSheet type:AlertViewTypeMonospaced];
    actionSheet.data = @[@"拍照", @"图库"];
    [alertView showSharedView];
    actionSheet.whenHide = ^(BOOL hide) {
        [alertView hide];
    };
    actionSheet.whenRegistered = ^(id data, NSInteger index) {
        [alertView hide];
        if (index == 0) {
            // 拍照
            if ( [UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera] ) {
                UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                imagePickerController.delegate = self;
                imagePickerController.navigationBar.tintColor = [UIColor blackColor];
                [imagePickerController.navigationBar setTitleTextAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:17], NSForegroundColorAttributeName : [UIColor blackColor]}];
                imagePickerController.allowsEditing = YES;
                imagePickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:imagePickerController animated:YES completion:NULL];
            } else {
                [self presentFailureTips:@"请在设置中打开摄像头权限"];
            }
        } else if (index == 1) {
            // 选照片
            if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypePhotoLibrary]) {
                UIImagePickerController *imagePickerController = [[UIImagePickerController alloc] init];
                imagePickerController.delegate = self;
                imagePickerController.navigationBar.tintColor = [UIColor blackColor];
                [imagePickerController.navigationBar setTitleTextAttributes:@{NSFontAttributeName: [UIFont boldSystemFontOfSize:17], NSForegroundColorAttributeName : [UIColor blackColor]}];
                imagePickerController.allowsEditing = YES;
                imagePickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                [self presentViewController:imagePickerController animated:YES completion:NULL];
            } else {
                [self presentFailureTips:@"请在设置中打开相册权限"];
            }
        }
    };
}

#pragma mark -

- (void)pushSettingPage {
    [AppAnalytics clickEvent:@"click_profile_settings"];
    ProfileSettingController *setting = [ProfileSettingController spawn];
    setting.hidesBottomBarWhenPushed = YES;
    setting.user = self.user;
    [self.navigationController pushViewController:setting animated:YES];
}

- (void)gotoWaterLaterList {
    WatchLaterListController *watchLaterList = [WatchLaterListController spawn];
    watchLaterList.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:watchLaterList animated:YES];
}

- (void)gotoCoupon {
    [AppAnalytics clickEvent:@"click_profile_cardPackage"];
    CouponListController *couponVC = [CouponListController spawn];
    couponVC.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:couponVC animated:YES];
}

- (void)gotoInvite {
    [AppAnalytics clickEvent:@"click_profile_inviteFriends"];
    ShareAlertView *shareAlert = [ShareAlertView loadFromNib];
    shareAlert.data = self.user;
    [shareAlert show];
}

#pragma mark - ProfileOperationCellDelegate

- (void)selectItemWithIndex:(OPERATION_TYPE)index {
    if (index == OPERATION_TYPE_FOLLOW) {
        //我的关注
        [AppAnalytics clickEvent:@"click_profile_myFollowed"];
        MyFollowListController *myFollowList = [MyFollowListController spawn];
        myFollowList.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:myFollowList animated:YES];
    } else if (index == OPERATION_TYPE_COLLECT) {
        //我的收藏
        [AppAnalytics clickEvent:@"click_profile_myCollection"];
        [self pushCollectPage];
    } else if (index == OPERATION_TYPE_WATCH_VIDEO) {
        //我的足迹
        [AppAnalytics clickEvent:@"click_profile_myFootprints"];
        MyWatchListController *myWatchList = [MyWatchListController spawn];
        myWatchList.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:myWatchList animated:YES];
    } else if (index == OPERATION_TYPE_MESSAGE) {
        //消息提醒
        [AppAnalytics clickEvent:@"click_profile_messageRemimder"];
        [self pushMessageMethod];
    }
}

- (void)pushCollectPage {
    MyCollectionController *myCollect = [[MyCollectionController alloc] init];
    myCollect.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:myCollect animated:YES];
}

- (void)pushMessageMethod {
    MessageRemindController *messagePage = [[MessageRemindController alloc] init];
    messagePage.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:messagePage animated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.cellsArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSString *identifier = self.cellsArray[section];
    if ([identifier isEqualToString:@"ProfileCommunityCell"]) {
        return 2;
    } else if ([identifier isEqualToString:@"ProfileTimelineCell"]) {
        return self.timelineModel.items.count;
    } else {
        return 1;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = self.cellsArray[indexPath.section];
    if ([identifier isEqualToString:@"ProfileOperationCell"]) {
        ProfileOperationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProfileOperationCell" forIndexPath:indexPath];
        cell.data = self.user;
        cell.selectBlock = ^(OPERATION_TYPE operationType) {
            [self selectItemWithIndex:operationType];
        };
        return cell;
    } else if ([identifier isEqualToString:@"ProfileWormholeCell"]) {
        ProfileWormholeCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProfileWormholeCell" forIndexPath:indexPath];
        cell.wormholeUser = WORMHOLE_USER_TYPE_MY;
        cell.data = self.wormholeModel;
        cell.wormholeAction = ^(WORMHOLE *wormhole) {
            // 虫洞详情 OR 新建虫洞
            if (wormhole) {
                WormholeVideoListController *wormholeVideoListController = [WormholeVideoListController spawn];
                wormholeVideoListController.wormholeID = wormhole.id;
                wormholeVideoListController.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:wormholeVideoListController animated:YES];
            } else {
                WormholeCreateController *wormholeCreate = [WormholeCreateController spawn];
                wormholeCreate.hidesBottomBarWhenPushed = YES;
                [self.navigationController pushViewController:wormholeCreate animated:YES];
            }
        };
        return cell;
    } else if ([identifier isEqualToString:@"ProfileCommunityCell"]) {
        ProfileCommunityCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProfileCommunityCell" forIndexPath:indexPath];
        cell.profileType = PROFILE_TYPE_WATCH_LATER + indexPath.row;
        return cell;
    } else if ([identifier isEqualToString:@"ProfileTimelineCell"]) {
        ProfileTimelineCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProfileTimelineCell" forIndexPath:indexPath];
        cell.isFirstCell = (indexPath.row == 0);
        cell.isLastCell = (indexPath.row == self.timelineModel.items.count - 1);
        cell.user = self.user;
        cell.data = self.timelineModel.items[indexPath.row];
        return cell;
    }
    return [UITableViewCell new];
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = self.cellsArray[indexPath.section];
    if ([identifier isEqualToString:@"ProfileWormholeCell"]) {
        UserWormholeListController *wormholeListController = [UserWormholeListController spawn];
        wormholeListController.userId = [UserModel sharedInstance].user.id;
        wormholeListController.wormholeUser = WORMHOLE_USER_MY;
        wormholeListController.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:wormholeListController animated:YES];
    } else if ([identifier isEqualToString:@"ProfileCommunityCell"]) {
        ProfileCommunityCell *cell = [tableView cellForRowAtIndexPath:indexPath];
        switch (cell.profileType) {
            case PROFILE_TYPE_WATCH_LATER:
                [self gotoWaterLaterList];
                break;
//            case PROFILE_TYPE_PACKAGE:
//                [self gotoCoupon];
//                break;
            case PROFILE_TYPE_INVITE:
                [self gotoInvite];
                break;
            default:
                break;
        }
    } else if ([identifier isEqualToString:@"ProfileTimelineCell"]) {
        TIMELINE *timeline = self.timelineModel.items[indexPath.row];
        NSString *link = timeline.link;
        if (link && link.length) {
            if (![DeepLink handleURLString:link withCompletion:nil]) {
                [[UIApplication sharedApplication].keyWindow presentFailureTips:@"无法打开的链接"];
            }
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    NSString *identifier = self.cellsArray[section];
    if ([identifier isEqualToString:@"ProfileTimelineCell"]) {
        return 60;
    } else if ([identifier isEqualToString:@"ProfileWormholeCell"]) {
        return 10;
    } else {
        return CGFLOAT_MIN;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *identifier = self.cellsArray[section];
    if ([identifier isEqualToString:@"ProfileWormholeCell"]) {
        UIView *header = [[UIView alloc] init];
        header.backgroundColor = [AppTheme LoginPlaceholderColor];
        return header;
    } else if ([identifier isEqualToString:@"ProfileTimelineCell"]) {
        TimelineHeaderView *header = [TimelineHeaderView loadFromNib];
        header.isUserMine = YES;
        return header;
    } else {
        return nil;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifier = self.cellsArray[indexPath.section];
    if ([identifier isEqualToString:@"ProfileOperationCell"]) {
        return 89;
    } else if ([identifier isEqualToString:@"ProfileCommunityCell"]) {
        return 48;
    } else if ([identifier isEqualToString:@"ProfileWormholeCell"]) {
        return 178;
    } else if ([identifier isEqualToString:@"ProfileTimelineCell"]) {
        TIMELINE *timeline = self.timelineModel.items[indexPath.row];
        return [ProfileTimelineCell heightForRowWithTimeline:timeline];
    }
    return CGFLOAT_MIN;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

#pragma mark - UIImagePickerControllerDelegate

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage *image = [info objectForKey:@"UIImagePickerControllerOriginalImage"];
    image = [image fixOrientation];
    CGRect crop = [[info valueForKey:@"UIImagePickerControllerCropRect"] CGRectValue];
    image = [self ordinaryCrop:image toRect:crop];
    @weakify(self);
    [picker dismissViewControllerAnimated:YES completion:^{
        @strongify(self);
        [self uploadPhotoWithImage:image];
    }];
}

- (UIImage *)ordinaryCrop:(UIImage *)imageToCrop toRect:(CGRect)cropRect {
    CGImageRef imageRef = CGImageCreateWithImageInRect([imageToCrop CGImage], cropRect);
    UIImage *cropped = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return cropped;
}

- (void)uploadPhotoWithImage:(UIImage *)image {
    @weakify(self)
    [UploadPhotoModel uploadWithFile:image completion:^(NSString *url, STIHTTPResponseError *e) {
        @strongify(self)
        if (e) {
            [self presentMessage:e.message withTips:@"上传失败"];
        } else {
            self.user.cover_photo.thumb = url;
            self.headerView.data = self.user;
            [self updateBackgroundImage:url];
        }
    }];
}

- (void)updateBackgroundImage:(NSString *)backgroundImage {
    @weakify(self)
    [[UserModel sharedInstance] updateUserProfileWithGender:self.user.gender name:self.user.name email:nil birthday:self.user.birthday introduce:self.user.introduce avatar:self.user.avatar.large cover_photo:backgroundImage constellation:self.user.constellation location:self.user.location completion:^(STIHTTPResponseError *e) {
        @strongify(self)
        if (e) {
            [self.view presentMessage:e.message withTips:@"修改失败"];
        }
    }];
}

#pragma mark - UINavigationControllerDelegate

- (void)navigationController:(UINavigationController *)navigationController willShowViewController:(UIViewController *)viewController animated:(BOOL)animated {
    if (navigationController.viewControllers.count > 1) {
        UIViewController *imageVC = navigationController.viewControllers[1];
        if ([imageVC isEqual:viewController]) {
            __weak typeof(imageVC) weakImageVC = imageVC;
            imageVC.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
                [weakImageVC.navigationController popViewControllerAnimated:YES];
            }];
        }
    }
}

#pragma mark - Loading

- (void)startLoadingAnimation {
    if (self.loadingImage.isAnimating) {
        return;
    }
    if ([UserModel online]) {
        [self.timelineModel refresh];
    }
    
    UIImageView *imageView = self.loadingImage;
    NSMutableArray *allImages = [NSMutableArray array];
    for (NSInteger i = 1; i < 35; i++) {
        UIImage *image = [UIImage imageNamed:[NSString stringWithFormat:@"icon_refresh_%ld", i]];
        [allImages addObject:image];
    }
    imageView.animationImages = allImages;
    imageView.animationDuration = 1;
    imageView.animationRepeatCount = 0;
    [imageView startAnimating];
    
    imageView.alpha = 1;
}

- (void)stopLoadingAnimation {
    UIImageView *imageView = self.loadingImage;
    [imageView stopAnimating];
    imageView.image = [UIImage imageNamed:@"icon_refresh_ok"];
    
    CGRect statusBarRect = [[UIApplication sharedApplication] statusBarFrame];
    [UIView animateWithDuration:0.5 animations:^{
        CGRect frame = imageView.frame;
        frame.origin.y = -24;
        imageView.frame = frame;
    } completion:^(BOOL finished) {
        imageView.alpha = 0;
        imageView.y = statusBarRect.size.height + 10;
    }];
}

- (void)setupRefreshFooterView {
    if (self.tableView.footer == nil) {
        @weakify(self)
        [self.tableView addFooterPullLoader:^{
            @strongify(self)
            [self.timelineModel loadMore];
        }];
    } else {
        if (self.timelineModel.isEmpty) {
            [self.tableView removeFooter];
        }
    }
}

@end
