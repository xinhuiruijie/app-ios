//
//  BlackListController.m
//  MotherPlanet
//
//  Created by 陈熙 on 2018/3/1.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "MyBlackListController.h"
#import "BlackListModel.h"
#import "BlackListCell.h"
#import "RemoveBlackListModel.h"

@interface MyBlackListController ()

@property (nonatomic, strong) BlackListModel *blackListModel;

@end

@implementation MyBlackListController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Profile"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavigation];
    self.tableView.backgroundColor = [AppTheme listBackgroundColor];
    [self modelInit];
    [self setupRefreshHeaderView];
    [self.tableView registerNib:[BlackListCell nib] forCellReuseIdentifier:@"BlackListCell"];
    [self.tableView registerNib:[EmptyTableCell nib] forCellReuseIdentifier:@"EmptyTableCell"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView.header beginRefreshing];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - CustomMethod

- (void)setupNavigation {
    self.navigationItem.title = @"黑名单";
    @weakify(self)
    self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)modelInit {
    self.blackListModel = [[BlackListModel alloc] init];
    @weakify(self)
    self.blackListModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        if (error == nil) {
            if (self.blackListModel.items.count) {
                [self setupRefreshFooterView];
                if (self.blackListModel.more) {
                    [self.tableView.footer endRefreshing];
                } else {
                    [self.tableView.footer noticeNoMoreData];
                }
            } else {
                [self.tableView removeFooter];
            }
        } else {
            [self.view presentMessage:error.message withTips:@"获取数据失败"];
        }
        [self.tableView.header endRefreshing];
        [self.tableView reloadData];
    };
}

- (void)setupRefreshHeaderView {
    @weakify(self)
    [self.tableView addHeaderPullLoader:^{
        @strongify(self)
        [self.blackListModel refresh];
    }];
}

- (void)setupRefreshFooterView {
    if (self.tableView.footer == nil) {
        @weakify(self)
        [self.tableView addFooterPullLoader:^{
            @strongify(self)
            [self.blackListModel loadMore];
        }];
    } else {
        if (self.blackListModel.isEmpty) {
            [self.tableView removeFooter];
        }
    }
}

- (void)removeUserFromBlackListWithUserId:(NSString *)user_id {
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }
    @weakify(self)
    [RemoveBlackListModel removeUserFromBlackListWithUserId:user_id completion:^(STIHTTPResponseError *e) {
        @strongify(self)
        if (e == nil) {
            [self.view presentSuccessTips:@"移出黑名单成功"];
            [self.blackListModel refresh];
        } else {
            [self.view presentMessage:e.message withTips:@"移出黑名单失败"];
        }
    }];
}

#pragma mark - Table view data source

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.blackListModel.loaded && self.blackListModel.isEmpty) {
        return 1;
    }
    return self.blackListModel.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.blackListModel.loaded && self.blackListModel.isEmpty) {
        EmptyTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EmptyTableCell" forIndexPath:indexPath];
        return cell;
    }
    BlackListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BlackListCell" forIndexPath:indexPath];
    cell.data = self.blackListModel.items[indexPath.row];
    return cell;
}

#pragma mark - TableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.blackListModel.loaded && self.blackListModel.isEmpty) {
        return;
    }
    GKZActionSheet *actionSheet = [GKZActionSheet loadFromNib];
    AlertView * alertView = [[AlertView alloc] initWithContent:actionSheet type:AlertViewTypeMonospaced];
    actionSheet.data = @[@"移出黑名单"];
    [alertView showSharedView];
    actionSheet.whenHide = ^(BOOL hide) {
        [alertView hide];
    };
    @weakify(self)
    actionSheet.whenRegistered = ^(id data, NSInteger index) {
        [alertView hide];
        if (index == 0) {
            @strongify(self)
            USER *user = self.blackListModel.items[indexPath.row];
            [self removeUserFromBlackListWithUserId:user.id];
        }
    };
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.blackListModel.loaded && self.blackListModel.isEmpty) {
        return tableView.height;
    }
    return MAX(ceil(kScreenWidth * 132 / 750), 66);
}

@end
