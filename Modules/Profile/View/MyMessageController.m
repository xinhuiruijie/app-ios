//
//  MyMessageController.m
//  MotherPlanet
//
//  Created by 陈熙 on 2018/1/2.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "MyMessageController.h"
#import "MyMessageModel.h"
#import "MyMessageCell.h"
#import "VideoInfoController.h"

@interface MyMessageController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic, strong) MyMessageModel *myMessageModel;

@end

@implementation MyMessageController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Profile"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self registerNib];
    [self setupModel];
    self.tableView.backgroundColor = [AppTheme listBackgroundColor];
    [self setupRefreshHeaderView];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.tableView.header beginRefreshing];
}

#pragma mark - CustomMethod

- (void)registerNib {
    self.tableView.backgroundColor = [AppTheme listBackgroundColor];
    [self.tableView registerNib:[MyMessageCell nib] forCellReuseIdentifier:@"MyMessageCell"];
    [self.tableView registerNib:[EmptyTableCell nib] forCellReuseIdentifier:@"EmptyTableCell"];
}

- (void)setupModel {
    self.myMessageModel = [[MyMessageModel alloc] init];
    @weakify(self)
    self.myMessageModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        [self.tableView.header endRefreshing];
        if (error == nil) {
            [self setupRefreshFooterView];
            if (self.myMessageModel.more) {
                [self.tableView.footer endRefreshing];
            } else {
                [self.tableView.footer noticeNoMoreData];
            }
            if ([self.delegate respondsToSelector:@selector(noticeSuperRefreshTimeline)]) {
                [self.delegate noticeSuperRefreshTimeline];
            }
        } else {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
        [self.tableView reloadData];
    };
}

- (void)setupRefreshHeaderView {
    @weakify(self)
    [self.tableView addHeaderPullLoader:^{
        @strongify(self)
        [self.myMessageModel refresh];
    }];
}

- (void)setupRefreshFooterView {
    if (self.tableView.footer == nil && !self.myMessageModel.isEmpty) {
        @weakify(self)
        [self.tableView addFooterPullLoader:^{
            @strongify(self)
            [self.myMessageModel loadMore];
        }];
    } else {
        if (self.myMessageModel.isEmpty) {
            [self.tableView removeFooter];
        }
    }
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.myMessageModel.isEmpty && self.myMessageModel.loaded) {
        return 1;
    }
    return self.myMessageModel.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.myMessageModel.isEmpty && self.myMessageModel.loaded) {
        EmptyTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EmptyTableCell" forIndexPath:indexPath];
        return cell;
    }
    MyMessageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyMessageCell" forIndexPath:indexPath];
    cell.data = self.myMessageModel.items[indexPath.row];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.myMessageModel.items.count) {
        MESSAGE *message = self.myMessageModel.items[indexPath.row];
        VIDEO *video = [[VIDEO alloc] init];
        if (message.video.id) {
            video.id = message.video.id;
        }
        VideoInfoController *videoInfo = [VideoInfoController spawn];
        videoInfo.video = video;
        
        videoInfo.hidesBottomBarWhenPushed = YES;
        [self.navigationController pushViewController:videoInfo animated:YES];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.myMessageModel.isEmpty && self.myMessageModel.loaded) {
        return self.tableView.height;
    }
    return [MyMessageCell heightForRowWithMessage:self.myMessageModel.items[indexPath.row]];
}

@end
