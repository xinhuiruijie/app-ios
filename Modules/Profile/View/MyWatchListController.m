//
//  MyWatchListController.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/22.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "MyWatchListController.h"
#import "MyWatchListCell.h"
#import "MyWatchListSectionHeader.h"
#import "MyWatchListModel.h"
#import "WatchVideoModel.h"
#import "OfflineWatchListModel.h"
#import "VideoInfoController.h"

@interface MyWatchListController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) MyWatchListModel *watchListModel;
@property (nonatomic, strong) OfflineWatchListModel *offlineWatchListModel;

@end

@implementation MyWatchListController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Profile"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.title = @"我的足迹";
    @weakify(self)
    self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [self.tableView registerNib:[MyWatchListCell nib] forCellReuseIdentifier:@"MyWatchListCell"];
    [self.tableView registerNib:[EmptyTableCell nib] forCellReuseIdentifier:@"EmptyTableCell"];
    
    [self setupModel];
}

- (void)setupModel {
    self.offlineWatchListModel = [[OfflineWatchListModel alloc] init];
    [self.offlineWatchListModel loadOfflineListCache];
    if ([UserModel online] && self.offlineWatchListModel.offlineWatchList.count) {
        NSArray *offlineList = [NSArray arrayWithArray:self.offlineWatchListModel.offlineWatchList];
        [WatchVideoModel syncWatchList:offlineList then:^(STIHTTPResponseError *error) {
            if (error == nil) {
                [self.offlineWatchListModel removeAll];
                [self.watchListModel refresh];
                [self removeButtonState];
            } else {
                [self presentMessage:error.message withTips:@"数据加载失败"];
            }
            [self.tableView reloadData];
        }];
    }
    
    self.watchListModel = [[MyWatchListModel alloc] init];
    @weakify(self)
    self.watchListModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        if (error == nil) {
            [self removeButtonState];
        } else {
            [self presentMessage:error.message withTips:@"数据加载失败"];
        }
        [self.tableView reloadData];
    };
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    if ([UserModel online]) {
        [self.watchListModel refresh];
    } else {
        [self.offlineWatchListModel loadOfflineListCache];
    }
}

- (void)removeButtonState {
    if (self.watchListModel.items.count || self.offlineWatchListModel.items.count) {
        @weakify(self)
        self.navigationItem.rightBarButtonItem = [AppTheme normalItemWithContent:@"清空" handler:^(id sender) {
            @strongify(self)
            [self removeRecord];
        }];
        self.navigationItem.rightBarButtonItem.enabled = YES;
    } else {
        self.navigationItem.rightBarButtonItem = [AppTheme normalItemWithContent:@"清空" handler:^(id sender) {
        }];
        self.navigationItem.rightBarButtonItem.enabled = NO;
        self.navigationItem.rightBarButtonItem.tintColor = [UIColor colorWithRGBValue:0xA9AEB5];
    }
}

- (void)removeRecord {
    MPAlertView *alertView = [MPAlertView alertWithTitle:@"清空足迹" message:@"确定清空足迹?" cancelButton:@"取消" confirmButton:@"确定"];
    @weakify(self)
    alertView.confirmAction = ^{
        @strongify(self)
        [self.watchListModel.items removeAllObjects];
        [self.offlineWatchListModel.items removeAllObjects];
        [self.offlineWatchListModel removeAll];
        [self.tableView reloadData];
        if ([UserModel online]) {
            [WatchVideoModel deleteWatchList:nil then:^(STIHTTPResponseError *error) {
                if (error == nil) {
                    [self.watchListModel refresh];
                } else {
                    [self presentMessage:error.message withTips:@"清空失败"];
                }
                [self.tableView reloadData];
            }];
        }
    };
    [alertView show];
}

- (void)pushVideoInfoWithVideo:(VIDEO *)video {
    VideoInfoController *videoInfo = [VideoInfoController spawn];
    videoInfo.video = video;
    
//    [[self topViewController] presentNavigationController:videoInfo];
    videoInfo.hidesBottomBarWhenPushed = YES;
    [[self topViewController].navigationController pushViewController:videoInfo animated:YES];
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([UserModel online]) {
        if ([self isOnlineWatchListEmpty]) {
            return 1;
        } else {
            return 2;
        }
    } else {
        if ([self isOfflineWatchListEmpty]) {
            return 1;
        } else {
            return 2;
        }
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([UserModel online]) {
        if ([self isOnlineWatchListEmpty]) {
            return 1;
        } else {
            if (section == 0) {
                return self.watchListModel.recentWatchArray.count;
            } else {
                return self.watchListModel.earlyWatchArray.count;
            }
        }
    } else {
        if ([self isOfflineWatchListEmpty]) {
            return 1;
        } else {
            if (section == 0) {
                return self.offlineWatchListModel.recentWatchArray.count;
            } else {
                return self.offlineWatchListModel.earlyWatchArray.count;
            }
        }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([UserModel online]) {
        if ([self isOnlineWatchListEmpty]) {
            EmptyTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EmptyTableCell" forIndexPath:indexPath];
            return cell;
        } else {
            MyWatchListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyWatchListCell" forIndexPath:indexPath];
            if (indexPath.section == 0) {
                cell.data = self.watchListModel.recentWatchArray[indexPath.row];
            } else {
                cell.data = self.watchListModel.earlyWatchArray[indexPath.row];
            }
            return cell;
        }
    } else {
        if ([self isOfflineWatchListEmpty]) {
            EmptyTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"EmptyTableCell" forIndexPath:indexPath];
            return cell;
        } else {
            MyWatchListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MyWatchListCell" forIndexPath:indexPath];
            if (indexPath.section == 0) {
                cell.data = self.offlineWatchListModel.recentWatchArray[indexPath.row];
            } else {
                cell.data = self.offlineWatchListModel.earlyWatchArray[indexPath.row];
            }
            return cell;
        }
    }

}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([UserModel online]) {
        if ([self isOnlineWatchListEmpty]) {
            return;
        } else {
            MyWatchListCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            VIDEO *video = cell.data;
            [self pushVideoInfoWithVideo:video];
        }
    } else {
        if ([self isOfflineWatchListEmpty]) {
            return;
        } else {
            MyWatchListCell *cell = [tableView cellForRowAtIndexPath:indexPath];
            VIDEO_HISTORY *videoHistory = cell.data;
            VIDEO *video = [[VIDEO alloc] init];
            video.id = videoHistory.video_id;
            [self pushVideoInfoWithVideo:video];
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if ([UserModel online]) {
        if ([self isOnlineWatchListEmpty]) {
            return tableView.height;
        } else {
            return 90;
        }
    } else {
        if ([self isOfflineWatchListEmpty]) {
            return tableView.height;
        } else {
            return 90;
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if ([UserModel online]) {
        if ([self isOnlineWatchListEmpty]) {
            return CGFLOAT_MIN;
        } else {
            if (section == 0 && self.watchListModel.recentWatchArray.count) {
                return 27;
            } else if (section == 1 && self.watchListModel.earlyWatchArray.count) {
                return 27;
            } else {
                return CGFLOAT_MIN;
            }
        }
    } else {
        if ([self isOfflineWatchListEmpty]) {
            return CGFLOAT_MIN;
        } else {
            if (section == 0 && self.offlineWatchListModel.recentWatchArray.count) {
                return 27;
            } else if (section == 1 && self.offlineWatchListModel.earlyWatchArray.count) {
                return 27;
            } else {
                return CGFLOAT_MIN;
            }
        }
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if ([UserModel online]) {
        if ([self isOnlineWatchListEmpty]) {
            return nil;
        } else {
            MyWatchListSectionHeader *sectionHeader = [MyWatchListSectionHeader loadFromNib];
            if (section == 0 && self.watchListModel.recentWatchArray.count) {
                sectionHeader.data = @"近三天";
                return sectionHeader;
            } else if (section == 1 && self.watchListModel.earlyWatchArray.count) {
                sectionHeader.data = @"更早";
                return sectionHeader;
            } else {
                return nil;
            }
        }
    } else {
        if ([self isOfflineWatchListEmpty]) {
            return nil;
        } else {
            MyWatchListSectionHeader *sectionHeader = [MyWatchListSectionHeader loadFromNib];
            if (section == 0 && self.offlineWatchListModel.recentWatchArray.count) {
                sectionHeader.data = @"近三天";
                return sectionHeader;
            } else if (section == 1 && self.offlineWatchListModel.earlyWatchArray.count) {
                sectionHeader.data = @"更早";
                return sectionHeader;
            } else {
                return nil;
            }
        }
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return nil;
}

- (BOOL)isOnlineWatchListEmpty {
    if (self.watchListModel.isEmpty && self.watchListModel.loaded) {
        return YES;
    }
    return NO;
}

- (BOOL)isOfflineWatchListEmpty {
    if (self.offlineWatchListModel.offlineWatchList && self.offlineWatchListModel.offlineWatchList.count) {
        return NO;
    }
    return YES;
}

@end
