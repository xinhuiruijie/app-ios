//
//  SetResolutionController.m
//  MotherPlanet
//
//  Created by liuyadi on 2018/4/19.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "SetResolutionController.h"
#import "SetResolutionCell.h"

@interface SetResolutionController ()

@property (nonatomic, strong) NSIndexPath *indexPath;

@end

@implementation SetResolutionController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Profile"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    @weakify(self)
    self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
        @strongify(self)
        [self backAction];
    }];
    self.navigationItem.title = @"默认清晰度";
    
    self.tableView.rowHeight = 45;
    [self.tableView registerNib:[SetResolutionCell nib] forCellReuseIdentifier:@"SetResolutionCell"];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
}

- (void)backAction {
//    if ([self.delegate respondsToSelector:@selector(finishSelectResolution:)]) {
//        NSString *styleString = [AppTheme defaultPlaceholder];
//        if (self.indexPath.row == 0) {
//            styleString = @"标清";
//        } else if (self.indexPath.row == 1) {
//            styleString = @"高清";
//        } else {
//            styleString = @"超清";
//        }
//        [self.delegate finishSelectResolution:styleString];
//    }
    [self.navigationController popViewControllerAnimated:YES];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SetResolutionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SetResolutionCell" forIndexPath:indexPath];
    cell.selected = (indexPath.row == [SettingModel sharedInstance].defaultQuality);
    cell.quality = VIDEO_QUALITY_NORMAL + indexPath.row;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SetResolutionCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    cell.selected = YES;
    [SettingModel sharedInstance].defaultQuality = VIDEO_QUALITY_NORMAL + indexPath.row;
    self.indexPath = indexPath;
    [tableView reloadData];
}

@end
