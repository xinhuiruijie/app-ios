
#import "STIHTTPNetwork.h"


#pragma mark - 枚举类型

#pragma mark - Declaration

@class OAUTH_CONFIG;
@class CONFIG_QQ_APP;
@class CONFIG_WEIBO_APP;
@class CONFIG_WECHAT_APP;

#pragma mark - Protocols

@protocol OAUTH_CONFIG <NSObject> @end
@protocol CONFIG_QQ_APP <NSObject> @end
@protocol CONFIG_WEIBO_APP <NSObject> @end
@protocol CONFIG_WECHAT_APP <NSObject> @end

#pragma mark - Classes

@interface OAUTH_CONFIG : NSObject
@property (nonatomic, strong) CONFIG_QQ_APP *qq_app;
@property (nonatomic, strong) CONFIG_WEIBO_APP *weibo_app;
@property (nonatomic, strong) CONFIG_WECHAT_APP *wechat_app;
@end
 
@interface CONFIG_QQ_APP : NSObject
@property (nonatomic, strong) NSString *app_id;
@property (nonatomic, strong) NSString *app_secret;
@end
 
@interface CONFIG_WEIBO_APP : NSObject
@property (nonatomic, strong) NSString *app_id;
@property (nonatomic, strong) NSString *app_secret;
@end
 
@interface CONFIG_WECHAT_APP : NSObject
@property (nonatomic, strong) NSString *app_id;
@property (nonatomic, strong) NSString *app_secret;
@end
 
#pragma mark - API

#pragma mark - POST /v1/api.config.get 

@interface V1_API_CONFIG_GET_REQUEST : STIHTTPRequest
@end

@interface V1_API_CONFIG_GET_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) OAUTH_CONFIG *oauth; // 配置信息
@property (nonatomic, strong) NSString *share_base_url; // 分享链接URL
@property (nonatomic, strong) NSString *integral_mall_url; // 积分商城URL
@property (nonatomic, strong) NSString *register_url; // 用户注册协议URL
@property (nonatomic, strong) NSString *copyright_url; // 版权声明URL
@property (nonatomic, strong) NSString *app_guide_url; // 软件说明URL
@property (nonatomic, strong) NSString *about_url; // 关于我们URL
@property (nonatomic, strong) NSString *score_guide_url; // 任务攻略URL
@property (nonatomic, strong) NSString *mall_disclaimer_url; // 商城免责URL
@property (nonatomic, strong) NSString *feedback_url; // 意见反馈URL
@property (nonatomic, strong) NSString *app_introduce_url; // 应用简介URL
@property (nonatomic, strong) NSString *apply_author_url; // 我要投稿URL
@end

@interface V1_API_CONFIG_GET_API : STIHTTPApi
@property (nonatomic, strong) V1_API_CONFIG_GET_REQUEST *req;
@property (nonatomic, strong) V1_API_CONFIG_GET_RESPONSE *resp;
@end

