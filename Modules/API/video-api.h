
#import "STIHTTPNetwork.h"

#pragma mark - Missing

@class USER;
@class PHOTO;
@class CATEGORY;
@class TAG;
@class PAGED;

@protocol USER;
@protocol PHOTO;
@protocol CATEGORY;
@protocol TAG;
@protocol PAGED;

#pragma mark - 枚举类型


#pragma mark - 网络类型

typedef NS_ENUM(NSUInteger, NETWORK_STATUS) {
    NETWORK_STATUS_WIFI = 1, // WIFI
    NETWORK_STATUS_GPRS = 2, // 3G/4G
};

typedef NS_ENUM(NSInteger, VIDEO_QUALITY) {
    VIDEO_QUALITY_NORMAL = 0,
    VIDEO_QUALITY_SD = 1,
    VIDEO_QUALITY_HD = 2,
};

#pragma mark - 举报类型

typedef NS_ENUM(NSUInteger, REPORT_TYPE) {
    REPORT_TYPE_ADVERTISING = 1, // 垃圾广告营销
    REPORT_TYPE_ABUSE = 2, // 恶意攻击谩骂
    REPORT_TYPE_SEXY = 3, // 淫秽色情信息
    REPORT_TYPE_ILLEGAL = 4, // 违法有害信息
    REPORT_TYPE_OTHER = 5, // 其它不良信息
};

#pragma mark - Declaration

@class VIDEO;
@class VIDEO_HISTORY;

#pragma mark - Protocols

@protocol VIDEO <NSObject> @end
@protocol VIDEO_HISTORY <NSObject> @end

#pragma mark - Classes

@interface VIDEO_COUNT_ARRAY : NSObject
@property (nonatomic, strong) NSNumber *roll;   // 是否滚动
@property (nonatomic, strong) NSNumber *time;   // 滚动时间间隔
@property (nonatomic, strong) NSNumber *number; // 点赞/评论/收藏/分享数
@property (nonatomic, strong) NSArray *list;    // 滚动列表
@property (nonatomic) BOOL hasRolled;   // 是否已滚动
@end

@interface VIDEO : NSObject
@property (nonatomic, strong) NSString *id; // id
@property (nonatomic, strong) NSString *title; // 标题
@property (nonatomic, strong) NSString *subtitle; // 简介
@property (nonatomic, strong) USER *owner; // 发表人
@property (nonatomic, strong) NSString *created_at; // 发表时间
@property (nonatomic, strong) PHOTO *photo; // 短片封面图
@property (nonatomic, strong) NSString *video_url; // 短片标清URL
@property (nonatomic, strong) NSString *video_url_sd; // 短片高清URL
@property (nonatomic, strong) NSString *video_url_hd; // 短片超清URL
@property (nonatomic, strong) NSString *video_time; // 短片时长
@property (nonatomic, strong) NSNumber *play_count; // 短片播放量
@property (nonatomic, strong) NSNumber *star_count; // 参与短片评分人数
@property (nonatomic, strong) NSNumber *comment_count; // 评论数
@property (nonatomic, strong) VIDEO_COUNT_ARRAY *comment_count_array;   // 评论动效
@property (nonatomic, strong) NSNumber *collect_count; // 收藏数
@property (nonatomic, strong) VIDEO_COUNT_ARRAY *collect_count_array;   // 收藏动效
@property (nonatomic, strong) NSNumber *share_count; // 分享数
@property (nonatomic, strong) VIDEO_COUNT_ARRAY *share_count_array;     // 分享动效
@property (nonatomic, strong) NSNumber *like_count; // 点赞数
@property (nonatomic, strong) VIDEO_COUNT_ARRAY *like_count_array;      // 点赞动效
@property (nonatomic, assign) CGFloat star; // 短片评分
@property (nonatomic, assign) CGFloat my_star; // 我对这个短片的评分（未评分默认返回0）
@property (nonatomic, strong) NSString *srt; // 字幕文件
@property (nonatomic, strong) NSString *dataflow; // 流量消耗
@property (nonatomic, assign) BOOL is_collect; // 是否已收藏
@property (nonatomic, assign) BOOL is_star; // 是否已评分
@property (nonatomic, assign) BOOL is_like; // 是否已点赞
@property (nonatomic, strong) CATEGORY *category; // 分类
@property (nonatomic, strong) NSArray<TAG> *tags; // 标签数组
@property (nonatomic, strong) NSString *watch_at; // 上次观看时间
@property (nonatomic, strong) NSNumber *record; // 上次观看进度(单位：秒)
@property (nonatomic, assign) BOOL isExpanded; //全文/收起
@property (nonatomic, strong) UIImage *shareImage; // 分享的图片
@property (nonatomic, assign) BOOL isCache; // 是否是缓存中的数据
@end
 
@interface VIDEO_HISTORY : NSObject
@property (nonatomic, strong) NSString *video_id; // 观看短片的id
@property (nonatomic, strong) NSString *watch_at; // 观看短片的时间
@end
 
#pragma mark - API

#pragma mark - POST /v1/api.video.category.list 分类短片列表

@interface V1_API_VIDEO_CATEGORY_LIST_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *category_id; // 分类id
@property (nonatomic, strong) NSNumber *page; // 当前第几页
@property (nonatomic, strong) NSNumber *per_page; // 每页多少
@end

@interface V1_API_VIDEO_CATEGORY_LIST_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray<VIDEO> *hot_videos; // 分类下的精选头条短片（三条）
@property (nonatomic, strong) NSArray<VIDEO> *videos; // 分类下的其他短片（不包含精选头条短片）
@property (nonatomic, strong) PAGED *paged; // 分页结果
@end

@interface V1_API_VIDEO_CATEGORY_LIST_API : STIHTTPApi
@property (nonatomic, strong) V1_API_VIDEO_CATEGORY_LIST_REQUEST *req;
@property (nonatomic, strong) V1_API_VIDEO_CATEGORY_LIST_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.video.list.hot 短片热议列表

@interface V1_API_VIDEO_LIST_HOT_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSNumber *page; // 当前第几页
@property (nonatomic, strong) NSNumber *per_page; // 每页多少
@end

@interface V1_API_VIDEO_LIST_HOT_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray<VIDEO> *videos; // 短片列表
@property (nonatomic, strong) PAGED *paged; // 分页结果
@end

@interface V1_API_VIDEO_LIST_HOT_API : STIHTTPApi
@property (nonatomic, strong) V1_API_VIDEO_LIST_HOT_REQUEST *req;
@property (nonatomic, strong) V1_API_VIDEO_LIST_HOT_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.video.get 短片详情

@interface V1_API_VIDEO_GET_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *video_id; // 短片ID
@property (nonatomic, strong) NSNumber *share_uid; // 分享者的UID
@property (nonatomic, assign) NETWORK_STATUS network; // 当前网络状态
@end

@interface V1_API_VIDEO_GET_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) VIDEO *video; // 短片               
@end

@interface V1_API_VIDEO_GET_API : STIHTTPApi
@property (nonatomic, strong) V1_API_VIDEO_GET_REQUEST *req;
@property (nonatomic, strong) V1_API_VIDEO_GET_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.video.favourite 收藏短片

@interface V1_API_VIDEO_FAVOURITE_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *video_id; // 短片ID
@end

@interface V1_API_VIDEO_FAVOURITE_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) VIDEO *video; // 短片               
@end

@interface V1_API_VIDEO_FAVOURITE_API : STIHTTPApi
@property (nonatomic, strong) V1_API_VIDEO_FAVOURITE_REQUEST *req;
@property (nonatomic, strong) V1_API_VIDEO_FAVOURITE_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.video.unfavourite 取消收藏短片

@interface V1_API_VIDEO_UNFAVOURITE_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *video_id; // 短片ID
@end

@interface V1_API_VIDEO_UNFAVOURITE_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) VIDEO *video; // 短片               
@end

@interface V1_API_VIDEO_UNFAVOURITE_API : STIHTTPApi
@property (nonatomic, strong) V1_API_VIDEO_UNFAVOURITE_REQUEST *req;
@property (nonatomic, strong) V1_API_VIDEO_UNFAVOURITE_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.video.favourite.delete 批量删除已收藏的短片

@interface V1_API_VIDEO_FAVOURITE_DELETE_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *video_ids; // 短片ID
@end

@interface V1_API_VIDEO_FAVOURITE_DELETE_RESPONSE : NSObject<STIHTTPResponse>
@end

@interface V1_API_VIDEO_FAVOURITE_DELETE_API : STIHTTPApi
@property (nonatomic, strong) V1_API_VIDEO_FAVOURITE_DELETE_REQUEST *req;
@property (nonatomic, strong) V1_API_VIDEO_FAVOURITE_DELETE_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.video.star 评分短片

@interface V1_API_VIDEO_STAR_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *video_id; // 短片ID
@property (nonatomic, strong) NSNumber *video_star; // 短片评分（1-10）
@end

@interface V1_API_VIDEO_STAR_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) VIDEO *video; // 短片               
@end

@interface V1_API_VIDEO_STAR_API : STIHTTPApi
@property (nonatomic, strong) V1_API_VIDEO_STAR_REQUEST *req;
@property (nonatomic, strong) V1_API_VIDEO_STAR_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.video.recommend.list 分类推荐短片列表

@interface V1_API_VIDEO_RECOMMEND_LIST_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *category_id; // 分类id
@property (nonatomic, strong) NSString *video_id; // 当前短片id
@end

@interface V1_API_VIDEO_RECOMMEND_LIST_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray<VIDEO> *videos; // 短片列表
@end

@interface V1_API_VIDEO_RECOMMEND_LIST_API : STIHTTPApi
@property (nonatomic, strong) V1_API_VIDEO_RECOMMEND_LIST_REQUEST *req;
@property (nonatomic, strong) V1_API_VIDEO_RECOMMEND_LIST_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.video.favourite.list 我的收藏短片列表

@interface V1_API_VIDEO_FAVOURITE_LIST_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSNumber *page; // 当前第几页
@property (nonatomic, strong) NSNumber *per_page; // 每页多少
@end

@interface V1_API_VIDEO_FAVOURITE_LIST_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray<VIDEO> *videos; // 收藏短片列表
@property (nonatomic, strong) PAGED *paged; // 分页结果
@end

@interface V1_API_VIDEO_FAVOURITE_LIST_API : STIHTTPApi
@property (nonatomic, strong) V1_API_VIDEO_FAVOURITE_LIST_REQUEST *req;
@property (nonatomic, strong) V1_API_VIDEO_FAVOURITE_LIST_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.video.watch.list 我的足迹（观看过的短片）

@interface V1_API_VIDEO_WATCH_LIST_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSNumber *page; // 当前第几页
@property (nonatomic, strong) NSNumber *per_page; // 每页多少
@end

@interface V1_API_VIDEO_WATCH_LIST_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray<VIDEO> *videos; // 观看过的短片列表
@property (nonatomic, strong) PAGED *paged; // 分页结果
@end

@interface V1_API_VIDEO_WATCH_LIST_API : STIHTTPApi
@property (nonatomic, strong) V1_API_VIDEO_WATCH_LIST_REQUEST *req;
@property (nonatomic, strong) V1_API_VIDEO_WATCH_LIST_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.watch.list.delete 删除/清空我的足迹（观看过的短片）

@interface V1_API_WATCH_LIST_DELETE_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *video_ids; // ids, 不传参数时全部删除
@end

@interface V1_API_WATCH_LIST_DELETE_RESPONSE : NSObject<STIHTTPResponse>
@end

@interface V1_API_WATCH_LIST_DELETE_API : STIHTTPApi
@property (nonatomic, strong) V1_API_WATCH_LIST_DELETE_REQUEST *req;
@property (nonatomic, strong) V1_API_WATCH_LIST_DELETE_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.watch.list.sync 同步我的足迹（观看过的短片）

@interface V1_API_WATCH_LIST_SYNC_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *video_history; // 观看过的短片列表（客户端缓存记录）, 传JSON字符串
@end

@interface V1_API_WATCH_LIST_SYNC_RESPONSE : NSObject<STIHTTPResponse>
@end

@interface V1_API_WATCH_LIST_SYNC_API : STIHTTPApi
@property (nonatomic, strong) V1_API_WATCH_LIST_SYNC_REQUEST *req;
@property (nonatomic, strong) V1_API_WATCH_LIST_SYNC_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.video.like 点赞短片

@interface V1_API_VIDEO_LIKE_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *video_id; // 短片ID
@end

@interface V1_API_VIDEO_LIKE_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) VIDEO *video; // 短片               
@end

@interface V1_API_VIDEO_LIKE_API : STIHTTPApi
@property (nonatomic, strong) V1_API_VIDEO_LIKE_REQUEST *req;
@property (nonatomic, strong) V1_API_VIDEO_LIKE_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.video.unlike 取消点赞短片

@interface V1_API_VIDEO_UNLIKE_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *video_id; // 短片ID
@end

@interface V1_API_VIDEO_UNLIKE_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) VIDEO *video; // 短片               
@end

@interface V1_API_VIDEO_UNLIKE_API : STIHTTPApi
@property (nonatomic, strong) V1_API_VIDEO_UNLIKE_REQUEST *req;
@property (nonatomic, strong) V1_API_VIDEO_UNLIKE_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.video.share 分享转发短片

@interface V1_API_VIDEO_SHARE_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *video_id; // 短片ID
@property (nonatomic, copy) NSNumber *app;  // 标识分享到那个app:微信好友:1, 朋友圈2,微博3,qq好友4,qq空间5
@end

@interface V1_API_VIDEO_SHARE_RESPONSE : NSObject<STIHTTPResponse>
@end

@interface V1_API_VIDEO_SHARE_API : STIHTTPApi
@property (nonatomic, strong) V1_API_VIDEO_SHARE_REQUEST *req;
@property (nonatomic, strong) V1_API_VIDEO_SHARE_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.video.report 举报短片

@interface V1_API_VIDEO_REPORT_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *video_id; // 短片ID
@property (nonatomic, assign) REPORT_TYPE type; // 举报类型
@end

@interface V1_API_VIDEO_REPORT_RESPONSE : NSObject<STIHTTPResponse>
@end

@interface V1_API_VIDEO_REPORT_API : STIHTTPApi
@property (nonatomic, strong) V1_API_VIDEO_REPORT_REQUEST *req;
@property (nonatomic, strong) V1_API_VIDEO_REPORT_RESPONSE *resp;
@end

