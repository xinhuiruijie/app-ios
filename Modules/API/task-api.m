
#import "task-api.h"

#pragma mark - Classes

@implementation TASK
@end
 

#pragma mark - API

#pragma mark - POST /v1/api.task.list 任务列表

@implementation V1_API_TASK_LIST_REQUEST
@end

@implementation V1_API_TASK_LIST_RESPONSE
@end

@implementation V1_API_TASK_LIST_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_TASK_LIST_REQUEST requestWithEndpoint:@"/v1/api.task.list" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_TASK_LIST_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.task.finish.invite 邀请好友成功

@implementation V1_API_TASK_FINISH_INVITE_REQUEST
@end

@implementation V1_API_TASK_FINISH_INVITE_RESPONSE
@end

@implementation V1_API_TASK_FINISH_INVITE_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_TASK_FINISH_INVITE_REQUEST requestWithEndpoint:@"/v1/api.task.finish.invite" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_TASK_FINISH_INVITE_RESPONSE class];
    }
    return self;
}

@end

