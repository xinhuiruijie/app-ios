
#import "article-api.h"

#pragma mark - Classes

@implementation ARTICLE
@end
 

#pragma mark - API

#pragma mark - POST /v1/api.article.list.day 日报精选列表

@implementation V1_API_ARTICLE_LIST_DAY_REQUEST
@end

@implementation V1_API_ARTICLE_LIST_DAY_RESPONSE
@end

@implementation V1_API_ARTICLE_LIST_DAY_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_ARTICLE_LIST_DAY_REQUEST requestWithEndpoint:@"/v1/api.article.list.day" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_ARTICLE_LIST_DAY_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.article.get 文章详情

@implementation V1_API_ARTICLE_GET_REQUEST
@end

@implementation V1_API_ARTICLE_GET_RESPONSE
@end

@implementation V1_API_ARTICLE_GET_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_ARTICLE_GET_REQUEST requestWithEndpoint:@"/v1/api.article.get" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_ARTICLE_GET_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.article.like 点赞文章

@implementation V1_API_ARTICLE_LIKE_REQUEST
@end

@implementation V1_API_ARTICLE_LIKE_RESPONSE
@end

@implementation V1_API_ARTICLE_LIKE_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_ARTICLE_LIKE_REQUEST requestWithEndpoint:@"/v1/api.article.like" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_ARTICLE_LIKE_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.article.unlike 取消点赞文章

@implementation V1_API_ARTICLE_UNLIKE_REQUEST
@end

@implementation V1_API_ARTICLE_UNLIKE_RESPONSE
@end

@implementation V1_API_ARTICLE_UNLIKE_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_ARTICLE_UNLIKE_REQUEST requestWithEndpoint:@"/v1/api.article.unlike" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_ARTICLE_UNLIKE_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.article.share 分享转发文章

@implementation V1_API_ARTICLE_SHARE_REQUEST
@end

@implementation V1_API_ARTICLE_SHARE_RESPONSE
@end

@implementation V1_API_ARTICLE_SHARE_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_ARTICLE_SHARE_REQUEST requestWithEndpoint:@"/v1/api.article.share" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_ARTICLE_SHARE_RESPONSE class];
    }
    return self;
}

@end

