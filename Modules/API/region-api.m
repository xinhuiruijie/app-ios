
#import "region-api.h"

#pragma mark - Classes


#pragma mark - API

#pragma mark - POST /v1/api.region.list 省份地区列表

@implementation V1_API_REGION_LIST_REQUEST
@end

@implementation V1_API_REGION_LIST_RESPONSE
@end

@implementation V1_API_REGION_LIST_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_REGION_LIST_REQUEST requestWithEndpoint:@"/v1/api.region.list" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_REGION_LIST_RESPONSE class];
    }
    return self;
}

@end

