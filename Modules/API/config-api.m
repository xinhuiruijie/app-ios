
#import "config-api.h"

#pragma mark - Classes

@implementation OAUTH_CONFIG
@end
 
@implementation CONFIG_QQ_APP
@end
 
@implementation CONFIG_WEIBO_APP
@end
 
@implementation CONFIG_WECHAT_APP
@end
 

#pragma mark - API

#pragma mark - POST /v1/api.config.get 

@implementation V1_API_CONFIG_GET_REQUEST
@end

@implementation V1_API_CONFIG_GET_RESPONSE
@end

@implementation V1_API_CONFIG_GET_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_CONFIG_GET_REQUEST requestWithEndpoint:@"/v1/api.config.get" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_CONFIG_GET_RESPONSE class];
    }
    return self;
}

@end

