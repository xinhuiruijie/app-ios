
#import "video-api.h"

#pragma mark - Classes

@implementation VIDEO_COUNT_ARRAY
@end

@implementation VIDEO
@end
 
@implementation VIDEO_HISTORY
@end
 

#pragma mark - API

#pragma mark - POST /v1/api.video.category.list 分类短片列表

@implementation V1_API_VIDEO_CATEGORY_LIST_REQUEST
@end

@implementation V1_API_VIDEO_CATEGORY_LIST_RESPONSE
@end

@implementation V1_API_VIDEO_CATEGORY_LIST_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_VIDEO_CATEGORY_LIST_REQUEST requestWithEndpoint:@"/v1/api.video.category.list" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_VIDEO_CATEGORY_LIST_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.video.list.hot 短片热议列表

@implementation V1_API_VIDEO_LIST_HOT_REQUEST
@end

@implementation V1_API_VIDEO_LIST_HOT_RESPONSE
@end

@implementation V1_API_VIDEO_LIST_HOT_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_VIDEO_LIST_HOT_REQUEST requestWithEndpoint:@"/v1/api.video.list.hot" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_VIDEO_LIST_HOT_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.video.get 短片详情

@implementation V1_API_VIDEO_GET_REQUEST
@end

@implementation V1_API_VIDEO_GET_RESPONSE
@end

@implementation V1_API_VIDEO_GET_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_VIDEO_GET_REQUEST requestWithEndpoint:@"/v1/api.video.get" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_VIDEO_GET_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.video.favourite 收藏短片

@implementation V1_API_VIDEO_FAVOURITE_REQUEST
@end

@implementation V1_API_VIDEO_FAVOURITE_RESPONSE
@end

@implementation V1_API_VIDEO_FAVOURITE_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_VIDEO_FAVOURITE_REQUEST requestWithEndpoint:@"/v1/api.video.favourite" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_VIDEO_FAVOURITE_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.video.unfavourite 取消收藏短片

@implementation V1_API_VIDEO_UNFAVOURITE_REQUEST
@end

@implementation V1_API_VIDEO_UNFAVOURITE_RESPONSE
@end

@implementation V1_API_VIDEO_UNFAVOURITE_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_VIDEO_UNFAVOURITE_REQUEST requestWithEndpoint:@"/v1/api.video.unfavourite" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_VIDEO_UNFAVOURITE_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.video.favourite.delete 批量删除已收藏的短片

@implementation V1_API_VIDEO_FAVOURITE_DELETE_REQUEST
@end

@implementation V1_API_VIDEO_FAVOURITE_DELETE_RESPONSE
@end

@implementation V1_API_VIDEO_FAVOURITE_DELETE_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_VIDEO_FAVOURITE_DELETE_REQUEST requestWithEndpoint:@"/v1/api.video.favourite.delete" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_VIDEO_FAVOURITE_DELETE_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.video.star 评分短片

@implementation V1_API_VIDEO_STAR_REQUEST
@end

@implementation V1_API_VIDEO_STAR_RESPONSE
@end

@implementation V1_API_VIDEO_STAR_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_VIDEO_STAR_REQUEST requestWithEndpoint:@"/v1/api.video.star" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_VIDEO_STAR_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.video.recommend.list 分类推荐短片列表

@implementation V1_API_VIDEO_RECOMMEND_LIST_REQUEST
@end

@implementation V1_API_VIDEO_RECOMMEND_LIST_RESPONSE
@end

@implementation V1_API_VIDEO_RECOMMEND_LIST_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_VIDEO_RECOMMEND_LIST_REQUEST requestWithEndpoint:@"/v1/api.video.recommend.list" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_VIDEO_RECOMMEND_LIST_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.video.favourite.list 我的收藏短片列表

@implementation V1_API_VIDEO_FAVOURITE_LIST_REQUEST
@end

@implementation V1_API_VIDEO_FAVOURITE_LIST_RESPONSE
@end

@implementation V1_API_VIDEO_FAVOURITE_LIST_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_VIDEO_FAVOURITE_LIST_REQUEST requestWithEndpoint:@"/v1/api.video.favourite.list" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_VIDEO_FAVOURITE_LIST_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.video.watch.list 我的足迹（观看过的短片）

@implementation V1_API_VIDEO_WATCH_LIST_REQUEST
@end

@implementation V1_API_VIDEO_WATCH_LIST_RESPONSE
@end

@implementation V1_API_VIDEO_WATCH_LIST_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_VIDEO_WATCH_LIST_REQUEST requestWithEndpoint:@"/v1/api.video.watch.list" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_VIDEO_WATCH_LIST_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.watch.list.delete 删除/清空我的足迹（观看过的短片）

@implementation V1_API_WATCH_LIST_DELETE_REQUEST
@end

@implementation V1_API_WATCH_LIST_DELETE_RESPONSE
@end

@implementation V1_API_WATCH_LIST_DELETE_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_WATCH_LIST_DELETE_REQUEST requestWithEndpoint:@"/v1/api.watch.list.delete" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_WATCH_LIST_DELETE_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.watch.list.sync 同步我的足迹（观看过的短片）

@implementation V1_API_WATCH_LIST_SYNC_REQUEST
@end

@implementation V1_API_WATCH_LIST_SYNC_RESPONSE
@end

@implementation V1_API_WATCH_LIST_SYNC_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_WATCH_LIST_SYNC_REQUEST requestWithEndpoint:@"/v1/api.watch.list.sync" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_WATCH_LIST_SYNC_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.video.like 点赞短片

@implementation V1_API_VIDEO_LIKE_REQUEST
@end

@implementation V1_API_VIDEO_LIKE_RESPONSE
@end

@implementation V1_API_VIDEO_LIKE_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_VIDEO_LIKE_REQUEST requestWithEndpoint:@"/v1/api.video.like" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_VIDEO_LIKE_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.video.unlike 取消点赞短片

@implementation V1_API_VIDEO_UNLIKE_REQUEST
@end

@implementation V1_API_VIDEO_UNLIKE_RESPONSE
@end

@implementation V1_API_VIDEO_UNLIKE_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_VIDEO_UNLIKE_REQUEST requestWithEndpoint:@"/v1/api.video.unlike" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_VIDEO_UNLIKE_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.video.share 分享转发短片

@implementation V1_API_VIDEO_SHARE_REQUEST
@end

@implementation V1_API_VIDEO_SHARE_RESPONSE
@end

@implementation V1_API_VIDEO_SHARE_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_VIDEO_SHARE_REQUEST requestWithEndpoint:@"/v1/api.video.share" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_VIDEO_SHARE_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.video.report 举报短片

@implementation V1_API_VIDEO_REPORT_REQUEST
@end

@implementation V1_API_VIDEO_REPORT_RESPONSE
@end

@implementation V1_API_VIDEO_REPORT_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_VIDEO_REPORT_REQUEST requestWithEndpoint:@"/v1/api.video.report" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_VIDEO_REPORT_RESPONSE class];
    }
    return self;
}

@end

