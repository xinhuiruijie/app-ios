
#import "STIHTTPNetwork.h"

#pragma mark - Missing

@class VIDEO;
@class TOPIC;
@class WORMHOLE;
@class USER;
@class PAGED;

@protocol VIDEO;
@protocol TOPIC;
@protocol WORMHOLE;
@protocol USER;
@protocol PAGED;

#pragma mark - 枚举类型


#pragma mark - 

typedef NS_ENUM(NSUInteger, SEARCH_TYPE) {
    SEARCH_TYPE_ALL = 1, // 全部
    SEARCH_TYPE_VIDEO = 2, // 短片
    SEARCH_TYPE_TOPIC = 3, // 话题
    SEARCH_TYPE_WORMHOLE = 4, // 虫洞
    SEARCH_TYPE_AUTHOR = 5, // 作者
    SEARCH_TYPE_USER = 6, // 用户
};

#pragma mark - Declaration

@class HOT_SERACH;

#pragma mark - Protocols

@protocol HOT_SERACH <NSObject> @end

#pragma mark - Classes

@interface HOT_SERACH : NSObject
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *title;
@property (nonatomic, assign) SEARCH_TYPE type;
@end
 
#pragma mark - API

#pragma mark - POST /v1/api.search.home 搜索首页

@interface V1_API_SEARCH_HOME_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *keyword; // 搜索内容
@property (nonatomic, assign) SEARCH_TYPE type; // 搜索类型
@end

@interface V1_API_SEARCH_HOME_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray<VIDEO> *videos; // 短片搜索结果
@property (nonatomic, strong) NSArray<TOPIC> *topics; // 话题搜索结果
@property (nonatomic, strong) NSArray<WORMHOLE> *wormholes; // 虫洞搜索结果  虫洞原型未确认
@property (nonatomic, strong) NSArray<USER> *authors; // 作者搜索结果
@property (nonatomic, strong) NSArray<USER> *users; // 用户搜索结果
@end

@interface V1_API_SEARCH_HOME_API : STIHTTPApi
@property (nonatomic, strong) V1_API_SEARCH_HOME_REQUEST *req;
@property (nonatomic, strong) V1_API_SEARCH_HOME_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.search.result 搜索某一类更多

@interface V1_API_SEARCH_RESULT_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *keyword; // 搜索内容
@property (nonatomic, assign) SEARCH_TYPE type; // 搜索类型（不支持ALL）
@property (nonatomic, strong) NSNumber *page; // 当前第几页
@property (nonatomic, strong) NSNumber *per_page; // 每页多少
@end

@interface V1_API_SEARCH_RESULT_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray<VIDEO> *videos; // 短片搜索结果
@property (nonatomic, strong) NSArray<TOPIC> *topics; // 话题搜索结果
@property (nonatomic, strong) NSArray<WORMHOLE> *wormholes; // 虫洞搜索结果  虫洞原型未确认
@property (nonatomic, strong) NSArray<USER> *authors; // 作者搜索结果
@property (nonatomic, strong) NSArray<USER> *users; // 用户搜索结果
@property (nonatomic, strong) PAGED *paged; // 分页结果
@end

@interface V1_API_SEARCH_RESULT_API : STIHTTPApi
@property (nonatomic, strong) V1_API_SEARCH_RESULT_REQUEST *req;
@property (nonatomic, strong) V1_API_SEARCH_RESULT_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.search.hot 热门搜索

@interface V1_API_SEARCH_HOT_REQUEST : STIHTTPRequest
@end

@interface V1_API_SEARCH_HOT_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray<HOT_SERACH> *hot_searches; //
@end

@interface V1_API_SEARCH_HOT_API : STIHTTPApi
@property (nonatomic, strong) V1_API_SEARCH_HOT_REQUEST *req;
@property (nonatomic, strong) V1_API_SEARCH_HOT_RESPONSE *resp;
@end

