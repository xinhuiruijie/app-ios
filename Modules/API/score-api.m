
#import "score-api.h"

#pragma mark - Classes

@implementation SCORE_ORDER
@end
 

#pragma mark - API

#pragma mark - POST /v1/api.score.recent 我的七天的积分情况

@implementation V1_API_SCORE_RECENT_REQUEST
@end

@implementation V1_API_SCORE_RECENT_RESPONSE
@end

@implementation V1_API_SCORE_RECENT_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_SCORE_RECENT_REQUEST requestWithEndpoint:@"/v1/api.score.recent" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_SCORE_RECENT_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.score.history.list 我的积分历史

@implementation V1_API_SCORE_HISTORY_LIST_REQUEST
@end

@implementation V1_API_SCORE_HISTORY_LIST_RESPONSE
@end

@implementation V1_API_SCORE_HISTORY_LIST_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_SCORE_HISTORY_LIST_REQUEST requestWithEndpoint:@"/v1/api.score.history.list" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_SCORE_HISTORY_LIST_RESPONSE class];
    }
    return self;
}

@end

