
#import "STIHTTPNetwork.h"

#pragma mark - Missing

@class PHOTO;
@class PAGED;

@protocol PHOTO;
@protocol PAGED;

#pragma mark - 枚举类型

#pragma mark - Declaration

@class ARTICLE;

#pragma mark - Protocols

@protocol ARTICLE <NSObject> @end

#pragma mark - Classes

@interface ARTICLE : NSObject
@property (nonatomic, strong) NSString * id; // id
@property (nonatomic, strong) NSString * title; // 标题
@property (nonatomic, strong) NSString * subtitle; // 简介
@property (nonatomic, strong) NSString * content; // 正文
@property (nonatomic, strong) PHOTO * photo; // 文章封面图
@property (nonatomic, strong) NSString * resource; // 来源
@property (nonatomic, strong) NSString * created_at; // 发表时间
@property (nonatomic, strong) NSNumber * view_count; // 文章被查看次数
@property (nonatomic, strong) NSNumber * like_count; // 点赞数
@property (nonatomic, strong) NSNumber * share_count; // 分享数
@property (nonatomic, assign) BOOL is_like; // 是否已点赞
@property (nonatomic, strong) UIImage *shareImage; // 分享的图片
@end
 
#pragma mark - API

#pragma mark - POST /v1/api.article.list.day 日报精选列表

@interface V1_API_ARTICLE_LIST_DAY_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSNumber * page; // 当前第几页
@property (nonatomic, strong) NSNumber * per_page; // 每页多少
@end

@interface V1_API_ARTICLE_LIST_DAY_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray<ARTICLE> * articles; // 文章列表
@property (nonatomic, strong) PAGED * paged; // 分页结果
@end

@interface V1_API_ARTICLE_LIST_DAY_API : STIHTTPApi
@property (nonatomic, strong) V1_API_ARTICLE_LIST_DAY_REQUEST * req;
@property (nonatomic, strong) V1_API_ARTICLE_LIST_DAY_RESPONSE * resp;
@end

#pragma mark - POST /v1/api.article.get 文章详情

@interface V1_API_ARTICLE_GET_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString * article_id; // 文章ID
@end

@interface V1_API_ARTICLE_GET_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) ARTICLE * article; // 文章                
@end

@interface V1_API_ARTICLE_GET_API : STIHTTPApi
@property (nonatomic, strong) V1_API_ARTICLE_GET_REQUEST * req;
@property (nonatomic, strong) V1_API_ARTICLE_GET_RESPONSE * resp;
@end

#pragma mark - POST /v1/api.article.like 点赞文章

@interface V1_API_ARTICLE_LIKE_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString * article_id; // 文章ID
@end

@interface V1_API_ARTICLE_LIKE_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, assign) BOOL is_like;
@end

@interface V1_API_ARTICLE_LIKE_API : STIHTTPApi
@property (nonatomic, strong) V1_API_ARTICLE_LIKE_REQUEST * req;
@property (nonatomic, strong) V1_API_ARTICLE_LIKE_RESPONSE * resp;
@end

#pragma mark - POST /v1/api.article.unlike 取消点赞文章

@interface V1_API_ARTICLE_UNLIKE_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString * article_id; // 文章ID
@end

@interface V1_API_ARTICLE_UNLIKE_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, assign) BOOL is_like;
@end

@interface V1_API_ARTICLE_UNLIKE_API : STIHTTPApi
@property (nonatomic, strong) V1_API_ARTICLE_UNLIKE_REQUEST * req;
@property (nonatomic, strong) V1_API_ARTICLE_UNLIKE_RESPONSE * resp;
@end

#pragma mark - POST /v1/api.article.share 分享转发文章

@interface V1_API_ARTICLE_SHARE_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString * article_id; // 文章ID
@end

@interface V1_API_ARTICLE_SHARE_RESPONSE : NSObject<STIHTTPResponse>
@end

@interface V1_API_ARTICLE_SHARE_API : STIHTTPApi
@property (nonatomic, strong) V1_API_ARTICLE_SHARE_REQUEST * req;
@property (nonatomic, strong) V1_API_ARTICLE_SHARE_RESPONSE * resp;
@end

