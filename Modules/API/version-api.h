
#import "STIHTTPNetwork.h"


#pragma mark - 枚举类型


#pragma mark - 更新类型

typedef NS_ENUM(NSUInteger, UPDATE_TYPE) {
    UPDATE_TYPE_OPTIONAL = 0, // 可选升级
    UPDATE_TYPE_FORCE = 1, // 强制升级
    UPDATE_TYPE_NONE = 2, // 不需要更新 
};

#pragma mark - Declaration

@class VERSION;

#pragma mark - Protocols

@protocol VERSION <NSObject> @end

#pragma mark - Classes

@interface VERSION : NSObject
@property (nonatomic, strong) NSString *version; // 版本号
@property (nonatomic, strong) NSString *url; // 下载链接
@property (nonatomic, strong) NSString *content; // 更新日志
@property (nonatomic, assign) UPDATE_TYPE type; // 类型
@end
 
#pragma mark - API

#pragma mark - POST /v1/api.version.check 检查版本更新

@interface V1_API_VERSION_CHECK_REQUEST : STIHTTPRequest
@end

@interface V1_API_VERSION_CHECK_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) VERSION *version; // 最新版本（如果有更新）
@end

@interface V1_API_VERSION_CHECK_API : STIHTTPApi
@property (nonatomic, strong) V1_API_VERSION_CHECK_REQUEST *req;
@property (nonatomic, strong) V1_API_VERSION_CHECK_RESPONSE *resp;
@end

