
#import "STIHTTPNetwork.h"

#pragma mark - Missing

@class USER;
@class PAGED;

@protocol USER;
@protocol PAGED;

#pragma mark - 枚举类型


#pragma mark - 回复评论类型

typedef NS_ENUM(NSUInteger, COMMENT_TYPE) {
    COMMENT_TYPE_COMMENT = 1, // 评论
    COMMENT_TYPE_REPLY = 2, // 回复
};

#pragma mark - Declaration

@class COMMENT;
@class REPLY;
@class REFERENCE;

#pragma mark - Protocols

@protocol COMMENT <NSObject> @end
@protocol REPLY <NSObject> @end
@protocol REFERENCE <NSObject> @end

#pragma mark - Classes

@interface COMMENT : NSObject
@property (nonatomic, strong) NSString * id;
@property (nonatomic, strong) USER * owner; // 用户    		
@property (nonatomic, strong) NSString * content; // 内容    		    		
@property (nonatomic, strong) NSString * created_at; // 创建时间
@property (nonatomic, strong) NSArray<REPLY> * replies; // 引用（短片详情里回复的引用是短片时，reference不返回）
@end
 
@interface REPLY : NSObject
@property (nonatomic, strong) NSString * id;
@property (nonatomic, strong) USER * owner; // 用户    		
@property (nonatomic, strong) NSString * content; // 内容    		    		
@property (nonatomic, strong) NSString * created_at; // 创建时间
@property (nonatomic, strong) REFERENCE * reference; // 引用（短片详情里回复的引用是短片时，reference不返回）
@end
 
@interface REFERENCE : NSObject
@property (nonatomic, strong) NSString * id; // 引用的id
@property (nonatomic, strong) USER * owner; // 用户    		
@property (nonatomic, strong) NSString * created_at; // 创建时间
@end
 
#pragma mark - API

#pragma mark - POST /v1/api.comment.video.list 全部评论列表

@interface V1_API_COMMENT_VIDEO_LIST_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString * video_id; // 短片ID				
@property (nonatomic, strong) NSNumber * page; // 当前第几页
@property (nonatomic, strong) NSNumber * per_page; // 每页多少
@end

@interface V1_API_COMMENT_VIDEO_LIST_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) PAGED * paged;
@property (nonatomic, strong) NSArray<COMMENT> * comments; // 评论列表
@end

@interface V1_API_COMMENT_VIDEO_LIST_API : STIHTTPApi
@property (nonatomic, strong) V1_API_COMMENT_VIDEO_LIST_REQUEST * req;
@property (nonatomic, strong) V1_API_COMMENT_VIDEO_LIST_RESPONSE * resp;
@end

#pragma mark - POST /v1/api.comment.video.send 发送评论

@interface V1_API_COMMENT_VIDEO_SEND_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString * video_id; // 短片ID				
@property (nonatomic, strong) NSString * content; // 内容
@end

@interface V1_API_COMMENT_VIDEO_SEND_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) COMMENT * comment; // 已发送的评论
@property (nonatomic, strong) NSNumber * comment_total; // 评论总数
@end

@interface V1_API_COMMENT_VIDEO_SEND_API : STIHTTPApi
@property (nonatomic, strong) V1_API_COMMENT_VIDEO_SEND_REQUEST * req;
@property (nonatomic, strong) V1_API_COMMENT_VIDEO_SEND_RESPONSE * resp;
@end

#pragma mark - POST /v1/api.comment.video.reply 回复评论

@interface V1_API_COMMENT_VIDEO_REPLY_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString * id; // 评论/回复ID
@property (nonatomic, assign) COMMENT_TYPE type; // 评论/回复类型
@property (nonatomic, strong) NSString * content; // 内容
@end

@interface V1_API_COMMENT_VIDEO_REPLY_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) COMMENT * comment; // 已回复的评论
@end

@interface V1_API_COMMENT_VIDEO_REPLY_API : STIHTTPApi
@property (nonatomic, strong) V1_API_COMMENT_VIDEO_REPLY_REQUEST * req;
@property (nonatomic, strong) V1_API_COMMENT_VIDEO_REPLY_RESPONSE * resp;
@end

#pragma mark - POST /v1/api.comment.report 举报评论/回复

@interface V1_API_COMMENT_REPORT_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *id; // 评论/回复ID
@property (nonatomic, assign) REPORT_TYPE type; // 举报类型
@end

@interface V1_API_COMMENT_REPORT_RESPONSE : NSObject<STIHTTPResponse>
@end

@interface V1_API_COMMENT_REPORT_API : STIHTTPApi
@property (nonatomic, strong) V1_API_COMMENT_REPORT_REQUEST *req;
@property (nonatomic, strong) V1_API_COMMENT_REPORT_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.user.black 拉黑用户（评论的人）

@interface V1_API_USER_BLACK_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *user_id; // 评论用户的id
@end

@interface V1_API_USER_BLACK_RESPONSE : NSObject<STIHTTPResponse>
@end

@interface V1_API_USER_BLACK_API : STIHTTPApi
@property (nonatomic, strong) V1_API_USER_BLACK_REQUEST *req;
@property (nonatomic, strong) V1_API_USER_BLACK_RESPONSE *resp;
@end

