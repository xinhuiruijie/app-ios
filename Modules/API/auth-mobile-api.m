
#import "auth-mobile-api.h"

#pragma mark - Classes

@implementation ACCOUNT
@end

#pragma mark - API

#pragma mark - POST /v1/api.auth.signin 登录

@implementation V1_API_AUTH_SIGNIN_REQUEST
@end

@implementation V1_API_AUTH_SIGNIN_RESPONSE
@end

@implementation V1_API_AUTH_SIGNIN_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_AUTH_SIGNIN_REQUEST requestWithEndpoint:@"/v1/api.auth.signin" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_AUTH_SIGNIN_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.auth.mobile.send 获得手机验证码

@implementation V1_API_AUTH_MOBILE_SEND_REQUEST
@end

@implementation V1_API_AUTH_MOBILE_SEND_RESPONSE
@end

@implementation V1_API_AUTH_MOBILE_SEND_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_AUTH_MOBILE_SEND_REQUEST requestWithEndpoint:@"/v1/api.auth.mobile.send" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_AUTH_MOBILE_SEND_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.auth.mobile.code.verify 注册点击下一步验证手机验证码

@implementation V1_API_AUTH_MOBILE_CODE_VERIFY_REQUEST
@end

@implementation V1_API_AUTH_MOBILE_CODE_VERIFY_RESPONSE
@end

@implementation V1_API_AUTH_MOBILE_CODE_VERIFY_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_AUTH_MOBILE_CODE_VERIFY_REQUEST requestWithEndpoint:@"/v1/api.auth.mobile.code.verify" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_AUTH_MOBILE_CODE_VERIFY_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.auth.mobile.signup 手机号注册

@implementation V1_API_AUTH_MOBILE_SIGNUP_REQUEST
@end

@implementation V1_API_AUTH_MOBILE_SIGNUP_RESPONSE
@end

@implementation V1_API_AUTH_MOBILE_SIGNUP_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_AUTH_MOBILE_SIGNUP_REQUEST requestWithEndpoint:@"/v1/api.auth.mobile.signup" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_AUTH_MOBILE_SIGNUP_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.auth.mobile.reset 手机重置密码(忘记密码)

@implementation V1_API_AUTH_MOBILE_RESET_REQUEST
@end

@implementation V1_API_AUTH_MOBILE_RESET_RESPONSE
@end

@implementation V1_API_AUTH_MOBILE_RESET_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_AUTH_MOBILE_RESET_REQUEST requestWithEndpoint:@"/v1/api.auth.mobile.reset" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_AUTH_MOBILE_RESET_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.auth.social 第三方登录

@implementation V1_API_AUTH_SOCIAL_REQUEST
@end

@implementation V1_API_AUTH_SOCIAL_RESPONSE
@end

@implementation V1_API_AUTH_SOCIAL_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_AUTH_SOCIAL_REQUEST requestWithEndpoint:@"/v1/api.auth.social" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_AUTH_SOCIAL_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.auth.mobile.bind 第三方登录绑定手机号

@implementation V1_API_AUTH_MOBILE_BIND_REQUEST
@end

@implementation V1_API_AUTH_MOBILE_BIND_RESPONSE
@end

@implementation V1_API_AUTH_MOBILE_BIND_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_AUTH_MOBILE_BIND_REQUEST requestWithEndpoint:@"/v1/api.auth.mobile.bind" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_AUTH_MOBILE_BIND_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.auth.social.bind 绑定第三方账号

@implementation V1_API_AUTH_SOCIAL_BIND_REQUEST
@end

@implementation V1_API_AUTH_SOCIAL_BIND_RESPONSE
@end

@implementation V1_API_AUTH_SOCIAL_BIND_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_AUTH_SOCIAL_BIND_REQUEST requestWithEndpoint:@"/v1/api.auth.social.bind" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_AUTH_SOCIAL_BIND_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.auth.social.unbind 解绑第三方账号

@implementation V1_API_AUTH_SOCIAL_UNBIND_REQUEST
@end

@implementation V1_API_AUTH_SOCIAL_UNBIND_RESPONSE
@end

@implementation V1_API_AUTH_SOCIAL_UNBIND_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_AUTH_SOCIAL_UNBIND_REQUEST requestWithEndpoint:@"/v1/api.auth.social.unbind" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_AUTH_SOCIAL_UNBIND_RESPONSE class];
    }
    return self;
}

@end

