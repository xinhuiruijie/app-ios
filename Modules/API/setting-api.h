
#import "STIHTTPNetwork.h"


#pragma mark - 枚举类型

#pragma mark - Declaration


#pragma mark - Protocols


#pragma mark - Classes

#pragma mark - API

#pragma mark - POST /v1/api.register.url 注册协议

@interface V1_API_REGISTER_URL_REQUEST : STIHTTPRequest
@end

@interface V1_API_REGISTER_URL_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSString *register_url; // 注册协议URL
@end

@interface V1_API_REGISTER_URL_API : STIHTTPApi
@property (nonatomic, strong) V1_API_REGISTER_URL_REQUEST *req;
@property (nonatomic, strong) V1_API_REGISTER_URL_RESPONSE *resp;
@end

