
#import "setting-api.h"

#pragma mark - Classes


#pragma mark - API

#pragma mark - POST /v1/api.register.url 注册协议

@implementation V1_API_REGISTER_URL_REQUEST
@end

@implementation V1_API_REGISTER_URL_RESPONSE
@end

@implementation V1_API_REGISTER_URL_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_REGISTER_URL_REQUEST requestWithEndpoint:@"/v1/api.register.url" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_REGISTER_URL_RESPONSE class];
    }
    return self;
}

@end

