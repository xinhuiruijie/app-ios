
#import "STIHTTPNetwork.h"


#pragma mark - 枚举类型

#pragma mark - Declaration

@class SCORE_ORDER;

#pragma mark - Protocols

@protocol SCORE_ORDER <NSObject> @end

#pragma mark - Classes

@interface SCORE_ORDER : NSObject
@property (nonatomic, strong) NSString * id; // 积分id
@property (nonatomic, strong) NSString * score; // 改变积分
@property (nonatomic, strong) NSString * change_desc; // 描述
@property (nonatomic, strong) NSString * created_at; // 时间
@end
 
#pragma mark - API

#pragma mark - POST /v1/api.score.recent 我的七天的积分情况

@interface V1_API_SCORE_RECENT_REQUEST : STIHTTPRequest
@end

@interface V1_API_SCORE_RECENT_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray * score; // 积分
@property (nonatomic, strong) NSNumber * total_score; // 当前总积分数
@end

@interface V1_API_SCORE_RECENT_API : STIHTTPApi
@property (nonatomic, strong) V1_API_SCORE_RECENT_REQUEST * req;
@property (nonatomic, strong) V1_API_SCORE_RECENT_RESPONSE * resp;
@end

#pragma mark - POST /v1/api.score.history.list 我的积分历史

@interface V1_API_SCORE_HISTORY_LIST_REQUEST : STIHTTPRequest
@end

@interface V1_API_SCORE_HISTORY_LIST_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray<SCORE_ORDER> * orders; // 积分历史
@end

@interface V1_API_SCORE_HISTORY_LIST_API : STIHTTPApi
@property (nonatomic, strong) V1_API_SCORE_HISTORY_LIST_REQUEST * req;
@property (nonatomic, strong) V1_API_SCORE_HISTORY_LIST_RESPONSE * resp;
@end

