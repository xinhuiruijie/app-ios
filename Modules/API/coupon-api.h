
#import "STIHTTPNetwork.h"

#pragma mark - Missing

@class PHOTO;
@class PAGED;

@protocol PHOTO;
@protocol PAGED;

#pragma mark - 枚举类型


#pragma mark - 

typedef NS_ENUM(NSUInteger, COUPON_TYPE) {
    COUPON_TYPE_NOT_USED = 1, // 未使用
    COUPON_TYPE_USED = 2, // 已使用
};

#pragma mark - Declaration

@class COUPON;

#pragma mark - Protocols

@protocol COUPON <NSObject> @end

#pragma mark - Classes

@interface COUPON : NSObject
@property (nonatomic, strong) NSString * id; // id
@property (nonatomic, strong) NSString * name; // 名称
@property (nonatomic, strong) PHOTO * image; // 封面  
@property (nonatomic, strong) NSString * start_date; // 有效开始时间
@property (nonatomic, strong) NSString * end_date; // 有效结束时间
//@property (nonatomic, strong) NSString * notice; // 使用说明
@end
 
#pragma mark - API

#pragma mark - POST /v1/api.coupon.list 我的卡包-待使用

@interface V1_API_COUPON_LIST_REQUEST : STIHTTPRequest
@property (nonatomic, assign) COUPON_TYPE type; // 优惠券类型
@property (nonatomic, strong) NSNumber * page; // 当前第几页
@property (nonatomic, strong) NSNumber * per_page; // 每页多少           
@end

@interface V1_API_COUPON_LIST_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray<COUPON> * coupons; // 优惠券列表
@property (nonatomic, strong) PAGED * paged; // 分页结果
@end

@interface V1_API_COUPON_LIST_API : STIHTTPApi
@property (nonatomic, strong) V1_API_COUPON_LIST_REQUEST * req;
@property (nonatomic, strong) V1_API_COUPON_LIST_RESPONSE * resp;
@end

