
#import "STIHTTPNetwork.h"

#pragma mark - Missing

@class VIDEO;
@class PAGED;

@protocol VIDEO;
@protocol PAGED;

#pragma mark - 枚举类型

#pragma mark - Declaration


#pragma mark - Protocols


#pragma mark - Classes

#pragma mark - API

#pragma mark - POST /v1/api.video.follow.list 已关注的作者短片列表（星球页）

@interface V1_API_VIDEO_FOLLOW_LIST_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSNumber *page; // 当前第几页
@property (nonatomic, strong) NSNumber *per_page; // 每页多少
@end

@interface V1_API_VIDEO_FOLLOW_LIST_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray<VIDEO> *videos; // 短片列表
@property (nonatomic, strong) PAGED *paged; // 分页结果
@end

@interface V1_API_VIDEO_FOLLOW_LIST_API : STIHTTPApi
@property (nonatomic, strong) V1_API_VIDEO_FOLLOW_LIST_REQUEST *req;
@property (nonatomic, strong) V1_API_VIDEO_FOLLOW_LIST_RESPONSE *resp;
@end

