
#import "STIHTTPNetwork.h"


#pragma mark - 枚举类型

#pragma mark - Declaration


#pragma mark - Protocols


#pragma mark - Classes

#pragma mark - API

#pragma mark - POST /v1/api.region.list 省份地区列表

@interface V1_API_REGION_LIST_REQUEST : STIHTTPRequest
@end

@interface V1_API_REGION_LIST_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray *regions; //
@end

@interface V1_API_REGION_LIST_API : STIHTTPApi
@property (nonatomic, strong) V1_API_REGION_LIST_REQUEST *req;
@property (nonatomic, strong) V1_API_REGION_LIST_RESPONSE *resp;
@end

