
#import "coupon-api.h"

#pragma mark - Classes

@implementation COUPON
@end
 

#pragma mark - API

#pragma mark - POST /v1/api.coupon.list 我的卡包-待使用

@implementation V1_API_COUPON_LIST_REQUEST
@end

@implementation V1_API_COUPON_LIST_RESPONSE
@end

@implementation V1_API_COUPON_LIST_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_COUPON_LIST_REQUEST requestWithEndpoint:@"/v1/api.coupon.list" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_COUPON_LIST_RESPONSE class];
    }
    return self;
}

@end

