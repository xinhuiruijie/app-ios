
#import "comment-api.h"

#pragma mark - Classes

@implementation COMMENT
@end
 
@implementation REPLY
@end
 
@implementation REFERENCE
@end
 

#pragma mark - API

#pragma mark - POST /v1/api.comment.video.list 全部评论列表

@implementation V1_API_COMMENT_VIDEO_LIST_REQUEST
@end

@implementation V1_API_COMMENT_VIDEO_LIST_RESPONSE
@end

@implementation V1_API_COMMENT_VIDEO_LIST_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_COMMENT_VIDEO_LIST_REQUEST requestWithEndpoint:@"/v1/api.comment.video.list" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_COMMENT_VIDEO_LIST_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.comment.video.send 发送评论

@implementation V1_API_COMMENT_VIDEO_SEND_REQUEST
@end

@implementation V1_API_COMMENT_VIDEO_SEND_RESPONSE
@end

@implementation V1_API_COMMENT_VIDEO_SEND_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_COMMENT_VIDEO_SEND_REQUEST requestWithEndpoint:@"/v1/api.comment.video.send" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_COMMENT_VIDEO_SEND_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.comment.video.reply 回复评论

@implementation V1_API_COMMENT_VIDEO_REPLY_REQUEST
@end

@implementation V1_API_COMMENT_VIDEO_REPLY_RESPONSE
@end

@implementation V1_API_COMMENT_VIDEO_REPLY_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_COMMENT_VIDEO_REPLY_REQUEST requestWithEndpoint:@"/v1/api.comment.video.reply" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_COMMENT_VIDEO_REPLY_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.comment.report 举报评论/回复

@implementation V1_API_COMMENT_REPORT_REQUEST
@end

@implementation V1_API_COMMENT_REPORT_RESPONSE
@end

@implementation V1_API_COMMENT_REPORT_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_COMMENT_REPORT_REQUEST requestWithEndpoint:@"/v1/api.comment.report" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_COMMENT_REPORT_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.user.black 拉黑用户（评论的人）

@implementation V1_API_USER_BLACK_REQUEST
@end

@implementation V1_API_USER_BLACK_RESPONSE
@end

@implementation V1_API_USER_BLACK_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_USER_BLACK_REQUEST requestWithEndpoint:@"/v1/api.user.black" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_USER_BLACK_RESPONSE class];
    }
    return self;
}

@end

