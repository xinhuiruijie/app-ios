
#import "message-api.h"

#pragma mark - Classes

@implementation MESSAGE
@end
 
@implementation SYSTEM_MESSAGE
@end
 

#pragma mark - API

#pragma mark - POST /v1/api.system.message.list 系统消息列表

@implementation V1_API_SYSTEM_MESSAGE_LIST_REQUEST
@end

@implementation V1_API_SYSTEM_MESSAGE_LIST_RESPONSE
@end

@implementation V1_API_SYSTEM_MESSAGE_LIST_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_SYSTEM_MESSAGE_LIST_REQUEST requestWithEndpoint:@"/v1/api.system.message.list" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_SYSTEM_MESSAGE_LIST_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.video.message.list 动态消息列表 (XXX回复/赞/评论了你)

@implementation V1_API_VIDEO_MESSAGE_LIST_REQUEST
@end

@implementation V1_API_VIDEO_MESSAGE_LIST_RESPONSE
@end

@implementation V1_API_VIDEO_MESSAGE_LIST_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_VIDEO_MESSAGE_LIST_REQUEST requestWithEndpoint:@"/v1/api.video.message.list" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_VIDEO_MESSAGE_LIST_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.system.message.unread 系统未读消息提示

@implementation V1_API_SYSTEM_MESSAGE_UNREAD_REQUEST
@end

@implementation V1_API_SYSTEM_MESSAGE_UNREAD_RESPONSE
@end

@implementation V1_API_SYSTEM_MESSAGE_UNREAD_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_SYSTEM_MESSAGE_UNREAD_REQUEST requestWithEndpoint:@"/v1/api.system.message.unread" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_SYSTEM_MESSAGE_UNREAD_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.video.message.unread 动态未读消息提示

@implementation V1_API_VIDEO_MESSAGE_UNREAD_REQUEST
@end

@implementation V1_API_VIDEO_MESSAGE_UNREAD_RESPONSE
@end

@implementation V1_API_VIDEO_MESSAGE_UNREAD_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_VIDEO_MESSAGE_UNREAD_REQUEST requestWithEndpoint:@"/v1/api.video.message.unread" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_VIDEO_MESSAGE_UNREAD_RESPONSE class];
    }
    return self;
}

@end

