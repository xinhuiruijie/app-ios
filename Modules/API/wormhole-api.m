
#import "wormhole-api.h"

#pragma mark - Classes

@implementation WORMHOLE
@end
 

#pragma mark - API

#pragma mark - POST /v1/api.wormhole.list 虫洞列表

@implementation V1_API_WORMHOLE_LIST_REQUEST
@end

@implementation V1_API_WORMHOLE_LIST_RESPONSE
@end

@implementation V1_API_WORMHOLE_LIST_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_WORMHOLE_LIST_REQUEST requestWithEndpoint:@"/v1/api.wormhole.list" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_WORMHOLE_LIST_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.wormhole.get 虫洞详情

@implementation V1_API_WORMHOLE_GET_REQUEST
@end

@implementation V1_API_WORMHOLE_GET_RESPONSE
@end

@implementation V1_API_WORMHOLE_GET_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_WORMHOLE_GET_REQUEST requestWithEndpoint:@"/v1/api.wormhole.get" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_WORMHOLE_GET_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.wormhole.create 新建虫洞

@implementation V1_API_WORMHOLE_CREATE_REQUEST
@end

@implementation V1_API_WORMHOLE_CREATE_RESPONSE
@end

@implementation V1_API_WORMHOLE_CREATE_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_WORMHOLE_CREATE_REQUEST requestWithEndpoint:@"/v1/api.wormhole.create" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_WORMHOLE_CREATE_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.wormhole.delete 删除虫洞

@implementation V1_API_WORMHOLE_DELETE_REQUEST
@end

@implementation V1_API_WORMHOLE_DELETE_RESPONSE
@end

@implementation V1_API_WORMHOLE_DELETE_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_WORMHOLE_DELETE_REQUEST requestWithEndpoint:@"/v1/api.wormhole.delete" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_WORMHOLE_DELETE_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.user.wormhole.list 用户的虫洞列表

@implementation V1_API_USER_WORMHOLE_LIST_REQUEST
@end

@implementation V1_API_USER_WORMHOLE_LIST_RESPONSE
@end

@implementation V1_API_USER_WORMHOLE_LIST_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_USER_WORMHOLE_LIST_REQUEST requestWithEndpoint:@"/v1/api.user.wormhole.list" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_USER_WORMHOLE_LIST_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.wormhole.add.video 添加短片到我的虫洞

@implementation V1_API_WORMHOLE_ADD_VIDEO_REQUEST
@end

@implementation V1_API_WORMHOLE_ADD_VIDEO_RESPONSE
@end

@implementation V1_API_WORMHOLE_ADD_VIDEO_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_WORMHOLE_ADD_VIDEO_REQUEST requestWithEndpoint:@"/v1/api.wormhole.add.video" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_WORMHOLE_ADD_VIDEO_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.wormhole.delete.video 从我的某一虫洞里删除某一短片

@implementation V1_API_WORMHOLE_DELETE_VIDEO_REQUEST
@end

@implementation V1_API_WORMHOLE_DELETE_VIDEO_RESPONSE
@end

@implementation V1_API_WORMHOLE_DELETE_VIDEO_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_WORMHOLE_DELETE_VIDEO_REQUEST requestWithEndpoint:@"/v1/api.wormhole.delete.video" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_WORMHOLE_DELETE_VIDEO_RESPONSE class];
    }
    return self;
}

@end

