
#import "STIHTTPNetwork.h"

#pragma mark - Missing

@class PHOTO;

@protocol PHOTO;

#pragma mark - 枚举类型

#pragma mark - Declaration

@class SPLASH;

#pragma mark - Protocols

@protocol SPLASH <NSObject> @end

#pragma mark - Classes

@interface SPLASH : NSObject
@property (nonatomic, strong) NSString * id;
@property (nonatomic, strong) NSString * splash_hash; // 唯一哈希值，通过id+url+update_time
@property (nonatomic, strong) NSString * begin_at; // 起始展示时间
@property (nonatomic, strong) NSString * finish_at; // 结束展示时间            
@property (nonatomic, strong) PHOTO * photo; // 图片
@property (nonatomic, strong) NSString * link; // 外链URL            
@property (nonatomic, strong) NSNumber * duration; // 显示时长
@end
 
#pragma mark - API

#pragma mark - POST /v1/api.splash.list 拉取闪屏

@interface V1_API_SPLASH_LIST_REQUEST : STIHTTPRequest
@end

@interface V1_API_SPLASH_LIST_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray<SPLASH> * splashes; // 闪屏列表
@end

@interface V1_API_SPLASH_LIST_API : STIHTTPApi
@property (nonatomic, strong) V1_API_SPLASH_LIST_REQUEST * req;
@property (nonatomic, strong) V1_API_SPLASH_LIST_RESPONSE * resp;
@end

