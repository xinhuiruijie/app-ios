
#import "STIHTTPNetwork.h"

#pragma mark - Missing

@class USER;
@class PHOTO;
@class VIDEO;
@class PAGED;

@protocol USER;
@protocol PHOTO;
@protocol VIDEO;
@protocol PAGED;

#pragma mark - 枚举类型


#pragma mark - 

typedef NS_ENUM(NSUInteger, TARGET_TYPE) {
    TARGET_TYPE_COMMENT = 1, // 评论
    TARGET_TYPE_REPLY = 2, // 回复
    TARGET_TYPE_LIKE = 3, // 点赞
};

#pragma mark - Declaration

@class MESSAGE;
@class SYSTEM_MESSAGE;

#pragma mark - Protocols

@protocol MESSAGE <NSObject> @end
@protocol SYSTEM_MESSAGE <NSObject> @end

#pragma mark - Classes

@interface MESSAGE : NSObject
@property (nonatomic, strong) NSString *id; // 消息的id
@property (nonatomic, strong) USER *owner; // 发送消息的用户信息
@property (nonatomic, strong) NSString *created_at; // 创建时间
@property (nonatomic, strong) NSString *push_at; //推送时间
@property (nonatomic, strong) NSString *title; // 消息标题
@property (nonatomic, strong) NSString *content; // 消息正文
@property (nonatomic, strong) PHOTO *photo; // 消息图片
@property (nonatomic, assign) TARGET_TYPE target_type; // 消息类型
@property (nonatomic, strong) VIDEO *video; // 短片ID,目前只有短片的推送                         // 目标id
@end
 
@interface SYSTEM_MESSAGE : NSObject
@property (nonatomic, strong) NSString *id; // 消息的id
@property (nonatomic, strong) NSString *created_at; // 创建时间
@property (nonatomic, strong) NSString *push_at; // 推送时间
@property (nonatomic, strong) NSString *content; // 消息内容
@property (nonatomic, strong) NSString *link; // deepLink
@end
 
#pragma mark - API

#pragma mark - POST /v1/api.system.message.list 系统消息列表

@interface V1_API_SYSTEM_MESSAGE_LIST_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSNumber *page; // 当前第几页
@property (nonatomic, strong) NSNumber *per_page; // 每页多少
@end

@interface V1_API_SYSTEM_MESSAGE_LIST_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) PAGED *paged; // 分页结果
@property (nonatomic, strong) NSArray<SYSTEM_MESSAGE> *messages; // 系统消息列表
@end

@interface V1_API_SYSTEM_MESSAGE_LIST_API : STIHTTPApi
@property (nonatomic, strong) V1_API_SYSTEM_MESSAGE_LIST_REQUEST *req;
@property (nonatomic, strong) V1_API_SYSTEM_MESSAGE_LIST_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.video.message.list 动态消息列表 (XXX回复/赞/评论了你)

@interface V1_API_VIDEO_MESSAGE_LIST_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSNumber *page; // 当前第几页
@property (nonatomic, strong) NSNumber *per_page; // 每页多少
@end

@interface V1_API_VIDEO_MESSAGE_LIST_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) PAGED *paged; // 分页结果
@property (nonatomic, strong) NSArray<MESSAGE> *messages; // 回复列表
@end

@interface V1_API_VIDEO_MESSAGE_LIST_API : STIHTTPApi
@property (nonatomic, strong) V1_API_VIDEO_MESSAGE_LIST_REQUEST *req;
@property (nonatomic, strong) V1_API_VIDEO_MESSAGE_LIST_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.system.message.unread 系统未读消息提示

@interface V1_API_SYSTEM_MESSAGE_UNREAD_REQUEST : STIHTTPRequest
@end

@interface V1_API_SYSTEM_MESSAGE_UNREAD_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, assign) BOOL has_unread; // 是否有未读消息
@end

@interface V1_API_SYSTEM_MESSAGE_UNREAD_API : STIHTTPApi
@property (nonatomic, strong) V1_API_SYSTEM_MESSAGE_UNREAD_REQUEST *req;
@property (nonatomic, strong) V1_API_SYSTEM_MESSAGE_UNREAD_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.video.message.unread 动态未读消息提示

@interface V1_API_VIDEO_MESSAGE_UNREAD_REQUEST : STIHTTPRequest
@end

@interface V1_API_VIDEO_MESSAGE_UNREAD_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, assign) BOOL has_unread; // 是否有未读消息
@end

@interface V1_API_VIDEO_MESSAGE_UNREAD_API : STIHTTPApi
@property (nonatomic, strong) V1_API_VIDEO_MESSAGE_UNREAD_REQUEST *req;
@property (nonatomic, strong) V1_API_VIDEO_MESSAGE_UNREAD_RESPONSE *resp;
@end

