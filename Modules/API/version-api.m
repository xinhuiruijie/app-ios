
#import "version-api.h"

#pragma mark - Classes

@implementation VERSION
@end
 

#pragma mark - API

#pragma mark - POST /v1/api.version.check 检查版本更新

@implementation V1_API_VERSION_CHECK_REQUEST
@end

@implementation V1_API_VERSION_CHECK_RESPONSE
@end

@implementation V1_API_VERSION_CHECK_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_VERSION_CHECK_REQUEST requestWithEndpoint:@"/v1/api.version.check" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_VERSION_CHECK_RESPONSE class];
    }
    return self;
}

@end

