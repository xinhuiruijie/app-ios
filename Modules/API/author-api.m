
#import "author-api.h"

#pragma mark - Classes


#pragma mark - API

#pragma mark - POST /v1/api.author.recent.join 最近加入的作者

@implementation V1_API_AUTHOR_RECENT_JOIN_REQUEST
@end

@implementation V1_API_AUTHOR_RECENT_JOIN_RESPONSE
@end

@implementation V1_API_AUTHOR_RECENT_JOIN_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_AUTHOR_RECENT_JOIN_REQUEST requestWithEndpoint:@"/v1/api.author.recent.join" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_AUTHOR_RECENT_JOIN_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.author.follow 关注作者

@implementation V1_API_AUTHOR_FOLLOW_REQUEST
@end

@implementation V1_API_AUTHOR_FOLLOW_RESPONSE
@end

@implementation V1_API_AUTHOR_FOLLOW_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_AUTHOR_FOLLOW_REQUEST requestWithEndpoint:@"/v1/api.author.follow" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_AUTHOR_FOLLOW_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.author.unfollow 取消关注作者

@implementation V1_API_AUTHOR_UNFOLLOW_REQUEST
@end

@implementation V1_API_AUTHOR_UNFOLLOW_RESPONSE
@end

@implementation V1_API_AUTHOR_UNFOLLOW_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_AUTHOR_UNFOLLOW_REQUEST requestWithEndpoint:@"/v1/api.author.unfollow" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_AUTHOR_UNFOLLOW_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.author.follow.list 我的关注列表

@implementation V1_API_AUTHOR_FOLLOW_LIST_REQUEST
@end

@implementation V1_API_AUTHOR_FOLLOW_LIST_RESPONSE
@end

@implementation V1_API_AUTHOR_FOLLOW_LIST_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_AUTHOR_FOLLOW_LIST_REQUEST requestWithEndpoint:@"/v1/api.author.follow.list" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_AUTHOR_FOLLOW_LIST_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.author.video.list 作者动态

@implementation V1_API_AUTHOR_VIDEO_LIST_REQUEST
@end

@implementation V1_API_AUTHOR_VIDEO_LIST_RESPONSE
@end

@implementation V1_API_AUTHOR_VIDEO_LIST_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_AUTHOR_VIDEO_LIST_REQUEST requestWithEndpoint:@"/v1/api.author.video.list" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_AUTHOR_VIDEO_LIST_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.author.report 举报作者

@implementation V1_API_AUTHOR_REPORT_REQUEST
@end

@implementation V1_API_AUTHOR_REPORT_RESPONSE
@end

@implementation V1_API_AUTHOR_REPORT_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_AUTHOR_REPORT_REQUEST requestWithEndpoint:@"/v1/api.author.report" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_AUTHOR_REPORT_RESPONSE class];
    }
    return self;
}

@end

