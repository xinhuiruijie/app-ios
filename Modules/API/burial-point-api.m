
#import "burial-point-api.h"

#pragma mark - Classes

@implementation BURIAL_POINT
@end
 

#pragma mark - API

#pragma mark - POST /v1/api.user.point 个人操作埋点统计

@implementation V1_API_USER_POINT_REQUEST
@end

@implementation V1_API_USER_POINT_RESPONSE
@end

@implementation V1_API_USER_POINT_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_USER_POINT_REQUEST requestWithEndpoint:@"/v1/api.user.point" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_USER_POINT_RESPONSE class];
    }
    return self;
}

@end

