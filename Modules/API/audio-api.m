
#import "audio-api.h"

#pragma mark - Classes

@implementation AUDIO
@end
 

#pragma mark - API

#pragma mark - POST /v1/api.audio.list 音频列表

@implementation V1_API_AUDIO_LIST_REQUEST
@end

@implementation V1_API_AUDIO_LIST_RESPONSE
@end

@implementation V1_API_AUDIO_LIST_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_AUDIO_LIST_REQUEST requestWithEndpoint:@"/v1/api.audio.list" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_AUDIO_LIST_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.audio.report 举报音频

@implementation V1_API_AUDIO_REPORT_REQUEST
@end

@implementation V1_API_AUDIO_REPORT_RESPONSE
@end

@implementation V1_API_AUDIO_REPORT_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_AUDIO_REPORT_REQUEST requestWithEndpoint:@"/v1/api.audio.report" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_AUDIO_REPORT_RESPONSE class];
    }
    return self;
}

@end

