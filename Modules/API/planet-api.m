
#import "planet-api.h"

#pragma mark - Classes


#pragma mark - API

#pragma mark - POST /v1/api.video.follow.list 已关注的作者短片列表（星球页）

@implementation V1_API_VIDEO_FOLLOW_LIST_REQUEST
@end

@implementation V1_API_VIDEO_FOLLOW_LIST_RESPONSE
@end

@implementation V1_API_VIDEO_FOLLOW_LIST_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_VIDEO_FOLLOW_LIST_REQUEST requestWithEndpoint:@"/v1/api.video.follow.list" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_VIDEO_FOLLOW_LIST_RESPONSE class];
    }
    return self;
}

@end

