
#import "search-api.h"

#pragma mark - Classes

@implementation HOT_SERACH
@end
 

#pragma mark - API

#pragma mark - POST /v1/api.search.home 搜索首页

@implementation V1_API_SEARCH_HOME_REQUEST
@end

@implementation V1_API_SEARCH_HOME_RESPONSE
@end

@implementation V1_API_SEARCH_HOME_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_SEARCH_HOME_REQUEST requestWithEndpoint:@"/v1/api.search.home" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_SEARCH_HOME_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.search.result 搜索某一类更多

@implementation V1_API_SEARCH_RESULT_REQUEST
@end

@implementation V1_API_SEARCH_RESULT_RESPONSE
@end

@implementation V1_API_SEARCH_RESULT_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_SEARCH_RESULT_REQUEST requestWithEndpoint:@"/v1/api.search.result" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_SEARCH_RESULT_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.search.hot 热门搜索

@implementation V1_API_SEARCH_HOT_REQUEST
@end

@implementation V1_API_SEARCH_HOT_RESPONSE
@end

@implementation V1_API_SEARCH_HOT_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_SEARCH_HOT_REQUEST requestWithEndpoint:@"/v1/api.search.hot" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_SEARCH_HOT_RESPONSE class];
    }
    return self;
}

@end

