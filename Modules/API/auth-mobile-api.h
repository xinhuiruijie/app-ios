
#import "STIHTTPNetwork.h"

#pragma mark - Missing

@class USER;
@class ACCOUNT;

@protocol USER;

#pragma mark - 枚举类型


#pragma mark - 验证码用途

typedef NS_ENUM(NSUInteger, CODE_TYPE) {
    CODE_TYPE_FORGOT_PASSWORD = 1, // 忘记密码
    CODE_TYPE_REGISTER = 2, // 注册
    CODE_TYPE_QUICK_LOGIN = 3, // 快捷登录
    CODE_TYPE_MODIFY_MOBILE = 4, // 修改手机号
};


#pragma mark - 帐号类型

typedef NS_ENUM(NSUInteger, SOCIAL_VENDOR) {
    SOCIAL_VENDOR_UNKNOWN = 1, // 未知
    SOCIAL_VENDOR_WEIXIN = 2, // 微信
    SOCIAL_VENDOR_WEIBO = 3, // 微博
    SOCIAL_VENDOR_QQ = 4, // QQ
};

#pragma mark - Declaration

#pragma mark - Protocols


#pragma mark - Classes

@interface ACCOUNT : NSObject
@property (nonatomic, assign) SOCIAL_VENDOR vendor; // 第三方平台类型
@property (nonatomic, strong) NSString * auth_key; // OAUTH KEY
@property (nonatomic, strong) NSString * auth_token; // OAUTH TOKEN
@property (nonatomic, strong) NSString * user_id; // 用户ID
@property (nonatomic, strong) NSString * expires; // 过期时间（秒数）
@end

#pragma mark - API

#pragma mark - POST /v1/api.auth.signin 登录

@interface V1_API_AUTH_SIGNIN_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *mobile; // 手机号
@property (nonatomic, strong) NSString *password; // 密码
@end

@interface V1_API_AUTH_SIGNIN_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSString *token; // Token
@property (nonatomic, strong) USER *user; // 用户资料                
@end

@interface V1_API_AUTH_SIGNIN_API : STIHTTPApi
@property (nonatomic, strong) V1_API_AUTH_SIGNIN_REQUEST *req;
@property (nonatomic, strong) V1_API_AUTH_SIGNIN_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.auth.mobile.send 获得手机验证码

@interface V1_API_AUTH_MOBILE_SEND_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *mobile; // 手机号码
@property (nonatomic, assign) CODE_TYPE type; // 验证码用途
@end

@interface V1_API_AUTH_MOBILE_SEND_RESPONSE : NSObject<STIHTTPResponse>
@end

@interface V1_API_AUTH_MOBILE_SEND_API : STIHTTPApi
@property (nonatomic, strong) V1_API_AUTH_MOBILE_SEND_REQUEST *req;
@property (nonatomic, strong) V1_API_AUTH_MOBILE_SEND_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.auth.mobile.code.verify 注册点击下一步验证手机验证码

@interface V1_API_AUTH_MOBILE_CODE_VERIFY_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *mobile; // 手机号码
@property (nonatomic, strong) NSString *code; // 验证码
@property (nonatomic, assign) CODE_TYPE type; // 验证码用途
@end

@interface V1_API_AUTH_MOBILE_CODE_VERIFY_RESPONSE : NSObject<STIHTTPResponse>
@end

@interface V1_API_AUTH_MOBILE_CODE_VERIFY_API : STIHTTPApi
@property (nonatomic, strong) V1_API_AUTH_MOBILE_CODE_VERIFY_REQUEST *req;
@property (nonatomic, strong) V1_API_AUTH_MOBILE_CODE_VERIFY_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.auth.mobile.signup 手机号注册

@interface V1_API_AUTH_MOBILE_SIGNUP_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *name; // 姓名
@property (nonatomic, strong) NSString *mobile; // 手机号
@property (nonatomic, assign) USER_GENDER gender; // 性别
@property (nonatomic, strong) NSString *avatar; // 头像
@property (nonatomic, strong) NSString *password; // 密码
@property (nonatomic, strong) NSNumber *share_uid; // 分享者的UID（邀请好友注册时传）
@end

@interface V1_API_AUTH_MOBILE_SIGNUP_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSString *token; // Token
@property (nonatomic, strong) USER *user; // 用户资料   
@end

@interface V1_API_AUTH_MOBILE_SIGNUP_API : STIHTTPApi
@property (nonatomic, strong) V1_API_AUTH_MOBILE_SIGNUP_REQUEST *req;
@property (nonatomic, strong) V1_API_AUTH_MOBILE_SIGNUP_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.auth.mobile.reset 手机重置密码(忘记密码)

@interface V1_API_AUTH_MOBILE_RESET_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *mobile; // 手机号
@property (nonatomic, strong) NSString *code; // 验证码
@property (nonatomic, strong) NSString *password; // 新密码
@end

@interface V1_API_AUTH_MOBILE_RESET_RESPONSE : NSObject<STIHTTPResponse>
@end

@interface V1_API_AUTH_MOBILE_RESET_API : STIHTTPApi
@property (nonatomic, strong) V1_API_AUTH_MOBILE_RESET_REQUEST *req;
@property (nonatomic, strong) V1_API_AUTH_MOBILE_RESET_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.auth.social 第三方登录

@interface V1_API_AUTH_SOCIAL_REQUEST : STIHTTPRequest
@property (nonatomic, assign) SOCIAL_VENDOR vendor; // 第三方平台类型
@property (nonatomic, strong) NSString *access_token; // OAUTH TOKEN
@property (nonatomic, strong) NSString *open_id; // 用户ID
@end

@interface V1_API_AUTH_SOCIAL_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSString *token; // 访问token
@property (nonatomic, strong) USER *user; // 用户资料
@end

@interface V1_API_AUTH_SOCIAL_API : STIHTTPApi
@property (nonatomic, strong) V1_API_AUTH_SOCIAL_REQUEST *req;
@property (nonatomic, strong) V1_API_AUTH_SOCIAL_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.auth.mobile.bind 第三方登录绑定手机号

@interface V1_API_AUTH_MOBILE_BIND_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *mobile; // 手机号
@property (nonatomic, strong) NSString *code; // 验证码
@property (nonatomic, strong) NSString *password; // 密码
@end

@interface V1_API_AUTH_MOBILE_BIND_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSString *token; // 访问token
@property (nonatomic, strong) USER *user; // 用户资料
@end

@interface V1_API_AUTH_MOBILE_BIND_API : STIHTTPApi
@property (nonatomic, strong) V1_API_AUTH_MOBILE_BIND_REQUEST *req;
@property (nonatomic, strong) V1_API_AUTH_MOBILE_BIND_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.auth.social.bind 绑定第三方账号

@interface V1_API_AUTH_SOCIAL_BIND_REQUEST : STIHTTPRequest
@property (nonatomic, assign) SOCIAL_VENDOR vendor; // 第三方平台类型
@property (nonatomic, strong) NSString *access_token; // OAUTH TOKEN
@property (nonatomic, strong) NSString *open_id; // 用户ID
@end

@interface V1_API_AUTH_SOCIAL_BIND_RESPONSE : NSObject<STIHTTPResponse>
@end

@interface V1_API_AUTH_SOCIAL_BIND_API : STIHTTPApi
@property (nonatomic, strong) V1_API_AUTH_SOCIAL_BIND_REQUEST *req;
@property (nonatomic, strong) V1_API_AUTH_SOCIAL_BIND_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.auth.social.unbind 解绑第三方账号

@interface V1_API_AUTH_SOCIAL_UNBIND_REQUEST : STIHTTPRequest
@property (nonatomic, assign) SOCIAL_VENDOR vendor; // 第三方平台类型
@end

@interface V1_API_AUTH_SOCIAL_UNBIND_RESPONSE : NSObject<STIHTTPResponse>
@end

@interface V1_API_AUTH_SOCIAL_UNBIND_API : STIHTTPApi
@property (nonatomic, strong) V1_API_AUTH_SOCIAL_UNBIND_REQUEST *req;
@property (nonatomic, strong) V1_API_AUTH_SOCIAL_UNBIND_RESPONSE *resp;
@end

