
#import "STIHTTPNetwork.h"

#import "video-api.h"

#pragma mark - Missing

@class USER;
@class PAGED;
@class VIDEO;

@protocol USER;
@protocol PAGED;
@protocol VIDEO;

#pragma mark - 枚举类型

#pragma mark - Declaration


#pragma mark - Protocols


#pragma mark - Classes

#pragma mark - API

#pragma mark - POST /v1/api.author.recent.join 最近加入的作者

@interface V1_API_AUTHOR_RECENT_JOIN_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSNumber *page; // 当前第几页
@property (nonatomic, strong) NSNumber *per_page; // 每页多少
@end

@interface V1_API_AUTHOR_RECENT_JOIN_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray<USER> *authors; // 最近加入的用户
@property (nonatomic, strong) PAGED *paged; // 分页结果
@end

@interface V1_API_AUTHOR_RECENT_JOIN_API : STIHTTPApi
@property (nonatomic, strong) V1_API_AUTHOR_RECENT_JOIN_REQUEST *req;
@property (nonatomic, strong) V1_API_AUTHOR_RECENT_JOIN_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.author.follow 关注作者

@interface V1_API_AUTHOR_FOLLOW_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *author_id; // 用户ID
@end

@interface V1_API_AUTHOR_FOLLOW_RESPONSE : NSObject<STIHTTPResponse>
@end

@interface V1_API_AUTHOR_FOLLOW_API : STIHTTPApi
@property (nonatomic, strong) V1_API_AUTHOR_FOLLOW_REQUEST *req;
@property (nonatomic, strong) V1_API_AUTHOR_FOLLOW_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.author.unfollow 取消关注作者

@interface V1_API_AUTHOR_UNFOLLOW_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *author_id; // 用户ID
@end

@interface V1_API_AUTHOR_UNFOLLOW_RESPONSE : NSObject<STIHTTPResponse>
@end

@interface V1_API_AUTHOR_UNFOLLOW_API : STIHTTPApi
@property (nonatomic, strong) V1_API_AUTHOR_UNFOLLOW_REQUEST *req;
@property (nonatomic, strong) V1_API_AUTHOR_UNFOLLOW_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.author.follow.list 我的关注列表

@interface V1_API_AUTHOR_FOLLOW_LIST_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSNumber *page; // 当前第几页
@property (nonatomic, strong) NSNumber *per_page; // 每页多少
@end

@interface V1_API_AUTHOR_FOLLOW_LIST_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray<USER> *authors; // 最近加入的用户
@property (nonatomic, strong) PAGED *paged; // 分页结果
@end

@interface V1_API_AUTHOR_FOLLOW_LIST_API : STIHTTPApi
@property (nonatomic, strong) V1_API_AUTHOR_FOLLOW_LIST_REQUEST *req;
@property (nonatomic, strong) V1_API_AUTHOR_FOLLOW_LIST_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.author.video.list 作者动态

@interface V1_API_AUTHOR_VIDEO_LIST_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *author_id; // 作者id
@property (nonatomic, strong) NSNumber *page; // 当前第几页
@property (nonatomic, strong) NSNumber *per_page; // 每页多少
@end

@interface V1_API_AUTHOR_VIDEO_LIST_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray<VIDEO> *videos; // 作者短片动态
@property (nonatomic, strong) PAGED *paged; // 分页结果
@end

@interface V1_API_AUTHOR_VIDEO_LIST_API : STIHTTPApi
@property (nonatomic, strong) V1_API_AUTHOR_VIDEO_LIST_REQUEST *req;
@property (nonatomic, strong) V1_API_AUTHOR_VIDEO_LIST_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.author.report 举报作者

@interface V1_API_AUTHOR_REPORT_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *author_id; // 作者id
@property (nonatomic, assign) REPORT_TYPE type; // 举报类型
@end

@interface V1_API_AUTHOR_REPORT_RESPONSE : NSObject<STIHTTPResponse>
@end

@interface V1_API_AUTHOR_REPORT_API : STIHTTPApi
@property (nonatomic, strong) V1_API_AUTHOR_REPORT_REQUEST *req;
@property (nonatomic, strong) V1_API_AUTHOR_REPORT_RESPONSE *resp;
@end

