
#import "STIHTTPNetwork.h"

#pragma mark - Missing

@class WORMHOLE;
@class AUDIO;

@protocol WORMHOLE;
@protocol AUDIO;

#pragma mark - 枚举类型

#pragma mark - Declaration


#pragma mark - Protocols


#pragma mark - Classes

#pragma mark - API

#pragma mark - POST /v1/api.laboratory.list 实验室数据列表

@interface V1_API_LABORATORY_LIST_REQUEST : STIHTTPRequest
@end

@interface V1_API_LABORATORY_LIST_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray<WORMHOLE> *wormholes; // 虫洞数据（3个）
@property (nonatomic, strong) NSNumber *wormhole_owner_count; // 虫洞参与人数（新建过虫洞的人数）
@property (nonatomic, strong) NSArray<AUDIO> *audios; // 音频数据（3个）
@property (nonatomic, strong) NSNumber *audio_latest_count; // 电台最近上新个数（最近七天上心的个数）
@end

@interface V1_API_LABORATORY_LIST_API : STIHTTPApi
@property (nonatomic, strong) V1_API_LABORATORY_LIST_REQUEST *req;
@property (nonatomic, strong) V1_API_LABORATORY_LIST_RESPONSE *resp;
@end

