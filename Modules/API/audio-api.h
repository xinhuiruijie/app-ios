
#import "STIHTTPNetwork.h"

#pragma mark - Missing

@class USER;
@class PHOTO;

@protocol USER;
@protocol PHOTO;

#pragma mark - 枚举类型

#pragma mark - Declaration

@class AUDIO;

#pragma mark - Protocols

@protocol AUDIO <NSObject> @end

#pragma mark - Classes

@interface AUDIO : NSObject
@property (nonatomic, strong) NSString *id; // id
@property (nonatomic, strong) NSString *title; // 标题
@property (nonatomic, strong) NSString *subtitle; // 简介
@property (nonatomic, strong) USER *owner; // 发表人
@property (nonatomic, strong) NSString *created_at; // 发表时间
@property (nonatomic, strong) PHOTO *photo; // 音频封面图
@property (nonatomic, strong) NSString *audio_url; // 音频URL
@property (nonatomic, strong) NSString *duration; // 音频时长
@end
 
#pragma mark - API

#pragma mark - POST /v1/api.audio.list 音频列表

@interface V1_API_AUDIO_LIST_REQUEST : STIHTTPRequest
@end

@interface V1_API_AUDIO_LIST_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray<AUDIO> *audios; // 分类下的其他短片（不包含精选头条短片）
@end

@interface V1_API_AUDIO_LIST_API : STIHTTPApi
@property (nonatomic, strong) V1_API_AUDIO_LIST_REQUEST * req;
@property (nonatomic, strong) V1_API_AUDIO_LIST_RESPONSE * resp;
@end

#pragma mark - POST /v1/api.audio.report 举报音频

@interface V1_API_AUDIO_REPORT_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *audio_id; // 音频ID
@property (nonatomic, assign) REPORT_TYPE type; // 举报类型
@end

@interface V1_API_AUDIO_REPORT_RESPONSE : NSObject<STIHTTPResponse>
@end

@interface V1_API_AUDIO_REPORT_API : STIHTTPApi
@property (nonatomic, strong) V1_API_AUDIO_REPORT_REQUEST * req;
@property (nonatomic, strong) V1_API_AUDIO_REPORT_RESPONSE * resp;
@end

