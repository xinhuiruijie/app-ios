
#import "STIHTTPNetwork.h"


#pragma mark - 枚举类型


#pragma mark - 操作模块名称

typedef NS_ENUM(NSUInteger, MODULE_NAME) {
    MODULE_NAME_CLICK_REGISTER_FINISH = 0, // 点击注册
    MODULE_NAME_CLICK_LOGIN_FINISH = 1, // 点击登录
    MODULE_NAME_CLICK_THUMBUP_FINISH = 2, // 点击赞按钮
    MODULE_NAME_CLICK_PLAY_FINISH = 3, // 点击播放
    MODULE_NAME_CLICK_SHARE_FINISH = 4, // 点击分享
    MODULE_NAME_CLICK_SHARE_FINISH_WECHAT = 5, // 点击分享微信
    MODULE_NAME_CLICK_SHARE_FINISH_MOMENTS = 6, // 点击分享朋友圈
    MODULE_NAME_CLICK_SHARE_FINISH_WEIBO = 7, // 点击分享微博
    MODULE_NAME_CLICK_SHARE_FINISH_QQ = 8, // 点击分享QQ好友
    MODULE_NAME_CLICK_SHARE_FINISH_QZONE = 9, // 点击分享QQ空间
    MODULE_NAME_CLICK_COLLECT_FINISH = 10, // 点击收藏
    MODULE_NAME_CLICK_SCORE_FINISH = 11, // 点击评论
};

#pragma mark - Declaration

@class BURIAL_POINT;

#pragma mark - Protocols

@protocol BURIAL_POINT <NSObject> @end

#pragma mark - Classes

@interface BURIAL_POINT : NSObject
@property (nonatomic, strong) NSString *id; // 统计的id
@property (nonatomic, assign) MODULE_NAME name; // 操作模块名称
@property (nonatomic, strong) NSString *click_count; // 点击次数
@property (nonatomic, strong) NSString *user_id; // 用户id
@property (nonatomic, strong) NSString *created_at; // 创建时间
@property (nonatomic, strong) NSString *update_at; // 更新时间
@end
 
#pragma mark - API

#pragma mark - POST /v1/api.user.point 个人操作埋点统计

@interface V1_API_USER_POINT_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *name;
@end

@interface V1_API_USER_POINT_RESPONSE : NSObject<STIHTTPResponse>
@end

@interface V1_API_USER_POINT_API : STIHTTPApi
@property (nonatomic, strong) V1_API_USER_POINT_REQUEST *req;
@property (nonatomic, strong) V1_API_USER_POINT_RESPONSE *resp;
@end

