
#import "STIHTTPNetwork.h"

#pragma mark - Missing

@class USER;
@class PHOTO;
@class VIDEO;
@class PAGED;

@protocol USER;
@protocol PHOTO;
@protocol VIDEO;
@protocol PAGED;

#pragma mark - 枚举类型

#pragma mark - Declaration

@class TOPIC;
@class TOPIC_VIDEO;

#pragma mark - Protocols

@protocol TOPIC <NSObject> @end
@protocol TOPIC_VIDEO <NSObject> @end

#pragma mark - Classes

@interface TOPIC : NSObject
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) NSString *title; // 标题
@property (nonatomic, strong) NSString *subtitle; // 副标题
@property (nonatomic, strong) USER *owner; // 话题作者
@property (nonatomic, strong) PHOTO *cover_photo; // 封面图片
@property (nonatomic, strong) PHOTO *photo; // 背景图片
@property (nonatomic, assign) BOOL is_collect; // 是否已收藏
@end
 
@interface TOPIC_VIDEO : NSObject
@property (nonatomic, strong) NSString *id;
@property (nonatomic, strong) VIDEO *video; // 短片
@property (nonatomic, strong) NSString *title; // 标题
@property (nonatomic, strong) NSString *subtitle; // 副标题
@end
 
#pragma mark - API

#pragma mark - POST /v1/api.topic.list 全部话题列表

@interface V1_API_TOPIC_LIST_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSNumber *page; // 当前第几页
@property (nonatomic, strong) NSNumber *per_page; // 每页多少
@end

@interface V1_API_TOPIC_LIST_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray<TOPIC> *topics; // 短片话题
@property (nonatomic, strong) PAGED *paged; // 分页结果
@end

@interface V1_API_TOPIC_LIST_API : STIHTTPApi
@property (nonatomic, strong) V1_API_TOPIC_LIST_REQUEST *req;
@property (nonatomic, strong) V1_API_TOPIC_LIST_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.topic.get 话题详情

@interface V1_API_TOPIC_GET_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *topic_id; // 短片话题ID
@end

@interface V1_API_TOPIC_GET_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) TOPIC *topic; // 短片话题
@end

@interface V1_API_TOPIC_GET_API : STIHTTPApi
@property (nonatomic, strong) V1_API_TOPIC_GET_REQUEST *req;
@property (nonatomic, strong) V1_API_TOPIC_GET_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.topic.favourite.list 我的收藏话题列表

@interface V1_API_TOPIC_FAVOURITE_LIST_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSNumber *page; // 当前第几页
@property (nonatomic, strong) NSNumber *per_page; // 每页多少
@end

@interface V1_API_TOPIC_FAVOURITE_LIST_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray<TOPIC> *topics; // 短片话题
@property (nonatomic, strong) PAGED *paged; // 分页结果
@end

@interface V1_API_TOPIC_FAVOURITE_LIST_API : STIHTTPApi
@property (nonatomic, strong) V1_API_TOPIC_FAVOURITE_LIST_REQUEST *req;
@property (nonatomic, strong) V1_API_TOPIC_FAVOURITE_LIST_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.topic.favourite 收藏短片话题

@interface V1_API_TOPIC_FAVOURITE_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *topic_id; // 话题ID
@end

@interface V1_API_TOPIC_FAVOURITE_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, assign) BOOL is_favourite;
@end

@interface V1_API_TOPIC_FAVOURITE_API : STIHTTPApi
@property (nonatomic, strong) V1_API_TOPIC_FAVOURITE_REQUEST *req;
@property (nonatomic, strong) V1_API_TOPIC_FAVOURITE_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.topic.unfavourite 取消收藏短片话题

@interface V1_API_TOPIC_UNFAVOURITE_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *topic_id; // 短片话题ID
@end

@interface V1_API_TOPIC_UNFAVOURITE_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, assign) BOOL is_favourite;
@end

@interface V1_API_TOPIC_UNFAVOURITE_API : STIHTTPApi
@property (nonatomic, strong) V1_API_TOPIC_UNFAVOURITE_REQUEST *req;
@property (nonatomic, strong) V1_API_TOPIC_UNFAVOURITE_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.topic.favourite.delete 批量删除已收藏的话题

@interface V1_API_TOPIC_FAVOURITE_DELETE_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *topic_ids; // 话题ID
@end

@interface V1_API_TOPIC_FAVOURITE_DELETE_RESPONSE : NSObject<STIHTTPResponse>
@end

@interface V1_API_TOPIC_FAVOURITE_DELETE_API : STIHTTPApi
@property (nonatomic, strong) V1_API_TOPIC_FAVOURITE_DELETE_REQUEST *req;
@property (nonatomic, strong) V1_API_TOPIC_FAVOURITE_DELETE_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.topic.video.list 话题短片列表

@interface V1_API_VIDEO_TOPIC_LIST_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *topic_id; // 话题id
@property (nonatomic, strong) NSNumber *page; // 当前第几页
@property (nonatomic, strong) NSNumber *per_page; // 每页多少
@end

@interface V1_API_VIDEO_TOPIC_LIST_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray<TOPIC_VIDEO> *topic_videos; // 话题短片列表
@property (nonatomic, strong) PAGED *paged; // 分页结果
@end

@interface V1_API_VIDEO_TOPIC_LIST_API : STIHTTPApi
@property (nonatomic, strong) V1_API_VIDEO_TOPIC_LIST_REQUEST *req;
@property (nonatomic, strong) V1_API_VIDEO_TOPIC_LIST_RESPONSE *resp;
@end

