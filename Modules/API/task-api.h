
#import "STIHTTPNetwork.h"

#pragma mark - Missing

@class PHOTO;

@protocol PHOTO;

#pragma mark - 枚举类型


#pragma mark - 

typedef NS_ENUM(NSUInteger, REWARD_TYPE) {
    REWARD_TYPE_ONE_TIME = 1, // 一次性任务(绑定社交平台)
    REWARD_TYPE_DAILY = 2, // 日常任务(登录、观看短片等)
    REWARD_TYPE_INVITATE = 3, // 邀请好友任务
};

#pragma mark - Declaration

@class TASK;

#pragma mark - Protocols

@protocol TASK <NSObject> @end

#pragma mark - Classes

@interface TASK : NSObject
@property (nonatomic, strong) NSString *id; // id
@property (nonatomic, strong) NSString *title; // 名称  
@property (nonatomic, strong) PHOTO *icon; // 图片   
@property (nonatomic, strong) NSString *code; // 任务操作，返回字符串，bindqq bindweibo bindwechat，通过判断字符串来跳转绑定第三方平台的类型         
@property (nonatomic, strong) NSNumber *times; // 需要完成的次数  
@property (nonatomic, strong) NSNumber *finish_times; // 已完成的次数 
@property (nonatomic, strong) NSString *reward; // 奖励  
@property (nonatomic, assign) REWARD_TYPE type; // 任务类型  
@property (nonatomic, assign) BOOL completed; // 任务是否完成  
@end
 
#pragma mark - API

#pragma mark - POST /v1/api.task.list 任务列表

@interface V1_API_TASK_LIST_REQUEST : STIHTTPRequest
@end

@interface V1_API_TASK_LIST_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray<TASK> *tasks; // 任务列表
@end

@interface V1_API_TASK_LIST_API : STIHTTPApi
@property (nonatomic, strong) V1_API_TASK_LIST_REQUEST *req;
@property (nonatomic, strong) V1_API_TASK_LIST_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.task.finish.invite 邀请好友成功

@interface V1_API_TASK_FINISH_INVITE_REQUEST : STIHTTPRequest
@end

@interface V1_API_TASK_FINISH_INVITE_RESPONSE : NSObject<STIHTTPResponse>
@end

@interface V1_API_TASK_FINISH_INVITE_API : STIHTTPApi
@property (nonatomic, strong) V1_API_TASK_FINISH_INVITE_REQUEST *req;
@property (nonatomic, strong) V1_API_TASK_FINISH_INVITE_RESPONSE *resp;
@end

