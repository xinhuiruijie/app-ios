
#import "home-api.h"

#pragma mark - Classes

@implementation BANNER
@end
 
@implementation CATEGORY
@end
 
@implementation TAG
@end
 
@implementation CARD
@end
 

#pragma mark - API

#pragma mark - POST /v1/api.banner.list banner

@implementation V1_API_BANNER_LIST_REQUEST
@end

@implementation V1_API_BANNER_LIST_RESPONSE
@end

@implementation V1_API_BANNER_LIST_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_BANNER_LIST_REQUEST requestWithEndpoint:@"/v1/api.banner.list" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_BANNER_LIST_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.home.topic.list 首页-推荐话题

@implementation V1_API_HOME_TOPIC_LIST_REQUEST
@end

@implementation V1_API_HOME_TOPIC_LIST_RESPONSE
@end

@implementation V1_API_HOME_TOPIC_LIST_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_HOME_TOPIC_LIST_REQUEST requestWithEndpoint:@"/v1/api.home.topic.list" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_HOME_TOPIC_LIST_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.home.video.list 首页-推荐短片

@implementation V1_API_HOME_VIDEO_LIST_REQUEST
@end

@implementation V1_API_HOME_VIDEO_LIST_RESPONSE
@end

@implementation V1_API_HOME_VIDEO_LIST_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_HOME_VIDEO_LIST_REQUEST requestWithEndpoint:@"/v1/api.home.video.list" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_HOME_VIDEO_LIST_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.home.copywriting.refreshen 首页刷新特定提示文案

@implementation V1_API_HOME_COPYWRITING_REFRESHEN_REQUEST
@end

@implementation V1_API_HOME_COPYWRITING_REFRESHEN_RESPONSE
@end

@implementation V1_API_HOME_COPYWRITING_REFRESHEN_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_HOME_COPYWRITING_REFRESHEN_REQUEST requestWithEndpoint:@"/v1/api.home.copywriting.refreshen" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_HOME_COPYWRITING_REFRESHEN_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.category.list 全部分类列表

@implementation V1_API_CATEGORY_LIST_REQUEST
@end

@implementation V1_API_CATEGORY_LIST_RESPONSE
@end

@implementation V1_API_CATEGORY_LIST_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_CATEGORY_LIST_REQUEST requestWithEndpoint:@"/v1/api.category.list" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_CATEGORY_LIST_RESPONSE class];
    }
    return self;
}

@end

