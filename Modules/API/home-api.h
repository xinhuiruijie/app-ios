
#import "STIHTTPNetwork.h"

#pragma mark - Missing

@class PHOTO;
@class TOPIC;
@class VIDEO;
@class PAGED;

@protocol PHOTO;
@protocol TOPIC;
@protocol VIDEO;
@protocol PAGED;

#pragma mark - 枚举类型


#pragma mark - 

typedef NS_ENUM(NSUInteger, BANNER_TYPE) {
    BANNER_TYPE_HOME = 1, // 首页
    BANNER_TYPE_PLANET = 2, // 星球
};

#pragma mark - Declaration

@class BANNER;
@class CATEGORY;
@class TAG;
@class CARD;

#pragma mark - Protocols

@protocol BANNER <NSObject> @end
@protocol CATEGORY <NSObject> @end
@protocol TAG <NSObject> @end
@protocol CARD <NSObject> @end

#pragma mark - Classes

@interface BANNER : NSObject
@property (nonatomic, strong) NSString * id; // 位置id
@property (nonatomic, strong) PHOTO * photo; // 图片
@property (nonatomic, strong) NSString * title; // 标题
@property (nonatomic, strong) NSString * subtitle; // 副标题
@property (nonatomic, strong) NSString * link; // 内链或外链
@property (nonatomic, strong) NSString * created_at; // 创建时间
@property (nonatomic, strong) NSString * updated_at; // 更新时间
@end
 
@interface CATEGORY : NSObject
@property (nonatomic, strong) NSString * id;
@property (nonatomic, strong) NSString * title; // 标题
@property (nonatomic, strong) PHOTO * photo; // 背景图片
@end
 
@interface TAG : NSObject
@property (nonatomic, strong) NSString * id;
@property (nonatomic, strong) NSString * name; // 名称
@end
 
@interface CARD : NSObject
@property (nonatomic, strong) NSString * id; // 唯一id
@property (nonatomic, strong) PHOTO * photo; // 图片
@property (nonatomic, strong) NSString * link; // 外链
@property (nonatomic, strong) NSString * title; // 标题
@property (nonatomic, strong) NSString * subtitle; // 子标题
@end
 
#pragma mark - API

#pragma mark - POST /v1/api.banner.list banner

@interface V1_API_BANNER_LIST_REQUEST : STIHTTPRequest
@property (nonatomic, assign) BANNER_TYPE banner_type; // banner类型（首页banner和热议banner）
@end

@interface V1_API_BANNER_LIST_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray<BANNER> * banners; //
@end

@interface V1_API_BANNER_LIST_API : STIHTTPApi
@property (nonatomic, strong) V1_API_BANNER_LIST_REQUEST * req;
@property (nonatomic, strong) V1_API_BANNER_LIST_RESPONSE * resp;
@end

#pragma mark - POST /v1/api.home.topic.list 首页-推荐话题

@interface V1_API_HOME_TOPIC_LIST_REQUEST : STIHTTPRequest
@end

@interface V1_API_HOME_TOPIC_LIST_RESPONSE : NSObject<STIHTTPResponse>
//@property (nonatomic, strong) NSArray<CARD> * cards; // 卡片（原首页显示分类的地方）
@property (nonatomic, strong) NSArray<TOPIC> * topics; // 首页推荐话题
@end

@interface V1_API_HOME_TOPIC_LIST_API : STIHTTPApi
@property (nonatomic, strong) V1_API_HOME_TOPIC_LIST_REQUEST * req;
@property (nonatomic, strong) V1_API_HOME_TOPIC_LIST_RESPONSE * resp;
@end

#pragma mark - POST /v1/api.home.video.list 首页-推荐短片

@interface V1_API_HOME_VIDEO_LIST_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSNumber * page; // 当前第几页
@property (nonatomic, strong) NSNumber * per_page; // 每页多少  
@end

@interface V1_API_HOME_VIDEO_LIST_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray<VIDEO> * videos; // 推荐短片
@property (nonatomic, strong) PAGED * paged; // 分页结果
@end

@interface V1_API_HOME_VIDEO_LIST_API : STIHTTPApi
@property (nonatomic, strong) V1_API_HOME_VIDEO_LIST_REQUEST * req;
@property (nonatomic, strong) V1_API_HOME_VIDEO_LIST_RESPONSE * resp;
@end

#pragma mark - POST /v1/api.home.copywriting.refreshen 首页刷新特定提示文案

@interface V1_API_HOME_COPYWRITING_REFRESHEN_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *last_request; // 上次拉取首页短片 时间
@end

@interface V1_API_HOME_COPYWRITING_REFRESHEN_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSString *copywriting; // 为null时 客户端默认展示（更了n条短片）
@end

@interface V1_API_HOME_COPYWRITING_REFRESHEN_API : STIHTTPApi
@property (nonatomic, strong) V1_API_HOME_COPYWRITING_REFRESHEN_REQUEST * req;
@property (nonatomic, strong) V1_API_HOME_COPYWRITING_REFRESHEN_RESPONSE * resp;
@end

#pragma mark - POST /v1/api.category.list 全部分类列表

@interface V1_API_CATEGORY_LIST_REQUEST : STIHTTPRequest
@end

@interface V1_API_CATEGORY_LIST_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray<CATEGORY> * category; // 分类
@end

@interface V1_API_CATEGORY_LIST_API : STIHTTPApi
@property (nonatomic, strong) V1_API_CATEGORY_LIST_REQUEST * req;
@property (nonatomic, strong) V1_API_CATEGORY_LIST_RESPONSE * resp;
@end

