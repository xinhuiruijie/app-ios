
#import "laboratory-api.h"

#pragma mark - Classes


#pragma mark - API

#pragma mark - POST /v1/api.laboratory.list 实验室数据列表

@implementation V1_API_LABORATORY_LIST_REQUEST
@end

@implementation V1_API_LABORATORY_LIST_RESPONSE
@end

@implementation V1_API_LABORATORY_LIST_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_LABORATORY_LIST_REQUEST requestWithEndpoint:@"/v1/api.laboratory.list" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_LABORATORY_LIST_RESPONSE class];
    }
    return self;
}

@end

