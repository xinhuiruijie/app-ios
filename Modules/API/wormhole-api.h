
#import "STIHTTPNetwork.h"

#pragma mark - Missing

@class USER;
@class PHOTO;
@class VIDEO;
@class PAGED;

@protocol USER;
@protocol PHOTO;
@protocol VIDEO;
@protocol PAGED;

#pragma mark - 枚举类型


#pragma mark - 

typedef NS_ENUM(NSUInteger, WORMHOLE_SORT) {
    WORMHOLE_SORT_LATEST = 1, // 最新
    WORMHOLE_SORT_HOT = 2, // 最热
};


#pragma mark - 

typedef NS_ENUM(NSUInteger, WORMHOLE_STATUS) {
    WORMHOLE_STATUS_PENDING = 0, // 待审核
    WORMHOLE_STATUS_PASSED = 1, // 审核通过
    WORMHOLE_STATUS_UNPASSED = 2, // 审核未通过
};

#pragma mark - Declaration

@class WORMHOLE;

#pragma mark - Protocols

@protocol WORMHOLE <NSObject> @end

#pragma mark - Classes

@interface WORMHOLE : NSObject
@property (nonatomic, strong) NSString *id; // id
@property (nonatomic, strong) NSString *title; // 名称
@property (nonatomic, strong) NSString *subtitle; // 标签
@property (nonatomic, strong) NSString *content; // 简介
@property (nonatomic, strong) USER *owner; // 创建人
@property (nonatomic, strong) NSString *created_at; // 创建时间
@property (nonatomic, strong) PHOTO *photo; // 虫洞封面图
@property (nonatomic, strong) NSArray<VIDEO> *videos; // 虫洞短片
@property (nonatomic, strong) NSNumber *user_count; // 点击虫洞的用户数
@end
 
#pragma mark - API

#pragma mark - POST /v1/api.wormhole.list 虫洞列表

@interface V1_API_WORMHOLE_LIST_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSNumber *page; // 当前第几页
@property (nonatomic, strong) NSNumber *per_page; // 每页多少
@property (nonatomic, assign) WORMHOLE_SORT sort; // 排序方式
@end

@interface V1_API_WORMHOLE_LIST_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray<WORMHOLE> *wormholes; // 虫洞列表
@property (nonatomic, strong) PAGED *paged; // 分页结果
@end

@interface V1_API_WORMHOLE_LIST_API : STIHTTPApi
@property (nonatomic, strong) V1_API_WORMHOLE_LIST_REQUEST *req;
@property (nonatomic, strong) V1_API_WORMHOLE_LIST_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.wormhole.get 虫洞详情

@interface V1_API_WORMHOLE_GET_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *wormhole_id; // 虫洞ID
@end

@interface V1_API_WORMHOLE_GET_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) WORMHOLE *wormhole; // 虫洞详情
@end

@interface V1_API_WORMHOLE_GET_API : STIHTTPApi
@property (nonatomic, strong) V1_API_WORMHOLE_GET_REQUEST *req;
@property (nonatomic, strong) V1_API_WORMHOLE_GET_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.wormhole.create 新建虫洞

@interface V1_API_WORMHOLE_CREATE_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *title; // 名称
@property (nonatomic, strong) NSString *subtitle; // 标签
@property (nonatomic, strong) NSString *content; // 简介
@property (nonatomic, strong) NSString *photo; // 虫洞封面图
@end

@interface V1_API_WORMHOLE_CREATE_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) WORMHOLE *wormhole; // 虫洞详情
@end

@interface V1_API_WORMHOLE_CREATE_API : STIHTTPApi
@property (nonatomic, strong) V1_API_WORMHOLE_CREATE_REQUEST *req;
@property (nonatomic, strong) V1_API_WORMHOLE_CREATE_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.wormhole.delete 删除虫洞

@interface V1_API_WORMHOLE_DELETE_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *wormhole_id; // 虫洞ID
@end

@interface V1_API_WORMHOLE_DELETE_RESPONSE : NSObject<STIHTTPResponse>
@end

@interface V1_API_WORMHOLE_DELETE_API : STIHTTPApi
@property (nonatomic, strong) V1_API_WORMHOLE_DELETE_REQUEST *req;
@property (nonatomic, strong) V1_API_WORMHOLE_DELETE_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.user.wormhole.list 用户的虫洞列表

@interface V1_API_USER_WORMHOLE_LIST_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *user_id; // 用户的虫洞列表(可以查看他人的虫洞列表)
@property (nonatomic, assign) WORMHOLE_STATUS status; // 用户审核状态
@property (nonatomic, strong) NSNumber *page; // 当前第几页
@property (nonatomic, strong) NSNumber *per_page; // 每页多少
@end

@interface V1_API_USER_WORMHOLE_LIST_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray<WORMHOLE> *wormholes; // 虫洞列表
@property (nonatomic, strong) PAGED *paged; // 分页结果
@end

@interface V1_API_USER_WORMHOLE_LIST_API : STIHTTPApi
@property (nonatomic, strong) V1_API_USER_WORMHOLE_LIST_REQUEST *req;
@property (nonatomic, strong) V1_API_USER_WORMHOLE_LIST_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.wormhole.add.video 添加短片到我的虫洞

@interface V1_API_WORMHOLE_ADD_VIDEO_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *video_id; // 短片id
@property (nonatomic, strong) NSString *wormhole_id; // 虫洞ID
@end

@interface V1_API_WORMHOLE_ADD_VIDEO_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) WORMHOLE *wormhole; // 虫洞详情
@end

@interface V1_API_WORMHOLE_ADD_VIDEO_API : STIHTTPApi
@property (nonatomic, strong) V1_API_WORMHOLE_ADD_VIDEO_REQUEST *req;
@property (nonatomic, strong) V1_API_WORMHOLE_ADD_VIDEO_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.wormhole.delete.video 从我的某一虫洞里删除某一短片

@interface V1_API_WORMHOLE_DELETE_VIDEO_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *video_id; // 短片id
@property (nonatomic, strong) NSString *wormhole_id; // 虫洞ID
@end

@interface V1_API_WORMHOLE_DELETE_VIDEO_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) WORMHOLE *wormhole; // 虫洞详情
@end

@interface V1_API_WORMHOLE_DELETE_VIDEO_API : STIHTTPApi
@property (nonatomic, strong) V1_API_WORMHOLE_DELETE_VIDEO_REQUEST *req;
@property (nonatomic, strong) V1_API_WORMHOLE_DELETE_VIDEO_RESPONSE *resp;
@end

