
#import "STIHTTPNetwork.h"


#pragma mark - 枚举类型

#pragma mark - Declaration

@class PAGED;
@class PHOTO;

#pragma mark - Protocols

@protocol PAGED <NSObject> @end
@protocol PHOTO <NSObject> @end

#pragma mark - Classes

@interface PAGED : NSObject
@property (nonatomic, strong) NSNumber * total; // 总数
@property (nonatomic, strong) NSNumber * page; // 页号
@property (nonatomic, strong) NSNumber * count; // 每页数量
@property (nonatomic, strong) NSNumber * more; // 是否还有更多
@end
 
@interface PHOTO : NSObject
@property (nonatomic, strong) NSNumber * width; // 宽度 px
@property (nonatomic, strong) NSNumber * height; // 宽度 px
@property (nonatomic, strong) NSString * thumb; // 预览图
@property (nonatomic, strong) NSString * large; // 高清图
@property (nonatomic, strong) NSString * thumb_square;  // 方形预览图，供分享使用
@end
 
#pragma mark - API

