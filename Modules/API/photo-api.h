
#import "STIHTTPNetwork.h"


#pragma mark - 枚举类型

#pragma mark - Declaration


#pragma mark - Protocols


#pragma mark - Classes

#pragma mark - API

#pragma mark - POST /v1/api.photo.upload 图片上传

@interface V1_API_PHOTO_UPLOAD_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString * file; // 文件
@end

@interface V1_API_PHOTO_UPLOAD_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSString * url; // 图片地址
@end

@interface V1_API_PHOTO_UPLOAD_API : STIHTTPApi
@property (nonatomic, strong) V1_API_PHOTO_UPLOAD_REQUEST * req;
@property (nonatomic, strong) V1_API_PHOTO_UPLOAD_RESPONSE * resp;
@end

