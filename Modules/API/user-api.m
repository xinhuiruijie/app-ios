
#import "user-api.h"

#pragma mark - Classes

@implementation USER
@end
 
@implementation TIMELINE
@end
 

#pragma mark - API

#pragma mark - POST /v1/api.user.profile.get 获取用户资料

@implementation V1_API_USER_PROFILE_GET_REQUEST
@end

@implementation V1_API_USER_PROFILE_GET_RESPONSE
@end

@implementation V1_API_USER_PROFILE_GET_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_USER_PROFILE_GET_REQUEST requestWithEndpoint:@"/v1/api.user.profile.get" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_USER_PROFILE_GET_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.user.profile.update 修改用户资料

@implementation V1_API_USER_PROFILE_UPDATE_REQUEST
@end

@implementation V1_API_USER_PROFILE_UPDATE_RESPONSE
@end

@implementation V1_API_USER_PROFILE_UPDATE_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_USER_PROFILE_UPDATE_REQUEST requestWithEndpoint:@"/v1/api.user.profile.update" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_USER_PROFILE_UPDATE_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.user.password.update 修改用户密码

@implementation V1_API_USER_PASSWORD_UPDATE_REQUEST
@end

@implementation V1_API_USER_PASSWORD_UPDATE_RESPONSE
@end

@implementation V1_API_USER_PASSWORD_UPDATE_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_USER_PASSWORD_UPDATE_REQUEST requestWithEndpoint:@"/v1/api.user.password.update" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_USER_PASSWORD_UPDATE_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.user.timeline.list 我的动态列表 (我点赞/关注/评论/转发(分享)了XXX)

@implementation V1_API_USER_TIMELINE_LIST_REQUEST
@end

@implementation V1_API_USER_TIMELINE_LIST_RESPONSE
@end

@implementation V1_API_USER_TIMELINE_LIST_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_USER_TIMELINE_LIST_REQUEST requestWithEndpoint:@"/v1/api.user.timeline.list" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_USER_TIMELINE_LIST_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.data.flow.query 查询定向流量使用情况

@implementation V1_API_DATA_FLOW_QUERY_REQUEST
@end

@implementation V1_API_DATA_FLOW_QUERY_RESPONSE
@end

@implementation V1_API_DATA_FLOW_QUERY_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_DATA_FLOW_QUERY_REQUEST requestWithEndpoint:@"/v1/api.data.flow.query" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_DATA_FLOW_QUERY_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.user.blacklist 我的黑名单

@implementation V1_API_USER_BLACKLIST_REQUEST
@end

@implementation V1_API_USER_BLACKLIST_RESPONSE
@end

@implementation V1_API_USER_BLACKLIST_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_USER_BLACKLIST_REQUEST requestWithEndpoint:@"/v1/api.user.blacklist" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_USER_BLACKLIST_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.user.blacklist.delete 移除我的黑名单

@implementation V1_API_USER_BLACKLIST_DELETE_REQUEST
@end

@implementation V1_API_USER_BLACKLIST_DELETE_RESPONSE
@end

@implementation V1_API_USER_BLACKLIST_DELETE_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_USER_BLACKLIST_DELETE_REQUEST requestWithEndpoint:@"/v1/api.user.blacklist.delete" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_USER_BLACKLIST_DELETE_RESPONSE class];
    }
    return self;
}

@end

