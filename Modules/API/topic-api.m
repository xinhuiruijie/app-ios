
#import "topic-api.h"

#pragma mark - Classes

@implementation TOPIC
@end
 
@implementation TOPIC_VIDEO
@end
 

#pragma mark - API

#pragma mark - POST /v1/api.topic.list 全部话题列表

@implementation V1_API_TOPIC_LIST_REQUEST
@end

@implementation V1_API_TOPIC_LIST_RESPONSE
@end

@implementation V1_API_TOPIC_LIST_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_TOPIC_LIST_REQUEST requestWithEndpoint:@"/v1/api.topic.list" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_TOPIC_LIST_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.topic.get 话题详情

@implementation V1_API_TOPIC_GET_REQUEST
@end

@implementation V1_API_TOPIC_GET_RESPONSE
@end

@implementation V1_API_TOPIC_GET_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_TOPIC_GET_REQUEST requestWithEndpoint:@"/v1/api.topic.get" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_TOPIC_GET_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.topic.favourite.list 我的收藏话题列表

@implementation V1_API_TOPIC_FAVOURITE_LIST_REQUEST
@end

@implementation V1_API_TOPIC_FAVOURITE_LIST_RESPONSE
@end

@implementation V1_API_TOPIC_FAVOURITE_LIST_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_TOPIC_FAVOURITE_LIST_REQUEST requestWithEndpoint:@"/v1/api.topic.favourite.list" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_TOPIC_FAVOURITE_LIST_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.topic.favourite 收藏短片话题

@implementation V1_API_TOPIC_FAVOURITE_REQUEST
@end

@implementation V1_API_TOPIC_FAVOURITE_RESPONSE
@end

@implementation V1_API_TOPIC_FAVOURITE_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_TOPIC_FAVOURITE_REQUEST requestWithEndpoint:@"/v1/api.topic.favourite" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_TOPIC_FAVOURITE_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.topic.unfavourite 取消收藏短片话题

@implementation V1_API_TOPIC_UNFAVOURITE_REQUEST
@end

@implementation V1_API_TOPIC_UNFAVOURITE_RESPONSE
@end

@implementation V1_API_TOPIC_UNFAVOURITE_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_TOPIC_UNFAVOURITE_REQUEST requestWithEndpoint:@"/v1/api.topic.unfavourite" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_TOPIC_UNFAVOURITE_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.topic.favourite.delete 批量删除已收藏的话题

@implementation V1_API_TOPIC_FAVOURITE_DELETE_REQUEST
@end

@implementation V1_API_TOPIC_FAVOURITE_DELETE_RESPONSE
@end

@implementation V1_API_TOPIC_FAVOURITE_DELETE_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_TOPIC_FAVOURITE_DELETE_REQUEST requestWithEndpoint:@"/v1/api.topic.favourite.delete" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_TOPIC_FAVOURITE_DELETE_RESPONSE class];
    }
    return self;
}

@end

#pragma mark - POST /v1/api.topic.video.list 话题短片列表

@implementation V1_API_VIDEO_TOPIC_LIST_REQUEST
@end

@implementation V1_API_VIDEO_TOPIC_LIST_RESPONSE
@end

@implementation V1_API_VIDEO_TOPIC_LIST_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_VIDEO_TOPIC_LIST_REQUEST requestWithEndpoint:@"/v1/api.topic.video.list" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_VIDEO_TOPIC_LIST_RESPONSE class];
    }
    return self;
}

@end

