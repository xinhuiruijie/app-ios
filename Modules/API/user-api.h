
#import "STIHTTPNetwork.h"

#pragma mark - Missing

@class PHOTO;
@class ARTICLE;
@class VIDEO;
@class PAGED;

@protocol PHOTO;
@protocol ARTICLE;
@protocol VIDEO;
@protocol PAGED;

#pragma mark - 枚举类型


#pragma mark - 用户性别

typedef NS_ENUM(NSUInteger, USER_GENDER) {
    USER_GENDER_UNKNOWN = 1, // 保密
    USER_GENDER_MALE = 2, // 男
    USER_GENDER_FEMALE = 3, // 女
};


#pragma mark - 动态类型

typedef NS_ENUM(NSUInteger, TIMELINE_TYPE) {
    TIMELINE_TYPE_LIKE = 1, // 点赞
    TIMELINE_TYPE_FOLLOW = 2, // 关注
    TIMELINE_TYPE_COMMENT = 3, // 评论
    TIMELINE_TYPE_SHARE = 4, // 分享
    TIMELINE_TYPE_COLLECT = 5, // 收藏
    TIMELINE_TYPE_STAR = 6, // 评分
};


#pragma mark - 动态引用数据类型

typedef NS_ENUM(NSUInteger, TIMELINE_REFERENCE_TYPE) {
    TIMELINE_REFERENCE_TYPE_ARTICLE = 1, // 文章
    TIMELINE_REFERENCE_TYPE_VIDEO = 2, // 短片
    TIMELINE_REFERENCE_TYPE_AUTHOR = 3, // 博主
};

#pragma mark - Declaration

@class USER;
@class TIMELINE;

#pragma mark - Protocols

@protocol USER <NSObject> @end
@protocol TIMELINE <NSObject> @end

#pragma mark - Classes

@interface USER : NSObject
@property (nonatomic, strong) NSString *id; // 用户id
@property (nonatomic, assign) USER_GENDER gender; // 性别
@property (nonatomic, strong) NSNumber *score; // 积分
@property (nonatomic, strong) NSString *name; // 姓名            
@property (nonatomic, strong) NSString *email; // Email
@property (nonatomic, strong) NSString *mobile; // 手机号
@property (nonatomic, strong) NSString *introduce; // 个人简介             
@property (nonatomic, strong) PHOTO *avatar; // 头像            
@property (nonatomic, strong) PHOTO *cover_photo; // 封面图片
@property (nonatomic, strong) NSString *constellation; // 星座（水瓶座, 双鱼座, 白羊座, 金牛座, 双子座, 巨蟹座, 狮子座, 处女座, 天秤座, 天蝎座, 射手座, 摩羯座） 
@property (nonatomic, strong) NSString *location; // 所在地
@property (nonatomic, strong) NSString *birthday; // 生日
@property (nonatomic, strong) NSNumber *post_count; // 发帖总数            
@property (nonatomic, strong) NSNumber *fans_count; // 粉丝个数          
@property (nonatomic, strong) NSNumber *follow_count; // 关注个数
@property (nonatomic, strong) NSNumber *unread_message_count; // 未读消息个数
@property (nonatomic, strong) NSNumber *watch_video_count; // 足迹短片个数
@property (nonatomic, strong) NSNumber *collect_count; // 收藏个数
@property (nonatomic, strong) NSNumber *video_count; // 短片个数
@property (nonatomic, strong) NSNumber *star_score; // 口碑分数（该用户名下所有短片短片的评分平均值）
@property (nonatomic, assign) BOOL is_bind_mobile; // 是否绑定了手机号
@property (nonatomic, assign) BOOL is_bind_pwd; // 是否设置了密码
@property (nonatomic, assign) BOOL is_auth; // 是否是第三方登录
@property (nonatomic, assign) BOOL is_author; // 是否是作者
@property (nonatomic, assign) BOOL is_bind_wechat; // 是否绑定微信账号
@property (nonatomic, assign) BOOL is_bind_weibo; // 是否绑定微博账号
@property (nonatomic, assign) BOOL is_bind_QQ; // 是否绑定QQ账号
@property (nonatomic, assign) BOOL is_follow; // 是否已关注（获取他人个人资料时有值）                                
@property (nonatomic, strong) NSString *created_at; // 加入时间
@property (nonatomic, strong) NSString *QQ_nickname; // QQ 昵称
@property (nonatomic, strong) NSString *wechat_nickname; // 微信 昵称
@property (nonatomic, strong) NSString *weibo_nickname; // 微博 昵称
@property (nonatomic, assign) BOOL completed; // 是否完善资料
@end
 
@interface TIMELINE : NSObject
@property (nonatomic, strong) NSNumber *id;
@property (nonatomic, assign) TIMELINE_TYPE type; // 动态类型
@property (nonatomic, assign) TIMELINE_REFERENCE_TYPE reference_type; // 动态引用数据类型
@property (nonatomic, strong) NSString *link; // link
@property (nonatomic, strong) ARTICLE *article; // 根据动态引用数据类型判断，为文章类型时才返回
@property (nonatomic, strong) VIDEO *video; // 根据动态引用数据类型判断，为短片类型时才返回
@property (nonatomic, strong) USER *author; // 根据动态引用数据类型判断，为关注博主类型时才返回
@property (nonatomic, strong) NSString *content; // 评论类型的时候，返回评论内容。其他类型不返回
@property (nonatomic, strong) NSString *created_at; // 时间
@property (nonatomic, strong) NSString *show_string; // 返回标题
@end
 
#pragma mark - API

#pragma mark - POST /v1/api.user.profile.get 获取用户资料

@interface V1_API_USER_PROFILE_GET_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *user_id; // 用户id（获取他人个人资料时传值）
@end

@interface V1_API_USER_PROFILE_GET_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) USER *user; // 用户资料
@end

@interface V1_API_USER_PROFILE_GET_API : STIHTTPApi
@property (nonatomic, strong) V1_API_USER_PROFILE_GET_REQUEST *req;
@property (nonatomic, strong) V1_API_USER_PROFILE_GET_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.user.profile.update 修改用户资料

@interface V1_API_USER_PROFILE_UPDATE_REQUEST : STIHTTPRequest
@property (nonatomic, assign) USER_GENDER gender; // 性别
@property (nonatomic, strong) NSString *name; // 姓名            
@property (nonatomic, strong) NSString *email; // Email
@property (nonatomic, strong) NSString *birthday; // 生日
@property (nonatomic, strong) NSString *introduce; // 个人签名
@property (nonatomic, strong) NSString *avatar; // 用户头像  
@property (nonatomic, strong) NSString *cover_photo; // 封面图片
@property (nonatomic, strong) NSString *constellation; // 星座
@property (nonatomic, strong) NSString *location; // 地址
@end

@interface V1_API_USER_PROFILE_UPDATE_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) USER *user; // 用户资料
@end

@interface V1_API_USER_PROFILE_UPDATE_API : STIHTTPApi
@property (nonatomic, strong) V1_API_USER_PROFILE_UPDATE_REQUEST *req;
@property (nonatomic, strong) V1_API_USER_PROFILE_UPDATE_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.user.password.update 修改用户密码

@interface V1_API_USER_PASSWORD_UPDATE_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *old_password; // 旧密码
@property (nonatomic, strong) NSString *password; // 新密码
@end

@interface V1_API_USER_PASSWORD_UPDATE_RESPONSE : NSObject<STIHTTPResponse>
@end

@interface V1_API_USER_PASSWORD_UPDATE_API : STIHTTPApi
@property (nonatomic, strong) V1_API_USER_PASSWORD_UPDATE_REQUEST *req;
@property (nonatomic, strong) V1_API_USER_PASSWORD_UPDATE_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.user.timeline.list 我的动态列表 (我点赞/关注/评论/转发(分享)了XXX)

@interface V1_API_USER_TIMELINE_LIST_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *user_id; // 获取他人动态传值
@property (nonatomic, strong) NSNumber *page; // 当前第几页
@property (nonatomic, strong) NSNumber *per_page; // 每页多少
@end

@interface V1_API_USER_TIMELINE_LIST_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) PAGED *paged; // 分页结果
@property (nonatomic, strong) NSArray<TIMELINE> *timeline; // 我的动态列表
@end

@interface V1_API_USER_TIMELINE_LIST_API : STIHTTPApi
@property (nonatomic, strong) V1_API_USER_TIMELINE_LIST_REQUEST *req;
@property (nonatomic, strong) V1_API_USER_TIMELINE_LIST_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.data.flow.query 查询定向流量使用情况

@interface V1_API_DATA_FLOW_QUERY_REQUEST : STIHTTPRequest
@end

@interface V1_API_DATA_FLOW_QUERY_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, assign) BOOL has_dataflow; // 是否有流量套餐，有流量套餐的时候下面三个字段才有值
@property (nonatomic, strong) NSNumber *total_flow; // 订购包总流量
@property (nonatomic, strong) NSNumber *used_flow; // 用户使用流量
@property (nonatomic, strong) NSNumber *surplus_flow; // 剩余流量
@end

@interface V1_API_DATA_FLOW_QUERY_API : STIHTTPApi
@property (nonatomic, strong) V1_API_DATA_FLOW_QUERY_REQUEST *req;
@property (nonatomic, strong) V1_API_DATA_FLOW_QUERY_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.user.blacklist 我的黑名单

@interface V1_API_USER_BLACKLIST_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSNumber *page; // 当前第几页
@property (nonatomic, strong) NSNumber *per_page; // 每页多少
@end

@interface V1_API_USER_BLACKLIST_RESPONSE : NSObject<STIHTTPResponse>
@property (nonatomic, strong) NSArray<USER> *users; // 我的黑名单
@property (nonatomic, strong) PAGED *paged; // 分页结果
@end

@interface V1_API_USER_BLACKLIST_API : STIHTTPApi
@property (nonatomic, strong) V1_API_USER_BLACKLIST_REQUEST *req;
@property (nonatomic, strong) V1_API_USER_BLACKLIST_RESPONSE *resp;
@end

#pragma mark - POST /v1/api.user.blacklist.delete 移除我的黑名单

@interface V1_API_USER_BLACKLIST_DELETE_REQUEST : STIHTTPRequest
@property (nonatomic, strong) NSString *user_id; // 用户id
@end

@interface V1_API_USER_BLACKLIST_DELETE_RESPONSE : NSObject<STIHTTPResponse>
@end

@interface V1_API_USER_BLACKLIST_DELETE_API : STIHTTPApi
@property (nonatomic, strong) V1_API_USER_BLACKLIST_DELETE_REQUEST *req;
@property (nonatomic, strong) V1_API_USER_BLACKLIST_DELETE_RESPONSE *resp;
@end

