
#import "photo-api.h"

#pragma mark - Classes


#pragma mark - API

#pragma mark - POST /v1/api.photo.upload 图片上传

@implementation V1_API_PHOTO_UPLOAD_REQUEST
@end

@implementation V1_API_PHOTO_UPLOAD_RESPONSE
@end

@implementation V1_API_PHOTO_UPLOAD_API

@synthesize req = _req;
@synthesize resp = _resp;

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.req = [V1_API_PHOTO_UPLOAD_REQUEST requestWithEndpoint:@"/v1/api.photo.upload" method:STIHTTPRequestMethodPost];
        self.req.responseClass = [V1_API_PHOTO_UPLOAD_RESPONSE class];
    }
    return self;
}

@end

