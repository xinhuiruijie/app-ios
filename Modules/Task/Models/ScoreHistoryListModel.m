//
//  ScoreHistoryListModel.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/30.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "ScoreHistoryListModel.h"

@implementation ScoreHistoryListModel

- (instancetype)init {
    if (self = [super init]) {
        self.items = [NSMutableArray array];
    }
    return self;
}

- (void)loadCache {
    NSArray *array = [self loadCacheWithKey:self.cacheKey objectClass:[SCORE_ORDER class]];
    [self.items removeAllObjects];
    if (array.count) {
        [self.items addObjectsFromArray:array];
    }
}

- (void)saveCache {
    [self saveCache:self.items key:self.cacheKey];
}

- (NSString *)cacheKey {
    return [[self class] description];
}

- (void)refresh {
    V1_API_SCORE_HISTORY_LIST_API * api = [[V1_API_SCORE_HISTORY_LIST_API alloc] init];
    api.whenUpdated = ^(V1_API_SCORE_HISTORY_LIST_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        self.loaded = YES;
        if ( error ) {
            PERFORM_BLOCK_SAFELY(self.whenUpdated, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                [self.items removeAllObjects];
                [self.items addObjectsFromArray:response.orders];
                [self saveCache];
                PERFORM_BLOCK_SAFELY(self.whenUpdated, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(self.whenUpdated, errors);
            }
        }
    };
    [api send];
}

@end
