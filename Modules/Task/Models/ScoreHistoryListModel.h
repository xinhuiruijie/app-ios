//
//  ScoreHistoryListModel.h
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/30.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "STIStreamModel.h"

@class ScoreGroupModel;

@interface ScoreHistoryListModel : STIStreamModel

@end
