//
//  MyRecentScoreModel.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/27.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "STIOnceModel.h"

@interface MyRecentScoreModel : STIOnceModel

@property (nonatomic, strong) NSArray *scoreList; // 七天积分折线图
@property (nonatomic, strong) NSNumber *totalScore; // 当前总积分数

@end
