//
//  TaskListModel.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/28.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "TaskListModel.h"

@implementation TaskListModel

- (void)loadCache {
    NSArray *items = [self loadCacheWithKey:self.cacheKey objectClass:[TASK class]];
    [self.items removeAllObjects];
    if (items.count) {
        [self.items addObjectsFromArray:items];
    }
}

- (void)saveCache {
    [self saveCache:self.items key:self.cacheKey];
}

- (NSString *)cacheKey {
    return [[self class] description];
}

- (void)refresh {
    V1_API_TASK_LIST_API * api = [[V1_API_TASK_LIST_API alloc] init];
    api.whenUpdated = ^(V1_API_TASK_LIST_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        self.loaded = YES;
        if ( error ) {
            PERFORM_BLOCK_SAFELY(self.whenUpdated, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                [self.items removeAllObjects];
                [self.items addObjectsFromArray:response.tasks];
                [self saveCache];
                PERFORM_BLOCK_SAFELY(self.whenUpdated, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(self.whenUpdated, errors);
            }
        }
    };
    [api send];
}

@end
