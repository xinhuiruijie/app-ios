//
//  MyRecentScoreModel.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/27.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "MyRecentScoreModel.h"

@implementation MyRecentScoreModel

- (id)init {
    if (self = [super init]) {
        [self loadCache];
    }
    return self;
}

- (void)loadCache {
    NSArray *scoreList = [self loadCacheWithKey:[self scoreListCacheKey] objectClass:[NSArray class]];
    NSNumber *totalScore = [self loadCacheWithKey:[self currentScoreCacheKey] objectClass:[NSNumber class]];
    
    if (scoreList && [scoreList isKindOfClass:[NSArray class]]) {
        self.scoreList = scoreList;
    }
    if (totalScore && [totalScore isKindOfClass:[NSNumber class]]) {
        self.totalScore = totalScore;
    }
}

- (void)saveCache {
    [self saveCache:self.totalScore key:[self currentScoreCacheKey]];
    [self saveCache:self.scoreList key:[self scoreListCacheKey]];
}

- (void)clearCache {
    self.scoreList = nil;
    self.totalScore = nil;
    
    [self clearCacheWithKey:[self currentScoreCacheKey]];
    [self clearCacheWithKey:[self scoreListCacheKey]];
}

- (NSString *)currentScoreCacheKey {
    return [NSString stringWithFormat:@"currentScoreCacheKey.uid%@", [UserModel sharedInstance].user.id];
}

- (NSString *)scoreListCacheKey {
    return [NSString stringWithFormat:@"scoreListCacheKey.uid%@", [UserModel sharedInstance].user.id];
}

- (void)refresh {
    V1_API_SCORE_RECENT_API * api = [[V1_API_SCORE_RECENT_API alloc] init];
    
    api.whenUpdated = ^(V1_API_SCORE_RECENT_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
        self.loaded = YES;
        if ( error ) {
            PERFORM_BLOCK_SAFELY(self.whenUpdated, error);
        } else {
            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
                self.totalScore = response.total_score;
                self.scoreList = response.score;
                PERFORM_BLOCK_SAFELY(self.whenUpdated, nil);
            } else {
                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
                errors.code = X_MPlanet_ErrorCode(allHeaders);
                errors.message = X_MPlanet_ErrorDesc(allHeaders);
                PERFORM_BLOCK_SAFELY(self.whenUpdated, errors);
            }
        }
    };
    
    [api send];
}

@end
