//
//  ScoreHistoryListCell.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/30.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "ScoreHistoryListCell.h"

@interface ScoreHistoryListCell ()
@property (weak, nonatomic) IBOutlet UILabel *descLabel;
@property (weak, nonatomic) IBOutlet UILabel *changeLabel;

@end

@implementation ScoreHistoryListCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)dataDidChange {
    if (!self.data || ![self.data isKindOfClass:[SCORE_ORDER class]]) {
        return;
    }
    SCORE_ORDER *order = self.data;
    self.descLabel.text = order.change_desc ?: [AppTheme defaultPlaceholder];
    self.changeLabel.text = order.score ?: [AppTheme defaultPlaceholder];
    if ([order.score containsString:@"-"]) {
        self.changeLabel.textColor = [UIColor colorWithRGBValue:0x444348];
        self.changeLabel.text = [NSString stringWithFormat:@"%@M币", order.score];
    } else {
        self.changeLabel.textColor = [UIColor colorWithRGBValue:0xEFB400];
        self.changeLabel.text = [NSString stringWithFormat:@"+%@M币", order.score];
    }
}

@end
