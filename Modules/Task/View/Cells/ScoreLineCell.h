//
//  ScoreLineCell.h
//  MotherPlanet
//
//  Created by 陈熙 on 2018/1/3.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ScoreLineHeaderViewDelegate <NSObject>

@optional

- (void)gotoScoreMall;
- (void)gotoScoreHistory;
- (void)gotoScoreGuide;

@end

@interface ScoreLineCell : UITableViewCell

@property (nonatomic, weak) id<ScoreLineHeaderViewDelegate> delegate;

@property (nonatomic, strong) NSArray *scoreList; // 七天积分折线图
@property (nonatomic, strong) NSNumber *totalScore; // 当前总积分数

- (void)startLineChartAnimation;

@end
