//
//  TaskHeaderView.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/30.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "TaskHeaderView.h"

@interface TaskHeaderView ()

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation TaskHeaderView

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setRewardType:(REWARD_TYPE)rewardType {
    switch (rewardType) {
        case REWARD_TYPE_ONE_TIME:
            self.titleLabel.text = @"绑定社交平台";
            break;
        case REWARD_TYPE_DAILY:
            self.titleLabel.text = @"每日任务";
            break;
        case REWARD_TYPE_INVITATE:
            self.titleLabel.text = @"邀请好友";
            break;
        default:
            break;
    }
}

- (void)dataDidChange {
    if (!self.data || ![self.data isKindOfClass:[SCORE_ORDER class]]) {
        return;
    }
    SCORE_ORDER *order = self.data;
    if (order.created_at && order.created_at.length) {
        self.titleLabel.text = [order.created_at stringFromTimestamp];
    }
}

@end
