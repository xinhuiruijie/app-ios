//
//  TaskInviteFriendCell.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/30.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "TaskInviteFriendCell.h"

@interface TaskInviteFriendCell ()

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;

@end

@implementation TaskInviteFriendCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

- (void)dataDidChange {
    if (!self.data || ![self.data isKindOfClass:[TASK class]]) {
        return;
    }
    TASK *task = self.data;
    [self.iconImageView setImageWithPhoto:task.icon];
}

@end
