//
//  TaskListCell.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/28.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "TaskListCell.h"

@interface TaskListCell ()

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UIView *taskCountView;
@property (weak, nonatomic) IBOutlet UILabel *taskCountLabel;

@end

@implementation TaskListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.taskCountView.layer.cornerRadius = 10;
    self.taskCountView.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)dataDidChange {
    if (!self.data || ![self.data isKindOfClass:[TASK class]]) {
        return;
    }
    TASK *task = self.data;
    [self.iconImageView setImageWithPhoto:task.icon];
    self.titleLabel.text = task.title ?: [AppTheme defaultPlaceholder];
    if (task.reward) {
        self.scoreLabel.text = [NSString stringWithFormat:@"+%@M币", task.reward];
    }
    if (task.completed) {
        self.taskCountView.backgroundColor = [UIColor colorWithRGBValue:0xE9ECF0];
        self.taskCountLabel.textColor = [UIColor colorWithRGBValue:0xCBD2DB];
    } else {
        self.taskCountView.backgroundColor = [UIColor colorWithRGBValue:0x343338];
        self.taskCountLabel.textColor = [UIColor colorWithRGBValue:0xEFB400];
    }
    task.finish_times = task.finish_times ?: @0;
    task.times = task.times ?: @1;
    self.taskCountLabel.text = [NSString stringWithFormat:@"%@/%@", task.finish_times, task.times];
}

@end
