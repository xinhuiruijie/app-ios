//
//  ScoreLineCell.m
//  MotherPlanet
//
//  Created by 陈熙 on 2018/1/3.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "ScoreLineCell.h"
#import "UUChart.h"
#import "PPCounter.h"

@interface ScoreLineCell () <UUChartDataSource>

@property (weak, nonatomic) IBOutlet UIView *welfareView;
@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UIView *lineChartView;
@property (weak, nonatomic) IBOutlet UIImageView *scoreArrowIcon;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleLabelTLC;

@property (nonatomic, strong) UIView *firstLine;
@property (nonatomic, strong) UIView *secondLine;
@property (nonatomic, strong) UIView *thirdLine;

@property (nonatomic, strong) UUChart *chartView;
@property (nonatomic, strong) NSArray *chartDataSource;

@end

@implementation ScoreLineCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.welfareView.layer.borderWidth = 2;
    self.welfareView.layer.borderColor = [AppTheme selectedColor].CGColor;
    self.welfareView.layer.cornerRadius = 11;
    self.welfareView.layer.masksToBounds = YES;
    
    self.chartDataSource = @[@"0",@"0",@"0",@"0",@"0",@"0",@"0"];
    
    CGFloat topInset = [SDiOSVersion deviceSize] == Screen5Dot8inch ? 54 : 30;
    self.titleLabelTLC.constant = topInset;
    
    [self setupLine];
    [self startLineAnimation];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

}

- (void)setupLine {
    self.firstLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 70, 33)];
    [self.firstLine addSubview:self.meteorImageView];
    [self addSubview:self.firstLine];
    
    self.secondLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 70, 33)];
    [self.secondLine addSubview:self.meteorImageView];
    [self addSubview:self.secondLine];
    
    self.thirdLine = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 70, 33)];
    [self.thirdLine addSubview:self.meteorImageView];
    [self addSubview:self.thirdLine];
}

- (void)startLineAnimation {
    [self firstLineStartAnimation];
    [self secondLineStartAnimation];
    [self thirdLineStartAnimation];
    
    [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(firstLineStartAnimation) userInfo:nil repeats:YES];
    [NSTimer scheduledTimerWithTimeInterval:1.2 target:self selector:@selector(secondLineStartAnimation) userInfo:nil repeats:YES];
    [NSTimer scheduledTimerWithTimeInterval:1.2 target:self selector:@selector(thirdLineStartAnimation) userInfo:nil repeats:YES];
}

- (UIImageView *)meteorImageView {
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"line_star"]];
    imageView.frame = CGRectMake(0, 0, 70, 33);
    return imageView;
}

- (void)firstLineStartAnimation {
    CGFloat beforeOriginX = kScreenWidth / 2 + 50;
    CGFloat beforeOriginY = -45;
    CGFloat afterOriginX = -99;
    CGFloat afterOriginY = 100;
    
    CATransform3D beforeTranslate = CATransform3DMakeTranslation(beforeOriginX, beforeOriginY, 0);
    CATransform3D afterTranslate = CATransform3DMakeTranslation(afterOriginX, afterOriginY, 0);
    
    self.firstLine.layer.transform = beforeTranslate;
    self.firstLine.layer.speed = 0.25;
    
    [UIView beginAnimations:@"translate" context:NULL];
    
    //旋转时间
    [UIView setAnimationDuration:0.25];
    self.firstLine.layer.transform = afterTranslate;
    [UIView commitAnimations];
}

- (void)secondLineStartAnimation {
    CGFloat beforeOriginX = kScreenWidth;
    CGFloat beforeOriginY = -47;
    CGFloat afterOriginX = -191;
    CGFloat afterOriginY = 200;
    
    CATransform3D beforeTranslate = CATransform3DMakeTranslation(beforeOriginX, beforeOriginY, 0);
    CATransform3D afterTranslate = CATransform3DMakeTranslation(afterOriginX, afterOriginY, 0);
    
    self.secondLine.layer.transform = beforeTranslate;
    self.secondLine.layer.speed = 0.8;
    
    [UIView beginAnimations:@"translate" context:NULL];
    
    //旋转时间
    [UIView setAnimationDuration:0.8];
    [UIView setAnimationDelay:0.16];
    self.secondLine.layer.transform = afterTranslate;
    [UIView commitAnimations];
}

- (void)thirdLineStartAnimation {
    CGFloat beforeOriginX = kScreenWidth;
    CGFloat beforeOriginY = 85;
    CGFloat afterOriginX = -191;
    CGFloat afterOriginY = 300;
    
    CATransform3D beforeTranslate = CATransform3DMakeTranslation(beforeOriginX, beforeOriginY, 0);
    CATransform3D afterTranslate = CATransform3DMakeTranslation(afterOriginX, afterOriginY, 0);
    
    self.thirdLine.layer.transform = beforeTranslate;
    self.thirdLine.layer.speed = 0.8;
    
    [UIView beginAnimations:@"translate" context:NULL];
    
    //旋转时间
    [UIView setAnimationDuration:0.8];
    [UIView setAnimationDelay:0.4];
    self.thirdLine.layer.transform = afterTranslate;
    [UIView commitAnimations];
}

#pragma mark - Score Data

- (void)setTotalScore:(NSNumber *)totalScore {
    _totalScore = totalScore;
    if (totalScore == nil) {
        self.scoreLabel.text = @"---";
    }
}

- (void)setScoreList:(NSArray *)scoreList {
    _scoreList = scoreList;
    if (scoreList == nil) {
        self.chartDataSource = @[@"0",@"0",@"0",@"0",@"0",@"0",@"0"];
    } else {
        self.chartDataSource = scoreList;
    }
//    [self startLineChartAnimation];
}

#pragma mark - UUChartDataSource

- (NSArray *)getXTitles:(int)num {
    NSMutableArray *xTitles = [NSMutableArray array];
    for (int i=0; i<num; i++) {
        NSString * str = [NSString stringWithFormat:@""];
        [xTitles addObject:str];
    }
    return xTitles;
}

#pragma mark - @required

//横坐标标题数组
- (NSArray *)chartConfigAxisXLabel:(UUChart *)chart {
    return [self getXTitles:7];
}

//数值多重数组
- (NSArray *)chartConfigAxisYValue:(UUChart *)chart {
    return @[self.chartDataSource];
}

#pragma mark - @optional

// 线的颜色
- (NSArray *)chartConfigColors:(UUChart *)chart {
    return @[[UUColor whiteColor]];
}

#pragma mark 折线图专享功能

//标记数值区域
- (CGRange)chartHighlightRangeInLine:(UUChart *)chart {
    return CGRangeZero;
}

//判断显示横线条
- (BOOL)chart:(UUChart *)chart showHorizonLineAtIndex:(NSInteger)index {
    return NO;
}

- (void)startLineChartAnimation {
    if (self.chartView) {
        [self.chartView removeFromSuperview];
        self.chartView = nil;
    }
    CGFloat height = floor(kScreenWidth * 172 / 375);
    
    self.chartView = [[UUChart alloc] initWithFrame:CGRectMake(0, 0, kScreenWidth - 20, height) dataSource:self style:UUChartStyleLine];
    self.chartView.backgroundColor = [UIColor clearColor];
    [self.chartView showInView:self.lineChartView];
    
    if (self.totalScore != nil) {
        self.scoreArrowIcon.alpha = 0;
        [self.scoreLabel pp_fromNumber:0 toNumber:self.totalScore.floatValue duration:0.42 animationOptions:PPCounterAnimationOptionCurveLinear format:^NSString *(CGFloat number) {
            NSInteger score = (NSInteger)floor(number);
            return [NSString stringWithFormat:@"%ld", score];
        } completion:nil];
        [UIView animateWithDuration:0.22 delay:0.2 options:UIViewAnimationOptionCurveEaseIn animations:^{
            self.scoreArrowIcon.alpha = 1;
        } completion:nil];
    }
}

- (IBAction)welfareAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(gotoScoreMall)]) {
        [self.delegate gotoScoreMall];
    }
}

- (IBAction)MScoreAction:(id)sender {
    
}

- (IBAction)scoreInfoAction:(id)sender {
    if (self.delegate && [self.delegate respondsToSelector:@selector(gotoScoreHistory)]) {
        [self.delegate gotoScoreHistory];
    }
}

- (IBAction)pushScoreGuideAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(gotoScoreGuide)]) {
        [self.delegate gotoScoreGuide];
    }
}

@end
