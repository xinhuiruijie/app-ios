//
//  ScoreHistoryTotalScoreCell.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/30.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "ScoreHistoryTotalScoreCell.h"

@interface ScoreHistoryTotalScoreCell ()

@property (weak, nonatomic) IBOutlet UILabel *totalScoreLabel;

@end

@implementation ScoreHistoryTotalScoreCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)dataDidChange {
    if (!self.data) {
        return;
    }
    NSNumber *totalScore = self.data;
    self.totalScoreLabel.text = [NSString stringWithFormat:@"%@", totalScore];
}

@end
