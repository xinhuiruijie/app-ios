//
//  ScoreHistoryController.h
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/30.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ScoreHistoryController : UIViewController

@property (nonatomic, strong) NSNumber *totalScore; // 当前总积分数

@end
