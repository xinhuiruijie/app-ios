//
//  ScoreHistoryController.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/30.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "ScoreHistoryController.h"
#import "ScoreHistoryListModel.h"
#import "ScoreHistoryTotalScoreCell.h"
#import "ScoreHistoryListCell.h"
#import "TaskHeaderView.h"

@interface ScoreHistoryController () <UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic, strong) ScoreHistoryListModel *scoreHistoryListModel;

@end

@implementation ScoreHistoryController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Task"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupNavigation];
    [self registerNib];
    [self setupModel];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    if ([UserModel online]) {
        [self.scoreHistoryListModel refresh];
    }
}

#pragma mark - CustomMethod

- (void)setupNavigation {
    self.navigationItem.title = @"M币历史";
    @weakify(self)
    self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
        @strongify(self)
        [self.navigationController popViewControllerAnimated:YES];
    }];
}

- (void)setupModel {
    self.scoreHistoryListModel = [[ScoreHistoryListModel alloc] init];
    @weakify(self)
    self.scoreHistoryListModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        if (error == nil) {
            [self.tableView reloadData];
        } else {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
    };
}

- (void)registerNib {
    [self.tableView registerNib:[ScoreHistoryTotalScoreCell nib] forCellReuseIdentifier:@"ScoreHistoryTotalScoreCell"];
    [self.tableView registerNib:[ScoreHistoryListCell nib] forCellReuseIdentifier:@"ScoreHistoryListCell"];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    }
    return self.scoreHistoryListModel.items.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        ScoreHistoryTotalScoreCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ScoreHistoryTotalScoreCell" forIndexPath:indexPath];
        cell.data = self.totalScore;
        return cell;
    }
    ScoreHistoryListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ScoreHistoryListCell" forIndexPath:indexPath];
    cell.data = self.scoreHistoryListModel.items[indexPath.row];
    return cell;
}

#pragma mark - UITableViewDelegate

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 121;
    }
    return 40;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return nil;
    }
    TaskHeaderView *header = [TaskHeaderView loadFromNib];
    header.data = self.scoreHistoryListModel.items.firstObject;
    return header;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 0) {
        return CGFLOAT_MIN;
    }
    return 37;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return CGFLOAT_MIN;
}

@end
