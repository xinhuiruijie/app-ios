//
//  TaskViewController.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/19.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "TaskViewController.h"
#import "MyRecentScoreModel.h"
#import "TaskListCell.h"
#import "TaskInviteFriendCell.h"
#import "TaskHeaderView.h"
#import "TaskListModel.h"
#import "ScoreHistoryController.h"
#import "ScoreLineCell.h"
#import "IntegralMallWebController.h"
#import "PotocolUrlController.h"

@interface TaskViewController () <ScoreLineHeaderViewDelegate, UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) MyRecentScoreModel *recentScoreModel;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, strong) NSMutableArray *cellsArray;
@property (nonatomic, strong) NSMutableArray *dailyArray;
@property (nonatomic, strong) NSMutableArray *bindingArray;
@property (nonatomic, strong) NSMutableArray *inviteArray;
@property (nonatomic, strong) TaskListModel *taskListModel;
@property (nonatomic, assign) NSInteger scoreLine;
@property (nonatomic, strong) ScoreLineCell *lineCell;

@end

@implementation TaskViewController

+ (instancetype)spawn {
    return [self loadFromStoryBoard:@"Task"];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.scoreLine = 0;
    self.dailyArray = [NSMutableArray array];
    self.cellsArray = [NSMutableArray array];
    self.bindingArray = [NSMutableArray array];
    self.inviteArray = [NSMutableArray array];
    
    [self validCell];
    
    self.tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStyleGrouped];
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.dataSource = self;
    self.tableView.delegate = self;
    self.tableView.bounces = NO;
    [self.view addSubview:self.tableView];
    if (iOSVersionGreaterThanOrEqualTo(@"11.0")) {
        self.tableView.estimatedRowHeight = 0;
        self.tableView.estimatedSectionHeaderHeight = 0;
        self.tableView.estimatedSectionFooterHeight = 0;
    }
    self.automaticallyAdjustsScrollViewInsets = NO;
    
    [self registerNib];
    [self setupModel];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:animated];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if ([UserModel online]) {
        [self.recentScoreModel refresh];
    } else {
        self.lineCell.totalScore = nil;
        self.lineCell.scoreList = nil;
        [self.tableView reloadData];
        [self.lineCell startLineChartAnimation];
    }
    [self.taskListModel refresh];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:animated];
}

#pragma mark - CustomMethod

- (void)setupModel {
    self.recentScoreModel = [[MyRecentScoreModel alloc] init];
    @weakify(self)
    self.recentScoreModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        if (error == nil) {
            [self validCell];
            self.lineCell.totalScore = self.recentScoreModel.totalScore;
            self.lineCell.scoreList = self.recentScoreModel.scoreList;
            [self.tableView reloadData];
            [self.lineCell startLineChartAnimation];
        } else {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
    };
    
    self.taskListModel = [[TaskListModel alloc] init];
    self.taskListModel.whenUpdated = ^(STIHTTPResponseError *error) {
        @strongify(self)
        if (error == nil) {
            [self validCell];
            [self.tableView reloadData];
        } else {
            [self presentMessage:error.message withTips:@"数据获取失败"];
        }
    };
}

- (void)registerNib {
    [self.tableView registerNib:[ScoreLineCell nib] forCellReuseIdentifier:@"ScoreLineCell"];
    [self.tableView registerNib:[TaskListCell nib] forCellReuseIdentifier:@"TaskListCell"];
    [self.tableView registerNib:[TaskInviteFriendCell nib] forCellReuseIdentifier:@"TaskInviteFriendCell"];
}

- (void)validCell {
    [self.cellsArray removeAllObjects];
    [self.dailyArray removeAllObjects];
    [self.bindingArray removeAllObjects];
    [self.inviteArray removeAllObjects];
    if (self.taskListModel.items) {
        for (TASK *task in self.taskListModel.items) {
            //日常任务
            if (task.type == REWARD_TYPE_DAILY) {
                [self.dailyArray addObject:task];
            }
            //一次性任务
            if (task.type == REWARD_TYPE_ONE_TIME) {
                if ([AppConfig isShowWechat]) {
                    if ([task.code isEqualToString:@"bindwechat"]) {
                        [self.bindingArray addObject:task];
                    }
                }
                if ([AppConfig isShowWeibo]) {
                    if ([task.code isEqualToString:@"bindweibo"]) {
                        [self.bindingArray addObject:task];
                    }
                }
                if ([AppConfig isShowQQ]) {
                    if ([task.code isEqualToString:@"bindqq"]) {
                        [self.bindingArray addObject:task];
                    }
                }
            }
            //邀请任务
            if (task.type == REWARD_TYPE_INVITATE) {
                [self.inviteArray addObject:task];
            }
        }
    }
    [self.cellsArray addObject:@(self.scoreLine)];
    if (self.dailyArray.count) {
        [self.cellsArray addObject:@(REWARD_TYPE_DAILY)];
    }
    if (self.bindingArray.count) {
        [self.cellsArray addObject:@(REWARD_TYPE_ONE_TIME)];
    }
    if (self.inviteArray.count) {
        [self.cellsArray addObject:@(REWARD_TYPE_INVITATE)];
    }
}

#pragma mark - ScoreLineHeaderViewDelegate

- (void)gotoScoreMall {
    // 积分商城
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }
    [AppAnalytics clickEvent:@"click_lab_welfare"];
    IntegralMallWebController *webViewController = [[IntegralMallWebController alloc] init];
    webViewController.url = [ConfigModel sharedInstance].integral_mall_url;
    webViewController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:webViewController animated:YES];
}

- (void)gotoScoreHistory {
    // 积分历史
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }
    [AppAnalytics clickEvent:@"click_lab_mCoinHistory"];
    ScoreHistoryController *scoreHistory = [ScoreHistoryController spawn];
    scoreHistory.hidesBottomBarWhenPushed = YES;
    scoreHistory.totalScore = self.recentScoreModel.totalScore;
    [self.navigationController pushViewController:scoreHistory animated:YES];
}

- (void)gotoScoreGuide {
    if (![UserModel online]) {
        [[Authorization sharedInstance] showAuth];
        return;
    }
    [AppAnalytics clickEvent:@"click_lab_mCoinStrategy"];
    PotocolUrlController *potocolController = [PotocolUrlController spawn];
    potocolController.potocolType = POTOCOL_TYPE_TASK_GUIDE;
    potocolController.hidesBottomBarWhenPushed = YES;
    [self.navigationController pushViewController:potocolController animated:YES];
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleLightContent;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGPoint offset = self.tableView.contentOffset;
    if (offset.y <= 0) {
        offset.y = 0;
    }
    self.tableView.contentOffset = offset;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return self.cellsArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    NSInteger identifier = [self.cellsArray[section] integerValue];
    if (identifier == self.scoreLine) {
        return 1;
    } else if (identifier == REWARD_TYPE_DAILY) {
        return self.dailyArray.count;
    } else if (identifier == REWARD_TYPE_ONE_TIME) {
        return self.bindingArray.count;
    } else {
        return self.inviteArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger identifier = [self.cellsArray[indexPath.section] integerValue];
    if (identifier == self.scoreLine) {
        ScoreLineCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ScoreLineCell" forIndexPath:indexPath];
        if ([UserModel online]) {
            cell.totalScore = self.recentScoreModel.totalScore;
            cell.scoreList = self.recentScoreModel.scoreList;
        } else {
            cell.totalScore = nil;
            cell.scoreList = nil;
        }
        cell.delegate = self;
        self.lineCell = cell;
        return cell;
    } else if (identifier == REWARD_TYPE_DAILY) {
        TaskListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TaskListCell" forIndexPath:indexPath];
        cell.data = self.dailyArray[indexPath.row];
        return cell;
    } else if (identifier == REWARD_TYPE_ONE_TIME) {
        TaskListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TaskListCell" forIndexPath:indexPath];
        cell.data = self.bindingArray[indexPath.row];
        return cell;
    } else {
        TaskInviteFriendCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TaskInviteFriendCell" forIndexPath:indexPath];
        cell.data = self.inviteArray[indexPath.row];
        return cell;
    }
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger identifier = [self.cellsArray[indexPath.section] integerValue];
    if (identifier == REWARD_TYPE_INVITATE) {
        if (![UserModel online]) {
            [[Authorization sharedInstance] showAuth];
            return;
        }
        [AppAnalytics clickEvent:@"click_lab_invite"];
        ShareAlertView *shareAlert = [ShareAlertView loadFromNib];
        shareAlert.data = [UserModel sharedInstance].user;
        [shareAlert show];
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSInteger identifier = [self.cellsArray[indexPath.section] integerValue];
    if (identifier == self.scoreLine) {
        return [SDiOSVersion deviceSize] == Screen5Dot8inch ? ceil(kScreenWidth * 788 / 750) : ceil(kScreenWidth * 764 / 750);;
    }
    return 54;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSInteger identifier = [self.cellsArray[section] integerValue];
    if (identifier == self.scoreLine) {
        return nil;
    } else if (identifier == REWARD_TYPE_DAILY) {
        TaskHeaderView *view = [TaskHeaderView loadFromNib];
        view.rewardType = REWARD_TYPE_DAILY;
        return view;
    } else if (identifier == REWARD_TYPE_ONE_TIME) {
        TaskHeaderView *view = [TaskHeaderView loadFromNib];
        view.rewardType = REWARD_TYPE_ONE_TIME;
        return view;
    } else {
        TaskHeaderView *view = [TaskHeaderView loadFromNib];
        view.rewardType = REWARD_TYPE_INVITATE;
        return view;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    NSInteger identifier = [self.cellsArray[section] integerValue];
    if (identifier == self.scoreLine) {
        return CGFLOAT_MIN;
    }
    return 34;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section {
    UIView *view = [UIView new];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return [AppTheme onePixel];
}

@end
