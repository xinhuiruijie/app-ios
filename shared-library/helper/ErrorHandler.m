//
//  ErrorHandler.m
//  mplus
//
//  Created by Chenyun on 14-11-11.
//  Copyright (c) 2014年 geek-zoo. All rights reserved.
//

#import "ErrorHandler.h"

@implementation ErrorHandler

+ (void)error:(STIHTTPResponseError *)error withTips:(NSString *)tips
{
	if ( error.message )
	{
		[[UIApplication sharedApplication].keyWindow  presentFailureTips:error.message];
	}
	else
	{
		[[UIApplication sharedApplication].keyWindow presentFailureTips:tips];
	}
}

@end
