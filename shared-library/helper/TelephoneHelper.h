//
//  TelephoneHelper.h
//  gaibianjia
//
//  Created by QFish on 6/12/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TelephoneHelper : NSObject

+ (void)callWithNumber:(NSString *)number onView:(UIView *)view;

@end
