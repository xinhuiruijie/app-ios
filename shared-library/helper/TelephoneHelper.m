//
//  TelephoneHelper.m
//  gaibianjia
//
//  Created by QFish on 6/12/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import "TelephoneHelper.h"

@implementation TelephoneHelper

+ (void)callWithNumber:(NSString *)number onView:(UIView *)view
{
    if ( number )
    {
        NSString * str = [[number componentsSeparatedByCharactersInSet:[[NSCharacterSet characterSetWithCharactersInString:@"0123456789-+()"] invertedSet]] componentsJoinedByString:@""];
        NSURL * url = [NSURL URLWithString:[NSString stringWithFormat:@"telprompt:%@", str]];

        if ( [[UIApplication sharedApplication] canOpenURL:url] && UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone )
        {
            [[UIApplication sharedApplication] openURL:url];
        }
        else
        {
            NSString * message = [NSString stringWithFormat:@"%@\n%@", @"无法拨打电话", number];
            [[[UIAlertView alloc] initWithTitle:message
                                        message:nil
                                       delegate:nil
                              cancelButtonTitle:@"确定"
                              otherButtonTitles:nil] show];
        }
    }
    else
    {
        [view presentFailureTips:@"电话号码无效"];
    }
}

@end
