//
//  ErrorHandler.h
//  mplus
//
//  Created by Chenyun on 14-11-11.
//  Copyright (c) 2014年 geek-zoo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ErrorHandler : NSObject

+ (void)error:(STIHTTPResponseError *)error withTips:(NSString *)tips;

@end
