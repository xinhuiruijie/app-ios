//
//  NSNumber+FishKit.m
//  xueyou
//
//  Created by QFish on 7/11/15.
//  Copyright (c) 2015 QFish. All rights reserved.
//

#import "NSNumber+FileSize.h"

@implementation NSNumber (FileSize)

- (NSString *)fileSizeToString
{
    long long fileSize = [self longLongValue];
    
    NSInteger KB = 1024;
    NSInteger MB = KB*KB;
    NSInteger GB = MB*KB;
    
    if (fileSize < KB)
    {
        return [NSString stringWithFormat:@"%@B",self];
        
    }else if (fileSize < MB)
    {
        return [NSString stringWithFormat:@"%.1fKB",((CGFloat)fileSize)/KB];
        
    }else if (fileSize < GB)
    {
        return [NSString stringWithFormat:@"%.1fMB",((CGFloat)fileSize)/MB];
        
    }else
    {
        return [NSString stringWithFormat:@"%.1fGB",((CGFloat)fileSize)/GB];
    }
}

@end
