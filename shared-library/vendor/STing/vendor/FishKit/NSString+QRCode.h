//
//  NSString+QRCode.h
//  YuqiShopex
//
//  Created by PURPLEPENG on 15/08/2017.
//  Copyright © 2017 PURPLEPENG. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface NSString (QRCode)

- (UIImage *)generateQRCodeWithSize:(CGFloat)size;
- (UIImage *)generateBarCodeWithSize:(CGSize)size;

@end
