//
//  NSString+Match.m
//  MotherPlanet
//
//  Created by PURPLEPENG on 21/04/2017.
//  Copyright © 2017 GeekZooStudio. All rights reserved.
//

#import "NSString+Match.h"

@implementation NSString (Match)

#pragma mark -

- (NSArray *)matchedPoundStringRange {
    NSError *error;
    
    NSString *regulaStr = @"#[^#]+#";
    
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:regulaStr
                                                                           options:NSRegularExpressionCaseInsensitive
                                                                             error:&error];
    
    return [regex matchesInString:self options:0 range:NSMakeRange(0, [self length])];
}

- (NSAttributedString *)matchedPoundStringWithFont:(float)font
                                         textColor:(UIColor *)color {
    NSMutableAttributedString *attriString = [[NSMutableAttributedString alloc] initWithString:self];
    
    [[self matchedPoundStringRange] enumerateObjectsUsingBlock:^(NSTextCheckingResult * obj, NSUInteger idx, BOOL *stop) {
        [attriString addAttributes:@{
                                     NSFontAttributeName:[UIFont systemFontOfSize:font],
                                     NSForegroundColorAttributeName:color
                                     }
                             range:obj.range];
    }];
    
    return attriString;
}

#pragma mark -

- (NSString *)matchAndReplaceImageTag:(void (^)(NSMutableArray * targetStrs))then {
    NSString *str = [self copy];
    NSRegularExpression *iExpression;
    NSString *pattern = @"\\[img\\]file://.*?\\[/img\\]";
    iExpression = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:NULL];
    NSRange paragaphRange = NSMakeRange(0, self.length);
    
    NSArray *matches = [iExpression matchesInString:self options:0 range:paragaphRange];
    if (matches) {
        NSMutableArray *replaceStrS = [[NSMutableArray alloc] initWithCapacity:matches.count];
        NSMutableArray *filePaths = [NSMutableArray array];
        // 要替换的目标字符串数组
        for(NSTextCheckingResult *result in matches) {
            NSString *targetStr = [self substringWithRange:result.range];
            [replaceStrS addObject:targetStr];
            NSString * filePath = targetStr;
            filePath = [filePath stringByReplacingOccurrencesOfString:@"[img]" withString:@""];
            filePath = [filePath stringByReplacingOccurrencesOfString:@"[/img]" withString:@""];
            if (filePath && filePath.length) {
                [filePaths addObject:filePath];
            }
        }
        then(filePaths);
        
        for (int i = 0; i < matches.count;i++) {
            str = [str stringByReplacingOccurrencesOfString:replaceStrS[i] withString:[NSString stringWithFormat:@"<!--img%@.png-->",@(i)]];
        }
    } else {
        then(nil);
    }
    
    return str;
}

// 用默认图片替换图片标签（网络图片）
- (NSString *)replaceTargetPattern:(NSString *)pattern withReplacement:(NSString *)replacement {
    return [self stringByReplacingTargetPattern:pattern withString:replacement];
}

// 用指定的字符串替换匹配的正则表达式
- (NSString *)stringByReplacingTargetPattern:(NSString *)pattern withString:(NSString *)replacement {
    NSString *str = [self copy];
    NSRegularExpression *iExpression;
    
    iExpression = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:NULL];
    NSRange paragaphRange = NSMakeRange(0, self.length);
    
    NSArray *matches = [iExpression matchesInString:self options:0 range:paragaphRange];
    if (matches) {
        NSMutableArray *replaceStrS = [[NSMutableArray alloc] initWithCapacity:matches.count];
        for(NSTextCheckingResult *result in matches) {
            NSString *targetStr = [self substringWithRange:result.range];
            [replaceStrS addObject:targetStr];
        }
        
        for (int i = 0; i < matches.count;i++) {
            str = [str stringByReplacingOccurrencesOfString:replaceStrS[i] withString:replacement];
        }
    }
    
    return str;
}

#pragma mark -

- (NSInteger)multipleNameLength {
    NSInteger count = 0;
    
    NSRegularExpression *iExpression;
    NSString *pattern = @"[^\u4e00-\u9fa5]+";
    iExpression = [NSRegularExpression regularExpressionWithPattern:pattern options:0 error:NULL];
    NSRange paragaphRange = NSMakeRange(0, self.length);
    
    NSArray *matches = [iExpression matchesInString:self options:0 range:paragaphRange];
    if (matches) {
        for (int i = 0; i < matches.count;i++) {
            NSTextCheckingResult *result = matches[i];
            count += result.range.length;
        }
    }
    
    NSRegularExpression *iExpression2;
    NSString *pattern2 = @"[\u4e00-\u9fa5]+";
    iExpression2 = [NSRegularExpression regularExpressionWithPattern:pattern2 options:0 error:NULL];
    
    NSArray *matches2 = [iExpression2 matchesInString:self options:0 range:paragaphRange];
    if (matches2) {
        for (int i = 0; i < matches2.count;i++) {
            NSTextCheckingResult *result = matches2[i];
            count += result.range.length * 2;
        }
    }
    
    return count;
}

@end
