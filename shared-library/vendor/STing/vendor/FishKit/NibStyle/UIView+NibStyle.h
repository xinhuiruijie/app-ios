//
//  UIView+NibStyle.h
//  mplus
//
//  Created by QFish on 10/16/14.
//  Copyright (c) 2014 geek-zoo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Samurai_Property.h"

@interface UIBorderView : UIView
@prop_assign( CGFloat,	lineWidth );
@end

@protocol UIStyler <NSObject>
@property (nonatomic, strong) NSDictionary * styles;
@end

@interface UIView (NibStyle)

#pragma mark -

@property (nonatomic) NSString * nibBackgroundColor;
@property (nonatomic) id nibBorderColor;
@property (nonatomic) NSNumber * nibBorderWidth;
@property (nonatomic) NSString * nibBorder;
@property (nonatomic) NSNumber * nibCornerRadius;
@property (nonatomic) NSString * nibCornerAutoRadius;
@property (nonatomic) NSNumber * nibAlpha;

@property (nonatomic, copy) NSString *styleId;
@property (nonatomic, copy) NSString *styleClass;

@property (nonatomic) NSString * fontType;

@property (nonatomic, assign) CGSize     nibShadowOffset;
@property (nonatomic, assign) UIColor  * nibShadowColor;
@property (nonatomic, assign) NSNumber * nibShadowRadius;
@property (nonatomic, assign) NSNumber * nibShadowOpacity;

@prop_unsafe( UIBorderView *,	topBorder );
@prop_unsafe( UIBorderView *,	leftBorder );
@prop_unsafe( UIBorderView *,	rightBorder );
@prop_unsafe( UIBorderView *,	bottomBorder );

+ (void)setGlobalStyler:(id<UIStyler>)styler;

@end
