//
//  NSObject+FKRuntime.h
//  gaibianjia
//
//  Created by QFish on 6/25/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (FKRuntime)
+ (NSArray *)fk_selectorNamesOfProtocol:(NSString *)protocol;
@end
