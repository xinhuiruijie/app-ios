//
//  UIImage+FishKit.m
//  Foomoo
//
//  Created by purplepeng on 14-6-7.
//  Copyright (c) 2014年 QFish.inc. All rights reserved.
//

#import "UIImage+FishKit.h"

@implementation UIImage (FishKit)

#pragma mark -

- (UIImage *)compressWithQuality:(CGFloat)quality
{
    NSData * imageData = UIImageJPEGRepresentation( self, quality );
    
    if ( imageData )
    {
        return [UIImage imageWithData:imageData];
    }
    
    return self;
}

- (UIImage *)scaleWithWidth:(CGFloat)width
{
    float ratio = (self.size.width / (self.size.height * 1.0 ));
    CGSize targetSize = CGSizeMake( (int)width, (int)(width / ratio));
    return [self scaleToSize:targetSize];
}

- (UIImage *)scaleToSize:(CGSize)size
{
    NSLog(@"scaleToSize is %@",NSStringFromCGSize(size));
    UIGraphicsBeginImageContext(size);
    [self drawInRect:CGRectMake(0, 0, size.width, size.height)];
    UIImage * scaledImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return scaledImage;
}

#pragma mark -

+ (UIImage *)imageWithColor:(UIColor *)color
{
	CGRect rect = CGRectMake(0.0f, 0.0f, 1.0f, 1.0f);
	UIGraphicsBeginImageContext(rect.size);
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGContextSetFillColorWithColor(context, color.CGColor);
	CGContextFillRect(context, rect);
	UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return image;
}

/**
 *  生成图片
 */
+ (UIImage *)imageWithColor:(UIColor*)color andSize:(CGSize)size
{
    CGRect r= CGRectMake(0.0f, 0.0f, size.width, size.height);
    UIGraphicsBeginImageContext(r.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, r);
    
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    return img;
}

#pragma mark -

- (UIImage *)fixedLibraryImageToSize:(CGSize)size
{
    UIImage * newImage = self;
    
    if ( newImage.size.height > size.height && newImage.size.width > size.width )
    {
        CGFloat width = newImage.size.width * size.height / newImage.size.height;
        CGFloat height = size.height;
        
        newImage = [newImage scaleToSize:CGSizeMake( floorf(width), height)];
        
        NSData * imageData = UIImageJPEGRepresentation( newImage, 0.6 );
        
        if ( imageData )
        {
            return [UIImage imageWithData:imageData];
        }
        else
        {
            return newImage;
        }
    }
    else
    {
        NSData * imageData = UIImageJPEGRepresentation( newImage, 0.6 );
        
        if ( imageData )
        {
            return [UIImage imageWithData:imageData];
        }
        else
        {
            return newImage;
        }
    }
}

- (UIImage *)cutToSize:(CGSize)size
{
    //压缩图片
    CGSize newSize;
    CGImageRef imageRef = nil;
    
    if ( (self.size.width / self.size.height) < (size.width / size.height) )
    {
        newSize.width = self.size.width;
        newSize.height = self.size.width * size.height / size.width;
        
        imageRef = CGImageCreateWithImageInRect([self CGImage], CGRectMake(0, fabs(self.size.height - newSize.height) / 2, newSize.width, newSize.height));
    }
    else
    {
        newSize.height = self.size.height;
        newSize.width = self.size.height * size.width / size.height;
        
        imageRef = CGImageCreateWithImageInRect([self CGImage], CGRectMake(fabs(self.size.width - newSize.width) / 2, 0, newSize.width, newSize.height));
    }
    
    return [UIImage imageWithCGImage:imageRef];
}

- (void)fixedCameraImageToSize:(CGSize)size then:(void (^)(UIImage *))then
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0) , ^{
        //        UIImage * image = [[self fixOrientation] fixedCameraImageToSize:size];
        UIImage * image = [[self fixOrientation] cutToSize:CGSizeMake( size.width * 2, size.height * 2)];
        image = [image scaleToSize:CGSizeMake( size.width * 2, size.height * 2)];
        dispatch_async(dispatch_get_main_queue(), ^{
            then(image);
        });
    });
}


- (UIImage *)fixedCameraImageToSize:(CGSize)size
{
    UIImage * newImage = self;
    
    if ( newImage.size.width > newImage.size.height )
    {
        newImage = [newImage scaleToSize:CGSizeMake( newImage.size.width * size.height / newImage.size.height, size.height )];
    }
    else
    {
        newImage = [newImage scaleToSize:CGSizeMake( size.width, newImage.size.height * size.width / newImage.size.width )];
    }
    
    NSData * imageData = UIImageJPEGRepresentation( newImage, 0.6 );
    
    if ( imageData )
    {
        return [UIImage imageWithData:imageData];
    }
    else
    {
        return newImage;
    }
    
    return newImage;
}

#pragma mark -

- (UIImage *)fixOrientation
{
    // No-op if the orientation is already correct
    if (self.imageOrientation == UIImageOrientationUp) return self;
    
    // We need to calculate the proper transformation to make the image upright.
    // We do it in 2 steps: Rotate if Left/Right/Down, and then flip if Mirrored.
    CGAffineTransform transform = CGAffineTransformIdentity;
    
    switch (self.imageOrientation) {
        case UIImageOrientationDown:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.width, self.size.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.width, 0);
            transform = CGAffineTransformRotate(transform, M_PI_2);
            break;
            
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, 0, self.size.height);
            transform = CGAffineTransformRotate(transform, -M_PI_2);
            break;
        default:
            break;
    }
    
    switch (self.imageOrientation) {
        case UIImageOrientationUpMirrored:
        case UIImageOrientationDownMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.width, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
            
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRightMirrored:
            transform = CGAffineTransformTranslate(transform, self.size.height, 0);
            transform = CGAffineTransformScale(transform, -1, 1);
            break;
        default:
            break;
    }
    
    // Now we draw the underlying CGImage into a new context, applying the transform
    // calculated above.
    CGContextRef ctx = CGBitmapContextCreate(NULL, self.size.width, self.size.height,
                                             CGImageGetBitsPerComponent(self.CGImage), 0,
                                             CGImageGetColorSpace(self.CGImage),
                                             CGImageGetBitmapInfo(self.CGImage));
    CGContextConcatCTM(ctx, transform);
    switch (self.imageOrientation) {
        case UIImageOrientationLeft:
        case UIImageOrientationLeftMirrored:
        case UIImageOrientationRight:
        case UIImageOrientationRightMirrored:
            // Grr...
            CGContextDrawImage(ctx, CGRectMake(0,0,self.size.height,self.size.width), self.CGImage);
            break;
            
        default:
            CGContextDrawImage(ctx, CGRectMake(0,0,self.size.width,self.size.height), self.CGImage);
            break;
    }
    
    // And now we just create a new UIImage from the drawing context
    CGImageRef cgimg = CGBitmapContextCreateImage(ctx);
    UIImage *img = [UIImage imageWithCGImage:cgimg];
    CGContextRelease(ctx);
    CGImageRelease(cgimg);
    return img;
}

@end
