//
//  NSString+Match.h
//  MotherPlanet
//
//  Created by PURPLEPENG on 21/04/2017.
//  Copyright © 2017 GeekZooStudio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Match)

- (NSArray *)matchedPoundStringRange;
- (NSString *)matchAndReplaceImageTag:(void (^)(NSMutableArray * targetStrs))then;

- (NSAttributedString *)matchedPoundStringWithFont:(float)font textColor:(UIColor *)color;
- (NSString *)replaceTargetPattern:(NSString *)pattern withReplacement:(NSString *)replacement;

- (NSInteger)multipleNameLength;

@end
