//
//  NSNumber+Currency.h
//  MotherPlanet
//
//  Created by PURPLEPENG on 21/04/2017.
//  Copyright © 2017 GeekZooStudio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSNumber (Currency)

- (NSString *)currencyString;

@end
