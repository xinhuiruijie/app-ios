//
//  NSObject+FKRuntime.m
//  gaibianjia
//
//  Created by QFish on 6/25/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import "NSObject+FKRuntime.h"

static NSArray * _protocol_getSelectorNameList(const char *protocol);

#pragma mark -

@implementation NSObject (FKRuntime)

+ (NSArray *)fk_selectorNamesOfProtocol:(NSString *)protocol
{
	return _protocol_getSelectorNameList(protocol.UTF8String);
}

@end

#pragma mark -

/**
 *  获取一个 Protocol 的所有 method，包括 property
 *
 *  @param protocol 目标 protocol 的名字
 *
 *  @return 所有selector的名字数组
 */
static NSArray * _protocol_getSelectorNameList(const char *protocol)
{
	unsigned int count;
	
	Protocol * protocl = objc_getProtocol(protocol);
	
	struct objc_method_description * methods = protocol_copyMethodDescriptionList(protocl, NO, YES, &count);
	
	NSMutableArray *sels = [NSMutableArray array];
	
	for(unsigned i = 0; i < count; i++)
	{
		[sels addObject:NSStringFromSelector(methods[i].name)];
	}
	
	free(methods);
	
	return sels;
}
