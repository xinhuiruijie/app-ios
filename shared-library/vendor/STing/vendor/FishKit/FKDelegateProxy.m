//
//  FishKit.h
//
//  Created by QFish on 4/27/14.
//  Copyright (c) 2014 QFish.inc. All rights reserved.
//

#import "FKDelegateProxy.h"
#import <objc/runtime.h>

@interface FKDelegateProxy ()
@end

@implementation FKDelegateProxy

- (instancetype)initWithProxy:(id)proxy;
{
    if (self)
    {
        _proxy = proxy;
    }
    return self;
}

- (NSInvocation *)_copyInvocation:(NSInvocation *)invocation
{
    NSInvocation *copy = [NSInvocation invocationWithMethodSignature:[invocation methodSignature]];
    NSUInteger argCount = [[invocation methodSignature] numberOfArguments];
    
    for (int i = 0; i < argCount; i++)
    {
        char buffer[sizeof(intmax_t)];
        [invocation getArgument:(void *)&buffer atIndex:i];
        [copy setArgument:(void *)&buffer atIndex:i];
    }
    
    return copy;
}

- (void)forwardInvocation:(NSInvocation *)invocation
{
	if ([self.origin respondsToSelector:invocation.selector])
	{
		[invocation invokeWithTarget:self.origin];
	}
	
    if ([self.proxy respondsToSelector:invocation.selector])
    {
        NSInvocation *invocationCopy = [self _copyInvocation:invocation];
        [invocationCopy invokeWithTarget:self.proxy];
    }
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)sel
{
    id result = [self.origin methodSignatureForSelector:sel];
    if (!result) {
        result = [self.proxy methodSignatureForSelector:sel];
    }
    
    return result;
}

- (BOOL)respondsToSelector:(SEL)aSelector
{
    return ([self.origin respondsToSelector:aSelector]
            || [self.proxy respondsToSelector:aSelector]);
}

@end
