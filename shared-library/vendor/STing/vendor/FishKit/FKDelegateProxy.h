//
//  FishKit.h
//
//  Created by QFish on 4/27/14.
//  Copyright (c) 2014 QFish.inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#define FKDELEGATE_HAS_BEEN_DEFINED 

@interface FKDelegateProxy : NSProxy

@property (nonatomic, readonly, weak) id proxy;	 // proxy is the middle man
@property (nonatomic, strong) id origin; // the origin delegate

- (instancetype)initWithProxy:(id)proxy;

@end
