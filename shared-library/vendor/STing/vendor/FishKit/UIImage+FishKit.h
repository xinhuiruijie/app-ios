//
//  UIImage+FishKit.h
//  Foomoo
//
//  Created by purplepeng on 14-6-7.
//  Copyright (c) 2014年 QFish.inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (FishKit)

- (UIImage *)compressWithQuality:(CGFloat)quality;
- (UIImage *)scaleWithWidth:(CGFloat)width;
- (UIImage *)scaleToSize:(CGSize)size;

+ (UIImage *)imageWithColor:(UIColor *)color;
/// 生成指定颜色的图片
+ (UIImage *)imageWithColor:(UIColor*)color andSize:(CGSize)size;

- (UIImage *)fixedLibraryImageToSize:(CGSize)size;
- (UIImage *)fixedCameraImageToSize:(CGSize)size;

- (void)fixedCameraImageToSize:(CGSize)size then:(void (^)(UIImage *))then;

- (UIImage *)fixOrientation;

@end
