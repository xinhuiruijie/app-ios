//
//  NSNumber+Currency.m
//  MotherPlanet
//
//  Created by PURPLEPENG on 21/04/2017.
//  Copyright © 2017 GeekZooStudio. All rights reserved.
//

#import "NSNumber+Currency.h"

@implementation NSNumber (Currency)

- (NSString *)currencyString {
    static NSNumberFormatter *formatter = nil;
    
    if (!formatter) {
        formatter = [[NSNumberFormatter alloc]init];
        [formatter setGroupingSeparator:@","];
        [formatter setGroupingSize:3];
        [formatter setUsesGroupingSeparator:YES];
    }
    
    return [formatter stringFromNumber:self];
}

@end
