//
//  NSString+Unicode.h
//  bbm2
//
//  Created by GeekZoo on 16/4/12.
//  Copyright © 2016年 PURPLEPENG. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Unicode)

+ (NSString *)replaceUnicode:(NSString *)aUnicodeString;

+(NSString *)utf8ToUnicode:(NSString *)string;

@end
