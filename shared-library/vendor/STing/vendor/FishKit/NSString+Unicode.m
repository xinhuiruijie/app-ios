//
//  NSString+Unicode.m
//  bbm2
//
//  Created by GeekZoo on 16/4/12.
//  Copyright © 2016年 PURPLEPENG. All rights reserved.
//

#import "NSString+Unicode.h"

@implementation NSString (Unicode)

+ (NSString *)replaceUnicode:(NSString *)aUnicodeString {
    if (aUnicodeString == nil)
        return @"";
    
	NSString *tempStr1 = [aUnicodeString stringByReplacingOccurrencesOfString:@"\\u"withString:@"\\U"];
	
	NSString *tempStr2 = [tempStr1 stringByReplacingOccurrencesOfString:@"\""withString:@"\\\""];
	
	NSString *tempStr3 = [[@"\""stringByAppendingString:tempStr2] stringByAppendingString:@"\""];
	
	NSData *tempData = [tempStr3 dataUsingEncoding:NSUTF8StringEncoding];
	
    NSString *returnStr = [NSPropertyListSerialization propertyListWithData:tempData
                                                                    options:NSPropertyListImmutable
                                                                     format:NULL
                                                                      error:NULL];
	return [returnStr stringByReplacingOccurrencesOfString:@"\\r\\n"withString:@"\n"];
}

+ (NSString *)utf8ToUnicode:(NSString *)string {
	//去掉首尾空格
	string = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
	
	NSUInteger length = [string length];
	
	NSMutableString *s = [NSMutableString stringWithCapacity:0];

	for (int i = 0; i < length; i++) {
		unichar _char = [string characterAtIndex:i];
		
		//屏蔽换行符
		if ( _char == '\n' )
			continue;
		
		NSString * charString = [NSString stringWithFormat:@"%x", _char];
		if ( charString.length == 2 )
			[s appendFormat:@"\\u00%x", _char];
		
		else
			[s appendFormat:@"\\u%x", _char];
	}
	return s;
}


//Unicode转UTF-8
+ (NSString *)encodeToPercentEscapeString:(NSString *)input {
	// Encode all the reserved characters, per RFC 3986
	// (<http://www.ietf.org/rfc/rfc3986.txt>)
	NSString *outputStr = (NSString *)
	CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault,
											(CFStringRef)input,
											NULL,
											(CFStringRef)@"!*'();:@&=+$,/?%#[]",
											kCFStringEncodingUTF8));
	return outputStr;
}

+ (NSString *)decodeFromPercentEscapeString: (NSString *) input
{
	NSMutableString *outputStr = [NSMutableString stringWithString:input];
	[outputStr replaceOccurrencesOfString:@"+"
							   withString:@" "
								  options:NSLiteralSearch
									range:NSMakeRange(0, [outputStr length])];
	
	return [outputStr stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

@end
