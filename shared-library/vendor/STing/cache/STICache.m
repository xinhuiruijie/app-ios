//
//  STICache.m
//  fomo
//
//  Created by QFish on 3/20/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import "STICache.h"
#import "PINCache.h"

@interface STICache ()
@singleton(STICache)
@property (nonatomic, strong) PINCache * cache;
@end

@implementation STICache

@def_singleton(STICache)

+ (NSString *)cacheName
{
    return [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"];
}

+ (instancetype)global
{
    static dispatch_once_t once;
    static __strong STICache * global = nil;
    dispatch_once( &once, ^{
        global = [[STICache alloc] init];
        global.cache = [[PINCache alloc] initWithName:[self cacheName]];
    } );
    return global;
}

- (void)switchCacheWithName:(NSString *)name
{
    if ( !name ) {
        name = @"default";
    }
    
    self.cache = [[PINCache alloc] initWithName:name];
}

#pragma mark -

- (void)setObject:(id <NSCoding, NSObject>)object forKey:(NSString *)key
{
    if ( !object )
        return;
    
    NSParameterAssert( [object respondsToSelector:@selector(initWithCoder:)] && [object respondsToSelector:@selector(encodeWithCoder:)] );
    
    [self.cache.diskCache setObject:(id<NSCoding>)object forKey:key];
}

- (id <NSCoding>)objectForKey:(NSString *)key
{
    return [self.cache.diskCache objectForKey:key];
}

- (void)removeObjectForKey:(NSString *)key
{
    [self.cache.diskCache removeObjectForKey:key];
}

#pragma mark -

+ (void)switchCacheWithName:(NSString *)name
{
    [[self sharedInstance] switchCacheWithName:name];
}

+ (void)setObject:(id)object forKey:(NSString *)key
{
    TODO("needs check null?")
//    if ([key containsString:@"null"])
//    {
//        
//    }
    [[self sharedInstance] setObject:object forKey:key];
}

+ (id)objectForKey:(NSString *)key
{
    TODO("needs check null?")
//    if ([key containsString:@"null"])
//    {
//        
//    }
    return [[self sharedInstance] objectForKey:key];
}

+ (void)removeObjectForKey:(NSString *)key
{
    return [[self sharedInstance] removeObjectForKey:key];
}

@end
