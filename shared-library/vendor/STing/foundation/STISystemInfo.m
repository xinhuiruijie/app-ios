//
//  STISystemInfo.m
//  vogue
//
//  Created by QFish on 12/10/14.
//  Copyright (c) 2014 geekzoo. All rights reserved.
//

#import "STISystemInfo.h"
#import "SSKeychain.h"
#import <sys/param.h>
#import <sys/mount.h>

@interface STISystemInfo ()
@end

@implementation STISystemInfo

+ (BOOL)everLaunched
{
	return [self launchedTimes] > 1;
}

+ (NSInteger)launchedTimes
{
    NSString * version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    NSString * launchedTimesKey = [NSString stringWithFormat:@"version-%@-app.launched.times",version];
    
    NSInteger launchedTimes = [[NSUserDefaults standardUserDefaults] integerForKey:launchedTimesKey];
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        [[NSUserDefaults standardUserDefaults] setInteger:(launchedTimes + 1)
                                                   forKey:launchedTimesKey];
    });
    
    return launchedTimes;
}

+ (NSString *)appVersion
{
	static NSString * __identifier = nil;
	if ( nil == __identifier )
	{
		__identifier = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"] copy];
	}
	return __identifier;
}

+ (NSString *)appIdentifier
{
#if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
    static NSString * __identifier = nil;
    if ( nil == __identifier )
    {
        __identifier = [[[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleIdentifier"] copy];
    }
    return __identifier;
#else	// #if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
    return @"";
#endif	// #if (TARGET_OS_IPHONE || TARGET_IPHONE_SIMULATOR)
}

+ (NSString *)deviceUUID
{
    static NSString * __udid = nil;
    if ( nil == __udid )
    {
        NSString * udid = [SSKeychain passwordForService:[self appIdentifier] account:@"udid"];
        if (!udid) {
            udid = [[UIDevice currentDevice].identifierForVendor UUIDString];
            [SSKeychain setPassword:udid forService:[self appIdentifier] account:@"udid"];
        }
        __udid = udid;
    }
    return __udid;
}

+ (NSString *)getPreferredLanguage
{
    NSUserDefaults * defaults = [NSUserDefaults standardUserDefaults];
    
    NSArray * allLanguages = [defaults objectForKey:@"AppleLanguages"];
    
    NSString * preferredLang = [allLanguages objectAtIndex:0];
    
    return preferredLang;
}

// 获取总磁盘容量
+ (long long)getTotalDiskSize
{
    struct statfs buf;
    unsigned long long freeSpace = -1;
    if (statfs("/var", &buf) >= 0)
    {
        freeSpace = (unsigned long long)(buf.f_bsize * buf.f_blocks);
    }
    return freeSpace;
}

// 获取可用磁盘容量
+ (long long)getAvailableDiskSize
{
    struct statfs buf;
    unsigned long long freeSpace = -1;
    if (statfs("/var", &buf) >= 0)
    {
        freeSpace = (unsigned long long)(buf.f_bsize * buf.f_bavail);
    }
    return freeSpace;
}

@end
