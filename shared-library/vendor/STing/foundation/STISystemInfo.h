//
//  STISystemInfo.h
//  vogue
//
//  Created by QFish on 12/10/14.
//  Copyright (c) 2014 geekzoo. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kScreenWidth [UIScreen mainScreen].bounds.size.width
#define kScreenHeight [UIScreen mainScreen].bounds.size.height
#define kScreenRect [UIScreen mainScreen].bounds

#pragma mark -

@interface STISystemInfo : NSObject

+ (BOOL)everLaunched;
+ (NSInteger)launchedTimes;

+ (NSString *)deviceUUID;
+ (NSString *)appVersion;
+ (NSString *)appIdentifier;
+ (NSString *)getPreferredLanguage;

+ (long long)getTotalDiskSize;
+ (long long)getAvailableDiskSize;

@end
