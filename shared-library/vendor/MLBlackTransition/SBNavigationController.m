//
//  SBNavigationController.m
//  gaibianjia
//
//  Created by QFish on 7/8/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import "SBNavigationController.h"
#import "MLBlackTransition.h"

@implementation SBNavigationController

+ (void)load
{
	[MLBlackTransition validatePanPackWithMLBlackTransitionGestureRecognizerType:MLBlackTransitionGestureRecognizerTypePan
																		   class:self];
}

#pragma mark - UIViewControllerRotation

- (BOOL)shouldAutorotate
{
    return self.topViewController.shouldAutorotate;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return self.topViewController.supportedInterfaceOrientations;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return self.topViewController.preferredInterfaceOrientationForPresentation;
}

//#pragma mark -

//- (nullable UIViewController *)popViewControllerAnimated:(BOOL)animated {
//    NSArray * viewControllers = self.viewControllers;
//    UIViewController *viewController = nil;
//    if (viewControllers && viewControllers.count >= 3) {
//        UIViewController *targetController = [viewControllers objectAtIndex:viewControllers.count - 2];
//        if (self.authorisedClass && [targetController isKindOfClass:self.authorisedClass]) {
//            viewController = [viewControllers objectAtIndex:viewControllers.count - 3];
//            self.authorisedClass = nil;
//        } else {
//            viewController = [viewControllers objectAtIndex:viewControllers.count - 2];
//        }
//        
//    } else if (viewControllers && viewControllers.count == 2) {
//        viewController = [viewControllers objectAtIndex:viewControllers.count - 2];
//    }
//    
//    [self popToViewController:viewController animated:animated];
//    return viewController;
//}

@end
