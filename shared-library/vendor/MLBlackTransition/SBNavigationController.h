//
//  SBNavigationController.h
//  gaibianjia
//
//  Created by QFish on 7/8/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SBNavigationController : UINavigationController

// 滑动返回特殊处理：pop时跳过前一个controller
//
//    |               |
//    |  ControllerA  |
//    |  ControllerB  | (When ControllerB is AauthorisedClass)
//    |  ControllerC  |
//    |  ControllerD  |
//     ———————————————
//         STACK
// 通常ControllerA滑动返回时，会返回到ControllerB；
// 当设置了AauthorisedClass，并且AauthorisedClass和ControllerB是相同Class时，pop是跳过ControllerB，返回到ControllerC
@property (nonatomic, strong) Class authorisedClass;

@end
