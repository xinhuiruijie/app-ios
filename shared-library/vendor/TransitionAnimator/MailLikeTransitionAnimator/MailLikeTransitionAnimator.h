//
//  MailLikeTransitionAnimator.h
//  Components
//
//  Created by QFish.
//  Copyright (c) 2015 QFish. All rights reserved.
//

/*******
 
## Example:
 
### 1. To be presented ViewController :

```
@interface ToBePresentedViewController () <UIViewControllerTransitioningDelegate>
@property (nonatomic, strong) MailLikeTransitionAnimator * animator;
@end
```
 
### 2. Set the transitioning delegate be self

```
- (void)viewDidLoad
{
	self.transitioningDelegate = self.animator;
}
```

### 3. Implement the delegate, just copy the following code to your vc.m

```
@implementation ToBePresentedViewController
 
#pragma mark - MailLikeTransitionAnimator
 
- (MailLikeTransitionAnimator *)animator
{
	if ( !_animator ) {
		_animator = [MailLikeTransitionAnimator new];
		_animator.viewController = self;
	}
	return _animator;
}

@end
```

### 4. Just present the ViewController as you always do

```
ToBePresentedViewController *vc = [ToBePresentedViewController new];
[self presentViewController:vc animated:YES completion:nil];
```
 
*******/

#import <Foundation/Foundation.h>
@import UIKit;

@interface MailLikeTransitionAnimator : UIPercentDrivenInteractiveTransition <UIViewControllerAnimatedTransitioning, UIViewControllerTransitioningDelegate>
@property (nonatomic, assign) BOOL isPresenting;
@property (nonatomic, assign) BOOL isInterative;
@property (nonatomic, assign) BOOL canSwipeDown;
@property (nonatomic, assign) float ratio;
@property (nonatomic, copy) void (^ whenBackgroundClicked)(void);
@property (nonatomic, weak) UIViewController *viewController;
@end
