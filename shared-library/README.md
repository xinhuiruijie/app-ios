## /

目录|描述
---|---------------
components | 视图控件
database|数据库
helper|非视图通用组件
vendor|常用第三方库

## /components/

目录|描述
---|---------------
filter-bar|筛选条，支持两种样式
photo-browser|大图浏览控件
theme|全局设置主题
indicator|提示控件
pull-loader|上下拉刷新
uikit|自定义UIKit控件
guide-view|新手引导图

## /vendor/

目录|描述
---|---------------
STing| 轻量级框架
MLBlackTransition|任意界面右滑返回
TransitionAnimator|ViewController 转场动画
