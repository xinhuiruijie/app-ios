//
//  GKZSearchController.m
//  gaibianjia
//
//  Created by QFish on 9/11/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import "GKZSearchController.h"

@interface GKZSearchController () <UISearchBarDelegate>

@property (nonatomic, assign) BOOL shouldShowHeader;

@property (nonatomic, strong) UIButton * mask;
@property (nonatomic, strong) UISearchBar * searchBar;

@property(nonatomic, strong) UIBarButtonItem *leftBarButtonItem;
@property(nonatomic, strong) UIBarButtonItem *rightBarButtonItem;
@end

@implementation GKZSearchController

- (instancetype)initWithViewController:(UIViewController *)vc scrollView:(UICollectionView *)scrollView
{
    self = [super init];
    if (self) {
        _viewController = vc;
        _scrollView = scrollView;
        _shouldShowHeader = YES;
        _leftBarButtonItem = vc.navigationItem.leftBarButtonItem;
        _rightBarButtonItem = vc.navigationItem.rightBarButtonItem;
    }
    return self;
}

#pragma mark - UISearchBarDelegate

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar
{
    self.searching = NO;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.viewController presentLoadingTips:@"加载中"];
    
    [self setMaskHidden:YES];
    
    [self.searchBar resignFirstResponder];
    
    PERFORM_BLOCK_SAFELY(self.whenSearchButtonClicked, searchBar.text);
}

#pragma mark - SearchBar

- (void)setSearching:(BOOL)searching
{
    [self setMaskHidden:!searching];
    
    if ( searching == _searching )
        return;
    
    _searching = searching;
    
    if ( searching )
    {
        [self.viewController.navigationItem setLeftBarButtonItem:nil animated:YES];
        [self.viewController.navigationItem setRightBarButtonItem:nil animated:YES];
        
        self.viewController.navigationItem.titleView = self.searchBar;
        
        [self.searchBar becomeFirstResponder];
        
        if ( self.shouldShowHeader )
        {
            self.shouldShowHeader = NO;
            
            if ( self.scrollView.contentOffset.y < 50  )
            {
                [UIView animateWithDuration:0.3 animations:^{
                    [self.scrollView setContentOffset:CGPointMake(0, 50)];
                } completion:^(BOOL finished) {
                    [self.scrollView setContentOffset:CGPointMake(0, 0)];
                    [self.scrollView reloadData];
                }];
            }
        }
    }
    else
    {
        self.shouldShowHeader = YES;
        
        [UIView animateWithDuration:0.3 animations:^{
            [self.scrollView reloadSections:[[NSIndexSet alloc] initWithIndex:0]];
        } completion:^(BOOL finished) {
            if ( self.scrollView.contentOffset.y == 50  )
            {
                [UIView animateWithDuration:0.3 animations:^{
                    [self.scrollView setContentOffset:CGPointMake(0, 0)];
                } completion:^(BOOL finished) {
                    [self.scrollView reloadData];
                }];
            }
        }];
        
        self.viewController.navigationItem.titleView = nil;
        
        [self.viewController.navigationItem setLeftBarButtonItem:self.leftBarButtonItem animated:YES];
        [self.viewController.navigationItem setRightBarButtonItem:self.rightBarButtonItem animated:YES];
    }
}

#pragma mark -

- (void)setMaskHidden:(BOOL)hidden
{
    if ( hidden == self.mask.hidden )
        return;
    
    if ( hidden )
    {
        [UIView animateWithDuration:0.3 animations:^{
            self.mask.alpha = 0;
        } completion:^(BOOL finished) {
            self.mask.hidden = YES;
        }];
    }
    else
    {
        self.mask.alpha = 0;
        self.mask.hidden = NO;
        [UIView animateWithDuration:0.3 animations:^{
            self.mask.alpha = 0.5;
        }];
    }
}

#pragma mark -

- (UISearchBar *)searchBar
{
    if ( _searchBar == nil )
    {
        //导航条的搜索条
        _searchBar = [[UISearchBar alloc]initWithFrame:CGRectMake(0, 0.0f , self.viewController.navigationItem.titleView.frame.size.width, 44.0f)];
        _searchBar.delegate = self;
        _searchBar.placeholder = @"搜索";
        _searchBar.showsCancelButton = YES;
    }
    
    return _searchBar;
}

- (UIButton *)mask
{
    if ( _mask == nil )
    {
        _mask = [UIButton buttonWithType:UIButtonTypeCustom];
        _mask.frame = self.viewController.view.bounds;
        _mask.backgroundColor = [UIColor blackColor];
        _mask.alpha = 0.5;
        _mask.hidden = YES;
        @weakify(self);
        [_mask bk_addEventHandler:^(id sender) {
            @strongify(self);
            self.searching = NO;
        } forControlEvents:UIControlEventTouchUpInside];
        [self.viewController.view addSubview:_mask];
    }
    
    [self.viewController.view bringSubviewToFront:_mask];
    
    return _mask;
}

@end
