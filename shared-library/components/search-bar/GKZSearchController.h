//
//  GKZSearchController.h
//  gaibianjia
//
//  Created by QFish on 9/11/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface GKZSearchController : NSObject

@property (nonatomic, weak, readonly) UIViewController *viewController;
@property (nonatomic, weak, readonly) UICollectionView * scrollView;

@property (nonatomic, assign, getter=isSearching) BOOL searching;

@property (nonatomic, copy) void (^ whenSearchButtonClicked)(NSString * text);
@property (nonatomic, copy) void (^ whenStateChanged)(BOOL isSearching);

- (instancetype)initWithViewController:(UIViewController *)vc scrollView:(UIScrollView *)scrollView;

@end
