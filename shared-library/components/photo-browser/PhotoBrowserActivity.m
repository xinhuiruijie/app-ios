//
//  PhotoBrowserActivity.m
//  gaibianjia
//
//  Created by QFish on 6/11/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import "PhotoBrowserActivity.h"
#import "PhotoBrowserContent.h"

static MWPhoto * MWPhotoFromAny(id obj)
{
    if ( [obj isKindOfClass:MWPhoto.class] ) {
        return obj;
    } else if ( [obj isKindOfClass:NSString.class] && ((NSString *)obj).length ) {
        return [MWPhoto photoWithURL:[NSURL URLWithString:obj]];
    } else if ( [obj isKindOfClass:NSURL.class] ) {
        return [MWPhoto photoWithURL:obj];
    } else if ( [obj isKindOfClass:UIImage.class] ) {
        return [MWPhoto photoWithImage:obj];
    }

    return nil;
}

@interface MWPhotoBrowser ()
- (void)toggleControls;
- (void)updateNavigation;
- (void)restorePreviousNavBarAppearance:(BOOL)animated;
- (void)setNavBarAppearance:(BOOL)animated;
- (void)storePreviousNavBarAppearance;
- (void)setControlsHidden:(BOOL)hidden animated:(BOOL)animated permanent:(BOOL)permanent;
@end

@interface PhotoBrowserActivity () <MWPhotoBrowserDelegate>
@property (nonatomic, strong) NSMutableArray * wmphotos;
@property (nonatomic, strong) UILabel * captionLabel;
@property (nonatomic, strong) UIView *captionBgView;
/**
 *  删除之后的图片数组
 */
@property (nonatomic, strong) NSMutableArray * removeImagesIndex;
@end

@implementation PhotoBrowserActivity

- (void)dealloc
{
	
}

+ (PhotoBrowserActivity *)browser
{
	return [PhotoBrowserActivity new];
}


- (instancetype)init
{
	self = [super init];

	if (self)
	{
		self.delegate = self;
		self.zoomPhotosToFill = YES;
		self.enableSwipeToDismiss = NO;
		self.enableGrid = NO;
		
		self.isDelete = NO;
		
		self.removeImagesIndex = [NSMutableArray array];
	}

	return self;
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    switch (self.captionPosition) {
        case PhotoBrowserCaptionPositionDefault:
        case PhotoBrowserCaptionPositionTop: {
            break;
        }
        case PhotoBrowserCaptionPositionBottom:
        case PhotoBrowserCaptionPositionBottomTextRightAlign: {
            
            self.captionBgView = [[UIView alloc] init];
            self.captionBgView.backgroundColor = [UIColor colorWithWhite:0.0 alpha:0.5];
            [self.view addSubview:self.captionBgView];
            
            self.captionLabel = [[UILabel alloc] init];
            self.captionLabel.backgroundColor = [UIColor clearColor];
            self.captionLabel.textColor = [UIColor whiteColor];
            self.captionLabel.font = [UIFont systemFontOfSize:16];
            self.captionLabel.textAlignment = NSTextAlignmentRight;
            [self.captionBgView addSubview:self.captionLabel];
        }
    }
	
	if ( _isDelete )
	{
        @weakify(self)
		self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender)
		{
            @strongify(self)
			[self.navigationController popViewControllerAnimated:YES];
		}];
		self.navigationItem.title = @"相机胶卷";
		self.navigationItem.rightBarButtonItem = [AppTheme defaultItemWithContent:@"删除"
																   handler:^(id sender)
		{
            @strongify(self)
			//MWPhoto 对象
			MWPhoto * mwphoto = [self.wmphotos objectAtIndex:self.currentIndex];
			
			[self.removeImagesIndex addObject:mwphoto.image];
			
			[self.wmphotos removeObjectAtIndex:self.currentIndex];
			if ( !self.wmphotos.count )
			{
				[self.navigationController popViewControllerAnimated:YES];
			}
			[self reloadData];
			self.navigationItem.title = @"相机胶卷";
		}];
	}
}

- (void)viewWillDisappear:(BOOL)animated
{
	[super viewWillDisappear:animated];
	if ( _isDelete )
	{
		//已删完,发送通知
		[[NSNotificationCenter defaultCenter] postNotificationName: @"更新图片集合" object:self.removeImagesIndex];
	}
}

- (void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    CGRect bounds = self.view.bounds;
    self.captionBgView.frame = CGRectMake(0, bounds.size.height - 32, bounds.size.width, 32);
    self.captionLabel.frame = CGRectMake(15, 0, self.captionBgView.frame.size.width - 30, 32);
}

- (void)toggleControls
{
    switch (self.captionPosition) {
        case PhotoBrowserCaptionPositionDefault:
        case PhotoBrowserCaptionPositionTop: {
            [super toggleControls];
			if (_isDelete)
			{
				self.navigationItem.title = @"相机胶卷";
			}
            break;
        }
        case PhotoBrowserCaptionPositionBottom:
        case PhotoBrowserCaptionPositionBottomTextRightAlign: {
            [self.navigationController popViewControllerAnimated:YES];
            break;
        }
    }
}

- (void)updateNavigation
{
    [super updateNavigation];
    
    self.captionLabel.text = self.title;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    switch (self.captionPosition) {
        case PhotoBrowserCaptionPositionDefault:
        case PhotoBrowserCaptionPositionTop: {
            break;
        }
        case PhotoBrowserCaptionPositionBottom: {
            [self.navigationController setNavigationBarHidden:YES animated:NO];
            [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
            break;
        }
        case PhotoBrowserCaptionPositionBottomTextRightAlign: {
            self.captionLabel.textAlignment = NSTextAlignmentRight;
            [self.navigationController setNavigationBarHidden:YES animated:NO];
            [UIApplication sharedApplication].statusBarStyle = UIStatusBarStyleLightContent;
            break;
        }
    }
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    switch (self.captionPosition) {
        case PhotoBrowserCaptionPositionDefault:
        case PhotoBrowserCaptionPositionTop: {
            break;
        }
        case PhotoBrowserCaptionPositionBottom:
        case PhotoBrowserCaptionPositionBottomTextRightAlign: {
            break;
        }
    }
}

- (void)setPhotos:(NSArray *)photos
{
	_photos = photos;

	NSMutableArray * wmphotos = [NSMutableArray array];

	[photos enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        MWPhoto * photo = nil;
        
        if ( [obj conformsToProtocol:@protocol(PhotoBrowserContent)] ) {
            photo = MWPhotoFromAny([(id<PhotoBrowserContent>)obj pb_content]);
        } else {
            photo = MWPhotoFromAny(obj);
        }
        
        if ( photo ) {
            [wmphotos addObject:photo];
        }
	}];

	self.wmphotos = wmphotos;
}

#pragma mark -

- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
	return _wmphotos.count;
}

- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
	if (index < _wmphotos.count)
		return [_wmphotos objectAtIndex:index];
	return nil;
}

@end
