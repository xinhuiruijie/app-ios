//
//  PhotoBrowser.h
//  gaibianjia
//
//  Created by QFish on 6/11/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 * Example:
 *
 * Supportted:
 *	 - MWPhoto.class
 *	 - NSString.class
 *	 - UIImage.class
 *	 - id<>.class
 *
 * [PhotoBrowserController openPhotos:@[NSString, UIImage, PHOTO, NSURL] in:vc];
 *
 */

typedef NS_ENUM(NSUInteger, PhotoBrowserCaptionPosition) {
    PhotoBrowserCaptionPositionDefault = 0,
    PhotoBrowserCaptionPositionTop,
    PhotoBrowserCaptionPositionBottom,
    PhotoBrowserCaptionPositionBottomTextRightAlign,
};

#pragma mark -

@interface PhotoBrowserController : NSObject

+ (void)openPhotos:(NSArray *)photos in:(UIViewController *)vc;
+ (void)openPhotos:(NSArray *)photos captionPosition:(PhotoBrowserCaptionPosition)captionPosition in:(UIViewController *)vc;
+ (void)openPhotos:(NSArray *)photos atIndex:(NSInteger )index in:(UIViewController *)vc;
+ (void)openPhotos:(NSArray *)photos captionPosition:(PhotoBrowserCaptionPosition)captionPosition atIndex:(NSInteger )index in:(UIViewController *)vc;

/*
 * 可删除的大图模式，需提前注册通知: "更新图片集合"
 */
+ (void)openPhotos:(NSArray *)photos atIndex:(NSInteger)index WithCanDelete:(BOOL)delete in:(UIViewController *)vc;

@end
