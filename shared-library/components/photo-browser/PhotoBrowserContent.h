//
//  PhotoBrowserContent.h
//  gaibianjia
//
//  Created by QFish on 9/8/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol PhotoBrowserContent <NSObject>
@required
- (id)pb_content;
@end
