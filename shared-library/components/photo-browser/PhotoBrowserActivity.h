//
//  PhotoBrowserActivity.h
//  gaibianjia
//
//  Created by QFish on 6/11/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MWPhotoBrowser.h"
#import "PhotoBrowserController.h"


@interface PhotoBrowserActivity : MWPhotoBrowser

@property (nonatomic, strong) NSArray * photos;
@property (nonatomic, assign) PhotoBrowserCaptionPosition captionPosition;

/**
 *  查看大图是否可删除，默认NO.
 *  删完之后，广播通知 : @"更新图片集合"
 */
@property (nonatomic, assign) BOOL isDelete;

// 图片位置
@property (nonatomic, assign) NSInteger index;
+ (PhotoBrowserActivity *)browser;


@end
