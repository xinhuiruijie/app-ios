//
//  MWSaveActionView.m
//  quchicha
//
//  Created by pro on 15/10/8.
//  Copyright (c) 2015年 Vicky. All rights reserved.
//

#import "MWSaveActionView.h"

@implementation MWSaveActionView
@synthesize container = _container;
@synthesize whenSeleced = _whenSeleced;
@synthesize whenRegistered = _whenRegistered;
@synthesize whenHide = _whenHide;

- (void)awakeFromNib
{
    [super awakeFromNib];
}

#pragma mark - 

handleSignal( saveImage )
{
    @weakify(self);
    [self.container hideWithCompetion:^(BOOL finished) {
        @strongify(self);
        self.whenSeleced(nil, nil);
    }];
}

handleSignal( cancel )
{
    [self.container hide];
}

@end
