//
//  MWSaveActionView.h
//  quchicha
//
//  Created by pro on 15/10/8.
//  Copyright (c) 2015年 Vicky. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AlertView.h"

@interface MWSaveActionView : UIView <AlertContentView>

@end
