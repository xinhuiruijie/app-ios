//
//  PhotoBrowser.m
//  gaibianjia
//
//  Created by QFish on 6/11/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import "PhotoBrowserController.h"
#import "PhotoBrowserActivity.h"

@interface PhotoBrowserController ()
@end

@implementation PhotoBrowserController

+ (void)openPhotos:(NSArray *)photos in:(UIViewController *)vc
{
    [self openPhotos:photos captionPosition:PhotoBrowserCaptionPositionDefault in:vc];
}

+ (void)openPhotos:(NSArray *)photos captionPosition:(PhotoBrowserCaptionPosition)captionPosition in:(UIViewController *)vc
{
    PhotoBrowserActivity * browserAgent = [PhotoBrowserActivity browser];
    browserAgent.captionPosition = captionPosition;
    browserAgent.photos = photos;
    [vc.navigationController pushViewController:browserAgent animated:YES];
}

+ (void)openPhotos:(NSArray *)photos atIndex:(NSInteger)index in:(UIViewController *)vc
{
	PhotoBrowserActivity * browserAgent = [PhotoBrowserActivity browser];
	browserAgent.captionPosition = PhotoBrowserCaptionPositionBottom;
	browserAgent.photos = photos;
	[browserAgent setCurrentPhotoIndex:index];
	[vc.navigationController pushViewController:browserAgent animated:YES];
}

+ (void)openPhotos:(NSArray *)photos atIndex:(NSInteger)index WithCanDelete:(BOOL)delete in:(UIViewController *)vc
{
	PhotoBrowserActivity * browserAgent = [PhotoBrowserActivity browser];
	browserAgent.captionPosition = PhotoBrowserCaptionPositionDefault;
	browserAgent.photos = photos;
	[browserAgent setCurrentPhotoIndex:index];
	browserAgent.isDelete = delete;
	
	[vc.navigationController pushViewController:browserAgent animated:YES];
}

+ (void)openPhotos:(NSArray *)photos captionPosition:(PhotoBrowserCaptionPosition)captionPosition atIndex:(NSInteger )index in:(UIViewController *)vc
{
    PhotoBrowserActivity * browserAgent = [PhotoBrowserActivity browser];
    browserAgent.captionPosition = captionPosition;
    browserAgent.photos = photos;
    [browserAgent setCurrentPhotoIndex:index];
    [vc.navigationController pushViewController:browserAgent animated:YES];
}

@end
