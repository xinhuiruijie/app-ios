//
//  XLCycleScrollView.m
//  CycleScrollViewDemo
//
//  Created by xie liang on 9/14/12.
//  Copyright (c) 2012 xie liang. All rights reserved.
//

#import "XLCycleScrollView.h"

@interface XLCycleScrollView ()
@property (nonatomic, strong) UIImageView * defaultImageView;
@property (nonatomic, strong) UIView * pageBgView;
@end

@implementation XLCycleScrollView

@synthesize scrollView = _scrollView;
@synthesize pageControl = _pageControl;
@synthesize currentPage = _curPage;
@synthesize datasource = _datasource;
@synthesize delegate = _delegate;

- (void)pageInit
{
    if ( !_scrollView )
    {
        self.clipsToBounds = YES;
        
        _scrollView = [[UIScrollView alloc] initWithFrame:self.bounds];
        _scrollView.delegate = self;
        _scrollView.contentSize = CGSizeMake(self.bounds.size.width * 3, self.bounds.size.height);
        _scrollView.showsHorizontalScrollIndicator = NO;
        _scrollView.contentOffset = CGPointMake(self.bounds.size.width, 0);
        _scrollView.pagingEnabled = YES;
        [self addSubview:_scrollView];
        
        self.pageBgView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 0, 0)];
        self.pageBgView.backgroundColor = [UIColor blackColor];
        self.pageBgView.layer.masksToBounds = YES;
        self.pageBgView.alpha = 0.0;
        self.pageBgView.layer.cornerRadius = 6;
        [self addSubview:self.pageBgView];
        
        CGRect rect = self.bounds;
        rect.origin.y = rect.size.height - 12;
        rect.size.height = 12;
        _pageControl = [[CYPageView alloc] initWithFrame:rect];
//        _pageControl = [[UIPageControl alloc] initWithFrame:rect];
        _pageControl.userInteractionEnabled = NO;
        _pageControl.currentBackgroundColor = [UIColor whiteColor];
        _pageControl.unSelectedBackgroundColor = [UIColor grayColor];
//        _pageControl.pageIndicatorTintColor = [UIColor grayColor];
//        _pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
        
        [self.superview addSubview:_pageControl];
        
        _curPage = 0;
        
        [_scrollView addObserver:self forKeyPath:@"panGestureRecognizer.state" options:NSKeyValueObservingOptionNew context:nil];
    }
}

#pragma mark -

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary<NSKeyValueChangeKey,id> *)change context:(void *)context {
    if ([keyPath isEqualToString:@"panGestureRecognizer.state"]) {
        UIGestureRecognizerState state = [change[NSKeyValueChangeNewKey] integerValue];
        if ([self.delegate respondsToSelector:@selector(gestureRecognizerStateChanged:)]) {
            [self.delegate gestureRecognizerStateChanged:state];
        }
    }
}

- (void)dealloc {
    [_scrollView removeObserver:self forKeyPath:@"panGestureRecognizer.state"];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self pageInit];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    [self pageInit];
}

- (void)setIsHidePageBgView:(BOOL)isHidePageBgView
{
    _isHidePageBgView = isHidePageBgView;
    
    self.pageBgView.hidden = isHidePageBgView;
}

- (void)selectNextView
{
//    [_scrollView setContentOffset:CGPointMake(self.width * 2, 0) animated:YES];
    
    [UIView animateWithDuration:1 delay:0.0 usingSpringWithDamping:1.0 initialSpringVelocity:1.0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        [_scrollView setContentOffset:CGPointMake(self.width * 2, 0) animated:NO];
    } completion:nil];
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    _scrollView.frame = self.bounds;
    _scrollView.contentSize = CGSizeMake(self.bounds.size.width * 3, self.bounds.size.height);
    _scrollView.contentOffset = CGPointMake(self.bounds.size.width, 0);
    
    [self reloadPageBgViewFrame];
    
    if ( self.defaultImageView )
    {
        self.defaultImageView.frame = self.frame;
    }
    
    [self loadData];
}

- (void)setDataource:(id<XLCycleScrollViewDatasource>)datasource
{
    _datasource = datasource;
    [self reloadData];
}

- (void)reloadData
{
    _totalPages = [_datasource numberOfPages];
    if ( _totalPages == 0 ) {
        
        [self.defaultImageView removeFromSuperview];
        self.defaultImageView = [[UIImageView alloc] initWithFrame:self.bounds];
        self.defaultImageView.image = [UIImage imageNamed:@""];
        [self addSubview:self.defaultImageView];
        _scrollView.userInteractionEnabled = NO;
        return;
    }
    else
    {
        _scrollView.userInteractionEnabled = YES;
        [self.defaultImageView removeFromSuperview];
    }
    
    _pageControl.numberOfPages = _totalPages;
    
    [self reloadPageBgViewFrame];
    [self loadData];
}

- (void)loadData
{
    if ( _totalPages == 0 )
    {
        return;
    }
    if ( _totalPages - 1 >= _curPage )
    {
        _pageControl.currentPage = _curPage;
    }
    else
    {
        _curPage = _totalPages - 1;
        _pageControl.currentPage = _curPage;
    }
    
    if ( self.whenSelectedIndex )
    {
        self.whenSelectedIndex(_curPage);
    }
    
    // 从scrollView上移除所有的subview
    NSArray *subViews = [_scrollView subviews];
    if([subViews count] != 0) {
        [subViews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    
    [self getDisplayImagesWithCurpage:(int)_curPage];
    
    for (int i = 0; i < 3; i++) {
        UIView *v = [_curViews objectAtIndex:i];
        v.userInteractionEnabled = YES;
        UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                    action:@selector(handleTap:)];
        [v addGestureRecognizer:singleTap];
        v.frame = CGRectMake(self.width * i, 0, self.width, self.height); //CGRectOffset(v.frame, v.frame.size.width * i, 0);
        [_scrollView addSubview:v];
    }
    
    [_scrollView setContentOffset:CGPointMake(_scrollView.frame.size.width, 0)];
}

- (void)getDisplayImagesWithCurpage:(int)page {
    
    int pre = [self validPageValue:_curPage-1];
    int last = [self validPageValue:_curPage+1];
    
    if (!_curViews) {
        _curViews = [[NSMutableArray alloc] init];
    }
    
    [_curViews removeAllObjects];
    
    [_curViews addObject:[_datasource pageAtIndex:pre]];
    [_curViews addObject:[_datasource pageAtIndex:page]];
    [_curViews addObject:[_datasource pageAtIndex:last]];
}

- (int)validPageValue:(NSInteger)value {
    
    if(value == -1) value = _totalPages - 1;
    if(value == _totalPages) value = 0;
    
    return (int)value;
}

- (void)handleTap:(UITapGestureRecognizer *)tap {
    
    if ([_delegate respondsToSelector:@selector(didClickPage:atIndex:)]) {
        [_delegate didClickPage:self atIndex:_curPage];
    }
    
}

- (void)setViewContent:(UIView *)view atIndex:(NSInteger)index
{
    if (index == _curPage) {
        [_curViews replaceObjectAtIndex:1 withObject:view];
        for (int i = 0; i < 3; i++) {
            UIView *v = [_curViews objectAtIndex:i];
            v.userInteractionEnabled = YES;
            UITapGestureRecognizer *singleTap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                        action:@selector(handleTap:)];
            [v addGestureRecognizer:singleTap];
            v.frame = CGRectMake(self.width * i, 0, self.width, self.height); //CGRectOffset(v.frame, v.frame.size.width * i, 0);
            [_scrollView addSubview:v];
        }
    }
}

- (void)reloadPageBgViewFrame
{
    if (_pageControl.numberOfPages <= 1) {
        self.pageBgView.hidden = YES;
        _pageControl.hidden = YES;
        _scrollView.scrollEnabled = NO;
        return;
    } else {
        self.pageBgView.hidden = NO;
        _pageControl.hidden = NO;
        _scrollView.scrollEnabled = YES;
    }
    
//    self.pageBgView.frame = CGRectMake( (self.width / 2) - (_pageControl.numberOfPages * 18 / 2) , self.height - 12, _pageControl.numberOfPages * 18, 12);
    CGFloat pageBgViewWidth = (_pageControl.numberOfPages * (_pageControl.indicatorSize.width + _pageControl.spacing)) - _pageControl.spacing + 20;
    CGFloat pageBgViewHeight = 12;
    CGFloat pageBgViewOriginY = (self.height + 10) - pageBgViewHeight - self.indicatorBottomMargin;
    CGFloat pageBgViewOriginX = self.width - pageBgViewWidth - 7;
    if (self.indicatorAlign == IndictarAlignCenter) {
        pageBgViewOriginX = (self.width - pageBgViewWidth) / 2.f;
        // TODO: 抽出margin
        pageBgViewOriginX += 15;
    }
    self.pageBgView.frame = CGRectMake( pageBgViewOriginX, pageBgViewOriginY, pageBgViewWidth, pageBgViewHeight);
    self.pageBgView.center = CGPointMake(self.center.x, self.pageBgView.center.y);
//    _pageControl.frame = CGRectMake(0, self.bounds.size.height - 10, self.bounds.size.width, 10);
    _pageControl.frame = self.pageBgView.frame;
    _pageControl.centerY = self.pageBgView.centerY;
}

#pragma mark - UIScrollViewDelegate
- (void)scrollViewDidScroll:(UIScrollView *)aScrollView {
    // selectNextView方法中使用UIView实现滚动动画，这里就需要延长执行
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(0.0 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        int x = aScrollView.contentOffset.x;
        
        if ( !self.pageControl.numberOfPages ) {
            return;
        }
        
        //往下翻一张
        if(x >= (2*self.frame.size.width)) {
            _curPage = [self validPageValue:_curPage+1];
            [self loadData];
        }
        
        //往上翻
        if(x <= 0) {
            _curPage = [self validPageValue:_curPage-1];
            [self loadData];
        }
    });
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)aScrollView {
    
    [_scrollView setContentOffset:CGPointMake(_scrollView.frame.size.width, 0) animated:YES];
}

@end
