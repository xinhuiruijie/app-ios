//
//  XLCycleScrollView.h
//  CycleScrollViewDemo
//
//  Created by xie liang on 9/14/12.
//  Copyright (c) 2012 xie liang. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CYPageView.h"

@protocol XLCycleScrollViewDelegate;
@protocol XLCycleScrollViewDatasource;

#pragma mark -

typedef NS_ENUM(NSUInteger, IndictarAlign) {
    IndictarAlignRight = 0,
    IndictarAlignCenter,
};

#pragma mark -

@interface XLCycleScrollView : UIView<UIScrollViewDelegate>
{
    UIScrollView *_scrollView;
	CYPageView *_pageControl;
//    UIPageControl *_pageControl;

    NSInteger _totalPages;
    NSInteger _curPage;
    
    NSMutableArray *_curViews;
}

@property (nonatomic,readonly) UIScrollView *scrollView;
@property (nonatomic,readonly) CYPageView *pageControl;
//@property (nonatomic,readonly) UIPageControl *pageControl;
@property (nonatomic,assign) NSInteger currentPage;
@property (nonatomic, assign) BOOL isHidePageBgView;
@property (nonatomic,assign,setter = setDataource:) id<XLCycleScrollViewDatasource> datasource;
@property (nonatomic,assign,setter = setDelegate:) id<XLCycleScrollViewDelegate> delegate;

@property (nonatomic, copy) void (^whenSelectedIndex)(NSInteger index);
@property (nonatomic, copy) void (^whenWillBeginDragging)(void);
@property (nonatomic, copy) void (^whenDidEndDragging)(void);
@property (nonatomic, assign) IndictarAlign indicatorAlign;
@property (nonatomic, assign) CGFloat indicatorBottomMargin;

- (void)reloadData;
- (void)setViewContent:(UIView *)view atIndex:(NSInteger)index;

- (void)selectNextView;

@end

@protocol XLCycleScrollViewDelegate <NSObject>

@optional
- (void)didClickPage:(XLCycleScrollView *)csView atIndex:(NSInteger)index;
- (void)gestureRecognizerStateChanged:(UIGestureRecognizerState)state;
@end

@protocol XLCycleScrollViewDatasource <NSObject>

@required
- (NSInteger)numberOfPages;
- (UIView *)pageAtIndex:(NSInteger)index;

@end
