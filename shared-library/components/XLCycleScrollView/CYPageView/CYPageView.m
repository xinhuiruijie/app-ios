//
//  CYPageView.m
//  testPageController
//
//  Created by Chenyun on 15/8/27.
//  Copyright (c) 2015年 geek-zoo. All rights reserved.
//

#import "CYPageView.h"

@interface CYPageView ()

@property (nonatomic, strong) NSMutableArray * pageViews;

@end

@implementation CYPageView

- (void)awakeFromNib
{
	[super awakeFromNib];
}

- (instancetype)initWithFrame:(CGRect)frame
{
	self = [super initWithFrame:frame];
	if (self) {
		self.pageViews = [NSMutableArray array];
		self.spacing = 3;
		self.indicatorSize = CGSizeMake(8, 8);
		self.indicatorCornerRadius = 0;
//		self.currentBackgroundColor = [UIColor whiteColor];
//		self.unSelectedBackgroundColor = [UIColor clearColor];
//		self.pageBorderColor = [UIColor whiteColor];
        self.currentBorderColor = [UIColor whiteColor];
//		self.backgroundColor = [UIColor clearColor];
		self.layer.cornerRadius = frame.size.height / 2;
        
	}

	return self;
}

#pragma mark -

- (void)setNumberOfPages:(NSInteger)numberOfPages
{
	_numberOfPages = numberOfPages;
	[self reloadPageView];
}

- (void)setSpacing:(NSInteger)spacing
{
	_spacing = spacing;
	[self reloadPageView];
}

- (void)setIndicatorCornerRadius:(NSInteger)indicatorCornerRadius
{
	_indicatorCornerRadius = indicatorCornerRadius;
	[self reloadPageView];
}

- (void)setIndicatorSize:(CGSize)indicatorSize
{
	_indicatorSize = indicatorSize;
	[self reloadPageView];
}

- (void)setPageBorderColor:(UIColor *)pageBorderColor
{
	_pageBorderColor = pageBorderColor;
	[self reloadPageView];
}

- (void)setCurrentBorderColor:(UIColor *)currentBorderColor
{
    _currentBorderColor = currentBorderColor;
    [self reloadPageView];
}

- (void)setCurrentBackgroundColor:(UIColor *)currentBackgroundColor
{
	_currentBackgroundColor = currentBackgroundColor;
	[self reloadPageView];
}

- (void)setUnSelectedBackgroundColor:(UIColor *)unSelectedBackgroundColor
{
	_unSelectedBackgroundColor = unSelectedBackgroundColor;
	[self reloadPageView];
}

- (void)reloadPageView
{
	[self removePageViews];

	for ( int i = 0 ; i < self.numberOfPages; i++ )
	{
//		CGFloat contentOffsetX = (i * self.indicatorSize.width + i * self.spacing) + (self.frame.size.width ) - ((self.numberOfPages) * (self.indicatorSize.width + self.spacing)) - 10;

        CGFloat originX = 10 + i * (self.indicatorSize.width + self.spacing);
		UIView * pageView = [[UIView alloc] initWithFrame:CGRectMake( originX, self.frame.size.height / 2 - self.indicatorSize.height / 2, self.indicatorSize.width, self.indicatorSize.height)];
		pageView.layer.cornerRadius = self.indicatorCornerRadius;
		pageView.layer.masksToBounds = YES;
		pageView.layer.borderWidth = 1 / [UIScreen mainScreen].scale;
//		pageView.layer.borderColor = self.pageBorderColor.CGColor;

		[self addSubview:pageView];

		[self.pageViews addObject:pageView];
	}

	[self selectPageView];
}

- (void)layoutSubviews
{
	[super layoutSubviews];
	[self reloadPageView];
}

#pragma mark -

- (void)setCurrentPage:(NSInteger)currentPage
{
	_currentPage = currentPage;
	[self selectPageView];
}

- (void)unSelectd
{
	for ( UIView * view  in self.pageViews )
	{
		view.backgroundColor = self.unSelectedBackgroundColor;//[UIColor clearColor];
        view.layer.borderColor = self.pageBorderColor.CGColor;
	}
}

- (void)selectPageView
{
	[self unSelectd];

	if ( self.pageViews.count )
	{
		if ( self.pageViews.count > _currentPage )
		{
			UIView * view = self.pageViews[_currentPage];
			view.backgroundColor = self.currentBackgroundColor;
            view.layer.borderColor = self.currentBackgroundColor.CGColor;
		}
	}
}

- (void)removePageViews
{
	for ( UIView * view  in self.pageViews )
	{
		[view removeFromSuperview];
	}

	[self.pageViews removeAllObjects];
}

@end
