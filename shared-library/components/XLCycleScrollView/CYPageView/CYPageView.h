//
//  CYPageView.h
//  testPageController
//
//  Created by Chenyun on 15/8/27.
//  Copyright (c) 2015年 geek-zoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CYPageView : UIView
@property (nonatomic, assign) NSInteger numberOfPages; // 总pages
@property (nonatomic, assign) NSInteger currentPage;   // 当前的page

@property (nonatomic, assign) NSInteger  spacing;	// 间距
@property (nonatomic, assign) NSInteger indicatorCornerRadius;  // 点的弧度
@property (nonatomic, assign) CGSize indicatorSize;  // 点的大小

@property (nonatomic, strong) UIColor * currentBorderColor;   // 当前的边框颜色颜色
@property (nonatomic, strong) UIColor * currentBackgroundColor;   // 当前的page颜色
@property (nonatomic, strong) UIColor * pageBorderColor;   // 未选中的pageBorderColor颜色
@property (nonatomic, strong) UIColor * unSelectedBackgroundColor;   // 未选中的pageBgColor颜色

@end