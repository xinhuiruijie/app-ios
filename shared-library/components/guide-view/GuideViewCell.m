//
//  GuideViewCell.m
//  gaibianjia
//
//  Created by QFish on 6/29/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import "GuideViewCell.h"

@interface GuideViewCell ()

@end

@implementation GuideViewCell

- (void)setCustomView:(UIView *)customView
{
	if ( _customView == customView )
		return;
	
	if ( _customView != nil ) {
		[_customView removeFromSuperview];
	}
	
	_customView = customView;
	
	[self.contentView addSubview:customView];
	
	[self setNeedsLayout];
}

#pragma mark -

- (void)dataDidChange
{
	if ( self.data )
	{
		if ( [self.data isKindOfClass:NSString.class] )
		{
			self.customView.hidden = YES;
			self.imageView.hidden = NO;
			self.imageView.image = [UIImage imageNamed:self.data];
		}
		else if ( [self.data isKindOfClass:UIImage.class] )
		{
			self.customView.hidden = YES;
			self.imageView.hidden = NO;
			self.imageView.image = self.data;
		}
		else if ( [self.data isKindOfClass:UIView.class] )
		{
			self.customView.hidden = NO;
			self.imageView.hidden = YES;
			self.customView = self.data;
		}
	}
}

#pragma mark -

- (void)layoutSubviews
{
	[super layoutSubviews];
	
	self.customView.frame = self.contentView.bounds;
}

@end
