//
//  GuideViewCell.h
//  gaibianjia
//
//  Created by QFish on 6/29/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GuideViewCell : UICollectionViewCell
@property (nonatomic, strong) IBOutlet UIImageView * imageView;
@property (nonatomic, strong) UIView * customView;
@end
