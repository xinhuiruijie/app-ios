//
//  GuideViewControrller.h
//  gaibianjia
//
//  Created by QFish on 6/29/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GuideViewControrller : UIViewController
/**
 *  Support UIImage, UIView.
 */
@property (nonatomic, strong) NSArray * contents;

/**
 *  Default is Horizontal
 */
@property (nonatomic, assign) UICollectionViewScrollDirection direction;

/**
 *  Disctance of the PageControl to bottom, default is 20.
 */
@property (nonatomic, assign) CGFloat bottomPadding;

@property(nonatomic,retain) UIColor *pageIndicatorTintColor;
@property(nonatomic,retain) UIColor *currentPageIndicatorTintColor;

@property (nonatomic, copy) void (^whenClicked)(NSUInteger index);
@property (nonatomic, copy) void (^whenScrolledToEnd)(void);

- (void)show;
- (void)hide;
- (void)hideAnimated:(BOOL)animated;

@end
