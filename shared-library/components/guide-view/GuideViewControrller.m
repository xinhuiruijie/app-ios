//
//  GuideViewControrller.m
//  gaibianjia
//
//  Created by QFish on 6/29/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import "GuideViewControrller.h"
#import "GuideViewCell.h"

@interface GuideViewControrller () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
@property (nonatomic, weak) IBOutlet UICollectionView * list;
@property (nonatomic, weak) IBOutlet UICollectionViewFlowLayout * flowLayout;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint * pageControlBC;
@property (nonatomic, weak) IBOutlet UIPageControl * pageControl;
@property (nonatomic, strong) UIWindow * window;
@end

@implementation GuideViewControrller

+ (instancetype)spawn
{
	return [GuideViewControrller loadFromNib];
}

- (void)dealloc
{
	
}

- (instancetype)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if ( self )
	{
		_direction = UICollectionViewScrollDirectionHorizontal;
		
		switch ( [SDiOSVersion deviceSize] )
		{
			case Screen3Dot5inch:
			case Screen4inch:
				_bottomPadding = 0;
				break;
			case UnknownSize:
			case Screen4Dot7inch:
			case Screen5Dot5inch:
				_bottomPadding = 20;
				break;
		}
	}
	return self;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	[self.list registerNib:[GuideViewCell nib] forCellWithReuseIdentifier:@"GuideViewCell"];
	
	self.flowLayout.scrollDirection = self.direction;

	self.pageControl.numberOfPages = self.contents.count;
	self.pageControlBC.constant = self.bottomPadding;
	self.pageControl.pageIndicatorTintColor = self.pageIndicatorTintColor;
	self.pageControl.currentPageIndicatorTintColor = self.currentPageIndicatorTintColor;
}

- (void)setContents:(NSArray *)contents
{
	if ( _contents == contents )
		return;
		
	_contents = contents;
	
	[self.list reloadData];
}

- (void)setDirection:(UICollectionViewScrollDirection)direction
{
	if ( _direction == direction )
		return;
	
	_direction = direction;
	
	self.flowLayout.scrollDirection = self.direction;
}

#pragma mark - UIScrollViewDelegate

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
	if ( self.direction == UICollectionViewScrollDirectionHorizontal )
	{
		if ( scrollView.frame.size.width + scrollView.contentOffset.x > scrollView.contentSize.width )
		{
			PERFORM_BLOCK_SAFELY(self.whenScrolledToEnd);
			[self hide];
		}
	}
	else if ( self.direction == UICollectionViewScrollDirectionVertical )
	{
		if ( scrollView.frame.size.height + scrollView.contentOffset.y > scrollView.contentSize.height )
		{
			PERFORM_BLOCK_SAFELY(self.whenScrolledToEnd);
			[self hide];
		}
	}
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
	if ( self.direction == UICollectionViewScrollDirectionHorizontal )
	{
		self.pageControl.currentPage = scrollView.contentOffset.x / scrollView.bounds.size.width;
	}
	else if ( self.direction == UICollectionViewScrollDirectionVertical )
	{
		self.pageControl.currentPage = scrollView.contentOffset.y / scrollView.bounds.size.height;
	}
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
	return self.contents.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	UICollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"GuideViewCell" forIndexPath:indexPath];
	cell.data = self.contents[indexPath.item];
	return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
	return collectionView.bounds.size;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
	PERFORM_BLOCK_SAFELY(self.whenClicked, indexPath.item);
}

#pragma mark - 

- (void)show
{
	if ( self.window == nil )
	{
		UIWindow * window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
		window.windowLevel = UIWindowLevelNormal + 1;
		window.rootViewController = self;
		self.window = window;
	}
	
	self.window.hidden = NO;
}

- (void)hide
{
	[self hideAnimated:YES];
}

- (void)hideAnimated:(BOOL)animated;
{
	[UIView animateWithDuration:animated ? 0.3 : 0 animations:^{
		self.window.transform = CGAffineTransformMakeScale(1.5, 1.5);
		self.window.alpha = 0;
	} completion:^(BOOL finished) {
		self.window.hidden = YES;
		self.window.alpha = 1;
		self.window.transform = CGAffineTransformIdentity;
	}];
}

#pragma mark - UIViewControllerRotation

- (BOOL)shouldAutorotate
{
    return NO;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

@end
