//
//  AlertView.m
//  fomo
//
//  Created by Chenyun on 15/3/18.
//  Copyright (c) 2015年 Geek Zoo Studio. All rights reserved.
//

#import "AlertView.h"

@interface AlertView ()
@property (nonatomic, strong) UIButton * mask;
@property (nonatomic, strong) UIView<AlertContentView> * content;
@property (nonatomic, strong) UIToolbar * toolBar;
@end

@implementation AlertView
- (void)dealloc
{
	
}

- (void)setViewYWithNumber:(NSInteger)number
{
	[UIView animateWithDuration:0.3 animations:^{
		self.content.y = number;
	}];
}

- (instancetype)initWithContent:(UIView<AlertContentView> *)content type:(AlertViewType)type;
{
	self = [super init];
	if (self) {
		CGRect bounds = [UIScreen mainScreen].bounds;
		self.frame = bounds;

		_mask = [[UIButton alloc] initWithFrame:bounds];
		_mask.alpha = 0.f;
		[_mask addTarget:self action:@selector(masked:) forControlEvents:UIControlEventTouchUpInside];
		_mask.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.6f];
		[self addSubview:_mask];

		if ( type == AlertViewTypeMonospaced )
		{
			CGRect contentFrame = content.frame;
			contentFrame.size.width = self.frame.size.width;
			content.frame = contentFrame;
		}

		_content = content;

		content.container = self;

		content.center = self.center;
		[self addSubview:content];
	}

	return self;
}

- (instancetype)initWithContent:(UIView<AlertContentView> *)content type:(AlertViewType)type backgroundAlpha:(CGFloat)alpha {
    self = [self initWithContent:content type:type];
    _mask.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:alpha];
    return self;
}

- (instancetype)initWithContent:(UIView<AlertContentView> *)content type:(AlertViewType)type backgroundBlur:(BOOL)blur {
    self = [self initWithContent:content type:type];
    _mask.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.0f];
    
    if (blur) {
        UIToolbar * toolBar = [[UIToolbar alloc] initWithFrame:self.bounds];
        toolBar.barStyle = UIBarStyleBlack;
        
        [self insertSubview:toolBar atIndex:0];
    }
    
    return self;
}

- (void)resetAnimation
{
	self.content.alpha = 0.f;
	self.content.top = self.height;
}

- (void)endAnimation
{
	self.content.alpha = 1.0f;
	self.content.center = self.center;
}

- (void)endSharedViewAnimation
{
	self.content.alpha = 1.f;
	self.content.top = self.height - self.content.size.height;
}

- (void)endModifyAnimation
{
	self.content.alpha = 1.f;
	CGPoint point = self.center;
	point.y = point.y - 40;
	self.content.center = point;
}

- (void)showIn:(UIView *)container
{
	[self resetAnimation];

	[container addSubview:self];
	[UIView animateWithDuration:0.55f delay:0.f usingSpringWithDamping:0.75 initialSpringVelocity:0.5 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
		self.mask.alpha = 1.f;
		[self endAnimation];
	} completion:^(BOOL finished) {
	}];
}

- (void)show
{
	[self showIn:[UIApplication sharedApplication].keyWindow];
}

- (void)showSharedViewIn:(UIView *)container
{
	[self resetAnimation];
	
	[container addSubview:self];
	[UIView animateWithDuration:0.55f delay:0.f usingSpringWithDamping:0.75 initialSpringVelocity:0.5 options:UIViewAnimationOptionAllowAnimatedContent animations:^{
		self.mask.alpha = 1.f;
		[self endSharedViewAnimation];
	} completion:^(BOOL finished) {
	}];
}

- (void)showSharedView
{
	[self showSharedViewIn:[UIApplication sharedApplication].keyWindow];
}

- (void)showModifyViewIn:(UIView *)container
{
	[self resetAnimation];
	
	[container addSubview:self];
	[UIView animateWithDuration:0.55f delay:0.f usingSpringWithDamping:0.75 initialSpringVelocity:0.5 options:UIViewAnimationOptionAllowAnimatedContent animations:^{
		self.mask.alpha = 1.f;
		[self endModifyAnimation];
	} completion:^(BOOL finished) {
	}];
}

- (void)hide
{
	[self hideWithCompetion:nil];
}

- (void)hideWithCompetion:(void (^)(BOOL))completion
{
	[UIView animateWithDuration:0.3f delay:0.f options:UIViewAnimationOptionCurveEaseIn animations:^{
		self.mask.alpha = 0.f;
		self.content.alpha = 0.f;
        self.toolBar.hidden = YES;
	} completion:^(BOOL finished) {
		[self removeFromSuperview];
		PERFORM_BLOCK_SAFELY(completion, finished);
//		PERFORM_BLOCK_SAFELY(self.content.whenHide, finished);
	}];
}

- (void)masked:(id)sender
{
    if (self.whenTouchMask) {
        self.whenTouchMask();
    }
	[self hide];
}

@end
