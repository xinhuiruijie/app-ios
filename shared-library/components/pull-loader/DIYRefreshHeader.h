//
//  DIYRefreshHeader.h
//  GOME
//
//  Created by GeekZooStudio on 16/11/23.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import "MJRefreshHeader.h"

@interface DIYRefreshHeader : MJRefreshHeader
@property (nonatomic, assign) CGFloat insetTop;
@end
