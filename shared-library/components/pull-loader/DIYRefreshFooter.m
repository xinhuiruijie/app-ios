//
//  DIYRefreshFooter.m
//  GOME
//
//  Created by GeekZooStudio on 16/11/24.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import "DIYRefreshFooter.h"
#import "MJRefreshConst.h"
#import "UIView+MJExtension.h"
#import "UIScrollView+MJExtension.h"

@interface DIYRefreshFooter ()
@property (nonatomic, weak) UIImageView *activityImage;
@end

@implementation DIYRefreshFooter

#pragma mark - 懒加载
- (UIImageView *)activityImage
{
    if (!_activityImage) {
        UIImageView *activityImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"b0_pull_loading"]];
        activityImage.alpha = 0.0;
        [self addSubview:_activityImage = activityImage];
    }
    return _activityImage;
}

#pragma mark - 初始化方法
- (void)layoutSubviews
{
    [super layoutSubviews];
    
    // 指示器
    self.activityImage.frame = CGRectMake(0, 0, 30, 30);
    if (self.stateHidden) {
        self.activityImage.center = CGPointMake(self.mj_w * 0.5, self.mj_h * 0.5);
    } else {
        self.activityImage.center = CGPointMake(self.mj_w * 0.5 - 100, self.mj_h * 0.5);
    }
}

#pragma mark - 公共方法
- (void)setState:(MJRefreshFooterState)state
{
    if (self.state == state) return;
    
    switch (state) {
        case MJRefreshFooterStateIdle:
            [self stopAnimation];
            break;
            
        case MJRefreshFooterStateRefreshing:
            [self startAnimation];
            break;
            
        case MJRefreshFooterStateNoMoreData:
            [self stopAnimation];
            break;
            
        default:
            break;
    }
    
    // super里面有回调，应该在最后面调用
    [super setState:state];
}

#pragma mark -

// 开始旋转
- (void)startAnimation
{
    self.activityImage.alpha = 1.0;
    [self.activityImage.layer addAnimation:[self rotationAuto] forKey:@"rotationAnimation"];
}
// 停止旋转
- (void)stopAnimation
{
    self.activityImage.alpha = 0.0;
    [self.activityImage.layer removeAnimationForKey:@"rotationAnimation"];
}
// 旋转动画
- (CABasicAnimation *)rotationAuto
{
    CABasicAnimation* rotationAnimation;
    rotationAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
    rotationAnimation.toValue = [NSNumber numberWithFloat: M_PI * 2.0 ];
    rotationAnimation.duration = 1;
    rotationAnimation.repeatCount = 1000;
    return rotationAnimation;
}

@end
