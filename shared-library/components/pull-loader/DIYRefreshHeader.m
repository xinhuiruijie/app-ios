//
//  DIYRefreshHeader.m
//  GOME
//
//  Created by GeekZooStudio on 16/11/23.
//  Copyright © 2016年 Geek Zoo Studio. All rights reserved.
//

#import "DIYRefreshHeader.h"
#import "MJRefreshConst.h"
#import "UIView+MJExtension.h"

@interface DIYRefreshHeader ()

@property (nonatomic, strong) UIImageView *backImage;
@property (nonatomic, strong) UILabel *timeLabel;
@property (nonatomic, weak) UIImageView *arrowImage;
@property (nonatomic, weak) UIImageView *activityImage;

@end

@implementation DIYRefreshHeader

#pragma mark - 懒加载

- (UIImageView *)backImage {
    if (!_backImage) {
        _backImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_search_ele"]];
        _backImage.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:_backImage];
        _backImage.hidden = YES;
    }
    return _backImage;
}

- (UILabel *)timeLabel {
    if (!_timeLabel) {
        _timeLabel = [[UILabel alloc] init];
        _timeLabel.font = [UIFont systemFontOfSize:10];
        _timeLabel.textColor = [UIColor colorWithRGBValue:0xCBD2DB];
        _timeLabel.text = @"下拉刷新";
        _timeLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:_timeLabel];
        _timeLabel.hidden = YES;
    }
    return _timeLabel;
}

- (UIImageView *)arrowImage {
    if (!_arrowImage) {
        
        UIImageView *arrowImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"home_refresh_enter_1"]];
        arrowImage.contentMode = UIViewContentModeScaleAspectFill;
        [self addSubview:_arrowImage = arrowImage];
    }
    
    return _arrowImage;
}

- (UIImageView *)activityImage {
    if (!_activityImage) {
        UIImageView * activityView = [[UIImageView alloc] init];
        activityView.contentMode = UIViewContentModeScaleAspectFill;
        NSMutableArray *animateImages = [NSMutableArray array];
        for (int i = 1; i < 40; i++) {
            NSString *imageName = [NSString stringWithFormat:@"home_refresh_circle%d", i];
            UIImage *image = [UIImage imageNamed:imageName];
            [animateImages addObject:image];
        }
        
        activityView.animationImages = animateImages;
        activityView.animationRepeatCount = 0;
        activityView.animationDuration = 0.06 * animateImages.count;
        activityView.alpha = 0.0;
        [self addSubview:_activityImage = activityView];
    }
    return _activityImage;
}

- (void)setPullingPercent:(CGFloat)pullingPercent {
    [super setPullingPercent:pullingPercent];
    // 忽略前50%的下拉比例
    CGFloat p = pullingPercent * 100 - 50;
    if (p > 0) {
        NSInteger i = p / (50 / 41.0);
        NSLog(@"pullingPercent ---------> %ld", i);
        if (i > 0 && i < 42) {
            NSString *imageName = [NSString stringWithFormat:@"home_refresh_enter_%ld", i];
            self.arrowImage.image = [UIImage imageNamed:imageName];
        }
    }
}

#pragma mark - 初始化
- (void)layoutSubviews {
    [super layoutSubviews];
    
    // 设置自己的位置
    self.mj_y = - self.mj_h - self.insetTop;
    
    self.backImage.frame = CGRectMake(0, 0, self.mj_w, self.mj_h);
    // 箭头
    self.arrowImage.frame = CGRectMake(0, 0, 32, 32);
    CGFloat arrowX = (self.stateHidden && self.updatedTimeHidden) ? self.mj_w * 0.5 : (self.mj_w * 0.5 - 100); // -50? 时间文字所占用的宽度
    self.arrowImage.center = CGPointMake(arrowX, self.mj_h * 0.6);
    
    // 加载中显示图片
    self.activityImage.frame = self.arrowImage.frame;
    self.activityImage.center = self.arrowImage.center;
    
    self.timeLabel.frame = CGRectMake(0, self.arrowImage.bottom + 5, self.mj_w, 12);
}

#pragma mark - 公共方法
#pragma mark 设置状态
- (void)setState:(MJRefreshHeaderState)state {
    if (self.state == state) return;
    
    // 旧状态
    MJRefreshHeaderState oldState = self.state;
    
    switch (state) {
        case MJRefreshHeaderStateIdle: {
            self.arrowImage.alpha = 1.0;
            self.activityImage.alpha = 0.0;
            self.arrowImage.image = [UIImage imageNamed:@"home_refresh_enter_1"];
            self.timeLabel.text = @"下拉刷新";
            if (oldState == MJRefreshHeaderStateRefreshing) {
                self.timeLabel.text = @"加载成功";
                [self.activityImage stopAnimating];
                self.activityImage.alpha = 0.0;
                self.arrowImage.alpha = 1.0;
                self.arrowImage.image = [UIImage imageNamed:@"home_refresh_enter_41"];
//                self.arrowImage.transform = CGAffineTransformIdentity;
//                self.arrowImage.image = [UIImage imageNamed:@"b0_pull_success"];
//                self.arrowImage.alpha = 1.0;
//                self.activityImage.alpha = 0.0;
//                [UIView animateWithDuration:MJRefreshSlowAnimationDuration animations:^{
//                    self.activityImage.alpha = 0.0;
//                } completion:^(BOOL finished) {
//                    self.arrowImage.alpha = 1.0;
//                    [self stopAnimation];
//                }];
                dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(MJRefreshSlowAnimationDuration * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                    self.timeLabel.text = @"下拉刷新";
                    self.arrowImage.image = [UIImage imageNamed:@"home_refresh_enter_1"];
                });
            }
//            else {
//                [UIView animateWithDuration:MJRefreshFastAnimationDuration animations:^{
//                    self.arrowImage.transform = CGAffineTransformIdentity;
//                }];
//            }
            break;
        }
            
        case MJRefreshHeaderStatePulling: {
            self.activityImage.alpha = 0.0;
            self.timeLabel.text = @"松开刷新";
            break;
        }
            
        case MJRefreshHeaderStateRefreshing: {
            self.timeLabel.text = @"正在刷新";
            self.arrowImage.alpha = 0.0;
            self.activityImage.alpha = 1.0;
            [self.activityImage startAnimating];
            break;
        }
            
        default:
            break;
    }
    
    // super里面有回调，应该在最后面调用
    [super setState:state];
}

@end
