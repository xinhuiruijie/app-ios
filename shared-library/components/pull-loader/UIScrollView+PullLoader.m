//
//  UIScrollView+PullLoader.m
//  gaibianjia
//
//  Created by QFish on 7/7/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import "UIScrollView+PullLoader.h"
#import "UIScrollView+MJRefresh.h"
#import "DIYRefreshHeader.h"
#import "DIYRefreshFooter.h"

#import "VideoInfoCloseHeader.h"

@implementation UIScrollView (PullLoader)

- (void)addHeaderPullLoader:(void (^)())callback
{
//    MJRefreshLegendHeader *header = [self addLegendHeaderWithRefreshingBlock:callback];
    
    DIYRefreshHeader *header = [[DIYRefreshHeader alloc] init];
    self.header = header;
    
    header.refreshingBlock = callback;
    header.dateKey = nil;
    header.insetTop = self.contentInset.top;
    header.updatedTimeHidden = YES;
    header.stateHidden = YES;
}

- (void)addVideoHeaderPullLoader:(void (^)())callback {
    VideoInfoCloseHeader *header = [[VideoInfoCloseHeader alloc] init];
    self.header = header;
    
    header.refreshingBlock = callback;
    header.dateKey = nil;
    header.insetTop = self.contentInset.top;
    header.updatedTimeHidden = YES;
    header.stateHidden = YES;
}

- (void)addFooterPullLoader:(void (^)())callback
{
    MJRefreshLegendFooter *footer = [self addLegendFooterWithRefreshingBlock:callback];
    
//    DIYRefreshFooter *footer = [[DIYRefreshFooter alloc] init];
    self.footer = footer;
    
    footer.refreshingBlock = callback;
    footer.stateHidden = YES;
}

#pragma mark -

static char DIYRefreshHeaderKey;
- (void)setHeader:(MJRefreshHeader *)header
{
    if (header != self.header) {
        [self.header removeFromSuperview];
        
        [self willChangeValueForKey:@"header"];
        objc_setAssociatedObject(self, &DIYRefreshHeaderKey,
                                 header,
                                 OBJC_ASSOCIATION_ASSIGN);
        [self didChangeValueForKey:@"header"];
        
        [self addSubview:header];
    }
}

- (MJRefreshHeader *)header
{
    return objc_getAssociatedObject(self, &DIYRefreshHeaderKey);
}

#pragma mark -

static char DIYRefreshFooterKey;
- (void)setFooter:(MJRefreshFooter *)footer
{
    if (footer != self.footer) {
        [self.footer removeFromSuperview];
        
        [self willChangeValueForKey:@"footer"];
        objc_setAssociatedObject(self, &DIYRefreshFooterKey,
                                 footer,
                                 OBJC_ASSOCIATION_ASSIGN);
        [self didChangeValueForKey:@"footer"];
        
        [self addSubview:footer];
    }
}

- (MJRefreshFooter *)footer
{
    return objc_getAssociatedObject(self, &DIYRefreshFooterKey);
}

@end
