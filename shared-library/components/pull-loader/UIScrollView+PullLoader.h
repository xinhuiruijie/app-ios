//
//  UIScrollView+PullLoader.h
//  gaibianjia
//
//  Created by QFish on 7/7/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

@interface UIScrollView (PullLoader)

- (void)addHeaderPullLoader:(void (^)())callback;
- (void)addFooterPullLoader:(void (^)())callback;

- (void)addVideoHeaderPullLoader:(void (^)())callback;

@end
