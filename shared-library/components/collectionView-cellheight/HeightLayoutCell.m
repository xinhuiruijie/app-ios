//
//  HeightLayoutCell.m
//  timelineDemo2
//
//  Created by GeekZoo on 16/3/23.
//  Copyright © 2016年 Huxiao. All rights reserved.

#import "HeightLayoutCell.h"

@interface HeightLayoutCell ()

// templateCells
@property (nonatomic, strong) NSMutableDictionary<NSString *, UICollectionViewCell *> * templateCells;

// 缓存
@property (nonatomic, strong) HeightCache * cache;

@end

@implementation HeightLayoutCell


- (instancetype)init
{
	self = [super init];
	if (self) {
		self.templateCells = [NSMutableDictionary dictionary];
		self.cache = [HeightCache new];
	}
	return self;
}

- (void)addTemplateCell:(UICollectionViewCell *)cell identifier:(NSString *)inentifier
{
	if ( !inentifier )
		return;
	else
	{
		self.templateCells[inentifier] = cell;
	}
}

#pragma mark -

- (CGFloat)heightForCellIndentitier:(NSString *)indentifier
							   data:(id )data
							  width:(CGFloat)width
						  indexPath:(NSIndexPath *)indexPath
				   additionalHeight:(CGFloat (^)())height
{
	CGFloat cellHeight = 0.f;
	
	if ( indexPath && [self.cache existCacheForIndexPath:indexPath] )
	{
		cellHeight = [self.cache heightForIndexPath:indexPath];;
		return cellHeight;
	}
	
	if ( !indentifier )
	{
		NSLog(@"不能传一个空indentifier!!!");
		return 0.f;
	}
	
	UICollectionViewCell * templateCell = self.templateCells[indentifier];
	if ( !templateCell )
	{
		NSLog(@"无此indentifier的cell!!!");
		return 0.f;
	}
	else
	{
		templateCell.data = data;
	
		NSLayoutConstraint * widthFenceConstraint = [NSLayoutConstraint constraintWithItem:templateCell.contentView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:width];
		[templateCell.contentView addConstraint:widthFenceConstraint];
		
		cellHeight += [templateCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
	}
	
	if ( height )
	{
		cellHeight += height();
	}
	
	[self.cache cacheHeight:cellHeight ForIndexPath:indexPath];
	
	return cellHeight;
}

- (CGFloat)heightForCellIndentitier:(NSString *)indentifier
							   data:(id )data
							  width:(CGFloat)width
								key:(id)key
				   additionalHeight:(CGFloat (^)())height
{
	CGFloat cellHeight = 0.f;
	
	if ( key && [self.cache existCacheForKey:key] )
	{
		cellHeight = [self.cache heightForKey:key];
		return cellHeight;
	}
	
	if ( !indentifier )
	{
		NSLog(@"不能传一个空indentifier!!!");
		return 0.f;
	}
	
	UICollectionViewCell * templateCell = self.templateCells[indentifier];
	if ( !templateCell )
	{
		NSLog(@"无此indentifier的cell!!!");
		return 0.f;
	}
	else
	{
		templateCell.data = data;
		
		NSLayoutConstraint * widthFenceConstraint = [NSLayoutConstraint constraintWithItem:templateCell.contentView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1.0 constant:width];
		[templateCell.contentView addConstraint:widthFenceConstraint];
		
		cellHeight += [templateCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize].height;
	}
	
	if ( height )
	{
		cellHeight += height();
	}
	
	
	cellHeight = ceil(cellHeight);
	
	if ( key )
	{
		[self.cache cacheHeight:cellHeight ForKey:key];
	}

	return cellHeight;
}

- (void)clearHeightCache
{
	[self.cache clearCache];
}

@end

#pragma mark -

@implementation UICollectionView (HeightLayoutCell)

- (HeightLayoutCell *)heightlayout
{
	HeightLayoutCell * obj = objc_getAssociatedObject(self, _cmd);
	if ( !obj ){
		obj = [HeightLayoutCell new];
		objc_setAssociatedObject(self, _cmd, obj, OBJC_ASSOCIATION_RETAIN_NONATOMIC);
	}
	return obj;
}

- (void)addTemplateCell:(UICollectionViewCell *)cell
			 identifier:(NSString *)inentifier
{
	[self.heightlayout addTemplateCell:cell
							 identifier:inentifier];
}

#pragma mark -

- (CGFloat)heightForCellIndentitier:(NSString *)indentifier
							   data:(id)data
							  width:(CGFloat)width
						  indexPath:(NSIndexPath *)indexPath
				   additionalHeight:(CGFloat (^)())height
{
	return 	[self.heightlayout heightForCellIndentitier:indentifier
													data:data
												   width:width
											   indexPath:indexPath
										additionalHeight:height];
}

- (CGFloat)heightForCellIndentitier:(NSString *)indentifier
							   data:(id)data
							  width:(CGFloat)width
								key:(id)key
				   additionalHeight:(CGFloat (^)())height
{
	return [self.heightlayout heightForCellIndentitier:indentifier
												  data:data
												 width:width
												   key:key
									  additionalHeight:height];
}


#pragma mark -

- (void)invalidateIndexPath:(NSIndexPath *)indexPath
{
	if ( [self.heightlayout.cache existCacheForIndexPath:indexPath] )
	{
		NSMutableDictionary * sectionDic = self.heightlayout.cache.cacheByIndexPath[indexPath.section];
		[sectionDic removeObjectForKey:@(indexPath.item)];
	}
}

- (void)invalidateKey:(id)key
{
	if ( [self.heightlayout.cache existCacheForKey:key] )
	{
		[self.heightlayout.cache.cacheByKey removeObjectForKey:key];
	}
}

- (void)clearHeightCache
{
	[self.heightlayout clearHeightCache];
}

@end




