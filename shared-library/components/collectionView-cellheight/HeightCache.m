//
//  HeightCache.m
//  timelineDemo2
//
//  Created by GeekZoo on 16/3/25.
//  Copyright © 2016年 Huxiao. All rights reserved.
//

#import "HeightCache.h"

@implementation HeightCache

#pragma mark -

- (instancetype)init
{
	self = [super init];
	if (self)
	{
		_cacheByIndexPath = [NSMutableArray array];
		_cacheByKey = [NSMutableDictionary dictionary];
	}
	return self;
}

- (void)clearCache
{
	[self.cacheByIndexPath removeAllObjects];
	[self.cacheByKey removeAllObjects];
}

#pragma mark - by indexPath

- (BOOL)existCacheForIndexPath:(NSIndexPath *)indexPath
{
	if ( indexPath.section < self.cacheByIndexPath.count )
	{
		NSMutableDictionary * sectionDic = self.cacheByIndexPath[indexPath.section];
		
		NSNumber * height = sectionDic[@(indexPath.row)];
		
		return  height && [height floatValue];
	}
	return NO;
}

- (void)cacheHeight:(CGFloat)height ForIndexPath:(NSIndexPath *)indexPath
{
	if ( !indexPath )
		return;
	
	if ( indexPath.section == self.cacheByIndexPath.count )
	{
		NSMutableDictionary * dic = [NSMutableDictionary dictionary];
		
		[dic setObject:@(height) forKey:@(indexPath.item)];
		
		[self.cacheByIndexPath addObject:dic];
	}
	else
	{
		NSMutableDictionary * dic = self.cacheByIndexPath[indexPath.section];
		
		[dic setObject:@(height) forKey:@(indexPath.item)];
	}
}

- (CGFloat)heightForIndexPath:(NSIndexPath *)indexPath
{
	return [self.cacheByIndexPath[indexPath.section][@(indexPath.row)] floatValue];
}

#pragma mark - by key

- (BOOL)existCacheForKey:(id)key
{
	NSNumber *number = [self.cacheByKey objectForKey:key];
	
	return number && [number floatValue];
}

- (void)cacheHeight:(CGFloat)height ForKey:(id)key
{
	[self.cacheByKey setValue:@(height) forKey:key];
}

- (CGFloat)heightForKey:(id)key
{
	return [self.cacheByKey[key] floatValue];
}

@end
