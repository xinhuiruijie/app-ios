//
//  HeightLayoutCell.h
//  timelineDemo2
//
//  Created by GeekZoo on 16/3/23.
//  Copyright © 2016年 Huxiao. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "HeightCache.h"

@interface HeightLayoutCell : NSObject
@end


/**
 *  cell的top, left, bottom, right均需要有相关约束，才能使用该工具来算高
 */
@interface UICollectionView (HeightLayoutCell)

@property (nonatomic, strong, readonly) HeightLayoutCell * heightlayout;

/**
 *  添加用于计算高度的tempateCell
 *
 *  @param cell
 *  @param inentifier 标识
 */
- (void)addTemplateCell:(UICollectionViewCell *)cell
			 identifier:(NSString *)inentifier;

- (CGFloat)heightForCellIndentitier:(NSString *)indentifier
							   data:(id)data
							  width:(CGFloat)width
						  indexPath:(NSIndexPath *)indexPath
				   additionalHeight:(CGFloat (^)())height;

- (CGFloat)heightForCellIndentitier:(NSString *)indentifier
							   data:(id)data
							  width:(CGFloat)width
								key:(id)key
				   additionalHeight:(CGFloat (^)())height;

- (void)invalidateIndexPath:(NSIndexPath *)indexPath;
- (void)invalidateKey:(id)key;

- (void)clearHeightCache;
@end



