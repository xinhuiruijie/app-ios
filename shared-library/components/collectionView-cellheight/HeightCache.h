//
//  HeightCache.h
//  timelineDemo2
//
//  Created by GeekZoo on 16/3/25.
//  Copyright © 2016年 Huxiao. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HeightCache : NSObject

@property (nonatomic, strong) NSMutableArray<NSMutableDictionary<NSNumber *, NSNumber*> *> * cacheByIndexPath;

@property (nonatomic, strong) NSMutableDictionary * cacheByKey;

- (BOOL)existCacheForIndexPath:(NSIndexPath *)indexPath;
- (void)cacheHeight:(CGFloat)height ForIndexPath:(NSIndexPath *)indexPath;
- (CGFloat)heightForIndexPath:(NSIndexPath *)indexPath;


- (BOOL)existCacheForKey:(id)key;
- (void)cacheHeight:(CGFloat)height ForKey:(id)key;
- (CGFloat)heightForKey:(id)key;

- (void)clearCache;

@end
