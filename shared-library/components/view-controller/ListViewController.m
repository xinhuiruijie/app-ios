//
//  ListViewController.m
//  gaibianjia
//
//  Created by QFish on 7/7/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import "ListViewController.h"
#import "UIScrollView+PullLoader.h"

@implementation ListViewController

- (UIScrollView *)list
{
	return nil;
}

- (void)viewDidLoad
{
	[super viewDidLoad];
	
	[self setupRefreshHeaderView];
}

#pragma mark - Header Refresh

- (void)setupRefreshHeaderView
{
	@weakify( self );
	[self.list addHeaderPullLoader:^{
		@strongify( self );
		[self refresh];
	}];
}

- (void)setupRefreshFooterView
{
	if ( self.list.footer == nil )
	{
		if ( !self.model.isEmpty )
		{
			@weakify(self);
			[self.list addFooterPullLoader:^{
				@strongify( self );
				[self loadmore];
			}];
		}
	}
	else
	{
		if ( self.model.isEmpty )
		{
			[self.list removeFooter];
		}
	}
}

- (void)refresh
{
	if ( [self.model isKindOfClass:STIStreamModel.class] )
	{
		[(STIStreamModel *)self.model refresh];
	}
	else if ( [self.model isKindOfClass:STIOnceModel.class] )
	{
		[(STIOnceModel *)self.model refresh];
	}
}

- (void)loadmore
{
	if ( [self.model isKindOfClass:STIStreamModel.class] )
	{
		[(STIStreamModel *)self.model loadMore];
	}
}

- (void)removeHeader
{
    [self.list removeHeader];
}

#pragma mark -

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return NO;
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

- (UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationPortrait;
}

@end

