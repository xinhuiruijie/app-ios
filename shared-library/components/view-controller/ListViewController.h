//
//  ListViewController.h
//  gaibianjia
//
//  Created by QFish on 7/7/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ListViewController <NSObject>

- (UIScrollView *)list;

@optional
- (STIReadModel *)model;
- (void)refresh;
- (void)loadmore;

@end

@interface ListViewController : UIViewController<ListViewController>

- (void)setupRefreshHeaderView;
- (void)setupRefreshFooterView;
- (void)removeHeader;

@end
