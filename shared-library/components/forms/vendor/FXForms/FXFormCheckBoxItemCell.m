//
//  FXFormCheckBoxItemCell.m
//  xueyou
//
//  Created by QFish on 8/7/15.
//  Copyright (c) 2015 QFish. All rights reserved.
//

#import "FXFormCheckBoxItemCell.h"

@interface FXFormCheckBoxItemCell ()
@property (nonatomic, weak) IBOutlet UILabel * titleLabel;
@property (nonatomic, weak) IBOutlet UIImageView * indicitor;
@end

@implementation FXFormCheckBoxItemCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.layer.cornerRadius = 5;
    self.layer.borderColor = [AppTheme mainColor].CGColor;
    self.layer.borderWidth = 1;

    self.selected = NO;
}

- (void)setSelected:(BOOL)selected
{
    [super setSelected:selected];
    
    if ( selected ) {
        self.backgroundColor = [AppTheme mainColor];
        self.titleLabel.textColor = [UIColor whiteColor];
//        self.indicitor.hidden = NO;
    } else {
        self.backgroundColor = [UIColor whiteColor];
        self.titleLabel.textColor = [UIColor darkGrayColor];
//        self.indicitor.hidden = YES;
    }
}

- (void)dataDidChange
{
    if ( self.data ) {
        self.titleLabel.text = self.data;
    }
}

@end
