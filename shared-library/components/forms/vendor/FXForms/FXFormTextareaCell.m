//
//  FXFormTextareaCell.m
//  xueyou
//
//  Created by QFish on 9/11/15.
//  Copyright (c) 2015 QFish. All rights reserved.
//

#import "FXFormTextareaCell.h"

@interface FXFormTextareaCell () <UITextViewDelegate>
@property (nonatomic, strong) IBOutlet UITextView *textView;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIView * background;
@property (nonatomic, weak) IBOutlet UIView * bottomLine;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint * bottomLineBC;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint * backgroundTC;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint * backgroundBC;
@property (nonatomic, assign) CGFloat cornerRadius;
@end

@implementation FXFormTextareaCell

+ (CGFloat)heightForField:(FXFormField *)field width:(CGFloat)width
{
    static UITextView *textView;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        textView = [[UITextView alloc] init];
        textView.font = [UIFont systemFontOfSize:17];
    });
    
    textView.text = [field fieldDescription] ?: @" ";
    CGSize textViewSize = [textView sizeThatFits:CGSizeMake(width - 10 - 10, FLT_MAX)];
    
    CGFloat height = [field.title length]? 21: 0; // label height
    height += 10 + ceilf(textViewSize.height) + 10;
    return height;
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
}

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.background.layer.borderWidth = [AppTheme onePixel];
    self.background.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    self.cornerRadius = 5;
    
    self.textView.delegate = self;
}

- (FXFormCellPosition)position
{
    NSIndexPath * indexPath = self.field.indexPath;
    NSUInteger count = [self.tableView numberOfRowsInSection:indexPath.section];
    
    if ( count == 1 ) return FXFormCellPositionOnly;
    if ( indexPath.row == 0 ) return FXFormCellPositionFirst;
    if ( indexPath.row == count - 1 ) return FXFormCellPositionLast;
    
    return FXFormCellPositionMiddle;
}

- (void)setPosition:(FXFormCellPosition)position
{
    self.bottomLine.hidden = YES;
    self.backgroundTC.constant = 0;
    self.backgroundBC.constant = 0;
    
    switch (position) {
        case FXFormCellPositionMiddle: {
            self.background.layer.cornerRadius = 0;
            self.backgroundTC.constant = -1;
            break;
        }
        case FXFormCellPositionFirst: {
            self.background.layer.cornerRadius = self.cornerRadius;
            //            self.background.backgroundColor = [UIColor redColor];
            self.backgroundBC.constant = -self.cornerRadius;
            self.bottomLineBC.constant = self.cornerRadius;
            self.bottomLine.hidden = NO;
            break;
        }
        case FXFormCellPositionLast: {
            self.background.layer.cornerRadius = self.cornerRadius;
            //            self.background.backgroundColor = [UIColor greenColor];
            self.backgroundTC.constant = -self.cornerRadius;
            break;
        }
        case FXFormCellPositionOnly: {
            self.background.layer.cornerRadius = self.cornerRadius;
            break;
        }
    }
}

- (void)setUp
{
    self.selectionStyle = UITableViewCellSelectionStyleNone;
    self.textLabel.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin;
    
//    self.textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 0, 320, 21)];
//    self.textView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleBottomMargin | UIViewAutoresizingFlexibleRightMargin;
//    self.textView.font = [UIFont systemFontOfSize:17];
//    self.textView.textColor = [UIColor colorWithRed:0.275f green:0.376f blue:0.522f alpha:1.000f];
//    self.textView.backgroundColor = [UIColor clearColor];
//    self.textView.delegate = self;
//    self.textView.scrollEnabled = NO;
//    [self.contentView addSubview:self.textView];
//    
//    self.detailTextLabel.textAlignment = NSTextAlignmentLeft;
//    self.detailTextLabel.numberOfLines = 0;
    
    [self.contentView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self.textView action:NSSelectorFromString(@"becomeFirstResponder")]];
}

- (void)dealloc
{
    _textView.delegate = nil;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
}

- (void)update
{
    self.position = self.position;
    
    self.titleLabel.text = self.field.title;
    self.textView.text = [self.field fieldDescription];
    
    self.textView.returnKeyType = UIReturnKeyDefault;
    self.textView.textAlignment = NSTextAlignmentLeft;
    self.textView.secureTextEntry = NO;
    
    if ([self.field.type isEqualToString:FXFormFieldTypeText])
    {
        self.textView.autocorrectionType = UITextAutocorrectionTypeDefault;
        self.textView.autocapitalizationType = UITextAutocapitalizationTypeSentences;
        self.textView.keyboardType = UIKeyboardTypeDefault;
    }
    else if ([self.field.type isEqualToString:FXFormFieldTypeUnsigned])
    {
        self.textView.autocorrectionType = UITextAutocorrectionTypeNo;
        self.textView.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.textView.keyboardType = UIKeyboardTypeNumberPad;
    }
    else if ([@[FXFormFieldTypeNumber, FXFormFieldTypeInteger, FXFormFieldTypeFloat] containsObject:self.field.type])
    {
        self.textView.autocorrectionType = UITextAutocorrectionTypeNo;
        self.textView.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.textView.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
    }
    else if ([self.field.type isEqualToString:FXFormFieldTypePassword])
    {
        self.textView.autocorrectionType = UITextAutocorrectionTypeNo;
        self.textView.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.textView.keyboardType = UIKeyboardTypeDefault;
        self.textView.secureTextEntry = YES;
    }
    else if ([self.field.type isEqualToString:FXFormFieldTypeEmail])
    {
        self.textView.autocorrectionType = UITextAutocorrectionTypeNo;
        self.textView.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.textView.keyboardType = UIKeyboardTypeEmailAddress;
    }
    else if ([self.field.type isEqualToString:FXFormFieldTypePhone])
    {
        self.textView.autocorrectionType = UITextAutocorrectionTypeNo;
        self.textView.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.textView.keyboardType = UIKeyboardTypePhonePad;
    }
    else if ([self.field.type isEqualToString:FXFormFieldTypeURL])
    {
        self.textView.autocorrectionType = UITextAutocorrectionTypeNo;
        self.textView.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.textView.keyboardType = UIKeyboardTypeURL;
    }
}

- (void)textViewDidBeginEditing:(__unused UITextView *)textView
{
    [self.textView selectAll:nil];
}

- (void)textViewDidChange:(UITextView *)textView
{
    [self updateFieldValue];
    
    //show/hide placeholder
    self.detailTextLabel.hidden = ([textView.text length] > 0);
    
    //resize the tableview if required
    UITableView *tableView = [self tableView];
    [tableView beginUpdates];
    [tableView endUpdates];
    
    //scroll to show cursor
    CGRect cursorRect = [self.textView caretRectForPosition:self.textView.selectedTextRange.end];
    [tableView scrollRectToVisible:[tableView convertRect:cursorRect fromView:self.textView] animated:YES];
}

- (void)textViewDidEndEditing:(__unused UITextView *)textView
{
    [self updateFieldValue];
    
    if (self.field.action) self.field.action(self);
}

- (void)updateFieldValue
{
    self.field.value = self.textView.text;
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (BOOL)becomeFirstResponder
{
    return [self.textView becomeFirstResponder];
}

- (BOOL)resignFirstResponder
{
    return [self.textView resignFirstResponder];
}

@end
