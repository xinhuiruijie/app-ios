//
//  FXFormCheckBoxCell.m
//  xueyou
//
//  Created by QFish on 8/7/15.
//  Copyright (c) 2015 QFish. All rights reserved.
//

#import "FXFormCheckBoxCell.h"

@interface FXFormCheckBoxCell() <UICollectionViewDataSource, UICollectionViewDelegate>
@property (nonatomic, weak) IBOutlet UICollectionView * list;
@end

@implementation FXFormCheckBoxCell

- (void)awakeFromNib
{
    [super awakeFromNib];

    self.list.allowsMultipleSelection = YES;
    [self.list registerNib:@"FXFormCheckBoxItemCell"];
}

+ (CGFloat)heightForField:(FXFormField *)field width:(CGFloat)width
{
    CGFloat height = ceil([field optionCount] / 4.f) * (30 + 5) + 15 + 1;
    return height;
}

- (void)didSelectWithTableView:(UITableView *)tableView
                    controller:(UIViewController *)controller
{
    
}

#pragma mark - Bind data

- (void)setUp
{
}

- (void)update
{
    [self.list reloadData];
    [self selectItems];
}

- (void)selectItems
{
    NSInteger i = 0;
    
    while ( i < self.field.optionCount )
    {
        if ( [self.field isOptionSelectedAtIndex:i] )
        {
            [self.list selectItemAtIndexPath:[NSIndexPath indexPathForItem:i inSection:0]
                                    animated:NO
                              scrollPosition:UICollectionViewScrollPositionNone];
        }
        
        i++;
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.field optionCount];
}

// The cell that is returned must be retrieved from a call to -dequeueReusableCellWithReuseIdentifier:forIndexPath:
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell * cell = [collectionView dequeue:@"FXFormCheckBoxItemCell" forIndexPath:indexPath];
    
    cell.data = [self.field optionDescriptionAtIndex:indexPath.item];
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.field setOptionSelected:YES atIndex:indexPath.item];
    
    if (self.field.action) self.field.action(self);
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self.field setOptionSelected:NO atIndex:indexPath.item];
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (BOOL)collectionView:(UICollectionView *)collectionView shouldDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

#pragma mark - UICollectionViewDelegateFlowLayoutDelegate


- (CGSize)item1Size
{
    static CGSize size;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        size.width  = (self.list.width - 10 - 10) / 4 - 5;
        size.height = 30;
    });
    return size;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(10, 10, 10, 10);
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [self item1Size];
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumLineSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
}

- (CGFloat)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout minimumInteritemSpacingForSectionAtIndex:(NSInteger)section
{
    return 5;
}

@end
