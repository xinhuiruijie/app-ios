//
//  FXFormInputCell.m
//  xueyou
//
//  Created by QFish on 9/11/15.
//  Copyright (c) 2015 QFish. All rights reserved.
//

#import "FXFormInputCell.h"

@interface FXFormInputCell () <UITextFieldDelegate>

@property (nonatomic, strong, readwrite) IBOutlet UITextField *textField;
@property (nonatomic, weak) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UILabel *unitLabel;
@property (nonatomic, weak) IBOutlet UIView * background;
@property (nonatomic, weak) IBOutlet UIView * bottomLine;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint * bottomLineBC;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint * backgroundTC;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint * backgroundBC;
@property (nonatomic, assign) CGFloat cornerRadius;
@property (nonatomic, assign, getter = isReturnKeyOverriden) BOOL returnKeyOverridden;

@end

@implementation FXFormInputCell

- (void)awakeFromNib
{
    [super awakeFromNib];
    
    self.background.layer.borderWidth = [AppTheme onePixel];
    self.background.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    self.textField.delegate = self;
    self.cornerRadius = 5;
    self.unitLabel.text = nil;
}

- (FXFormCellPosition)position
{
    NSIndexPath * indexPath = self.field.indexPath;
    NSUInteger count = [self.tableView numberOfRowsInSection:indexPath.section];
    
    if ( count == 1 ) return FXFormCellPositionOnly;
    if ( indexPath.row == 0 ) return FXFormCellPositionFirst;
    if ( indexPath.row == count - 1 ) return FXFormCellPositionLast;
    
    return FXFormCellPositionMiddle;
}

- (void)setPosition:(FXFormCellPosition)position
{
    self.bottomLine.hidden = YES;
    self.backgroundTC.constant = 0;
    self.backgroundBC.constant = 0;
    self.textField.returnKeyType = UIReturnKeyNext;
    
    switch (position) {
        case FXFormCellPositionMiddle: {
            self.background.layer.cornerRadius = 0;
            self.backgroundTC.constant = -1;
            break;
        }
        case FXFormCellPositionFirst: {
            self.background.layer.cornerRadius = self.cornerRadius;
//            self.background.backgroundColor = [UIColor redColor];
            self.backgroundBC.constant = -self.cornerRadius;
            self.bottomLineBC.constant = self.cornerRadius;
            self.bottomLine.hidden = NO;
            break;
        }
        case FXFormCellPositionLast: {
            self.background.layer.cornerRadius = self.cornerRadius;
//            self.background.backgroundColor = [UIColor greenColor];
            self.backgroundTC.constant = -self.cornerRadius;
    self.textField.returnKeyType = UIReturnKeyDone;
            break;
        }
        case FXFormCellPositionOnly: {
            self.background.layer.cornerRadius = self.cornerRadius;
            break;
        }
    }
}

- (void)setUp
{
     self.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [self.contentView addGestureRecognizer:[[UITapGestureRecognizer alloc] initWithTarget:self.textField action:NSSelectorFromString(@"becomeFirstResponder")]];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(textDidChange) name:UITextFieldTextDidChangeNotification object:self.textField];
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)update
{
    self.position = self.position;
    
    self.titleLabel.text = self.field.title;
    self.textField.placeholder = [self.field.placeholder fieldDescription];
    self.textField.text = [self.field fieldDescription];
    
    self.textField.textAlignment = [self.field.title length]? NSTextAlignmentRight: NSTextAlignmentLeft;
    self.textField.secureTextEntry = NO;
    
    if ([self.field.type isEqualToString:FXFormFieldTypeText])
    {
        self.textField.autocorrectionType = UITextAutocorrectionTypeDefault;
        self.textField.autocapitalizationType = UITextAutocapitalizationTypeSentences;
        self.textField.keyboardType = UIKeyboardTypeDefault;
    }
    else if ([self.field.type isEqualToString:FXFormFieldTypeUnsigned])
    {
        self.textField.autocorrectionType = UITextAutocorrectionTypeNo;
        self.textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.textField.keyboardType = UIKeyboardTypeNumberPad;
        self.textField.textAlignment = NSTextAlignmentRight;
    }
    else if ([@[FXFormFieldTypeNumber, FXFormFieldTypeInteger, FXFormFieldTypeFloat] containsObject:self.field.type])
    {
        self.textField.autocorrectionType = UITextAutocorrectionTypeNo;
        self.textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.textField.keyboardType = UIKeyboardTypeNumbersAndPunctuation;
        self.textField.textAlignment = NSTextAlignmentRight;
    }
    else if ([self.field.type isEqualToString:FXFormFieldTypePassword])
    {
        self.textField.autocorrectionType = UITextAutocorrectionTypeNo;
        self.textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.textField.keyboardType = UIKeyboardTypeDefault;
        self.textField.secureTextEntry = YES;
    }
    else if ([self.field.type isEqualToString:FXFormFieldTypeEmail])
    {
        self.textField.autocorrectionType = UITextAutocorrectionTypeNo;
        self.textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.textField.keyboardType = UIKeyboardTypeEmailAddress;
    }
    else if ([self.field.type isEqualToString:FXFormFieldTypePhone])
    {
        self.textField.autocorrectionType = UITextAutocorrectionTypeNo;
        self.textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.textField.keyboardType = UIKeyboardTypePhonePad;
    }
    else if ([self.field.type isEqualToString:FXFormFieldTypeURL])
    {
        self.textField.autocorrectionType = UITextAutocorrectionTypeNo;
        self.textField.autocapitalizationType = UITextAutocapitalizationTypeNone;
        self.textField.keyboardType = UIKeyboardTypeURL;
    }
}

- (BOOL)textFieldShouldBeginEditing:(__unused UITextField *)textField
{
    //welcome to hacksville, population: you
    if (!self.returnKeyOverridden)
    {
        //get return key type
        UIReturnKeyType returnKeyType = UIReturnKeyDone;
        UITableViewCell <FXFormFieldCell> *nextCell = self.nextCell;
        if ([nextCell canBecomeFirstResponder])
        {
            returnKeyType = UIReturnKeyNext;
        }
        
        self.textField.returnKeyType = returnKeyType;
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(__unused UITextField *)textField
{
    [self.textField selectAll:nil];
}

- (void)textDidChange
{
    [self updateFieldValue];
}

- (BOOL)textFieldShouldReturn:(__unused UITextField *)textField
{
    if (self.textField.returnKeyType == UIReturnKeyNext)
    {
        [self.nextCell becomeFirstResponder];
    }
    else
    {
        [self.textField resignFirstResponder];
    }
    return NO;
}

- (void)textFieldDidEndEditing:(__unused UITextField *)textField
{
    [self updateFieldValue];
    
    if (self.field.action) self.field.action(self);
}

- (void)updateFieldValue
{
    self.field.value = self.textField.text;
}

- (BOOL)canBecomeFirstResponder
{
    return YES;
}

- (BOOL)becomeFirstResponder
{
    return [self.textField becomeFirstResponder];
}

- (BOOL)resignFirstResponder
{
    return [self.textField resignFirstResponder];
}

@end
