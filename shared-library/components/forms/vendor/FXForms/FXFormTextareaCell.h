//
//  FXFormTextareaCell.h
//  xueyou
//
//  Created by QFish on 9/11/15.
//  Copyright (c) 2015 QFish. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FXForms.h"

@interface FXFormTextareaCell : FXFormBaseCell
@property (nonatomic, readonly) UITextView *textView;
@end
