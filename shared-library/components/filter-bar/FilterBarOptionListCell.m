//
//  FilterBarOptionListCell.m
//  gaibianjia
//
//  Created by QFish on 6/5/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import "FilterBarOptionListCell.h"

@interface FilterBarOptionListCell ()
@property (nonatomic, weak) IBOutlet UIButton * button;
@end

static UIImage * kNormalBackgroundImage;
static UIImage * kSelectedBackgroundImage;

@implementation FilterBarOptionListCell

- (void)setSelected:(BOOL)selected
{
	[super setSelected:selected];

	self.button.selected = selected;
}

- (void)awakeFromNib
{
	self.button.userInteractionEnabled = NO;
	
	static dispatch_once_t onceToken;
	dispatch_once(&onceToken, ^{
		kNormalBackgroundImage = [[UIImage imageNamed:@"b1_btn"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
		kSelectedBackgroundImage = [[UIImage imageNamed:@"b1_btn_sel"] resizableImageWithCapInsets:UIEdgeInsetsMake(10, 10, 10, 10)];
	});
	
	[self.button setBackgroundImage:kNormalBackgroundImage forState:UIControlStateNormal];
	[self.button setBackgroundImage:kSelectedBackgroundImage forState:UIControlStateSelected];
}

- (void)dataDidChange
{
	[self.button setTitle:[self.data valueForKey:@"name"] forState:UIControlStateNormal];
}

@end
