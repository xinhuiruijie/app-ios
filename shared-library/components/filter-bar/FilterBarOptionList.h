//
//  FilterBarOptionList.h
//  gaibianjia
//
//  Created by QFish on 6/5/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FilterBarOptionList : UIView

// Thw following properties valid in WideStyle
@property (nonatomic, assign) NSUInteger columnCount; // default 3
@property (nonatomic, assign) CGSize itemSize;		  // default 101 * 35
@property (nonatomic, assign) CGFloat maxHeight;	  // [UIScreen mainScreen].height / 2
@property (nonatomic, assign) CGFloat padding;		  // default 6

@property (nonatomic, copy) void (^ whenTapped)(void);
@property (nonatomic, copy) void (^ whenSelected)(id option);

@property (nonatomic, assign) BOOL shown;

- (void)setShown:(BOOL)shown completion:(void (^)(BOOL))completion;

@end
