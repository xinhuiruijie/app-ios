//
//  FilterBar.m
//  gaibianjia
//
//  Created by QFish on 6/5/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import "FilterBar.h"
#import "FilterBarOptionList.h"

#pragma mark - FilterBarData

@implementation FilterBarData
@end

#pragma mark - FilterBar

@interface FilterBar()
// W: width, C: constraint, H: height
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *line1WC; // 水平第一个分割线
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *line2WC; // 水平第二个分割线
@property (nonatomic, weak) IBOutlet NSLayoutConstraint *bottomLineHC; // 底部边框线

@property (nonatomic, strong) NSMutableArray * buttons;
@property (nonatomic, strong) NSMutableArray * filters;

@property (nonatomic, strong) FilterBarOptionList * optionList;

@property (nonatomic, strong) NSMutableArray * imageViews;
@end

@implementation FilterBar

+ (instancetype)spawn
{
	return [self loadFromNib];
}

- (void)awakeFromNib
{
	[@[self.line1WC, self.line2WC, self.bottomLineHC] enumerateObjectsUsingBlock:^(NSLayoutConstraint * obj, NSUInteger idx, BOOL *stop) {
		obj.constant = [AppTheme onePixel];
	}];
	
	self.buttons = [NSMutableArray array];
	self.filters = [NSMutableArray array];
	self.imageViews = [NSMutableArray array];
	@weakify(self);
	
	[self.subviews enumerateObjectsUsingBlock:^(UIView * obj, NSUInteger idx, BOOL *stop) {
		@strongify(self);
		if ( [obj isKindOfClass:UIButton.class] ) {
			[self.buttons addObject:obj];
		}
		if ( [obj isKindOfClass:UIImageView.class]) {
			[self.imageViews addObject:obj];
		}
	}];
}

#pragma mark -

- (void)dataDidChange
{
	[self.filters removeAllObjects];
	[self.filters addObjectsFromArray:self.data];
	
	[self refreshTitle];
}

- (void)refreshTitle
{
	[self.buttons enumerateObjectsUsingBlock:^(UIButton * obj, NSUInteger idx, BOOL *stop) {
		FilterBarData * data = self.filters[idx];
		NSString * title = nil;
		if ( data.selectedOption ) {
			title = [data.selectedOption valueForKey:@"name"];
		} else {
			title = data.title;
		}
		[obj setTitle:title forState:UIControlStateNormal];
		[obj setSelected:NO]; // 取消选中状态
	}];
}

#pragma mark - signal

handleSignal(button, click)
{
	__block NSUInteger index = 0;
	@weakify(self);
	[self.buttons enumerateObjectsUsingBlock:^(UIButton * obj, NSUInteger idx, BOOL *stop){
		@strongify(self);
		
		UIImageView * imageView = self.imageViews[idx];
		
		if ( signal.uiButton == obj )
		{
			if ( obj.selected )
			{
				obj.selected = NO;
				[self hideOptionListWithCompletion:nil];
			}
			else
			{
				obj.selected = YES;
				index = idx;
				imageView.image = [UIImage imageNamed:@"up"];
			}
		}
		else
		{
			obj.selected = NO;
			imageView.image = [UIImage imageNamed:@"down"];
		}
	}];
	self.selectedIndex = index;
//	[self.imageViews enumerateObjectsUsingBlock:^(UIImageView * obj, NSUInteger idx, BOOL * _Nonnull stop) {
//		if ( idx == index ) {
//			[obj setImage: [UIImage imageNamed:@"up"]];
//		}else
//		{
//			[obj setImage: [UIImage imageNamed:@"down"]];
//		}
//		
//	}];
	
	PERFORM_BLOCK_SAFELY(self.whenSelectedTitle);
	
	FilterBarData * data = self.filters[index];
	
	if ( nil == data.options || 0 == data.options.count )
	{
		[self hideOptionListWithCompletion:nil];
	}
	else
	{
		self.optionList.data = data;
		
		[self showOptionList];
	}
}

#pragma mark - FilterBarOptionList

- (FilterBarOptionList *)optionList
{
	@weakify(self);

	if ( _optionList == nil )
	{
		_optionList = [FilterBarOptionList spawn];
		
		CGRect frame = self.superview.frame;
		frame.origin.y = self.height;
		frame.size.height -= self.height;
		_optionList.frame = frame;
		
		_optionList.whenSelected = ^ (id option) {
			@strongify(self);
			FilterBarData * data = self.filters[self.selectedIndex];
			data.selectedOption = option;
			@weakify(self);
			[self refreshTitle];
			[self hideOptionListWithCompletion:^{
				@strongify(self);
				PERFORM_BLOCK_SAFELY(self.whenSelectedOption);
			}];
		};
		
		_optionList.whenTapped = ^ {
			@strongify(self);
			[self hideOptionListWithCompletion:nil];
		};
	}
	
	return _optionList;
}

- (void)showOptionList
{
	[self.superview insertSubview:self.optionList belowSubview:self];
	self.optionList.shown = YES;
}

- (void)hideOptionListWithCompletion:(void (^)(void))completion
{
	[self.imageViews enumerateObjectsUsingBlock:^(UIImageView * obj, NSUInteger idx, BOOL * _Nonnull stop)
	{
		[obj setImage: [UIImage imageNamed:@"down"]];
	}];
	
	@weakify(self);
	[self.optionList setShown:NO completion:^(BOOL finished){
		@strongify(self);
		[self.optionList removeFromSuperview];
		[self refreshTitle];
		PERFORM_BLOCK_SAFELY(completion);
	}];
}

@end
