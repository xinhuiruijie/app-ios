//
//  FilterBarOptionList.m
//  gaibianjia
//
//  Created by QFish on 6/5/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import "FilterBarOptionList.h"
#import "FilterBarOptionListCell.h"
#import "FilterBarOptionListWideCell.h"
#import "FilterBar.h"

@interface FilterBarOptionList () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>
@property (nonatomic, weak) IBOutlet UIButton * background;
@property (nonatomic, weak) IBOutlet UICollectionView * list;
@property (nonatomic, weak) IBOutlet NSLayoutConstraint * listHC;
@property (nonatomic, strong) NSArray * options;
@property (nonatomic, assign) FilterBarOptionStyle optionStyle;
@property (nonatomic, strong) id selectedOption;
@end

@implementation FilterBarOptionList

+ (instancetype)spawn
{
	return [self loadFromNib];
}

- (void)awakeFromNib
{
	[super awakeFromNib];
	
	[self.list registerNib:[FilterBarOptionListCell nib] forCellWithReuseIdentifier:@"FilterBarOptionListCell"];
	[self.list registerNib:[FilterBarOptionListWideCell nib] forCellWithReuseIdentifier:@"FilterBarOptionListWideCell"];
	self.list.alpha = 0;
	
	self.columnCount = 3;
	self.maxHeight = [UIScreen mainScreen].bounds.size.height / 2;
	self.padding = 6;
}

- (IBAction)tapped:(UIGestureRecognizer *)gesture
{
	PERFORM_BLOCK_SAFELY(self.whenTapped);
}

- (void)dataDidChange
{
	self.optionStyle = ((FilterBarData *)self.data).optionStyle;
	self.options = ((FilterBarData *)self.data).options;
	self.selectedOption = ((FilterBarData *)self.data).selectedOption;
	
	CGFloat listHeight;
	CGSize itemSize;
	
	if ( self.optionStyle == FilterBarOptionStyleWide )
	{
		itemSize = CGSizeMake(self.width, 40);
		listHeight = ceil(self.options.count * 1.0) * itemSize.height;
	}
	else
	{
		itemSize = CGSizeMake(101, 35);
		listHeight = ceil(self.options.count * 1.0 / self.columnCount) * itemSize.height + self.padding * 2;
	}

	self.itemSize = itemSize;
	self.listHC.constant = MIN(listHeight, self.maxHeight);
	
	[self setNeedsLayout];
	
	[self.list reloadData];
}

#pragma mark - 

- (void)setShown:(BOOL)shown
{
	[self setShown:shown completion:nil];
}

- (void)setShown:(BOOL)shown completion:(void (^)(BOOL))completion
{
	if ( _shown == shown )
		return;
	
	_shown = shown;
	
	if ( shown )
	{
		[UIView animateWithDuration:0.3 animations:^{
			self.list.alpha = 1;
			self.background.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.2];
		} completion:completion];
	}
	else
	{
		[UIView animateWithDuration:0.3 animations:^{
			self.list.alpha = 0;
			self.background.backgroundColor = [UIColor clearColor];
		} completion:completion];
	}
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
	return self.options.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
	UICollectionViewCell * cell = nil;

	if ( self.optionStyle == FilterBarOptionStyleWide )
	{
		cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FilterBarOptionListWideCell" forIndexPath:indexPath];
	}
	else
	{
		cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FilterBarOptionListCell" forIndexPath:indexPath];
	}
	
	cell.data = self.options[indexPath.item];
	cell.selected = [[cell.data valueForKey:@"id"] isEqual:[self.selectedOption valueForKey:@"id"]];
	return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
	return self.itemSize;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
	if ( self.optionStyle == FilterBarOptionStyleWide )
	{
		return UIEdgeInsetsZero;
	}
	else
	{
		CGFloat width = [UIScreen mainScreen].bounds.size.width;
		CGFloat padding = (width - self.itemSize.width * self.columnCount) / 2.0;
		return UIEdgeInsetsMake(self.padding, padding, self.padding, padding);
	}
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
	UICollectionViewCell * cell = [collectionView cellForItemAtIndexPath:indexPath];
	cell.selected = YES;
	
	PERFORM_BLOCK_SAFELY(self.whenSelected, self.options[indexPath.item]);
}

- (void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath
{
	UICollectionViewCell * cell = [collectionView cellForItemAtIndexPath:indexPath];
	cell.selected = NO;
}

@end
