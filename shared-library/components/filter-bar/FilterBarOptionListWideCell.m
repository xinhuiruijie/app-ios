//
//  FilterBarOptionListWideCell.m
//  gaibianjia
//
//  Created by QFish on 6/9/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import "FilterBarOptionListWideCell.h"

@interface FilterBarOptionListWideCell ()
@property (nonatomic, weak) IBOutlet NSLayoutConstraint * lineHC;
@property (nonatomic, weak) IBOutlet UILabel * nameLabel;
@property (nonatomic, weak) IBOutlet UIImageView * accessory;
@end

@implementation FilterBarOptionListWideCell

- (void)setSelected:(BOOL)selected
{
	[super setSelected:selected];
	
	self.accessory.hidden = !selected;
	
	self.nameLabel.textColor = selected ? [AppTheme mainColor] : [AppTheme titleColor];
}

- (void)awakeFromNib
{
	[super awakeFromNib];
	
	self.lineHC.constant = [AppTheme onePixel];
}

- (void)dataDidChange
{
	self.nameLabel.text = [self.data valueForKey:@"name"];
}

@end
