//
//  FilterBar.h
//  gaibianjia
//
//  Created by QFish on 6/5/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

/*
 
 Example:
 
 ```
 #pragma mark - Filter Bar
 
 - (void)updateFilter
 {
	self.filterBar.data = [self filterData];
 }
 
 - (void)loadFilter
 {
	self.model.buildingId = self.buildingId;
	self.model.layoutId = ((HOUSE_LAYOUT *)((FilterBarData *)self.filters[0]).selectedOption).id;
	self.model.styleId = ((DECORATION_STYLE *)((FilterBarData *)self.filters[1]).selectedOption).id;
	self.model.sort = ((FilterBarData *)self.filters[2]).selectedOption[@"id"];
 }
 
 - (void)setupFilter
 {
	self.listTC.constant = kFilterHeight;
	
	self.filterBar = [FilterBar spawn];
	[self.view addSubview:self.filterBar];
	
	@weakify(self);
	self.filterBar.whenSelectedOption = ^{
		@strongify(self);
		[self loadFilter];
		[self.model refresh];
	};
 }
 
 - (NSArray *)filterData
 {
	FilterBarData * data1 = [FilterBarData new];
	data1.title = @"房型";
	data1.options = self.filterModel.layouts;
	data1.selectedOption = data1.selectedOption ?: self.houseLayout;
 
	FilterBarData * data2 = [FilterBarData new];
	data2.title = @"风格";
	data2.options = self.filterModel.styles;
	data2.selectedOption = data2.selectedOption ?: nil;
 
	FilterBarData * data3 = [FilterBarData new];
	data3.title = @"最新";
	data3.optionStyle = FilterBarOptionStyleWide;
	data3.options = self.filterModel.sorts;
	data3.selectedOption = data3.selectedOption ?: data3.options.firstObject;
 
	_filters = @[data1, data2, data3];
	
	return _filters;
 }

 ```
 */

// 用到的数据结构里必须有 name 字段，id 字段

typedef NS_ENUM(NSUInteger, FilterBarOptionStyle) {
	FilterBarOptionStyleDefault,
	FilterBarOptionStyleWide,
};

@interface FilterBarData : NSObject
@property (nonatomic, strong) NSString * title;
@property (nonatomic, strong) NSArray * options;
@property (nonatomic, assign) FilterBarOptionStyle optionStyle;
@property (nonatomic, strong) id selectedOption; // OUT
@end

#pragma mark -

@interface FilterBar : UIView
@property (nonatomic, copy) void (^whenSelectedTitle)(void);
@property (nonatomic, copy) void (^whenSelectedOption)(void);
@property (nonatomic, assign) NSUInteger selectedIndex;
@property (nonatomic, strong) NSArray * selectedOptions;
@end
