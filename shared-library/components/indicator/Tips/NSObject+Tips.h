//
//  NSObject+Tips.h
//  Foomoo
//
//  Created by QFish on 6/6/14.
//  Copyright (c) 2014 QFish.inc. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark - UIView

@interface UIView (Tips)
- (void)presentMessage:(NSString *)message withTips:(NSString *)tips;
- (void)presentMessageTips:(NSString *)message;
- (void)presentMessageTips:(NSString *)message
                customView:(UIView *)customView;
- (void)presentSuccessTips:(NSString *)message;
- (void)presentFailureTips:(NSString *)message;
- (void)presentLoadingTips:(NSString *)message;
- (void)presentLoadingTips:(NSString *)message
                customView:(UIView *)customView;
- (void)presentCustomLoadingTips:(NSString *)message
                      customView:(UIView *)customView;

- (void)dismissTips;

@end

#pragma mark - UIViewController

@interface UIViewController (Tips)
- (void)presentMessage:(NSString *)message withTips:(NSString *)tips;
- (void)presentMessageTips:(NSString *)message;
- (void)presentMessageTips:(NSString *)message
                customView:(UIView *)customView;
- (void)presentSuccessTips:(NSString *)message;
- (void)presentFailureTips:(NSString *)message;
- (void)presentLoadingTips:(NSString *)message;
- (void)presentLoadingTips:(NSString *)message
                customView:(UIView *)customView;
- (void)presentCustomLoadingTips:(NSString *)message
                      customView:(UIView *)customView;

- (void)dismissTips;

@end
