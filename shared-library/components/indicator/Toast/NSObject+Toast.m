//
//  NSObject+Tips.m
//  Foomoo
//
//  Created by QFish on 6/6/14.
//  Copyright (c) 2014 QFish.inc. All rights reserved.
//

#import "NSObject+Tips.h"
#import "CRToast.h"

@implementation UIView (Toast)

- (void)showToast:(NSString *)message autoHide:(BOOL)autoHide
{
	[CRToastManager showNotificationWithOptions:[self optionsWithTitle:message] completionBlock:^{}];
}

- (void)toastMessageTips:(NSString *)message
{
	if ( ![CRToastManager isShow] )
	{
		[self showToast:message autoHide:YES];
	}
}

- (void)toastSuccessTips:(NSString *)message
{
	if ( ![CRToastManager isShow] )
	{
		[self showToast:message autoHide:YES];
	}
}

- (void)toastFailureTips:(NSString *)message
{
	if ( ![CRToastManager isShow] )
	{
		[self showToast:message autoHide:YES];
	}
}

- (void)toastLoadingTips:(NSString *)message
{
	if ( ![CRToastManager isShow] )
	{
		[self toastLoadingTips:message];
	}
}

- (void)dismissToast
{
	[CRToastManager dismissNotification:YES];
}

#pragma mark -

- (NSDictionary *)optionsWithTitle:(NSString *)title
{
	NSMutableDictionary * options = [@{
									   kCRToastNotificationTypeKey		:	@(CRToastTypeNavigationBar),
									   kCRToastNotificationPresentationTypeKey	:	@(CRToastPresentationTypeCover),
									   kCRToastUnderStatusBarKey		:	@(YES),
									   kCRToastTextKey					:	title,
									   kCRToastTextAlignmentKey			:	@(NSTextAlignmentCenter),
									   kCRToastTimeIntervalKey			:	@(1),
									   kCRToastAnimationInDirectionKey	:	@(0),
									   kCRToastAnimationOutDirectionKey	:	@(0)
									   } mutableCopy];
	return options;
}

@end

@implementation UIViewController (Toast)

- (void)toastMessageTips:(NSString *)message
{
	if ( ![CRToastManager isShow] )
	{
		[self.view showToast:message autoHide:YES];
	}
}

- (void)toastSuccessTips:(NSString *)message
{
	if ( ![CRToastManager isShow] )
	{
		[self.view showToast:message autoHide:YES];
	}
}

- (void)toastFailureTips:(NSString *)message
{
	if ( ![CRToastManager isShow] )
	{
		[self.view showToast:message autoHide:YES];
	}
}

- (void)toastLoadingTips:(NSString *)message
{
	if ( ![CRToastManager isShow] )
	{
		[self.view toastLoadingTips:message];
	}
}

- (void)dismissToast
{
    [self.view dismissTips];
}

@end