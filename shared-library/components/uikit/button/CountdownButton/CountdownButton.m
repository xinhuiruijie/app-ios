//
//  CTButton.m
//  buttonDemo
//
//  Created by Chenyun on 14/11/23.
//  Copyright (c) 2014年 geek-zoo. All rights reserved.
//

#import "CountdownButton.h"

@interface CountdownButton ()
{
	int _count;
	NSTimer * _timer;
//	UILabel * _label;
}

@end

@implementation CountdownButton

- (void)awakeFromNib
{
	[super awakeFromNib];

	[self setTitle:@"获取验证码" forState:UIControlStateDisabled];
	[self setTitle:@"获取验证码" forState:UIControlStateNormal];
    [self.titleLabel setFont:[UIFont systemFontOfSize:15]];
	
	_count = 60;

//	_label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)];
//	_label.textAlignment = NSTextAlignmentCenter;
//	_label.textColor = [UIColor colorWithRGBValue:0x585D64];
//	_label.font = self.titleLabel.font;
//	_label.adjustsFontSizeToFitWidth = YES;
//	[self addSubview:_label];
}

#pragma amrk -

- (void)start
{
    self.enabled = NO;
//    [self setBackgroundColor:[AppTheme lineColor]];
//    _label.textColor = [UIColor colorWithRGBValue:0x585D64];
	NSString * time = [NSString stringWithFormat:@"%ds后重新获取", _count];

//	_label.text = time;
    [self setTitle:time forState:UIControlStateDisabled];

	if ( _timer )
	{
		[_timer invalidate];
		_timer = nil;
	}

	_timer = [NSTimer scheduledTimerWithTimeInterval:1.f
											  target:self
											selector:@selector(updateLabel)
											userInfo:nil
											 repeats:YES];
}

- (void)updateLabel
{
	_count--;
	
	if ( _count <= 0 )
	{
		[self stop];
	}
	else
	{
		NSString * time = [NSString stringWithFormat:@"%ds后重新获取", _count];
//		_label.text = time;
        [self setTitle:time forState:UIControlStateDisabled];
	}
}

- (void)stop
{
	[_timer invalidate];
	_count = 60;
//    self.nibBorderColor = @"FA3E3D";
	self.enabled = YES;
//    _label.textColor = [UIColor colorWithRGBValue:0x585D64];
////    [self setBackgroundColor:[AppTheme mainColor]];
//	_label.text = @"";
    [self setTitle:@"重新获取" forState:UIControlStateDisabled];
	[self setTitle:@"重新获取" forState:UIControlStateNormal];
}

#pragma mark - 

- (void)dealloc
{
	[self stop];
}

//#pragma mark - 
//
//- (void)layoutSubviews
//{
//	[super layoutSubviews];
//	_label.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
//}

@end
