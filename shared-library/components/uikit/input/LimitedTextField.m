//
//  LimitedTextField.m
//  gaibianjia
//
//  Created by QFish on 6/19/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import "LimitedTextField.h"
#import "UITextField+BlocksKit.h"

@implementation LimitedTextField

- (void)awakeFromNib
{
	[super awakeFromNib];
	
	self.maxLength = 6;
	
	self.bk_shouldChangeCharactersInRangeWithReplacementStringBlock = ^(UITextField * textField, NSRange range, NSString * string)
	{
		NSString * text = [textField.text stringByReplacingCharactersInRange:range withString:string];
		
		if ( self.maxLength > 0 && text.length > self.maxLength )
		{
			return NO;
		}
		
		return YES;
	};
}

@end
