//
//  LimitedTextField.h
//  gaibianjia
//
//  Created by QFish on 6/19/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LimitedTextField : UITextField
@property (nonatomic, assign) NSInteger maxLength;
@end
