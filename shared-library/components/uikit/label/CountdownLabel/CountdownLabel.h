//
//  CountdownLabel.h
//  CountdownLabel
//
//  Created by Chenyun on 15/6/5.
//  Copyright (c) 2015年 geek-zoo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountdownLabel : UILabel

@property (nonatomic, copy) void (^whenTimeOut)(id data);

- (void)startCountdownWithTime:(int)time;
- (void)stop;

@end