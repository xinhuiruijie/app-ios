//
//  CountdownLabel.m
//  CountdownLabel
//
//  Created by Chenyun on 15/6/5.
//  Copyright (c) 2015年 geek-zoo. All rights reserved.
//

#import "CountdownLabel.h"

@implementation CountdownLabel
{
	int _count;
	NSTimer * _timer;
}

- (void)startCountdownWithTime:(int)time
{
	_count = time;
	[self start];
}

- (void)start
{
	NSString * time = [NSString stringWithFormat:@"请在%d:%d内付款", _count / 60, _count % 60];
	
	self.text = time;
	
	if ( _timer )
	{
		[_timer invalidate];
		_timer = nil;
	}
	
	_timer = [NSTimer scheduledTimerWithTimeInterval:1.f
											  target:self
											selector:@selector(updateLabel)
											userInfo:nil
											 repeats:YES];
}

- (void)updateLabel
{
	_count--;
	
	if ( _count < 0 )
	{
		if ( self.whenTimeOut )
		{
			self.whenTimeOut(nil);
		}

		[self stop];
	}
	else
	{
		NSString * time = [NSString stringWithFormat:@"请在%d:%d内付款", _count / 60, _count % 60];
		self.text = time;
	}
}

- (void)stop
{
	[_timer invalidate];
}

#pragma mark -

- (void)dealloc
{
	[self stop];
}

#pragma mark -

- (void)layoutSubviews
{
	[super layoutSubviews];
//	self.frame = CGRectMake(0, 0, self.frame.size.width, self.frame.size.height);
}

@end
