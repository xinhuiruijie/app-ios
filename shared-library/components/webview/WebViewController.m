//
//  WebViewController.m
//  gaibianjia
//
//  Created by QFish on 6/26/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import "WebViewController.h"
#import "SAMWebViewController.h"
#import "BaseWebViewController.h"

@implementation WebViewController

+ (UIViewController *)openURL:(NSString *)url in:(UIViewController *)vc
{
    return [self openURL:url in:vc title:nil toolbarHidden:YES];
}

+ (UIViewController *)openURL:(NSString *)url in:(UIViewController *)vc title:(NSString *)title
{
    return [self openURL:url in:vc title:title toolbarHidden:YES];
}

+ (UIViewController *)openURL:(NSString *)url in:(UIViewController *)vc title:(NSString *)title toolbarHidden:(BOOL)toolbarHidden
{
    UIViewController *viewController = [self openURL:url in:vc toolbarHidden:YES beforePushing:nil];
    if ( title && title.length )
    {
        viewController.title = title;
    }
    
    return viewController;
}

+ (UIViewController *)openURL:(NSString *)url in:(UIViewController *)vc beforePushing:(void (^)(UIViewController *vc))beforePushing
{
    return [self openURL:url in:vc toolbarHidden:YES beforePushing:beforePushing];
}

+ (UIViewController *)openURL:(NSString *)url in:(UIViewController *)vc toolbarHidden:(BOOL)toolbarHidden beforePushing:(void (^)(UIViewController *vc))beforePushing;
{
    if ( nil == url )
    {
        [[[UIAlertView alloc] initWithTitle:@"提醒" message:@"无法打开网页链接" delegate:nil cancelButtonTitle:@"确定" otherButtonTitles:nil] show];
        
        return nil;
    }
    
    if ( NO == [url hasPrefix:@"http://"] && NO == [url hasPrefix:@"https://"] )
    {
        url = [NSString stringWithFormat:@"http://%@", url];
    }
    
    NSArray * cookies = [[NSHTTPCookieStorage sharedHTTPCookieStorage] cookies];
    
    for ( NSHTTPCookie * cookie in cookies )
    {
        [[NSHTTPCookieStorage sharedHTTPCookieStorage] deleteCookie:cookie];
    }
    
    BaseWebViewController *webViewController = [[BaseWebViewController alloc] init];
    
    if ( beforePushing ) {
        beforePushing(webViewController);
    }
    
    [webViewController loadURL:url];
    
    webViewController.hidesBottomBarWhenPushed = YES;
    
    [vc.navigationController pushViewController:webViewController animated:YES];
    
    return webViewController;
}

@end
