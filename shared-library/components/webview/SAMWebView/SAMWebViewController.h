//
//  SAMWebViewController.h
//  SAMWebView
//
//  Created by Sam Soffes on 7/28/12.
//  Copyright 2012 Sam Soffes. All rights reserved.
//

#import "SAMWebView.h"

typedef void(^dissBlock)(void);

#pragma mark -

typedef  NS_ENUM(NSUInteger, ActionType){
    ActionTypePush = 0,       // push方式进入view controller
    ActionTypeFromWindow, // 从splashWindow进入webview
};

@interface SAMWebViewController : UIViewController <SAMWebViewDelegate>

@property (nonatomic, assign) BOOL toolbarHidden;
@property (nonatomic, readonly) SAMWebView *webView;
@property (nonatomic, readonly) NSURL *currentURL;
@property (nonatomic, assign) ActionType actionType;

@property (nonatomic, copy) dissBlock dissblock;
@end
