//
//  WebViewController.h
//  gaibianjia
//
//  Created by QFish on 6/26/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 * Example:
 *
 * // 打开百度
 * [WebViewController openURL:@"http://baidu.com" in:vc];
 *
 * // 打开百度，页面 title 为 标题
 * [WebViewController openURL:@"http://baidu.com" in:vc title:@"标题"];
 *
 * // 打开百度，页面 title 为 标题，同时显示 底部操作栏
 * [WebViewController openURL:@"http://baidu.com" in:vc title:@"标题" toolbarHidden:NO];
 *
 * // 最大限度自定
 * [WebViewController openURL:@"http://baidu.com" in:vc beforePushing:^(SAMWebViewController *vc) {
 *      // Push 之前的时机，比如：
 *      vc.hidesBottomBarWhenPushed = YES;
 * }];
 *
 */

@interface WebViewController : NSObject

+ (UIViewController *)openURL:(NSString *)url in:(UIViewController *)vc;
+ (UIViewController *)openURL:(NSString *)url in:(UIViewController *)vc title:(NSString *)title;
+ (UIViewController *)openURL:(NSString *)url in:(UIViewController *)vc title:(NSString *)title toolbarHidden:(BOOL)toolbarHidden;

+ (UIViewController *)openURL:(NSString *)url in:(UIViewController *)vc beforePushing:(void (^)(UIViewController *vc))beforePushing;
+ (UIViewController *)openURL:(NSString *)url in:(UIViewController *)vc toolbarHidden:(BOOL)toolbarHidden beforePushing:(void (^)(UIViewController *vc))beforePushing;

@end
