//
//  BaseWebViewController.m
//  GeekMall
//
//  Created by liuyadi on 2017/9/12.
//  Copyright © 2017年 GeekZooStudio. All rights reserved.
//

#import "BaseWebViewController.h"
#import "WebViewJavascriptBridge.h"

@interface BaseWebViewController () <UIWebViewDelegate>

@property (nonatomic, strong) UIWebView *webView;
@property (nonatomic, strong) WebViewJavascriptBridge *bridge;

@end

@implementation BaseWebViewController

- (instancetype)init {
    self = [super init];
    if (self) {
        self.webView.frame = self.view.bounds;
        [self.view addSubview:self.webView];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    @weakify(self);
    self.navigationItem.leftBarButtonItem = [AppTheme backItemWithHandler:^(id sender) {
        @strongify(self);
        [self.navigationController popViewControllerAnimated:YES];
    }];
    
    [self reloadBridge];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(loginSucceed) name:AuthorizationWasSucceedNotification object:nil];
}

- (void)loadURL:(NSString *)url {
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
}

#pragma mark - Accessors

- (UIWebView *)webView {
    if (_webView == nil) {
        _webView = [[UIWebView alloc] init];
        _webView.backgroundColor = [UIColor whiteColor];
        _webView.autoresizingMask = (UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight);
        _webView.delegate = self;
    }
    return _webView;
}

#pragma mark - UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)webView {
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    NSString *title = [webView stringByEvaluatingJavaScriptFromString:@"document.title"];
    if (title.length > 0) {
        self.title = title;
    }
}

//- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
//    [self loadFailedView];
//}

#pragma mark -

- (void)reloadView {
    [self.webView reload];
    
//    Reachability *reach = [Reachability reachabilityForInternetConnection];
//    NetworkStatus internetStatus = [reach currentReachabilityStatus];
//    if (internetStatus == NotReachable) {
//        //没有可以访问的网络
//        [self loadFailedView];
//    }
}

- (void)loadFailedView {
    NSString *filepath = [[NSBundle mainBundle] pathForResource:@"load-fail" ofType:@"html"];
    NSData *htmlData = [NSData dataWithContentsOfFile:filepath];
    NSString *htmlString = [[NSString alloc] initWithData:htmlData encoding:NSUTF8StringEncoding];
    
    //Add deepLink.js to the html file
    NSString *source = [[NSBundle mainBundle] pathForResource:@"deepLink" ofType:@"js"];
    NSString *jsString = [[NSString alloc] initWithData:[NSData dataWithContentsOfFile:source] encoding:NSUTF8StringEncoding];
    htmlString = [htmlString stringByReplacingOccurrencesOfString:@"<!--deepLink.js-->" withString:jsString];
    
    NSURL *baseURL = [NSURL fileURLWithPath:filepath];
    [self.webView loadHTMLString:htmlString baseURL:baseURL];
}

#pragma mark - JS Bridge

- (void)reloadBridge {
    self.bridge = [WebViewJavascriptBridge bridgeForWebView:self.webView webViewDelegate:self handler:nil];
    
    // openlink
    [self.bridge registerHandler:@"openLink" handler:^(id data, WVJBResponseCallback responseCallback) {
        NSString *link = (NSString *)data;
        if (link && link.length) {
            if (![DeepLink handleURLString:link withCompletion:nil]) {
                [[UIApplication sharedApplication].keyWindow presentFailureTips:@"无法打开的链接"];
            }
        }
    }];
    
    // 获取用户信息
    [self.bridge registerHandler:@"getProfile" handler:^(id data, WVJBResponseCallback responseCallback) {
        NSString *response = [[UserModel sharedInstance].user JSONStringRepresentation];
        responseCallback(response);
    }];
    
    // 获取用户token
    [self.bridge registerHandler:@"getToken" handler:^(id data, WVJBResponseCallback responseCallback) {
        NSString *response = [UserModel sharedInstance].token;
        responseCallback(response);
    }];
    
    // 登录
    [self.bridge registerHandler:@"doLogin" handler:^(id data, WVJBResponseCallback responseCallback) {
        [[Authorization sharedInstance] showAuth];
    }];
    
    // setTitle
    [self.bridge registerHandler:@"setTitle" handler:^(id data, WVJBResponseCallback responseCallback) {
        NSString *title = (NSString *)data;
        if (title.length > 0) {
            self.title = title;
        }
    }];
    
    // 判断页面是否在app中打开
    [self.bridge registerHandler:@"checkInApp" handler:^(id data, WVJBResponseCallback responseCallback) {
        responseCallback(@(YES));
    }];
    
    // 重新加载
    @weakify(self)
    [self.bridge registerHandler:@"onReloadPage" handler:^(id data, WVJBResponseCallback responseCallback) {
        @strongify(self)
        [self reloadView];
    }];
    
    // dismiss
    [self.bridge registerHandler:@"goBackNative" handler:^(id data, WVJBResponseCallback responseCallback) {
        @strongify(self)
        [self close];
    }];
}

- (void)close {
    int n = (int)[self.navigationController.childViewControllers count];
    if (n>1)
    {
        //push
        [self.navigationController popViewControllerAnimated:YES];
    }
    else
    {
        //present
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (void)loginSucceed {
    [self.webView reload];
}

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AuthorizationWasSucceedNotification object:nil];
}

@end
