//
//  BaseWebViewController.h
//  GeekMall
//
//  Created by liuyadi on 2017/9/12.
//  Copyright © 2017年 GeekZooStudio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <WebKit/WebKit.h>

@interface BaseWebViewController : UIViewController

- (void)loadURL:(NSString *)url;

@end
