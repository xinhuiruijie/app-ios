//
//  FileManager.m
//  marathon
//
//  Created by PURPLEPENG on 6/11/15.
//  Copyright (c) 2015 PURPLEPENG. All rights reserved.
//

#import "FileManager.h"
#import "SSZipArchive.h"

#pragma mark -

NSString * const DownloadPendingNotification = @"com.geek-zoo.DownloadPendingNotification";
NSString * const DownloadingNotification = @"com.geek-zoo.DownloadingNotification";
NSString * const DownloadSucceedNotification = @"com.geek-zoo.DownloadSucceedNotification";
NSString * const DownloadFailedNotification = @"com.geek-zoo.DownloadFailedNotification";

NSString * const UnpackageSucceedNotification = @"com.geek-zoo.UnpackageSucceedNotification";
NSString * const UnpackageFailedNotification = @"com.geek-zoo.UnpackageFailedNotification";

#pragma mark -

@interface FileManager ()

@property (nonatomic, strong) NSMutableDictionary * dicOperation;
@property (nonatomic, strong) NSMutableDictionary * userInfo;

@property (nonatomic, strong) NSTimer * timer;
@property (nonatomic, assign) BOOL didStart;
@property (nonatomic, assign) int64_t bytesWritten;
@property (nonatomic, assign) int64_t totalBytesWritten;
@property (nonatomic, assign) int64_t totalBytesExpectedToWrite;

@end

#pragma mark -

@implementation FileManager

@def_singleton( FileManager )

- (id)init
{
    if ( self = [super init] )
    {
        self.dicOperation = [[NSMutableDictionary alloc] init];
        
        [self addNotificationObservers];
    }
    
    return self;
}

- (void)dealloc
{
    [self removeNotificationObservers];
}

#pragma mark -

- (void)addNotificationObservers
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(onAPPTerminate) name:UIApplicationWillTerminateNotification object:nil];
}

- (void)removeNotificationObservers
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

#pragma mark - download manager

- (void)startDownloadWithURL:(NSString *)urlString
                    userInfo:(NSMutableDictionary *)userInfo
{
    NSString * card_id = [userInfo objectForKey:@"card_id"];
    
    [self reset];
    
    [self startTimer];

    self.userInfo = userInfo;
    
    NSURLSessionConfiguration * configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    AFURLSessionManager * manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:configuration];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:DownloadPendingNotification object:nil userInfo:userInfo];
    
    NSURLSessionDownloadTask * downloadTask;
    
    NSURL * (^destination)(NSURL *targetPath, NSURLResponse *response)  = ^ NSURL *(NSURL *targetPath, NSURLResponse *response) {
        
        NSURL *documentsDirectoryURL = [[NSFileManager defaultManager] URLForDirectory:NSDocumentDirectory inDomain:NSUserDomainMask appropriateForURL:nil create:NO error:nil];
        
        NSString * card_id = [userInfo objectForKey:@"card_id"];
        
        return [documentsDirectoryURL URLByAppendingPathComponent:[NSString stringWithFormat:@"%@%@",card_id,[response suggestedFilename]]];
    };
    
    void (^completionHandler)(NSURLResponse *response, NSURL *filePath, NSError *error) = ^ (NSURLResponse *response, NSURL *filePath, NSError *error) {
        
        [self stopTimer];
        
        if ( error )
        {
            userInfo[@"error"] = error;
            
            NSLog(@"File downloaded error: %@", error);
            
            if ( error.code != NSURLErrorCancelled )
            {
                [self clearTaskResumeDataCache:card_id];
            }
            
            [[NSNotificationCenter defaultCenter] postNotificationName:DownloadFailedNotification object:nil userInfo:userInfo];
        }
        else
        {
            NSLog(@"File downloaded to: %@", filePath);
        
            NSString * destinationPath = [filePath path];
            
            [userInfo setValue:destinationPath.lastPathComponent forKey:@"local_path"];
            
            [[NSNotificationCenter defaultCenter] postNotificationName:DownloadSucceedNotification object:nil userInfo:userInfo];
        }
        
        [self.dicOperation removeObjectForKey:card_id];
    };

    NSData * resumeData = [self loadTaskResumeDataCache:card_id];
    
    if ( resumeData )
    {
        downloadTask = [manager downloadTaskWithResumeData:resumeData
                                                  progress:nil
                                               destination:[destination copy]
                                         completionHandler:[completionHandler copy]];
    }
    else
    {
        NSURL * URL = [NSURL URLWithString:urlString];
        
        NSURLRequest * request = [NSURLRequest requestWithURL:URL];
        
        downloadTask = [manager downloadTaskWithRequest:request
                                               progress:nil
                                            destination:[destination copy]
                                      completionHandler:[completionHandler copy]];
    }
    
    [self.dicOperation setObject:downloadTask forKey:card_id];
    
    [manager setDownloadTaskDidWriteDataBlock:^(NSURLSession *session, NSURLSessionDownloadTask *downloadTask, int64_t bytesWritten, int64_t totalBytesWritten, int64_t totalBytesExpectedToWrite) {
        
//        NSLog(@"downloading %@-%@-%@",@(self.bytesWritten),@(self.totalBytesWritten),@(self.totalBytesExpectedToWrite));
        self.bytesWritten += bytesWritten;
        self.totalBytesWritten = totalBytesWritten;
        self.totalBytesExpectedToWrite = totalBytesExpectedToWrite;
        
        if ( !self.didStart ) {
            [self update];
            self.didStart = YES;
        }

    }];
    
    [downloadTask resume];
}

- (void)pauseDownloadWithUserInfo:(NSMutableDictionary *)userInfo
{
    NSString * card_id = [userInfo objectForKey:@"card_id"];
    if ( [self.dicOperation hasObjectForKey:card_id] )
    {
        [self saveDownloadTask:card_id];
    }
    
    [self stopTimer];
}

- (void)cancelDownloadWithUserInfo:(NSMutableDictionary *)userInfo
{
    NSString * card_id = [userInfo objectForKey:@"card_id"];
    if ( [self.dicOperation hasObjectForKey:card_id] )
    {
        [self clearDownloadTask:card_id];
        
        [self stopTimer];
    }
}

#pragma mark - DownloadTask

- (void)saveDownloadTask:(NSString *)card_id
{
    NSURLSessionDownloadTask * downloadTask = [self.dicOperation objectForKey:card_id];
    
    [downloadTask cancelByProducingResumeData:^(NSData * _Nullable resumeData) {
        [self saveTaskResumeDataCache:resumeData card_id:card_id];
        [self.dicOperation removeObjectForKey:card_id];
    }];
}

- (void)clearDownloadTask:(NSString *)card_id
{
    NSURLSessionDownloadTask * downloadTask = [self.dicOperation objectForKey:card_id];
    
    [downloadTask cancel];
    
    [self clearTaskResumeDataCache:card_id];
    
    [self.dicOperation removeObjectForKey:card_id];
}

#pragma mark - Resume Data Cache

- (NSData *)loadTaskResumeDataCache:(NSString *)card_id
{
    return [STICache.global objectForKey:[self taskResumeDataCacheKey:card_id]];
}

- (void)saveTaskResumeDataCache:(NSData *)resumeData
                        card_id:(NSString *)card_id
{
    [STICache.global setObject:resumeData forKey:[self taskResumeDataCacheKey:card_id]];
}

- (void)clearTaskResumeDataCache:(NSString *)card_id
{
    [STICache.global removeObjectForKey:[self taskResumeDataCacheKey:card_id]];
}

- (NSString *)taskResumeDataCacheKey:(NSString *)card_id
{
    return [NSString stringWithFormat:@"FileManager-Task-%@",card_id];
}

#pragma mark -

- (void)startTimer
{
    if ( !self.timer )
    {
        self.timer = [NSTimer scheduledTimerWithTimeInterval:1.f
                                             target:self
                                           selector:@selector(update)
                                           userInfo:nil
                                            repeats:YES];
    }
}

- (void)stopTimer
{
    __block BOOL shouldStop = YES;
    
    [self.dicOperation enumerateKeysAndObjectsUsingBlock:^(NSString * key, NSURLSessionDownloadTask * obj, BOOL * stop) {
        if ( obj.state == NSURLSessionTaskStateRunning ) {
            shouldStop = NO;
            *stop = YES;
        }
    }];
    
    if ( shouldStop )
    {
        [self.timer invalidate];
        self.timer = nil;
    }
}

- (void)update
{
    if ( self.totalBytesExpectedToWrite == 0 )
        return;
    
    float progress = ((self.totalBytesWritten * 1.0) / self.totalBytesExpectedToWrite);

    [self.userInfo setValue:@(self.bytesWritten) forKey:@"bytesWritten"];
    [self.userInfo setValue:@(self.totalBytesWritten) forKey:@"totalBytesWritten"];
    [self.userInfo setValue:@(self.totalBytesExpectedToWrite) forKey:@"totalBytesExpectedToWrite"];
    [self.userInfo setValue:@(progress) forKey:@"progress"];
    
    [[NSNotificationCenter defaultCenter] postNotificationName:DownloadingNotification object:nil userInfo:self.userInfo];

//    NSLog(@"updating %@-%@-%@",@(self.bytesWritten),@(self.totalBytesWritten),@(self.totalBytesExpectedToWrite));
    self.bytesWritten = 0;
}

- (void)reset
{
    self.bytesWritten = 0;
    self.totalBytesWritten = 0;
    self.totalBytesExpectedToWrite = 0;
}

// 异常退出APP（kill APP）时，需要保存TaskResumeData
- (void)save
{
    [self.dicOperation enumerateKeysAndObjectsUsingBlock:^(NSString * key, NSURLSessionDownloadTask * obj, BOOL * stop)
     {
        if ( obj.state == NSURLSessionTaskStateRunning )
        {
            [self saveDownloadTask:key];
        }
    }];
}

#pragma mark -

- (void)onAPPTerminate
{
    [self save];
}


#pragma mark - ZipArchive manager

- (void)unpackageFromURL:(NSURL *)destinationURL
{
    [self unpackageFromURL:destinationURL
                  userInfo:nil];
}

- (void)unpackageFromURL:(NSURL *)destinationURL
                userInfo:(NSMutableDictionary *)userInfo
{
    UIApplication * application = [UIApplication sharedApplication];
    NSString *destinationPath = [[destinationURL path] stringByDeletingPathExtension];
    
    // GCD 多线程
    __block UIBackgroundTaskIdentifier backgroundTask = [application beginBackgroundTaskWithExpirationHandler:^{
        [application endBackgroundTask:backgroundTask];
        backgroundTask = UIBackgroundTaskInvalid;
    }];
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        // 解压文件中;目标文件夹:destinationPath
        BOOL unzipSuccessful = NO;
        unzipSuccessful = [SSZipArchive unzipFileAtPath:[destinationURL path] toDestination:destinationPath];
        if (!unzipSuccessful)
        {
            // 解压文件出现错误;请确定文件是合法的 zip 文件, 下载临时存放地:[destinationURL path]
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [[NSNotificationCenter defaultCenter] postNotificationName:UnpackageFailedNotification object:nil userInfo:userInfo];
            });
        }
        
        // 解压文件成功, 并成功解压到目标文件夹, 清除临时下载文件:[destinationURL path]
        NSFileManager * fileMgr = [NSFileManager defaultManager];
        NSError * error;
        if ([fileMgr removeItemAtPath:[destinationURL path] error:&error] != YES){
        }
        
        if (unzipSuccessful)
        {
            // 通知主线程的 UI 变更.
            dispatch_async(dispatch_get_main_queue(), ^(void) {
                [userInfo setValue:destinationPath.lastPathComponent forKey:@"local_path"];
                [[NSNotificationCenter defaultCenter] postNotificationName:UnpackageSucceedNotification object:nil userInfo:userInfo];
            });
        }
        
        [application endBackgroundTask:backgroundTask];
        backgroundTask = UIBackgroundTaskInvalid;
    });
}

@end
