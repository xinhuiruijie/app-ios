//
//  FileManager.h
//  marathon
//
//  Created by PURPLEPENG on 6/11/15.
//  Copyright (c) 2015 PURPLEPENG. All rights reserved.
//

#import <Foundation/Foundation.h>

#pragma mark -

FOUNDATION_EXPORT NSString * const DownloadPendingNotification;
FOUNDATION_EXPORT NSString * const DownloadingNotification;
FOUNDATION_EXPORT NSString * const DownloadSucceedNotification;
FOUNDATION_EXPORT NSString * const DownloadFailedNotification;

FOUNDATION_EXPORT NSString * const UnpackageSucceedNotification;
FOUNDATION_EXPORT NSString * const UnpackageFailedNotification;

#pragma mark -

@interface FileManager : NSObject

@singleton( FileManager )

#pragma mark - download manager

- (void)startDownloadWithURL:(NSString *)urlString
                    userInfo:(NSMutableDictionary *)userInfo;
- (void)pauseDownloadWithUserInfo:(NSMutableDictionary *)userInfo;
- (void)cancelDownloadWithUserInfo:(NSMutableDictionary *)userInfo;

#pragma mark - ZipArchive manager
- (void)unpackageFromURL:(NSURL *)destinationURL;
- (void)unpackageFromURL:(NSURL *)destinationURL
                userInfo:(NSMutableDictionary *)userInfo;

@end
