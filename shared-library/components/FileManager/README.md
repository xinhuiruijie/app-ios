###概述
FileManager主要实现`文件下载`、`文件解压`功能。

* 文件下载
	* 使用[AFNetworking](https://github.com/AFNetworking/AFNetworking)	
* 文件解压
	* 使用[SSZipArchive](https://github.com/ZipArchive/ZipArchive) 
		* 添加libz library到target