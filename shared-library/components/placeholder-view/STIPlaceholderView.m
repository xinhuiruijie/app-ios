//
//  STIPlaceholderView.m
//  gaibianjia
//
//  Created by QFish on 6/23/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import "STIPlaceholderView.h"
#import "STIPlaceholder.h"

@implementation STIPlaceholderView

- (void)awakeFromNib
{
    [super awakeFromNib];
    
	UIView * view = [STIPlaceholder sharedInstance].view;
	[self addSubview:view];
	self.customView = view;
}

- (void)layoutSubviews
{
	[super layoutSubviews];

	self.customView.frame = self.frame;
}

@end
