//
//  STIPlaceholder.m
//  gaibianjia
//
//  Created by QFish on 6/23/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import "STIPlaceholder.h"

NSString * const STIPlaceholderCellIdenifier = @"STIPlaceholderCellIdenifier";

#pragma mark -

@interface STIPlaceholder()
@property (nonatomic, strong) NSMutableDictionary * nibs;
@property (nonatomic, strong) NSMutableDictionary * classes;
@end

@implementation STIPlaceholder

@def_singleton(STIPlaceholder)

- (instancetype)init
{
    self = [super init];
    if (self) {
        _nibs = [NSMutableDictionary dictionary];
        _classes = [NSMutableDictionary dictionary];
    }
    return self;
}

- (UIView *)view
{
	if ( self.customViewNib )
	{
		return [[self.customViewNib instantiateWithOwner:nil options:nil] firstObject];
	}
	else if ( self.customViewClass )
	{
		return [self.customViewClass new];
	}
	
	return nil;
}

+ (void)addNib:(UINib *)nib forKey:(NSString *)key
{
    
}

+ (void)addClass:(UINib *)nib forKey:(NSString *)key
{
    
}

+ (void)registerTo:(UIScrollView *)list
{
	if ( [list isKindOfClass:UITableView.class] ) {
		[self registerToTableView:(UITableView *)list];
	} else if ( [list isKindOfClass:UICollectionView.class] ) {
		[self registerToCollectionView:(UICollectionView *)list];
	}
}

+ (void)registerToTableView:(UITableView *)tableView
{
	[tableView registerClass:STIPlaceholderTableViewCell.class forCellReuseIdentifier:STIPlaceholderCellIdenifier];
}

+ (void)registerToCollectionView:(UICollectionView *)collectionView
{
	[collectionView registerClass:STIPlaceholderCollectionViewCell.class forCellWithReuseIdentifier:STIPlaceholderCellIdenifier];
}

+ (id)cellForList:(id)list atIndexPath:(NSIndexPath *)indexPath
{
	if ( [list isKindOfClass:UITableView.class] ) {
		return [list dequeueReusableCellWithIdentifier:STIPlaceholderCellIdenifier forIndexPath:indexPath];
	} else if ( [list isKindOfClass:UICollectionView.class] ) {
		return [list dequeueReusableCellWithReuseIdentifier:STIPlaceholderCellIdenifier forIndexPath:indexPath];
	}
	return nil;
}

@end
