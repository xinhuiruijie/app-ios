//
//  STIPlaceholderTableViewCell.m
//  gaibianjia
//
//  Created by QFish on 6/23/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import "STIPlaceholderTableViewCell.h"
#import "STIPlaceholder.h"

@implementation STIPlaceholderTableViewCell

- (void)willMoveToSuperview:(UIView *)newSuperview
{
	[super willMoveToSuperview:newSuperview];
	
	self.backgroundColor = [UIColor clearColor];
	self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (UIView *)customView
{
	if ( _customView == nil ) {
		_customView = [STIPlaceholder sharedInstance].view;
		[self.contentView addSubview:_customView];
	}
	return _customView;
}

- (void)layoutSubviews
{
	[super layoutSubviews];
	
	self.customView.frame = self.contentView.frame;
}

@end
