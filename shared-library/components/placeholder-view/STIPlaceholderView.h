//
//  STIPlaceholderView.h
//  gaibianjia
//
//  Created by QFish on 6/23/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface STIPlaceholderView : UIView
@property (nonatomic, strong) UIView * customView;
@end
