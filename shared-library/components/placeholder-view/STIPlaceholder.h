//
//  STIPlaceholder.h
//  gaibianjia
//
//  Created by QFish on 6/23/15.
//  Copyright (c) 2015 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "STIPlaceholderView.h"
#import "STIPlaceholderTableViewCell.h"
#import "STIPlaceholderCollectionViewCell.h"

/**
 *
 
 ### 配置
 1. 创建一个 Placeholder view，可以用nib，也可以纯代码
 2. 找个位置配置一下 `customViewNib` or `customViewClass`, nib的优先级比class高
 
 ### 使用
 3. 如果是`TableView` or `CollectionView`, 先注册cell
 4. 然后在 deque cell 的 reuse identifier 是 `STIPlaceholderCellIdenifier`
 
 *
 */

FOUNDATION_EXPORT NSString * const STIPlaceholderCellIdenifier;

@interface STIPlaceholder : NSObject

@singleton(STIPlaceholder)

//  设置自定义view，如果设置，image将会被覆盖
@property (nonatomic, strong) UINib * customViewNib;
@property (nonatomic, strong) Class customViewClass;

+ (void)addNib:(UINib *)nib forKey:(NSString *)key;
+ (void)addClass:(UINib *)nib forKey:(NSString *)key;

// Generated a new view
- (UIView *)view;

// 向 list 注册 cell
+ (void)registerTo:(UIScrollView *)list;

+ (id)cellForList:(id)list atIndexPath:(NSIndexPath *)indexPath;

@end
