//
//  DateAnimatedView.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "DateAnimatedView.h"
#import "PPCounter.h"

@interface DateAnimatedView ()

@property (nonatomic, strong) UILabel *dayLabel;
@property (nonatomic, strong) UILabel *monthLabel;

@property (nonatomic, assign) NSInteger day;

@end

@implementation DateAnimatedView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        UILabel *dayLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 26, 20)];
        dayLabel.font = [UIFont fontWithName:@"Gotham-Medium" size:18];
        dayLabel.textAlignment = NSTextAlignmentRight;
        self.dayLabel = dayLabel;
        [self addSubview:dayLabel];
        
        UILabel *monthLabel = [[UILabel alloc] initWithFrame:CGRectMake(dayLabel.width, 5, 24, 15)];
        monthLabel.font = [UIFont fontWithName:@"Gotham-Book" size:10];
        monthLabel.textAlignment = NSTextAlignmentLeft;
        self.monthLabel = monthLabel;
        [self addSubview:monthLabel];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.size = CGSizeMake(60, 20);
}

- (void)startAnimation {
    NSArray *monthArray = @[@"", @"JAN", @"FEB", @"MAR", @"APR", @"MAY", @"JUN", @"JUL", @"AUG", @"SEP", @"QCT", @"NOV", @"DEC"];
    NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth fromDate:[NSDate date]];
    self.day = components.day;
    self.monthLabel.text = monthArray[components.month];
    
    CGFloat duration = 1.0;
    if (self.day < 5) {
        duration = 0.2;
    } else if (self.day < 10) {
        duration = 0.4;
    } else if (self.day < 20) {
        duration = 0.8;
    } else {
        duration = 1.0;
    }
    
    [self.dayLabel pp_fromNumber:1 toNumber:self.day duration:duration animationOptions:PPCounterAnimationOptionCurveLinear format:^NSString *(CGFloat number) {
        // 此处自由拼接内容
        NSInteger day = (NSInteger)floor(number);
        return [NSString stringWithFormat:@"%02ld", day];
    } completion:nil];
}

@end
