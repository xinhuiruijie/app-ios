//
//  ZLPlaceholderTextView.m
//  HemeiUnion
//
//  Created by GeekZooStudio on 2017/8/31.
//  Copyright © 2017年 GeekZooStudio. All rights reserved.
//

#import "ZLPlaceholderTextView.h"

@implementation ZLPlaceholderTextView 

- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self setupUI];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupUI];
    }
    return self;
}

+ (instancetype )placeholderTextView {
    return [[self alloc]init];
}

- (void)setupUI{
    [self addSubview:self.placeholderTextView];
    [self addSubview:self.textView];
}

#pragma mark - --- delegate 视图委托 ---

/**
 * 这个方法专门用来布局子控件，一般在这里设置子控件的frame
 * 当控件本身的尺寸发生改变的时候，系统会自动调用这个方法
 *
 */
- (void)layoutSubviews{
    [super layoutSubviews];
    
    CGFloat textViewW = self.frame.size.width - 30;
    CGFloat textViewH = self.frame.size.height;
    self.textView.frame = CGRectMake(15, 0, textViewW, textViewH );
    self.placeholderTextView.frame  = CGRectMake(15, 0, textViewW, textViewH );
    
}

#pragma mark - 

- (BOOL)textViewShouldBeginEditing:(UITextView *)textView {
    if (self.delegate && [self.delegate respondsToSelector:@selector(textViewShouldBeginEditing:)]) {
        return [self.delegate textViewShouldBeginEditing:textView];
    }
    return YES;
}

- (BOOL)textViewShouldEndEditing:(UITextView *)textView {
    if (self.delegate && [self.delegate respondsToSelector:@selector(textViewShouldEndEditing:)]) {
        return [self.delegate textViewShouldEndEditing:textView];
    }
    return YES;
}

- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if (self.delegate && [self.delegate respondsToSelector:@selector(textView:shouldChangeTextInRange:replacementText:)]) {
        return [self.delegate textView:textView shouldChangeTextInRange:range replacementText:text];
    }
    
    if ([text isEqualToString:@"\n"]) {
        [textView resignFirstResponder];
        return NO;
    }
    
    if (text.length == 0 && range.location == 0) {
        self.placeholderTextView.hidden = NO;
    } else {
        self.placeholderTextView.hidden =YES;
    }
    return YES;
}

/*由于联想输入的时候，函数textView:shouldChangeTextInRange:replacementText:无法判断字数，
 因此使用textViewDidChange对TextView里面的字数进行判断
 */
- (void)textViewDidChange:(UITextView *)textView
{
    if (textView.text.length == 0) {
        self.placeholderTextView.hidden = NO;
    }else{
        self.placeholderTextView.hidden =YES;
    }
    
    //该判断用于联想输入
    if (self.maxLenght > 0 && textView.text.length > self.maxLenght)
    {
        textView.text = [textView.text substringToIndex:self.maxLenght];
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(textViewDidChange:)]) {
        [self.delegate textViewDidChange:textView];
    }
}

#pragma mark - --- event response 事件相应 ---

#pragma mark - --- private methods 私有方法 ---

#pragma mark - --- setters 属性 ---

- (void)setFont:(UIFont *)font {
    _font = font;
    self.textView.font = font;
    self.placeholderTextView.font = font;
}

- (void)setText:(NSString *)text {
    self.textView.text = text;
    if (text.length == 0) {
        self.placeholderTextView.hidden = NO;
    }else{
        self.placeholderTextView.hidden =YES;
    }
}

- (NSString *)text {
    return self.textView.text;
}

- (void)setPlaceholder:(NSString *)placeholder {
    _placeholder = placeholder;
    self.placeholderTextView.text = placeholder;
}

#pragma mark - --- getters 属性 —--
- (UITextView *)textView{
    if (!_textView) {
        _textView = [[UITextView alloc] init];
        _textView.backgroundColor = [UIColor clearColor];
        _textView.font = [UIFont systemFontOfSize:13];
        _textView.delegate = self;
    }
    
    return _textView;
}

- (UITextView *)placeholderTextView{
    if (!_placeholderTextView) {
        _placeholderTextView = [[UITextView alloc] init];
        _placeholderTextView.font = [UIFont systemFontOfSize:13];
        _placeholderTextView.backgroundColor = [UIColor clearColor];
        _placeholderTextView.textColor = [AppTheme subTextColor];
    }
    return _placeholderTextView;
}

@end
