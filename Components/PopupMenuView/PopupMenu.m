//
//  PopupMenu.m
//  TIMChat
//
//  Created by AlexiChen on 16/2/29.
//  Copyright © 2016年 AlexiChen. All rights reserved.
//


#import "PopupMenu.h"
#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
@interface PopupMenuItemView : UITableViewCell
@property (nonatomic, strong) MenuItem *item;
@property (nonatomic, assign) CGSize iconSize;
@property (nonatomic, assign) BOOL hideBottomLine;
@end

@interface PopupMenuView : UIView
- (void)dismissMenu:(BOOL) animated;
@end

@interface PopupMenuOverlay : UIView
@end

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

@implementation PopupMenuItemView
{
    UIImageView *_iconImgView;
    UILabel     *_titleLabel;
    UIView      *_bottomLine;
}

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        _iconSize = (CGSize){25,25};
        
        _iconImgView = [[UIImageView alloc] init];
        _iconImgView.contentMode = UIViewContentModeScaleAspectFit;
        [self.contentView addSubview:_iconImgView];
        
        _titleLabel = [[UILabel alloc] init];
        [self.contentView addSubview:_titleLabel];
        
        _bottomLine = [[UIView alloc] init];
        _bottomLine.backgroundColor = [AppTheme searchLineColor];
        [self.contentView addSubview:_bottomLine];
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGFloat margin = 10.0;
    CGFloat marginForMiddle = 5.0;
    _iconImgView.frame = self.item.icon ? CGRectMake(margin, (self.height - self.iconSize.height) / 2.0, self.iconSize.width, self.iconSize.height) : CGRectZero;
    CGFloat titleX = _iconImgView.x + _iconImgView.width + marginForMiddle;
    _titleLabel.frame = CGRectMake(titleX, 0, self.width - titleX - margin, self.height);
    _bottomLine.frame = CGRectMake(0, self.height - [AppTheme onePixel], self.width, [AppTheme onePixel]);
}

- (void)setItem:(MenuItem *)item {
    _item = item;
    _iconImgView.image = item.icon;
    _titleLabel.text = item.title;
    if ([item isKindOfClass:[PopupMenuItem class]]) {
        _titleLabel.font = ((PopupMenuItem*)item).titleFont;
        _titleLabel.textColor = ((PopupMenuItem*)item).titleColor;
    }
}

- (void)setHideBottomLine:(BOOL)hideBottomLine {
    _hideBottomLine = hideBottomLine;
    _bottomLine.hidden = hideBottomLine;
}

@end

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

@implementation PopupMenuOverlay

const CGFloat kArrowSize = 0.f;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.opaque = NO;
    }
    return self;
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    UIView *touched = [[touches anyObject] view];
    if (touched == self)
    {
        for (UIView *v in self.subviews)
        {
            if ([v isKindOfClass:[PopupMenuView class]])
            {
                PopupMenuView *popv = (PopupMenuView *)v;
                [popv performSelector:@selector(dismissMenu:) withObject:@(YES)];
            }
        }
    }
}

@end

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

typedef enum {
    
    PopupMenuViewArrowDirectionNone,
    PopupMenuViewArrowDirectionUp,
    PopupMenuViewArrowDirectionDown,
    PopupMenuViewArrowDirectionLeft,
    PopupMenuViewArrowDirectionRight,
    
} PopupMenuViewArrowDirection;

@implementation PopupMenuView
{
    PopupMenuViewArrowDirection     _arrowDirection;
    CGFloat                         _arrowPosition;
    UIView                          *_contentView;
    NSArray                         *_menuItems;
}

- (id)init {
    self = [super initWithFrame:CGRectZero];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        self.opaque = YES;
        self.alpha = 0;
        
        self.layer.shadowColor = [UIColor lightGrayColor].CGColor;
        self.layer.shadowOpacity = 0.2;
        self.layer.shadowOffset = CGSizeMake(0, 0);
        self.layer.shadowRadius = 8;
    }
    
    return self;
}

// - (void) dealloc { NSLog(@"dealloc %@", self); }

- (void)setupFrameInView:(UIView *)view fromRect:(CGRect)fromRect
{
    const CGSize contentSize = _contentView.frame.size;
    
    const CGFloat outerWidth = view.bounds.size.width;
    const CGFloat outerHeight = view.bounds.size.height;
    
    const CGFloat rectX0 = fromRect.origin.x;
    const CGFloat rectX1 = fromRect.origin.x + fromRect.size.width;
    const CGFloat rectXM = fromRect.origin.x + fromRect.size.width * 0.5f;
    const CGFloat rectY0 = fromRect.origin.y;
    const CGFloat rectY1 = fromRect.origin.y + fromRect.size.height;
    const CGFloat rectYM = fromRect.origin.y + fromRect.size.height * 0.5f;;
    
    const CGFloat widthPlusArrow = contentSize.width + kArrowSize;
    const CGFloat heightPlusArrow = contentSize.height + kArrowSize;
    const CGFloat widthHalf = contentSize.width * 0.5f;
    const CGFloat heightHalf = contentSize.height * 0.5f;
    
    const CGFloat kMargin = 5.f;
    
    if (widthPlusArrow < rectX0) {
        _arrowDirection = PopupMenuViewArrowDirectionRight;
        
        CGPoint point = (CGPoint){
            rectX0 - widthPlusArrow,
            rectY0
        };
        
        _arrowPosition = rectYM - point.y;
        _contentView.frame = (CGRect){CGPointZero, contentSize};
        
        self.frame = (CGRect) {
            point,
            contentSize.width + kArrowSize,
            contentSize.height
        };
    }
    else if (heightPlusArrow < (outerHeight - rectY1))
    {
        _arrowDirection = PopupMenuViewArrowDirectionUp;
        CGPoint point = (CGPoint) { rectXM - widthHalf, rectY1 };
        
        if (point.x < kMargin)
        {
            point.x = kMargin;
        }
        
        if ((point.x + contentSize.width + kMargin) > outerWidth)
        {
            point.x = outerWidth - contentSize.width - kMargin;
        }
        
        _arrowPosition = rectXM - point.x;
        //_arrowPosition = MAX(16, MIN(_arrowPosition, contentSize.width - 16));
        _contentView.frame = (CGRect){0, kArrowSize, contentSize};
        
        self.frame = (CGRect) { point, contentSize.width, contentSize.height + kArrowSize };
    }
    else if (heightPlusArrow < rectY0)
    {
        _arrowDirection = PopupMenuViewArrowDirectionDown;
        CGPoint point = (CGPoint){ rectXM - widthHalf, rectY0 - heightPlusArrow };
        
        if (point.x < kMargin)
            point.x = kMargin;
        
        if ((point.x + contentSize.width + kMargin) > outerWidth)
            point.x = outerWidth - contentSize.width - kMargin;
        
        _arrowPosition = rectXM - point.x;
        _contentView.frame = (CGRect){CGPointZero, contentSize};
        
        self.frame = (CGRect) {
            point,
            contentSize.width,
            contentSize.height + kArrowSize
        };
        
    }
    else if (widthPlusArrow < (outerWidth - rectX1))
    {
        _arrowDirection = PopupMenuViewArrowDirectionLeft;
        CGPoint point = (CGPoint){
            rectX1,
            rectYM - heightHalf
        };
        
        if (point.y < kMargin)
            point.y = kMargin;
        
        if ((point.y + contentSize.height + kMargin) > outerHeight)
            point.y = outerHeight - contentSize.height - kMargin;
        
        _arrowPosition = rectYM - point.y;
        _contentView.frame = (CGRect){kArrowSize, 0, contentSize};
        
        self.frame = (CGRect) {
            point,
            contentSize.width + kArrowSize,
            contentSize.height
        };
        
    } else {
        _arrowDirection = PopupMenuViewArrowDirectionNone;
        
        self.frame = (CGRect) {
            (outerWidth - contentSize.width)   * 0.5f,
            (outerHeight - contentSize.height) * 0.5f,
            contentSize,
        };
    }
}

- (void)showMenuInView:(UIView *)view fromRect:(CGRect)rect menuItems:(NSArray *)menuItems
{
    _menuItems = menuItems;
    
    _contentView = [self mkContentView];
    [self addSubview:_contentView];
    
    [self setupFrameInView:view fromRect:rect];
    
    UIImageView *imageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"bg_fm_more_bubble"]];
    imageView.frame = _contentView.frame;
    imageView.width += 6;
    [self insertSubview:imageView belowSubview:_contentView];
    
    PopupMenuOverlay *overlay = [[PopupMenuOverlay alloc] initWithFrame:view.bounds];
    [overlay addSubview:self];
    [view addSubview:overlay];
    
    _contentView.hidden = YES;
    const CGRect toFrame = self.frame;
    self.frame = (CGRect){rect.origin.x, rect.origin.y + rect.size.height * 0.5, 1, 1};
    imageView.hidden = YES;
    
    [UIView animateWithDuration:0.1 animations:^{
        self.alpha = 1.0f;
        self.frame = toFrame;
    } completion:^(BOOL completed) {
        _contentView.hidden = NO;
        imageView.hidden = NO;
    }];
    
}

- (void)dismissMenu:(BOOL) animated
{
    if (self.superview) {
        
//        if (animated) {
//
//            _contentView.hidden = YES;
//            const CGRect toFrame = (CGRect){self.arrowPoint, 1, 1};
//
//            [UIView animateWithDuration:0.2 animations:^(void) {
//
//                self.alpha = 0;
//                self.frame = toFrame;
//
//            } completion:^(BOOL finished) {
//
//                if ([self.superview isKindOfClass:[PopupMenuOverlay class]])
//                    [self.superview removeFromSuperview];
//                [self removeFromSuperview];
//            }];
//
//        } else
//        {
        
            if ([self.superview isKindOfClass:[PopupMenuOverlay class]])
                [self.superview removeFromSuperview];
            [self removeFromSuperview];
//        }
    }
}

- (void)performAction:(id)sender
{
    [self dismissMenu:YES];
    
    UIButton *button = (UIButton *)sender;
    PopupMenuItem *menuItem = _menuItems[button.tag];
    [menuItem menuAction];
}

- (UIView *) mkContentView
{
    for (UIView *v in self.subviews) {
        [v removeFromSuperview];
    }
    
    if (!_menuItems.count) {
        return nil;
    }
    
    const CGFloat kMinMenuItemHeight = 32.f;
    const CGFloat kMinMenuItemWidth = 32.f;
    const CGFloat kMarginMiddle = 5.f;
    const CGFloat kMarginX = 10.f;
    const CGFloat kMarginY = 10.f;
    
    CGFloat maxImageWidth = 0;
    CGFloat maxItemHeight = 0;
    CGFloat maxItemWidth = 0;
    
    // 找出最大icon size
    for (PopupMenuItem *menuItem in _menuItems) {
        const CGSize imageSize = menuItem.icon.size;
        if (imageSize.width > maxImageWidth) {
            maxImageWidth = imageSize.width;
        }
    }
    maxImageWidth = 22;
    
    const CGSize imageSize = CGSizeMake(22, 22);
    for (PopupMenuItem *menuItem in _menuItems) {
        const CGSize titleSize = [menuItem.title boundingRectWithSize:CGSizeMake(kScreenWidth, 60) options:NSStringDrawingUsesLineFragmentOrigin | NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName: menuItem.titleFont} context:nil].size;
        
        const CGFloat itemHeight = MAX(titleSize.height, imageSize.height) + kMarginY * 2;
        const CGFloat itemWidth = (menuItem.icon ? maxImageWidth + kMarginMiddle : 0) + titleSize.width + kMarginX * 2;
        
        if (itemHeight > maxItemHeight)
            maxItemHeight = itemHeight;
        
        if (itemWidth > maxItemWidth)
            maxItemWidth = itemWidth;
    }
    
    maxItemWidth  = MAX(maxItemWidth, kMinMenuItemWidth);
    maxItemHeight = MAX(maxItemHeight, kMinMenuItemHeight);
    
    UIImage *selectedImage = [PopupMenuView selectedImage:(CGSize){maxItemWidth, maxItemHeight + 2}];
    
    CGFloat itemY = 0;
    UIView *contentView = [[UIView alloc] initWithFrame:CGRectZero];
    contentView.frame = (CGRect){0, 0, maxItemWidth, itemY + maxItemHeight * _menuItems.count};
    contentView.layer.cornerRadius = 3;
    contentView.layer.masksToBounds = YES;
    contentView.autoresizingMask = UIViewAutoresizingNone;
    contentView.backgroundColor = [UIColor whiteColor];
    contentView.opaque = NO;
    
    for (int i = 0; i < _menuItems.count; i++) {
        PopupMenuItem *menuItem = _menuItems[i];
        
        PopupMenuItemView *itemView = [[PopupMenuItemView alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@""];
        itemView.frame = CGRectMake(0, i*maxItemHeight, maxItemWidth, maxItemHeight);
        itemView.item = menuItem;
        itemView.iconSize = imageSize;
        itemView.hideBottomLine = i == _menuItems.count-1;
        [contentView addSubview:itemView];
        
        if (menuItem.enabled) {
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.tag = i;
            button.frame = itemView.bounds;
            button.enabled = menuItem.enabled;
            button.backgroundColor = [UIColor clearColor];
            button.opaque = NO;
            button.autoresizingMask = UIViewAutoresizingNone;
            
            [button addTarget:self
                       action:@selector(performAction:)
             forControlEvents:UIControlEventTouchUpInside];
            
            [button setBackgroundImage:selectedImage forState:UIControlStateHighlighted];
            
            [itemView addSubview:button];
        }
    }
    
    /*
    for (PopupMenuItem *menuItem in _menuItems) {
        
        const CGRect itemFrame = (CGRect){0, itemY, maxItemWidth, maxItemHeight};
        
        UIView *itemView = [[UIView alloc] initWithFrame:itemFrame];
        itemView.autoresizingMask = UIViewAutoresizingNone;
        itemView.backgroundColor = [UIColor clearColor];
        itemView.opaque = NO;
        
        [contentView addSubview:itemView];
        
        if (menuItem.enabled) {
            
            UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
            button.tag = itemNum;
            button.frame = itemView.bounds;
            button.enabled = menuItem.enabled;
            button.backgroundColor = [UIColor clearColor];
            button.opaque = NO;
            button.autoresizingMask = UIViewAutoresizingNone;
            
            [button addTarget:self
                       action:@selector(performAction:)
             forControlEvents:UIControlEventTouchUpInside];
            
            [button setBackgroundImage:selectedImage forState:UIControlStateHighlighted];
            
            [itemView addSubview:button];
        }
        
        if (menuItem.title.length) {
            
            CGRect titleFrame;
            
            if (!menuItem.enabled && !menuItem.icon) {
                
                titleFrame = (CGRect){
                    kMarginX,
                    kMarginY,
                    maxItemWidth - kMarginX * 4,
                    maxItemHeight - kMarginY * 2
                };
                
            } else {
                
                titleFrame = (CGRect){
                    titleX,
                    kMarginY,
                    titleWidth,
                    maxItemHeight - kMarginY * 2
                };
            }
            
            UILabel *titleLabel = [[UILabel alloc] initWithFrame:titleFrame];
            titleLabel.text = menuItem.title;
            titleLabel.font = menuItem.titleFont;
            titleLabel.textAlignment = menuItem.alignment;
            titleLabel.textColor = menuItem.foreColor ? menuItem.foreColor : [UIColor whiteColor];
            titleLabel.backgroundColor = [UIColor clearColor];
            titleLabel.autoresizingMask = UIViewAutoresizingNone;
            [itemView addSubview:titleLabel];
        }
        
        if (menuItem.icon) {
            
            const CGRect imageFrame = {kMarginX * 2, kMarginY, maxImageWidth, maxItemHeight - kMarginY * 2};
            UIImageView *imageView = [[UIImageView alloc] initWithFrame:imageFrame];
            imageView.image = menuItem.icon;
            imageView.clipsToBounds = YES;
            imageView.contentMode = UIViewContentModeCenter;
            imageView.autoresizingMask = UIViewAutoresizingNone;
            [itemView addSubview:imageView];
        }
        
        if (itemNum < _menuItems.count - 1) {
            
            UIImageView *gradientView = [[UIImageView alloc] initWithImage:gradientLine];
            gradientView.frame = (CGRect){kMarginX * 2, maxItemHeight + 1, gradientLine.size};
            gradientView.contentMode = UIViewContentModeLeft;
            [itemView addSubview:gradientView];
            
            itemY += 2;
        }
        
        itemY += maxItemHeight;
        ++itemNum;
    }
    */
    return contentView;
}

- (CGPoint) arrowPoint
{
    CGPoint point;
    
    if (_arrowDirection == PopupMenuViewArrowDirectionUp) {
        
        point = (CGPoint){ CGRectGetMinX(self.frame) + _arrowPosition, CGRectGetMinY(self.frame) };
        
    } else if (_arrowDirection == PopupMenuViewArrowDirectionDown) {
        
        point = (CGPoint){ CGRectGetMinX(self.frame) + _arrowPosition, CGRectGetMaxY(self.frame) };
        
    } else if (_arrowDirection == PopupMenuViewArrowDirectionLeft) {
        
        point = (CGPoint){ CGRectGetMinX(self.frame), CGRectGetMinY(self.frame) + _arrowPosition  };
        
    } else if (_arrowDirection == PopupMenuViewArrowDirectionRight) {
        
        point = (CGPoint){ CGRectGetMaxX(self.frame), CGRectGetMinY(self.frame) + _arrowPosition  };
        
    } else {
        
        point = self.center;
    }
    
    return point;
}

+ (UIImage *) selectedImage: (CGSize) size
{
    //    const CGFloat locations[] = {0,1};
    //    const CGFloat components[] = {
    //        0.216, 0.471, 0.871, 1,
    //        0.059, 0.353, 0.839, 1,
    //    };
    //
    //    return [self gradientImageWithSize:size locations:locations components:components count:2];
    return [UIImage imageWithColor:[UIColor colorWithWhite:0.0 alpha:0.2] andSize:size];
}

+ (UIImage *)gradientLine:(CGSize)size
{
    //    const CGFloat locations[5] = {0,0.2,0.5,0.8,1};
    //
    //    const CGFloat R = 0.44f, G = 0.44f, B = 0.44f;
    //
    //    const CGFloat components[20] = {
    //        R,G,B,0.1,
    //        R,G,B,0.4,
    //        R,G,B,0.7,
    //        R,G,B,0.4,
    //        R,G,B,0.1
    //    };
    //
    //    return [self gradientImageWithSize:size locations:locations components:components count:5];
    return [UIImage imageWithColor:[AppTheme lineColor] andSize:size];
}

+ (UIImage *) gradientImageWithSize:(CGSize)size locations:(const CGFloat [])locations components:(const CGFloat []) components count:(NSUInteger)count
{
    UIGraphicsBeginImageContextWithOptions(size, NO, 0);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGGradientRef colorGradient = CGGradientCreateWithColorComponents(colorSpace, components, locations, 2);
    CGColorSpaceRelease(colorSpace);
    CGContextDrawLinearGradient(context, colorGradient, (CGPoint){0, 0}, (CGPoint){size.width, 0}, 0);
    CGGradientRelease(colorGradient);
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (void)drawRect:(CGRect)rect
{
    [self drawBackground:self.bounds inContext:UIGraphicsGetCurrentContext()];
}

- (void)drawBackground:(CGRect)frame inContext:(CGContextRef) context
{
    CGFloat R0 = 0.267, G0 = 0.303, B0 = 0.335;
    CGFloat R1 = 0.040, G1 = 0.040, B1 = 0.040;
    
    UIColor *tintColor = [PopupMenu tintColor];
    if (tintColor)
    {
        CGFloat a;
        [tintColor getRed:&R0 green:&G0 blue:&B0 alpha:&a];
    }
    
    CGFloat X0 = frame.origin.x;
    CGFloat X1 = frame.origin.x + frame.size.width;
    CGFloat Y0 = frame.origin.y;
    CGFloat Y1 = frame.origin.y + frame.size.height;
    
    // render arrow
    
    UIBezierPath *arrowPath = [UIBezierPath bezierPath];
    
    // fix the issue with gap of arrow's base if on the edge
    const CGFloat kEmbedFix = 3.f;
    
    if (_arrowDirection == PopupMenuViewArrowDirectionUp) {
        
        const CGFloat arrowXM = _arrowPosition;
        const CGFloat arrowX0 = arrowXM - kArrowSize;
        const CGFloat arrowX1 = arrowXM + kArrowSize;
        const CGFloat arrowY0 = Y0;
        const CGFloat arrowY1 = Y0 + kArrowSize + kEmbedFix;
        
        [arrowPath moveToPoint:    (CGPoint){arrowXM, arrowY0}];
        [arrowPath addLineToPoint: (CGPoint){arrowX1, arrowY1}];
        [arrowPath addLineToPoint: (CGPoint){arrowX0, arrowY1}];
        [arrowPath addLineToPoint: (CGPoint){arrowXM, arrowY0}];
        
        [[UIColor colorWithRed:R0 green:G0 blue:B0 alpha:1] set];
        
        Y0 += kArrowSize;
        
    } else if (_arrowDirection == PopupMenuViewArrowDirectionDown) {
        
        const CGFloat arrowXM = _arrowPosition;
        const CGFloat arrowX0 = arrowXM - kArrowSize;
        const CGFloat arrowX1 = arrowXM + kArrowSize;
        const CGFloat arrowY0 = Y1 - kArrowSize - kEmbedFix;
        const CGFloat arrowY1 = Y1;
        
        [arrowPath moveToPoint:    (CGPoint){arrowXM, arrowY1}];
        [arrowPath addLineToPoint: (CGPoint){arrowX1, arrowY0}];
        [arrowPath addLineToPoint: (CGPoint){arrowX0, arrowY0}];
        [arrowPath addLineToPoint: (CGPoint){arrowXM, arrowY1}];
        
        [[UIColor colorWithRed:R1 green:G1 blue:B1 alpha:1] set];
        
        Y1 -= kArrowSize;
        
    } else if (_arrowDirection == PopupMenuViewArrowDirectionLeft) {
        
        const CGFloat arrowYM = _arrowPosition;
        const CGFloat arrowX0 = X0;
        const CGFloat arrowX1 = X0 + kArrowSize + kEmbedFix;
        const CGFloat arrowY0 = arrowYM - kArrowSize;;
        const CGFloat arrowY1 = arrowYM + kArrowSize;
        
        [arrowPath moveToPoint:    (CGPoint){arrowX0, arrowYM}];
        [arrowPath addLineToPoint: (CGPoint){arrowX1, arrowY0}];
        [arrowPath addLineToPoint: (CGPoint){arrowX1, arrowY1}];
        [arrowPath addLineToPoint: (CGPoint){arrowX0, arrowYM}];
        
        [[UIColor colorWithRed:R0 green:G0 blue:B0 alpha:1] set];
        
        X0 += kArrowSize;
        
    } else if (_arrowDirection == PopupMenuViewArrowDirectionRight) {
        
        const CGFloat arrowYM = _arrowPosition;
        const CGFloat arrowX0 = X1;
        const CGFloat arrowX1 = X1 - kArrowSize - kEmbedFix;
        const CGFloat arrowY0 = arrowYM - kArrowSize;;
        const CGFloat arrowY1 = arrowYM + kArrowSize;
        
        [arrowPath moveToPoint:    (CGPoint){arrowX0, arrowYM}];
        [arrowPath addLineToPoint: (CGPoint){arrowX1, arrowY0}];
        [arrowPath addLineToPoint: (CGPoint){arrowX1, arrowY1}];
        [arrowPath addLineToPoint: (CGPoint){arrowX0, arrowYM}];
        
//        [[UIColor colorWithRed:R1 green:G1 blue:B1 alpha:1] set];
        [[UIColor whiteColor] set];
        
        X1 -= kArrowSize;
    }
    
    [arrowPath fill];
    
    // render body
    
    const CGRect bodyFrame = {X0, Y0, X1 - X0, Y1 - Y0};
    
    UIBezierPath *borderPath = [UIBezierPath bezierPathWithRoundedRect:bodyFrame
                                                          cornerRadius:8];
    
    const CGFloat locations[] = {0, 1};
    const CGFloat components[] = {
        R0, G0, B0, 1,
        R0, R0, R0, 1,
    };
    
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    CGGradientRef gradient = CGGradientCreateWithColorComponents(colorSpace,
                                                                 components,
                                                                 locations,
                                                                 sizeof(locations)/sizeof(locations[0]));
    CGColorSpaceRelease(colorSpace);
    
    
    [borderPath addClip];
    
    CGPoint start, end;
    
    if (_arrowDirection == PopupMenuViewArrowDirectionLeft ||
        _arrowDirection == PopupMenuViewArrowDirectionRight) {
        
        start = (CGPoint){X0, Y0};
        end = (CGPoint){X1, Y0};
        
    } else {
        
        start = (CGPoint){X0, Y0};
        end = (CGPoint){X0, Y1};
    }
    
    CGContextDrawLinearGradient(context, gradient, start, end, 0);
    
    CGGradientRelease(gradient);
}

@end

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////



@implementation PopupMenu {
    
    PopupMenuView *_menuView;
    BOOL        _observing;
}

static PopupMenu    *gMenu;
static UIColor      *gTintColor;
static UIFont       *gTitleFont;


+ (instancetype) sharedMenu
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        
        gMenu = [[PopupMenu alloc] init];
    });
    return gMenu;
}

- (id) init
{
    NSAssert(!gMenu, @"singleton object");
    
    self = [super init];
    if (self)
    {
        gTintColor = [UIColor whiteColor];
        gTitleFont = [UIFont systemFontOfSize:16];
    }
    return self;
}

- (void) dealloc
{
    if (_observing)
    {
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
}

- (void) showMenuInView:(UIView *)view fromRect:(CGRect)rect  menuItems:(NSArray *)menuItems
{
    NSParameterAssert(view);
    NSParameterAssert(menuItems.count);
    
    if (_menuView)
    {
        [_menuView dismissMenu:NO];
        _menuView = nil;
    }
    
    if (!_observing)
    {
        _observing = YES;
        
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(orientationWillChange:)  name:UIApplicationWillChangeStatusBarOrientationNotification object:nil];
    }
    
    _menuView = [[PopupMenuView alloc] init];
    [_menuView showMenuInView:view fromRect:rect menuItems:menuItems];
    _menuView.x -=10;
}

- (void) dismissMenu
{
    if (_menuView)
    {
        [_menuView dismissMenu:NO];
        _menuView = nil;
    }
    
    if (_observing)
    {
        _observing = NO;
        [[NSNotificationCenter defaultCenter] removeObserver:self];
    }
}

- (void) orientationWillChange:(NSNotification *)n
{
    [self dismissMenu];
}

+ (void)showMenuInView:(UIView *)view fromRect:(CGRect)rect menuItems:(NSArray *)menuItems
{
    [[self sharedMenu] showMenuInView:view fromRect:rect menuItems:menuItems];
}

+ (void)dismissMenu
{
    [[self sharedMenu] dismissMenu];
}

+ (UIColor *)tintColor
{
    return gTintColor;
}

+ (void)setTintColor:(UIColor *)tintColor
{
    if (tintColor != gTintColor)
    {
        gTintColor = tintColor;
    }
}

+ (UIFont *) titleFont
{
    return gTitleFont;
}

+ (void)setTitleFont:(UIFont *)titleFont
{
    if (titleFont != gTitleFont)
    {
        gTitleFont = titleFont;
    }
}

@end
