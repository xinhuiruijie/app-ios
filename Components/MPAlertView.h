//
//  MPAlertView.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/23.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MPAlertView : UIView

@property (nonatomic, copy) void(^cancelAction)(void);
@property (nonatomic, copy) void(^confirmAction)(void);

+ (instancetype)alertWithIcon:(UIImage *)icon title:(NSString *)title message:(NSString *)message confirmButton:(NSString *)confirmButton;
+ (instancetype)alertWithTitle:(NSString *)title cancelButton:(NSString *)cancelButton confirmButton:(NSString *)confirmButton;
+ (instancetype)alertWithTitle:(NSString *)title message:(NSString *)message cancelButton:(NSString *)cancelButton confirmButton:(NSString *)confirmButton;

- (void)show;
- (void)hide;

@end
