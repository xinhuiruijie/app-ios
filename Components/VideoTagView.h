//
//  VideoTagView.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/22.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, VIDEO_TAG_TYPE) {
    VIDEO_CATEGORY = 1,
    VIDEO_TAG,
};

@interface VideoTagView : UIView

@property (nonatomic, assign) VIDEO_TAG_TYPE type;
@property (nonatomic, strong) NSString *title;

@end
