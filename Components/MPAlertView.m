//
//  MPAlertView.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/23.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "MPAlertView.h"

@interface MPAlertView ()

@property (weak, nonatomic) IBOutlet UIView *backView;
@property (weak, nonatomic) IBOutlet UIView *alertView;
@property (weak, nonatomic) IBOutlet UIImageView *alertIcon;
@property (weak, nonatomic) IBOutlet UILabel *alertTitle;
@property (weak, nonatomic) IBOutlet UILabel *alertMessage;

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *confirmButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalLineWC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alertIconHC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alertIconTC;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *verticalLineCenterX;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *alertTitleTC;

@end

@implementation MPAlertView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    self.alertView.layer.cornerRadius = 12;
    self.alertView.layer.masksToBounds = YES;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.frame = self.superview.bounds;
}

+ (instancetype)alertWithTitle:(NSString *)title cancelButton:(NSString *)cancelButton confirmButton:(NSString *)confirmButton {
    return [[MPAlertView alloc] alertWithIcon:nil title:title message:nil cancelButton:cancelButton confirmButton:confirmButton];
}

+ (instancetype)alertWithIcon:(UIImage *)icon title:(NSString *)title message:(NSString *)message confirmButton:(NSString *)confirmButton {
    return [[MPAlertView alloc] alertWithIcon:icon title:title message:message cancelButton:nil confirmButton:confirmButton];
}

+ (instancetype)alertWithTitle:(NSString *)title message:(NSString *)message cancelButton:(NSString *)cancelButton confirmButton:(NSString *)confirmButton {
    return [[MPAlertView alloc] alertWithIcon:nil title:title message:message cancelButton:cancelButton confirmButton:confirmButton];
}

- (instancetype)alertWithIcon:(UIImage *)icon title:(NSString *)title message:(NSString *)message cancelButton:(NSString *)cancelButton confirmButton:(NSString *)confirmButton {
    MPAlertView *actionAlert = [MPAlertView loadFromNib];
    if (icon) {
        actionAlert.alertIcon.image = icon;
        actionAlert.alertIconHC.constant = 35;
        actionAlert.alertIconTC.constant = 24;
        actionAlert.alertTitleTC.constant = 12;
    } else {
        actionAlert.alertIcon.image = nil;
        actionAlert.alertIconHC.constant = 0;
        actionAlert.alertIconTC.constant = 0;
        actionAlert.alertTitleTC.constant = 30;
    }
    
    actionAlert.alertTitle.text = title;
    actionAlert.alertMessage.text = message;
    [actionAlert.confirmButton setTitle:confirmButton forState:UIControlStateNormal];
    
    if (cancelButton && cancelButton.length) {
        [actionAlert.cancelButton setTitle:cancelButton forState:UIControlStateNormal];
        actionAlert.verticalLineWC.constant = [AppTheme onePixel];
        actionAlert.verticalLineCenterX.constant = 0;
    } else {
        actionAlert.verticalLineWC.constant = 0;
        actionAlert.verticalLineCenterX.constant = 0 - (actionAlert.cancelButton.width + [AppTheme onePixel]);
    }
    
    return actionAlert;
}

#pragma mark - Action

- (IBAction)cancelAction:(id)sender {
    if (self.cancelAction) {
        self.cancelAction();
    }
//    [self cancelActionAnimated];
    [self hide];
}

- (IBAction)confirmAction:(id)sender {
    if (self.confirmAction) {
        self.confirmAction();
    }
//    [self confirmActionAnimated];
    [self hide];
}

#pragma mark -

- (void)show {
    self.alpha = 0.0;
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 1.0;
    }];
//    [self showActionAnimated];
}

- (void)hide {
    [UIView animateWithDuration:0.3 animations:^{
        self.alpha = 0.0;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)showActionAnimated {
    CGFloat afterOriginY = self.alertView.height / 2;
    CGFloat originY = kScreenHeight / 2 + self.alertView.height + afterOriginY;
    CGFloat originX = self.alertView.width / 2;
    
    CATransform3D translate = CATransform3DMakeTranslation(originX, -originY, 0); //平移
    CATransform3D rotate = CATransform3DRotate(translate, 9 * M_PI / 180, 0, 0, 1);
    
    CATransform3D afterTranslate = CATransform3DMakeTranslation(originX, afterOriginY, 0); //平移
    CATransform3D afterRotate = CATransform3DRotate(afterTranslate, 0, 0, 0, 1);

    // 锚点为中心点时translate为（0，0），锚点左边改变时，应该从原始锚点计算偏移值
    self.alertView.layer.anchorPoint = CGPointMake(1, 1);
    self.alertView.layer.transform = rotate;

    self.backView.alpha = 0;
    // Spring动画不能设置具体弹跳数值，不过这样写真的很👎
    [UIView animateWithDuration:0.28 delay:0 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        self.backView.alpha = 0.5;
        self.alertView.layer.transform = afterRotate;
    } completion:nil];
    [UIView animateWithDuration:0.08 delay:0.28 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        self.alertView.layer.transform = CATransform3DRotate(afterTranslate, -1.7 * M_PI / 180, 0, 0, 1);
    } completion:nil];
    [UIView animateWithDuration:0.16 delay:0.36 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        self.alertView.layer.transform = CATransform3DRotate(afterTranslate, 0.7 * M_PI / 180, 0, 0, 1);
    } completion:nil];
    [UIView animateWithDuration:0.12 delay:0.52 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        self.alertView.layer.transform = CATransform3DRotate(afterTranslate, -0.3 * M_PI / 180, 0, 0, 1);
    } completion:nil];
    [UIView animateWithDuration:0.16 delay:0.64 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        self.alertView.layer.transform = afterRotate;
    } completion:nil];
    
//    [UIView animateWithDuration:0.6 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:0.8 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
//        self.backView.alpha = 0.5;
//        self.alertView.layer.transform = afterRotate;
//    } completion:^(BOOL finished) {
//    }];
}

- (void)cancelActionAnimated {
    CGFloat originY = kScreenHeight / 2 + self.alertView.height;
    CATransform3D translate = CATransform3DMakeTranslation(0, originY, 0); //平移
    CATransform3D rotate = CATransform3DRotate(translate, 20 * M_PI / 180, 0, 0, 1);
    
    self.alertView.layer.transform = CATransform3DIdentity;
    self.alertView.layer.anchorPoint = CGPointMake(0.5, 0.5);
    
    self.backView.alpha = 0.5;
    [UIView animateWithDuration:0.5 delay:0 usingSpringWithDamping:0.8 initialSpringVelocity:1.0 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        self.backView.alpha = 0;
        self.alertView.layer.transform = rotate;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

- (void)confirmActionAnimated {
    CATransform3D scale = CATransform3DMakeScale(0.1, 0.1, 1);
    
    self.alertView.layer.transform = CATransform3DIdentity;
    self.alertView.layer.anchorPoint = CGPointMake(0.5, 0.5);
    
    self.backView.alpha = 0.5;
    [UIView animateWithDuration:0.3 animations:^{
        self.backView.alpha = 0;
        self.alertView.layer.transform = scale;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

@end
