//
//  VideoTagView.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/22.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "VideoTagView.h"
#import "SearchViewController.h"

@interface VideoTagView ()

@property (nonatomic, strong) UIButton *tagButton;

@end

@implementation VideoTagView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [AppTheme lineColor];
        self.layer.cornerRadius = 9;
        self.layer.masksToBounds = YES;
        
        UIButton *tagButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, frame.size.height)];
        [tagButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [tagButton.titleLabel setFont:[UIFont systemFontOfSize:12]];
        [tagButton.titleLabel setTextAlignment:NSTextAlignmentCenter];
        [tagButton.titleLabel setLineBreakMode:NSLineBreakByTruncatingTail];
        [tagButton setTitleEdgeInsets:UIEdgeInsetsMake(0, 6, 0, 6)];
        [tagButton addTarget:self action:@selector(tagClick:) forControlEvents:UIControlEventTouchUpInside];
        self.tagButton = tagButton;
        
        [self addSubview:tagButton];
    }
    return self;
}

- (void)setTitle:(NSString *)title {
    _title = title;
    [self.tagButton setTitle:title forState:UIControlStateNormal];
}

- (void)setType:(VIDEO_TAG_TYPE)type {
    _type = type;
    if (type == VIDEO_CATEGORY) {
        self.backgroundColor = [AppTheme selectedColor];
        [self.tagButton setBackgroundImage:[UIImage imageWithColor:[AppTheme selectedColor]] forState:UIControlStateNormal];
    } else {
        self.backgroundColor = [AppTheme lineColor];
        [self.tagButton setBackgroundImage:[UIImage imageWithColor:[AppTheme lineColor]] forState:UIControlStateNormal];
        [self.tagButton setBackgroundImage:[UIImage imageWithColor:[AppTheme selectedColor]] forState:UIControlStateHighlighted];
    }
}

- (void)tagClick:(UIButton *)sender {
    if (self.type == VIDEO_CATEGORY) {
        return;
    }
    NSString *title = [NSString stringWithString:self.title];
    if ([title rangeOfString:@"#"].location == 0) {
        title = [title substringFromIndex:1];
    }
    
    if ([[MPRootViewController sharedInstance].currentViewController isKindOfClass:[SearchViewController class]]) {
        SearchViewController *searchController = (SearchViewController *)[MPRootViewController sharedInstance].currentViewController;
        searchController.keyword = title;
        searchController.actionState = SearchActionStateDone;
        [searchController reloadSearch];
    } else {
        NSString *link = [NSString stringWithFormat:@"mplanet://search?k=%@", title];
        if (![DeepLink handleURLString:link withCompletion:nil]) {
            [[UIApplication sharedApplication].keyWindow presentFailureTips:@"无法打开的链接"];
        }
    }
}

@end
