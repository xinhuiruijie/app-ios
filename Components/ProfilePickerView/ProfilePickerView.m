//
//  ProfilePickerView.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "ProfilePickerView.h"

@interface ProfilePickerView () <UIPickerViewDataSource, UIPickerViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (weak, nonatomic) IBOutlet UIView *basicView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

@property (nonatomic, strong) NSMutableArray *dataArray;

@end

@implementation ProfilePickerView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.pickerView.dataSource = self;
    self.pickerView.delegate = self;
    self.titleLabel.text = @"选择所在地";
    self.dataArray = [NSMutableArray array];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
}

#pragma mark - CustomMethod

- (void)dataDidChange {
    self.dataArray = self.data;
    [self.pickerView selectRow:self.selectIndex inComponent:0 animated:YES];
    [self.pickerView reloadComponent:0];
}

- (void)show {
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [self showActionAnimated];
}

- (void)hide {
    [self hideActionAnimated];
}

- (void)showActionAnimated {
    CATransform3D translate = CATransform3DMakeTranslation(0, kScreenHeight, 0); //平移
    self.basicView.layer.transform = translate;
    self.backButton.alpha = 0;
    
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:1.0 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        self.backButton.alpha = 0.5;
        self.basicView.layer.transform = CATransform3DIdentity;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)hideActionAnimated {
    CATransform3D translate = CATransform3DMakeTranslation(0, kScreenHeight, 0); //平移
    self.basicView.layer.transform = CATransform3DIdentity;
    self.backButton.alpha = 0.5;
    
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:1.0 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        self.backButton.alpha = 0;
        self.basicView.layer.transform = translate;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
    }];
}

#pragma mark - UIPickerViewDataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return self.dataArray.count;
}

#pragma mark - UIPickerViewDelegate

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    return self.dataArray[row];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view {
    UILabel *pickerLabel = (UILabel *)view;
    if (!pickerLabel) {
        pickerLabel = [[UILabel alloc] init];
        pickerLabel.adjustsFontSizeToFitWidth = YES;
        pickerLabel.textAlignment = NSTextAlignmentCenter;
        pickerLabel.backgroundColor = [UIColor clearColor];
        pickerLabel.font = [UIFont systemFontOfSize:18];
    }
    pickerLabel.text = [self pickerView:pickerView titleForRow:row forComponent:component];
    return pickerLabel;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    self.selectIndex = row;
}

#pragma mark - IBAction

- (IBAction)hidePickerView:(id)sender {
    [self hide];
}

- (IBAction)cancelAction:(id)sender {
    [self hide];
}

- (IBAction)confirmAction:(id)sender {
    if ([self.delegate respondsToSelector:@selector(finishPickProvinceForRow:)]) {
        [self.delegate finishPickProvinceForRow:self.selectIndex];
    }
    [self hide];
}
@end
