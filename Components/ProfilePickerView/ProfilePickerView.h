//
//  ProfilePickerView.h
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/20.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ProfilePickerViewDelegate <NSObject>

- (void)finishPickProvinceForRow:(NSInteger)row;

@end

typedef NS_ENUM(NSInteger, EDIT_TYPE) {
    EDIT_TYPE_BIRTHDAY = 1,
    EDIT_TYPE_CONSTELLATION,
    EDIT_TYPE_ADDRESS,
};

@interface ProfilePickerView : UIView

@property (nonatomic, weak) id <ProfilePickerViewDelegate> delegate;

@property (nonatomic, assign) NSInteger selectIndex;

- (void)show;

- (void)hide;

@end
