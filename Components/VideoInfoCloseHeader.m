//
//  VideoInfoCloseHeader.m
//  MotherPlanet
//
//  Created by liuyadi on 2018/5/22.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "VideoInfoCloseHeader.h"
#import "MJRefreshConst.h"
#import "UIView+MJExtension.h"

@interface VideoInfoCloseHeader ()

@property (nonatomic, strong) UIImageView *closeImage;

@end

@implementation VideoInfoCloseHeader

#pragma mark - 懒加载

- (UIImageView *)closeImage {
    if (!_closeImage) {
        _closeImage = [[UIImageView alloc] init];
        _closeImage.contentMode = UIViewContentModeScaleAspectFit;
        NSMutableArray *animateImages = [NSMutableArray array];
        for (int i = 1; i < 18; i++) {
            NSString *imageName = [NSString stringWithFormat:@"video_pull_%d", i];
            UIImage *image = [UIImage imageNamed:imageName];
            [animateImages addObject:image];
        }
        _closeImage.image = [UIImage imageNamed:@"video_pull_1"];
        _closeImage.animationImages = animateImages;
        [self addSubview:_closeImage];
    }
    return _closeImage;
}

- (void)setPullingPercent:(CGFloat)pullingPercent {
    [super setPullingPercent:pullingPercent];
    // 忽略前50%的下拉比例
    CGFloat p = pullingPercent * 100 - 30;
    if (p > 0) {
        NSInteger i = p / (30 / 9.0);
        if (i > 0 && i < 18) {
            NSString *imageName = [NSString stringWithFormat:@"video_pull_%ld", i];
            self.closeImage.image = [UIImage imageNamed:imageName];
        }
    }
}

#pragma mark - 初始化
- (void)layoutSubviews {
    [super layoutSubviews];
    
    // 设置自己的位置
    self.mj_y = - self.mj_h - self.insetTop;
    
    // 加载中显示图片
    self.closeImage.frame = CGRectMake(0, 0, 80, 36);
    self.closeImage.center = CGPointMake(self.width / 2, self.height / 2);
}

@end
