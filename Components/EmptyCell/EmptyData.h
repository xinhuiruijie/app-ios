//
//  EmptyData.h
//  GeekMall
//
//  Created by liuyadi on 2017/10/30.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EmptyData : NSObject

@property (nonatomic, strong) NSString *imageUrl;
@property (nonatomic, strong) NSString *emptyTip;

@end
