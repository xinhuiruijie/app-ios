//
//  EmptyTableCell.h
//  GeekMall
//
//  Created by liuyadi on 2017/10/30.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmptyTableCell : UITableViewCell

@property (nonatomic, assign) BOOL half;

@end
