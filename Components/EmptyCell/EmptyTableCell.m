//
//  EmptyTableCell.m
//  GeekMall
//
//  Created by liuyadi on 2017/10/30.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "EmptyTableCell.h"

@interface EmptyTableCell ()

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleCenterY;

@end

@implementation EmptyTableCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.backgroundColor = [UIColor whiteColor];
}

- (void)layoutSubviews {
    [super layoutSubviews];
    if (self.half) {
        self.titleCenterY.constant = 50;
    } else {
        self.titleCenterY.constant = 0;
    }
}

- (void)dataDidChange {
    
}

@end
