//
//  EmptyCell.h
//  GeekMall
//
//  Created by liuyadi on 2017/10/30.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#ifndef EmptyCell_h
#define EmptyCell_h

#import "EmptyData.h"
#import "EmptyCollectionCell.h"
#import "EmptyTableCell.h"

#endif /* EmptyCell_h */
