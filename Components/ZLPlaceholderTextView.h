//
//  ZLPlaceholderTextView.h
//  HemeiUnion
//
//  Created by GeekZooStudio on 2017/8/31.
//  Copyright © 2017年 GeekZooStudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ZLPlaceholderTextViewDelegate <UITextViewDelegate>
@end

@interface ZLPlaceholderTextView : UIView <UITextViewDelegate>
/// textView
@property (nonatomic, strong, nullable) UITextView  *textView;
/// placeholder
@property (nonatomic, strong, nullable) UITextView  *placeholderTextView;

@property (nonatomic, weak) id<ZLPlaceholderTextViewDelegate> _Nullable delegate;

@property (nonatomic, strong, nullable) UIFont      *font;
@property (nonatomic, strong, nullable) NSString    *text;
@property (nonatomic, strong, nullable) NSString    *placeholder;
@property (nonatomic, assign) NSInteger maxLenght; // 限制输入字数，超出部分不可输入，0表示不限制，默认是0

+ (instancetype _Nonnull )placeholderTextView;

@end
