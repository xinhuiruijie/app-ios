//
//  VideoInfoCloseHeader.h
//  MotherPlanet
//
//  Created by liuyadi on 2018/5/22.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "MJRefreshHeader.h"

@interface VideoInfoCloseHeader : MJRefreshHeader

@property (nonatomic, assign) CGFloat insetTop;

@end
