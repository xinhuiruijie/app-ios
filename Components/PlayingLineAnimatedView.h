//
//  PlayingLineAnimatedView.h
//  MotherPlanet
//
//  Created by GeekZooStudio on 2018/4/23.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PlayingLineAnimatedView : UIView
- (instancetype)initWithFrame:(CGRect)frame lineWidth:(float)lineWidth lineColor:(UIColor*)lineColor;
- (void)addAnimation;
- (void)removeAnimation;
@end
