//
//  NSString+TextLength.m
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/27.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "NSString+TextLength.h"

@implementation NSString (TextLength)

- (NSUInteger)textLength {
    NSUInteger asciiLength = 0;
    for (NSUInteger i = 0; i < self.length; i++) {
        unichar uc = [self characterAtIndex:i];
        //判断是否是中文
        BOOL isChinese = (uc > 0x4e00 && uc < 0x9fff);
        asciiLength += isChinese ? 2 : 1;
    }
    NSUInteger unicodeLength = asciiLength;
    return unicodeLength;
}

@end
