//
//  NSString+TextLength.h
//  MotherPlanet
//
//  Created by 陈熙 on 2017/12/27.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (TextLength)

- (NSUInteger)textLength;

@end
