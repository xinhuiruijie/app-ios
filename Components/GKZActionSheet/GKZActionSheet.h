//
//  GKZActionSheet.h
//  GeekMall
//
//  Created by liuyadi on 2017/10/25.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface GKZActionSheet : UIView <AlertContentView>

@end
