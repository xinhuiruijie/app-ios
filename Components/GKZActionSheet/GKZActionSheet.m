//
//  GKZActionSheet.m
//  GeekMall
//
//  Created by liuyadi on 2017/10/25.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "GKZActionSheet.h"

@interface GKZActionSheetCell : UITableViewCell
@property (nonatomic, strong) UILabel *nameLabel;
@end

@implementation GKZActionSheetCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        UILabel *nameLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, self.width, self.height)];
        nameLabel.textColor = [AppTheme mainColor];
        nameLabel.font = [UIFont systemFontOfSize:14];
        nameLabel.textAlignment = NSTextAlignmentCenter;
        [self addSubview:nameLabel];
        self.nameLabel = nameLabel;
        
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.nameLabel.frame = CGRectMake(0, 0, self.width, self.height);
}

- (void)dataDidChange {
    NSString *name = self.data;
    self.nameLabel.text = name;
}

@end

@interface GKZActionSheet () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ViewHC;
@property (nonatomic, strong) NSArray *dataSource;

@end

@implementation GKZActionSheet

@synthesize container = _container;
@synthesize whenSeleced = _whenSeleced;
@synthesize whenRegistered = _whenRegistered;
@synthesize whenHide = _whenHide;

- (IBAction)cancelAction:(id)sender {
    if (self.whenHide) {
        self.whenHide(YES);
    }
}

- (void)awakeFromNib {
    [super awakeFromNib];
    self.dataSource = [NSArray array];
}

- (void)dataDidChange {
    self.dataSource = self.data;
    CGFloat height = 44 * (self.dataSource.count + 1) + 12 + 15;
    self.ViewHC.constant = height;
    self.height = height + 44;
    [self.tableView reloadData];
}

#pragma mark - UITableViewDataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.dataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    GKZActionSheetCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GKZActionSheetCell"];
    if (cell == nil) {
        cell = [[GKZActionSheetCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"GKZActionSheetCell"];
    }
    cell.data = self.dataSource[indexPath.row];
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.whenRegistered) {
        self.whenRegistered(self.dataSource[indexPath.row], indexPath.row);
    }
}

@end
