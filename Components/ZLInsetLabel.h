//
//  ZLInsetLabel.h
//  cowry
//
//  Created by GeekZooStudio on 2018/5/7.
//  Copyright © 2018年 GeekZooStudio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZLInsetLabel : UILabel
@property (nonatomic, assign) UIEdgeInsets textInsets;
@end
