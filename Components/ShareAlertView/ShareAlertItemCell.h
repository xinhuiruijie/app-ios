//
//  ShareAlertItemCell.h
//  GeekMall
//
//  Created by 陈熙 on 2017/11/14.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ShareAlertItemCell : UICollectionViewCell

@property (nonatomic, assign) SHARE_TYPE type;

@end
