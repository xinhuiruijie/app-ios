//
//  BaseShareView.m
//  MotherPlanet
//
//  Created by liuyadi on 2018/4/17.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "BaseShareView.h"
#import "ConfigModel.h"
#import "VideoModel.h"
#import "ArticleModel.h"
#import "UserPointModel.h"

NSNotificationName kMPSharedFinishNotification = @"kMPSharedFinishNotification";

@implementation BaseShareView

- (void)awakeFromNib {
    [super awakeFromNib];
    self.shareItems = [NSMutableArray array];
    if ([AppConfig wechatSharedShow]) {
        [self.shareItems addObject:@(SHARE_TYPE_WECHAT)];
        [self.shareItems addObject:@(SHARE_TYPE_MOMENTS)];
    }
    if ([AppConfig weiboSharedShow]) {
        [self.shareItems addObject:@(SHARE_TYPE_WEIBO)];
    }
    if ([AppConfig QQSharedShow]) {
        [self.shareItems addObject:@(SHARE_TYPE_QQ)];
        [self.shareItems addObject:@(SHARE_TYPE_QQZONE)];
    }
    
    [self.shareItems addObject:@(SHARE_TYPE_COPY_LINK)];
}

- (void)dataDidChange {
    self.sharePost = [[SharedPost alloc] init];
    
    if ([self.data isKindOfClass:[VIDEO class]]) {
        VIDEO *video = self.data;
        self.video = video;
        self.sharePost.title = video.title ?: [AppTheme defaultPlaceholder];
        self.sharePost.text = video.subtitle ?: [AppTheme defaultPlaceholder];
        if (video.shareImage) {
            self.sharePost.photo = video.shareImage;
            self.sharePost.thumb = [video.shareImage scaleToSize:CGSizeMake(60, 60)];
        } else {
            self.sharePost.photo = [UIImage imageNamed:@"shareIcon"];
            self.sharePost.thumb = [UIImage imageNamed:@"shareIcon"];
        }
        
        if ([UserModel online]) {
            self.sharePost.url = [NSString stringWithFormat:@"%@/#/video?videoId=%@&userId=%@", [ConfigModel sharedInstance].share_url, video.id, [UserModel sharedInstance].user.id];
        } else {
            self.sharePost.url = [NSString stringWithFormat:@"%@/#/video?videoId=%@", [ConfigModel sharedInstance].share_url, video.id];
        }
    } else if ([self.data isKindOfClass:[ARTICLE class]]) {
        ARTICLE *article = self.data;
        self.article = article;
        self.sharePost.title = article.title ?: [AppTheme defaultPlaceholder];
        self.sharePost.text = article.subtitle ?: [AppTheme defaultPlaceholder];
        if (article.shareImage) {
            self.sharePost.photo = article.shareImage;
            self.sharePost.thumb = [article.shareImage scaleToSize:CGSizeMake(60, 60)];
        } else {
            self.sharePost.photo = [UIImage imageNamed:@"shareIcon"];
            self.sharePost.thumb = [UIImage imageNamed:@"shareIcon"];
        }
        
        if ([UserModel online]) {
            self.sharePost.url = [NSString stringWithFormat:@"%@/#/article?articleId=%@&userId=%@", [ConfigModel sharedInstance].share_url, article.id, [UserModel sharedInstance].user.id];
        } else {
            self.sharePost.url = [NSString stringWithFormat:@"%@/#/article?articleId=%@", [ConfigModel sharedInstance].share_url, article.id];
        }
    } else if ([self.data isKindOfClass:[AUDIO class]]) {
        AUDIO *audio = self.data;
        self.audio = audio;
        self.sharePost.title = audio.title ?: [AppTheme defaultPlaceholder];
        self.sharePost.text = audio.subtitle ?: [AppTheme defaultPlaceholder];
        if (audio.photo) {
            self.sharePost.photo = audio.photo.large;
            self.sharePost.thumb = audio.photo.thumb;
        } else {
            self.sharePost.photo = [UIImage imageNamed:@"shareIcon"];
            self.sharePost.thumb = [UIImage imageNamed:@"shareIcon"];
        }
        if ([UserModel online]) {
            self.sharePost.url = [NSString stringWithFormat:@"%@/#/audio?audioId=%@&userId=%@", [ConfigModel sharedInstance].share_url, audio.id, [UserModel sharedInstance].user.id];
        } else {
            self.sharePost.url = [NSString stringWithFormat:@"%@/#/audio?audioId=%@", [ConfigModel sharedInstance].share_url, audio.id];
        }
        
    } else if ([self.data isKindOfClass:[USER class]]) {
        if ([UserModel online]) {
            USER *user = self.data;
            self.sharePost.title = @"欢迎加入母星系Motherplanet";
            self.sharePost.text = @"一起创造不可思议";
            self.sharePost.photo = [UIImage imageNamed:@"shareIcon"];
            self.sharePost.thumb = [UIImage imageNamed:@"shareIcon"];
            self.sharePost.url = [NSString stringWithFormat:@"%@/#/signup?userId=%@", [ConfigModel sharedInstance].share_url, user.id];
        }
    }
}

#pragma mark - vendor

- (void)qqShared {
    if ([TencentOAuth iphoneQQInstalled] == NO) {
        [[UIApplication sharedApplication].keyWindow presentSuccessTips:@"尚未安装QQ客户端"];
        return;
    }
    
    if ([self.data isKindOfClass:[VIDEO class]]) {
        [AppAnalytics clickEvent:@"click_video_shareToQQ"];
    }
    ALIAS([TencentOpenShared sharedInstance], tencent);
    tencent.post = self.sharePost;
    @weakify(self)
    tencent.whenShareSucceed = ^{
        @strongify(self)
        [UserPointModel userPointName:MODULE_NAME_CLICK_SHARE_FINISH_QQ];
        [[MPRootViewController sharedInstance].topViewController.view presentSuccessTips:@"分享成功"];
        [self countShareTimesWithType:SHARE_TYPE_QQ];
        [self sharedFinish];
    };
    tencent.whenShareFailed = ^{
        @strongify(self)
        [[MPRootViewController sharedInstance].topViewController.view presentSuccessTips:@"取消分享"];
        [self countShareTimesWithType:SHARE_TYPE_QQ];
        [self sharedFinish];
    };
    [tencent shareQq];
    [self hide];
}

- (void)qqZoneShared {
    if ([TencentOAuth iphoneQQInstalled] == NO) {
        [[UIApplication sharedApplication].keyWindow presentSuccessTips:@"尚未安装QQ客户端"];
        return;
    }
    
    if ([self.data isKindOfClass:[VIDEO class]]) {
        [AppAnalytics clickEvent:@"click_video_shareToQQSpace"];
    }
    ALIAS([TencentOpenShared sharedInstance], tencent);
    tencent.post = self.sharePost;
    @weakify(self)
    tencent.whenShareSucceed = ^{
        @strongify(self)
        [UserPointModel userPointName:MODULE_NAME_CLICK_SHARE_FINISH_QZONE];
        [[MPRootViewController sharedInstance].topViewController.view presentSuccessTips:@"分享成功"];
        [self countShareTimesWithType:SHARE_TYPE_QQZONE];
        [self sharedFinish];
    };
    tencent.whenShareFailed = ^{
        @strongify(self)
        [[MPRootViewController sharedInstance].topViewController.view presentSuccessTips:@"取消分享"];
        [self countShareTimesWithType:SHARE_TYPE_QQZONE];
        [self sharedFinish];
    };
    [tencent shareQzone];
    [self hide];
}

- (void)wechatShared {
    if ([WXApi isWXAppInstalled] == NO) {
        [[UIApplication sharedApplication].keyWindow presentSuccessTips:@"尚未安装微信客户端"];
        return;
    }
    
    if ([self.data isKindOfClass:[VIDEO class]]) {
        [AppAnalytics clickEvent:@"click_video_shareToWeChat"];
    }
    ALIAS([WXChatShared sharedInstance], wechat);
    wechat.post = self.sharePost;
    @weakify(self)
    wechat.whenShareSucceed = ^{
        @strongify(self)
        [UserPointModel userPointName:MODULE_NAME_CLICK_SHARE_FINISH_WECHAT];
        [[MPRootViewController sharedInstance].topViewController.view presentSuccessTips:@"分享成功"];
        [self countShareTimesWithType:SHARE_TYPE_WECHAT];
        [self sharedFinish];
    };
    wechat.whenShareFailed = ^{
        @strongify(self)
        [[MPRootViewController sharedInstance].topViewController.view presentSuccessTips:@"取消分享"];
        [self countShareTimesWithType:SHARE_TYPE_WECHAT];
        [self sharedFinish];
    };
    [wechat shareFriend];
    [self hide];
}

- (void)wechatMomentsShared {
    if ([WXApi isWXAppInstalled] == NO) {
        [[UIApplication sharedApplication].keyWindow presentSuccessTips:@"尚未安装微信客户端"];
        return;
    }
    
    if ([self.data isKindOfClass:[VIDEO class]]) {
        [AppAnalytics clickEvent:@"click_video_shareToMoments"];
    }
    ALIAS([WXChatShared sharedInstance], wechat);
    wechat.post = self.sharePost;
    @weakify(self)
    wechat.whenShareSucceed = ^{
        @strongify(self)
        [UserPointModel userPointName:MODULE_NAME_CLICK_SHARE_FINISH_MOMENTS];
        [[MPRootViewController sharedInstance].topViewController.view presentSuccessTips:@"分享成功"];
        [self countShareTimesWithType:SHARE_TYPE_MOMENTS];
        [self sharedFinish];
    };
    wechat.whenShareFailed = ^{
        @strongify(self)
        [[MPRootViewController sharedInstance].topViewController.view presentSuccessTips:@"取消分享"];
        [self countShareTimesWithType:SHARE_TYPE_MOMENTS];
        [self sharedFinish];
    };
    [wechat shareTimeline];
    [self hide];
}

- (void)weiboShared {
    if ([WeiboSDK isWeiboAppInstalled] == NO) {
        [[UIApplication sharedApplication].keyWindow presentSuccessTips:@"尚未安装微博客户端"];
        return;
    }
    
    if ([self.data isKindOfClass:[VIDEO class]]) {
        [AppAnalytics clickEvent:@"click_video_shareToWeibo"];
    }
    ALIAS([SinaWeibo sharedInstance], sina);
    sina.post = self.sharePost;
    @weakify(self)
    sina.whenShareSucceed = ^{
        @strongify(self)
        [UserPointModel userPointName:MODULE_NAME_CLICK_SHARE_FINISH_WEIBO];
        [[MPRootViewController sharedInstance].topViewController.view presentSuccessTips:@"分享成功"];
        [self countShareTimesWithType:SHARE_TYPE_WEIBO];
        [self sharedFinish];
    };
    sina.whenShareFailed = ^{
        @strongify(self)
        [[MPRootViewController sharedInstance].topViewController.view presentSuccessTips:@"取消分享"];
        [self countShareTimesWithType:SHARE_TYPE_WEIBO];
        [self sharedFinish];
    };
    
    [sina share];
    [self hide];
}

- (void)copyLinkShared {
    if ([self.data isKindOfClass:[VIDEO class]]) {
        [AppAnalytics clickEvent:@"click_video_shareCopyLink"];
    }
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    if (self.sharePost.url) {
        pasteboard.string = self.sharePost.url;
        [[MPRootViewController sharedInstance].topViewController.view presentSuccessTips:@"复制成功"];
        [self hide];
    }
}

- (void)countShareTimesWithType:(SHARE_TYPE)shareType {
    if (self.video) {
        @weakify(self)
        [VideoModel share:self.video type:(int)shareType+1 then:^(STIHTTPResponseError *error) {
            @strongify(self)
            if (error) {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
        }];
    }
    if (self.article) {
        @weakify(self)
        [ArticleModel share:self.article then:^(STIHTTPResponseError *error) {
            @strongify(self)
            if (error) {
                [self presentMessage:error.message withTips:@"数据获取失败"];
            }
        }];
    }
}

- (void)sharedFinish {
    if (self.video) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [[NSNotificationCenter defaultCenter] postNotificationName:kMPSharedFinishNotification object:self.video userInfo:nil];
        });
    }
}

- (void)show {
    
}

- (void)hide {
    
}

@end
