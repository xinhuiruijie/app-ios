//
//  DarkShareItems.m
//  MotherPlanet
//
//  Created by liuyadi on 2018/4/17.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "DarkShareItems.h"
#import "BaseShareView.h"

@interface DarkShareItems ()

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation DarkShareItems

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)dataDidChange {
    if (self.data == nil) {
        self.iconImageView.hidden = YES;
        self.titleLabel.hidden = YES;
    } else {
        self.iconImageView.hidden = NO;
        self.titleLabel.hidden = NO;
        
        NSNumber *data = self.data;
        switch ([data integerValue]) {
            case SHARE_TYPE_WECHAT: {
                self.iconImageView.image = [UIImage imageNamed:@"icon_share_wechat"];
                self.titleLabel.text = @"微信好友";
            }
                break;
            case SHARE_TYPE_MOMENTS: {
                self.iconImageView.image = [UIImage imageNamed:@"icon_share_fc"];
                self.titleLabel.text= @"朋友圈";
            }
                break;
            case SHARE_TYPE_WEIBO: {
                self.iconImageView.image = [UIImage imageNamed:@"icon_share_sina"];
                self.titleLabel.text = @"微博";
            }
                break;
            case SHARE_TYPE_QQ: {
                self.iconImageView.image = [UIImage imageNamed:@"icon_share_qq"];
                self.titleLabel.text = @"QQ好友";
            }
                break;
            case SHARE_TYPE_QQZONE: {
                self.iconImageView.image = [UIImage imageNamed:@"icon_share_zone"];
                self.titleLabel.text = @"QQ空间";
            }
                break;
            case SHARE_TYPE_COPY_LINK: {
                self.iconImageView.image = [UIImage imageNamed:@"icon_share_link"];
                self.titleLabel.text = @"复制链接";
            }
            default:
                break;
        }
    }
}

@end
