//
//  BaseShareView.h
//  MotherPlanet
//
//  Created by liuyadi on 2018/4/17.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TencentOpenShared.h"
#import "WXChatShared.h"
#import "SinaWeibo.h"
#import "SharedPost.h"

extern NSNotificationName kMPSharedFinishNotification;

typedef NS_ENUM(NSInteger, SHARE_TYPE) {
    SHARE_TYPE_WECHAT = 0,
    SHARE_TYPE_MOMENTS,
    SHARE_TYPE_WEIBO,
    SHARE_TYPE_QQ,
    SHARE_TYPE_QQZONE,
    SHARE_TYPE_COPY_LINK,
};

@interface BaseShareView : UIView

- (void)show;
- (void)hide;

- (void)qqShared;
- (void)qqZoneShared;
- (void)wechatShared;
- (void)wechatMomentsShared;
- (void)weiboShared;
- (void)copyLinkShared;

@property (nonatomic, strong) NSMutableArray *shareItems;
@property (nonatomic, strong) SharedPost *sharePost;
@property (nonatomic, strong) VIDEO *video;
@property (nonatomic, strong) ARTICLE *article;
@property (nonatomic, strong) AUDIO *audio;

@end
