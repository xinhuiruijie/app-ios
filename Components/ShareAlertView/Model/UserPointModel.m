//
//  UserPointModel.m
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/11/22.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "UserPointModel.h"

@implementation UserPointModel

+ (void)userPointName:(MODULE_NAME)moduleName {
    NSString *name = [NSString string];
    switch (moduleName) {
        case MODULE_NAME_CLICK_REGISTER_FINISH:
            name = @"click_register_finish";
            break;
        case MODULE_NAME_CLICK_LOGIN_FINISH:
            name = @"click_login_finish";
            break;
        case MODULE_NAME_CLICK_THUMBUP_FINISH:
            name = @"click_thumbUp_finish";
            break;
        case MODULE_NAME_CLICK_PLAY_FINISH:
            name = @"click_play_finish";
            break;
        case MODULE_NAME_CLICK_SHARE_FINISH:
            name = @"click_share_finish";
            break;
        case MODULE_NAME_CLICK_SHARE_FINISH_WECHAT:
            name = @"click_share_finish_WeChat";
            break;
        case MODULE_NAME_CLICK_SHARE_FINISH_MOMENTS:
            name = @"click_share_finish_moments";
            break;
        case MODULE_NAME_CLICK_SHARE_FINISH_WEIBO:
            name = @"click_share_finish_Weibo";
            break;
        case MODULE_NAME_CLICK_SHARE_FINISH_QQ:
            name = @"click_share_finish_QQ";
            break;
        case MODULE_NAME_CLICK_SHARE_FINISH_QZONE:
            name = @"click_share_finish_Qzone";
            break;
        case MODULE_NAME_CLICK_COLLECT_FINISH:
            name = @"click_collect_finish";
            break;
        case MODULE_NAME_CLICK_SCORE_FINISH:
            name = @"click_score_finish";
            break;
            
        default:
            break;
    }
    
    V1_API_USER_POINT_API *api = [[V1_API_USER_POINT_API alloc] init];
    api.req.name = name;
    api.whenUpdated = ^(V1_API_USER_POINT_RESPONSE *response, NSDictionary *allHeaders, STIHTTPResponseError *error) {
//        if ( error ) {
//            PERFORM_BLOCK_SAFELY(then, error);
//        } else {
//            if (X_MPlanet_ErrorCode(allHeaders) == 0) {
//                PERFORM_BLOCK_SAFELY(then, nil);
//            } else {
//                STIHTTPResponseError * errors = [[STIHTTPResponseError alloc] init];
//                errors.code = X_MPlanet_ErrorCode(allHeaders);
//                errors.message = X_MPlanet_ErrorDesc(allHeaders);
//                PERFORM_BLOCK_SAFELY(then, errors);
//            }
//        }
    };
    [api send];
}

@end
