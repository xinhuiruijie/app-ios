//
//  UserPointModel.h
//  MotherPlanet
//
//  Created by 宋朝阳 on 2018/11/22.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "STIStreamModel.h"

NS_ASSUME_NONNULL_BEGIN

@interface UserPointModel : STIStreamModel

+ (void)userPointName:(MODULE_NAME)moduleName;

@end

NS_ASSUME_NONNULL_END
