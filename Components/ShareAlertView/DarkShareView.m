//
//  DarkShareView.m
//  MotherPlanet
//
//  Created by liuyadi on 2018/4/17.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "DarkShareView.h"
#import "DarkShareItems.h"
#import "UserPointModel.h"

@interface DarkShareView () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UIView *actionView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UILabel *shareTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *shareTitleLabel;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@end

@implementation DarkShareView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [UserPointModel userPointName:MODULE_NAME_CLICK_SHARE_FINISH];
    
    [self customize];
    
    self.pageControl.numberOfPages = self.shareItems.count / 5 + self.shareItems.count % 5;
    self.pageControl.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.8, 0.8);
}

- (void)layoutSubviews {
    [super layoutSubviews];
    CGFloat bottomHeight = [SDiOSVersion deviceSize] == Screen5Dot8inch ? 34 : 0 ;
    self.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight + bottomHeight);
}

- (void)dataDidChange {
    [super dataDidChange];
    
    if ([self.data isKindOfClass:[VIDEO class]]) {
        self.shareTypeLabel.text = @"分享短片";
        self.shareTitleLabel.text = self.video.title;
    } else if ([self.data isKindOfClass:[ARTICLE class]]) {
        self.shareTypeLabel.text = @"分享日报";
        self.shareTitleLabel.text = self.article.title;
    } else if ([self.data isKindOfClass:[AUDIO class]]) {
        self.shareTypeLabel.text = @"分享节目";
        self.shareTitleLabel.text = self.audio.title;
    }
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
//    return self.shareItems.count;
    return 10;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    DarkShareItems *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DarkShareItems" forIndexPath:indexPath];
    if (self.shareItems.count <= indexPath.row) {
        cell.data = nil;
    } else {
        cell.data = self.shareItems[indexPath.item];
    }
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (self.shareItems.count <= indexPath.row) {
        return;
    }
    switch ([self.shareItems[indexPath.item] integerValue]) {
        case SHARE_TYPE_WECHAT:
            [self wechatShared];
            break;
        case SHARE_TYPE_MOMENTS:
            [self wechatMomentsShared];
            break;
        case SHARE_TYPE_WEIBO:
            [self weiboShared];
            break;
        case SHARE_TYPE_QQ:
            [self qqShared];
            break;
        case SHARE_TYPE_QQZONE:
            [self qqZoneShared];
            break;
        case SHARE_TYPE_COPY_LINK:
            [self copyLinkShared];
            break;
        default:
            break;
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGFloat width = collectionView.width / 5;
    return CGSizeMake(width, 75);
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat page = scrollView.contentOffset.x / self.collectionView.width;
    self.pageControl.currentPage = page;
}

#pragma mark - CustomMethod

- (void)customize {
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    layout.minimumLineSpacing = CGFLOAT_MIN;
    layout.minimumInteritemSpacing = CGFLOAT_MIN;
    layout.sectionInset = UIEdgeInsetsZero;
    [self.collectionView registerNib:[DarkShareItems nib] forCellWithReuseIdentifier:@"DarkShareItems"];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
}

- (void)show {
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [self showActionAnimated];
}

- (void)hide {
    [self hideActionAnimated];
}

- (void)showActionAnimated {
    CATransform3D translate = CATransform3DMakeTranslation(0, kScreenHeight, 0); //平移
    self.actionView.layer.transform = translate;
    self.backButton.alpha = 0;
    
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:1.0 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        self.backButton.alpha = 0.5;
        self.actionView.layer.transform = CATransform3DIdentity;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)hideActionAnimated {
    CATransform3D translate = CATransform3DMakeTranslation(0, kScreenHeight, 0); //平移
    self.actionView.layer.transform = CATransform3DIdentity;
    self.backButton.alpha = 0.5;
    
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:1.0 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        self.backButton.alpha = 0;
        self.actionView.layer.transform = translate;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        [[UIApplication sharedApplication].keyWindow dismissTips];
    }];
}

#pragma mark - IBAction

- (IBAction)hideShadowAction:(id)sender {
    [self hide];
}

- (IBAction)cancelAction:(id)sender {
    [self hide];
}

@end
