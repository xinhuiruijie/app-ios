//
//  ShareAlertItemCell.m
//  GeekMall
//
//  Created by 陈熙 on 2017/11/14.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "ShareAlertItemCell.h"
#import "BaseShareView.h"

@interface ShareAlertItemCell ()

@property (weak, nonatomic) IBOutlet UIImageView *iconImageView;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation ShareAlertItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)setType:(SHARE_TYPE)type {
    _type = type;
    switch (type) {
        case SHARE_TYPE_WECHAT: {
            self.iconImageView.image = [UIImage imageNamed:@"icon_pro_share_wechat"];
            self.titleLabel.text = @"微信好友";
        }
            break;
        case SHARE_TYPE_MOMENTS: {
            self.iconImageView.image = [UIImage imageNamed:@"icon_pro_share_fc"];
            self.titleLabel.text= @"朋友圈";
        }
            break;
        case SHARE_TYPE_WEIBO: {
            self.iconImageView.image = [UIImage imageNamed:@"icon_pro_share_sina"];
            self.titleLabel.text = @"微博";
        }
            break;
        case SHARE_TYPE_QQ: {
            self.iconImageView.image = [UIImage imageNamed:@"icon_pro_share_qq"];
            self.titleLabel.text = @"QQ好友";
        }
            break;
        case SHARE_TYPE_QQZONE: {
            self.iconImageView.image = [[UIImage imageNamed:@"icon_share_zone"] imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
            self.titleLabel.text = @"QQ空间";
        }
            break;
        case SHARE_TYPE_COPY_LINK: {
            self.iconImageView.image = [UIImage imageNamed:@"icon_share_link"];
            self.titleLabel.text = @"复制链接";
        }
        default:
            break;
    }
}

@end
