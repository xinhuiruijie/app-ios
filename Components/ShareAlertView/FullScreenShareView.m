//
//  FullScreenShareView.m
//  MotherPlanet
//
//  Created by liuyadi on 2018/4/18.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "FullScreenShareView.h"
#import "DarkShareItems.h"
#import "UserPointModel.h"

@interface FullScreenShareView () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UIView *actionView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;

@end

@implementation FullScreenShareView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [UserPointModel userPointName:MODULE_NAME_CLICK_SHARE_FINISH];
    
    [self.collectionView registerNib:[DarkShareItems nib] forCellWithReuseIdentifier:@"DarkShareItems"];
    
    UICollectionViewFlowLayout *layout = (UICollectionViewFlowLayout *)self.collectionView.collectionViewLayout;
    layout.minimumLineSpacing = CGFLOAT_MIN;
    layout.minimumInteritemSpacing = CGFLOAT_MIN;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    self.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return 6;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    DarkShareItems *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"DarkShareItems" forIndexPath:indexPath];
    SHARE_TYPE type = SHARE_TYPE_WECHAT + indexPath.row;
    cell.data = @(type);
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    SHARE_TYPE type = SHARE_TYPE_WECHAT + indexPath.row;
    switch (type) {
        case SHARE_TYPE_WECHAT:
            [self wechatShared];
            break;
        case SHARE_TYPE_MOMENTS:
            [self wechatMomentsShared];
            break;
        case SHARE_TYPE_WEIBO:
            [self weiboShared];
            break;
        case SHARE_TYPE_QQ:
            [self qqShared];
            break;
        case SHARE_TYPE_QQZONE:
            [self qqZoneShared];
            break;
        case SHARE_TYPE_COPY_LINK:
            [self copyLinkShared];
            break;
        default:
            break;
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(self.collectionView.width / 2, self.collectionView.height / 3);
}

- (void)show {
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [self showActionAnimated];
}

- (void)hide {
    [self hideActionAnimated];
}

- (void)showActionAnimated {
    CATransform3D translate = CATransform3DMakeTranslation(kScreenWidth, 0, 0); //平移
    self.actionView.layer.transform = translate;
    self.backButton.alpha = 0;
    
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:1.0 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        self.backButton.alpha = 0.5;
        self.actionView.layer.transform = CATransform3DIdentity;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)hideActionAnimated {
    CATransform3D translate = CATransform3DMakeTranslation(kScreenWidth, 0, 0); //平移
    self.actionView.layer.transform = CATransform3DIdentity;
    self.backButton.alpha = 0.5;
    
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:1.0 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        self.backButton.alpha = 0;
        self.actionView.layer.transform = translate;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        [[UIApplication sharedApplication].keyWindow dismissTips];
    }];
}

#pragma mark - IBAction

- (IBAction)hideShadowAction:(id)sender {
    [self hide];
}

- (IBAction)cancelAction:(id)sender {
    [self hide];
}

@end
