//
//  ShareAlertView.m
//  GeekMall
//
//  Created by 陈熙 on 2017/11/14.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "ShareAlertView.h"
#import "ShareAlertItemCell.h"
#import "UserPointModel.h"

@interface ShareAlertView () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (weak, nonatomic) IBOutlet UIView *actionView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *collectionViewHLC;
@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *spaceHLC;

@end

@implementation ShareAlertView

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [UserPointModel userPointName:MODULE_NAME_CLICK_SHARE_FINISH];
    
    [self customize];
    
    self.shareItems = [NSMutableArray array];
    if ([AppConfig wechatSharedShow]) {
        [self.shareItems addObject:@(SHARE_TYPE_WECHAT)];
        [self.shareItems addObject:@(SHARE_TYPE_MOMENTS)];
    }
    if ([AppConfig weiboSharedShow]) {
        [self.shareItems addObject:@(SHARE_TYPE_WEIBO)];
    }
    if ([AppConfig QQSharedShow]) {
        [self.shareItems addObject:@(SHARE_TYPE_QQ)];
    }
}

- (void)layoutSubviews {
    [super layoutSubviews];
    self.frame = CGRectMake(0, 0, kScreenWidth, kScreenHeight);
}

#pragma mark - UICollectionViewDataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.shareItems.count;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    ShareAlertItemCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ShareAlertItemCell" forIndexPath:indexPath];
    cell.type = [self.shareItems[indexPath.item] integerValue];
    return cell;
}

#pragma mark - UICollectionViewDelegate

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    [[UIApplication sharedApplication].keyWindow presentLoadingTips:nil];
    switch ([self.shareItems[indexPath.item] integerValue]) {
        case 0:
            [self wechatShared];
            break;
        case 1:
            [self wechatMomentsShared];
            break;
        case 2:
            [self weiboShared];
            break;
        case 3:
            [self qqShared];
            break;
        case 4:
            [self qqZoneShared];
            break;
        case 5:
            [self copyLinkShared];
            break;
        default:
            break;
    }
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(collectionView.width / 4, collectionView.height);
}

#pragma mark - CustomMethod

- (void)customize {
    UICollectionViewFlowLayout *layout = [[UICollectionViewFlowLayout alloc] init];
    layout.minimumLineSpacing = CGFLOAT_MIN;
    layout.minimumInteritemSpacing = CGFLOAT_MIN;
    layout.sectionInset = UIEdgeInsetsZero;
    self.collectionView.collectionViewLayout = layout;
    [self.collectionView registerNib:[ShareAlertItemCell nib] forCellWithReuseIdentifier:@"ShareAlertItemCell"];
    self.collectionView.dataSource = self;
    self.collectionView.delegate = self;
}

- (void)show {
    [[UIApplication sharedApplication].keyWindow addSubview:self];
    [self showActionAnimated];
}

- (void)hide {
    [self hideActionAnimated];
}

- (void)showActionAnimated {
    CATransform3D translate = CATransform3DMakeTranslation(0, kScreenHeight, 0); //平移
    self.actionView.layer.transform = translate;
    self.backButton.alpha = 0;
    
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:1.0 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        self.backButton.alpha = 0.5;
        self.actionView.layer.transform = CATransform3DIdentity;
    } completion:^(BOOL finished) {
        
    }];
}

- (void)hideActionAnimated {
    CATransform3D translate = CATransform3DMakeTranslation(0, kScreenHeight, 0); //平移
    self.actionView.layer.transform = CATransform3DIdentity;
    self.backButton.alpha = 0.5;
    
    [UIView animateWithDuration:0.4 delay:0 usingSpringWithDamping:1.0 initialSpringVelocity:1.0 options:UIViewAnimationOptionTransitionFlipFromLeft animations:^{
        self.backButton.alpha = 0;
        self.actionView.layer.transform = translate;
    } completion:^(BOOL finished) {
        [self removeFromSuperview];
        [[UIApplication sharedApplication].keyWindow dismissTips];
    }];
}

#pragma mark - IBAction

- (IBAction)hideShadowAction:(id)sender {
    [self hide];
}

- (IBAction)cancelAction:(id)sender {
    [self hide];
}

@end
