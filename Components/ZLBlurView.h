//
//  ZLBlurView.h
//  MotherPlanet
//
//  Created by GeekZooStudio on 2018/4/16.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ZLBlurView : UIView
@property (nonatomic, strong) UIColor *blurTintColor;
@property (readonly) UIToolbar *toolBar;
@end
