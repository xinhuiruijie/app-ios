//
//  PlayingLineAnimatedView.m
//  MotherPlanet
//
//  Created by GeekZooStudio on 2018/4/23.
//  Copyright © 2018年 Geek Zoo Studio. All rights reserved.
//

#import "PlayingLineAnimatedView.h"

static NSInteger lineCount = 4;

@implementation PlayingLineAnimatedView
{
    float _lineWidth;
    UIColor *_lineColor;
    NSMutableArray *_lineArray;
}

- (instancetype)initWithCoder:(NSCoder *)coder
{
    self = [super initWithCoder:coder];
    if (self) {
        _lineWidth = 2;
        _lineColor = [AppTheme textSelectedColor];
        _lineArray = [[NSMutableArray alloc] initWithCapacity:lineCount];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame lineWidth:(float)lineWidth lineColor:(UIColor *)lineColor {
    self = [super initWithFrame:frame];
    if (self) {
        _lineWidth = lineWidth;
        _lineColor = lineColor;
        _lineArray = [[NSMutableArray alloc] initWithCapacity:lineCount];
    }
    return self;
}

- (void)didMoveToSuperview {
    [super didMoveToSuperview];
    [self buildDefaultLayout];
}

- (NSArray *)values {
    return @[@[@0.2, @0.4, @0.6, @0.8, @1.0, @0.8, @0.6, @0.4, @0.2, @0.0, @0.2],
             @[@0.8, @1.0, @0.8, @0.6, @0.4, @0.2, @0.0, @0.2, @0.4, @0.6, @0.8],
             @[@0.4, @0.6, @0.8, @1.0, @0.8, @0.6, @0.4, @0.2, @0.0, @0.2, @0.4],
             @[@0.6, @0.8, @1.0, @0.8, @0.6, @0.4, @0.2, @0.0, @0.2, @0.4, @0.6]];
}

- (void)buildDefaultLayout {
    float margin = (self.bounds.size.width - lineCount * _lineWidth) / 4.0;
    float height = self.bounds.size.height;
    
    NSArray *pillarHeights = @[@(0.2 * height), @(0.8 * height), @(0.4 * height), @(0.6 * height)];
    
    for (int i = 0; i < lineCount; i++) {
        //初始化layer
        CAShapeLayer *layer = [[CAShapeLayer alloc] init];
        layer.fillColor = [UIColor clearColor].CGColor;
        layer.lineCap = kCALineCapRound;
        layer.strokeColor = _lineColor.CGColor;
        layer.frame = self.bounds;
        layer.lineWidth = _lineWidth;
        [self.layer addSublayer:layer];
        
        //设置layer的位置
        CGFloat pillarHeight = [pillarHeights[i] floatValue];
        CGFloat x = (i + 1) * margin + i * _lineWidth;
        CGPoint startPoint = CGPointMake(x, height);
        CGPoint toPoint = CGPointMake(x, height - pillarHeight);
        UIBezierPath * path = [UIBezierPath bezierPath];
        [path moveToPoint:startPoint];
        [path addLineToPoint:toPoint];
        layer.path = path.CGPath;
        [_lineArray addObject:layer];
    }
}

- (void)buildLayout {
    float margin = (self.bounds.size.width - lineCount * _lineWidth) / 4.0;
    float height = self.bounds.size.height;
    
    for (int i = 0; i < lineCount; i++) {
        //初始化layer
        CAShapeLayer *layer = [[CAShapeLayer alloc] init];
        layer.fillColor = [UIColor clearColor].CGColor;
        layer.lineCap = kCALineCapRound;
        layer.strokeColor = _lineColor.CGColor;
        layer.frame = self.bounds;
        layer.lineWidth = _lineWidth;
        [self.layer addSublayer:layer];
        
        //设置layer的位置
        CGFloat pillarHeight = height;
        CGFloat x = (i + 1) * margin + i * _lineWidth;
        CGPoint startPoint = CGPointMake(x, height);
        CGPoint toPoint = CGPointMake(x, height - pillarHeight);
        UIBezierPath * path = [UIBezierPath bezierPath];
        [path moveToPoint:startPoint];
        [path addLineToPoint:toPoint];
        layer.path = path.CGPath;
        [_lineArray addObject:layer];
    }
}

- (void)removeLayout {
    for (int i = 0; i < _lineArray.count; i++) {
        CALayer *layer = [_lineArray objectAtIndex:i];
        [layer removeFromSuperlayer];
    }
    
    [_lineArray removeAllObjects];
}

- (void)addAnimation {
    [self removeLayout];
    
    [self buildLayout];
    
    for (int i = 0; i < _lineArray.count; i++) {
        CALayer *layer = [_lineArray objectAtIndex:i];
        //设置动画
        CAKeyframeAnimation * animation = [CAKeyframeAnimation animationWithKeyPath:@"strokeEnd"];
        animation.values = [self values][i];
        animation.duration = 1.5;
        animation.repeatCount = NSIntegerMax;
        animation.removedOnCompletion = NO; // 必须设为NO否则会被销毁掉
        animation.fillMode = kCAFillModeForwards;
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
        [layer addAnimation:animation forKey:@"ESSEQAnimation"];
    }
}

- (void)removeAnimation {
    for (int i = 0; i < _lineArray.count; i++) {
        CALayer *layer = [_lineArray objectAtIndex:i];
        [layer removeAnimationForKey:@"ESSEQAnimation"];
    }
    
    [self removeLayout];
    
    [self buildDefaultLayout];
}

@end
