//
//  TopicCustomTransition.m
//  PuBuLiu
//
//  Created by 王艳清 on 16/7/1.
//  Copyright © 2016年 王艳清. All rights reserved.
//

#import "TopicCustomTransition.h"
#import "TopicListController.h"
#import "TopicVideoListController.h"
#import "TopicListCell.h"
#import "TopicVideoHeader.h"

@interface TopicCustomTransition ()

@property (nonatomic, assign) TransitionType type;

@end

@implementation TopicCustomTransition

- (instancetype)initWithTransitionType:(TransitionType)type {
    if (self = [super init]) {
        _type = type;
    }
    return self;
}

- (NSTimeInterval)transitionDuration:(id<UIViewControllerContextTransitioning>)transitionContext {
    return 0.25;
}

- (void)animateTransition:(id<UIViewControllerContextTransitioning>)transitionContext {
    switch (_type) {
        case push:
            [self doPushAnimation:transitionContext];
            break;
        case pop:
            [self doPopAnimation:transitionContext];
            break;
        default:
            break;
    }
}

/**
 *  执行push过渡动画
 */
- (void)doPushAnimation:(id<UIViewControllerContextTransitioning>)transitionContext{
    TopicListController *fromVC = (TopicListController *)[transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    TopicVideoListController *toVC = (TopicVideoListController *)[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    //拿到当前点击的cell的imageView
    TopicListCell *cell = fromVC.currentCell;
    UIView *containerView = [transitionContext containerView];
    //snapshotViewAfterScreenUpdates 对cell的imageView截图保存成另一个视图用于过渡，并将视图转换到当前控制器的坐标
    UIView *tempView = [cell.coverView snapshotViewAfterScreenUpdates:NO];
    tempView.frame = [cell convertRect:cell.coverView.bounds toView: containerView];
    //设置动画前的各个控件的状态
    cell.hidden = YES;
    toVC.view.alpha = 0;
    toVC.header.hidden = YES;
    //tempView 添加到containerView中，要保证在最前方，所以后添加
    [containerView addSubview:toVC.view];
    [containerView addSubview:tempView];
    //开始做动画
    tempView.contentMode = UIViewContentModeScaleAspectFill;
    [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
        CGFloat headerHeight = ceil(kScreenWidth * 420 / 750) + ([SDiOSVersion deviceSize] == Screen5Dot8inch ? 22 : 0);
        tempView.frame = [toVC.header.coverPhoto convertRect:CGRectMake(0, 0, kScreenWidth, headerHeight) toView:containerView];
        toVC.view.alpha = 1;
    }completion:^(BOOL finished) {
//        CGFloat headerHeight = ceil(kScreenWidth * 311 / 750);
//        tempView.frame = CGRectMake(15, 15, kScreenWidth - 30, headerHeight);
        cell.hidden = NO;
        tempView.hidden = YES;
        toVC.header.hidden = NO;
        //如果动画过渡取消了就标记不完成，否则才完成，这里可以直接写YES，如果有手势过渡才需要判断，必须标记，否则系统不会中动画完成的部署，会出现无法交互之类的bug
        [transitionContext completeTransition:YES];
    }];
}
/**
 *  执行pop过渡动画
 */
- (void)doPopAnimation:(id<UIViewControllerContextTransitioning>)transitionContext{
    TopicVideoListController *fromVC = (TopicVideoListController *)[transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    TopicListController *toVC = (TopicListController *)[transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    TopicListCell *cell = toVC.currentCell;
    UIView *containerView = [transitionContext containerView];
    //这里的lastView就是push时候初始化的那个tempView
    UIView *tempView = containerView.subviews.lastObject;
    BOOL animated = [NSStringFromClass([tempView class]) isEqualToString:@"_UIReplicantView"];
    if (animated) {
        //设置初始状态
        cell.hidden = YES;
        fromVC.header.hidden = YES;
        tempView.hidden = NO;
        [containerView insertSubview:toVC.view atIndex:0];
        
        [UIView animateWithDuration:[self transitionDuration:transitionContext] animations:^{
            tempView.frame = [cell.animatedView convertRect:cell.animatedView.bounds toView:containerView];
            fromVC.view.alpha = 0;
        } completion:^(BOOL finished) {
            //由于加入了手势必须判断
            [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
            if ([transitionContext transitionWasCancelled]) {//手势取消了，原来隐藏的imageView要显示出来
                //失败了隐藏tempView，显示fromVC.imageView
                tempView.hidden = YES;
                fromVC.header.hidden = NO;
            }else{//手势成功，cell的imageView也要显示出来
                //成功了移除tempView，下一次pop的时候又要创建，然后显示cell的imageView
                cell.hidden = NO;
                [tempView removeFromSuperview];
            }
        }];
    } else {
        //设置初始状态
//        cell.hidden = YES;
//        fromVC.header.hidden = YES;
//        tempView.hidden = NO;
        [containerView insertSubview:toVC.view atIndex:0];
//
//        tempView.frame = [cell.animatedView convertRect:cell.animatedView.bounds toView:containerView];
        fromVC.view.alpha = 0;
        
        [transitionContext completeTransition:![transitionContext transitionWasCancelled]];
//        if ([transitionContext transitionWasCancelled]) {//手势取消了，原来隐藏的imageView要显示出来
//            //失败了隐藏tempView，显示fromVC.imageView
//            tempView.hidden = YES;
//            fromVC.header.hidden = NO;
//        }else{//手势成功，cell的imageView也要显示出来
//            //成功了移除tempView，下一次pop的时候又要创建，然后显示cell的imageView
            cell.hidden = NO;
            [tempView removeFromSuperview];
//        }
        
        CATransition *transition = [CATransition animation];
        transition.duration = 0.3f;
        transition.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
        transition.type = kCATransitionPush;
        transition.subtype = kCATransitionFromLeft;
        [toVC.view.layer addAnimation:transition forKey:nil];
    }
}

@end
