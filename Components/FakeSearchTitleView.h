//
//  FakeSearchTitleView.h
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/21.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger, FakeSearchType) {
    FakeSearchHome,
    FakeSearchPlanet,
};

@interface FakeSearchTitleView : UIView

@property (nonatomic, assign) FakeSearchType searchType;

- (void)startAnimation;

@end
