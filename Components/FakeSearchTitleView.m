//
//  FakeSearchTitleView.m
//  MotherPlanet
//
//  Created by liuyadi on 2017/12/21.
//  Copyright © 2017年 Geek Zoo Studio. All rights reserved.
//

#import "FakeSearchTitleView.h"
#import "SearchViewController.h"

@interface FakeSearchTitleView () <UITextFieldDelegate>

@property (nonatomic, strong) UIView *parentView;
@property (nonatomic, strong) UILabel *placeHolderLabel;
@property (nonatomic, strong) UIButton *transparentButton;

@end

@implementation FakeSearchTitleView

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = [UIColor clearColor];
        
        CGFloat width = frame.size.width - 180;
        
        UIView *parentView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, width, frame.size.height)];
        parentView.center = CGPointMake(frame.size.width / 2, frame.size.height / 2);
        parentView.backgroundColor = [UIColor colorWithRGBValue:0xE9ECF0];
        parentView.layer.cornerRadius = 12;
        parentView.layer.masksToBounds = YES;
        [self addSubview:parentView];
        self.parentView = parentView;
        
        UILabel *placeHolderLabel = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, width - 30, frame.size.height)];
        placeHolderLabel.textAlignment = NSTextAlignmentCenter;
        placeHolderLabel.text = @"#发现不可思议#";
        placeHolderLabel.font = [UIFont systemFontOfSize:11];
        placeHolderLabel.textColor = [AppTheme normalColor];
        [self.parentView addSubview:placeHolderLabel];
        self.placeHolderLabel = placeHolderLabel;
        
        UIButton *transparentButton = [[UIButton alloc] initWithFrame:placeHolderLabel.frame];
        [transparentButton addTarget:self action:@selector(gotoSearchView) forControlEvents:UIControlEventTouchUpInside];
        [self.parentView addSubview:transparentButton];
        self.transparentButton = transparentButton;
        
        self.alpha = 0;
    }
    return self;
}

- (void)layoutSubviews {
    [super layoutSubviews];
    
    CGFloat width = self.bounds.size.width - 10;    // 左边距离navigationItem有假的间距
//    self.parentView.frame = self.bounds;
    self.parentView.frame = CGRectMake(0, 0, width, self.bounds.size.height);
    self.placeHolderLabel.frame = self.parentView.bounds;
    self.transparentButton.frame = self.parentView.bounds;
}

- (void)gotoSearchView {
    if (self.searchType == FakeSearchHome) {
        [AppAnalytics clickEvent:@"click_star_search"];
    } else if (self.searchType == FakeSearchPlanet) {
        [AppAnalytics clickEvent:@"click_planet_search"];
    }
    SearchViewController *searchView = [SearchViewController spawn];
    searchView.hidesBottomBarWhenPushed = YES;
    [[MPRootViewController sharedInstance].currentViewController.navigationController pushViewController:searchView animated:NO];
}

- (void)startAnimation {
//    CATransform3D translate = CATransform3DMakeTranslation(100, 0, 0);
//    CATransform3D scale = CATransform3DScale(translate, 1, 1, 1);
//
//    CATransform3D beforeTranslate = CATransform3DMakeTranslation(0, 0, 0);
//    self.layer.transform = CATransform3DScale(beforeTranslate, 0, 1, 1);
//    self.layer.anchorPoint = CGPointMake(1, 0.5);
    self.alpha = 0;
    [UIView animateWithDuration:0.6 animations:^{
//        self.layer.transform = scale;
        self.alpha = 1.0;
    } completion:nil];
    
//    [UIView animateWithDuration:0.8 delay:0.1 options:UIViewAnimationOptionCurveLinear animations:^{
//        self.alpha = 1.0;
//    } completion:nil];
}

@end
